#ifndef STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#endif

#include <tchar.h>
#include <stdio.h>
#include <assert.h>
#include <core\SkCanvas.h>
#include <core\SkBitmap.h>
#include <core\SkTypeface.h>
#include <core\SkImageDecoder.h>
#include <core\SkStream.h>
#include "RenderTarget_Gl.h"
#include "GetResource.h"
#include "Core/Warp/IWarp.h"
#include "Yuv420p_Gl.h"
#include "GLSharder.h"
//#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")
#pragma warning(disable: 4244 4267)

#ifndef SAFE_RELEASE
#define SAFE_RELEASE(p)      { if (p) { (p)->Release(); (p)=NULL; } }
#endif



namespace KKUI
{

	SRenderFactory_Gl::SRenderFactory_Gl()
		: m_pFragmentShader(NULL)
		, m_pVertexShader(NULL)
	{
		//int i = gladLoadGL();
		SetupPixelFormat(GetShellWindow(), &m_hRC, &m_hDC);
	}
	SRenderFactory_Gl::~SRenderFactory_Gl()
	{


	}

	void SRenderFactory_Gl::MakeLoadContext()
	{

		int cc = wglMakeCurrent(m_hDC, m_hRC);
		cc=0;
	}


	int SRenderFactory_Gl::SetupPixelFormat(HWND hwnd, HGLRC* prc, HDC* pDC)
	{
		//创建上下文
		*pDC = ::GetDC(hwnd);
		PIXELFORMATDESCRIPTOR pfd, *ppfd;
		int pixelformat;
		ppfd = &pfd;
		ppfd->nSize = sizeof(PIXELFORMATDESCRIPTOR);
		ppfd->nVersion = 1;
		ppfd->dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
		ppfd->dwLayerMask = PFD_MAIN_PLANE;
		ppfd->iPixelType = PFD_TYPE_COLORINDEX;
		ppfd->cColorBits = 8;
		ppfd->cDepthBits = 16;
		ppfd->cAccumBits = 0;
		ppfd->cStencilBits = 0;

		pixelformat = ChoosePixelFormat(*pDC, ppfd);

		if ( (pixelformat = ChoosePixelFormat(*pDC, ppfd)) == 0 )
		{
			return FALSE;
		}

		if (SetPixelFormat(*pDC, pixelformat, ppfd) == FALSE)
		{
			return FALSE;
		}

		
		*prc = wglCreateContext(*pDC);

		int i = gladLoadGL();
		/*共享

		BOOL wglShareLists(
			HGLR C   hglrc1,  // 提供共享资源的render context
			HGLRC   hglrc2    // 共享别人资源的render context
			);
		*/
		//wglShareLists(m_hRC2, m_hRC1); 

		wglMakeCurrent(*pDC, *prc);

		const GLubyte* OpenGLVersion =glGetString(GL_VERSION); //返回当前OpenGL实现的版本号

		//const GLubyte* gl = glGetString(GL_EXTENSIONS);
		//glCreateShaderObjectARB()

		return 1;
	}

	bool SRenderFactory_Gl::init()
	{
		//MakeLoadContext();
		m_pFragmentShader = new CFragmentShader_Gl(this);
		//m_pFragmentShader->IniPs();
		m_pVertexShader = new CVertexShader_Gl(this);
		//m_pVertexShader->IniPs();
		return 0;
	}



	int    SRenderFactory_Gl::InitShader()
	{


		return 0;
	}
	int  SRenderFactory_Gl::CreateRenderTarget(IRenderTarget ** ppRenderTarget,void* Handle,int nWid,int nHei)
	{
		CRendGurd gd(m_lock);
		CRenderTarget_Gl* re = new CRenderTarget_Gl(this,(HWND)Handle,nWid,nHei);
		*ppRenderTarget = re;
		m_IRenderObjSet.insert((IRenderObj*)re);
		return 0;
	}


	

	
	int    SRenderFactory_Gl::CreateBitmap(IBitmap ** ppBitmap)
	{
		CRendGurd gd( m_lock);
		CBitmap_Gl *p = new CBitmap_Gl(this);
		*ppBitmap = p;
		m_IRenderObjSet.insert(p);
		return 0;
	}

	int    SRenderFactory_Gl::CreateYUV420p(IPicYuv420p ** ppBitmap)
	{
		CRendGurd gd( m_lock);
		CYuv420p_Gl* pGL = new CYuv420p_Gl(this);
		*ppBitmap=pGL;
		m_IRenderObjSet.insert(pGL);
		return 0;
	}
	int   SRenderFactory_Gl::BeforePaint(IRenderTarget* pRenderTarget)
	{
		this->Lock();
        CRenderTarget_Gl* gl= (CRenderTarget_Gl*)pRenderTarget;
		gl->Inix();
		return 1;

	}
	void   SRenderFactory_Gl::EndPaint(IRenderTarget* pRenderTarget,bool WarpNet)
	{

		CRenderTarget_Gl* gl= (CRenderTarget_Gl*)pRenderTarget;
		gl->SwapBuffer();
		this->UnLock();
		//SwapBuffers(m_hDC);
	}
	

	void SRenderFactory_Gl::CreateFont(IFont** ppFont,int FontSize,wchar_t* FontName)
	{
		CFont_Gl* ff = new CFont_Gl(this,FontSize,FontName);
		*ppFont=ff;
		m_IRenderObjSet.insert(ff);
	}

	void   SRenderFactory_Gl:: Lock()
	{
		m_lock.Lock();
	}
	void   SRenderFactory_Gl::UnLock()
	{
		m_lock.Unlock();
	}
	void*   SRenderFactory_Gl::CreateVideoDecoder(int CodecId,int Width,int Height)
	{
		return 0;
	}
	void    SRenderFactory_Gl::DeleteVideoDecoder(void* pVD)
	{

	}

	void*      SRenderFactory_Gl::CreateDxTexture()
	{

		return 0;
	}
	void       SRenderFactory_Gl::DeleteDxTexturer(void* DxTexture)
	{

	}

	CBitmap_Gl::CBitmap_Gl(SRenderFactory_Gl* pRenderFactory)
		: m_pSrcBuffer(NULL)
		, m_nSrcBufferLen(0)
	{
	
	}

	CBitmap_Gl::~CBitmap_Gl()
	{
		if (m_pSrcBuffer)
			stbi_image_free(m_pSrcBuffer);
		m_pSrcBuffer = NULL;
	}
	void   CBitmap_Gl::Release2()
	{

	}
	void   CBitmap_Gl::ReLoadRes()
	{
		if(!m_pSrcBuffer)
			return;
		//glDeleteTextures(1, &m_glTexture);

		/*if (m_nChannels == 3)
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_sz.cx, m_sz.cy, 0, GL_RGB, GL_UNSIGNED_BYTE, m_pSrcBuffer);
		else if (m_nChannels == 4)
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_sz.cx, m_sz.cy, 0, GL_RGBA, GL_UNSIGNED_BYTE, m_pSrcBuffer);*/
	}
	kkSize    CBitmap_Gl::Size() const
	{
		return m_sz;
	}

	int       CBitmap_Gl::Width()
	{
		return m_sz.cx;
	}
	int       CBitmap_Gl::Height()
	{
		return m_sz.cy;
	}

	int   CBitmap_Gl::LoadFromFile(const char* FileName)
	{
		int width, height, nChannels;
		m_pSrcBuffer = stbi_load(FileName, &width, &height, &nChannels, STBI_default);
		if (m_pSrcBuffer)
		{
			m_nSrcBufferLen = _msize(m_pSrcBuffer);
			m_sz.cx = width;
			m_sz.cy = height;
			m_nChannels = nChannels;
		}
		return  0;
	}
	int	CBitmap_Gl::LoadFromMemory(const void* pBuf, int szLen)
	{
		int width, height, nChannels;
		m_pSrcBuffer = stbi_load_from_memory((stbi_uc*)(pBuf), szLen, &width, &height, &nChannels, STBI_default);
		if (m_pSrcBuffer)
		{
			m_nSrcBufferLen = szLen;
			m_sz.cx = width;
			m_sz.cy = height;
			m_nChannels = nChannels;
		}
		return  0;
	}

	void CBitmap_Gl::ReleaseBuffer()
	{
		if (m_pSrcBuffer)
			stbi_image_free(m_pSrcBuffer);
		m_pSrcBuffer = NULL;
	}

	int CBitmap_Gl::GetChannelCount()
	{
		return m_nChannels;
	}

	void * CBitmap_Gl::GetBuffer()
	{
		return m_pSrcBuffer;
	}
	
	CRenderTarget_Gl::CRenderTarget_Gl(SRenderFactory_Gl* pRenderFactory, HWND h, int nWidth, int nHeight): 
		m_hView(h),
		m_pRenderFactory(pRenderFactory), 
		m_nWidth(nWidth),
		m_nHeight(nHeight),
		m_hDC(NULL),
		m_hRC(NULL)
	{
		m_pRenderFactory->AddRef();
		m_pRenderFactory->SetupPixelFormat(h, &m_hRC, &m_hDC); 

		int ret = wglShareLists(m_pRenderFactory->m_hRC, m_hRC); 
		ret= 0;
	}


	CRenderTarget_Gl::~CRenderTarget_Gl()
	{
		Release2();
	
	}
	void  CRenderTarget_Gl::SetEnableWarp(void*  pIWarp)
	{

		m_nWarp =pIWarp!=0 ? 1:0;

		m_pIWarp=pIWarp;
	}
	void  CRenderTarget_Gl::SetFusionzone(void* Fusionzone)
	{
		m_pFusionzone=Fusionzone;
	}

	void CRenderTarget_Gl::DrawWarp(SVertInfo* VertInfo,int VertCount)
	{
		int i=0;
	}

	

	void CRenderTarget_Gl::SwapBuffer()
	{
		SwapBuffers(m_hDC);
	}

	GLfloat CRenderTarget_Gl::GetRValue(unsigned int cl)
	{
		BYTE rgb = (BYTE)(cl >> 16);
		return (GLfloat)rgb / 255;
	}

	GLfloat CRenderTarget_Gl::GetGValue(unsigned int cl)
	{
		BYTE rgb = (BYTE)(cl >> 8);
		return (GLfloat)rgb / 255;
	}

	GLfloat CRenderTarget_Gl::GetBValue(unsigned int cl)
	{
		BYTE rgb = (BYTE)(cl);
		return (GLfloat)rgb / 255;
	}

	void CRenderTarget_Gl::RecreateSwapChain()
	{
		
		int i=0;
	}

	void* CRenderTarget_Gl::OpenglExtProc(const char *name)
	{
		return NULL;
	}

	void  CRenderTarget_Gl::Release2()
	{
		/*if (m_hRC)
			wglDeleteContext(m_hRC);
		if (m_hDC)
			DeleteDC(m_hDC);

		m_hRC = NULL;
		m_hDC = NULL;*/
	}
	void  CRenderTarget_Gl::ReLoadRes()
	{
		int i=0;
	}
	void CRenderTarget_Gl::Inix()
	{
	      bool xx=wglMakeCurrent(m_hDC, m_hRC);
		  glClearColor(0,0,0,1);
		  glClear(GL_COLOR_BUFFER_BIT); //GL_COLOR_BUFFER_BIT| GL_DEPTH_BUFFER_BIT);
		 //glShadeModel(GL_SMOOTH);      //平滑着色

		  glViewport(0, 0, m_nWidth, m_nHeight);
		  glMatrixMode(GL_PROJECTION);
		  glLoadIdentity();
		  glTranslatef(0.0, 0.0, 0.0);
		  glOrtho(0.0, m_nWidth, m_nHeight, 0.0,0.0,1.0);

		 //glMatrixMode(GL_MODELVIEW);
		 // int r= glGetError();// != 
		  //GL_NO_ERROR
	}
	
	void   CRenderTarget_Gl::ReSize(int nWidth, int nHeight)
	{
		if( m_nWidth == nWidth && m_nHeight==nHeight)
			return;
		//Release2();
        //SetupPixelFormat();
		m_nWidth = nWidth;
		m_nHeight= nHeight;
		m_pRenderFactory->Lock();

		Inix();

		//RecreateSwapChain(); 
		m_pRenderFactory->UnLock();
	}

	int CRenderTarget_Gl::DrawBitmap(kkRect* pRcDest,IBitmap *pBitmap,int xSrc,int ySrc,int cx,int cy,unsigned char byAlpha/*=0xFF*/)
	{
		CBitmap_Gl* bit =( CBitmap_Gl*) pBitmap;
		kkSize sz = bit->Size();
		GlVertex vertex[4];

		//0(leftTop)->1(rightTop)->2(leftBottom)->3(rightBottom)
		GetVertex(vertex, *pRcDest, xSrc, ySrc, cx, cy, sz.cx, sz.cy);
		GLuint texture;
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		if (bit->GetChannelCount() == 3)
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, bit->Width(), bit->Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, bit->GetBuffer());
		else if (bit->GetChannelCount() == 4)
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, bit->Width(), bit->Height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, bit->GetBuffer());
		
		glEnable(GL_TEXTURE_2D);
		glColor3ub(255, 255, 255);
		glBegin(GL_TRIANGLE_STRIP);

		//0(leftTop)->1(rightTop)->2(leftBottom)->3(rightBottom)
		for (int i = 0; i < 4; ++i)
		{
			glTexCoord2f(vertex[i].s, vertex[i].t);
			glVertex3i(vertex[i].x, vertex[i].y, vertex[i].z);
		}
		glEnd();
		glDisable(GL_TEXTURE_2D);
		glDeleteTextures(1, &texture);
		return 1;
	}

	int CRenderTarget_Gl::DrawBitmapEx(kkRect* pRcDest,IBitmap *pBitmap,kkRect* pRcSrc,unsigned int expendMode,unsigned char byAlpha/*=0xFF/**/)
	{
		unsigned int expandModeLow = LOWORD(expendMode);

		kkSize sz=pBitmap->Size();
		if(expandModeLow == EM_NULL)
			return DrawBitmap(pRcDest,pBitmap,pRcSrc->left,pRcSrc->top,sz.cx,sz.cy,byAlpha);
		if(expandModeLow == EM_STRETCH)
		{
			//::AlphaBlend(m_hdc,pRcDest->left,pRcDest->top,pRcDest->right-pRcDest->left,pRcDest->bottom-pRcDest->top,
			//hmemdc,pRcSrc->left,pRcSrc->top,pRcSrc->right-pRcSrc->left,pRcSrc->bottom-pRcSrc->top,bf);

			DrawBitmap(pRcDest,pBitmap,pRcSrc->left,pRcSrc->top,pRcSrc->right-pRcSrc->left,pRcSrc->bottom-pRcSrc->top,byAlpha);
		}
		else
		{


			//int nWid=pRcSrc->right-pRcSrc->left;
			//int nHei=pRcSrc->bottom-pRcSrc->top;
			//for(int y=pRcDest->top ;y<pRcDest->bottom;y+=nHei)
			//{
			//    for(int x=pRcDest->left; x<pRcDest->right; x+=nWid)
			//    {
			//       // ::AlphaBlend(m_hdc,x,y,nWid,nHei,
			//         //   hmemdc,pRcSrc->left,pRcSrc->top,nWid,nHei,
			//        //    bf);                    
			//    }
			//}
		}

		return 1;
	}

	int CRenderTarget_Gl::DrawBitmap9Patch(kkRect* pRcDest,IBitmap *pBitmap,kkRect* pRcSrc,kkRect* pRcSourMargin,unsigned expendMode,unsigned char byAlpha/*=0xFF*/)
	{
		int xDest[4] = {pRcDest->left,pRcDest->left+pRcSourMargin->left,pRcDest->right-pRcSourMargin->right,pRcDest->right};
		int xSrc[4] = {pRcSrc->left,pRcSrc->left+pRcSourMargin->left,pRcSrc->right-pRcSourMargin->right,pRcSrc->right};
		int yDest[4] = {pRcDest->top,pRcDest->top+pRcSourMargin->top,pRcDest->bottom-pRcSourMargin->bottom,pRcDest->bottom};
		int ySrc[4] = {pRcSrc->top,pRcSrc->top+pRcSourMargin->top,pRcSrc->bottom-pRcSourMargin->bottom,pRcSrc->bottom};

		//首先保证九宫分割正常
		if(!(xSrc[0] <= xSrc[1] && xSrc[1] <= xSrc[2] && xSrc[2] <= xSrc[3])) 
			return S_FALSE;
		if(!(ySrc[0] <= ySrc[1] && ySrc[1] <= ySrc[2] && ySrc[2] <= ySrc[3])) 
			return S_FALSE;

		//调整目标位置
		int nDestWid=pRcDest->right-pRcDest->left;
		int nDestHei=pRcDest->bottom-pRcDest->top;

		if((pRcSourMargin->left + pRcSourMargin->right) > nDestWid)
		{//边缘宽度大于目标宽度的处理
			if(pRcSourMargin->left >= nDestWid)
			{//只绘制左边部分
				xSrc[1] = xSrc[2] = xSrc[3] = xSrc[0]+nDestWid;
				xDest[1] = xDest[2] = xDest[3] = xDest[0]+nDestWid;
			}else if(pRcSourMargin->right >= nDestWid)
			{//只绘制右边部分
				xSrc[0] = xSrc[1] = xSrc[2] = xSrc[3]-nDestWid;
				xDest[0] = xDest[1] = xDest[2] = xDest[3]-nDestWid;
			}else
			{//先绘制左边部分，剩余的用右边填充
				int nRemain=xDest[3]-xDest[1];
				xSrc[2] = xSrc[3]-nRemain;
				xDest[2] = xDest[3]-nRemain;
			}
		}

		if(pRcSourMargin->top + pRcSourMargin->bottom > nDestHei)
		{
			if(pRcSourMargin->top >= nDestHei)
			{//只绘制上边部分
				ySrc[1] = ySrc[2] = ySrc[3] = ySrc[0]+nDestHei;
				yDest[1] = yDest[2] = yDest[3] = yDest[0]+nDestHei;
			}else if(pRcSourMargin->bottom >= nDestHei)
			{//只绘制下边部分
				ySrc[0] = ySrc[1] = ySrc[2] = ySrc[3]-nDestHei;
				yDest[0] = yDest[1] = yDest[2] = yDest[3]-nDestHei;
			}else
			{//先绘制左边部分，剩余的用右边填充
				int nRemain=yDest[3]-yDest[1];
				ySrc[2] = ySrc[3]-nRemain;
				yDest[2] = yDest[3]-nRemain;
			}
		}

		//定义绘制模式
		unsigned int mode[3][3]={
			{EM_NULL,expendMode,EM_NULL},
			{expendMode,expendMode,expendMode},
			{EM_NULL,expendMode,EM_NULL}
		};

		for(int y=0;y<3;y++)
		{
			if(ySrc[y] == ySrc[y+1]) 
				continue;
			for(int x=0;x<3;x++)
			{
				if(xSrc[x] == xSrc[x+1])
					continue;
				kkRect rcSrc = {xSrc[x],ySrc[y],xSrc[x+1],ySrc[y+1]};
				kkRect rcDest ={xDest[x],yDest[y],xDest[x+1],yDest[y+1]};
				DrawBitmapEx(&rcDest,pBitmap,&rcSrc,mode[y][x],byAlpha);
			}
		}

		return S_OK;
	}

	int CRenderTarget_Gl::DrawYuv420p(IPicYuv420p *pYuv420p,SinkFilter*Filter,const SPalette& Palette,SVertInfo* VertInfo,int VertCount,int UseLast,SMaskInfo* maskinfo/*=0*/,int maskcount/*=0*/)
	{
		CYuv420p_Gl* pGL = (CYuv420p_Gl*)pYuv420p;
		GLuint rttTex = pGL->GetRttTexture();

		if (0 != rttTex)
		{
			

			GlVertex vertex[6];
			
			for(int i=0;i<6; ++i)
			{
				vertex[i].x = VertInfo[i].x;
				vertex[i].y = VertInfo[i].y;
				vertex[i].s = VertInfo[i].uv.x;
				vertex[i].t = VertInfo[i].uv.y;
				vertex[i].z = 0.0;// VertInfo[i].z;
			}

			glEnable(GL_TEXTURE_2D);
			glColor4f(1.0, 1.0, 1.0,1.0);
			//glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, rttTex);
			GLuint p = 0;
			CVertexShader_Gl* pVerShader = m_pRenderFactory->GetVertexShader();	
			if (pVerShader)
			{
				pVerShader->IniPs(p);
				//= pVerShader->GetVertexShader();
				glUseProgram(p);
			}
			
			CFragmentShader_Gl* pFragmentShader = m_pRenderFactory->GetFragmentShader();	
			if (pFragmentShader)
			{
				pVerShader->IniPs(p);
				//= pVerShader->GetVertexShader();
				glUseProgram(p);
			}

			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
			glBegin(GL_TRIANGLES);


			//0(leftTop)->1(rightTop)->2(leftBottom)->3(rightBottom)
			for (int i = 0; i < 6; ++i)
			{
				glTexCoord2f(vertex[i].s, vertex[i].t);
				glVertex3i(vertex[i].x, vertex[i].y, vertex[i].z);
			}

			glEnd();
			glDisable(GL_TEXTURE_2D);
			glDisable(GL_BLEND);
			//glBindFramebuffer(GL_FRAMEBUFFER, 0);
		}



		return 1;
	}

	int CRenderTarget_Gl::FilterYuv420p(IPicYuv420p *pYuv420p,SinkFilter* Filter)
	{
		return 0;
	}

	void CRenderTarget_Gl::GetVertex(GlVertex* vertex, const kkRect& rt, int xSrc, int ySrc, int NeedWidth, int NeedHeight, int SWidth, int SHeight)
	{
		int w = rt.right - rt.left;
		int h = rt.bottom - rt.top;

		vertex[0].x = rt.left;  //A     A(leftTop)->B(rightTop)->C(leftBottom)->D->(rightBottom)
		vertex[0].y = rt.top;
		vertex[0].z = 0.0f;
		vertex[0].s = (float)xSrc / SWidth;  //x
		vertex[0].t = (float)ySrc / SHeight; //y

		vertex[1].x = rt.right;  //B
		vertex[1].y = rt.top;
		vertex[1].z = 0.0f;
		vertex[1].s = (float)(xSrc + NeedWidth) / SWidth;
		vertex[1].t = (float)ySrc / SHeight;

		vertex[2].x = rt.left;   //C
		vertex[2].y = rt.bottom;
		vertex[2].z = 0.0f;
		vertex[2].s = (float)xSrc / SWidth;
		vertex[2].t = (float)(ySrc + NeedHeight) / SHeight;

		vertex[3].x = rt.right; //D
		vertex[3].y = rt.bottom;
		vertex[3].z = 0.0f;
		vertex[3].s = (float)(xSrc + NeedWidth) / SWidth;
		vertex[3].t = (float)(ySrc + NeedHeight) / SHeight;
	}

	void CRenderTarget_Gl::DrawLine(int startx, int starty, int endx, int endy, unsigned int color)
	{
		glColor3f(GetRValue(color), GetGValue(color), GetBValue(color));
		glBegin(GL_LINES);
		glVertex3f(startx+0.5, starty+0.5, 0.0f);
		glVertex3f(endx+0.5, endy+0.5, 0.0f);
		glEnd();	
	}

	void CRenderTarget_Gl::DrawLines(kkPoint* pts,int count, unsigned int color)
	{
		int i=0;
	}

	void CRenderTarget_Gl::DrawLines(SUVInfo* pts,int count, unsigned int color)
	{
		int i=0;
	}

	void CRenderTarget_Gl::DrawText(IFont* pFont,wchar_t *text,kkRect* pRcDest,unsigned int color/*=0xFFFFFFFF*/)
	{
		//CreateFontIndirect()
		CFont_Gl* gl = (CFont_Gl*)pFont;
		gl->DrawText(*pRcDest, text, GetRValue(color), GetGValue(color), GetBValue(color));

		/*HFONT hFont =CreateFont(gl->TextSize(),0,0,0,400,0,0,0,GB2312_CHARSET,0,0,0,FF_MODERN, gl->FaceName());
		CGLFont f;
		f.settext(pRcDest->left, pRcDest->top, text, hFont, 1, 1, 1);

		DeleteObject(hFont);*/
	}

	void CRenderTarget_Gl::MeasureText(IFont* pFont, wchar_t *text, kkSize *pSz)
	{
		CFont_Gl* gl = (CFont_Gl*)pFont;
		HDC hdc = gl->GetDC();
		SIZE sz;
		//和dx9获取到的大小不一样？？
		GetTextExtentPoint32(hdc, text, lstrlen(text), &sz);
		pSz->cx = sz.cx;
		pSz->cy = sz.cy;
	}

	void CRenderTarget_Gl::TextOutV(IFont* pFont,int x,int y , wchar_t* strText,unsigned int color/*=0xFFFFFFFF*/)
	{
		wchar_t* p = strText;
		while (*p)
		{
			wchar_t*  p2 = p + 1;
			kkSize szWord;
			wchar_t temp[2] = { *p };
			MeasureText(pFont, temp, &szWord);

			kkRect Rt = { 0,0 };
			Rt.left = x;
			Rt.top = y;
			Rt.bottom = Rt.top + szWord.cy;
			Rt.right = Rt.left + szWord.cx;
			DrawText(pFont, temp, &Rt, color);
			p = p2;
			y += szWord.cy-4;
		}
	}

	void CRenderTarget_Gl::DrawTextV(IFont* pFont,wchar_t *text,kkRect* pRcDest,unsigned int color/*=0xFFFFFFFF*/)
	{
		TextOutV(pFont, pRcDest->left, pRcDest->top, text, color);
	}

	void CRenderTarget_Gl::MeasureTextV(IFont* pFont,wchar_t *text,kkSize *pSz)
	{
		kkSize szRet = { 0,0 };
		wchar_t* p = text;
		while (*p)
		{
			wchar_t* p2 = p + 1;
			kkSize szWord;
			wchar_t temp[2] = { *p };
			MeasureText(pFont, temp, &szWord);
			szRet.cx = szRet.cx > szWord.cx ? szRet.cx : szWord.cx;
			szRet.cy += szWord.cy;
			p = p2;
		}
		*pSz = szRet;
	}

	void CRenderTarget_Gl::Rectangle(kkRect* rt,unsigned int cr)
	{
		int i=0;
	}

	void CRenderTarget_Gl::RectangleByPt(kkPoint* pt,int width,int height,unsigned int cr)
	{
		int i=0;
	}

	int CRenderTarget_Gl::FillRect(kkRect* rt,unsigned int cr)
	{
		glColor3f(GetRValue(cr), GetGValue(cr), GetBValue(cr));
		glBegin(GL_TRIANGLE_STRIP);
		glVertex3f(rt->left, rt->bottom, 0.0f);
		glVertex3f(rt->left, rt->top, 0.0f);
		glVertex3f(rt->right, rt->bottom, 0.0f);
		glVertex3f(rt->right, rt->top, 0.0f);
		glEnd();	
		return 1;
	}

	void CRenderTarget_Gl::FillRect(kkRect* rt,unsigned int cr1,unsigned int cr2,unsigned int cr3,unsigned int cr4)
	{
		glBegin(GL_TRIANGLE_STRIP);
		glColor3f(GetRValue(cr2), GetGValue(cr2), GetGValue(cr2));
		glVertex3f(rt->left, rt->bottom, 0.0f);
		glColor3f(GetRValue(cr1), GetGValue(cr1), GetGValue(cr1));
		glVertex3f(rt->left, rt->top, 0.0f);
		glColor3f(GetRValue(cr4), GetGValue(cr4), GetGValue(cr4));
		glVertex3f(rt->right, rt->bottom, 0.0f);
		glColor3f(GetRValue(cr3), GetGValue(cr3), GetGValue(cr3));
		glVertex3f(rt->right, rt->top, 0.0f);
		glEnd();
	}

	void CRenderTarget_Gl::FillRect(SUVInfo* pts1,SUVInfo* pts2,SUVInfo* pts3,SUVInfo* pts4,unsigned int cr1,unsigned int cr2,unsigned int cr3,unsigned int cr4)
	{
		int i=0;
	}

	void CRenderTarget_Gl::FillRectByPt(kkPoint* pt,int width,int height,unsigned int cr)
	{
		int i=0;
	}



	void CRenderTarget_Gl::DrawCircle(int xCenter, int yCenter, int nRadius,unsigned int cr,int fillmodel/*=0*/)
	{
		glColor3f(GetRValue(cr), GetGValue(cr), GetBValue(cr));
		int x,y,delta,delta1,delta2,direction;
		x=0; 
		y=nRadius;

		delta=2*(1-nRadius);
		
		int i=0;
		glBegin(GL_POINTS);
		if(fillmodel==1)
		{
			i=1;
			glVertex2i(xCenter, yCenter);
		}
		while(y>=0){
			//(x,y)		
			glVertex2i(x+xCenter, y+yCenter);
			++i;

			//(x,-y)
			glVertex2i(x+xCenter, -y+yCenter);	
			++i;

			//(-x, y)
			glVertex2i(-x+xCenter, y+yCenter);
			++i;

			//// //(-x, y)
			glVertex2i(-x+xCenter, -y+yCenter);
			++i;



			if(delta<0)
			{   
				delta1=2*(delta+y)-1;
				if(delta1<=0)
					direction=1; 
				else 
					direction=2; 
			}else if(delta>0)
			{   
				delta2=2*(delta-x)-1;
				if(delta2<=0)
					direction=2; 
				else 
					direction=3; 
			}else  
				direction=2;

			if (direction == 1)
			{
				x++; 
				delta += 2 * x + 1;
			}
			else if (direction == 2) {
				x++; 
				y--; 
				delta+=2*(x-y+1); 
			}
			else if (direction == 3) {
				y--;
				delta+=(-2*y+1);
			}
		}
		
		glEnd();	
	}

	CFont_Gl::CFont_Gl(SRenderFactory_Gl* pRenderFactory, int FontSize, wchar_t* FontName)
		: m_nTextSize(FontSize)
		, m_pRenderFactory(pRenderFactory)
		, m_hFont(NULL)
		, m_hdc(NULL)
	{
		memset(m_szFontName, 0, 128);
		if(FontName)
			wcscpy(m_szFontName, FontName);

		LOGFONT lf;
		ZeroMemory(&lf, sizeof(LOGFONT));
		lf.lfHeight = m_nTextSize;
		lf.lfItalic = false;
		lf.lfCharSet = DEFAULT_CHARSET;
		lf.lfOutPrecision = 0;
		lf.lfQuality = 0;
		lf.lfPitchAndFamily = 0;
		if (m_szFontName) {
			wcscpy(lf.lfFaceName, m_szFontName);
		}
		
		m_hFont = CreateFontIndirect(&lf);
		//m_hFont = CreateFont(m_nTextSize, 0,0,0, FW_NORMAL, 0,0,0,DEFAULT_CHARSET,0,0,0,DEFAULT_PITCH, m_szFontName);
		m_hdc = ::GetDC(NULL);

		m_pRenderFactory->AddRef();
		ReLoadRes();	 
	}


	CFont_Gl::~CFont_Gl()
	{
		if (m_hFont)
			DeleteObject(m_hFont);
		m_hFont = NULL;

		if (m_hdc)
			DeleteDC(m_hdc);
		m_hdc = NULL;
	}

	void CFont_Gl::Release2()
	{
		
	}

	void CFont_Gl::ReLoadRes()
	{

	}
	//179.135.233.147
	//179.135.239.151  (712,536)
	void CFont_Gl::DrawText(kkRect& rect, LPCTSTR lpszText, GLfloat r, GLfloat g, GLfloat b)
	{
		glPushMatrix();
		glPushAttrib(GL_CURRENT_BIT);
		//glDisable(GL_TEXTURE_2D);
		glColor3f(r, g, b);
		//glDisable(GL_LIGHTING); 

		double x = rect.left;
		double y = rect.top;
		double z = 0.0f;

		BITMAP bm;
		SIZE size;
		HDC memdc = ::CreateCompatibleDC(wglGetCurrentDC());
		SelectObject(memdc, m_hFont);
		::GetTextExtentPoint32(memdc, lpszText, _tcslen(lpszText), &size);
		HBITMAP hbm = CreateBitmap(
			size.cx,         // bitmap width, in pixels
			size.cy,        // bitmap height, in pixels
			1,       // number of color planes
			1,   // number of bits to identify color
			NULL // color data array
		);
		HBITMAP oldBmp = (HBITMAP)SelectObject(memdc, hbm/*bitmap*/);
		SetBkColor(memdc, RGB(0, 0, 0));
		SetTextColor(memdc, RGB(255, 255, 255));
		RECT rc = {0,0, size.cx, size.cy};
		::DrawText(memdc, lpszText, _tcslen(lpszText), &rc, DT_CENTER);
		//TextOut(memdc, 0, 0, lpszText, _tcslen(lpszText));
		::GetObject(hbm, sizeof(BITMAP), &bm);

		//把图片大小变为2的幂次方
		size.cx = (bm.bmWidth + 31) & (~31);

		int bufsize = size.cy * size.cx;
		struct {
			BITMAPINFOHEADER bih;
			RGBQUAD col[2];
		}bic;
		BITMAPINFO *binf = (BITMAPINFO *)&bic;
		binf->bmiHeader.biSize = sizeof(binf->bmiHeader);
		binf->bmiHeader.biWidth = bm.bmWidth;
		binf->bmiHeader.biHeight = bm.bmHeight;
		binf->bmiHeader.biPlanes = 1;
		binf->bmiHeader.biBitCount = 1;
		binf->bmiHeader.biCompression = BI_RGB;
		binf->bmiHeader.biSizeImage = bufsize;

		UCHAR* Bits = new UCHAR[bufsize];
		::GetDIBits(memdc, hbm, 0, bm.bmHeight, Bits, binf, DIB_RGB_COLORS);

		//glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		glRasterPos2i(x, y);
		glBitmap(size.cx, size.cy, 0, 12, 0, 0, Bits);
		delete[] Bits;
		Bits = NULL;

		SelectObject(memdc, oldBmp);
		::DeleteDC(memdc);
		DeleteObject(hbm);

		//glEnable(GL_LIGHTING);     
		//glEnable(GL_TEXTURE_2D);
		glPopAttrib();
		glPopMatrix();
	}

	int CFont_Gl::TextSize()
	{
		return m_nTextSize;
	}
	

	HDC CFont_Gl::GetDC()
	{
		return m_hdc;
	}

	CGLFont::CGLFont()
	{

	}
	CGLFont::~CGLFont()
	{

	}

	void CGLFont::entext(float x, float y, LPCTSTR str, HFONT hFont,
		float r, float g, float b)
	{
		HDC hdc = wglGetCurrentDC();
		SelectObject(hdc, hFont);
		unsigned int Base = glGenLists(96);
		wglUseFontBitmaps(hdc, 32, 96, Base);
		glDisable(GL_TEXTURE_2D);
		glDisable(GL_LIGHTING);
		glPushAttrib(GL_LIST_BIT);
		glColor3f(r, g, b);
		glRasterPos2f(x / 100, y / 100);
		glListBase(Base - 32);
		glCallLists(_tcslen(str), GL_UNSIGNED_BYTE, str);
		glPopAttrib();
		glEnable(GL_LIGHTING);
		glEnable(GL_TEXTURE_2D);
		glDeleteLists(Base, 96);
	}

	//////////////////////////////////////////////////////////////////
	void CGLFont::c3dtext(LPCTSTR str, HFONT hFont, float z)
	{
		glDisable(GL_TEXTURE_2D);
		glDisable(GL_LIGHTING);
		Printfc3d(TEXT("立体汉字"), hFont, z);
		glEnable(GL_LIGHTING);
		glEnable(GL_TEXTURE_2D);
	}
	void CGLFont::Printfc3d(LPCTSTR strText, HFONT hFont, float z)
	{
		HDC hdc = wglGetCurrentDC();
		HFONT hOldFont = (HFONT)::SelectObject(hdc, hFont);
		UCHAR * pChar = (UCHAR*)strText;
		int   nListNum;
		DWORD dwChar;
		GLYPHMETRICSFLOAT pgmf[1];
		glPushMatrix();
		for (int i = 0; i < _tcslen(strText); i++)
		{
			if (IsDBCSLeadByte((BYTE)pChar))
			{
				dwChar = (DWORD)((pChar[i] << 8) | pChar[i + 1]);
				i++;
			}
			else 
				dwChar = pChar[i];

			nListNum = glGenLists(1);
			wglUseFontOutlines(hdc,
				dwChar,
				1,
				nListNum,
				0.0f,
				z,
				WGL_FONT_POLYGONS,
				pgmf
				);
			glCallList(nListNum);
			glDeleteLists(nListNum, 1);
		}
		glPopMatrix();
		::SelectObject(hdc, hOldFont);
	}

	////////////////////////////////////////////////////////////////////////
	void CGLFont::settext(float x, float y, LPCTSTR str, HFONT Font, float r, float g, float b)

	{
		
	}
	void CGLFont::Printftext(int x, int y, LPCTSTR lpszText, HFONT hFont)
	{ //CBitmap bitmap;
		BITMAP bm;
		SIZE size;
		HDC MDC = ::CreateCompatibleDC(0);
		SelectObject(MDC, hFont);
		::GetTextExtentPoint32(MDC, lpszText, _tcslen(lpszText), &size);
		//bitmap.CreateBitmap(size.cx, size.cy, 1, 1, NULL);
		HBITMAP hbm = CreateBitmap(
			size.cx,         // bitmap width, in pixels
			size.cy,        // bitmap height, in pixels
			1,       // number of color planes
			1,   // number of bits to identify color
			NULL // color data array
			);
		HBITMAP oldBmp = (HBITMAP)SelectObject(MDC, hbm/*bitmap*/);
		SetBkColor(MDC, RGB(0, 0, 0));
		SetTextColor(MDC, RGB(255, 255, 255));
		TextOut(MDC, 0, 0, lpszText, _tcslen(lpszText));
		//bitmap.GetBitmap(&bm);
		::GetObject(hbm, sizeof(BITMAP), &bm);

		//把图片大小变为2的幂次方
		size.cx = (bm.bmWidth + 31) & (~31);

		int bufsize = size.cy * size.cx;
		struct {
			BITMAPINFOHEADER bih;
			RGBQUAD col[2];
		}bic;
		BITMAPINFO *binf = (BITMAPINFO *)&bic;
		binf->bmiHeader.biSize = sizeof(binf->bmiHeader);
		binf->bmiHeader.biWidth = bm.bmWidth;
		binf->bmiHeader.biHeight = bm.bmHeight;
		binf->bmiHeader.biPlanes = 1;
		binf->bmiHeader.biBitCount = 1;
		binf->bmiHeader.biCompression = BI_RGB;
		binf->bmiHeader.biSizeImage = bufsize;

		UCHAR* Bits = new UCHAR[bufsize];
		::GetDIBits(MDC, hbm, 0, bm.bmHeight, Bits, binf, DIB_RGB_COLORS);

		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		glRasterPos2i(x, y);
		glBitmap(size.cx, size.cy, 0, 0, 0, 0, Bits);
		delete[] Bits;
		SelectObject(MDC, oldBmp);
		::DeleteDC(MDC);
		DeleteObject(hbm);
	}


	extern "C"
	{
		void  __declspec(dllexport) CreateRenderFactoryGl(KKUI::IRenderFactory** ff)
		{
			KKUI::SRenderFactory_Gl* f = new KKUI::SRenderFactory_Gl();
			*ff=f;
		}
	}

	CFragmentShader_Gl::CFragmentShader_Gl(SRenderFactory_Gl* pRenderFactory)
		: m_pRenderFactory(pRenderFactory)
	{
	
	}

	CFragmentShader_Gl::~CFragmentShader_Gl()
	{

	}

	
	void CFragmentShader_Gl::Release2()
	{

	}

	void CFragmentShader_Gl::ReLoadRes()
	{

	}

	void CFragmentShader_Gl::IniPs(GLuint& program)
	{
		GLint fragCompiled, linked;
		GLint f;
		//Shader: step1
		if (glCreateShader)
			f = glCreateShader(GL_VERTEX_SHADER);
		//f = glCreateShader(GL_FRAGMENT_SHADER);

		//Shader: step2
		glShaderSource(f, 1, &fragment_shader, NULL);
		//glShaderSource(f, 1, &fs,NULL);

		//Shader: step3
		glCompileShader(f);

		//Debug
		glGetShaderiv(f, GL_COMPILE_STATUS, &fragCompiled);
		//glCompileShader(f);
		//glGetShaderiv(f, GL_COMPILE_STATUS, &fragCompiled);

		//Program: Step1
		program = glCreateProgram();
		//Program: Step2
		glAttachShader(program, f);
		//glAttachShader(p,f); 

		//glBindAttribLocation(p, ATTRIB_VERTEX, "vertexIn");
		//glBindAttribLocation(p, ATTRIB_TEXTURE, "textureIn");
		//Program: Step3
		glLinkProgram(program);
		//Debug
		glGetProgramiv(program, GL_LINK_STATUS, &linked);  

		//Program: Step4
		//glUseProgram(p);
		
	}

	CVertexShader_Gl::CVertexShader_Gl(SRenderFactory_Gl* pRenderFactory)
		: m_pRenderFactory(pRenderFactory)
		, m_Program(0)
	{

	}

	CVertexShader_Gl::~CVertexShader_Gl()
	{

	}

	void CVertexShader_Gl::Release2()
	{

	}

	void CVertexShader_Gl::ReLoadRes()
	{

	}

	void CVertexShader_Gl::IniPs(GLuint& program)
	{
		GLint vertCompiled, linked;
		GLint v;
		//Shader: step1
		if (glCreateShader)
			v = glCreateShader(GL_VERTEX_SHADER);
		//f = glCreateShader(GL_FRAGMENT_SHADER);

		//Shader: step2
		glShaderSource(v, 1, &texture_vertex_shader2, NULL);
		//glShaderSource(f, 1, &fs,NULL);

		//Shader: step3
		glCompileShader(v);

		//Debug
		glGetShaderiv(v, GL_COMPILE_STATUS, &vertCompiled);
		//glCompileShader(f);
		//glGetShaderiv(f, GL_COMPILE_STATUS, &fragCompiled);

		//Program: Step1
		program = glCreateProgram();
		//Program: Step2
		glAttachShader(program, v);
		//glAttachShader(p,f); 

		//glBindAttribLocation(p, ATTRIB_VERTEX, "vertexIn");
		//glBindAttribLocation(p, ATTRIB_TEXTURE, "textureIn");
		//Program: Step3
		glLinkProgram(program);
		//Debug
		glGetProgramiv(program, GL_LINK_STATUS, &linked);  

		//Program: Step4
		//glUseProgram(p);
	}


	
}