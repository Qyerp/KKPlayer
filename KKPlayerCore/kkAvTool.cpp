#include <stdio.h>      /* printf */
#include <stdlib.h>
#include <math.h>

#include "kkAvTool.h"
#include "Includeffmpeg.h"
void IniFF();
CKkAvTool::CKkAvTool()
{
   IniFF();
}
CKkAvTool::~CKkAvTool()
{

}


std::string  Dump_format(AVCodecContext  *avctx  ,AVFormatContext *ic, const char *url,int width,int height,int pix_fmt)
{
    int i;
    uint8_t *printed = ic->nb_streams ? (uint8_t *)av_mallocz(ic->nb_streams) : NULL;
    if (ic->nb_streams && !printed)
        return "";

  
    char temp[1024]="";
	std::string jsonstr="{";
    {
			int ret;
			for (int i = 0; i < ic->nb_streams; i++)
			{
					 AVStream *st = ic->streams[i];
					 if(!st)
						 continue;
					 AVCodec* codec = avcodec_find_decoder(st->codecpar->codec_id);
					 if(!codec)
						 continue;

					 if(st->codecpar->codec_type == AVMEDIA_TYPE_VIDEO ){
						 
						 
						 sprintf(temp,"\"VCodec\":\"%s\",",codec->name);
			             jsonstr+=temp;

						
			            

						 switch (avctx->codec_tag) 
						 {
								case MKTAG('H', 'a', 'p', '1'):		
									 sprintf(temp,"\"pix_fmt\":%d,",1001);
									break;
								case MKTAG('H', 'a', 'p', '5'):
									 sprintf(temp,"\"pix_fmt\":%d,",1005);
									break;
                                default:
									 sprintf(temp,"\"pix_fmt\":%d,",pix_fmt);
						 };
                         jsonstr+=temp;
					 }else if( st->codecpar->codec_type ==AVMEDIA_TYPE_AUDIO ){
					     sprintf(temp,"\"ACodec\":\"%s\",",codec->name);
			             jsonstr+=temp;
					 }
			}
           

        if (ic->duration != AV_NOPTS_VALUE) {
            int hours, mins, secs, us;
            int64_t duration = ic->duration + (ic->duration <= INT64_MAX - 5000 ? 5000 : 0);
            secs  = duration / AV_TIME_BASE;
            us    = duration % AV_TIME_BASE;
            mins  = secs / 60;
            secs %= 60;
            hours = mins / 60;
            mins %= 60;
			sprintf(temp,"\"secs\":%d,",secs);
			jsonstr+=temp;

			sprintf(temp,"\"mins\":%d,",mins );
			jsonstr+=temp;
			
			sprintf(temp,"\"hours\":%d,",hours );
			jsonstr+=temp;

			
			
			sprintf(temp,"\"width\":%d,", width);
			jsonstr+=temp;

			sprintf(temp,"\"height\":%d",height );
			jsonstr+=temp;

			
        } else {
            
        }
        if (ic->start_time != AV_NOPTS_VALUE) {
            int secs, us;
            secs = abs((int)(ic->start_time / AV_TIME_BASE));
            us   = abs((int)(ic->start_time % AV_TIME_BASE));
        }
		jsonstr+="}";
    }

    
    av_free(printed);
	return jsonstr;
}

#ifdef WIN32
void SaveBmp(void* bgra,int bgralen,int width,int height)
{
            BITMAPINFOHEADER header;
			header.biSize = sizeof(BITMAPINFOHEADER);
			int bpp=32;
			header.biWidth = width;
			header.biHeight = height*(-1);
			header.biBitCount = bpp;
			header.biCompression = 0;
			header.biSizeImage = 0;
			header.biClrImportant = 0;
			header.biClrUsed = 0;
			header.biXPelsPerMeter = 0;
			header.biYPelsPerMeter = 0;
			header.biPlanes = 1;


			//3 构造文件头
			BITMAPFILEHEADER bmpFileHeader;
			HANDLE hFile = NULL;
			DWORD dwTotalWriten = 0;
			DWORD dwWriten;

			bmpFileHeader.bfType = 0x4d42; //'BM';
			bmpFileHeader.bfOffBits=sizeof(BITMAPFILEHEADER)+sizeof(BITMAPINFOHEADER);
			bmpFileHeader.bfSize = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER)+ width*height*bpp/8;


			int Totl=sizeof(BITMAPFILEHEADER)+sizeof(BITMAPINFOHEADER)+bgralen;
			unsigned char *Imgbufx=(unsigned char *)::malloc(Totl);
			unsigned char *bufx2=Imgbufx;
			memcpy(bufx2,&bmpFileHeader, sizeof(BITMAPFILEHEADER));
			bufx2+=sizeof(BITMAPFILEHEADER);

			memcpy(bufx2,&header, sizeof(BITMAPINFOHEADER));
			bufx2+=sizeof(BITMAPINFOHEADER);
			memcpy(bufx2,bgra, bgralen);
			

			FILE* fp=fopen("D:/xxx.bmp","wb");
		    if(fp){
				::fwrite(Imgbufx,Totl,1,fp);
				fclose(fp);
		    }
			::free(Imgbufx);
}
#endif
//http://cmzx3444.iteye.com/blog/965792
int CKkAvTool:: GetMediaInfo(const std::string& path,char** OutJson,int NeePic,int sec,int& NeedWidth,int& NeedHeight,char** BGRA,int* BGRAlen)
{
	*OutJson =0;
	*BGRA =0;
	*BGRAlen=0;
   AVFormatContext *pFormatCtx    = 0;  
   AVInputFormat   *iformat       = 0;
   AVDictionary    *format_opts   = 0;
   AVFrame         *pFrame        = 0;  
   AVFrame         *pFrameRGB     = 0;  
   AVCodecContext  *pCodecCtx     = 0;
   std::string     strJson ="";
   AVPacket         packet;  
   //图片转化上下文
   struct SwsContext * swsContext = 0;  
   int                 frameFinished=0,err=-1;  
   err =avformat_open_input(&pFormatCtx,path.c_str(),iformat,   &format_opts);
   if(err < 0){
	  goto end;
   }

   // Retrieve stream information  
   err=avformat_find_stream_info(pFormatCtx,0);
   if( err < 0)  {
      goto end;
   }
  
   
 
    
	if(!NeePic)
	{
	     goto end;
	}
	// Find the first video stream  
    int videoStream = -1;  
    for (int i = 0; i < pFormatCtx->nb_streams; i++)  
	{
        if (pFormatCtx->streams[i]->codecpar->codec_type ==AVMEDIA_TYPE_VIDEO)  
        {  
            videoStream = i;  
            break;  
        }  
	}
   
    if( videoStream < 0)  {
		err= videoStream;
        goto end;
   }


	pCodecCtx = avcodec_alloc_context3(NULL);
	

	err = avcodec_parameters_to_context(pCodecCtx, pFormatCtx->streams[videoStream]->codecpar); 
    if( err < 0)  {
      goto end;
    }

    // Find the decoder for the video stream  
    AVCodec  *pCodec = avcodec_find_decoder(pCodecCtx->codec_id);  
    if (pCodec == NULL)  
    {  
		 err=-1;
         goto end;
    }  
    err=avcodec_open2(pCodecCtx, pCodec,NULL);
    if ( err< 0)  
     {  
		 err=-1;
         goto end;
    } 
  

    pFrame = av_frame_alloc();
  
  
    pFrameRGB =av_frame_alloc();
    if (pFrameRGB == NULL)  {  
		 err=-1;
         goto end;
    } 


	    int64_t timestamp;
		
		/* add the stream start time */
		if (pFormatCtx->start_time != AV_NOPTS_VALUE)
		{
			timestamp= AV_TIME_BASE*sec;
			timestamp += pFormatCtx->start_time;
			err = avformat_seek_file(pFormatCtx, -1, INT64_MIN, timestamp, INT64_MAX, 0);
			if (err< 0) {
				
			}
       }

	 if(NeedWidth<= 0 )
		 NeedWidth = pCodecCtx->width;
	 if( NeedHeight <=0)
         NeedHeight =pCodecCtx->height ;


	
	 int	dh = NeedWidth * pCodecCtx->height / pCodecCtx->width;
	 float fff = 0.9;
	 while (dh>NeedHeight)
	 {
		NeedWidth = NeedWidth*fff;
		dh = dh*fff;
		fff -= 0.1;
	 }

	
     NeedHeight = dh ;

	AVPixelFormat  outf=  AV_PIX_FMT_BGRA;
	int numBytes =  av_image_get_buffer_size(outf,  NeedWidth, NeedHeight,1);
  
	uint8_t * buffer = (uint8_t *) av_malloc(numBytes * sizeof(uint8_t));  
  
    
    avpicture_fill((AVPicture *) pFrameRGB, buffer, outf,  NeedWidth,NeedHeight);  
   
	
    swsContext = sws_getContext(pCodecCtx->width, pCodecCtx->height, pCodecCtx->pix_fmt,
		 NeedWidth, NeedHeight, 
		outf, SWS_BICUBLIN, NULL, NULL, NULL);  
  
	 err=0;
    // Read frames and save first five frames to disk  
    while (av_read_frame(pFormatCtx, &packet) >= 0)  
    {  
		int Getpic=0;
        //视频真
        if (packet.stream_index == videoStream)  
        {  
            // Decode video frame  
            avcodec_decode_video2(pCodecCtx, pFrame, &frameFinished, &packet);  
            //Did we get a video frame?  
            if (frameFinished&&pFrame->key_frame)  
            {  
				// pFrame-
                //yuv转 BGRA
                sws_scale(swsContext, pFrame->data, pFrame->linesize, 0, pCodecCtx->height, pFrameRGB->data,   pFrameRGB->linesize);  
				Getpic= 1;

				*BGRA=(char*)buffer;
				*BGRAlen=numBytes;
            }  
        }  
        //Free the packet;
        av_packet_unref(&packet); 
		if( Getpic)
			break;
    }  
    int picw=0,pich=0;
	if(pCodecCtx){
		picw=pCodecCtx->width;
		pich=pCodecCtx->height;
	}
	 //获取文件信息
    strJson= Dump_format(pCodecCtx ,pFormatCtx,  path.c_str(), picw,pich,	pCodecCtx->pix_fmt);
    if(strJson.length()>3){
       char* xxx=    ( char*) av_malloc(strJson.length()+10);   
	   memset(xxx,0,strJson.length()+10);
	   strcpy(xxx,strJson.c_str());
	   *OutJson=xxx;
    }
end:
	if(pFrameRGB)
	{
       //av_free(buffer);  
	   //buffer =0;
       av_frame_unref(pFrameRGB);  
	   pFrameRGB =0;
	}
  
    // Free the YUV frame  
	if(pFrame){
       av_frame_unref(pFrame);  
	}
  

	if(swsContext){
		sws_freeContext(swsContext);
		swsContext=0;
	}

    //关闭解码器上下文
	if(pCodecCtx!=0){
       avcodec_close(pCodecCtx);
	   pCodecCtx =0;
	}
  
    //关闭输入环境
	if(pFormatCtx)
    avformat_close_input(&pFormatCtx);  

	//SaveBmp(buffer,*BGRAlen,NeedWidth,NeedHeight);
    return err;  
}

void KKPlayerFree(void* ptr)
{
	if(ptr){
	   av_free(ptr);  
	}
}