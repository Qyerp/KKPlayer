#include "UISliceEditor.h"
#include <math.h>
namespace KKUI
{
	
	CUISliceEditor::CUISliceEditor():m_pUIYUV(0),m_bVertIndex(-1),m_nAllowEditor(1)
	{
		m_bDraging =EDrag_Type::EDrag_None;
		m_nAllowEditor = 1;
	}
	CUISliceEditor::~CUISliceEditor()
	{
		if(m_pUIYUV){
		    m_pUIYUV->Release();
		}
	}

    CUIWindow* CUISliceEditor::WndFromPoint( kkPoint  pt, int bOnlyText)
	{
		CPoint pt2;
		pt2.x = pt.x;
		pt2.y = pt.y;
		int ret = 0;
		if (PtInVert(pt2, ret))
			return this;
	     return CUIWindow::WndFromPoint( pt, bOnlyText);
	}
	int   CUISliceEditor:: IsContainPoint(const kkPoint &pt,int bClientOnly)
	{
	     int ret= CUIWindow::IsContainPoint(pt,bClientOnly);
		 if(!ret){
		    CPoint pt2;
			pt2.x=pt.x;
			pt2.y=pt.y;
			return PtInVert(pt2,ret);
		 }
		 return ret;
	}
	int     CUISliceEditor::OnNcHitTest    (kkPoint pt)
	{
		int ret = 0;
		CPoint pt2;
		pt2.x = pt.x;
		pt2.y = pt.y;
		if (PtInVert(pt2, ret))
			return 1;
		return CUIWindow::OnNcHitTest(pt);
	}
	void    CUISliceEditor::SetUIYUV (CUIYUV*  pUIYUV)
	{
		if( pUIYUV!=0)
		{
	        m_pUIYUV = pUIYUV;
			m_pUIYUV->AddRef();
			AdjustVertPt(0,0,0,1);
		}
	}
	bool CUISliceEditor::PtInVert(CPoint pt,int& Out)
	{
		 CRect rt;
	      for (int i = 0; i < 4;  i++) 
		  {
			  GetRectByPt(m_PtS[i],rt);

			  if(kkPtInRect(pt,rt)){
				  Out = i;
				  return 1;
			  }
		  }
		  return 0;
	}
	void CUISliceEditor::GetRectByPt(const CPoint& pt,CRect& rt)
	{
	     rt.left = pt.x-4;
		 rt.top = pt.y-4;

		 rt.right = pt.x+4;
		 rt.bottom = pt.y+4;
	}
	void CUISliceEditor::AdjustVertPt(int x,int y,int index,int init)
	{
		if(init==1)
		{
			   
               SUVInfo Pts[4];
			   if(m_pUIYUV)
					m_pUIYUV->GetClip(Pts,4);
				
				CRect rt;
				this->GetWindowRect(&rt);
				{
					m_PtS[0].x = rt.left + Pts[0].x*rt.Width();
					m_PtS[0].y = rt.top  + Pts[0].y*rt.Height();		
				}

				{
					m_PtS[1].x =  rt.left + Pts[1].x *rt.Width();
					m_PtS[1].y = rt.top  +  Pts[1].y*rt.Height();
				}

				{
					m_PtS[2].x =rt.left +  Pts[2].x * rt.Width();
					m_PtS[2].y =rt.top  +  Pts[2].y * rt.Height();
				}

				{
					m_PtS[3].x =  rt.left + Pts[3].x *rt.Width();
					m_PtS[3].y = rt.top  +  Pts[3].y * rt.Height();
				}
				return;
		}
		else if(index==-1){
		
			m_PtS[0].x += x;
			m_PtS[0].y += y;

			m_PtS[1].x += x;
			m_PtS[1].y += y;

			m_PtS[2].x += x;
			m_PtS[2].y += y;

			m_PtS[3].x += x;
			m_PtS[3].y += y;
		} else if(m_pUIYUV&&init != 1){
		
			
			        m_PtS[index].x =x;// rt.left + xSrc*rt.Width();
					m_PtS[index].y = y;//rt.top  + ySrc*rt.Height();	

			  
		}
		
		CRect ClientRt;
		this->GetWindowRect(&ClientRt);

		SUVInfo Pts[4];
		Pts[0].x = (float)(m_PtS[0].x- ClientRt.left)/ClientRt.Width();
		Pts[0].y = (float)(m_PtS[0].y - ClientRt.top) /ClientRt.Height();
		//if(Pts[0].x)
		Pts[1].x = (float)(m_PtS[1].x - ClientRt.left) / ClientRt.Width();
		Pts[1].y = (float)(m_PtS[1].y - ClientRt.top) / ClientRt.Height();

		Pts[2].x = (float)(m_PtS[2].x - ClientRt.left) / ClientRt.Width();
		Pts[2].y = (float)(m_PtS[2].y - ClientRt.top) / ClientRt.Height();

		Pts[3].x = (float)(m_PtS[3].x - ClientRt.left) / ClientRt.Width();
		Pts[3].y = (float)(m_PtS[3].y - ClientRt.top) / ClientRt.Height();
		
		m_pUIYUV->SetClip(Pts,4);

	}
	void CUISliceEditor::OnPaint( IRenderTarget  *pRT){

        COLORREF crBorder=m_style.m_crBorder;  
		if(m_pUIYUV){
			CRect ClientRt;
			GetWindowRect(&ClientRt);

			IPicYuv420p* iYuv=m_pUIYUV->GetIPicYuv420p();
			if(iYuv){
		         SPalette Palette;
				 Palette.Alpha = 255;
				 SUVInfo Pts[4];
				 Pts[0].x = (float)(m_PtS[0].x - ClientRt.left) / ClientRt.Width();
				 Pts[0].y = (float)(m_PtS[0].y - ClientRt.top) / ClientRt.Height();

				 Pts[1].x = (float)(m_PtS[1].x - ClientRt.left) / ClientRt.Width();
				 Pts[1].y = (float)(m_PtS[1].y - ClientRt.top) / ClientRt.Height();

				 Pts[2].x = (float)(m_PtS[2].x - ClientRt.left) / ClientRt.Width();
				 Pts[2].y = (float)(m_PtS[2].y - ClientRt.top) / ClientRt.Height();

				 Pts[3].x = (float)(m_PtS[3].x - ClientRt.left) / ClientRt.Width();
				 Pts[3].y = (float)(m_PtS[3].y - ClientRt.top) / ClientRt.Height();


				   SVertInfo VertInfo[6];
				   VertInfo[0].x    = m_PtS[0].x;
				   VertInfo[0].y    = m_PtS[0].y;
				   VertInfo[0].uv.x = Pts[0].x;
				   VertInfo[0].uv.y = Pts[0].y;

				   VertInfo[1].x = m_PtS[1].x;;
				   VertInfo[1].y = m_PtS[1].y;
				   VertInfo[1].uv.x = Pts[1].x;
				   VertInfo[1].uv.y = Pts[1].y;

				   VertInfo[2].x = m_PtS[2].x;
				   VertInfo[2].y = m_PtS[2].y;
				   VertInfo[2].uv.x  = Pts[2].x;
				   VertInfo[2].uv.y  = Pts[2].y;

				 
				   VertInfo[3].x = m_PtS[2].x;
				   VertInfo[3].y = m_PtS[2].y;
				   VertInfo[3].uv.x = Pts[2].x;
				   VertInfo[3].uv.y = Pts[2].y;

				   VertInfo[4].x    = m_PtS[3].x;
				   VertInfo[4].y    = m_PtS[3].y;
				   VertInfo[4].uv.x = Pts[3].x;
				   VertInfo[4].uv.y = Pts[3].y;

				   VertInfo[5].x    = m_PtS[0].x;
				   VertInfo[5].y    = m_PtS[0].y;
				   VertInfo[5].uv.x = Pts[0].x;
				   VertInfo[5].uv.y  = Pts[0].y;


				 pRT->DrawYuv420p(iYuv,0,Palette,VertInfo,6);
			}
		}

		if(!m_nAllowEditor)
			return ;

		CRect  rc;
		{
			GetRectByPt(m_PtS[0],rc);
			pRT->FillRect(&rc,crBorder);
		}

		{
			
			GetRectByPt(m_PtS[1],rc);
			pRT->FillRect(&rc,crBorder);

			pRT->DrawLine(m_PtS[0].x, m_PtS[0].y, m_PtS[1].x, m_PtS[1].y, crBorder);
		}

		{
			
			GetRectByPt(m_PtS[2],rc);
			pRT->FillRect(&rc,crBorder);
			pRT->DrawLine(m_PtS[1].x, m_PtS[1].y, m_PtS[2].x, m_PtS[2].y, crBorder);
		}

		{
			GetRectByPt(m_PtS[3],rc);
			pRT->FillRect(&rc,crBorder);
			pRT->DrawLine( m_PtS[2].x, m_PtS[2].y, m_PtS[3].x, m_PtS[3].y, crBorder);
		}
		pRT->DrawLine(m_PtS[3].x, m_PtS[3].y, m_PtS[0].x, m_PtS[0].y, crBorder);
		
	}
    
	void CUISliceEditor::OnEraseBkgnd( IRenderTarget  *pRT)
	{
	    
	}
	int  CUISliceEditor::OnRelayout(const CRect &rtwnd)
	{
		
		int ret= CUIWindow::OnRelayout(rtwnd);

		AdjustVertPt(0,0,0,1);
		return ret;
	}

	

	
	
	void CUISliceEditor::OnMouseMove(unsigned int nFlags,CPoint pt)
	{
		if(!m_nAllowEditor)
			return ;

		if(m_bDraging==EDrag_Type::EDrag_Vect&&m_bVertIndex>-1){
			
			AdjustVertPt(pt.x,pt.y,m_bVertIndex);
			Invalidate();
		}else if(m_bDraging==EDrag_Type::EDrag_Client){
		    int cx = pt.x-m_LastPt.x;
			int cy = pt.y-m_LastPt.y;
			if (cx < 0)
			{
				//this->m_PtS[0].x+cx<
			}
			m_LastPt = pt;
		    AdjustVertPt(cx,cy,-1);
			Invalidate();
		}
	}


	void CUISliceEditor::OnMouseLeave(unsigned int nFlags)
	{
		 m_bDraging =EDrag_Type::EDrag_None;
		 m_bVertIndex = -1;
	}
	
	void CUISliceEditor::OnLButtonDown(unsigned int nFlags,CPoint pt)
	{
		if(!m_nAllowEditor)
			return ;
	   m_LastPt = pt;
	   if(PtInVert(pt,m_bVertIndex)){
		    m_bDraging =EDrag_Type::EDrag_Vect;
	   }else{
	        m_bDraging =EDrag_Type::EDrag_Client;
			m_bVertIndex = -1;
	   }
	}
	void CUISliceEditor::OnLButtonUp(unsigned int nFlags,CPoint pt)
	{
	 m_bDraging =EDrag_Type::EDrag_None;
	 m_bVertIndex = -1;
	}
	void CUISliceEditor::OnRButtonUp(unsigned int nFlags,CPoint pt)
	{
       m_bDraging =EDrag_Type::EDrag_None;
	}
	void CUISliceEditor::OnNcLButtonDown(unsigned int nFlags,CPoint pt)
	{
	    OnLButtonDown(nFlags,pt);
	}
	void CUISliceEditor::OnNcLButtonUp(unsigned int nFlags,CPoint pt)
	{
	    OnLButtonUp(nFlags,pt);
	}
}