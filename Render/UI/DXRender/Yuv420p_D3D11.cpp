#include "RenderTarget_D3D11.h"
#include "Yuv420p_D3D11.h"

#include "FastMemcpy.h"

namespace KKUI
{
	CYuv420p_D3D11::CYuv420p_D3D11(SRenderFactory_D3D11* pRenderFactory):
		m_pRenderFactory(pRenderFactory),
		m_pRenderTexture(0),
		m_pClipTexture(0),
		m_pQuoteObj(0),
		m_pYTexture(0),
	    m_pUTexture(0),
		m_pVTexture(0),
		m_pARGBTexture(0),
		m_nPreOk(1),
		m_pRTView(0),
		m_pSRView(0),
		m_pYSRView(0),
		m_pUSRView(0),
		m_pVSRView(0),
		m_pDSV(0),
		m_pVertexBuffer(0)
	{
		m_pRenderFactory->AddRef();
		m_Size.cx = 0;
		m_Size.cy = 0;

		ID3D11Device* pDevice = m_pRenderFactory->GetDevice();

		D3D11_BUFFER_DESC constantBufferDesc;
		memset(&constantBufferDesc, 0, sizeof(constantBufferDesc));
		constantBufferDesc.ByteWidth = sizeof(VertexShaderConstants);
		constantBufferDesc.Usage = D3D11_USAGE_DEFAULT;
		constantBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		pDevice->CreateBuffer(
			&constantBufferDesc,
			NULL,
			&m_pVsShaderConstants
		);

		D3D11_SAMPLER_DESC sampDesc;
		ZeroMemory(&sampDesc, sizeof(sampDesc));
		sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
		sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
		sampDesc.MinLOD = 0;
		sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
		pDevice->CreateSamplerState(&sampDesc, & m_pSamplerState);

	}
	
	CYuv420p_D3D11::~CYuv420p_D3D11()
	{
		 if(m_pQuoteObj)
			m_pQuoteObj->Release();

	    Release2();
		m_pRenderFactory->RemoveObj(this);
		m_pRenderFactory->Release();

		
	}
    void CYuv420p_D3D11:: Release2()
	{
		  m_nPreOk = 0;
		  int lx=0;
		  if(m_pYTexture)
			  lx=m_pYTexture->Release();
		  assert(lx == 0);
		  m_pYTexture = 0;
	      if(m_pUTexture)
			 lx= m_pUTexture->Release();
		  assert(lx == 0);
		  m_pUTexture = 0;
		  if(m_pVTexture)
             lx= m_pVTexture->Release();
		  assert(lx == 0);
		  m_pVTexture = 0;

		  if(m_pARGBTexture)
			  m_pARGBTexture->Release();
		  m_pARGBTexture = 0;

		  if(m_pRenderTexture)
			  lx= m_pRenderTexture->Release();
		  assert(lx == 0);
		  m_pRenderTexture = 0;

			if(m_pClipTexture)
				lx=m_pClipTexture->Release();
			assert(lx == 0);
			m_pClipTexture=0;
	}
	void  CYuv420p_D3D11::ReLoadRes()
	{
		  m_nPreOk = 1;
	}
	//http://adec.altervista.org/blog/yuv-422-v210-10-bit-packed-decoder-in-glsl/
	
	void   CYuv420p_D3D11::RenderToTex()
	{
		
		if(this->m_pRTView)
		{
			ID3D11Device* pDevice = m_pRenderFactory->GetDevice();
			ID3D11DeviceContext*  DevCtx = m_pRenderFactory->GetContext();
			//DevCtx->ClearState();
			
		
			float ClearColor[4] = { 0.0f,0.0f, 0.0f, 1.0f }; // red,green,blue,alpha
			DevCtx->ClearRenderTargetView(m_pRTView, ClearColor);
			//清除深度
			DevCtx->ClearDepthStencilView(m_pDSV, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
			DevCtx->OMSetRenderTargets(1, &m_pRTView, 0);
			DevCtx->OMSetBlendState(0, 0, 0xFFFFFFFF);
			//return;
			D3D11_VIEWPORT viewport = {};
			Float4X4 projection = MatrixIdentity();
			Float4X4       view = {};
			view.m[0][0] = 2.0f / m_Size.cx;
			view.m[0][1] = 0.0f;
			view.m[0][2] = 0.0f;
			view.m[0][3] = 0.0f;
			view.m[1][0] = 0.0f;
			view.m[1][1] = -2.0f / m_Size.cy;
			view.m[1][2] = 0.0f;
			view.m[1][3] = 0.0f;
			view.m[2][0] = 0.0f;
			view.m[2][1] = 0.0f;
			view.m[2][2] = 1.0f;
			view.m[2][3] = 0.0f;
			view.m[3][0] = -1.0f;
			view.m[3][1] = 1.0f;
			view.m[3][2] = 0.0f;
			view.m[3][3] = 1.0f;

			viewport.TopLeftX = 0;
			viewport.TopLeftY = 0;
			viewport.Width = m_Size.cx;
			viewport.Height = m_Size.cy;
			viewport.MinDepth = 0.0f;
			viewport.MaxDepth = 1.0f;
			DevCtx->RSSetViewports(1, &viewport);
			VertexShaderConstants m_VsShaderConstantsData = { 0,0 };
			m_VsShaderConstantsData.model = MatrixIdentity();
			m_VsShaderConstantsData.projectionAndView = MatrixMultiply(view, projection);

			//DevCtx->RSSetViewports(1, &viewport);
			DevCtx->UpdateSubresource(
				(ID3D11Resource *)m_pVsShaderConstants,
				0,
				NULL,
				&m_VsShaderConstantsData,
				0,
				0
			);

			

		    //默认顶点色器
		    DevCtx->VSSetShader(m_pRenderFactory->GetDefVertexShader() ,NULL,0);
		    DevCtx->VSSetConstantBuffers( 0, 1, &m_pVsShaderConstants);
			float ff = 1.0;
			VertexPositionColor vertices[4] = {

				{ { 0.0,        0.0,       0.0f },      { 0.0f, 0.0f },     { ff, ff, ff, ff } },
				{ { 0.0,        m_Size.cy, 0.0f },      { 0.0f, 1.0f },     { ff, ff, ff, ff } },
				{ { m_Size.cx,  0,         0.0f },      { 1.0f, 0.0f },     { ff, ff, ff, ff } },
			    { { m_Size.cx,  m_Size.cy, 0.0f },      { 1.0f, 1.0f },     { ff, ff, ff, ff } }
			};
			UpdateVertexBuffer(vertices, sizeof(VertexPositionColor) * 4);

			ID3D11PixelShader*   yuvps = m_pRenderFactory->GetDefYuvPixelShader()->GetPs();
			//设置像素着色器
			DevCtx->PSSetShader(yuvps, NULL, 0);
			ID3D11ShaderResourceView* ShaderResource[3] = { m_pYSRView,m_pUSRView,m_pVSRView };
			DevCtx->PSSetShaderResources(0, 3, ShaderResource);
			
			//设置着色器采样
			DevCtx->PSSetSamplers(0,1, & m_pSamplerState);
			DevCtx->IASetInputLayout(m_pRenderFactory->GetDefInputLayout());

			DevCtx->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
			DevCtx->Draw(4, 0);
			//DevCtx->PSSetShaderResources(0, 0, 0);

			//参考https://www.cnblogs.com/X-Jun/p/9028764.html
	   }
	}

	int    CYuv420p_D3D11::UpdateVertexBuffer(const void * vertexData, size_t dataSizeInBytes)
	{

		D3D11_BUFFER_DESC vertexBufferDesc = {};
		HRESULT result = S_OK;
		D3D11_SUBRESOURCE_DATA vertexBufferData;


		if (m_pVertexBuffer) {
			m_pVertexBuffer->GetDesc(&vertexBufferDesc);
			//ID3D11Buffer_GetDesc(m_pVertexBuffer, &vertexBufferDesc);
		}

		ID3D11DeviceContext* devCtx = m_pRenderFactory->GetContext();
		ID3D11Device*        pDevice = m_pRenderFactory->GetDevice();
	
		ID3D11VertexShader*  vs = m_pRenderFactory->GetDefVertexShader();
		if (m_pVertexBuffer && vertexBufferDesc.ByteWidth >= dataSizeInBytes) {
			D3D11_MAPPED_SUBRESOURCE mappedResource;

			result = devCtx->Map(
				(ID3D11Resource *)m_pVertexBuffer,
				0,
				D3D11_MAP_WRITE_DISCARD,
				0,
				&mappedResource
			);
			if (FAILED(result)) {

				return 0;
			}
			memcpy(mappedResource.pData, vertexData, dataSizeInBytes);
			devCtx->Unmap((ID3D11Resource *)m_pVertexBuffer, 0);
		}
		else {

			//m_pVertexBuffer

			vertexBufferDesc.ByteWidth = (UINT)dataSizeInBytes;
			vertexBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
			vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
			vertexBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

			memset(&vertexBufferData, 0, sizeof(vertexBufferData));
			vertexBufferData.pSysMem = vertexData;
			vertexBufferData.SysMemPitch = 0;
			vertexBufferData.SysMemSlicePitch = 0;

			result = pDevice->CreateBuffer(
				&vertexBufferDesc,
				&vertexBufferData,
				&m_pVertexBuffer
			);
			if (FAILED(result)) {
				//WIN_SetErrorFromHRESULT(SDL_COMPOSE_ERROR("ID3D11Device1::CreateBuffer [vertex buffer]"), result);
				return -1;
			}

		


			
		}

		unsigned int nStride = sizeof(VertexPositionColor);
		unsigned int nOffset = 0;
		devCtx->IASetVertexBuffers(
			0,
			1,
			&m_pVertexBuffer,
			&nStride,
			&nOffset
		);
      
		return 0;
	}

	int    CYuv420p_D3D11::Load(const kkPicInfo* Picinfo)
	{

		
		if(m_Size.cx != Picinfo->width || m_Size.cy != Picinfo->height)
		{
	          if(m_pYTexture){
				  m_pYTexture->Release();
				  m_pYTexture = 0;
			  }

			  if(m_pUTexture){
				  m_pUTexture->Release();
				  m_pUTexture = 0;
			  }
			  if(m_pVTexture){
				  m_pVTexture->Release();
				  m_pVTexture = 0;
			  }

              if (m_pRenderTexture) {
				  m_pRenderTexture->Release();
				  m_pRenderTexture = 0;
			  }
			  if(m_pRTView){
				  m_pRTView->Release();
				  m_pRTView = 0;
			  }
			  
			  if (m_pSRView) {
				  m_pSRView->Release();
				  m_pSRView = 0;
			  }

		 }

		if(Picinfo->picformat == 0||  Picinfo->picformat == 2 || Picinfo->picformat == 74)
            return  LoadYuv(Picinfo);
		else if(Picinfo->picformat == 1)
             return   LoadARGB(Picinfo);
		else if(Picinfo->picformat ==3)
			return LoadDxva2(Picinfo);
		else if(Picinfo->picformat ==4)
			return LoadDxTexture(Picinfo);
		else if(Picinfo->picformat == 28)
             return   LoadRGBA(Picinfo);
		return 0;
	}
	
	int   CYuv420p_D3D11::LoadDxTexture(const kkPicInfo* Picinfo)
	{
	      
		    return 1;
	}
	int   CYuv420p_D3D11::LoadDxva2(const kkPicInfo* Picinfo)
	{
		
		  
		    return 1;
	}

	int	  CYuv420p_D3D11::LoadData(const kkPicInfo* Picinfo)
	{
	//	return 0;
		     HRESULT hr =0;
		     ID3D11Device* pDevice= m_pRenderFactory-> GetDevice();
		     if (m_pYTexture==0 || m_pUTexture==0 ||  m_pVTexture==0 )
			 {

				 D3D11_TEXTURE2D_DESC textureDesc={0,0};
				 textureDesc.Width  = Picinfo->width;
				 textureDesc.Height = Picinfo->height;
				 textureDesc.MipLevels = 1;
				 textureDesc.ArraySize = 1;
				 textureDesc.Format = DXGI_FORMAT_R8_UNORM;
				 textureDesc.SampleDesc.Count = 1;
				 textureDesc.SampleDesc.Quality = 0;
		  		 textureDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
				 textureDesc.Usage = D3D11_USAGE_DYNAMIC;
				 textureDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
					 //D3D11_USAGE_DYNAMIC;// D3D11_USAGE_STAGING;
            
					  if(Picinfo->picformat==0){
						     hr =pDevice->CreateTexture2D(&textureDesc,0,&m_pYTexture);	
                            
							 textureDesc.Width  = (Picinfo->width+1)/2;
				             textureDesc.Height = (Picinfo->height+1) /2;
                             hr =pDevice->CreateTexture2D(&textureDesc,0,&m_pUTexture);	
                             hr = pDevice->CreateTexture2D(&textureDesc,0,&m_pVTexture);	



							
					  }else if(Picinfo->picformat==2 || Picinfo->picformat==1){
							 hr =pDevice->CreateTexture2D(&textureDesc,0,&m_pYTexture);	
							textureDesc.Width  = (Picinfo->width+1)/2;
				            textureDesc.Height = Picinfo->height;
                            hr = pDevice->CreateTexture2D(&textureDesc,0,&m_pUTexture);	
                            hr = pDevice->CreateTexture2D(&textureDesc,0,&m_pVTexture);	

					  }else if(Picinfo->picformat==74) {
						     textureDesc.Format = DXGI_FORMAT_R16_UNORM;
							 hr =pDevice->CreateTexture2D(&textureDesc,0,&m_pYTexture);	
							textureDesc.Width  = (Picinfo->width+1)/2;
				            textureDesc.Height = Picinfo->height;
                           hr =  pDevice->CreateTexture2D(&textureDesc,0,&m_pUTexture);	
                           hr =  pDevice->CreateTexture2D(&textureDesc,0,&m_pVTexture);	
					  }else {
					     assert(0);
					  }

					   D3D11_SHADER_RESOURCE_VIEW_DESC shaderResourceViewDesc = {};
				       shaderResourceViewDesc.Format = textureDesc.Format;
				       shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
				       shaderResourceViewDesc.Texture2D.MostDetailedMip = 0;
					   shaderResourceViewDesc.Texture2D.MipLevels = textureDesc.MipLevels;

					 

				       hr = pDevice->CreateShaderResourceView(m_pYTexture, &shaderResourceViewDesc, &m_pYSRView);
					   hr = pDevice->CreateShaderResourceView(m_pUTexture, &shaderResourceViewDesc, &m_pUSRView);
					   hr = pDevice->CreateShaderResourceView(m_pVTexture, &shaderResourceViewDesc, &m_pVSRView);

			 }

			  m_Size.cx = Picinfo->width;
			  m_Size.cy = Picinfo->height;
			  int pixel_h =  m_Size.cy ;
			  int pixel_u =  pixel_h;
			  int pixel_v =  pixel_h;

			  if(Picinfo->picformat==0)
			  {
				   pixel_u =  pixel_h/2;
			       pixel_v =  pixel_h/2;
			  }

              byte *pY=Picinfo->data[0];
			  byte *pU=Picinfo->data[1];
			  byte *pV=Picinfo->data[2];



			 D3D11_TEXTURE2D_DESC TextureDesc={0,0};
			 D3D11_MAPPED_SUBRESOURCE TextureMemory={0,0};
			 m_pYTexture->GetDesc(&TextureDesc);
			 ID3D11DeviceContext* pDevCtx =m_pRenderFactory->GetContext();
			 hr = pDevCtx->Map((ID3D11Resource *)m_pYTexture,0, D3D11_MAP_WRITE_DISCARD,0,&TextureMemory);

			 //Y
			 byte *pDest = (byte *) TextureMemory.pData;
			 int stride = TextureMemory.RowPitch;
					int stride2 = Picinfo->linesize[0];
					if(stride< stride2)
					{
					  stride2 = stride;
					}

					for( int i=0;i < pixel_h;i ++){  
						  memcpy_fast(pDest + i * stride,pY + i * Picinfo->linesize[0],stride2);  
					} 

			 pDevCtx->Unmap(m_pYTexture,0);


			 m_pUTexture->GetDesc(&TextureDesc);
			 hr = pDevCtx->Map(m_pUTexture,0, D3D11_MAP_WRITE_DISCARD,0,&TextureMemory);
	         pDest = (byte *)TextureMemory.pData;
			 stride = TextureMemory.RowPitch;
			 stride2 = Picinfo->linesize[1];
			 if(stride< stride2)
			 {
			   stride2 = stride;
			 }
			 for(int i=0;i <  pixel_u;i ++){  
				 memcpy_fast(pDest+ i * stride,pU + i * Picinfo->linesize[1], stride2);  
			 }  
			 pDevCtx->Unmap(m_pUTexture,0);

			 m_pVTexture->GetDesc(&TextureDesc);
			 hr = pDevCtx->Map(m_pVTexture,0, D3D11_MAP_WRITE_DISCARD,0,&TextureMemory);
             pDest  = (byte *)TextureMemory.pData;
			 stride = TextureMemory.RowPitch;
			 stride2 = Picinfo->linesize[2];
			 if(stride<stride2){
				stride2 = stride;
			 }

			 for(int i=0;i < pixel_v;i ++){ 
				 memcpy_fast(pDest +  i * stride,pV + i * Picinfo->linesize[2], stride2);  
			 }

			 pDevCtx->Unmap(m_pVTexture,0);


			return 1;
	}
	int   CYuv420p_D3D11::LoadYuv(const kkPicInfo* Picinfo)
	{
		     m_pRenderFactory->Lock();
	         HRESULT hr =0;
		     ID3D11Device* pDevice = m_pRenderFactory-> GetDevice();
			 ID3D11DeviceContext*  DevCtx= m_pRenderFactory->GetContext();
			

		     LoadData(Picinfo);
			 if(!m_pRenderTexture)
			 {
				 D3D11_TEXTURE2D_DESC textureDesc = {0,0};
				 textureDesc.Width  = Picinfo->width;
				 textureDesc.Height = Picinfo->height;
				 textureDesc.MipLevels = 1;
				 textureDesc.ArraySize = 1;
				 textureDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
				 textureDesc.SampleDesc.Count = 1;
				 textureDesc.SampleDesc.Quality = 0;
		  		 textureDesc.MiscFlags = 0;
				 textureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
                 textureDesc.MiscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS;
			     hr =  pDevice->CreateTexture2D(&textureDesc,0,&m_pRenderTexture);
				 if (FAILED(hr))
				 {
					 assert(0);
				 }

				 //填充渲染目标视图形容体,并进行创建目标渲染视图
				 D3D11_RENDER_TARGET_VIEW_DESC renderTargetViewDesc;
				 renderTargetViewDesc.Format = textureDesc.Format;
				 renderTargetViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
				 renderTargetViewDesc.Texture2D.MipSlice = 0;
				 hr = pDevice->CreateRenderTargetView(m_pRenderTexture, &renderTargetViewDesc, &m_pRTView);

				 //填充着色器资源视图形容体,并进行创建着色器资源视图
				 D3D11_SHADER_RESOURCE_VIEW_DESC shaderResourceViewDesc;
				 shaderResourceViewDesc.Format = textureDesc.Format;
				 shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
				 shaderResourceViewDesc.Texture2D.MostDetailedMip = 0;
				 shaderResourceViewDesc.Texture2D.MipLevels = textureDesc.MipLevels;
				 hr = pDevice->CreateShaderResourceView(m_pRenderTexture, &shaderResourceViewDesc, &m_pSRView);

				

                //https://github.com/MKXJun/DirectX11-With-Windows-SDK
				//4. 创建与纹理等宽高的深度/模板缓冲区和对应的视图
				textureDesc.MipLevels = 0;
				textureDesc.ArraySize = 1;
				textureDesc.SampleDesc.Count = 1;
				textureDesc.SampleDesc.Quality = 0;
				textureDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
				textureDesc.Usage = D3D11_USAGE_DEFAULT;
				textureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
				textureDesc.CPUAccessFlags = 0;
				textureDesc.MiscFlags = 0;

	            ID3D11Texture2D* depthTex;
	            hr = pDevice->CreateTexture2D(&textureDesc, 0, &depthTex);

				D3D11_DEPTH_STENCIL_VIEW_DESC dsvDesc;
				dsvDesc.Format = textureDesc.Format;
				dsvDesc.Flags = 0;
				dsvDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
				dsvDesc.Texture2D.MipSlice = 0;
	            hr = pDevice->CreateDepthStencilView(depthTex,&dsvDesc,&m_pDSV);
		     } 
	    m_pRenderFactory->UnLock();
	    return 1;
	}
	int   CYuv420p_D3D11::LoadARGB   (const kkPicInfo* Picinfo)
	{ 
		   
		   return 1;
	}



	int   CYuv420p_D3D11::LoadRGBA   (const kkPicInfo* Picinfo)
	{ 
		  HRESULT hr =0;
	      
		  
		  ID3D11Device* pDevice= m_pRenderFactory-> GetDevice();
         


         if(!m_pARGBTexture)
		 {
			 D3D11_TEXTURE2D_DESC textureDesc;

			 textureDesc.Width  = Picinfo->width;
			 textureDesc.Height = Picinfo->height;
			 textureDesc.MipLevels = 1;
			 textureDesc.ArraySize = 1;
			 textureDesc.Format = DXGI_FORMAT_R8G8B8A8_UINT;
			 textureDesc.SampleDesc.Count = 1;
			 textureDesc.SampleDesc.Quality = 0;
		  	 textureDesc.MiscFlags = 0;

			 hr =  pDevice->CreateTexture2D(&textureDesc,0,&m_pARGBTexture);
					
				if (FAILED(hr))
				{
					 assert(0);
				}
		   }
		   
		   return 1;
	}

	ID3D11Texture2D*   CYuv420p_D3D11::LoadToClipTexture(SUVInfo xy1,SUVInfo xy2,SUVInfo xy3,SUVInfo xy4,int clipwidth,int clipheight)
	{
		 
		    return m_pClipTexture;
	}

    
	kkSize  CYuv420p_D3D11::Size() const
	{
		if(m_pQuoteObj)
			return m_pQuoteObj->Size();

		return  m_Size;
	}
	ID3D11Texture2D* CYuv420p_D3D11::GetTexture()
	{   
		if(m_pQuoteObj)
			return m_pQuoteObj->GetTexture();

		return m_pRenderTexture; 
	}
	void          CYuv420p_D3D11::ClearPicData(unsigned int cr)
	{ 

        
	}
	void          CYuv420p_D3D11::SetQuoteObj(IPicYuv420p*     pParentObj)
	{
	    if(m_pQuoteObj)
			m_pQuoteObj->Release();

		pParentObj->AddRef();
		m_pQuoteObj=(CYuv420p_D3D11*)pParentObj;
	}
}