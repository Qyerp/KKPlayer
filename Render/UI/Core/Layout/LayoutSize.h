﻿#ifndef KKUI_LayoutSize_H_
#define KKUI_LayoutSize_H_
#include "TString.h"
namespace KKUI
{

	class CLayoutSize
	{
	public:
		enum Unit{
			px=0,dp,dip,sp
		};

		CLayoutSize();

		CLayoutSize(float fSize,Unit unit);
		float fSize;
		Unit  unit;

		void setWrapContent();
		bool isWrapContent() const;

		void setMatchParent();
		bool isMatchParent() const;

		void setSize(float fSize, Unit unit);
		bool isSpecifiedSize() const;
		
		void setInvalid();
		bool isValid() const;

		int  toPixelSize(int scale) const;

		SStringA toString() const;
		
		bool isZero() const ;

		void parseString(const SStringA & strSize);

		CLayoutSize & operator = (const CLayoutSize & src);
		
		bool valueEqual(float value);

		static CLayoutSize fromString(const SStringA & strSize);
		static bool fequal(float a, float b);
	};
}
#endif