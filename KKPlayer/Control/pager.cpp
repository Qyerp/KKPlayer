#include "StdAfx.h"
#include "pager.h"


CPager::CPager(DuiLib::CPaintManagerUI* ppm,CBookLibrary* pBookLibrary, int count, int pageSize)
{
	m_ppm = ppm;
	m_pBookLibrary = pBookLibrary;

	Init( count, pageSize);
}

void CPager::PreBtn()
{
	if( (m_pageIndex - 1) >= 1 )
	{
		Init( m_count, m_pageSize, --m_pageIndex );
		m_pBookLibrary->Search(m_pageIndex);

		//if(Clicked != NULL)
		Clicked;
	}
}

void CPager::NextBtn()
{
	if( (m_pageIndex + 1) <= m_pageCount  )
	{
		Init(m_count,m_pageSize, ++m_pageIndex);
		m_pBookLibrary->Search(m_pageIndex);
	}
	
}

void CPager::Init( int count, int pageSize, int pageIndex)
{
	m_count = count;
	m_pageSize = pageSize;
	m_pageIndex = pageIndex;

	this->RemoveAll();
	m_pBtnList.clear();

	DuiLib::CDialogBuilder builder;
	DuiLib::CContainerUI* pList = static_cast<CContainerUI*>(builder.Create(_T("pager.xml"), 0, NULL, m_ppm));
	if( pList != NULL ) {

		this->Add(pList);

		m_pageCount = count / pageSize + ((count % pageSize == 0) ? 0 : 1) ;

		int posX = 0;
		int num_ellips_btn = 0;

		for(int i=1; i<= m_pageCount; i++)
		{
			if( i == 1 || i == m_pageCount)
			{
				AddButton(i, posX, m_pageIndex);
				posX++;
			}
			else
			{
				if(abs(m_pageIndex - i) < 3)
				{
					AddButton(i, posX, m_pageIndex);
					posX++;
				}
				else
				{
					if(i < m_pageIndex)
					{
						if(num_ellips_btn < 1)
						{
							AddEllipsisButton(posX);
							num_ellips_btn++;
							posX++;
						}
					}
					else
					{
						if((m_pageIndex <= 4 && num_ellips_btn < 1) ||
							(m_pageIndex > 4 && num_ellips_btn < 2))
						{
							AddEllipsisButton(posX);
							num_ellips_btn++;
							posX++;
						}
					}
				}
			}

		}


	}



}


CPager::~CPager(void)
{
	SAFE_RELEASE(m_pBookLibrary);
	m_pBtnList.clear();

}

bool CPager::OnTheEvent (void* pParam)
{
	DuiLib::TEventUI* pEvent = (DuiLib::TEventUI*)pParam;
	DuiLib::CContainerUI* pContainer = NULL;

	if (pEvent->pSender == NULL) {
		return true;
	}

	if (_tcsicmp (pEvent->pSender->GetClass(), _T ("ContainerUI")) != 0) {
		pContainer = static_cast <DuiLib::CContainerUI*> (pEvent->pSender->GetParent());
	}
	else {
		pContainer = static_cast <DuiLib::CContainerUI*> (pEvent->pSender);
	}

	if (pContainer == NULL) {
		return true;
	}

	if (pEvent->Type == DuiLib::UIEVENT_BUTTONDOWN) {
		OnClick(pEvent, pContainer);
	}

	return true;
}


void CPager::OnClick( DuiLib::TEventUI* pEvent, DuiLib::CContainerUI* pContainer )
{
	if(_tcsicmp (pEvent->pSender->GetClass(), _T("ButtonUI")) == 0)
	{
		DuiLib::CButtonUI* btn =  static_cast<DuiLib::CButtonUI*>(pEvent->pSender);
		m_pageIndex = _wtoi(btn->GetText());
		Init(m_count,m_pageSize,m_pageIndex);
		m_pBookLibrary->Search(m_pageIndex);
	}
}

void CPager::AddEllipsisButton( int posX )
{
	DuiLib::CButtonUI* btn = new DuiLib::CButtonUI();
	CString iCount;
	iCount.Format(_T("...") );
	btn->SetText(iCount);
	btn->SetFloat(true);
	CString str;
	str.Format( _T("%d,20,0,0"), (50+(posX++)* 35) );
	btn->SetAttribute(_T("pos"), str);
	btn->SetBkImage(_T("page-link.png"))  ;
	btn->SetPushedImage(_T("page-link.png"));
	btn->SetAttribute(_T("width"),_T("21"));
	btn->SetAttribute(_T("height"),_T("21"));
	this->Add(btn);
	m_pBtnList.push_back(btn);	
}

void CPager::AddButton( int i, int posX, int m_pageIndex )
{
	DuiLib::CButtonUI* btn = new DuiLib::CButtonUI();
	CString iCount;
	iCount.Format(_T("%d"), i );
	btn->SetText(iCount);
	btn->SetFloat(true);
	CString str;
	str.Format( _T("%d,20,0,0"), (50+(posX++)* 35) );
	btn->SetAttribute(_T("pos"), str);
	i == m_pageIndex ? btn->SetBkImage(_T("page-hover.png")) : btn->SetBkImage(_T("page-link.png"))  ;
	btn->SetPushedImage(_T("page-hover.png"));
	btn->SetAttribute(_T("width"),_T("21"));
	btn->SetAttribute(_T("height"),_T("21"));
	btn->OnEvent += MakeDelegate (this, &CPager::OnTheEvent);
	this->Add(btn);
	m_pBtnList.push_back(btn);	
}