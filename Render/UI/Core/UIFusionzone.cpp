#include "UIFusionzone.h"
#include "UIMsView.h"
#include "SkinPool.h"
#include "Math/BezierSpline.h"

namespace KKUI
{
	void GetScalRect(kkRect& rt,const float& f);
	void GetScalRect(CRect& rt,const float& f);
	
	CUIFusionzone::CUIFusionzone(void):
	  m_fDisplayScale(1.0),
	  m_nFusionType(0),
	  m_nChannelColor(0x00000000),
	  m_nBalance(1)
	{
		 m_fUv1.x  = 0.0;
		 m_fUv1.y  = 0.0;

		 m_fUv2.x = 0.0;
		 m_fUv2.y = 0.0;
	}
	CUIFusionzone::~CUIFusionzone(void)
	{
	         
	}
	
	int   CUIFusionzone::GetCtlAttr(const SStringA& attrName,SStringA& OutAttr)
	{
	     if(CUIMoveWindow::GetCtlAttr(attrName,OutAttr))
			 return 1;
		 else  if(attrName.Compare("windowrect")==0){

		 }
		 return 0;

	}
	int   CUIFusionzone::OnCreate( int)
    {    
	    
	     CUIWindow* pa =this->GetParent();
		 if(pa->IsClass("uimsview"))
		 {
			 CUIMsView* view = (CUIMsView*)pa;
			 float f=  view->GetDisplayScale();
			 m_fDisplayScale =f;
		 }
         return 0;
    }
    
	void   CUIFusionzone::OnMouseLeave(unsigned int nFlags)
	{
	        if(m_bDraging ==EDrag_NcClient)
			   return;  
		  
			 m_bDraging =0;
			 m_PtSize = sUnkown;
	}
    void  CUIFusionzone::OnUpdateFloatPosition(const  CSize&  szView,const CRect & rcParent)
	{
		 CUIMoveWindow::OnUpdateFloatPosition(szView,rcParent);
	}
	int   CUIFusionzone::OnNcHitTest(kkPoint pt)
	{

        
		if( m_bDraging ==EDrag_NcClient)
			return 1;

		 if(m_bDraging == EDrag_Client)
			 return 0;
        int ret= 0;
		{
		   CRect rcClient;
		   GetClientRect(&rcClient);
		   rcClient.left +=4;
		   rcClient.top += 4;
		   rcClient.right  -= 4;
		   rcClient.bottom -= 4;
		   ret=!rcClient.PtInRect(pt);
		}
		return ret;
	}
	int     CUIFusionzone::InitRegion(SUVInfo* infos,int count)
	{
	      if(count<4)
			  return 0;

		  m_XY1 = *infos;
		  m_XY2 = *(infos+1);
		  m_XY3 = *(infos+2);
		  m_XY4 = *(infos+3);
		  return 0;
	}
    void    CUIFusionzone::GetScale(SUVInfo& fUv1,SUVInfo& fUv2)
	{
	            fUv1  = m_fUv1;
			    fUv2  = m_fUv2;
	}
	void    CUIFusionzone::SetScale(SUVInfo& fUv1,SUVInfo& fUv2)
	{
	            m_fUv1  = fUv1;
			    m_fUv2  = fUv2;
	}


	void         CUIFusionzone::DrawFusionzone(IRenderTarget* pRT,kkRect& rt,int syn)
	{
	      CRect Rt;
		  int Width  =rt.right -rt.left;
		  int Height  =rt.bottom-rt.top;
            
		  Rt.left = m_XY1.x * Width;
		  Rt.top  = m_XY1.y * Height;

		  Rt.right   = m_XY3.x * Width;
		  Rt.bottom  = m_XY3.y * Height;

		  OnDraw(pRT,Rt,0);
		 
		 if(syn)
		 {
			   
			   pRT->Rectangle(&Rt,m_style.m_crBorder);
		 }
	}
	int          CUIFusionzone::IsContainPoint(const kkPoint &pt,int bClientOnly)
	{

        if (m_bDraging == EDrag_NcClient) {
			return 1;
		}
		CPoint Pt;
		Pt.x = pt.x;
		Pt.y = pt.y;
		
	    int bRet = 0;
		if(bClientOnly){

			 if(m_bDraging == EDrag_Client)
			    return 1;

			CRect rcClient;
			GetClientRect(&rcClient);
			GetScalRect(rcClient,m_fDisplayScale);
			bRet = rcClient.PtInRect(Pt);
		}else{
			CRect rcWindow;
		    GetWindowRect(&rcWindow);
			GetScalRect(rcWindow,m_fDisplayScale);
			bRet = rcWindow.PtInRect(Pt);
		}
		
		return bRet;

	}
    CUIWindow*   CUIFusionzone::WndFromPoint( kkPoint  ptHitTest, int bOnlyText)
	{

	    CRect rcWindow;
		GetWindowRect(&rcWindow);
		

		CPoint ptx;
		ptx.x = ptHitTest.x;
		ptx.y = ptHitTest.y;
		
		if(m_bDraging == EDrag_WarpVect|| m_bDraging == EDrag_Client) {
			return this;
		}
		if(!rcWindow.PtInRect(ptx))
			return NULL;
		return this;
	}
	void         CUIFusionzone::OnSize(unsigned int nType, int x,int y)
	{
	
	}

	CUIWindow*   CUIFusionzone::SetCapture()
	{
		CUIWindow*  Par = this->GetParent();
		return GetContainer()->OnSetUIWndCapture(this);
	}

	
	void         CUIFusionzone::OnLButtonDBLClk(unsigned int nFlags,CPoint pt)
	{
	    SetCapture();
		
		UIEvent ev;
		ev.Handle = this;
		ev.type = E_CltLBDBLClk;
		ev.pa1 = 0;
		GetContainer()->GetIUIMessage()->OnUIEvent(&ev);

	}
	void         CUIFusionzone::OnRButtonUp(unsigned int nFlags,CPoint pt)
	{
	     UIEvent ev;
		 ev.Handle = this;
		 ev.type   = E_RClick;
		 ev.pa1    = pt.x;
         ev.pa2    = pt.y;
		 GetContainer()->GetIUIMessage()->OnUIEvent(&ev);

	}
	void         CUIFusionzone::OnLButtonUp(UINT nFlags, CPoint point)
	{
	    UIEvent ev;
		ev.Handle = this;
		ev.type = E_Click;
		ev.pa1 = 0;
		GetContainer()->GetIUIMessage()->OnUIEvent(&ev);
		this->Invalidate();
		m_bDraging = 0;
		m_PtSize = sUnkown;
	}
	void         CUIFusionzone::OnNcLButtonUp(UINT nFlags,CPoint pt)
	{
		m_bDraging = 0;
		m_PtSize = sUnkown;
	}
	
	void         CUIFusionzone::OnLButtonDown(unsigned int nFlags, CPoint pt)
	{
		SetCapture();
		
		m_bDraging = EDrag_Client;
		CRect rt;
		CUIWindow::GetWindowRect(&rt);
        m_ptClick = pt - rt.TopLeft();
		
		GetContainer()->GetIUIMessage()->UISetCursor("sizeall");
		
	}
	void         CUIFusionzone::OnNcLButtonDown(unsigned int nFlags,CPoint pt)
	 {
		 CRect rt;
		 CUIWindow::GetWindowRect(&rt);
		 m_ptClick = pt;// -rt.TopLeft();
		 m_bDraging =EDrag_NcClient;
		 SetCapture();
	 }
	void         CUIFusionzone::SetDisplayScale(float fDisplayScale)
	 {
		m_fDisplayScale= fDisplayScale;
	 }

	void         CUIFusionzone::OnMouseMove(unsigned int nFlags,CPoint pt)
	{
		

		if (!m_bDraging)
		{
		   
		}
		
        if(m_bDraging&&  nFlags & 0x0001 ==0x0001)
        {
			if(m_bDraging==EDrag_Client)
			{
					CPoint ptLT =pt - m_ptClick;
					ptLT.x *= m_fDisplayScale;
					ptLT.y *= m_fDisplayScale;
					CRect rcWnd;
					CUIWindow::GetWindowRect(&rcWnd);
					rcWnd.MoveToXY(ptLT);
		            
					
					 CPoint pts[4];

					 pts[0].y = rcWnd.top;
					 pts[0].x = rcWnd.left;

					 pts[1].y = rcWnd.top;
					 pts[1].x = rcWnd.right;

					 pts[2].y = rcWnd.bottom;
					 pts[2].x = rcWnd.right;

					 pts[3].y = rcWnd.bottom;
					 pts[3].x = rcWnd.left;
					 
					
					
					OnRelayout(&rcWnd);
					SStringA Pos;
					Pos.Format("%d,%d,@%d,@%d",rcWnd.left,rcWnd.top,rcWnd.Width(),rcWnd.Height());
					this->SetAttribute("pos", Pos);
					this->GetParent()->UpdateChildrenPosition();

					
					 UIEvent ev;
					 ev.Handle = this;
					 ev.type   = E_CltMove;
					 ev.pa1    = 0;
					 GetContainer()->GetIUIMessage()->OnUIEvent(&ev);
			}
        }else{
		
		   m_bDraging = 0;
		}
	 }
	 
	void         CUIFusionzone::OnKeyMove(EKEY_MOVE KeyMove)
	 {
	    CRect rt;
		CUIWindow::GetWindowRect(&rt);
	
		int ps = m_fDisplayScale*1;
		if(KeyMove == EKM_Left)
		{
			rt.left  -=ps;
			rt.right -=ps;
		}
		else if(KeyMove == EKM_Top){
			rt.top    -=ps;
			rt.bottom -=ps;
		}else if(KeyMove == EKM_Right)
		{
		    rt.left  +=ps;
			rt.right +=ps;
		}
        else if(KeyMove == EKM_Bottom)
		{
		    rt.top    +=ps;
			rt.bottom +=ps;
		}
		
		SStringA Pos;
		OnRelayout(&rt);
		Pos.Format("%d,%d,@%d,@%d",rt.left,rt.top,rt.Width(),rt.Height());
		this->SetAttribute("pos", Pos);
		this->GetParent()->UpdateChildrenPosition();

		UIEvent ev;
		ev.Handle = this;
		ev.type   = E_CltMove;
		ev.pa1    = 0;
		GetContainer()->GetIUIMessage()->OnUIEvent(&ev);

		m_bDraging =0;
		this->Invalidate();
	 }
	void         CUIFusionzone::OnNcMouseMove(unsigned int nFlags,CPoint pt)
	{
		  
		  CRect rcWnd;
		  CUIWindow::GetWindowRect(&rcWnd);
			
	       
		  if(!m_bDraging)
		  {
			  if(pt.x >= rcWnd.right-4 && pt.x <= rcWnd.right)
			  {
					m_PtSize= sRight;
			  }else if(pt.y > rcWnd.top && pt.y  < rcWnd.top + 4 ){
					 m_PtSize=  sTop;
			  }else if(pt.y  >= rcWnd.bottom && pt.y < rcWnd.bottom +4){
					 m_PtSize=  sBottom;
			  }else if(pt.x  >= rcWnd.left && pt.x < rcWnd.left + 4) {
				   m_PtSize =sLeft;
			  }else {
				   m_PtSize = sUnkown;
			  }
		  } 
		 PtSize tt=m_PtSize;
	     if(m_bDraging==EDrag_NcClient&&nFlags & 0x0001 ==0x0001){
            SetCapture();
			CRect rcWindow;
		    CUIWindow::GetWindowRect(&rcWnd);
            CPoint ptLT = pt - m_ptClick;
		
			if(ptLT.x!=0 || 1)
			{
					m_ptClick = pt;
					CRect rt = rcWnd;
					if(tt==sLeft)
					{
						rt.left += ptLT.x;
					}else if(tt== sRight){
					   rt.right += ptLT.x;
					}else if(tt == sTop)
					{
						rt.top+=ptLT.y;
					}else if(tt == sBottom)
					{
						rt.bottom +=ptLT.y;
					}

					if(rt.Width()<=10||rt.Height()<=10)
						return ;
					SStringA Pos;
					Pos.Format("%d,%d,@%d,@%d",rt.left,rt.top,rt.Width(),rt.Height());
					this->SetAttribute("pos", Pos);
					OnRelayout(&rt);
					this->GetParent()->UpdateChildrenPosition();

					 UIEvent ev;
					 ev.Handle = this;
					 ev.type   = E_CtlSize;
					 ev.pa1    = 0;

					 GetContainer()->GetIUIMessage()->OnUIEvent(&ev);
			}
		}
	   if(tt== sRight|| tt == sLeft)
		   GetContainer()->GetIUIMessage()->UISetCursor("sizewe");
	   else  if(tt== sTop || tt==sBottom)
		  GetContainer()->GetIUIMessage()->UISetCursor("sizens");
	}
	void         CUIFusionzone::OnNcMouseLeave(unsigned int nFlags)
	 {
		
		m_bDraging = 0;
		m_PtSize = sUnkown;
			
	 }
	
	void        CUIFusionzone::OnDraw(IRenderTarget  *pRT,CRect& Rt,int syn)
	{
	             unsigned int cr1 = 0xFF000000;
				 unsigned int cr2 = m_nChannelColor;
				 int width =  Rt.right -Rt.left;
		         int height = Rt.bottom - Rt.top;

				 if( m_nBalance){
				 
				         if(m_nFusionType==EFT_LEFT)
						     pRT->FillRect(&Rt,cr1,cr1,cr2,cr2);

						  if(m_nFusionType==EFT_RIGHT)
								pRT->FillRect(&Rt,cr2,cr2,cr1,cr1);

						  if(m_nFusionType==EFT_TOP)
								pRT->FillRect(&Rt,cr1,cr2,cr1,cr2);

						  if(m_nFusionType==EFT_BOTTOM)
								pRT->FillRect(&Rt,cr2,cr1,cr2,cr1);
				 
				 }else{
						 SUVInfo  Uv1 = { Rt.left, Rt.top };
						 SUVInfo  Uv2 = { Rt.right,Rt.bottom};

						 SUVInfo  FirstCtrlPt  = { Rt.left +  m_fUv1.x  * width, Rt.top + m_fUv1.y  * height};
						 SUVInfo  SecondCtrlPt = { Rt.left +  m_fUv2.x  * width, Rt.top + m_fUv2.y  * height};

						 {
							  std::vector<SUVInfo> XyVec;
							  CBezierSpline::GetBezier(Uv1,Uv2,FirstCtrlPt,SecondCtrlPt,XyVec);

							  int count = XyVec.size()-1;
							  for(int i=0;i<count;i++)
							  {
								  SUVInfo info1= XyVec.at(i);
								  if(m_nFusionType==EFT_LEFT)
								      info1.x =Rt.left+ Rt.right - info1.x;
								 
								 SUVInfo info2= XyVec.at(i+1);
								 if(m_nFusionType==EFT_LEFT)
								  info2.x =Rt.left+ Rt.right - info2.x;

								  SUVInfo info3= { Rt.left ,info1.y};
								  SUVInfo info4= { Rt.left ,info2.y};

								  if(m_nFusionType==EFT_LEFT)
									  pRT->FillRect(&info3,&info1, &info4,&info2,  cr1,cr2,cr1,cr2);

								  if(m_nFusionType==EFT_RIGHT)
								  {
									  //info1.x = Rt.right + Rt.right - info1.x;
									  info3.x  = Rt.right;
									  info3.y  = info1.y;

									  info4.x  = Rt.right;
									  info4.y  = info2.y; /**/
									  pRT->FillRect(&info1,&info3,&info2,&info4,   cr2,cr1,cr2,cr1);
								  }

								  if (m_nFusionType == EFT_TOP) {
									  info3.x =  info1.x;
									  info3.y =  Rt.top;

									  info4.x =info2.x;
									  info4.y =   Rt.top;
									  pRT->FillRect(&info1, &info3,&info2, &info4,  cr2,cr1, cr2,  cr1);
								  }
										

								  if(m_nFusionType==EFT_BOTTOM)
								  {
										  
									  info3.x  = info1.x; 
									  info3.y  = Rt.bottom;

									  info4.x  = info2.x; 
									  info4.y  = Rt.bottom;
									  pRT->FillRect(&info1,&info3,&info2,  &info4,  cr2,cr1,  cr2, cr1);
								  }

								 // pRT->FillRect(&info3,&info1,&info4,&info2,cr1,cr2,cr1,cr2);
							  }

							  if( syn){
							  count = XyVec.size();
							  for (int i = 0; i<count; i++)
							  {
								  SUVInfo& info1 = XyVec.at(i);
								  if(m_nFusionType==EFT_LEFT)
								    info1.x = Rt.left + Rt.right - info1.x;
							  }

							  pRT-> DrawLines(&XyVec[0],XyVec.size(),0xFFFF0000);
							  }
						 }	
				 }

	}
	void        CUIFusionzone::OnPaint( IRenderTarget  *pRT)
	{
			     CRect Rt;
			     GetClientRect(&Rt);
	       
				 OnDraw(pRT,Rt);
			

			     pRT->Rectangle(&Rt,m_style.m_crBorder);
	 }
	void        CUIFusionzone::OnEraseBkgnd( IRenderTarget  *pRT)
	{
	
	
	}
}
