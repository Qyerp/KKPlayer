
#ifndef SDLSound_H_
#define SDLSound_H_
#include "IKKAudio.h"
#include "KKPlayer.h"
#define USE_SDL2 1

#ifdef USE_SDL2
    #include "../../SDL2-2.0.8/include/SDL.h"
	#pragma comment(lib, "sdl2.lib")
#else
	#include "../../SDL-1.2.15/include/SDL.h"
	#pragma comment(lib, "sdl.lib")
#endif

#include "../KKPlayerCore/KKLock.h"
class CSDLSound: public IKKAudio
{
	public:
		CSDLSound( );
		~CSDLSound();
		virtual void SetWindowHAND(int m_hwnd);
	
		/********设置音频回到函数*********/
		virtual void SetAudioCallBack(pfun fun,void* UserData);
		/***********初始化音频设备*********/
		int OpenAudio( int &wanted_channel_layout, int &wanted_nb_channels, int &wanted_sample_rate);
		/*******读取音频数据********/
		virtual bool ReadAudio();	   
		virtual void Start();	   
		virtual void Stop();	   
		/*********关闭**********/
		virtual void CloseAudio();	
		/*********设置音量************/
		virtual void SetVolume(long value);
		virtual long GetVolume();

		void  SetPlayer(KKPlayer* pPlayer);
public:
		void KKSDLCall( Uint8 *stream, int len);
private:
	    CKKLock m_lock;
		pfun    m_pFun;
		void*   m_UserData;
		long    m_Vol;
        KKPlayer*       m_pPlayer;
#ifdef USE_SDL2
		SDL_AudioDeviceID m_pSdlAudio;
#else
		void*   m_pSdlAudio;
#endif

};
#endif