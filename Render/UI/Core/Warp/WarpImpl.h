#ifndef WarpImpl_H_
#define WarpImpl_H_
#include "IWarp.h"
#include <vector>
#include "../UIWindow.h"
namespace KKUI
{
	class CWarpImpl:public IWarp
	{
	     public:
	           CWarpImpl();
			   void  OnPaint(IRenderTarget  *pRT,const CRect& rt);
			   void  OnPaintMesh(IRenderTarget  *pRT,const CRect& rt,unsigned int cr);
			   void  GetPtInfo(int RowId,int ColId, SUVInfo& Out) ;
			   void  SetPtInfo(int RowId,int ColId,const SUVInfo& Out) ;
			   bool  TestPtInMestWarping(CPoint pt,int &rowOut,int& colOut,const CRect& rt);
			   //设置分割
			   void  SetXYDiv(int x, int y) ;
			   void  GetXYDiv(int &x, int &y);
			   int   GetWarpInfos(SUVInfo* infos,int count);
	           int   SetWarpInfos(SUVInfo* infos,int count);
	    private:
               //分段绘制
			   void PaintMesh(IRenderTarget  *pRT,const CRect& rt,unsigned int cr);
			   //分段绘制
			   void PaintCell(IRenderTarget  *pRT,const CRect& rt);
	};
}
#endif