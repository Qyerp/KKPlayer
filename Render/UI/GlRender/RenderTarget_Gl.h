#ifndef RenderTarget_Gl_H_
#define RenderTarget_Gl_H_



#include <map>
#include <set>
#include <string>
#include <vector>
#include "ObjRef.h"
#include "Core/ObjectImp.h"
#include "Core/IRender.h"
#include "RendLock.h"
#include "glad.h"
#include <gl/glu.h>
//#include <gl/gl.h>


namespace KKUI
{
#define GL_PI 3.1415926f
#ifdef GetRValue
#undef GetRValue
#endif
#ifdef GetGValue
#undef GetGValue
#endif
#ifdef GetBValue
#undef GetBValue
#endif

	class CRenderTarget_Gl;
	class CVertexShader_Gl;
	class CFragmentShader_Gl;
	class SRenderFactory_Gl : public  TObjRefImpl<IRenderFactory>
	{
		friend class CRenderTarget_Gl;
	public:
		SRenderFactory_Gl();
		~SRenderFactory_Gl();
		bool                        init();
		//初始化内置着色器
		int                          InitShader();
		int                          CreateRenderTarget(IRenderTarget ** ppRenderTarget,void* Handle,int nWid,int nHei);
		int                          CreateBitmap(IBitmap ** ppBitmap);
		int                          CreateYUV420p(IPicYuv420p ** ppBitmap);
		int                          BeforePaint(IRenderTarget* pRenderTarget);
		void                         EndPaint(IRenderTarget* pRenderTarget,bool WarpNet=0);
		void                         CreateFont(IFont** ppFont,int FontSize,wchar_t* FontName);


		void                         Lock();
		void                         UnLock();

		void*                        CreateVideoDecoder(int CodecId,int Width,int Height);
		void                         DeleteVideoDecoder(void* pDXVA2VD);

		void*                        CreateDxTexture();
		void                         DeleteDxTexturer(void* DxTexture);

		int							SetupPixelFormat(HWND hwnd, HGLRC* prc, HDC* pDC);
		void						 MakeLoadContext();

		CVertexShader_Gl*			GetVertexShader() {return m_pVertexShader;}
		CFragmentShader_Gl*         GetFragmentShader() {return m_pFragmentShader;}
	private:

		CRendLock                   m_lock;
		//呈现对象管理器
		std::set<IRenderObj*>        m_IRenderObjSet;

		//HGLRC m_hRC1;			   //ogl上下文，主线程
		HGLRC m_hRC;			   //load线程
		HDC   m_hDC;		
		///********像素着色器**********/
		//std::map<std::string ,CPixelShader_D3D11*> m_IPsObjMap;

		CFragmentShader_Gl*      m_pFragmentShader;
		CVertexShader_Gl*		 m_pVertexShader;
	};


	class CFont_Gl : public TObjRefImpl<IFont>
	{
	public:
		CFont_Gl(SRenderFactory_Gl* pRenderFactory, int FontSize, wchar_t* FontName);
		~CFont_Gl();
		void          Release2();
		void          ReLoadRes();
		void		  DrawText(kkRect& rect, LPCTSTR lpszText, GLfloat r, GLfloat g, GLfloat b);
		int           TextSize();
		HDC			  GetDC();
	private:
		HDC						m_hdc;
		HFONT					m_hFont;
		SRenderFactory_Gl*      m_pRenderFactory;
		int                     m_nTextSize;
		wchar_t                 m_szFontName[64];
	};


	class CGLFont
	{
	public:
		void entext(float x, float y, LPCTSTR str, HFONT hFont, float r, float g, float b);
		void c3dtext(LPCTSTR str, HFONT hFont, float z);
		void Printfc3d(LPCTSTR strText, HFONT hFont, float z = 0.05f);
		void Printftext(int x, int y, LPCTSTR lpszText, HFONT hFont);
		void settext(float x, float y, LPCTSTR str, HFONT Font, float r, float g, float b);

		CGLFont();
		virtual ~CGLFont();
	private:
		HFONT hFont;
	};


	class CBitmap_Gl:public TObjRefImpl<IBitmap>
	{
	public:
		CBitmap_Gl(SRenderFactory_Gl* pRenderFactory);
		~CBitmap_Gl();
		void          Release2();
		void          ReLoadRes();
		virtual kkSize       Size() const;
		virtual int          Width();
		virtual int          Height();
		int                  LoadFromFile(const char*FileName);
		int	                 LoadFromMemory(const void* pBuf,int szLen);
		void*				 GetBuffer();		//获取图片数据
		void				 ReleaseBuffer();	//释放数据
		int					 GetChannelCount();	//通道数
	private:
		//暂且先缓存
		void*                   m_pSrcBuffer;
		int                     m_nSrcBufferLen;
		kkSize                  m_sz;
		int						m_nChannels;
	};

	struct GlVertex
	{
		GLfloat x, y, z;
		GLfloat s, t;
	};

	class CRenderTarget_Gl: public TObjRefImpl<IRenderTarget>
	{
	public:
		CRenderTarget_Gl(SRenderFactory_Gl* pRenderFactory,HWND h,int nWidth, int nHeight);
		~CRenderTarget_Gl();
		void  Release2();
		void  ReLoadRes();
		void  ReSize(int nWidth, int nHeight);
		//绘制位图
		int   DrawBitmap(kkRect* pRcDest,IBitmap *pBitmap,int xSrc,int ySrc,int cx,int cy,unsigned char byAlpha=0xFF);
		int   DrawBitmapEx(kkRect*  pRcDest,IBitmap *pBitmap,kkRect* pRcSrc,unsigned int expendMode,unsigned char byAlpha=0xFF/**/ );
		int   DrawBitmap9Patch(kkRect* pRcDest,IBitmap *pBitmap,kkRect* pRcSrc,kkRect* pRcSourMargin,unsigned expendMode,unsigned char byAlpha=0xFF);

		//绘制YUV, VertInfo按照顺时针排，从矩形的左上角开始排列。SPalette:调色板
		virtual int   DrawYuv420p(IPicYuv420p *pYuv420p,SinkFilter*Filter,const SPalette& Palette,SVertInfo* VertInfo,int VertCount,int UseLast=0,SMaskInfo* maskinfo=0,int maskcount=0);

		//滤镜处理
		virtual int   FilterYuv420p(IPicYuv420p *pYuv420p,SinkFilter* Filter);

		void  GetVertex(GlVertex* vertex, const kkRect& rt, int xSrc, int ySrc, int NeedWidth, int NeedHeight, int SWidth, int SHeight);


		void  DrawLine(int startx,int starty, int endx,int endy, unsigned int color);
		void  DrawLines(kkPoint* pts,int count, unsigned int color);
		void  DrawLines(SUVInfo* pts,int count, unsigned int color);

		void  DrawText(IFont* pFont,wchar_t *text,kkRect* pRcDest,unsigned int color=0xFFFFFFFF);
		void  MeasureText(IFont* pFont,wchar_t *text,kkSize *pSz );

		//垂直绘制文本
		void  TextOutV(IFont* pFont,int x,int y , wchar_t* strText,unsigned int color=0xFFFFFFFF);
		void  DrawTextV(IFont* pFont,wchar_t *text,kkRect* pRcDest,unsigned int color=0xFFFFFFFF);
		void  MeasureTextV(IFont* pFont,wchar_t *text,kkSize *pSz );


		void  Rectangle(kkRect* rt,unsigned int cr);

		void  RectangleByPt(kkPoint* pt,int width,int height,unsigned int cr);
		int   FillRect(kkRect* rt,unsigned int cr);
		void  FillRect(kkRect* rt,unsigned int cr1,unsigned int cr2,unsigned int cr3,unsigned int cr4);
		void  FillRect(SUVInfo* pts1,SUVInfo* pts2,SUVInfo* pts3,SUVInfo* pts4,unsigned int cr1,unsigned int cr2,unsigned int cr3,unsigned int cr4);
		void  FillRectByPt(kkPoint* pt,int width,int height,unsigned int cr);

		void  DrawCircle(int xCenter, int yCenter, int nRadius,unsigned int cr,int fillmodel=0);
		int   GetWidth(){return    m_nWidth;}
		int   GetHeight(){return   m_nHeight;}

		//是否启用变形
		void  SetEnableWarp(void*  pIWarp);
		int   GetEnableWarp(){return m_nWarp;}
		void* GetIWarp(){return m_pIWarp;}
		void  SetFusionzone(void* Fusionzone);
		//是否启用融合带
		int   GetEnableFusion(){return m_nEnableFusion;}
		void  SetEnableFusionzone(int Fusionzone){m_nEnableFusion = Fusionzone; }
		//当有变形是通过这个来绘制
		void  DrawWarp(SVertInfo* VertInfo,int VertCount);

		//绘制融合带
		void  OnFusionzone();


		//opengl
		BOOL SetupPixelFormat();
		void Inix();
		void SwapBuffer();

	private:
		inline GLfloat GetRValue(unsigned int cl);
		inline GLfloat GetGValue(unsigned int cl);
		inline GLfloat GetBValue(unsigned int cl);
		void RecreateSwapChain();
		static void* OpenglExtProc(const char *name);

		int m_nWidth;
		int m_nHeight;
		HWND m_hView;

		HGLRC m_hRC;			//ogl上下文
		HDC   m_hDC;				
		SRenderFactory_Gl*		 m_pRenderFactory;
		int                      m_nWarp;
		//启用融合带
		int                      m_nEnableFusion; 
		//融合带
		void*                    m_pFusionzone;
		void*                    m_pIWarp;

	};

	//顶点着色器
	class CVertexShader_Gl : public TObjRefImpl<IRenderOthert>
	{
	public:
		CVertexShader_Gl(SRenderFactory_Gl* pRenderFactory);
		~CVertexShader_Gl();
		void  Release2();
		void  ReLoadRes();

		/*LPD3DXCONSTANTTABLE      GetConstantTable(){return m_pConstantTable;}
		LPDIRECT3DPIXELSHADER9	 GetVertexShader(){return m_pPixelShader;}
		D3DXHANDLE	    GetSamplerHandel(char *name);
		HRESULT        SetMatrix(char* MatrixName, D3DXMATRIX	&Matrix);
		HRESULT        SetBool(char* Name, bool b);
		HRESULT        SetInt(char* Name, int b);
		HRESULT        SetFloat(char* Name, float b);
		HRESULT        SetVectorArray(char* Name,CONST D3DXVECTOR4* pVector, UINT Count);
		HRESULT        SetIntArray( char* Name,CONST INT* pn, UINT Count) ; 
		HRESULT        GetConstantDesc(D3DXHANDLE hConstant, D3DXCONSTANT_DESC *pConstantDesc, UINT *pCount);*/
		GLuint		  GetVertexShader() {return m_Program;}
		virtual void IniPs(GLuint& program);
		virtual const char* GetPsName() { return "";}
	protected:
		//LPDIRECT3DPIXELSHADER9	  m_pPixelShader;
		SRenderFactory_Gl*     m_pRenderFactory;
		GLuint m_Program;
		bool	m_bInit;
	};

	

	//片段着色器
	class CFragmentShader_Gl : public TObjRefImpl<IRenderOthert>
	{
	public:
		CFragmentShader_Gl(SRenderFactory_Gl* pRenderFactory);
		~CFragmentShader_Gl();
		void  Release2();
		void  ReLoadRes();

		/*LPD3DXCONSTANTTABLE      GetConstantTable(){return m_pConstantTable;}
		LPDIRECT3DPIXELSHADER9	  GetPixelShader(){return m_pPixelShader;}
		D3DXHANDLE	    GetSamplerHandel(char *name);
		HRESULT        SetMatrix(char* MatrixName, D3DXMATRIX	&Matrix);
		HRESULT        SetBool(char* Name, bool b);
		HRESULT        SetInt(char* Name, int b);
		HRESULT        SetFloat(char* Name, float b);
		HRESULT        SetVectorArray(char* Name,CONST D3DXVECTOR4* pVector, UINT Count);
		HRESULT        SetIntArray( char* Name,CONST INT* pn, UINT Count) ; 
		HRESULT        GetConstantDesc(D3DXHANDLE hConstant, D3DXCONSTANT_DESC *pConstantDesc, UINT *pCount);*/
		virtual void IniPs(GLuint& program);
		virtual const char* GetPsName() { return "";}
	protected:
		//LPDIRECT3DPIXELSHADER9	  m_pPixelShader;
		SRenderFactory_Gl*     m_pRenderFactory;
		GLuint m_Program;
	};

	///*******yuv着色器******/
	//class CYUVPixelShader_D3D9:public CPixelShader_D3D9
	//{
	//public:
	//	CYUVPixelShader_D3D9(SRenderFactory_D3D9* pRenderFactory);
	//	~CYUVPixelShader_D3D9(); 
	//	void IniPs();
	//	void  ReLoadRes();
	//	const char* GetPsName(){return "yuvps";}
	//};

	////亮度，明亮度调节
	//class CARGBPixelShader_D3D9:public CPixelShader_D3D9
	//{
	//public:
	//	CARGBPixelShader_D3D9(SRenderFactory_D3D9* pRenderFactory);
	//	~CARGBPixelShader_D3D9(); 
	//	void IniPs();
	//	void  ReLoadRes();
	//	const char* GetPsName(){return "argbps";}
	//};
	////通用着色器
	//class CCommPixelShader_D3D9:public CPixelShader_D3D9
	//{
	//public:
	//	CCommPixelShader_D3D9(SRenderFactory_D3D9* pRenderFactory,const DWORD*  m_psCode,const char* psName );
	//	~CCommPixelShader_D3D9(); 
	//	void IniPs();
	//	void  ReLoadRes();
	//	const char* GetPsName();
	//private:
	//	const DWORD*  m_pPsCode;
	//	std::string   m_strPsName;

	//};

}

#endif