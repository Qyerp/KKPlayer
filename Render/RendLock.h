/**************************kkplayer*********************************************/
/*******************Copyright (c) Saint ******************/
/******************Author: Saint *********************/
/*******************Author e-mail: lssaint@163.com ******************/
/*******************Author qq: 784200679 ******************/
/*******************KKPlayer  WWW: http://www.70ic.com/KKplayer ********************************/
/*************************date��2015-6-25**********************************************/
#ifndef RendLock_H
#define RendLock_H
/***���ֵ***/
class CRendLock
{
public:
	CRendLock();
    ~CRendLock();
    void Lock();
    void Unlock();
    int  TryLock();
private:
		CRendLock(const CRendLock& cs);
		CRendLock operator = (const CRendLock& cs);
 
private:
        //�ٽ�ֵ
	void* m_crisec;
};

class CRendGurd
{
	  public:
		        CRendGurd(CRendLock& lock):m_lock(lock)
				{
					m_lock.Lock();
				}
				
				~CRendGurd()
				{
					m_lock.Unlock();
				}
	private:
			   void* operator new(size_t size );
			   void  operator delete(void *ptr);
			   CRendGurd(const CRendGurd& cs);
			   CRendGurd& operator = (CRendGurd& cs);
	private:
			CRendLock   &m_lock;
};

#endif