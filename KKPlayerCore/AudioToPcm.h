#ifndef AudioToPcm_H_
#define AudioToPcm_H_
class  IRecPcm
{
	public:
	    virtual  void OnRecPcm(void* UserData, int Id,int pts, int totalpts,void* buf,int buflen) = 0;
};

//����ģʽ
class CAudioToPcm
{
	public:
		  CAudioToPcm(int Id, IRecPcm* pIRecPcm);
		  ~CAudioToPcm();
		  //Ƶ�ʣ�16bit,��ɹ
		  int AudioToPcm(void* UserData,const char* path,int ch_layout,int sample_rate);
		  int PlayPause(int type);
		  void SetVol(int vol);
		  int GetPlayState();
	private:
		int        m_nObjId;
		IRecPcm*   m_pIRecPcm;
		int        m_nPlayState;
		int        m_nVol;

};
#endif
