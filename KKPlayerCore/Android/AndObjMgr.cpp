#include "AndObjMgr.h"
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)
CAndObjMgr::CAndObjMgr():m_nObjId(0)
{
	
	
}
CAndObjMgr::~CAndObjMgr()
{
	
	
}
int     CAndObjMgr::CreateObj(int ObjType)
{
	if (ObjType != 2)
		return 0;
	Lock();
    KKObjInfo info;
	info.ObjHandle=++m_nObjId;
	CAudioToPcm* obj = new CAudioToPcm(info.ObjHandle,this);
	info.Obj=obj;
	
	info.ObjType=ObjType;   //对象类型.
	info.JCall = 0;         //一个Java回调对象
	info.UseCount = 1;
	m_ObjMap[info.ObjHandle] = info;
	Unlock();
	return info.ObjHandle;
}
int     CAndObjMgr::DeleteObj(JNIEnv *env, int ObjId)
{
	int ret = -1;
	Lock();
	KKObjInfo* Info = GetObjInfo(ObjId);
	if (Info != 0&& Info->UseCount==0) {
		ret = 0;
		if (Info->JCall) {
			///释放全局对象
			env->DeleteGlobalRef(Info->JCall);
		}
		
		if (Info->ObjType == 2) {
			CAudioToPcm* obj =(CAudioToPcm*) Info->Obj;
			delete obj;
		}

		std::map<int, KKObjInfo>::iterator It = m_ObjMap.find(ObjId);
		m_ObjMap.erase(It);
	}
	Unlock();
	return ret;
}
void    CAndObjMgr::Lock()
{
	m_Lock.Lock();
	
}
void    CAndObjMgr::Unlock()
{
	m_Lock.Unlock();
	
}
KKObjInfo* CAndObjMgr::GetObjInfo(int ObjId)
{
	std::map<int, KKObjInfo>::iterator It = m_ObjMap.find(ObjId);
	if (It != m_ObjMap.end())
	{ 
		return &It->second;
	}
	return 0;
}
int       CAndObjMgr::SetObjCall(JNIEnv *env, int ObjId, jobject icall)
{
	int ret = -1;
	Lock();
	KKObjInfo* Info = GetObjInfo(ObjId);
	if (Info != 0) {
		//转化为全局对象
		jobject GObj = env->NewGlobalRef(icall);
		if (Info->JCall) {
			///释放全局对象
			env->DeleteGlobalRef(Info->JCall);
		}
		if (Info->Obj_class) {
			env->DeleteGlobalRef(Info->Obj_class);
		}
		Info->JCall = GObj;
		jclass   obj_class= env->GetObjectClass(Info->JCall);
		Info->Obj_class= (jclass)env->NewGlobalRef(obj_class);
		Info->OnRecPcmBufferId = env->GetMethodID(Info->Obj_class, "OnRecPcmBuffer", "(III[B)V");
		ret = 0;
	}else{
		LOGE("SetObjCall can't Id\n");
	}
	Unlock();
	return ret;
}
int  CAndObjMgr::AddRef(int ObjId)
{
	int ret = -1;
	Lock();
	KKObjInfo* Info = GetObjInfo(ObjId);
	if (Info != 0) {
		ret = ++Info->UseCount;
	}
	Unlock();
	return ret;
}
int  CAndObjMgr::ReduceRef(int ObjId)
{
	int ret = -1;
	Lock();
	KKObjInfo* Info = GetObjInfo(ObjId);
	if (Info != 0) {
		ret = --Info->UseCount;
	}
	Unlock();
	return ret;
}

void CAndObjMgr::OnRecPcm(void* UserData, int Id, int pts,int totalpts,void* buf, int buflen)
{
	int ret = -1;
	Lock();
	KKObjInfo* Info = GetObjInfo(Id);
	if (Info != 0) {
		JNIEnv *env = (JNIEnv*)UserData;
		jbyteArray arry = env->NewByteArray(buflen);
		env->SetByteArrayRegion(arry, 0, buflen, (const jbyte*)buf);
		
		env->CallVoidMethod(Info->JCall, Info->OnRecPcmBufferId, Id, pts, totalpts,arry);
		env->DeleteLocalRef(arry);
	}else{
		LOGE("OnRecPcm can't Id\n");
	}
	Unlock();
	return ;
}