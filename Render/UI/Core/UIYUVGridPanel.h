﻿#ifndef UIYUVGridPane_H_
#define UIYUVGridPanel_H_
#include "UIWindow.h"
#include "UISkin.h"
#include "Layout/LayoutSize.h"
#include "UIYUV.h"
#include <string>
namespace KKUI
{
	
    class CUIYUVGridPanel: public CUIWindow
    {
		KKUI_CLASS_NAME(CUIWindow,CUIYUVGridPanel, "uiyuvgridpanel")
		public:
				CUIYUVGridPanel();
				virtual      ~CUIYUVGridPanel();
                int           LoadYUV(int row,int col,kkPicInfo *info,wchar_t* strTxt);
				int           SetItemTxt(int row,int col,std::wstring strTxt);
				int           SetItemUserData (int row,int col,void* userData);
				int           SetRowCol(int row,int col);
				void          ClearData(unsigned int cr);
				void          ClearItemData(int nRow,int nCol,unsigned int cr);
				void*         GetPanelItem(int row,int col);
		protected:
				int           OnCreate(int);
				void          OnNcPaint(IRenderTarget *pRT);
				void          OnSize(unsigned int nType, int x,int y);
		protected:
				KKUI_MSG_MAP_BEGIN()
					MSG_UI_CREATE(OnCreate)
					MSG_UI_NCPAINT(OnNcPaint)
					MSG_UI_SIZE(OnSize)
				KKUI_MSG_MAP_END(CUIWindow)
		protected:
			 IFont*       m_pFont;  
			 int          m_nRow;
			 int          m_nCol;
    };
}
#endif