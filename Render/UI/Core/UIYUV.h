#ifndef  KKUI_UIYUV_H_
#define  KKUI_UIYUV_H_
#include "UIWindow.h"
#include "RendLock.h"
#include "UIMoveWindow.h"
#include "IDisplayScale.h"
#include <vector>
namespace KKUI
{
	

    class IRotate
	{
	   public:
                 virtual  int GetRotate() = 0;
				  virtual	void   GetCenterPt2(CPoint& pt,kkRect* rt)=0;
	};
	class      CShapeMask
	{
	    public:
			CShapeMask(IRotate* p):m_nId(0),m_nHidePx(0),m_nPtCount(0),m_pIRotate(p)
			 {
			  
			 }
			 virtual ~CShapeMask(){}
			 virtual void  MoveMask(float x,float y)=0;
             //判断点是否在多边形内 
			 int        PnPoly(CPoint pt,CRect Rt);

			 CPoint     GetPt(int index,CRect Rt);
			 //测试是否在线段上
             bool       PtInSegment(CPoint pt,CRect Rt,int& Out);
			 
			 //点在顶点上
			 bool       PtInVert(CPoint pt,CRect Rt,int r,int& Out);
            
			 //在Index后插入一丁点
			 int        InsertVert( SUVInfo pt,int Index);
			
			
			 void       SetId(int Id)  {m_nId = Id;}
			 int        GetId()        {return m_nId;}
			 EMaskType  GetMaskType()  {return type;}
			
			 SUVInfo    m_UVS[20]; 
			 int        m_nPtCount;
			 int        m_nHidePx;
	    protected:
			 EMaskType  type;
			 int        m_nId;
			 IRotate*    m_pIRotate;
			 
	};
	//矩形
	class CSquareMask:public CShapeMask
	{
	    public:
		        CSquareMask(IRotate* p);
				~CSquareMask();
				void  MoveMask(float x,float y);
	};
	//三角形
	class CTriangleMask:public CShapeMask
	{
	   public:
		        CTriangleMask(IRotate* p);
				~CTriangleMask();
				void  MoveMask(float x,float y);
	};

	//显示区域的形状，目前只支持矩形(即4条边)
	class CUIYUV: public CUIMoveWindow,public IDisplayScale,public ITimelineHandler,public  IRotate
	{
	    KKUI_CLASS_NAME(CUIMoveWindow,CUIYUV,"uiyuv")
	    public:
				  CUIYUV();
				  ~CUIYUV();
				  void           InitAfter();
				  int            LoadYUV(kkPicInfo *info);
				  void           OnLButtonDown(unsigned int nFlags,CPoint pt);
				  void           OnLButtonUp(unsigned int nFlags,CPoint pt);
				  void           OnRButtonUp(unsigned int nFlags,CPoint pt);

				  //遮罩
				  int            GetMaskCount();
				  int            GetMaskInfo(int id,  SUVInfo* pPts,int ptCount,int* HidePixel);
				  int            SetMaskInfo(int id,  SUVInfo* pPts,int ptCount);

				  int            AddFilter(SinkFilter& Filter);
				  SinkFilter*    GetFristFilter();
                  void           DelFilter(int Id);

				  //添加mask
			      int            AddMask(EMaskType type);
				  //跟改像素方式
				  void           MaskChangePixel(int maskId, int HidePixel);
				  //删除mask
				  void           DelMask(int Id);
                  //保留前一帧
				  void           SavePreFrame(bool Save);
				  //获取图层的剪切
                  void           GetPicClip(SUVInfo* pPts,int ptCount);
				  void           SetPicClip(SUVInfo* pPts,int ptCount);
				

				  CShapeMask*    TestPtInMask(CPoint pt);
				  void           MoveMask(int x,int y);
				  void           OnUpdateFloatPosition(const  CSize&  szView,const CRect & rcParent);
				  IPicYuv420p*   GetIPicYuv420p();
				
				  //设置YUV的引用
				  void           SetQuoteYuv(CUIYUV* pQuoteYuvCtl);

				  void           SetClip(SUVInfo* pPts,int ptCount);
				  void           GetClip(SUVInfo* pPts,int ptCount);
				 
                  //设置旋转
                  void           SetRotate(int Rotate);
                  int            GetRotate();

				  void           SetIsRect(int IsIsRect);
				  int            GetIsRect();
				  //判断rt是否在区域里
				  int            RectInterRegion(CRect rt);
				  int            GetRegion(CPoint* Pts,int count);
				  //相对坐标
				  int            GetFRegion(SUVInfo* Pts, int ptCount);
				  void           SetFRegion(SUVInfo* Pts, int ptCount);

				  int            IsContainPoint(const kkPoint &pt,int bClientOnly);
				  int            OnNcHitTest(kkPoint pt);
				  CUIWindow*     WndFromPoint( kkPoint  ptHitTest, int bOnlyText);
				

				  void           ClearPicData(unsigned int Color);
				  void           Active();
				  void           SetTxt(std::wstring strTxt);
				  void           SetAalpha(int a);
				
				  //创建图片缓冲，对4k，有显著的效果
				  void           CreatePicCache(int width,int height,int format);
	    protected:
			       //点是否在区域内
			       int           IsPtInRegion(CRect& rt,kkPoint pt,int& nc);
				   //点是否在可以调节
				   void          GetJustPtsInNcRect(kkRect rt,kkPoint* PtNc);
			       void          GetRegionbyRotate(lpkkRect lprc,const kkPoint &OPt,float Rotate,kkPoint *OutPts,int PtsOut);

			       void          OnNextFrame();
			       int           OnSetCursor(const CPoint &pt);
			       bool          CanResponseMouseMsg();
			       int           OnCreate( int);
				   void          OnDestroy();
				   void          OnPaint( IRenderTarget  *pRT);
				   void          OnPaintEx( IRenderTarget  *pRT,CRect* rt);
				   void          OnEraseBkgnd( IRenderTarget  *pRT);
				   void          OnNcPaint(IRenderTarget  *pRT);
				
				   void          GetCenterPt2(CPoint& pt,kkRect* rt);
				   // int           OnRelayout(const CRect &rcWnd);
				   //更新坐标信息
				   void          OnUpDataCoordinate(int DisplayScale,int Old,int nRotate);
				   void          SetDisplayScale(float fDisplayScale);
				  
				   void          OnKeyMove(EKEY_MOVE KeyMove);
				   void          OnMouseMove(unsigned int nFlags,CPoint pt);
				   void          OnMouseLeave(unsigned int nFlags);
				   void          OnNcMouseLeave(unsigned int nFlags);
				   void          OnNcLButtonDown(unsigned int nFlags,CPoint pt);
			       void          OnNcLButtonUp(unsigned int nFlags,CPoint pt);
                   void          OnNcMouseMove(unsigned int nFlags,CPoint pt);
				   int           OnAttrVSkin(SStringA strValue,BOOL bLoading);
                   int           OnAttrBkSkin(SStringA strValue,BOOL bLoading);
				   int           OnAttrFlagSkin(SStringA strValue,BOOL bLoading);

				   int           OnFadeInMsec(SStringA strValue,BOOL bLoading);
				   int           OnFadeOutMsec(SStringA strValue,BOOL bLoading);

				   void          OnLButtonDBLClk(unsigned int nFlags,CPoint pt);
				   KKUI_ATTRS_BEGIN()
							ATTR_CUSTOM("FadeInMsec",OnFadeInMsec)
							ATTR_CUSTOM("FadeOutMsec",OnFadeOutMsec)

							ATTR_INT("Alpha1",m_nAlpha,0)
							ATTR_INT("Alpha2",m_nAlpha2,0)

							ATTR_INT("ColorR",m_nColorR,0)
							ATTR_INT("ColorG",m_nColorG,0)
							ATTR_INT("ColorB",m_nColorB,0)
							ATTR_INT("displayYuv",m_nDisplayYuv,0)
							ATTR_INT("IsRect",m_nIsRect,1)
							ATTR_INT("AdjustSize",m_nAdjustSize,1)
							ATTR_INT("Rotate",m_nRotate,1)
							ATTR_INT("Float",m_bFloat,0)
							//边框颜色
							ATTR_COLOR("crBorderCapture", m_crBorderCapture, 1);
				            ATTR_CUSTOM("VSkin",OnAttrVSkin)
                            ATTR_CUSTOM("bkSkin",OnAttrBkSkin)
							ATTR_CUSTOM("flagSkin",OnAttrFlagSkin)
				   KKUI_ATTRS_ENDEX(CUIMoveWindow)

				   KKUI_MSG_MAP_BEGIN()   
				        MSG_UI_CREATE(OnCreate)
						MSG_UI_SIZE(OnSize)
						MSG_UI_LBUTTONDOWN(OnLButtonDown)
						MSG_UI_LBUTTONUP(OnLButtonUp)
						MSG_UI_LBUTTONDBLCLK(OnLButtonDBLClk)
						MSG_UI_RBUTTONUP(OnRButtonUp)  
						MSG_UI_MOUSEMOVE(OnMouseMove)
						MSG_UI_MOUSELEAVE(OnMouseLeave)
						MSG_UI_NCMOUSELEAVE(OnNcMouseLeave)
						MSG_UI_NCMOUSEMOVE(OnNcMouseMove)
                        MSG_UI_NCLBUTTONDOWN(OnNcLButtonDown)
				        MSG_UI_NCLBUTTONUP(OnNcLButtonUp)

						MSG_UI_PAINT(OnPaint)
						MSG_UI_PAINT_EX(OnPaintEx)
						MSG_UI_ERASEBKGND(OnEraseBkgnd)
						MSG_UI_NCPAINT(OnNcPaint)
						MSG_UI_DESTROY(OnDestroy)
				   KKUI_MSG_MAP_END(CUIMoveWindow)
	    private:
			        void GetRectByPt(const CPoint& pt,CRect& rt,int wh=8);

					ISkinObj*                   m_pBkSkin;
					ISkinObj*                   m_pFlagSkin;
			        CSkinImgFrame*              m_pCircleSkin;

					//引用的对象
					CUIYUV*                     m_pQuoteYuvCtl;
					//是否为矩形
					int                         m_nIsRect;
					int                         m_nPtIndex;
					int                         m_nAdjustSize;
					//用于描述非矩形
					float                       m_fPosX1;
					float                       m_fPosY1;
					float                       m_fPosX2;
					float                       m_fPosY2;
					float                       m_fPosX3;
					float                       m_fPosY3;
					float                       m_fPosX4;
					float                       m_fPosY4;


					IPicYuv420p*                m_pIPicYuv420p;
					

					IFont*                      m_pFont;  
					std::wstring                m_strTxt;
					unsigned char               m_nAlpha;
                    unsigned char               m_nAlpha2;


					unsigned char               m_nColorR;
					unsigned char               m_nColorG;
					unsigned char               m_nColorB;
					//捕获时的边框颜色
					COLORREF                    m_crBorderCapture;
					//遮罩
					std::vector<CShapeMask*>    m_MaskVect;
					int                         m_nMaskId;
					CShapeMask*                 m_pHoverMask;
				
					//滤镜
					SinkFilter*                 m_pHeaderFilter;
					SinkFilter*                 m_pTailFilter;
					int                         m_nVertIndex;
					
					//用于图片截图
					float                       m_fPicX1;
					float                       m_fPicY1;
					float                       m_fPicX2;
					float                       m_fPicY2;
					float                       m_fPicX3;
					float                       m_fPicY3;
					float                       m_fPicX4;
					float                       m_fPicY4;
					
			
					int                          m_nDisplayYuv; 
					//旋转角度
                    int                          m_nRotate;
                    //缩放
					float                        m_fDisplayScale;
					//磁吸状态
                    SMagnetRegion                m_sMagnetRegion;

					//淡入毫秒
					int                          m_nFadeInMsec;
                    int                          m_nFadeInMsec2;
					//淡出毫秒
					int                          m_nFadeOutMsec;
					int                          m_nFadeOutMsec2;

					//顶点数据
					SVertInfo*                   m_pVertInfo;
	};
}
#endif