#include "UIWindow.h"
#include <assert.h>
#include "Layout/UiLayout.h"
#include "UIApp.h"
#include "SkinPool.h"
namespace KKUI{
	CUIWindow::CUIWindow():m_pParent(0),
				           m_pFirstChild(0),
				           m_pLastChild(0),
				           m_pNextSibling(0),
				           m_pPrevSibling(0),
				           m_nChildrenCount(0),
						   m_pUIContainer(0),
						   m_bFloat(0),
						   m_bVisible(1),
				           m_bDisplay(1), 
						   m_bDisable(0),
						   m_layoutDirty(dirty_self),
						   m_uZorder(0),
						   m_nID(0),
						   m_bMsgTransparent(0),
						   m_pUserData(0),
						   m_dwState(WndState_Normal),
						   m_pCurMsg(0)
		                  
	{
	    m_nMaxWidth.setWrapContent();
	   	m_pLayout.Attach(new CUILayout());
		m_pLayoutParam.Attach(new CUILayoutParam());
		m_pLayoutParam->SetMatchParent(Both);
	}
    CUIWindow::~CUIWindow()
	{
	     ReleaseFocus();
	}

	
	void   CUIWindow::ReleaseFocus()
	{
		IUIContainer*  pContainer = this->GetContainer();
		if(pContainer )
		{
		     CUIWindow* cap=  pContainer->GetUIWndCapture();
			 if(cap==this){
			     ReleaseCapture();
			 }
		}
	}
	
	///设置控件位置
   
	bool       CUIWindow::IsVisible(int bCheckParent) const
	{
	   if(bCheckParent) 
		      return (0 == (m_dwState & WndState_Invisible));
		else 
			return m_bVisible;
	}
    void       CUIWindow::SetVisible(bool bVisible)
	{
	   m_bVisible=bVisible;
	}
	int  CUIWindow::GetCtlAttr(const SStringA& attrName,SStringA& OutAttr)
	{
		   if(attrName.Compare("windowrect")==0){
			   CRect rt;
			   GetWindowRect(&rt);
		       OutAttr.Format("%d,%d,%d,%d",rt.left,rt.top,rt.right,rt.bottom);
			   return 1;
		   }else if(attrName.Compare("crborder")==0)
		   { 
			   OutAttr.Format("%d",this->m_style.m_crBorder);
		       return 1;
		   }
	       return  0;
	}

	 void CUIWindow::OnSize(unsigned int nType, int x,int y)
	 {
       
	 }

	void CUIWindow::OnDestroy()
	{
	    CUIWindow *pChild=m_pFirstChild;
		while (pChild)
		{
			CUIWindow *pNextChild=pChild->m_pNextSibling;
			pChild->SendMessage(UI_DESTROY);
			pChild->Release();

			pChild=pNextChild;
		}
		m_pFirstChild=m_pLastChild=NULL;
		m_nChildrenCount=0;
	}
	int  CUIWindow::OnCreate( int)
	{
	   return 0;
	}
	void CUIWindow::OnPaint( IRenderTarget  *pRT)
	{
		
	}
    void CUIWindow::OnEraseBkgnd( IRenderTarget  *pRT)
	{
		CRect rt;
		GetClientRect(&rt);


		kkRect rcWindow;
		GetWindowRect(&rcWindow);

		if(!m_style.GetMargin().IsRectNull())
		{
			if(this->m_style.m_crBorder!= CR_INVALID)
			{
				  CRect leftRt=rcWindow;
				  leftRt.right=leftRt.left+1;
			      pRT->FillRect(&leftRt,m_style.m_crBorder);	

				  CRect rightRt=rcWindow;
				  rightRt.left=rightRt.right-1;
				  pRT->FillRect(&rightRt,m_style.m_crBorder);	

				  CRect TopRt=rcWindow;
				  TopRt.bottom=TopRt.top+1;
				  pRT->FillRect(&TopRt,m_style.m_crBorder);	

				  CRect BpttomRt=rcWindow;
				  BpttomRt.top= BpttomRt.bottom-1;
				  pRT->FillRect(&BpttomRt,m_style.m_crBorder);	
			}
			   
		}

		if(this->m_style.m_crBg!= CR_INVALID)
			pRT->FillRect(&rt,this->m_style.m_crBg);
		//pRT->DrawLine(rt.left,rt.top,rt.right,rt.top,0);
	}
	

	void CUIWindow::_PaintClient(IRenderTarget *pRT)
	{
	        SendMessage(UI_ERASEBKGND, (uint_ptr)pRT);
			SendMessage(UI_PAINT, (uint_ptr)pRT);
			//SendMessage(UI_NCPAINT, (unsigned int)pRT);
			
	}
	void CUIWindow::_PaintRegion(IRenderTarget *pRT,unsigned int iZorderBegin,unsigned int iZorderEnd)
	{
	   if(!IsVisible(0))  //只在自己完全可见的情况下才绘制
			return;
		if(m_uZorder >= iZorderBegin
			&& m_uZorder < iZorderEnd )
		{
			_PaintClient(pRT);
		}

	    CUIWindow *pChild = GetWindow(GSW_FIRSTCHILD);
		while(pChild)
		{
			if(pChild->m_uZorder >= iZorderEnd) 
				break;
			if(pChild->m_uZorder< iZorderBegin)
			{//看整个分枝的zorder是不是在绘制范围内
				CUIWindow *pNextChild = pChild->GetWindow(GSW_NEXTSIBLING);
				if(pNextChild)
				{
					if(pNextChild->m_uZorder<=iZorderBegin)
					{
						pChild = pNextChild;
						continue;
					}
				}else
				{//最后一个节点时查看最后子窗口的zorder
					CUIWindow *pLastChild = pChild;
					while(pLastChild->GetChildrenCount())
					{
						pLastChild = pLastChild->GetWindow(GSW_LASTCHILD);
					}
					if(pLastChild->m_uZorder < iZorderBegin)
					{
						break;
					}
				}
			}
		
			pChild->_PaintRegion(pRT,iZorderBegin,iZorderEnd);
			
			pChild = pChild->GetWindow(GSW_NEXTSIBLING);
		}
		SendMessage(UI_NCPAINT, (uint_ptr)pRT);
	}

	void   CUIWindow::OnPrint(IRenderTarget *pRT)
	{
		UpdateLayout();
		_PaintRegion(pRT, 0, -1);
	}
	void CUIWindow::GetScaleSkin(ISkinObj * &pSkin,int nScale)
	{
	   if(!pSkin)
		   return;
		SStringA strName=pSkin->GetName();
		if(!strName.IsEmpty())
		{
			ISkinObj * pNewSkin =   GETSKIN(strName,nScale);
			if(pNewSkin) 
				pSkin = pNewSkin;
		}
	}
	
	///事件操作
    void CUIWindow::OnEvent(UITEvent& Event)
    {
   
    }
    CUIWindow*  CUIWindow::FindCtl(FINDCONTROLPROC Proc, void* pData,unsigned uFlags)
    {
           return Proc(this, pData);
	}

	
    CUIWindow *CUIWindow::GetParent() const
    {
        return m_pParent;
    }

	void CUIWindow::InsertChild(CUIWindow *pNewChild,CUIWindow *pInsertAfter)
	{
	   if(pNewChild->m_pParent == this) 
            return;

        pNewChild->SetContainer(GetContainer());
        pNewChild->m_pParent=this;

        pNewChild->m_pPrevSibling=pNewChild->m_pNextSibling=NULL;

        if(pInsertAfter==m_pLastChild) 
			pInsertAfter=UICWND_LAST;

        if(pInsertAfter==UICWND_LAST)
        {
            //insert window at head
            pNewChild->m_pPrevSibling=m_pLastChild;
            if(m_pLastChild) 
				m_pLastChild->m_pNextSibling=pNewChild;
            else 
				m_pFirstChild=pNewChild;
            m_pLastChild=pNewChild;
        }
        else if(pInsertAfter==UICWND_FIRST)
        {
            //insert window at tail
            pNewChild->m_pNextSibling=m_pFirstChild;
            if(m_pFirstChild) 
				m_pFirstChild->m_pPrevSibling=pNewChild;
            else 
				m_pLastChild=pNewChild;
            m_pFirstChild=pNewChild;
        }
        else
        {
            //insert window at middle
            assert(pInsertAfter->m_pParent == this);
            assert(m_pFirstChild && m_pLastChild);
            CUIWindow *pNext=pInsertAfter->m_pNextSibling;
            assert(pNext);
            pInsertAfter->m_pNextSibling=pNewChild;
            pNewChild->m_pPrevSibling=pInsertAfter;
            pNewChild->m_pNextSibling=pNext;
            pNext->m_pPrevSibling=pNewChild;
        }
        m_nChildrenCount++;
        
        //继承父窗口的disable状态
       // pNewChild->OnEnable(!IsDisabled(1),ParentEnable);

        //只在插入新控件时需要标记zorder失效,删除控件不需要标记
	//	if(GetContainer())
       // GetContainer()->MarkWndTreeZorderDirty();
	}

    bool  CUIWindow::RemoveChild(CUIWindow  *pChild)
	{
		if(!pChild)
			return 0;
	    if(this != pChild->GetParent()) 
            return 0;

         CUIWindow *pPrevSib=pChild->m_pPrevSibling;
         CUIWindow *pNextSib=pChild->m_pNextSibling;

        if(pPrevSib) 
            pPrevSib->m_pNextSibling=pNextSib;
        else 
            m_pFirstChild=pNextSib;

        if(pNextSib) 
            pNextSib->m_pPrevSibling=pPrevSib;
        else 
            m_pLastChild=pPrevSib;

		pChild->SetContainer(0);
        pChild->m_pParent=NULL;
        pChild->m_pNextSibling = NULL;
        pChild->m_pPrevSibling = NULL;
        m_nChildrenCount--;

        return 1;
	}
	int   CUIWindow::MoveChild(CUIWindow  *pChild,CUIWindow *pMoveAfter)
	{
	     if(!pChild || !pMoveAfter)
			 return 0;
	     if(!RemoveChild(pChild))
			 return 0;

		 InsertChild(pChild,pMoveAfter);

		 return 1;
	}
                //销毁一个子窗口
    bool  CUIWindow::DestroyChild(CUIWindow  *pChild)
	{
		this->GetContainer()->ClearHover(pChild);
	    if(this != pChild->GetParent()) 
			return 0;
        
        RemoveChild(pChild);
        pChild->Release();
		return 1;
	}
	IUIContainer*  CUIWindow::GetContainer()
	{
	   return m_pUIContainer;
	}
    void    CUIWindow::SetContainer(IUIContainer *pContainer)
	{
	   m_pUIContainer = pContainer;
	}

	int     CUIWindow::SendMessage(unsigned int Msg, uint_ptr wParam, int_ptr lParam,int *pbMsgHandled)
	{
	    long lResult = 0;

		//if ( Msg < WM_USER
		//	&& Msg != WM_DESTROY
		//	&& Msg != WM_CLOSE
		//	)
		//{
		//	TestMainThread();
		//}

		AddRef();

		UIWNDMSG msgCur= {Msg,wParam,lParam};

		UIWNDMSG *pOldMsg=m_pCurMsg;
		m_pCurMsg=&msgCur;

		int bOldMsgHandle=IsMsgHandled();//备分上一个消息的处理状态

		SetMsgHandled(0);

		ProcessUIWndMessage(Msg, wParam, lParam, lResult);

		if(pbMsgHandled) 
			*pbMsgHandled=IsMsgHandled();

		SetMsgHandled(bOldMsgHandle);//恢复上一个消息的处理状态

		m_pCurMsg=pOldMsg;
		Release();

		return lResult;
	}

	void CUIWindow::UpdateChildrenPosition()
	{
	   if(m_layoutDirty == dirty_self)
		{//当前窗口所有子窗口全部重新布局
		   ILayout *lay = GetLayout();
		   lay->LayoutChildren(this);

			CRect rt;
			this->GetClientRect(&rt);
			CUIWindow *pChild=GetWindow(GSW_FIRSTCHILD);
			while(pChild)
			{
				if(pChild->IsFloat())
				{

					 CSize  szView(0,0);
					pChild->OnUpdateFloatPosition(szView,rt);
					if(pChild->m_layoutDirty != dirty_clean)
					{
						pChild->UpdateChildrenPosition();
					}
				}
				pChild=pChild->GetWindow(GSW_NEXTSIBLING);
			}
		}else if(m_layoutDirty == dirty_child){
			//只有个别子窗口需要重新布局
			CUIWindow *pChild = GetNextLayoutChild(NULL);
			while(pChild)
			{
				if(pChild->m_layoutDirty != dirty_clean)
				{
					pChild->UpdateChildrenPosition();
				}
				pChild = GetNextLayoutChild(pChild);
			}
		}
	}
	void CUIWindow::OnUpdateFloatPosition(const  CSize&  szView,const CRect & rcParent)
	{
	
	}
	void CUIWindow::RequestRelayout()
	{
	       RequestRelayout(this,1);//此处bSourceResizable可以为任意值
	}
	void CUIWindow::RequestRelayout( CUIWindow *pSource,int bSourceResizable)
	{
	    if(bSourceResizable)
		{//源窗口大小发生变化,当前窗口的所有子窗口全部重新布局
			m_layoutDirty = dirty_self;
		}

		if(m_layoutDirty != dirty_self)
		{
			//需要检测当前窗口是不是内容自适应
			m_layoutDirty = (pSource == this || GetLayoutParam()->IsWrapContent(Any)) ? dirty_self:dirty_child;
			m_layoutDirty = dirty_self;
		}

		//
		CUIWindow *pParent = GetParent();
		if(pParent) 
			pParent->RequestRelayout(pSource,GetLayoutParam()->IsWrapContent(Any) || !IsDisplay());

		

	}
	void       CUIWindow::UpdateLayout()
	{
	    if(m_layoutDirty == dirty_clean) 
			return;
		UpdateChildrenPosition();
		m_layoutDirty = dirty_clean;
	}
	CUIWindow* CUIWindow::SetCapture()
	{
		return GetContainer()->OnSetUIWndCapture(this);
	}

	int CUIWindow::ReleaseCapture()
	{
		return GetContainer()->OnReleaseUIWndCapture();
	}
    int CUIWindow::OnSetCursor(const CPoint &pt)
	{  if(this->GetContainer())
		this->GetContainer()->GetIUIMessage()->UISetCursor("arrow");
	   return 1;
	 }

	int  CUIWindow::OnAttrName(const SStringA& strValue, int bLoading)
	{
	   m_strName = strValue;
	   return 1;
	}

	void CUIWindow::OnActivate(unsigned int  nState)
	{
	
	}
	void   CUIWindow::Invalidate()
	{
			CRect rt;
			GetWindowRect(&rt);
			GetContainer()->GetIUIMessage()->UIInvalidateRect(rt.left,rt.top,rt.right,rt.bottom);
	}
	void    CUIWindow::InvalidateRect(CRect& rt)
	{
	   GetContainer()->GetIUIMessage()->UIInvalidateRect(rt.left,rt.right,rt.top,rt.bottom);
	}
	void     CUIWindow::SetUserData(void* UserData)
	{
	     m_pUserData = UserData;
	}
	void*   CUIWindow::GetUserData()
	{
	   return  m_pUserData;
	}
	
    int  CUIWindow::OnRelayout(const CRect &rcWnd)
	{
		CRect rcWindow;
		GetWindowRect(&rcWindow);

	    if (rcWnd.EqualRect(&rcWindow) && m_layoutDirty == dirty_clean)
			return 0;

		if(!rcWnd.EqualRect(&rcWindow))
		{
			m_layoutDirty = dirty_self;

			//InvalidateRect(m_rcWindow);
			m_rcWindow = rcWnd;

			if(m_rcWindow.left>m_rcWindow.right) 
				m_rcWindow.right = m_rcWindow.left;
			if(m_rcWindow.top>m_rcWindow.bottom) 
				m_rcWindow.bottom = m_rcWindow.top;

			GetContainer()->GetIUIMessage()->UIInvalidateRect(m_rcWindow.left,m_rcWindow.right,m_rcWindow.top,m_rcWindow.bottom);
		

			//SendMessage(UI_NCCALCSIZE);//计算非客户区大小
		}


		UpdateChildrenPosition();   //更新子窗口位置

		CRect rcClient;
		GetClientRect(&rcClient);

		
		SendMessage(UI_SIZE,0,MAKELPARAM(rcClient.Width(),rcClient.Height()));
		
		m_layoutDirty = dirty_clean;
		return 1;

	}
	CUIWindow *CUIWindow::GetWindow(int uCode) const
	{
	    CUIWindow *pRet=NULL;
		switch(uCode)
		{
		case GSW_FIRSTCHILD:
			pRet=m_pFirstChild;
			break;
		case GSW_LASTCHILD:
			pRet=m_pLastChild;
			break;
		case GSW_PREVSIBLING:
			pRet=m_pPrevSibling;
			break;
		case GSW_NEXTSIBLING:
			pRet=m_pNextSibling;
			break;
		
		case GSW_PARENT:
			pRet=m_pParent;
			break;
		}
		return pRet;
	}
    CUIWndStyle& CUIWindow::GetStyle()
	{
		return m_style;
	}
	bool CUIWindow::SetLayoutParam(ILayoutParam * pLayoutParam)
	{
		CUIWindow *pParent = GetParent();
		if(!pParent->GetLayout()->IsParamAcceptable(pLayoutParam))
			return false;
		m_pLayoutParam = pLayoutParam;
		return true;
	}
	void CUIWindow::GetWindowRect(kkRect* pRect)  const
	{
	    assert(pRect);
		CRect rc = m_rcWindow;
		*pRect=rc;
	}
    void CUIWindow::GetClientRect(kkRect* pRect) const
	{
		assert(pRect);

		kkRect rcWindow;
		GetWindowRect(&rcWindow);

		CRect rc = rcWindow;
		rc.DeflateRect(m_style.GetMargin());
		*pRect=rc;
	}
	CUIWindow *CUIWindow::GetNextLayoutChild(CUIWindow *pCurChild)
	{
 	    CUIWindow *pRet = NULL;
		if(pCurChild == NULL)
		{
			pRet = GetWindow(GSW_FIRSTCHILD);
		}else
		{
			pRet = pCurChild->GetWindow(GSW_NEXTSIBLING);
		}

		if (pRet) {
			
			if (pRet && (pRet->IsFloat() || (!pRet->IsDisplay() && !pRet->IsVisible(0))))
			{
		
				return GetNextLayoutChild(pRet);
			}
		}
			
		return pRet;
	}
	CRect  CUIWindow::GetChildrenLayoutRect()
	{
	    CRect rcRet;
		GetClientRect(&rcRet);
		rcRet.DeflateRect(GetStyle().GetPadding());
		return rcRet;
	}
	int CUIWindow::InitFromXml   (pugi::xml_node xmlNode)
	{
	    if (xmlNode)
		{

			if(m_pLayoutParam) 
				m_pLayoutParam->Clear();

			CObject::InitFromXml(xmlNode);
		}

		//发送UI_CREATE消息
		if(0!=SendMessage(UI_CREATE))
		{
			if(m_pParent)    
				m_pParent->DestroyChild(this);
			return FALSE;
		}

		//创建子窗口
		CreateChildren(xmlNode);

		RequestRelayout();
		return 1;
	}

	CUIWindow*    CUIWindow::CreateChildren(char* strxml)
	{
		pugi::xml_document xmlDoc;
		if(!xmlDoc.load_buffer(strxml,strlen(strxml),pugi::parse_default,pugi::encoding_utf8)) 
			return NULL;
		CUIWindow* bLoaded=CreateChildren(xmlDoc);
		if(!bLoaded) 
			return NULL;
		else 
			return bLoaded;
	}
	const static char KLabelInclude[] = "include";
    CUIWindow*  CUIWindow::CreateChildren(pugi::xml_node xmlNode)
	{
		for (pugi::xml_node xmlChild=xmlNode.first_child(); xmlChild; xmlChild=xmlChild.next_sibling())
		{
			if(xmlChild.type() != pugi::node_element)
				continue;

			
			if(strcmp(xmlChild.name(),KLabelInclude)==0)
			{
				//在窗口布局中支持include标签
			/*	SStringA strSrc = xmlChild.attribute(L"src").value();
				pugi::xml_document xmlDoc;*/
				
				assert(0);
			}else if(!xmlChild.get_userdata())//通过userdata来标记一个节点是否可以忽略
			{
				CUIWindow *pChild = CUIApp::getSingleton().CreateUIByName((char*)xmlChild.name());
				if(pChild)
				{
					pChild->SetContainer(this->GetContainer());
					InsertChild(pChild);
					pChild-> InitAfter();
					pChild->InitFromXml(xmlChild);
					return pChild;
				}/**/
			}
		}
		return 0;
	}

	// Hittest children
	CUIWindow*  CUIWindow::WndFromPoint( kkPoint  ptHitTest, int bOnlyText)
	{
		if(!IsContainPoint(ptHitTest,0))
			return NULL;

		if(!IsContainPoint(ptHitTest,1))
			return this;//只在鼠标位于客户区时，才继续搜索子窗口

		

		CUIWindow  *pChild=GetWindow(GSW_LASTCHILD);
		while(pChild)
		{
			if (pChild->IsVisible(0) && !pChild->IsMsgTransparent())
			{
				CUIWindow  *swndChild = pChild->WndFromPoint(ptHitTest, bOnlyText);

				if (swndChild) 
					return swndChild;
			}

			pChild=pChild->GetWindow(GSW_PREVSIBLING);
		}

		return this;
	}

	int CUIWindow::IsDisabled(int bCheckParent /*= FALSE*/)
	{
		if(bCheckParent) 
			return m_dwState & WndState_Disable;
		else return m_bDisable;
	}

	int CUIWindow::IsContainPoint(const kkPoint &pt,int bClientOnly)
	{
	    int bRet = 0;
		if(bClientOnly){
			CRect rcClient;
			GetClientRect(&rcClient);
			bRet = rcClient.PtInRect(pt);
		}else{
			CRect rcWindow;
		    GetWindowRect(&rcWindow);
			bRet = rcWindow.PtInRect(pt);
		}
		
		return bRet;
	}
	int CUIWindow::OnNcHitTest(kkPoint pt)
	{
		CRect rcClient;
		GetClientRect(&rcClient);
		return !rcClient.PtInRect(pt);
	}

	const int KWnd_MaxSize  = 0x7fffff;
	CSize CUIWindow::GetDesiredSize(kkRect* pRcContainer)
	{
		CRect rcContainer;
		if(pRcContainer)
		{
			rcContainer = *pRcContainer;
			rcContainer.MoveToXY(0,0);
		}else
		{
			rcContainer.SetRect(0,0,KWnd_MaxSize,KWnd_MaxSize);
		}

		CSize szRet(KWnd_MaxSize,KWnd_MaxSize);
		if(GetLayoutParam()->IsSpecifiedSize(Horz))
		{//检查设置大小
			szRet.cx = GetLayoutParam()->GetSpecifiedSize(Horz).toPixelSize(GetScale());
		}else if(GetLayoutParam()->IsMatchParent(Horz))
		{
			szRet.cx = rcContainer.Width();
		}

		if(GetLayoutParam()->IsSpecifiedSize(Vert))
		{//检查设置大小
			szRet.cy = GetLayoutParam()->GetSpecifiedSize(Vert).toPixelSize(GetScale());
		}else if(GetLayoutParam()->IsMatchParent(Vert))
		{
			szRet.cy = rcContainer.Height();
		}

		if(szRet.cx !=KWnd_MaxSize && szRet.cy != KWnd_MaxSize) 
			return szRet;

		int nTestDrawMode =0;//DT_CENTER | DT_RIGHT | DT_VCENTER | DT_BOTTOM);

		CRect rcPadding = GetStyle().GetPadding();
		//计算文本大小
		CRect rcTest4Text (0,0,szRet.cx,szRet.cy);
		int nMaxWid = GetLayoutParam()->IsWrapContent(Horz)? m_nMaxWidth.toPixelSize(GetScale()):szRet.cx;
		if(nMaxWid == SIZE_WRAP_CONTENT) 
		{
			nMaxWid = KWnd_MaxSize;
		}
		else //if(nMaxWid >= SIZE_SPEC)
		{
			nMaxWid -= rcPadding.left + rcPadding.right;
		}
		rcTest4Text.right = max(nMaxWid,10);

	
		//计算子窗口大小
		CSize szChilds = GetLayout()->MeasureChildren(this,rcContainer.Width(),rcContainer.Height());

		
		CRect rcTest(0,0, max(szChilds.cx,rcTest4Text.right),max(szChilds.cy,rcTest4Text.bottom));

		rcTest.InflateRect(&m_style.GetMargin());
		rcTest.InflateRect(&rcPadding);

		if(GetLayoutParam()->IsWrapContent(Horz)) 
			szRet.cx = rcTest.Width();
		if(GetLayoutParam()->IsWrapContent(Vert)) 
			szRet.cy = rcTest.Height();

		return szRet;
	}
	CSize CUIWindow::GetDesiredSize(int nParentWid , int nParentHei )
	{
	    bool isParentHorzWrapContent = nParentWid<0;
		bool isParentVertWrapContent = nParentHei<0;

		nParentWid = abs(nParentWid);
		nParentHei = abs(nParentHei);

		ILayoutParam * pLayoutParam = GetLayoutParam();
		bool bSaveHorz = isParentHorzWrapContent && pLayoutParam->IsMatchParent(Horz);
		bool bSaveVert = isParentVertWrapContent && pLayoutParam->IsMatchParent(Vert);
		if(bSaveHorz)
			pLayoutParam->SetWrapContent(Horz);

		if(bSaveVert)
			pLayoutParam->SetWrapContent(Vert);

		CRect rcContainer(0,0,nParentWid,nParentHei);
		CSize szRet = GetDesiredSize(&rcContainer);

		if(bSaveHorz) 
			pLayoutParam->SetMatchParent(Horz);
		if(bSaveVert) 
			pLayoutParam->SetMatchParent(Vert);

		return szRet;
	}
	void   CUIWindow::Move(kkRect* prect)
	{
	    if(prect)
		{
			m_bFloat = 1;//使用Move后，程序不再自动计算窗口坐标
			OnRelayout(prect);
		}else if(GetParent())
		{
			//恢复自动计算位置
			m_bFloat =0;
			//重新计算自己及兄弟窗口的坐标
			RequestRelayout();
		}
	}
	CUIWindow*    CUIWindow::FindChildByName(const char* pszName , int nDeep)
	{
	   if(!pszName || nDeep ==0) 
		   return NULL;

		CUIWindow *pChild = GetWindow(GSW_FIRSTCHILD);
		while(pChild)
		{
			if (pChild->m_strName == pszName)
				return pChild;
			pChild = pChild->GetWindow(GSW_NEXTSIBLING);
		}

		if(nDeep>0) 
			nDeep--;
		if(nDeep==0) 
			return NULL;

		pChild = GetWindow(GSW_FIRSTCHILD);
		while(pChild)
		{
			CUIWindow*  pChildFind=pChild->FindChildByName(pszName,nDeep);
			if(pChildFind) 
				return pChildFind;
			pChild = pChild->GetWindow(GSW_NEXTSIBLING);
		}

		return NULL;
	}
}