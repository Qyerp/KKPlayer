#include <jni.h>
#include <pthread.h>
#include <string>
#include <stdlib.h>
#include <malloc.h>
#include "AndPlayerStateNotifyMgr.h"
#include "AndKKPlayerUI.h"
#include "AndObjMgr.h"
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include "../AudioToPcm.h"
#include <map>
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

//状态通知管理器
CAndPlayerStateNotifyMgr*  G_pPlayerStateNotifyMgr=0;
//全局对象管理器
CAndObjMgr*                 G_pAndObjMgr = 0;


void* ConvertThread(void *param)
{

}

jobject GenPlayerCtx(JNIEnv *pEv)
{
    const char *stClassPath = "com/ic70/kkplayer/sdk/CKKPlayerCtx";
    const jclass ViewSurfaceClass = pEv->FindClass(stClassPath);
    if (ViewSurfaceClass == 0) {
       LOGE("FindClass (%s) failed", stClassPath);
    }

    const jmethodID constructor = pEv->GetMethodID( ViewSurfaceClass, "<init>", "()V" );
    if (constructor == 0) {
      LOGE("GetMethonID(<init>) CKKPlayerCtx");
    }
    jobject  obj = pEv->NewObject(ViewSurfaceClass, constructor);
    if (obj == 0) {
       LOGE("NewObject() CKKPlayerCtx failed");
    }
	
	jobject CtxObj = pEv->NewGlobalRef(obj);
    if (CtxObj == 0) {
       LOGE("NewGlobalRef() CtxObj failed");
    }

    //Now that we have a globalRef, we can free the localRef
    pEv->DeleteLocalRef(obj);
	return CtxObj;
}

CAndKKPlayerUI*  GetPlayer(JNIEnv *pEv, jobject playerObj)
{ 
	jclass clazz =pEv->GetObjectClass(playerObj);
    jfieldID id_PlayerIns = pEv->GetFieldID(clazz, "PlayerIns", "[B");
	jfieldID id_PlayerId = pEv->GetFieldID(clazz, "PlayerId", "I");
	jobject PlayerIns=pEv->GetObjectField(playerObj,id_PlayerIns);
	jbyte bys[64] = {};
	pEv->GetByteArrayRegion((jbyteArray)PlayerIns,0,16,bys);
	CAndKKPlayerUI* Ins = 0;//(CAndKKPlayerUI*)pEv->GetIntField(playerObj, id_PlayerId);
	memcpy(&Ins,bys,sizeof(CAndKKPlayerUI*));
	//LOGE("Ins2:%u \n",Ins);
	return  Ins;
}	
JNIEXPORT void JNICALL GIni(JNIEnv *pEv, jobject instance)
{
	if (G_pPlayerStateNotifyMgr == 0) {
		G_pPlayerStateNotifyMgr = new CAndPlayerStateNotifyMgr();
		if(G_pPlayerStateNotifyMgr){
		   G_pPlayerStateNotifyMgr->Start();
		}
	}
	if (G_pAndObjMgr == 0) {
		G_pAndObjMgr = new CAndObjMgr();
	}
	
}
///播放器状态通知
JNIEXPORT void    JNICALL SetIPlayerStateNotify(JNIEnv *env, jobject instance,jobject player,jobject notify)
{ 
   //JniFindJavaClass(env);
   LOGE("G_pPlayerStateNotifyMgr %d\n",G_pPlayerStateNotifyMgr);
   if(G_pPlayerStateNotifyMgr==0){
     G_pPlayerStateNotifyMgr = new CAndPlayerStateNotifyMgr();
     G_pPlayerStateNotifyMgr->Start();
   }
   
   CAndKKPlayerUI *pKKUI = GetPlayer(env, player);
   G_pPlayerStateNotifyMgr->SetPlayerNotify(env,player,pKKUI,notify);
}

static int G_PlayerId=100;
/***********初始一个KKUI**********/
JNIEXPORT jobject    JNICALL IniKK(JNIEnv *pEv, jobject p, jint Render)
{
   
    const CAndKKPlayerUI *pKKUI = new CAndKKPlayerUI(Render);
	//LOGE("Nint1 \n");
    jobject playerObj=GenPlayerCtx(pEv);
	//LOGE("Nint2 %d\n", playerObj);
	jclass clazz =pEv->GetObjectClass(playerObj);
	jfieldID id_PlayerIns = pEv->GetFieldID(clazz, "PlayerIns", "[B");
	jfieldID id_PlayerId = pEv->GetFieldID(clazz, "PlayerId", "I");
	if(id_PlayerIns==0){

		LOGE("can't PlayerIns \n");
	}
	if(id_PlayerId==0){

		LOGE("can't id_PlayerId \n");
	}
	jobject PlayerIns=pEv->GetObjectField(playerObj, id_PlayerIns);
	
	if(PlayerIns==0){

		LOGE("can't PlayerIns \n");
	}
	
	jbyte bys[16] = {};
	CAndKKPlayerUI* Ins = 0;
	int PtrLen = sizeof(CAndKKPlayerUI*);
	memcpy(bys, &pKKUI, PtrLen);
	pEv->SetByteArrayRegion((jbyteArray)PlayerIns,0,16,bys);
	G_PlayerId++;
	pEv->SetIntField(playerObj,id_PlayerId, G_PlayerId);
	unsigned int ll = (unsigned int)pKKUI;
	LOGE("Player %u",ll);

//	pEv->GetByteArrayRegion((jbyteArray)PlayerIns, 0, 16, bys);

	memcpy(&Ins, bys, sizeof(CAndKKPlayerUI*));
	LOGE("Ins1:%u \n", Ins);
    return playerObj;
}

JNIEXPORT jint    JNICALL IniGl(JNIEnv *env, jobject instance, jobject player)
{
    CAndKKPlayerUI *pKKUI = GetPlayer(env, player);
    jint ret=pKKUI->Init();
	pKKUI->SetSurfaceTexture(env);
	return ret;
}

JNIEXPORT void    JNICALL SetDecoderMethod(JNIEnv *env, jobject instance,  jobject player, jint method)
{
	CAndKKPlayerUI *pKKUI = GetPlayer(env, player);
	if(pKKUI!=0){
		pKKUI->SetDecoderMethod(method);
	}
	else {
		LOGE("pKKUI=0");
	}
}

JNIEXPORT jobject JNICALL GetSurfaceTexture(JNIEnv * env, jobject instance, jobject player)
{
	CAndKKPlayerUI *pKKUI = GetPlayer(env, player);
	if(pKKUI!=0){
		return pKKUI->GetSurfaceTexture();
	}
	return 0;
}

JNIEXPORT void    JNICALL OnSurfaceTextureFrameAailable(JNIEnv * env, jobject instance, jobject player)
{
	CAndKKPlayerUI *pKKUI =  GetPlayer(env, player);
	if(pKKUI!=0){
		pKKUI->OnSurfaceTextureFrameAailable();
	}
}

JNIEXPORT void    JNICALL SetKeepRatio(JNIEnv *env, jobject instance, jobject player, jint KeepRatio)
{
	CAndKKPlayerUI *pKKUI = GetPlayer(env, player);
    pKKUI->SetKeepRatio(KeepRatio);
}

JNIEXPORT jint JNICALL OnSize(JNIEnv *env, jobject instance, jobject player,jint w, jint h) {
    CAndKKPlayerUI *pKKUI=GetPlayer(env, player);
    return pKKUI->OnSize(w,h);
}

JNIEXPORT jint JNICALL GlRender(JNIEnv *env, jobject instance, jobject player) {
    CAndKKPlayerUI *pKKUI=GetPlayer(env, player);
    pKKUI->renderFrame(0);
    return 1;
}


JNIEXPORT void JNICALL DelKK(JNIEnv *env, jobject instance,jobject player)
{
    
       
       CAndKKPlayerUI *pKKUI=GetPlayer(env, player);
	   if(G_pPlayerStateNotifyMgr)
	      G_pPlayerStateNotifyMgr->RemovePlayerNotify(env,pKKUI);
       delete pKKUI;
       LOGE("DelKK\n");
    
}
JNIEXPORT jint JNICALL KKOpenMedia(JNIEnv *env, jobject instance, jstring str_,jobject player)
{
    //char str[] ="rtmp://121.42.14.63/live/livestream";
    const char *str=env->GetStringUTFChars(str_, 0);

    CAndKKPlayerUI *pKKUI=GetPlayer(env, player);
	LOGE("xx1 \n");
    int l= pKKUI->OpenMedia((char*)str);
	LOGE("xx2 \n");
    env->ReleaseStringUTFChars(str_, str);
    return l;
}
JNIEXPORT jint JNICALL KKIsNeedReConnect(JNIEnv *env, jobject instance,jobject player)
{
    CAndKKPlayerUI *pKKUI=GetPlayer(env, player);
    if(pKKUI->GetNeedReconnect())
        return 1;
    return 0;
}

JNIEXPORT void JNICALL Pause(JNIEnv *env, jobject instance, jobject player,jint pause) {

       CAndKKPlayerUI *pKKUI=GetPlayer(env, player);
       pKKUI->Pause(pause);
}
JNIEXPORT void JNICALL Seek(JNIEnv *env, jobject instance, jobject player, jint value)
{
	
    CAndKKPlayerUI *pKKUI=GetPlayer(env, player);
    pKKUI->Seek(value);
	
}
JNIEXPORT void JNICALL KKCloseMedia(JNIEnv *env,jobject instance, jobject player)
{
    CAndKKPlayerUI *pKKUI=GetPlayer(env, player);
    pKKUI->CloseMedia();
}
JNIEXPORT jint JNICALL KKGetPlayerState(JNIEnv *env, jobject instance,jobject player)
{
    CAndKKPlayerUI *pKKUI=GetPlayer(env, player);
    return  pKKUI->GetPlayerState();
	return -1;
}
JNIEXPORT jint JNICALL KKIsReady(JNIEnv *env, jobject instance,jobject player)
{

    CAndKKPlayerUI *pKKUI=GetPlayer(env, player);
    return  pKKUI->GetIsReady();
	
	return -1;
}
JNIEXPORT jint JNICALL KKGetRealtime(JNIEnv *env, jobject instance,jobject player)
{
    CAndKKPlayerUI *pKKUI=GetPlayer(env, player);
    return  pKKUI->GetRealtime();	
	return -1;
}
JNIEXPORT jint JNICALL KKGetRealtimeDelay(JNIEnv *env, jobject instance,jobject player)
{
    CAndKKPlayerUI *pKKUI=GetPlayer(env, player);
    return  pKKUI->GetRealtimeDelay();
	return -1;
}
JNIEXPORT void JNICALL KKForceFlushQue(JNIEnv *env, jobject instance,jobject player)
{
        CAndKKPlayerUI *pKKUI=GetPlayer(env, player);
        pKKUI->ForceFlushQue();
}

JNIEXPORT jint JNICALL KKSetMinRealtimeDelay(JNIEnv *env, jobject instance,jobject player, jint value)
{
    CAndKKPlayerUI *pKKUI=GetPlayer(env, player);
    pKKUI->SetMinRealtimeDelay(value);
	return -1;
}
jstring cToJstringutf(JNIEnv* env, const char* pat) {  
    jclass strClass = env->FindClass("java/lang/String");  
    jmethodID ctorID = env->GetMethodID(strClass, "<init>",  "([BLjava/lang/String;)V");  
    jbyteArray bytes = env->NewByteArray( strlen(pat));  
    env->SetByteArrayRegion( bytes, 0, strlen(pat), (jbyte*) pat);  
    jstring encoding = env->NewStringUTF("utf-8");  
    return (jstring)env->NewObject( strClass, ctorID, bytes, encoding);  
}
JNIEXPORT jint JNICALL GetKKMediaInfo(JNIEnv *env, jobject instance,jobject player, jobject jInfo)
{
	int ret=0;
	CAndKKPlayerUI *pKKUI=GetPlayer(env, player);
	if(pKKUI){
        MEDIA_INFO info=pKKUI->GetMediaInfo();
        jclass Infocls = env->GetObjectClass(jInfo); 
        jfieldID jfsAVRes = env->GetFieldID(Infocls, "AVRes", "Ljava/lang/String;");
        info.AVRes[1023]=0;		
		if(strlen(info.AVRes)>0)
		env->SetObjectField(jInfo, jfsAVRes, env->NewStringUTF(info.AVRes));
      
	    
	    ///视频解码器名称
	    jfieldID jfVideoCodecname = env->GetFieldID(Infocls, "VideoCodecname", "Ljava/lang/String;");
        if(strlen(info.videoinfo.codecname)>0)		
		env->SetObjectField(jInfo, jfVideoCodecname, cToJstringutf(env,info.videoinfo.codecname));
	
	    ///平均码率
	    jfieldID jfVideoBitrate = env->GetFieldID(Infocls, "VideoBitrate", "I");		
        env->SetIntField(jInfo, jfVideoBitrate, info.videoinfo.bitrate);		
		
        ///平均帧率
	    jfieldID jfVideoFramerate = env->GetFieldID(Infocls, "VideoFramerate", "I");		
        env->SetIntField(jInfo, jfVideoFramerate, info.videoinfo.framerate);	
   
   
        ///音频解码器名称
        jfieldID jfAudioCodecname = env->GetFieldID(Infocls, "AudioCodecname", "Ljava/lang/String;");
        if(strlen(info.audioinfo.codecname)>0)		
		env->SetObjectField(jInfo, jfAudioCodecname, cToJstringutf(env,info.audioinfo.codecname));
	    
		///音频采样率
	    jfieldID jfAudioSample_rate = env->GetFieldID(Infocls, "AudioSample_rate", "I");		
        env->SetIntField(jInfo, jfAudioSample_rate, info.audioinfo.sample_rate);	
       
	    ///声 道 数
	    jfieldID jfAudioChannels = env->GetFieldID(Infocls, "AudioChannels", "I");		
        env->SetIntField(jInfo, jfAudioChannels, info.audioinfo.channels);	
        
		
		///文件大小
        jfieldID jfiFileSize = env->GetFieldID(Infocls, "FileSize", "I");		
        env->SetIntField(jInfo, jfiFileSize, info.FileSize);		
		
		///当前播放时间
		jfieldID jfiCurTime = env->GetFieldID(Infocls, "CurTime", "I");	
		env->SetIntField(jInfo, jfiCurTime, (int)info.fCurTime);
		
		///总时间
		jfieldID jfiTotalTime = env->GetFieldID(Infocls, "TotalTime", "I");	
		env->SetIntField(jInfo, jfiTotalTime, info.TotalTime); 
		
		///序列号
		jfieldID jfiSerial = env->GetFieldID(Infocls, "Serial", "I");	
		env->SetIntField(jInfo, jfiSerial, info.serial);
		
		///序列号1
		jfieldID jfiSerial1 = env->GetFieldID(Infocls, "Serial1", "I");	
		env->SetIntField(jInfo, jfiSerial1, info.serial1);
		
		jfieldID jfiOpen = env->GetFieldID(Infocls, "Open", "I");	
		env->SetIntField(jInfo, jfiOpen, info.Open);
		  
		jfieldID jfiKKState = env->GetFieldID(Infocls, "KKState", "I");	
		env->SetIntField(jInfo, jfiKKState, info.KKState);
		
		jfieldID jfiSegId = env->GetFieldID(Infocls, "SegId", "I");	
		env->SetIntField(jInfo, jfiSegId, info.SegId);
		//return 1;
		jfieldID jfsSpeedInfo= env->GetFieldID(Infocls, "SpeedInfo", "Ljava/lang/String;");	
		
		info.SpeedInfo[1023]=0;
	    if(strlen(info.SpeedInfo)>0)	
		    env->SetObjectField(jInfo, jfsSpeedInfo, env->NewStringUTF(info.SpeedInfo));
		if(info.serial1!=-1)
			ret=1;
		return ret;
    }
	return 0;
}


//音频转pcm
JNIEXPORT jint JNICALL CreateAudioToPcm(JNIEnv *env, jobject instance)
{
	return  G_pAndObjMgr->CreateObj(2);
	
}

JNIEXPORT jint JNICALL SetAudioToPcmCall(JNIEnv *env, jobject instance, jint obj,jobject icall)
{
	//JniFindJavaClass(env);
	return G_pAndObjMgr->SetObjCall(env, obj, icall);
}
JNIEXPORT void JNICALL AudioToPcmSetVol(JNIEnv *env, jobject instance,jint obj,int vol)
{
	int ret = -1;
	CAudioToPcm* pcm = 0;
	
	G_pAndObjMgr->Lock();
	KKObjInfo* ObjInfo = G_pAndObjMgr->GetObjInfo(obj);
	if (ObjInfo) {
		//添加引用
		G_pAndObjMgr->AddRef(obj);
	}
	G_pAndObjMgr->Unlock();
	if (ObjInfo) {
		ret = 0;
		pcm = (CAudioToPcm*)ObjInfo->Obj;
		pcm->SetVol(vol);
		//减少使用
		G_pAndObjMgr->Lock();
		G_pAndObjMgr->ReduceRef(obj);
		G_pAndObjMgr->Unlock();
	}else{
		LOGE("can't obj \n");
	}
	
}
JNIEXPORT jint JNICALL AudioToPcmOpenFile(JNIEnv *env, jobject instance,jint obj, jstring Filepath,jint  ch_layout,jint  sample_rate)
{
	int ret = -1;
	CAudioToPcm* pcm = 0;
	
	G_pAndObjMgr->Lock();
	KKObjInfo* ObjInfo = G_pAndObjMgr->GetObjInfo(obj);
	if (ObjInfo) {
		//添加引用
		G_pAndObjMgr->AddRef(obj);
	}
	G_pAndObjMgr->Unlock();
	if (ObjInfo) {
		ret = 0;
		pcm = (CAudioToPcm*)ObjInfo->Obj;
		const char *str = env->GetStringUTFChars(Filepath, 0);
		ret = pcm->AudioToPcm(env,str, ch_layout,sample_rate);
		LOGE(str);
		LOGE("\n");
		env->ReleaseStringUTFChars(Filepath, str);

		//减少使用
		G_pAndObjMgr->Lock();
		G_pAndObjMgr->ReduceRef(obj);
		G_pAndObjMgr->Unlock();
	}else{
		LOGE("can't obj \n");
	}
	
	return ret;
}

JNIEXPORT jint JNICALL AudioToPcmPause(JNIEnv *env, jobject instance,jint obj,jint pause)
{
	int ret = -1;
	CAudioToPcm* pcm = 0;

	G_pAndObjMgr->Lock();
	KKObjInfo* ObjInfo = G_pAndObjMgr->GetObjInfo(obj);
	if (ObjInfo) {
		//添加引用
		G_pAndObjMgr->AddRef(obj);
	}
	G_pAndObjMgr->Unlock();
	if (ObjInfo) {
		CAudioToPcm* pcm = (CAudioToPcm*)ObjInfo->Obj;
		pcm->PlayPause(pause);
		//减少使用
		G_pAndObjMgr->Lock();
		G_pAndObjMgr->ReduceRef(obj);
		G_pAndObjMgr->Unlock();
	}else{
		
		 LOGE("can't obj \n");
	}

	return ret;
}
JNIEXPORT jint JNICALL GetAudioToPcmState(JNIEnv *env, jobject instance,jint obj)
{
	int ret = -1;
	CAudioToPcm* pcm = 0;

	G_pAndObjMgr->Lock();
	KKObjInfo* ObjInfo = G_pAndObjMgr->GetObjInfo(obj);
	if (ObjInfo) {
		//添加引用
		G_pAndObjMgr->AddRef(obj);
	}
	G_pAndObjMgr->Unlock();
	if (ObjInfo) {
		CAudioToPcm* pcm = (CAudioToPcm*)ObjInfo->Obj;
		ret = pcm->GetPlayState();
		//减少使用
		G_pAndObjMgr->Lock();
		G_pAndObjMgr->ReduceRef(obj);
		G_pAndObjMgr->Unlock();
	}

	return ret;
}

JNIEXPORT jint JNICALL DelAudioToPcm(JNIEnv *env, jobject instance,jint obj)
{
	int ret= G_pAndObjMgr->DeleteObj(env, obj);
	return ret;
}

static JNINativeMethod gMethods[] = {
	{ "GIni",                          "()V",                    (void*)GIni },
	{ "IniKK",                         "(I)Lcom/ic70/kkplayer/sdk/CKKPlayerCtx;",  (void*)IniKK },
	{ "SetIPlayerStateNotify",         "(Lcom/ic70/kkplayer/sdk/CKKPlayerCtx;Lcom/ic70/kkplayer/sdk/IKKPlayerErrNotify;)V", (void*)SetIPlayerStateNotify },
	{ "IniGl",                         "(Lcom/ic70/kkplayer/sdk/CKKPlayerCtx;)I",  (void*)IniGl },
	{ "SetDecoderMethod",              "(Lcom/ic70/kkplayer/sdk/CKKPlayerCtx;I)V", (void*)SetDecoderMethod },
	{ "GetSurfaceTexture",             "(Lcom/ic70/kkplayer/sdk/CKKPlayerCtx;)Landroid/graphics/SurfaceTexture;", (void*)GetSurfaceTexture },
	{ "OnSurfaceTextureFrameAailable", "(Lcom/ic70/kkplayer/sdk/CKKPlayerCtx;)V",   (void*)OnSurfaceTextureFrameAailable },
	{ "SetKeepRatio",                  "(Lcom/ic70/kkplayer/sdk/CKKPlayerCtx;I)V",  (void*)SetKeepRatio },
	{ "OnSize",                        "(Lcom/ic70/kkplayer/sdk/CKKPlayerCtx;II)I", (void*)OnSize },
	{ "GlRender",                      "(Lcom/ic70/kkplayer/sdk/CKKPlayerCtx;)I",   (void*)GlRender},
	{ "DelKK",                         "(Lcom/ic70/kkplayer/sdk/CKKPlayerCtx;)V",   (void*)DelKK },
	{ "KKOpenMedia",                   "(Ljava/lang/String;Lcom/ic70/kkplayer/sdk/CKKPlayerCtx;)I", (void*)KKOpenMedia },
	{ "KKIsNeedReConnect",             "(Lcom/ic70/kkplayer/sdk/CKKPlayerCtx;)I",  (void*)KKIsNeedReConnect },
	{ "Pause",                         "(Lcom/ic70/kkplayer/sdk/CKKPlayerCtx;I)V",   (void*)Pause },
	{ "Seek",                          "(Lcom/ic70/kkplayer/sdk/CKKPlayerCtx;I)V",  (void*)Seek },
	{ "KKCloseMedia" ,                  "(Lcom/ic70/kkplayer/sdk/CKKPlayerCtx;)I",   (void*)KKCloseMedia},
	{ "KKGetPlayerState",              "(Lcom/ic70/kkplayer/sdk/CKKPlayerCtx;)I",   (void*)KKGetPlayerState},
	{ "KKIsReady",                     "(Lcom/ic70/kkplayer/sdk/CKKPlayerCtx;)I",   (void*)KKIsReady},
	{ "KKGetRealtime",                 "(Lcom/ic70/kkplayer/sdk/CKKPlayerCtx;)I",   (void*)KKGetRealtime},
	{ "KKGetRealtimeDelay",            "(Lcom/ic70/kkplayer/sdk/CKKPlayerCtx;)I",   (void*)KKGetRealtimeDelay},
	{ "KKForceFlushQue",               "(Lcom/ic70/kkplayer/sdk/CKKPlayerCtx;)V",   (void*)KKForceFlushQue},
	{ "KKSetMinRealtimeDelay",         "(Lcom/ic70/kkplayer/sdk/CKKPlayerCtx;I)I",  (void*)KKSetMinRealtimeDelay},
    { "GetKKMediaInfo",                "(Lcom/ic70/kkplayer/sdk/CKKPlayerCtx;Lcom/ic70/kkplayer/sdk/CkkMediaInfo;)I",(void*)GetKKMediaInfo },
	
	{ "CreateAudioToPcm","()I",(void*)CreateAudioToPcm},
	{ "SetAudioToPcmCall","(ILcom/ic70/kkplayer/sdk/IKKAudioToPcm;)I",(void*)SetAudioToPcmCall},
	{ "AudioToPcmSetVol","(II)V",(void*)AudioToPcmSetVol},
	
    { "AudioToPcmOpenFile","(ILjava/lang/String;II)I",(void*)AudioToPcmOpenFile},
	{ "AudioToPcmPause","(II)I",(void*)AudioToPcmPause},
	{ "GetAudioToPcmState","(I)I",(void*)GetAudioToPcmState},
	{ "DelAudioToPcm","(I)I",(void*)DelAudioToPcm}
};
///Java虚拟机变量
static JavaVM *        kk_Gjava_vm=NULL;

static  pthread_mutex_t kk_Gvm_lock = PTHREAD_MUTEX_INITIALIZER;
extern "C"
{
int av_jni_set_java_vm(void *vm, void *log_ctx);
}
/**
* JNI 库模块加载事件处理接口。可以做一些函数的加载
*/
JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM * java_vm, void * reserved)
{
	kk_Gjava_vm=java_vm;
	av_jni_set_java_vm(java_vm, NULL);

    JNIEnv *env=0;
	if ( java_vm->GetEnv( (void**) &env, JNI_VERSION_1_6) != JNI_OK){
		 LOGE("JNI_OnLoad err \n");
		 return -1;
	}
    LOGE("JNI_OnLoad %d \n", env );
	
	jclass clazz = env->FindClass("com/ic70/kkplayer/sdk/CJniKKPlayer");
	if (clazz == NULL) {
		LOGE("FindClass err \n");
		return JNI_FALSE;
	}
	if (env->RegisterNatives(clazz, gMethods,
		sizeof(gMethods)/sizeof((gMethods)[0])) < 0) {
		LOGE("RegisterNatives err \n");
		return JNI_FALSE;
	}/**/
 
	return JNI_VERSION_1_6;
}
/**
* JNI 库模块卸载事件处理接口。
*/
JNIEXPORT void JNICALL JNI_OnUnload(JavaVM * java_vm, void * reserved)
{

    if(G_pPlayerStateNotifyMgr){
       G_pPlayerStateNotifyMgr->Stop();
       delete G_pPlayerStateNotifyMgr;
       G_pPlayerStateNotifyMgr = 0;
    }
	kk_Gjava_vm=0;
}


///线程附加到vm中
void* kk_jni_attach_env()
{
    int ret = 0;
    JNIEnv *env = NULL;

	
    pthread_mutex_lock(&kk_Gvm_lock);
    JavaVM* java_vm=kk_Gjava_vm;
    if (!java_vm) {
         LOGE("No Java virtual machine has been registered\n");
         return NULL;
    }

    ret = java_vm->GetEnv((void **)&env, JNI_VERSION_1_6);
    switch(ret)
	{
		case JNI_EDETACHED:
			if (java_vm->AttachCurrentThread(&env, NULL) != 0) {
				LOGE("Failed to attach the JNI environment to the current thread\n");
				env = NULL;
			}
			break;
		case JNI_OK:
			break;
		case JNI_EVERSION:
			LOGE("The specified JNI version is not supported\n");
			break;
		default:
			LOGE("Failed to get the JNI environment attached to this thread \n");
			break;
    }

	if(env!=NULL){
		LOGE("attached to this thread Ok \n");
	}
	 pthread_mutex_unlock(&kk_Gvm_lock);
    return env;
}
int kk_jni_detach_env()
{
    
	pthread_mutex_lock(&kk_Gvm_lock);
	JavaVM* java_vm=kk_Gjava_vm;
    if (java_vm == NULL) {
        LOGE("No Java virtual machine has been registered\n");
		pthread_mutex_unlock(&kk_Gvm_lock);
        return -1;
    }

    int ret=java_vm->DetachCurrentThread();
	pthread_mutex_unlock(&kk_Gvm_lock);
	LOGE("detached to this thread Ok \n");
	return ret;
}
