#ifndef KKUI_MsgLoop_h_
#define KKUI_MsgLoop_h_

#ifndef WM_SYSTIMER
#define WM_SYSTIMER 0x0118   //(caret blink)
#endif//WM_SYSTIMER
#include <vector>
namespace KKUI
{
    
   template<class T>
   BOOL RemoveElementFromArray(std::vector<T> &arr, T ele)
    {
        for(size_t i=0;i<arr.size();i++)
        {
            if(arr[i] == ele)
            {
               // arr.RemoveAt(i);
                return TRUE;
            }
        }
        return FALSE;
    }
    
    struct IMessageFilter
    {
        virtual BOOL PreTranslateMessage(MSG* pMsg) = 0;
    };

    ///////////////////////////////////////////////////////////////////////////////
    // CIdleHandler - Interface for idle processing

    struct IIdleHandler
    {
        virtual BOOL OnIdle() = 0;
    };

    class SMessageLoop
    {
		public:
			SMessageLoop():m_bRunning(FALSE)
			{
	        
			}
			virtual ~SMessageLoop(){}
	        
			std::vector<IMessageFilter*> m_aMsgFilter;
			std::vector<IIdleHandler*> m_aIdleHandler;
			MSG m_msg;
	                
			// Message filter operations
			BOOL AddMessageFilter(IMessageFilter* pMessageFilter);

			BOOL RemoveMessageFilter(IMessageFilter* pMessageFilter);

			// Idle handler operations
			BOOL AddIdleHandler(IIdleHandler* pIdleHandler);

			BOOL RemoveIdleHandler(IIdleHandler* pIdleHandler);

			static BOOL IsIdleMessage(MSG* pMsg);

			// Overrideables
			// Override to change message filtering
			virtual BOOL PreTranslateMessage(MSG* pMsg);

			// override to change idle processing
			virtual BOOL OnIdle(int /*nIdleCount*/);
	        
			virtual int Run();
	        
			virtual BOOL IsRunning() const{return m_bRunning;}
	        
			void OnMsg(LPMSG pMsg);
	        
		protected:
			BOOL m_bRunning;
    };


}
#endif