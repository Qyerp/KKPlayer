﻿#include "UIYUVGridPanel.h"
#include "UIYUV.h"
namespace KKUI
{
	CUIYUVGridPanel::CUIYUVGridPanel():m_nRow(3),m_nCol(3),m_pFont(0)
	{
	
	}
	CUIYUVGridPanel::~CUIYUVGridPanel()
	{
	
	}
	int CUIYUVGridPanel::LoadYUV(int row,int col,kkPicInfo *info,wchar_t* strTxt)
	{
	    int id= (row-1)*m_nCol + (col-1);

		int i=0;
		CUIWindow *pChild=GetWindow(GSW_FIRSTCHILD);
	    while(pChild){
		  if(i==id){
             CUIYUV* yuv = ( CUIYUV*)pChild;
             if(strTxt)
			    yuv->SetTxt(strTxt);
			 yuv->LoadYUV(info);
		     return 1;
		  }
		   pChild=pChild->GetWindow(GSW_NEXTSIBLING);
		   i++;
	   }
	   Invalidate();
	   return 0;
	}
	int  CUIYUVGridPanel::SetItemTxt(int row,int col,std::wstring strTxt)
	{
	    int id= (row-1)*m_nCol + (col-1);

		int i=0;
		CUIWindow *pChild=GetWindow(GSW_FIRSTCHILD);
	    while(pChild){

		  if(i==id)
		  {
             CUIYUV* yuv = ( CUIYUV*)pChild;
			 yuv->SetTxt(strTxt);
		     return 1;
		  }
		   pChild=pChild->GetWindow(GSW_NEXTSIBLING);
		   i++;
	   }
		return 0;
	}
	int   CUIYUVGridPanel::SetItemUserData (int row,int col,void* userData)
	{
	    int id= (row-1)*m_nCol + (col-1);

		int i=0;
		CUIWindow *pChild=GetWindow(GSW_FIRSTCHILD);
	    while(pChild){

		  if(i==id)
		  {
             CUIYUV* yuv = ( CUIYUV*)pChild;
			 yuv->SetUserData(userData);
		     return 1;
		  }
		   pChild=pChild->GetWindow(GSW_NEXTSIBLING);
		   i++;
	   }
		return 0;
	}
	void          CUIYUVGridPanel::ClearItemData(int nRow,int nCol,unsigned int cr)
	{
	    int id= (nRow-1)*m_nCol + (nCol-1);
		int i=0;
		CUIWindow *pChild=GetWindow(GSW_FIRSTCHILD);
	    while(pChild)
		{

		   if(i==id){
             CUIYUV* yuv = ( CUIYUV*)pChild;
			 yuv->SetTxt(L"");
			 yuv->ClearPicData(cr);
		     return;
		   }
		   pChild=pChild->GetWindow(GSW_NEXTSIBLING);
		   i++;
	    }
		return;
	}
	void* CUIYUVGridPanel::GetPanelItem(int nRow,int nCol)
	{
	    int id= (nRow-1)*m_nCol + (nCol-1);
		int i=0;
		CUIWindow *pChild=GetWindow(GSW_FIRSTCHILD);
	    while(pChild)
		{

		   if(i==id){
             //CUIYUV* yuv = ( CUIYUV*)pChild;
		     return pChild;
		   }
		   pChild=pChild->GetWindow(GSW_NEXTSIBLING);
		   i++;
	    }
		return 0;
	}
	int   CUIYUVGridPanel::OnCreate(int)
	{

		

		 SetRowCol(m_nRow,m_nCol);

	     
	     return 0;
	}
	int  CUIYUVGridPanel::SetRowCol(int nRow,int nCol)
	{
		CUIWindow *pChild=GetWindow(GSW_FIRSTCHILD);
		while (pChild)
		{
			
			if(DestroyChild(pChild))
			    pChild=GetWindow(GSW_FIRSTCHILD);
			else
				 pChild = 0;
		}
		m_pFirstChild=m_pLastChild=NULL;
		m_nChildrenCount=0;

		  m_nRow = nRow;
		  m_nCol = nCol;
	      CRect rt;
		  GetClientRect(&rt);
		  int h = rt.Height()/m_nRow;
		  int w = rt.Width()/m_nCol;
		  char*    format= "<uiyuv name=\"item_%d_%d\" isLock=\"1\" pos=\"%d,%d,@%d,@%d\" margin=\"1,1,1,1\" colorBorder=\"#FFFF00FF\" colorBkgnd=\"#FFFF0000\" />";
		  char temp[1024]="";
		  for(int i=0,row = rt.top;i<m_nRow;row+=h,i++)
		  {
			  for(int j=0,col = rt.left;j< m_nCol;col+=w,j++)
			  {
			       sprintf(temp,format, i,j,col,row,w,h);
                   this->CreateChildren(temp);
			  }
		  }
		  return 1;
	}
	void CUIYUVGridPanel::ClearData(unsigned int cr)
	{
	   CUIWindow *pChild=m_pFirstChild;
		while (pChild)
		{
			((CUIYUV*)pChild)->SetTxt(L"");
			((CUIYUV*)pChild)->ClearPicData(cr);

			CUIWindow *pNextChild=pChild->GetWindow(GSW_NEXTSIBLING);
			pChild=pNextChild;
		}
	}
	void CUIYUVGridPanel::OnSize(unsigned int nType, int x,int y)
	{
	      CRect rt;
		  GetClientRect(&rt);
		  int h = rt.Height()/m_nRow;
		  int w = rt.Width()/m_nCol;
		  char temp[1024]="";
		  CUIWindow *pChild=0;
		  for(int i=0,row = rt.top;i<m_nRow;row+=h,i++)
		  {
			  for(int j=0,col = rt.left;j< m_nCol;col+=w,j++)
			  {
			      
				  if(pChild==0)
				       pChild=GetWindow(GSW_FIRSTCHILD);
				  else
					  pChild=pChild->GetWindow(GSW_NEXTSIBLING);
			 
                  CRect rcWnd;
				  rcWnd.left   = col;
				  rcWnd.top    = row;
				  rcWnd.bottom = rcWnd.top+ h;
				  rcWnd.right  = rcWnd.left+ w;
				  pChild->OnRelayout(&rcWnd);

				   SStringA Pos;
					Pos.Format("%d,%d,@%d,@%d",rcWnd.left,rcWnd.top,rcWnd.Width(),rcWnd.Height());
					 pChild->SetAttribute("pos", Pos);

			  }
		  }
		  	UpdateChildrenPosition();
	}
	void  CUIYUVGridPanel::OnNcPaint(IRenderTarget *pRT)
	{

		if(m_pFont==0){
			this->GetContainer()->GetIRenderFactory()->CreateFont(&m_pFont,12,L"宋体");
		}
	   	CRect rt;
		GetClientRect(&rt);
		int h = rt.Height()/m_nRow;
		int w = rt.Width()/m_nCol;
		unsigned int color = 0xFFFFFFFF;
		for(int i = rt.top;i<rt.bottom;i+=h)
		{
			pRT->DrawLine(rt.left,i,rt.right,i,color);
		}
		for(int i = rt.left;i<rt.right;i+=w)
		{
			pRT->DrawLine(i,rt.top,i,rt.bottom, color);
		}

	
		
	}
}