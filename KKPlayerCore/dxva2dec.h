/************dxva2 �°����*************/
#ifndef dxva2dec_H_
#define dxva2dec_H_
#include "render/render.h"
extern "C" {
	#include "libavformat/avformat.h"
	#include "libavcodec/avcodec.h"
	#include "libavcodec/dxva2.h"
	#include "libswscale/swscale.h"
	#include "libavutil/buffer.h"
}
int BindDxva2Module2(AVCodecContext  *pCodecCtx,IDXVA2Ctx* pIDxva);
#endif