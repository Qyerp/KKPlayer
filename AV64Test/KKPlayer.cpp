#include "stdafx.h"
#include "KKPlayer.h"

typedef  void (*fpKKPlayerErrNotify)(void *UserData,int errcode);
typedef  void *(*fpCreateKKPlayer)(HWND h,RECT Rt,DWORD style,HWND *OutHwnd,bool yuv420);
typedef  void (*fpRefreshDuiKKPlayer)(void* player);
typedef  void (*fpKKDuiOnSize)(void* player,int w,int h);
///返回数据RGBA。
typedef  void *(*fpCreateDuiKKPlayer)      (HWND hAudio,fpRenderImgCall fp,void* UserData);
///返回指定格式的数据，视频图片大小不变
typedef  void *(*fpCreateDuiRawKKPlayer)   (HWND hAudio,fpRenderImgCall fp,void *RenderUserData,int imgType);
typedef  int  (*fpKKOpenMedia)             (void* player,const char* url,const char* cmd);
typedef  void (*fpKKCloseMedia)            (void* player);
typedef  void (*fpSetKKVolume)             (void* player,int volume,bool tip);
typedef  void (*fpKKDelPlayer)             (void* player,bool dui);
typedef  void (*fpKKSetErrNotify)          (void* player,fpKKPlayerErrNotify noti,void* UserData);
typedef  void (*fpSetMaxRealtimeDelay)     (void* player,double Delay);
///获取视频信息
typedef  int (*fpGetMediaInfo)            (const char* path,char** OutJson,int NeePic,int sec,int *NeedWidth,int *NeedHeight,char** BGRA,int* BGRAlen);
typedef  void (*fpKKFree)                  (void* ptr);

static HMODULE hSysResource=0;
static fpCreateKKPlayer    m_FnCreatePlayer=0;
static fpCreateDuiKKPlayer m_FnCreateDuiKKPlayer=0;
static fpCreateDuiRawKKPlayer  m_FnCreateDuiRawKKPlayer=0;

static fpKKOpenMedia                  m_FnOpenMedia=0;
static fpKKCloseMedia                 m_FnKKCloseMedia=0;
static fpSetKKVolume                  m_FnSetKKVolume=0;
static fpKKDelPlayer                  m_FnKKDelPlayer=0;
static fpKKSetErrNotify               m_FnKKSetErrNotify=0;
static fpKKDuiOnSize                  m_FpKKDuiOnSize=0;
static fpRefreshDuiKKPlayer           m_FpRefreshDuiKKPlayer=0;
static fpSetMaxRealtimeDelay          m_FpSetMaxRealtimeDelay=0;
static fpGetMediaInfo                 m_FpGetMediaInfo=0;
static fpKKFree                       m_FpKKFree;

void Inikkpalyer()
{
	 if(m_FnCreatePlayer)
		return;

     if(hSysResource==NULL)
	 {
	     hSysResource=LoadLibrary(_T("libkkplayer.dll"));

		 if(hSysResource){
			 m_FnCreatePlayer = (fpCreateKKPlayer )GetProcAddress(hSysResource, "CreateKKPlayer");
			 m_FnCreateDuiKKPlayer= (fpCreateDuiKKPlayer )GetProcAddress(hSysResource, "CreateDuiKKPlayer");
			 m_FnCreateDuiRawKKPlayer=(fpCreateDuiRawKKPlayer )GetProcAddress(hSysResource, "CreateDuiRawKKPlayer");
			 m_FpRefreshDuiKKPlayer=(fpRefreshDuiKKPlayer)GetProcAddress(hSysResource, "RefreshDuiKKPlayer");
			 m_FpKKDuiOnSize= (fpKKDuiOnSize)GetProcAddress(hSysResource, "KKDuiOnSize");
			 m_FnOpenMedia= (fpKKOpenMedia )GetProcAddress(hSysResource, "KKOpenMedia");
			 m_FnKKCloseMedia= (fpKKCloseMedia )GetProcAddress(hSysResource, "KKCloseMedia");
			 m_FnSetKKVolume= (fpSetKKVolume )GetProcAddress(hSysResource, "KKSetVolume");
			 m_FnKKDelPlayer= (fpKKDelPlayer )GetProcAddress(hSysResource, "KKDelPlayer");
			 m_FnKKSetErrNotify= (fpKKSetErrNotify )GetProcAddress(hSysResource, "KKSetErrNotify");
			 m_FpSetMaxRealtimeDelay=(fpSetMaxRealtimeDelay )GetProcAddress(hSysResource, "SetMaxRealtimeDelay");
			 m_FpGetMediaInfo =(fpGetMediaInfo)GetProcAddress(hSysResource, "GetMediaInfo");
			 m_FpKKFree= (fpKKFree)GetProcAddress(hSysResource, "KKFree");
		 }else{
			 ::MessageBox(0,L"libkkplayer.dll err",L"",0);
		 }
	 }
}
CKKPlayer::CKKPlayer():m_hWnd(0),m_pPlayerh(0)
,m_pNty(0)
,m_UserData(0)
,m_FnRenderImgCal(0)
,m_nDui(0)
{
	Inikkpalyer();
}
CKKPlayer::~CKKPlayer()
{
	if(m_pPlayerh!=NULL){
		bool Dui=false;
		if(m_nDui)
			Dui=true;
	    m_FnKKDelPlayer(m_pPlayerh,Dui);
		m_pPlayerh=NULL;
	}
}
enum EKKPlayerErr
{
     KKOpenUrlOk=0,          /***播发器打开成功**/
	 KKOpenUrlOkFailure=1,  /**播发器打开失败***/
	 KKAVNotStream=2,
	 KKAVReady=3,          ///缓冲已经准备就绪
	 KKAVWait=4,           ///需要缓冲
	 KKRealTimeOver=5,     ///流媒体结束
};
void CKKPlayer::ErrNotify(void *UserData,int errcode)
{
	CKKPlayer *Player=(CKKPlayer *)UserData;
	if(!Player->m_pNty)
		return;
	if(errcode==KKOpenUrlOkFailure || errcode==KKRealTimeOver){
	    Player->m_pNty->OnPlayerState( Player->m_UserData,AVOpenFailed);
	}else if(errcode==KKAVReady){
		Player->m_pNty->OnPlayerState( Player->m_UserData,AVLoadingOver);
   }
}
HWND CKKPlayer::CreateKKPlayer(HWND h,RECT Rt,DWORD style,bool yuv420)
{
	if( m_FnCreatePlayer){
	      m_pPlayerh = m_FnCreatePlayer(h,Rt,style,&m_hWnd,yuv420);
		  m_FnKKSetErrNotify(m_pPlayerh,ErrNotify,this);
		  return m_hWnd;
	}
	return NULL;
}
void CKKPlayer::DuiSize(int w,int h)
{
     if(m_FpKKDuiOnSize)
	 {
	      m_FpKKDuiOnSize(m_pPlayerh,w,h);
	 }
}
void   CKKPlayer::SetMaxRealtimeDelay(double Delay)
{
    if(m_FpSetMaxRealtimeDelay&&m_pPlayerh)
	{
	   m_FpSetMaxRealtimeDelay(m_pPlayerh,Delay);
	}
}
void CKKPlayer::CreateDuiKKPlayer(HWND hAudio,fpRenderImgCall fpCall,void *UserData)
{
	if(m_FnCreateDuiKKPlayer){
		m_nDui=1;
	    m_pPlayerh =  m_FnCreateDuiKKPlayer(hAudio,fpCall,UserData);
		m_FnKKSetErrNotify(m_pPlayerh,ErrNotify,this);
	}
}
void CKKPlayer::CreateDuiRawKKPlayer(HWND hAudio,int imgType,fpRenderImgCall fpCall,void *UserData)
{
    if(m_FnCreateDuiKKPlayer)
	{
		m_nDui=1;
	    m_pPlayerh =  m_FnCreateDuiRawKKPlayer(hAudio,fpCall,UserData,imgType);
		m_FnKKSetErrNotify(m_pPlayerh,ErrNotify,this);
	}
}
void CKKPlayer::RefreshDuiPlayer()
{
	if(m_pPlayerh!=NULL)
      m_FpRefreshDuiKKPlayer(m_pPlayerh);
}

void CKKPlayer::init()
{
Inikkpalyer();
}
void CKKPlayer::SetAVPlayerNotify(IAVPlayerNotify* nty)
{
   m_pNty=nty;
}
int  CKKPlayer::OpenMedia(std::string url)
{
	if(m_pPlayerh!=NULL){
	    return m_FnOpenMedia(m_pPlayerh,url.c_str(),"");
	}
	return -1;
}
void CKKPlayer::CloseMedia()
{
	if(m_pPlayerh!=NULL){
		 m_FnKKCloseMedia(m_pPlayerh);
	}
}
void CKKPlayer::SetVlcVolume(int volume,bool tip)
{
	if(m_pPlayerh!=NULL){
		m_FnSetKKVolume(m_pPlayerh,volume,tip);
	}
}
void  CKKPlayer::SetUserData(void* UserData)
{
	m_UserData=UserData;
}

void CKKPlayer::GetFileMediaInfo(const std::string& path,char** OutJson,int NeePic,int sec,int &NeedWidth,int &NeedHeight,char** OutBGRA,int* BGRAlen)
{
	m_FpGetMediaInfo(path.c_str(), OutJson,NeePic,sec,&NeedWidth, &NeedHeight, OutBGRA, BGRAlen);
}
void  CKKPlayer::KKFree(void* ptr)
{
m_FpKKFree(ptr);
}