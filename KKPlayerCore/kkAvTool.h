#ifndef  kkAvTool_H_
#define  kkAvTool_H_
#include <string>
//文件信息简单获取操作
class    CKkAvTool
{
  public:
	     CKkAvTool();
	     ~CKkAvTool();

		 //获取文件媒体信息
		 int GetMediaInfo(const std::string& path,char** OutJson,int NeePic,int sec,int &NeedWidth,int &NeedHeight,char** BGRA,int* BGRAlen);
};
void KKPlayerFree(void* ptr);
#endif