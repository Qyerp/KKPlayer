/**************************kkplayer*********************************************/
/*******************Copyright (c) Saint ******************/
/******************Author: Saint *********************/
/*******************Author e-mail: lssaint@163.com ******************/
/*******************Author qq: 784200679 ******************/
/*******************KKPlayer  WWW: http://www.70ic.com/KKplayer ********************************/
/*************************date：2015-6-25**********************************************/
#include <queue>
#include <list>
#include "stdafx.h"

#include "KKPlayerInfo.h"
#include "IKKAudio.h"
#include "render/render.h"
#include "IKKPlayUI.h"


#include "KKLock.h"
#include "KKVideoInfo.h"
#include "KKPlugin.h"
#include <string>
#include <queue>

#ifndef KKPlayer_H_
#define KKPlayer_H_
enum  EStreamMediaType
{
     ESMT_No=0,
	 ESMT_Http=1,
	 ESMT_Rtmp=2,
	 ESMT_Rtsp=3,
	 ESMT_Hls=4,
};

enum EPlayerState
{
	 EPS_Ini     = 0,
     EPS_Opening = 1,
     EPS_ReadingThread = 2,
	 EPS_ReadingPre = 3,
	 EPS_ReadingOk  = 4,
};
class KKPlayer
{
    public:

		    static fpPrepareDecompress G_fpPrepareDecompress;
	        KKPlayer(IKKPlayUI* pPlayUI,IKKAudio* pSound);
	        ~KKPlayer(void);
			/******Windows平台调用**********/
			void          SetWindowHwnd(HWND hwnd);

			//设置播放模式
			void          SetPlayAvModel(int model);
			///设置URL替换函数
			void          SetKKPlayerGetUrl(fpKKPlayerGetUrl pKKPlayerGetUrl);
			///强制中断。
			void          ForceAbort();

			///设置是否呈现
			void          SetRender(bool bRender);
			/*********打开媒体.成功返回0，失败返回-1.************/
			int           OpenMedia(const char* URL,const char* Other=""); 
			
			/*********关闭播放器*********/
			void          CloseMedia(); 
			void          RenderImage(IkkRender *pRender,bool Force);
			
			void          OnDecelerate();
			void          OnAccelerate();
			int           GetAVRate();
#ifdef WIN32_KK
			/*****Gdi*****/
			void          OnDrawImageByDc(HDC memdc);
			void          VideoDisplay(void *buf,int w,int h,void *usadata,double last_duration,double pts,double duration,long long pos,double diff);
#endif           
			void          SetVolume(long value);
			long          GetVolume();
			void          ResetPlayIni();
			//暂停 pause 1 暂停，0 开始
			void          Pause(int pause);
			//快进快退，相对
			int           KKSeek( SeekEnum en,int value);
			//单位时间秒
			int           AVSeek(int value,short segid=-1);
			
			
			//获取播放信息
			bool          GetMediaInfo(MEDIA_INFO &info);
			
			//得到包序列号
			int           GetPktSerial();	
			short         GetSegId();
			short         GetCurSegId();
		
		

			//1为流媒体,没有open返回-1/******是否是流媒体,该方法不是很准确*****/
			int GetRealtime();

			//解码成BGRA格式
			void SetBGRA();
			///抓取视频图片
			bool GrabAvPicBGRA(void* buf,int len,int &w,int &h,bool keepscale=true);
			/******是否准备好了,准备好返回1，否则返回0，没有open返回-1*******/
			int GetIsReady();
			
			//得到延迟
			int GetRealtimeDelay();
			//强制刷新播放器Que
			void ForceFlushQue();
		
			/******设置实时流媒体最小延迟,最小值2，单位秒**********/
			int SetMaxRealtimeDelay(double Delay);
            
			//显示视频追踪信息,返回1成功
			int ShowTraceAV(bool Show);

			//获取播放的时间
			float GetPlayTime();
			int GetTotalTime();

			
			/*******************插件分析,返回1有对应的插件*********************/
			int KKProtocolAnalyze(char *StrfileName,KKPluginInfo &KKPl);

			SKK_VideoState*  GetVideoState(){return               m_pVideoInfo; }
			/**********添加IO插件**********/
			static void AddKKPluginInfo(KKPluginInfo &info);

			/******添加输入格式插件********/
			static void RegisterInputFormat(AVInputFormat *format);
			/****注册一个解码器********/
			static void RegisterAvDecoder(AVCodec* decoder);

			static void SetStoreInfo(const char *strstoredir,int downspeedlimit,int upspeedlimit);
			static std::list<KKPluginInfo>&  GetKKPluginInfoList();
#ifdef Android_Plat
			void *  GetVideoRefreshJNIEnv()
			{
			    return m_pVideoRefreshJNIEnv;
			}
			///设置Surface,用于硬件渲染。
			void    SetViewSurface(void* surface);
			bool    IsMediacodecSurfaceDisplay();
#endif

			//重置参数，只有部分参数有效有效
			void     RetArg(const char* cmd);
			void     SetStartTime(int Startime);
			void     SetEndTime(int EndTime);

			
			void     EnbleUniformLoad();
			double   OnLoadVideo(IkkRender *pRender);
private:
	       
	        /*********视频刷新线程********/
	        static unsigned __stdcall VideoRefreshthread(LPVOID lpParameter);
			//音频回调线程
			static unsigned __stdcall Audio_Thread(LPVOID lpParameter);
			//文件读取线程
	        static unsigned __stdcall ReadAV_thread(LPVOID lpParameter);
			
			void        OpenAudioDev();
			
			//视频读取结束后处理
			void        OnAvOverHandle(int AVQueSize,bool flush=false);
			void        InterSeek(AVFormatContext*  pAVForCtx);
			int64_t     ResetStartIo(int ini=0);
            //seek 处理
			void        OnRealSeek();

			//数据读取
	        void        OnReadAV();
	
			///延迟分析
			void        AvDelayParser();
			void        OnResetQue();
	        
			//视频刷新
			double      OnVideoRefresh();
			/*******显示视频，及刷新**********/
		    void        video_image_refresh(SKK_VideoState *is);
			
			/******绘制音频图谱******/
            void        video_audio_display(IkkRender *pRender,SKK_VideoState *s);

			/********流媒体这是刷新函数**********/
			void        Avflush(int64_t seek_target); 
			void        AvflushRealTime(int Avtype);
			//读音频回调
			void        ReadAudioCall();
			void        PacketQueuefree();

			
			 //视频信息
			
			KKPlayer(const KKPlayer& cs);
			KKPlayer operator = (const KKPlayer& cs);
private:
	        //插件信息
	        static std::list<KKPluginInfo>  KKPluginInfoList;

		
			EStreamMediaType                m_nEStreamMediaType;
			///命令行选项
			std::string                     m_strcmd;
			
			//缓存信息
			AVCACHE_INFO                    m_AVCacheInfo;

	        //视频信息
	        SKK_VideoState*                 m_pVideoInfo; 

			
			
			
            //播放信息
			MEDIA_INFO                      m_AVPlayInfo;
			//播放器锁
	        CKKLock                         m_PlayerLock;
			//
			CKKLock                         m_CtxLock;
			///指示是否有分片
			volatile int                    m_nNeedReset;
			///缓存计数
			int                             m_CacheAvCounter;
			///是否显示
			bool                            m_bRender;

			CKKLock                         m_StateLock;
            volatile  EPlayerState          m_ePlayerState;

	        volatile  bool                  m_bOpen;
	        IKKPlayUI*                      m_pPlayUI;
	       
			//当前包序列号
			volatile int                    m_PktSerial;
	        IKKAudio*                       m_pSound;
			///窗口句柄
			HWND                            m_hwnd;

			//开始时间
			int64_t                         m_nStartTime;
			//结束时间
			int64_t                         m_nAvEndTime;


			///seektime
			int                             m_nSeekTime;
			
			//当前时间
			float                           m_fCurTime;
			//用于内部
			int                             m_nInterCurTime;
			//总时间
			int                             m_TotalTime;
			/// 
			
			int                             m_nhasVideoAudio;
			//视频读取线程
			SKK_ThreadInfo                  m_ReadThreadInfo;
			//视频刷新线程
			SKK_ThreadInfo                  m_VideoRefreshthreadInfo;
            //音频数据回调线程
			SKK_ThreadInfo                  m_AudioCallthreadInfo;
			

			int64_t                         m_lstPts;
			
            //用于渲染音频波形的缓存
			void*                           m_pAudioPicBuf;
			int                             m_AudioPicBufLen;
			//解码后图形的格式
			AVPixelFormat                   m_DstAVff;
			double                          m_nOpenTime;
			int                             m_nStreamMediaType;
#ifdef Android_Plat
			void*                           m_pVideoRefreshJNIEnv;
			void*                           m_pViewSurface;
			bool                            m_bSurfaceDisplay;
#endif
			///用于替换掉原来的url
		    fpKKPlayerGetUrl                m_pKKPlayerGetUrl;      
			int                             m_nOverNotify;
			// 0： 默认  1：为循环模式
			int                             m_nPlayAvModel;
			//使用统一线程Load
			int                             m_nUniformLoad;
			AVFrame*                        m_pLoadFrame;
		    mutable volatile	int         m_nUnUsedFrame;
			mutable volatile    float       m_dRemainingTime;
};

#endif
