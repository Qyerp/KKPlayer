// AudioToPcm.cpp : 定义控制台应用程序的入口点。
//
#include "AudioToPcm.h"
#include "Includeffmpeg.h"
#ifdef WIN32
#include "stdafx.h"
#include <assert.h>
int main()
{
	CAudioToPcm audiopcm;
	audiopcm.AudioToPcm("E:/ProgramTool/Android51see/AudioToPcm/Debug/黄昏里.mp3", 16000);
    return 0;
}
#endif
// WIN32


//123456789
//123456
//admin
enum EPlayState
{
	AV_Stop,
	AV_Play,
	AV_Pause,
};
void RaiseVolume(char* buf, int size, int uRepeat, double vol);
void IniFF();
CAudioToPcm::CAudioToPcm(int Id, IRecPcm* pIRecPcm):
m_nObjId(Id),
m_pIRecPcm(pIRecPcm),
m_nPlayState(0),
m_nVol(100)
{
	IniFF();
}
CAudioToPcm::~CAudioToPcm()
{

}
int CAudioToPcm::PlayPause(int type)
{
	if(m_nPlayState!=AV_Stop)
	m_nPlayState = type;
}
int CAudioToPcm::GetPlayState()
{
	return m_nPlayState;
}
void CAudioToPcm::SetVol(int vol)
{
	m_nVol = vol;
	
}
//频率，16bit
int CAudioToPcm::AudioToPcm(void* UserData,const char* path,int ch_layout,int sample_rate)
{
	m_nPlayState = AV_Play;
	AVFormatContext* pFormatCtx = avformat_alloc_context();
	AVDictionary*    format_opts = NULL;
	AVInputFormat*   iformat = NULL;
	AVCodecContext  *AudioCodecCtx = NULL;
	AVCodec         *AudioCodec = NULL;
	SwrContext*      au_convert_ctx =NULL; 
	int              out_buffer_size =0;
	uint8_t*         out_buffer =0;
	AVSampleFormat out_sample_fmt = AV_SAMPLE_FMT_S16;
	int out_sample_rate = sample_rate;
	int out_ch_layout = ch_layout;
	int AudioStreamIndex = -1;
	AVDictionary **opts=0;
    char out_pcm_buf[2048]="";
	int  out_pcm_buflen = 2048;
	int  out_pcm_acbuflen = 0;
	int  last_pts=0;
	int  totalpts=0;
	AVPacket pkt1, *pkt = &pkt1;
	AVFrame *AudioFrame = av_frame_alloc();
	int err = avformat_open_input(&pFormatCtx, path, iformat, &format_opts);
	//文件打开失败
	if (err<0) {

		avformat_free_context(pFormatCtx);
		pFormatCtx = 0;
		goto end;
	}


	av_format_inject_global_side_data(pFormatCtx);


	
	//获取流信息
	if (avformat_find_stream_info(pFormatCtx, opts)<0) {
		goto end;
	}

	if (opts != NULL) {
		for (int i = 0; i < pFormatCtx->nb_streams; i++)
			av_dict_free(&opts[i]);
		av_freep(&opts);
	}
	av_dict_free(&format_opts);

	
	
	for (int i = 0; i < pFormatCtx->nb_streams; i++) {
		AVStream *st = pFormatCtx->streams[i];
		if (st->codecpar->codec_type == AVMEDIA_TYPE_AUDIO)
			AudioStreamIndex = i;
		
	}
	
	AudioCodecCtx = avcodec_alloc_context3(NULL);
	
	//音频
	if (AudioStreamIndex >= 0){
		err = avcodec_parameters_to_context(AudioCodecCtx, pFormatCtx->streams[AudioStreamIndex]->codecpar);
		if (err < 0)
			goto end;
		AudioCodec = avcodec_find_decoder(AudioCodecCtx->codec_id);
		AVDictionary *opts = NULL;
		av_dict_set(&opts, "refcounted_frames", "1", 0);
		//打开解码器
		if ((err = avcodec_open2(AudioCodecCtx, AudioCodec,&opts)) < 0)
		{
			goto end;
		}

		av_codec_set_lowres(AudioCodecCtx, 0);
		if (AudioCodec->capabilities & CODEC_CAP_DR1)
			AudioCodecCtx->flags |= CODEC_FLAG_EMU_EDGE;
		av_dict_free(&opts);
	}


	
	

	
	totalpts = pFormatCtx->duration / 1000;
	//Swr
	au_convert_ctx = swr_alloc();
	au_convert_ctx = swr_alloc_set_opts(au_convert_ctx, out_ch_layout, out_sample_fmt, out_sample_rate,
		AudioCodecCtx->channels, AudioCodecCtx->sample_fmt, AudioCodecCtx->sample_rate, 0, NULL);
	swr_init(au_convert_ctx);

	out_buffer_size = av_samples_get_buffer_size(NULL, out_ch_layout, out_sample_rate, out_sample_fmt, 1);
	out_buffer = (uint8_t *)av_malloc(out_buffer_size * 2);
	//FILE *pFile = fopen("output.pcm", "wb");
	
	err = 0;
	
	while (1)
	{

		err = av_read_frame(pFormatCtx, pkt);
		if (err == AVERROR_EOF|| m_nPlayState == AV_Stop)
		{
						 if(out_pcm_acbuflen>=0){
							 if(m_nVol!=100){
								 double ff=(double)m_nVol/100;
							     RaiseVolume(out_pcm_buf, out_pcm_acbuflen, 1, ff);
							 }
						     m_pIRecPcm->OnRecPcm(UserData,m_nObjId, last_pts,totalpts,out_pcm_buf, out_pcm_acbuflen);
						 }
			break;
		}
		if (m_nPlayState == AV_Pause)
		{
			av_usleep(10000);
			continue;
		}
		if (err>-1&&pkt->stream_index == AudioStreamIndex) {
			int got_frame = avcodec_send_packet(AudioCodecCtx,pkt);
			if (got_frame<0) {
				got_frame = 0;
				assert(0);
			}else {/**/
				got_frame = avcodec_receive_frame(AudioCodecCtx, AudioFrame);
				if (got_frame >= 0)
				{
					got_frame = 1;
					AudioFrame->pts = pkt->pts;

					AVRational tb = pFormatCtx->streams[AudioStreamIndex]->time_base;
					if (AudioFrame->pts != AV_NOPTS_VALUE)
						AudioFrame->pts = av_rescale_q(AudioFrame->pts, tb, AudioCodecCtx->time_base);
					else/**/
						AudioFrame->pts = pkt->pts;

					 last_pts= AudioFrame->pts* 1000*av_q2d(AudioCodecCtx->time_base);
					//printf("pts:%d pts size:%d \n", AudioFrame->pts, pkt->size);

					int lx=swr_convert(au_convert_ctx, &out_buffer, out_buffer_size, (const uint8_t **)AudioFrame->data, AudioFrame->nb_samples)*2;
					if (lx > 0&& m_pIRecPcm) {
						
						 int buflen=out_pcm_buflen -out_pcm_acbuflen;
						 buflen= buflen>lx ?lx:buflen;
	                     memcpy(out_pcm_buf+out_pcm_acbuflen,out_buffer,buflen);
	                     out_pcm_acbuflen+=buflen;
						 if(out_pcm_acbuflen>=2048){
							 if(m_nVol!=100){
								 double ff=(double)m_nVol/100;
							     RaiseVolume(out_pcm_buf, out_pcm_acbuflen, 1, ff);
							 }
						    m_pIRecPcm->OnRecPcm(UserData,m_nObjId, last_pts,totalpts,out_pcm_buf, out_pcm_acbuflen);
                            out_pcm_acbuflen = 0 ;
							
							if(buflen!=lx){
								 memcpy(out_pcm_buf,out_buffer+buflen,lx-buflen);
								 out_pcm_acbuflen=lx-buflen;
							}
						 }
					     //int sl=10000;//AudioFrame->nb_samples*1000000 / AudioFrame->sample_rate;
						 //av_usleep(sl);
					//	fwrite(out_buffer, 1, lx * 2, pFile);
					}
					av_frame_unref(AudioFrame);
					//av_usleep(10)
				}
				else {
					got_frame = 0;
				}
			}

			av_packet_unref(pkt);
		}
		else
		{
			av_packet_unref(pkt);
		}

		
		
	}
	
	av_frame_free(&AudioFrame);
	swr_free(&au_convert_ctx);
	//fclose(pFile);
	av_free(out_buffer);
end:
	if(format_opts)
	    av_dict_free(&format_opts);

	if (AudioCodecCtx)
		avcodec_free_context(&AudioCodecCtx);

	if (pFormatCtx != NULL) {
		avformat_close_input(&pFormatCtx);
	}

	m_nPlayState = AV_Stop;
	return err;
}