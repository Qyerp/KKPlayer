#ifndef RttCache_D3D9_H_
#define RttCache_D3D9_H_
#include <map>
#include <set>
#include <vector>
#include <string>
#include <d3d9.h>
#include "ObjRef.h"
#include "Core/ObjectImp.h"
#include "Core/IRender.h"

//���ڻ���
namespace KKUI
{
	 struct SRender3DTexture;
	 class SRenderFactory_D3D9;


	 struct SYUVKey
	 {
	       int                     nWidth;
		   int                     nHeight;

		   // 0: yuv 420p  1 yuv 422p  2 yuv 422p 10bit;
		   int                     nYuvFormat;
		   bool operator<         (const SYUVKey& right) const;
	 };
	 typedef struct SYUVInfoTex
	 {
		   IDirect3DTexture9*      pYTexture;
		   IDirect3DTexture9*      pUTexture;
		   IDirect3DTexture9*      pVTexture;
		   int Used;
	    //   bool operator<(const SYUVInfoTex& right) const;
	 }SYUVInfoTex;
     class CRttCache_D3D9
	 {
	     public:
			     CRttCache_D3D9(SRenderFactory_D3D9* pRenderFactory);
				 ~CRttCache_D3D9();
				 //��Ⱦ�������
			     IDirect3DSurface9*     GetBufferRenderTexture(int width,int height, IDirect3DTexture9**      ppTexture);
				 void                   ReleaseBufferRenderTexture(int width,int height, IDirect3DTexture9*  pTexture);

				 //
				 int                    GetCacheYuvInfo(int width,int height,int yuvFormat,IDirect3DTexture9** ppYTexture,IDirect3DTexture9**   ppUTexture,IDirect3DTexture9**      ppVTexture);
				 void                   ReleaseX();
				 void                   ReLoadResX();

				 //����Rtt ����
				 void                   CreateRttCache(int width,int height);
	     private:
			     //��Ⱦ�������
                 std::map<unsigned long long, std::vector<SRender3DTexture*> >   m_BufferRenderMap;
                 std::map<SYUVKey, SYUVInfoTex >                   m_YUVInfoTexMap;


				 SRenderFactory_D3D9*    m_pRenderFactory;


	 };
}
#endif