package com.ic70.kkplayer.kkplayer;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import com.ic70.kkplayer.sdk.CkkMediaInfo;
import com.ic70.kkplayer.sdk.CKKMoviePath;

public class LocalListActivity extends AppCompatActivity implements IKKMessageHandler {

    private COs_KKHander m_Handler=null;
    //视频文件管理
    private CFileManage m_FileManage=null;
    boolean m_HandleIni=false;
    private List<CKKMoviePath> m_lstFileList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.av_local_list_view);
        m_FileManage = new CFileManage();
        //Ini();

        Button btn_LocalPage=(Button) this.findViewById(R.id.btn_stream);
        btn_LocalPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(v.getContext(), AvListActivity.class);//(context.this, Activity02.class);
                v.getContext().startActivity(intent);
            }
        });
    }
    @Override
    protected void onStart() {
        super.onStart();

    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        // TODO Auto-generated method stub
        super.onWindowFocusChanged(hasFocus);
        if(hasFocus){
            Ini();
        }
    }
    void Ini(){

        if(m_Handler==null)
        {
            m_Handler = new COs_KKHander(this);
            m_FileManage.start(m_Handler  ,GetPopWait());
            m_HandleIni=true;
        }

        Button btn_StreamPage=(Button) this.findViewById(R.id.btn_stream);
        btn_StreamPage.setCompoundDrawables(null, null, null,null);
        Button btn_LocalPage=(Button) this.findViewById(R.id.btn_LocalPage);
        Drawable bottomDrawable = getResources().getDrawable(R.drawable.line);
        bottomDrawable.setBounds(0, 0, bottomDrawable.getMinimumWidth(),bottomDrawable.getMinimumHeight());
        btn_LocalPage.setCompoundDrawables(null, null, null,bottomDrawable);

        btn_LocalPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("pop","11");
                PopupWindow popupWindow = new PopupWindow();
                popupWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
                popupWindow.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
                popupWindow.setFocusable(true);
                Log.d("pop","xx");
                View view = LayoutInflater.from(v.getContext()).inflate(R.layout.pop_wait,null);
                if(view ==null)
                    Log.d("pop","view null");
                else
                    Log.d("pop","view ");
                popupWindow.setContentView(view);
                Log.d("pop","view 2");
                popupWindow.showAtLocation(getWindow().getDecorView(), Gravity.CENTER,0,0);
                Log.d("pop","view 3");

            }
        });
    }

    public void HandleKKObj(Object obj)
    {
       Message msg=(Message)obj;
        switch (msg.what) {
            case  COs_KKHander.MSG_LoadFile:
                COs_KKHander.Os_Message objms =(COs_KKHander.Os_Message)msg.obj;
                m_lstFileList =( List<CKKMoviePath>) objms.obj;
                PopupWindow pop=(PopupWindow)objms.obj2;
                pop.dismiss();
                LoadFileInfo(m_lstFileList);
                break;
        }
        //this.finish();
    }

    void LoadFileInfo(List<CKKMoviePath> Partlist)
    {
        ListView Localmovie_list = (ListView) this.findViewById(R.id.avlocallist);
        Localmovie_list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        if(Partlist.size()<=0) {
            CKKMoviePath KKpath = new CKKMoviePath();
            KKpath.MovieName="测试";
            Partlist.add(KKpath);
        }
        CKKMListAdapter adapter1=new CKKMListAdapter(this,Partlist);
        Localmovie_list.setAdapter( adapter1);
    }
    PopupWindow  GetPopWait()
   {

     /*  LayoutInflater inflater = LayoutInflater.from(this);
       View v = inflater.inflate(R.layout.pop_wait, null);// 得到加载view
       LinearLayout layout = (LinearLayout) v.findViewById(R.id.dialog_loading_view);// 加载布局

       Dialog loadingDialog = new Dialog(this);
       loadingDialog.setCancelable(true); // 是否可以按“返回键”消失
       loadingDialog.setCanceledOnTouchOutside(false); // 点击加载框以外的区域
       loadingDialog.setContentView(layout, new LinearLayout.LayoutParams(
               LinearLayout.LayoutParams.MATCH_PARENT,
               LinearLayout.LayoutParams.MATCH_PARENT));// 设置布局

       Window window = loadingDialog.getWindow();
       WindowManager.LayoutParams lp = window.getAttributes();
       lp.width = WindowManager.LayoutParams.MATCH_PARENT;
       lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
       window.setGravity(Gravity.CENTER);
       window.setAttributes(lp);
      // window.setWindowAnimations(R.anim.dirtiprotate);
       loadingDialog.show();

*/
       Log.d("pop","11");
            PopupWindow popupWindow = new PopupWindow();
            popupWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
            popupWindow.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
            popupWindow.setFocusable(true);
       Log.d("pop","xx");
            View view = LayoutInflater.from(this).inflate(R.layout.pop_wait,null);
       if(view ==null)
          Log.d("pop","view null");
       else
           Log.d("pop","view ");
            popupWindow.setContentView(view);
       Log.d("pop","view 2");
            popupWindow.showAtLocation(getWindow().getDecorView(), Gravity.CENTER,0,0);
       Log.d("pop","view 3");
           //popupWindow.dismiss();
            return popupWindow;
   }
}
