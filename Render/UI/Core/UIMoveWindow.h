#ifndef UIMoveWindow_H_
#define UIMoveWindow_H_
#include "UIWindow.h"
namespace KKUI
{
	enum EKEY_MOVE
	{
	    EKM_Left   = 0,
		EKM_Top    = 1,
		EKM_Right  = 2,
		EKM_Bottom = 3
	};
    class CUIMoveWindow : public CUIWindow
    {
        KKUI_CLASS_NAME(CUIWindow, CUIMoveWindow,"uimovewindow")
		public:
			 CUIMoveWindow(void);
			 ~CUIMoveWindow(void);
	         void  GetWindowRect(kkRect* pRect) const;
		protected:
			 int  GetCtlAttr(const SStringA& attrName,SStringA& OutAttr);
			 //检查是否有磁性吸附，pt 一般为四个点
			 virtual int   CheckMagnetisRect(CPoint* pt,int ptCount,SMagnetRegion & M);
				//更新窗口位置
			 virtual void  OnUpdateFloatPosition(const  CSize&  szView,const CRect & rcParent);
		protected:
			        
			virtual int   OnSetCursor(const CPoint &pt);
			void          OnLButtonDown(unsigned int nFlags,CPoint pt);
			void          OnLButtonUp(unsigned int nFlags,CPoint pt);

			void          OnNcLButtonDown(unsigned int nFlags,CPoint pt);
			void          OnNcLButtonUp(unsigned int nFlags,CPoint pt);


			void          OnMouseMove(unsigned int nFlags,CPoint pt);
            void          OnNcMouseMove(unsigned int nFlags,CPoint pt);
            void          OnEraseBkgnd( IRenderTarget  *pRT);
            int           OnJustSize(const SStringA& strValue, int bLoading);
			void          OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
			virtual void  OnKeyMove(EKEY_MOVE KeyMove);
			KKUI_MSG_MAP_BEGIN()
                MSG_UI_KEYDOWN(OnKeyDown) 
				MSG_UI_MOUSEMOVE(OnMouseMove)
				MSG_UI_NCMOUSEMOVE(OnNcMouseMove)
				MSG_UI_LBUTTONDOWN(OnLButtonDown)
				MSG_UI_LBUTTONUP(OnLButtonUp)
				MSG_UI_NCLBUTTONDOWN(OnNcLButtonDown)
			
				MSG_UI_NCLBUTTONUP(OnNcLButtonUp)
				MSG_UI_ERASEBKGND(OnEraseBkgnd)
			KKUI_MSG_MAP_END(CUIWindow)
	        
		protected:
			enum {
			HALIGN_LEFT = 0x01,
			HALIGN_RIGHT = 0x02,
			VALIGN_TOP   = 0x10,
			VALIGN_BOTTOM = 0x20,
			};
	        

			KKUI_ATTRS_BEGIN()
				ATTR_INT("vAlign",m_nFloatVAlign,0)
				ATTR_INT("hAlign",m_nFloatHAlign,0)
				ATTR_INT("distX",m_nDistX,0)
				ATTR_INT("distY",m_nDistY,0)
				ATTR_INT("isLock",m_nIsLock,0)
				ATTR_CUSTOM("JustSize",OnJustSize)
			KKUI_ATTRS_END()
	        
			int m_nFloatVAlign;
			int m_nFloatHAlign;
			int m_nDistX,m_nDistY;
			CRect      m_OldRect;
			int        m_nMoveing;
			//锁定
	        int        m_nIsLock;
			//是否在拖动中
			int        m_bDraging;
			CPoint     m_ptClick;
			CSize      m_szView;
    };

}

#endif