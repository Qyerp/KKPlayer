// Render.cpp : 定义 DLL 应用程序的导出函数。
//

#include "stdafx.h"
#include <map>
#include "Render.h"
#include "renderD3D.h"
#include "renderGDI.h"
#include "renderRaw.h"
#include "RendLock.h"
CRendLock                 m_RendLock;
FpRenderLock              G_pRenderLock      = 0;
FpRenderUnLock            G_pRenderUnLock    = 0;

FpCreateDxva2Ctx          G_pCreateDxva2Ctx  = 0;
FpFreeDxva2Ctx            G_pFreeDxva2Ctx    = 0;

FpCreateDXTexture         G_pCreateDXTexture = 0;
FpFreeDXTexture           G_pFreeDXTexture=0;

void skpngZhuc();

#pragma comment (lib,"Windowscodecs.lib")
#pragma comment (lib,"Usp10.lib")
#pragma comment (lib,"Opengl32.lib")

extern "C"
{
	void __declspec(dllexport) *CreateRender(HWND h,char *Oput,int cpu_flags)
	{

		 static bool xxxaa=true;
		 if(xxxaa){
		     skpngZhuc();
		     xxxaa=false;
		 }
		 CRendGurd gd(m_RendLock);
		 IkkRender *m_pRender = NULL;
		 if(*Oput==0){
		            m_pRender =new CRenderGDI();
					m_pRender->init(h);
					*Oput=0;
				
		 }else if(*Oput==1){
		         m_pRender =new CRenderD3D(cpu_flags);
				 if(!m_pRender->init(h))
				 {
					delete m_pRender;
					m_pRender =new CRenderGDI();
					m_pRender->init(h);
					*Oput=0;
				 }else{
					*Oput=1;
				 }
		 }else if(*Oput==2){
		        m_pRender =new  CRenderRaw();
				*Oput=2;
		 }
		 return m_pRender;
	}


	char __declspec(dllexport) DelRender(void *p,char RenderType)
	{
          IkkRender *pIn=(IkkRender*)p;
		  if(pIn){

			  if(pIn->GetRenderType()== GDI_RENDER){
				   CRenderGDI *px=(CRenderGDI*)pIn;
				  delete px;
			  }else if(pIn->GetRenderType()==  D3D9_RENDER){
				 CRenderD3D *px=(CRenderD3D*)pIn;
				  delete px;
			  }else if(pIn->GetRenderType()==  RAW_RENDER){
				  CRenderRaw *px=(CRenderRaw*)pIn;
				  delete px;
			  }else{
			      assert(0);
			  }
			  return 1;
		  }
		 return 0;
	}




	//设置render 锁，初始化调用即可
	void __declspec(dllexport) SetDuiRenderLock(FpRenderLock pRenderLock,FpRenderUnLock pRenderUnLock)
	{
	      G_pRenderLock    = pRenderLock;
          G_pRenderUnLock  = pRenderUnLock;
	}


	void __declspec(dllexport) SetDxva2CreateFree(FpCreateDxva2Ctx  pCreateDxva2Ctx,FpFreeDxva2Ctx pFreeDxva2Ctx)
	{
	      G_pCreateDxva2Ctx    = pCreateDxva2Ctx;
          G_pFreeDxva2Ctx     =  pFreeDxva2Ctx;
	}


	void __declspec(dllexport) SetDXTextureCreateFree(FpCreateDXTexture  pCreateDXTexture,FpFreeDXTexture pFreeDXTexture)
	{
	     G_pCreateDXTexture = pCreateDXTexture;
		 G_pFreeDXTexture   = pFreeDXTexture;
	}

}

bool Utf8toWchar(wchar_t *pwstr,size_t len,const char *str)  
{  
    if(str){  
		  size_t nu = strlen(str);  
		  size_t n =(size_t)MultiByteToWideChar(CP_UTF8,0,( const char *)str,(int )nu,NULL,0);  
		   if(n>=len)    
			   n=len-1;  
		  MultiByteToWideChar(CP_UTF8,0,( const char *)str,(int )nu,pwstr,(int)n);  
		  pwstr[n]=0;  
		  return true;
    }  
	return false;
}  
void charTowchar(const char *chr, wchar_t *wchar, int size)  
{     
	MultiByteToWideChar( CP_ACP, 0, chr,  
		strlen(chr)+1, wchar, size/sizeof(wchar[0]) );  
}