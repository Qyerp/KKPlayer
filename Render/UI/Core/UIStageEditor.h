#ifndef  KKUISliceEditor_H_
#define  KKUISliceEditor_H_
#include "UIWindow.h"
#include "UIMoveState.h"
namespace KKUI
{
    //舞台编辑器
	class CUIStageEditor: public CUIWindow
	{
	    KKUI_CLASS_NAME(CUIWindow,CUIStageEditor,"uistageeditor")
	    public:
				  CUIStageEditor();
				  ~CUIStageEditor();
				  
				  void   SetStage (CUIMoveState*  pStage);
				  int         OnNcHitTest(kkPoint pt);
				   int         IsContainPoint(const kkPoint &pt,int bClientOnly);
				   CUIWindow*  WndFromPoint( kkPoint  ptHitTest, int bOnlyText);
	    protected:
			       void  OnLButtonDown  (unsigned int nFlags,CPoint pt);
				   void  OnLButtonUp(UINT nFlags, CPoint point);
				   void  OnMouseMove    (unsigned int nFlags,CPoint pt);
				   void  OnNcMouseMove  (unsigned int nFlags,CPoint pt);

			       void  OnSize         (unsigned int nType, int x,int y);
				   void  OnPaint        (IRenderTarget  *pRT);
	               void  OnEraseBkgnd   (IRenderTarget  *pRT);


				   
				   KKUI_MSG_MAP_BEGIN()   

					    MSG_UI_LBUTTONDOWN  (OnLButtonDown)
						MSG_UI_LBUTTONUP    (OnLButtonUp)
						MSG_UI_MOUSEMOVE    (OnMouseMove)
						MSG_UI_NCMOUSEMOVE  (OnNcMouseMove)
						MSG_UI_SIZE         (OnSize)
						MSG_UI_PAINT        (OnPaint)
						MSG_UI_ERASEBKGND   (OnEraseBkgnd)
					   
				   KKUI_MSG_MAP_END(CUIWindow)
	    private:
					CUIMoveState*                     m_pStage;
				    WarpInfo                          m_CurWarpInfo;
					//是否在拖动中
			        int                               m_bDraging;
	};
}
#endif