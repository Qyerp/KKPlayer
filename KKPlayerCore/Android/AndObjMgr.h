#ifndef AndObjMgr_H_
#define AndObjMgr_H_
#include "../stdafx.h"
#include "../KKLock.h"
#include "../AudioToPcm.h"
#include <jni.h>
#include "../stdafx.h"
#include <map>
typedef struct KKObjInfo
{
	void*    Obj;
	int      ObjHandle;
	int      ObjType;   //对象类型.
	jobject  JCall;     //一个Java回调对象
	jclass     Obj_class;
	jmethodID  OnRecPcmBufferId;
	int      UseCount;
	KKObjInfo()
	{
		Obj_class = 0;
		OnRecPcmBufferId = 0;
		Obj = 0;
		ObjHandle = 0;
		ObjType = 0;
		JCall = 0;
		UseCount = 0;
	}
}KKObjInfo;
class CAndObjMgr:public IRecPcm
{
	public:
	       CAndObjMgr();
	       ~CAndObjMgr();
		   int     CreateObj(int ObjType);
		   int     DeleteObj(JNIEnv *env, int ObjId);
		   void    Lock();
		   void    Unlock();
		   //使用时需要枷锁
		   KKObjInfo* GetObjInfo(int ObjId);

		   //设置对象回调
		   int        SetObjCall(JNIEnv *env, int ObjId, jobject icall);
		   int        AddRef(int ObjId);
		   int        ReduceRef(int ObjId);
	private:
		   void       OnRecPcm(void* UserData, int Id,int pts,int totalpts, void* buf, int buflen);
	       int                  m_nObjId;
	       CKKLock              m_Lock;
	       std::map<int,KKObjInfo>  m_ObjMap;
};
#endif