#ifndef UIWndStyle_H_
#define UIWndStyle_H_
#include "UIBase.h"
#define GETCOLOR(x)         KKUI::GetColor(x)
#include "ObjectImp.h"
#include "AttrCracker.h"
#include "UIMisc.h"
#include "Layout/LayoutSize.h"

namespace KKUI
{
	unsigned int  GetColor(const SStringA & strValue);
    bool ParseValue(const SStringA & strValue, unsigned int & value);
   class CUIWndStyle:public CObject
   {
		KKUI_CLASS_NAME_EX( CObject,SwndStyle, "style",Style)
		enum
		{
			Align_Left               = 0x000UL, // valign = top
			Align_Center             = 0x100UL, // valign = middle
			Align_Right              = 0x200UL, // valign = bottom

			VAlign_Top               = 0x0000UL, // valign = top
			VAlign_Middle            = 0x1000UL, // valign = middle
			VAlign_Bottom            = 0x2000UL, // valign = bottom
		};

	   public:
			CUIWndStyle();
			CRect GetMargin() const;
			CRect GetPadding() const;
       protected:
			int OnAttrMargin(const SStringA &strValue,int bLoading);
			void _ParseLayoutSize4(const SStringA & strValue, CLayoutSize layoutSizes[]);

			KKUI_ATTRS_BEGIN()
				 //背景颜色
				 ATTR_COLOR("colorBkgnd", m_crBg, 1);
			     //边框
			     ATTR_CUSTOM("margin", OnAttrMargin)
			     //边框颜色
				 ATTR_COLOR("colorBorder", m_crBorder, 1);
			KKUI_ATTRS_END()
  public:
           COLORREF m_crBg;                /**<背景颜色 */
		   COLORREF m_crBorder;            /**<边框颜色 */
		   int      m_bTrackMouseEvent;     //监视鼠标进入及移出消息
   protected:
	       int GetScale() const;

	       int		m_nScale;
	   	   CLayoutSize    m_rcMargin[4];   /**< 4周非客户区大小 */
	       CLayoutSize    m_rcInset[4];    /**< 文字区4个方向的内边距 */
		 
   };
}
#endif