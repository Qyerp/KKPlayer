#include "Yuv420p_Gl.h"
#include "FastMemcpy.h"
#include "RenderTarget_Gl.h"
namespace KKUI
{
	
	CYuv420p_Gl::CYuv420p_Gl(SRenderFactory_Gl* pRenderFactory)
		: m_pRenderFactory(pRenderFactory)
		, m_nY(0)
		, m_nU(0)
		, m_nV(0)
		, m_nRtt(0)
		, m_nRttTex(0)
		, m_nRgb(0)
	{
		m_pRenderFactory->AddRef();
		
	}
	CYuv420p_Gl::~CYuv420p_Gl()
	{
		m_pRenderFactory->Release();
	}
	void CYuv420p_Gl::Release2()
	{
		int i = 0;
	}

	void CYuv420p_Gl::ReLoadRes()
	{
		int i = 0;
	}

	kkSize CYuv420p_Gl::Size() const
	{
		return m_Size;
	}

	void CYuv420p_Gl::ClearPicData(unsigned int Color)
	{
		int i = 0;
	}

	void CYuv420p_Gl::SetQuoteObj(IPicYuv420p* pParentObj)
	{
		int i = 0;
	}
	
	int CYuv420p_Gl::Load(const kkPicInfo* data)
	{	
		m_pRenderFactory->Lock();
		m_pRenderFactory->MakeLoadContext();
		
		

		if (0 == m_nY)
		{
			glGenTextures(1, &m_nY);
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, m_nY);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

			//glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, bit->Width(), bit->Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, bit->GetBuffer());
		}

		if (0 == m_nU)
		{
			glGenTextures(1, &m_nU);
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, m_nU);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

			//glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, bit->Width(), bit->Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, bit->GetBuffer());
		}
		
		if (0 == m_nV)
		{
			glGenTextures(1, &m_nV);
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, m_nV);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

			//glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, bit->Width(), bit->Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, bit->GetBuffer());
		}
		
		if (0 == m_nRtt)
		{
			glGenFramebuffers(1, &m_nRtt);
			glBindFramebuffer(GL_FRAMEBUFFER, m_nRtt);


			glGenTextures(1, &m_nRttTex); // Create a empty texture as the render target
			glBindTexture(GL_TEXTURE_2D, m_nRttTex);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);


			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, data->width, data->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

			// Attach the texture object to frame buffer object
			
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_nRttTex, 0);

			GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
			if (status != GL_FRAMEBUFFER_COMPLETE)
			{

			}

			glBindFramebuffer(GL_FRAMEBUFFER, 0); // Unbind the frame buffer
		}

		/*if(data->picformat == 0||  data->picformat == 2 || data->picformat == 74)
			return  LoadYuv(data);
		else if(Picinfo->picformat == 1)*/
		LoadARGB(data);
		/*
		else if(Picinfo->picformat ==3)
			return LoadDxva2(Picinfo);
		else if(Picinfo->picformat ==4)
			return LoadDxTexture(Picinfo);
		else if(Picinfo->picformat == 28)
			return   LoadRGBA(Picinfo);*/

		m_pRenderFactory->UnLock();
		return 0;
	}

	GLuint CYuv420p_Gl::GetRttTexture()
	{
		return m_nRttTex;
	}

	int CYuv420p_Gl::LoadYuv(const kkPicInfo* data)
	{
		int nYWidth = data->width;
		int nYHeight = data->height;
		int nUWidth = 0;
		int nUHeight = 0;
		if(m_Size.cx != data->width || m_Size.cy != data->height)
		{
			if(data->picformat==0)
			{
				nUWidth = (data->width+1)/2;
				nUHeight =(data->height+1)/2;
			}
			else if(data->picformat==2 || data->picformat==1)
			{
				nUWidth = (data->width+1)/2;
				nUHeight =data->height;
			}
			else if(data->picformat==74) 
			{
				nUWidth = (data->width+1)/2;
				nUHeight =data->height+1;
			}


			m_Size.cx = data->width;
			m_Size.cy = data->height;
		}
			//Y
			//
			//glActiveTexture(GL_TEXTURE0);

			glBindTexture(GL_TEXTURE_2D, m_nY);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, nYWidth, nYHeight, 0, GL_RED, GL_UNSIGNED_BYTE, data->data[0]); 

			//glUniform1i(textureUniformY, 0);    
			//U

			//glActiveTexture(GL_TEXTURE1);
			glBindTexture(GL_TEXTURE_2D, m_nU);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, nUWidth, nUHeight, 0, GL_RED, GL_UNSIGNED_BYTE, data->data[1]);       
			//glUniform1i(textureUniformU, 1);
			//V

			//glActiveTexture(GL_TEXTURE2);
			glBindTexture(GL_TEXTURE_2D, m_nV);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, nUWidth, nUHeight, 0, GL_RED, GL_UNSIGNED_BYTE, data->data[2]);    
			//glUniform1i(textureUniformV, 2);   
		

		//glBindFramebuffer(GL_FRAMEBUFFER, m_nRtt); // Bind the frame buffer to render the scene to the texture
		//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		return 1;
	}

	int CYuv420p_Gl::LoadARGB(const kkPicInfo* data)
	{

		if (0 == m_nRgb)
		{
			glGenTextures(1, &m_nRgb);
			//glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, m_nRgb);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			

			//glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, bit->Width(), bit->Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, bit->GetBuffer());
		}

		if(m_Size.cx != data->width || m_Size.cy != data->height)
		{

			m_Size.cx = data->width;
			m_Size.cy = data->height;

		}

	    glViewport(0, 0,m_Size.cx, m_Size.cy);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glTranslatef(0.0, 0.0, 0.0);
		glOrtho(0.0, m_Size.cx, m_Size.cy, 0.0, 0.0, 1.0);
		/*	*/
		//glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, m_nRgb);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_Size.cx, m_Size.cy, 0, GL_BGRA, GL_UNSIGNED_BYTE, data->data[0]); 

		glBindFramebuffer(GL_FRAMEBUFFER, m_nRtt); // Bind the frame buffer to render the scene to the texture
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glColor4f(1.0, 1.0, 1.0, 1.0);
		

		//glEnable(GL_BLEND);
		//glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_TEXTURE_2D);
		glBegin(GL_TRIANGLE_STRIP);

		//����
		 glTexCoord2i(0, 0);
		 glVertex3f(0, m_Size.cy, 0);

		 //����
		 glTexCoord2i(1,0);
		 glVertex3f(m_Size.cx, m_Size.cy, 0);

		 //����
		 glTexCoord2i(0, 1);
		 glVertex3f(0, 0, 0);
		
		 //����
		 glTexCoord2i(1, 1);
		 glVertex3f(m_Size.cx,0, 0);

		glEnd();
		glDisable(GL_TEXTURE_2D);
		//glDisable(GL_BLEND);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		return 0;
	}

}