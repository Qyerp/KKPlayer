#ifndef GlEsYuv_H_
#define GlEsYuv_H_
#define GL_GLEXT_PROTOTYPES
#include <EGL/egl.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <android/log.h>
#include <jni.h>
#include "../KKPlayer.h"
class CGlEsYuv
{
	public:
	        CGlEsYuv();
			~CGlEsYuv();
			void Init();
			//加载数据
	        void LoadDtata(kkAVPicInfo *Picinfo);
			//显示调用
			void OnRender(GLfloat* m, GLfloat* AVPos);
	private:
        GLuint m_nTexYId;
        GLuint m_nTexUId;
        GLuint m_nTexVId; 
		GLuint m_nTex_y  ;
		GLuint m_nTex_u   ;
		GLuint m_nTex_v ;
		
		GLuint m_nProgram;
		GLuint m_Mvp;
        GLuint m_nAv4_pos;
		GLuint m_nAv2_Texpos;
		GLuint m_nVShader;
		GLuint m_nFraShader;
};
#endif