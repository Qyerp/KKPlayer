#ifndef UIMoveState_H_
#define UIMoveState_H_
#include "UIMoveWindow.h"
#include "IDisplayScale.h"
#include "UIFusionzone.h"
#include <vector>
#include "Warp/IWarp.h"
#include "Fusionzone/IFusionzone.h"
//舞台控件
namespace KKUI
{
	//不支持子窗口
    class  CUIMoveState: public  CUIMoveWindow,public IDisplayScale,public IFusionzoneGather
    {
		friend class CUIStageEditor;
        KKUI_CLASS_NAME( CUIMoveWindow,CUIMoveState,"uimovestate")
		public:
				 CUIMoveState(void);
				 ~CUIMoveState(void);

				 //获取相交的图层控件
				 std::vector<CUIWindow*>  GetIntersectCtlVec();
				 int            GetCtlAttr(const SStringA& attrName,SStringA& OutAttr);
				 //舞台绘制
	             void           OnStatgePaint(IRenderTarget  *pRT);

				 //舞台绘制编辑者绘制
	             void           OnStatgeEditorPaint(IRenderTarget  *pRT, CRect& rcParent);

				 //清理相交的控件
				 void           ClearIntersectCtl();
				 //添加相交的控件
				 void           AddIntersectCtl(CUIWindow* ctl);

				 void           DelIntersectCtl(CUIWindow* ctl);
				 void           OnUpdateFloatPosition(const  CSize&  szView,const CRect & rcParent);
				 
				 CUIWindow*     WndFromPoint( kkPoint  ptHitTest, int bOnlyText);
				 int            IsContainPoint(const kkPoint &pt,int bClientOnly);
				 int            OnNcHitTest(kkPoint pt);

				 /*******变形操作**********/
				 IWarp*         GetIWarp();
				 int            GetEnbleIWarp(){return m_nEnbleIWarp;}
				 void           SetEnbleIWarp(int  nEnbleIWarp){m_nEnbleIWarp=nEnbleIWarp;}
                 void           SetXYDiv(int x,int y);
				 void           GetXYDiv(int &x,int &y);
				 int            GetWarpInfos(SUVInfo* infos,int count);
				 int            SetWarpInfos(SUVInfo* infos,int count);
		
				 std::vector<IFusionzone*> GetIFusionzoneVect() ;
                 int            AddFusionzone(CUIFusionzone* Fusionzone);
                 void           RemoveFusionzone(CUIFusionzone* Fusionzone);
				 float          GetDisplayScale(){return  m_fDisplayScale;}
		protected:
			     CUIWindow*   SetCapture();
				 int    OnCreate( int);
				 void   OnPaint( IRenderTarget  *pRT);
				 void   OnEraseBkgnd( IRenderTarget  *pRT);
				 void   OnSize(unsigned int nType, int x,int y);


				 void   OnLButtonDown  (unsigned int nFlags,CPoint pt);
				 void   OnLButtonDBLClk(unsigned int nFlags,CPoint pt);

				 void   OnLButtonUp(UINT nFlags, CPoint point);
				 void   OnRButtonUp(unsigned int nFlags,CPoint pt);
			
			     void   OnNcLButtonUp(UINT nFlags,CPoint pt);

				 int    OnStateName(const SStringA& strValue, int bLoading);
				 void   OnNcLButtonDown(unsigned int nFlags,CPoint pt);
				 void   SetDisplayScale(float fDisplayScale);
				 void   OnMouseMove(unsigned int nFlags,CPoint pt);
				 void   OnKeyMove(EKEY_MOVE KeyMove);
				 void   OnNcMouseMove(unsigned int nFlags,CPoint pt);
				 void   OnNcMouseLeave(unsigned int nFlags);
				 //背景
				 int    OnAttrBkSkin(SStringA strValue,BOOL bLoading);
				 KKUI_ATTRS_BEGIN()
					    ATTR_CUSTOM("bkSkin",     OnAttrBkSkin)
						ATTR_CUSTOM("StateName",   OnStateName)
						ATTR_INT   ("Ligature",m_nLigature,0)
				 KKUI_ATTRS_ENDEX(CUIMoveWindow)

				 KKUI_MSG_MAP_BEGIN()
				         MSG_UI_CREATE(OnCreate)
						 MSG_UI_PAINT(OnPaint)
						 MSG_UI_SIZE(OnSize)

                         MSG_UI_LBUTTONDOWN  (OnLButtonDown)
						 MSG_UI_LBUTTONDBLCLK(OnLButtonDBLClk)
						 MSG_UI_NCLBUTTONDOWN(OnNcLButtonDown)

						 MSG_UI_MOUSEMOVE(OnMouseMove)
						 MSG_UI_NCMOUSEMOVE(OnNcMouseMove)
						 MSG_UI_NCMOUSELEAVE(OnNcMouseLeave)
						 MSG_UI_RBUTTONUP(OnRButtonUp)
						 MSG_UI_NCLBUTTONUP(OnNcLButtonUp)
			             MSG_UI_LBUTTONUP(OnLButtonUp)
						 MSG_UI_ERASEBKGND(OnEraseBkgnd)
				 KKUI_MSG_MAP_END(CUIMoveWindow)

				 void LoadData( IRenderTarget  *pRT);
		 private: 
			     unsigned   int m_nDisplayColor;
				 ISkinObj*                 m_pBkSkin;
			     std::vector<CUIWindow*>   m_IntersectCtlVec;
				 std::vector<IFusionzone*> m_FusionzoneVec;
				 IBitmap*    m_pBitmapLefTop;
				 IBitmap*    m_pBitmapRightBu;
				 IBitmap*    m_pBitmapCenter;
				 IFont*      m_pFont;
				 int         m_nNeedData;
				 // 1 为连线，0 未连线
				 int         m_nLigature;
				 float       m_fDisplayScale;
				 SStringA    m_strStateName;

				 WarpInfo    m_CurWarpInfo;
				 IWarp*      m_pIWarp;
				 int         m_nEnbleIWarp;
				 //磁吸状态
                 SMagnetRegion                m_sMagnetRegion;
	
    };

}

#endif