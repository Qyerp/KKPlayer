#include "UIYUV.h"
#include "UIMsView.h"
#include "SkinPool.h"
#include <math.h>
void OutputDebugStringKK(char* lpOutputString);
namespace KKUI
{
	void GetScalRect(kkRect& rt,const float& f);
	void GetScalRect(CRect& rt,const float& f);
	void ExtendNcRect(CRect* rt)
	{
	 
		rt->left -=5;
		rt->top  -=5;
		rt->bottom +=5;
		rt->right +=5;
	}
	int   CrossProduct(CPoint& A, CPoint& B, CPoint& C)
	{
		return (B.x - A.x) * (C.y - A.y) - (C.x - A.x) * (B.y - A.y);
	} 
	bool  OnSegment(CPoint P1, CPoint P2, CPoint Q)
	{
		int condition1 = 0, condition2 = 0;
			if (CrossProduct(P1, P2,Q) == 0)//条件1   
				condition1 = 1;

		if (
			(min(P1.x, P2.x) <= Q.x) && (Q.x <= max(P1.x, P2.x)) &&
			(min(P1.y, P2.y) <= Q.y) && (Q.y <= max(P1.y, P2.y))
			)//条件2   
			condition2 = 1;
		if (condition1 && condition2)
			return true;
		else
			return false;
	}

	int   PtInCircle(CPoint texpos,CPoint RadiusXY,long nRadius)
	{
		//判断是否在圆内
		int lx = pow((double)abs(texpos.x - RadiusXY.x),2) + pow((double)abs(texpos.y - RadiusXY.y),2);
		int lx2= lx-pow((double)nRadius,2);
		if(lx2 <= 0)
			return 1;
		return 0;
	}

	//测试是否在多边形内
	int   CShapeMask::PnPoly(CPoint pt,CRect Rt)
	{
		  int i, j, c = 0;
		  CPoint*     pPtS = 0; 
		  CPoint      PtS[20];

		  CPoint m_PtS[20];

		  CPoint OPt;
		  m_pIRotate->GetCenterPt2(OPt,&Rt);
          int nRotate = m_pIRotate->GetRotate();

          for(int i = 0; i < m_nPtCount;  i++)
		  {	 
			  m_PtS[i].x = m_UVS[i].x * Rt.Width() + Rt.left;
			  m_PtS[i].y = m_UVS[i].y * Rt.Height() + Rt.top;

			  if(nRotate){
			      kkPtbyRotate( m_PtS[i],OPt,nRotate);
		       }

		  }
		  m_PtS[m_nPtCount] = m_PtS[0];
          pPtS = m_PtS;
          i= j = c = 0;
		  for (i = 0, j = m_nPtCount-1; i < m_nPtCount; j = i++) 
		  {
			if ( ((pPtS[i].y>pt.y) != (pPtS[j].y>pt.y)) && 
			(pt.x < (pPtS[j].x-pPtS[i].x) * (pt.y-pPtS[i].y) / (pPtS[j].y-pPtS[i].y) + pPtS[i].x) )
			  c = !c;
		  }
		  
          return c;
	}

	//测试点是否在区域内
	int   PtInPoly(CPoint pt,CPoint* pPtS,int nPtCount)
	{
	      int i, j, c = 0;
		  for (i = 0, j = nPtCount-1; i < nPtCount; j = i++) 
		  {
			if ( ((pPtS[i].y>pt.y) != (pPtS[j].y>pt.y)) && 
			(pt.x < (pPtS[j].x- pPtS[i].x) * (pt.y-pPtS[i].y) / (pPtS[j].y-pPtS[i].y) + pPtS[i].x) )
			  c = !c;
		  }
          return c;
	}
	bool  CShapeMask::PtInVert(CPoint pt,CRect Rt,int r,int& Out)
	{
		  CPoint m_PtS[20];
		  CPoint OPt;
		  m_pIRotate->GetCenterPt2(OPt,&Rt);
          int nRotate = m_pIRotate->GetRotate();

          for(int i = 0; i < m_nPtCount;  i++)
		  {	 
			  m_PtS[i].x = m_UVS[i].x * Rt.Width() + Rt.left;
			  m_PtS[i].y = m_UVS[i].y * Rt.Height() + Rt.top;
			  if(nRotate){
			      kkPtbyRotate( m_PtS[i],OPt,nRotate);
		       }
		  }
		  m_PtS[m_nPtCount] = m_PtS[0];
		  for (int i = 0; i < m_nPtCount;  i++) 
		  {
			  if(PtInCircle( pt,m_PtS[i],r )){
				  Out = i;
				  return 1;
			  }
		  }
		  return 0;
	}
	int   CShapeMask::InsertVert( SUVInfo pt,int Index)
	{
	     for(int i=m_nPtCount;i>Index;i--)
		 {
		    m_UVS[i] = m_UVS[i-1];
		 }
		 m_UVS[++Index] = pt;
		 m_nPtCount++;
		 m_UVS[m_nPtCount] = m_UVS[0];
		 return 1;
	}
	
	CPoint  CShapeMask::GetPt(int i,CRect Rt)
	{
		CPoint pt;
	    pt.x = m_UVS[i].x * Rt.Width()+ Rt.left;
		pt.y = m_UVS[i].y * Rt.Height() + Rt.top;
		return pt;
	}
	//测试是否在线段上
	bool  CShapeMask::PtInSegment(CPoint pt,CRect Rt,int& Out)
	{

		  CPoint m_PtS[20];
          for(int i = 0; i < m_nPtCount;  i++)
		  {	 
			  m_PtS[i].x = m_UVS[i].x * Rt.Width()  + Rt.left;
			  m_PtS[i].y = m_UVS[i].y * Rt.Height() + Rt.top;
		  }
		  m_PtS[m_nPtCount] = m_PtS[0];

		  for(int i = 0; i < m_nPtCount;  i++)
		  {

			  CPoint PtS[4];
			  PtS[0] = m_PtS[i];
			  PtS[0].x-=1;
			  PtS[0].y-=1;
			  PtS[1] = m_PtS[i+1];
			   PtS[1].x-=1;
			  PtS[1].y-=1;

			  PtS[2] = m_PtS[i+1];
			  PtS[2].x+=2;
			  PtS[2].y+=2;

			  PtS[3] = m_PtS[i];
			  PtS[3].x+=2;
			  PtS[3].y+=2;

               if(PtInPoly( pt,PtS,4))
			   {
			        Out = i;
					return 1;
			   }
		  }
          

		  if(OnSegment(m_PtS[m_nPtCount-1], m_PtS[0], pt))
				return 1;
		  return 0;
	}
	CSquareMask::CSquareMask(IRotate* p):CShapeMask(p)
	{
        m_nPtCount = 4;
		type = SquareMask;
		
	}
    CSquareMask::~CSquareMask()
	{
		
	}
    void   CSquareMask::MoveMask(float xx,float yy)
	{
		for(int j=0;j<m_nPtCount;j++)
		{
			m_UVS[j].x-=xx;
			m_UVS[j].y-=yy;
		}
		m_UVS[m_nPtCount]=m_UVS[0];
	
	}


	CTriangleMask::CTriangleMask(IRotate* p):CShapeMask(p)
	{
	    m_nPtCount = 3;
		type = TriangleMask;
	}
	CTriangleMask::~CTriangleMask()
	{
	
	
	}
	void CTriangleMask::MoveMask(float xx,float yy)
	{
	    for(int j=0;j<m_nPtCount;j++)
		{
			m_UVS[j].x-=xx;
			m_UVS[j].y-=yy;
		}
		m_UVS[m_nPtCount]=m_UVS[0];
	}
	

	CUIYUV::CUIYUV():
	    m_nIsRect(1),
		m_fPosX1(0.0),
		m_fPosY1(0.0),
		m_fPosX2(1.0),
		m_fPosY2(0.0),
		m_fPosX3(1.0),
		m_fPosY3(1.0),
		m_fPosX4(0.0),
		m_fPosY4(1.0),
		///
        m_fPicX1(0.0),
		m_fPicY1(0.0),
		m_fPicX2(1.0),
		m_fPicY2(0.0),
		m_fPicX3(1.0),
		m_fPicY3(1.0),
		m_fPicX4(0.0),
		m_fPicY4(1.0),
        ///
		m_nAdjustSize(0),
	    m_pIPicYuv420p(0),
		m_pQuoteYuvCtl(0),
		m_nAlpha(255),
		m_nAlpha2(255),
		m_nColorR(255),
		m_nColorG(255),
		m_nColorB(255),
		m_nMaskId(0),
		m_pHoverMask(0),
		m_pCircleSkin(0),
		m_nVertIndex(-1),
	   
		m_nDisplayYuv(1),
		m_pFont(0),
		m_nRotate(0),
		m_pBkSkin(0),
		m_pFlagSkin(0),
		m_fDisplayScale(1.0),
		m_nFadeInMsec(0),
		m_nFadeInMsec2(0),
		m_nFadeOutMsec(0),
		m_nFadeOutMsec2(0),
		m_pHeaderFilter(0),
		m_pTailFilter(0),
		m_pVertInfo(0)
	{
		m_pVertInfo = new SVertInfo[20];
		//磁吸状态
		m_sMagnetRegion.LeftTop =EMagnetState::EMS_NO;
	    m_sMagnetRegion.RightTop =EMagnetState::EMS_NO;

	    m_sMagnetRegion.LeftBottom =EMagnetState::EMS_NO;
	    m_sMagnetRegion.RightBottom =EMagnetState::EMS_NO;
	
	}
	CUIYUV::~CUIYUV()
	{
		delete m_pVertInfo;
		if(this->GetContainer())
		   this->GetContainer()->UnregisterTimelineHandler(this);
		if(m_pIPicYuv420p!=0)
			m_pIPicYuv420p->Release();
		m_pIPicYuv420p = 0;

		
		if(m_pQuoteYuvCtl!=0)
			m_pQuoteYuvCtl->Release();
		m_pQuoteYuvCtl = 0;

	}
	void  CUIYUV::OnNextFrame()
	{
	    //淡入毫秒
		if(m_nFadeInMsec > 0)
		{
			 float ff = 1 - (float)m_nFadeInMsec / m_nFadeInMsec2;
			 m_nAlpha = ff* 255;
			 m_nFadeInMsec -=20;
		}else{
		      m_nAlpha =255;
		}
			
        //淡出毫秒
		if( m_nFadeOutMsec > 0)
		{
			 float ff        =(float) m_nFadeOutMsec/ m_nFadeOutMsec2;
			 m_nAlpha2       = ff* 255;
			 m_nFadeOutMsec -=20;
		}else{
		      m_nAlpha2 =0;
		}

		SinkFilter* pFilter = m_pHeaderFilter;
		while(pFilter)
		{
           
			if(!strcmp(pFilter->FilterName,"Sin波形")){
				pFilter->Par1++;
				if(pFilter->Par1%pFilter->Par2==0)
				{
				    pFilter->Par7+=0.5;
				}
			}else if(!strcmp(pFilter->FilterName,"闪屏")){
			    
				pFilter->Par2++;
				if(pFilter->Par2%pFilter->Par3==0)
				{
				   if(pFilter->Par1)
					   pFilter->Par1=0;
				   else
					   pFilter->Par1=1;
				}
			}
			pFilter =pFilter->Next;
			
		}
	}
	void    CUIYUV::InitAfter()
	{
	     this->GetContainer()->RegisterTimelineHandler(this);
	}
    int   CUIYUV::OnCreate( int)
    {    
	     if(m_pIPicYuv420p==0){
			 this->GetContainer()->GetIRenderFactory()->CreateYUV420p(&m_pIPicYuv420p);
		 }

	     CUIWindow* pa =this->GetParent();
		 if(pa->IsClass("uimsview"))
		 {
			 CUIMsView* view = (CUIMsView*)pa;
			 float f=  view->GetDisplayScale();
			 m_fDisplayScale =f;
		 }
         return 0;
    }
	void  CUIYUV::OnDestroy()
	{
		this->GetContainer()->UnregisterTimelineHandler(this);
	}
	void  CUIYUV::GetRectByPt(const CPoint& pt,CRect& rt,int wh)
	{
	     rt.left = pt.x-wh;
		 rt.top = pt.y-wh;

		 rt.right = pt.x+wh;
		 rt.bottom = pt.y+wh;
	}
	void  CUIYUV::OnPaint( IRenderTarget  *pRT)
	{
		bool Active=false;
        COLORREF crBorder=m_style.m_crBorder;  
		CUIWindow*  winp =this->GetParent();
		if(winp){
			if(winp->IsClass("uimsview")){

				CUIMsView *ms=(CUIMsView*)winp ;
				if(ms->GetActiveYUV()==this){
			       crBorder=m_crBorderCapture;
				   Active=true;
				}
			}
		}
		
        CRect rt;
		GetClientRect(&rt);
		GetScalRect(rt,m_fDisplayScale);
	    kkPoint OPt ={0,0};
		OPt.x = rt.left + (rt.right -rt.left)/2;
		OPt.y = rt.top  + (rt.bottom -rt.top)/2;

		IPicYuv420p* pIPicYuv420p  = m_pIPicYuv420p;
	
		if(m_pQuoteYuvCtl){
			pIPicYuv420p  = m_pQuoteYuvCtl->GetIPicYuv420p();
		}

		if(pIPicYuv420p&&m_nDisplayYuv){
			
		
            int MaskCount=0;
			SMaskInfo maskinfo[10];
			if(m_MaskVect.size()>0)
			{
				MaskCount = m_MaskVect.size();
				MaskCount= MaskCount > 10 ?  10:MaskCount;
				CShapeMask* msk =0;
				for(int i=0;i<MaskCount;++i)
				{
				    msk =m_MaskVect.at(i);
				    maskinfo[i].uvinfo =msk->m_UVS;
					
					maskinfo[i].count  =msk->m_nPtCount;
					maskinfo[i].masktype  =msk->GetMaskType();
					maskinfo[i].hidepx  =msk->m_nHidePx;
				}
			}

	
			kkPoint OutPts[4]={0,0};			 
			GetRegionbyRotate(&rt,OPt,m_nRotate, OutPts,4);

		
		   SPalette Palette;
		   Palette.Alpha = m_nAlpha;
		   
		   m_pVertInfo[0].x =  OutPts[3].x;
		   m_pVertInfo[0].y =  OutPts[3].y;
		   m_pVertInfo[0].uv.x =m_fPicX4;
		   m_pVertInfo[0].uv.y =m_fPicY4;

		   m_pVertInfo[1].x =  OutPts[0].x;
		   m_pVertInfo[1].y =  OutPts[0].y;
		   m_pVertInfo[1].uv.x =m_fPicX1;
		   m_pVertInfo[1].uv.y =m_fPicY1;

		   m_pVertInfo[2].x =  OutPts[2].x;
		   m_pVertInfo[2].y =  OutPts[2].y;
		   m_pVertInfo[2].uv.x = m_fPicX3;
		   m_pVertInfo[2].uv.y  =m_fPicY3;

		   
		   m_pVertInfo[3].x    =  OutPts[0].x;
		   m_pVertInfo[3].y    =  OutPts[0].y;
		   m_pVertInfo[3].uv.x =  m_fPicX1;
		   m_pVertInfo[3].uv.y =  m_fPicY1;

		   m_pVertInfo[4].x     =  OutPts[1].x;
		   m_pVertInfo[4].y     =  OutPts[1].y;
		   m_pVertInfo[4].uv.x  =  m_fPicX2;
		   m_pVertInfo[4].uv.y  =  m_fPicY2;

		   m_pVertInfo[5].x     =  OutPts[2].x;
		   m_pVertInfo[5].y     =  OutPts[2].y;
		   m_pVertInfo[5].uv.x  =  m_fPicX3;
		   m_pVertInfo[5].uv.y  =  m_fPicY3;




		   
			
		   // if(!pIPicYuv420p->HasPreFrame())
			pRT->DrawYuv420p(pIPicYuv420p,m_pHeaderFilter,Palette, m_pVertInfo,6,0,maskinfo,MaskCount);

			if(pIPicYuv420p->HasPreFrame()){
				  Palette.Alpha =m_nAlpha2;
			      pRT->DrawYuv420p(pIPicYuv420p,m_pHeaderFilter,Palette, m_pVertInfo,6,1,maskinfo,MaskCount);
			}/**/
		}else{
			//pRT->FillRect(&rt,0xFF0000

        }
		
		//绘制mask
	    if(Active)
		{
			COLORREF crBorder = m_crBorderCapture;
			CRect  TempRt;
			CPoint TempPt1 ;
			CPoint TempPt2 ;

			for(int i=0;i< m_MaskVect.size();i++)
			{
				CShapeMask* msk = m_MaskVect.at(i);
				if(msk->GetMaskType() ==SquareMask||msk->GetMaskType() ==TriangleMask){
					for(int j=0;j<msk->m_nPtCount;j++){
						 CPoint TempPt1 = msk->GetPt(j,rt);
			             CPoint TempPt2 = msk->GetPt(j+1,rt);
						 if(m_nRotate){
							  kkPtbyRotate(TempPt1,OPt,m_nRotate);
							  kkPtbyRotate(TempPt2,OPt,m_nRotate);
						 }
						 pRT->DrawLine(TempPt1.x,TempPt1.y,TempPt2.x,TempPt2.y, crBorder);
					}/**/

					if(m_pCircleSkin){
						CRect Winrt;
						GetWindowRect(&Winrt);
						GetScalRect(Winrt,m_fDisplayScale);
						for(int j=0;j<msk->m_nPtCount;j++)
						{
							 CPoint TempPt1 = msk->GetPt(j, Winrt);
							 if(m_nRotate){
								  kkPtbyRotate(TempPt1,OPt,m_nRotate);
								
							 }

							 GetRectByPt( TempPt1,TempRt);
							 m_pCircleSkin->Draw(pRT,TempRt,0,255);
							 pRT->DrawCircle(TempPt1.x,TempPt1.y,8,crBorder);

						 }
					}
				}
			}
		}


        int index=0;
		if(m_pHoverMask && m_pHoverMask->PtInSegment(m_ptClick,rt,index) && !m_pHoverMask->PtInVert(m_ptClick,rt,8,index)){

			CPoint TempPt1=m_ptClick;
			if(m_nRotate){
					kkPtbyRotate(TempPt1,OPt,m_nRotate);
								
			}/**/
			 pRT->DrawCircle( TempPt1.x, TempPt1.y,8,crBorder);
		}
		
	}
    void  CUIYUV::OnPaintEx( IRenderTarget  *pRT,CRect* rt)
	{
		
	    kkPoint OPt ={0,0};
		OPt.x = rt->left + ( rt->right - rt->left)/2;
		OPt.y = rt->top  + ( rt->bottom - rt->top)/2;

		IPicYuv420p* pIPicYuv420p  = m_pIPicYuv420p;
		if(m_pQuoteYuvCtl){
			pIPicYuv420p  = m_pQuoteYuvCtl->GetIPicYuv420p();
		}

	    if(pIPicYuv420p&&m_nDisplayYuv){
			int MaskCount=0;
			SMaskInfo maskinfo[10];
			if(m_MaskVect.size()>0)
			{
				MaskCount = m_MaskVect.size();
				MaskCount= MaskCount > 10 ?  10:MaskCount;
				CShapeMask* msk =0;
				for(int i=0;i<MaskCount;++i)
				{
				    msk =m_MaskVect.at(i);
					//msk->RefreshUVMap(rt);
				    maskinfo[i].uvinfo =msk->m_UVS;
					
					maskinfo[i].count  =msk->m_nPtCount;
					maskinfo[i].masktype  =msk->GetMaskType();
					maskinfo[i].hidepx  =msk->m_nHidePx;
				}
			}

	       SPalette Palette;
		   Palette.Alpha = m_nAlpha;
		   kkPoint OutPts[4]={0,0};			 
		   GetRegionbyRotate(rt,OPt,m_nRotate, OutPts,4);

		  
		   m_pVertInfo[0].x =  OutPts[3].x;
		   m_pVertInfo[0].y =  OutPts[3].y;
		   m_pVertInfo[0].uv.x =m_fPicX4;
		   m_pVertInfo[0].uv.y =m_fPicY4;

		   m_pVertInfo[1].x =  OutPts[0].x;
		   m_pVertInfo[1].y =  OutPts[0].y;
		   m_pVertInfo[1].uv.x =m_fPicX1;
		   m_pVertInfo[1].uv.y =m_fPicY1;

		   m_pVertInfo[2].x =  OutPts[2].x;
		   m_pVertInfo[2].y =  OutPts[2].y;
		   m_pVertInfo[2].uv.x  = m_fPicX3;
		   m_pVertInfo[2].uv.y  = m_fPicY3;

		   
		   m_pVertInfo[3].x    =  OutPts[0].x;
		   m_pVertInfo[3].y    =  OutPts[0].y;
		   m_pVertInfo[3].uv.x =  m_fPicX1;
		   m_pVertInfo[3].uv.y =  m_fPicY1;

		   m_pVertInfo[4].x     =  OutPts[1].x;
		   m_pVertInfo[4].y     =  OutPts[1].y;
		   m_pVertInfo[4].uv.x  =  m_fPicX2;
		   m_pVertInfo[4].uv.y  =  m_fPicY2;

		   m_pVertInfo[5].x     =  OutPts[2].x;
		   m_pVertInfo[5].y     =  OutPts[2].y;
		   m_pVertInfo[5].uv.x  =  m_fPicX3;
		   m_pVertInfo[5].uv.y  =  m_fPicY3;


		  // m_fPicStartX,m_fPicStartY,m_fPicEndX,m_fPicEndY
	       pRT->DrawYuv420p(pIPicYuv420p,m_pHeaderFilter,Palette,m_pVertInfo,6,0,maskinfo,MaskCount);
		  // pRT->FillRect(rt,0xFFFF0000);
		   if(pIPicYuv420p->HasPreFrame()){
				  Palette.Alpha = m_nAlpha2;
				  if(Palette.Alpha>0)
			      pRT->DrawYuv420p(pIPicYuv420p,m_pHeaderFilter,Palette,m_pVertInfo,6,1,maskinfo,MaskCount);
			}
		}
	}
	void  CUIYUV::OnEraseBkgnd( IRenderTarget  *pRT)
	{
	    
		
	}

	void  CUIYUV::GetRegionbyRotate(lpkkRect lprc,const kkPoint &OPt,float Rotate,kkPoint *OutPts,int PtsOut)
	{
		if(m_nIsRect)
		{
	       kkRectbyRotate(lprc,OPt,m_nRotate, OutPts,4);
		   return;
		}

		int width =lprc->right - lprc->left;
		int height =lprc->bottom - lprc->top;
		kkPoint LeftTop  = {lprc->left  +    width*m_fPosX1,lprc->top+height*m_fPosY1};
		kkPtbyRotate(LeftTop,OPt,Rotate);

		kkPoint RightTop = {lprc->left  +    width*m_fPosX2,lprc->top+height*m_fPosY2};
		kkPtbyRotate(RightTop,OPt,Rotate);

		kkPoint RightBottom = {lprc->left+   width*m_fPosX3,lprc->top+height*m_fPosY3};
		kkPtbyRotate(RightBottom,OPt,Rotate);

		kkPoint LeftBottom  = {lprc->left+   width*m_fPosX4,lprc->top+height*m_fPosY4};
		kkPtbyRotate(LeftBottom,OPt,Rotate);

		OutPts[0] = LeftTop;
		OutPts[1] = RightTop;
		OutPts[2] = RightBottom;
		OutPts[3] = LeftBottom;
		
	}
	void  CUIYUV::OnNcPaint(IRenderTarget  *pRT)
	{
	    CRect rt;
		GetClientRect(&rt);
        GetScalRect(rt,m_fDisplayScale);
		kkPoint OutPts[4]={0,0};
		bool Active=false;
		kkRect rcWindow;
		GetWindowRect(&rcWindow);
		GetScalRect(rcWindow,m_fDisplayScale);

        COLORREF crBorder=m_style.m_crBorder;  
		unsigned int lxxx= 0;
		CUIWindow*  winp =this->GetParent();
		if(winp){
			if(winp->IsClass("uimsview")){

				CUIMsView *ms=(CUIMsView*)winp ;
				if(ms->GetActiveYUV()==this){
			     //  crBorder=m_crBorderCapture;
				   Active=true;
				}else{
				   //return;
				}
			}
		}
		
		
         
	
     
		    if(!m_style.GetMargin().IsRectNull())
			{
				if(crBorder!= CR_INVALID)
				{
					
		              CPoint OPt;
					  GetCenterPt2(OPt,&rcWindow);

					 
					  GetRegionbyRotate(&rcWindow,OPt,m_nRotate, OutPts,4);

					 


						   
							  //Top
							  pRT->DrawLine( OutPts[0].x, OutPts[0].y, OutPts[1].x, OutPts[1].y,crBorder);
							 
							  
							  //right
							  pRT->DrawLine( OutPts[1].x, OutPts[1].y, OutPts[2].x, OutPts[2].y,crBorder);
							 

							  //bottom
							  pRT->DrawLine( OutPts[2].x, OutPts[2].y, OutPts[3].x, OutPts[3].y,crBorder);
							 

							  //left
							  pRT->DrawLine( OutPts[3].x, OutPts[3].y, OutPts[0].x, OutPts[0].y,crBorder);
							  
						
						 
							  if( Active){
								   pRT->DrawLine( OutPts[0].x, OutPts[0].y+1, OutPts[1].x, OutPts[1].y+1,crBorder);
								   pRT->DrawLine( OutPts[1].x-1, OutPts[1].y, OutPts[2].x-1, OutPts[2].y,crBorder);
								   pRT->DrawLine( OutPts[2].x, OutPts[2].y-1, OutPts[3].x, OutPts[3].y-1,crBorder);
                                   pRT->DrawLine( OutPts[3].x+1, OutPts[3].y, OutPts[0].x+1, OutPts[0].y,crBorder);
								   
							  }
					 
							  if(m_nAdjustSize)
							  {
								   if(m_nIsRect)
										   {
													  COLORREF crBorder2=crBorder;
													  if( m_nPtIndex==1)
													  {
														  crBorder2=0xFFFFFF00;
													  }else{
														  crBorder2=crBorder;
													  }

													  kkPoint PtNc[4]={0,0};
													  GetJustPtsInNcRect(rcWindow,PtNc);
													  CRect TempRt;
													  GetRectByPt( PtNc[0],TempRt,4);
													  pRT->FillRect(&TempRt,crBorder2);

													  if( m_nPtIndex==2)
													  {
														  crBorder2=0xFFFFFF00;
													  }else{
														  crBorder2=crBorder;
													  }
													  GetRectByPt( PtNc[1],TempRt,4);
													  pRT->FillRect(&TempRt,crBorder2);

													  if( m_nPtIndex==3)
													  {
														  crBorder2=0xFFFFFF00;
													  }else{
														  crBorder2=crBorder;
													  }
													  GetRectByPt( PtNc[2],TempRt,4);
													  pRT->FillRect(&TempRt,crBorder2);

													  if( m_nPtIndex==4)
													  {
														  crBorder2=0xFFFFFF00;
													  }else{
														  crBorder2=crBorder;
													  }
													  GetRectByPt( PtNc[3],TempRt,4);
													  pRT->FillRect(&TempRt,crBorder2);

								   }else if(!m_nIsRect)
									  {

										  COLORREF crBorder2=crBorder;
										  if( m_nPtIndex==1)
										  {
											  crBorder2=0xFFFFFF00;
										  }else{
											  crBorder2=crBorder;
										  }
										  CPoint TempPt1(OutPts[0]);
										 
										  pRT->DrawCircle(TempPt1.x,TempPt1.y,8,crBorder2);


										  if( m_nPtIndex==2)
										  {
											  crBorder2=0xFFFFFF000;
										  }else{
											  crBorder2=crBorder;
										  }
										  TempPt1 = OutPts[1];
									     
										  pRT->DrawCircle(TempPt1.x,TempPt1.y,8,crBorder2);

										  if( m_nPtIndex==3)
										  {
											  crBorder2=0xFFFFFF00;
										  }else{
											  crBorder2=crBorder;
										  }
										  TempPt1 = OutPts[2];
									    
										  pRT->DrawCircle(TempPt1.x,TempPt1.y,8,crBorder2);


										  if( m_nPtIndex==4)
										  {
											  crBorder2=0xFFFFFF00;
										  }else{
											  crBorder2=crBorder;
										  }
										  TempPt1 = OutPts[3];
									 
										  pRT->DrawCircle(TempPt1.x,TempPt1.y,8,crBorder2);

									  }
							  
							  }

#ifdef _DEBUG
					  if(m_nRotate){
						      pRT->DrawLine( rcWindow.left, rcWindow.top, rcWindow.right, rcWindow.top,crBorder);
							  pRT->DrawLine( rcWindow.right, rcWindow.top, rcWindow.right, rcWindow.bottom,crBorder);
							  pRT->DrawLine( rcWindow.right,rcWindow.bottom, rcWindow.left,rcWindow.bottom,crBorder);
							  pRT->DrawLine(  rcWindow.left, rcWindow.top,rcWindow.left,rcWindow.bottom,crBorder);
					  }
#endif
					  
			}
		}

		if(m_strTxt.length()>0){

			if(m_pFont==0){
			      this->GetContainer()->GetIRenderFactory()->CreateFont(&m_pFont,12,L"宋体");
		    }
			rt.left+=10;
			rt.top+=10;
		
			pRT->DrawText(m_pFont,(wchar_t*)m_strTxt.c_str(),&rt);
		}
		if(m_pFlagSkin){

			 kkSize sz=m_pFlagSkin->GetSkinSize();
			 CRect rt;
			 GetClientRect(&rt);
			 rt.left+=5;
			 rt.right=rt.left+sz.cx;
			 rt.top  =rt.bottom-sz.cy-2;
             rt.bottom-=2;
			 m_pFlagSkin->Draw(pRT,rt,0);
		}
	}
	int   CUIYUV::OnAttrVSkin( SStringA strValue,BOOL bLoading )
	{
		ISkinObj *pSbSkin=GETSKIN(strValue,GetScale());
		assert(pSbSkin);
		if(!pSbSkin->IsClass(CSkinImgFrame::GetClassName()))
			return E_FAIL;
		m_pCircleSkin=(CSkinImgFrame*)pSbSkin;
		assert(m_pCircleSkin);
		return bLoading?S_FALSE:S_OK;
	}
    int   CUIYUV::OnAttrBkSkin(SStringA strValue,BOOL bLoading)
	{
		if(!strValue.Compare("")){
			ISkinObj *pSbSkin=GETSKIN(strValue,GetScale());
			assert(pSbSkin);
			if(!pSbSkin->IsClass(CSkinImgFrame::GetClassName()))
				return E_FAIL;
			m_pBkSkin = pSbSkin;
		}else{
		    m_pBkSkin = 0;
		}
		return S_OK;
	}
	int   CUIYUV::OnAttrFlagSkin(SStringA strValue,BOOL bLoading)
	{
	   if(strValue.Compare("")){
			ISkinObj *pSbSkin=GETSKIN(strValue,GetScale());
			assert(pSbSkin);
			if(!pSbSkin->IsClass(CSkinImgFrame::GetClassName()))
				return E_FAIL;
			m_pFlagSkin = pSbSkin;
		}else{
		    m_pFlagSkin = 0;
		}
		return S_OK;
	}
	 int    CUIYUV::OnFadeInMsec(SStringA strValue,BOOL bLoading)
	 {
			if(strValue.Compare("")){
				m_nAlpha = 0;
				m_nFadeInMsec = m_nFadeInMsec2 = atoi(strValue.GetBuffer(100));
			}else{
				m_nFadeInMsec =m_nFadeInMsec2= 0;
				m_nAlpha=255;
			}
			return S_OK;
	 }
	int    CUIYUV::OnFadeOutMsec(SStringA strValue,BOOL bLoading)
	{
	     if(strValue.Compare("")){
				m_nAlpha2 =255;
				m_nFadeOutMsec = m_nFadeOutMsec2 = atoi(strValue.GetBuffer(100));
			}else{
				m_nFadeOutMsec = m_nFadeOutMsec2= 0;
				m_nAlpha2 = 0;
			}
			return S_OK;
	}
	void  CUIYUV::MoveMask(int xx,int yy)
	{
        for(int i=0;i< m_MaskVect.size();i++)
		{
			CShapeMask* msk = m_MaskVect.at(i);
			msk->MoveMask(xx,yy);
		}
	}
	void  CUIYUV::OnUpdateFloatPosition(const  CSize&  szView,const CRect & rcParent)
	{
	    CRect rt;
		CUIWindow::GetWindowRect(&rt);
	
		int x=m_szView.cx -szView.cx;
		int y=m_szView.cy -szView.cy;
        m_szView = szView;
		
        CSize sz(GetLayoutParam()->GetSpecifiedSize(Horz).toPixelSize(GetScale()),GetLayoutParam()->GetSpecifiedSize(Vert).toPixelSize(GetScale()));

		
		CRect rcWnd(0,0,sz.cx,sz.cy);
		rcWnd.MoveToX(rt.left+x);
		rcWnd.MoveToY(rt.top+y);
       
		
		
       //Move(&rcWnd);
		OnRelayout(&rcWnd);
	}
	void       CUIYUV::SetClip(SUVInfo* pPts,int ptCount)
	{
		 m_fPicX1 = pPts->x;
		 m_fPicY1 = pPts->y;

		 m_fPicX2= (pPts+1)->x;
		 m_fPicY2= (pPts+1)->y;

		 m_fPicX3= (pPts+2)->x;
		 m_fPicY3= (pPts+2)->y;

		 m_fPicX4= (pPts+3)->x;
		 m_fPicY4= (pPts+3)->y;
	}
	void       CUIYUV::GetClip(SUVInfo* pPts,int ptCount)
	{
	     pPts->x = m_fPicX1;
		 pPts->y = m_fPicY1;

		 (pPts+1)->x = m_fPicX2;
		 (pPts+1)->y = m_fPicY2;

		 (pPts+2)->x = m_fPicX3;
		 (pPts+2)->y = m_fPicY3;

		 (pPts+3)->x =  m_fPicX4;
		 (pPts+3)->y =  m_fPicY4;
	}
	void      CUIYUV::SetRotate(int Rotate)
	{
	     m_nRotate = Rotate;
	}
	int    CUIYUV::GetRotate()
	{
	    return m_nRotate;
	}
	void   CUIYUV::SetIsRect(int IsRect)
	{
	    m_nIsRect=IsRect;
		this->Invalidate();
	}
	int    CUIYUV::GetIsRect()
	{
	   return m_nIsRect;
	}
	void       GenRectByPt(kkPoint* pt,int width,int height,CRect& rt)
   {
         
		 int w = width/2;
		 int h = height/2;

		 rt.left   = pt->x-w;
		 rt.right  = pt->x+w;
		 rt.top    = pt->y-h;
		 rt.bottom = pt->y+h;
	}

	
	

	
	IPicYuv420p*   CUIYUV::GetIPicYuv420p()
	{
		if(m_pQuoteYuvCtl)
           return m_pQuoteYuvCtl->GetIPicYuv420p();
		return  m_pIPicYuv420p;
	}
	
	void           CUIYUV::SetQuoteYuv(CUIYUV* pQuoteYuvCtl)
	{
		
		if(pQuoteYuvCtl&&m_pIPicYuv420p){
			m_pQuoteYuvCtl=pQuoteYuvCtl;
			m_pQuoteYuvCtl->AddRef();
		}
	}

	void       CUIYUV::OnUpDataCoordinate(int DisplayScale,int Old,int nRotate)
	{
	   
	}
	//在这里要做变换等操作
	void       CUIYUV::SetDisplayScale(float fDisplayScale)
	{
	   
             m_fDisplayScale=fDisplayScale;
	}
	
	void     CUIYUV::OnKeyMove(EKEY_MOVE KeyMove)
	{
	    CRect rt;
		CUIWindow::GetWindowRect(&rt);
	
		int ps = m_fDisplayScale*1;
		if(KeyMove == EKM_Left)
		{
			rt.left  -=ps;
			rt.right -=ps;
		}
		else if(KeyMove == EKM_Top){
			rt.top    -=ps;
			rt.bottom -=ps;
		}else if(KeyMove == EKM_Right)
		{
		    rt.left  +=ps;
			rt.right +=ps;
		}
        else if(KeyMove == EKM_Bottom)
		{
		    rt.top    +=ps;
			rt.bottom +=ps;
		}
		
		SStringA Pos;
		OnRelayout(&rt);
		Pos.Format("%d,%d,@%d,@%d",rt.left,rt.top,rt.Width(),rt.Height());
		this->SetAttribute("pos", Pos);
		this->GetParent()->UpdateChildrenPosition();

		
		UIEvent ev;
		ev.Handle = this;
		ev.type   = E_CltMove;
		ev.pa1    = 0;
		GetContainer()->GetIUIMessage()->OnUIEvent(&ev);

		m_bDraging =0;
		this->Invalidate();
	}
	void     CUIYUV::OnMouseMove(unsigned int nFlags,CPoint pt)
	{
		

		 m_nMoveing = 0;
		if(m_nIsLock)
			return ;
		if(!CanResponseMouseMsg())
		     return;

		
             
		CRect rcWnd;
	
		CUIWindow::GetWindowRect(&rcWnd);
        GetScalRect(rcWnd,m_fDisplayScale);

       
		

		int xx=0,yy=0;
	//	m_ptClick=pt;
	    if(m_bDraging&&  nFlags & 0x0001 ==0x0001 )
        {
            CPoint ptLT =pt - m_ptClick;
			CPoint ptLT2 = m_ptClick;
			CPoint ptLT3 = pt;
			m_ptClick = pt;
			//有个点还原操作
			if(m_pHoverMask){	
				CRect rt;
				GetWindowRect(&rt);
				GetScalRect(rt,m_fDisplayScale);


				CPoint OPt;
		        GetCenterPt2(OPt,&rt);
			    kkPtbyRotate(ptLT3,OPt,-m_nRotate);
				kkPtbyRotate(ptLT2, OPt, -m_nRotate);
				ptLT = ptLT3 - ptLT2;
				if(m_bDraging==(int)EDrag_Vect)
				{
					 m_pHoverMask->m_UVS[m_nVertIndex].x = (float)(ptLT3.x - rt.left)/ rt.Width();
					 m_pHoverMask->m_UVS[m_nVertIndex].y = (float)(ptLT3.y - rt.top) / rt.Height();
					 m_pHoverMask->m_UVS[m_pHoverMask->m_nPtCount] = m_pHoverMask->m_UVS[0];
			
				}else{
				    /*********移动*********/
					xx = ptLT.x;
					yy = ptLT.y;


					CPoint cx = m_pHoverMask->GetPt(0,rt);
					cx.x += xx;
					cx.y += yy;
					if (rt.PtInRect(pt)){
						if (xx != 0 || yy != 0) {
							SUVInfo info;
							info.x = -(float)xx / rt.Width();
							info.y = -(float)yy / rt.Height();
							m_pHoverMask->MoveMask(info.x, info.y);
						}
					}
				}
				this->Invalidate();
			}else if(m_bFloat){
				    m_nMoveing = 1;
				    SStringA Pos;
				   
					ptLT.x *= m_fDisplayScale;
			        ptLT.y *= m_fDisplayScale;
                    CUIWindow::GetWindowRect(&rcWnd);
					CRect  rcOld = rcWnd;
					
					if(ptLT.x ==rcWnd.left &&  ptLT.y ==rcWnd.top)
					{
					   return;
					}
					
					rcWnd.left  +=ptLT.x;
					rcWnd.right +=ptLT.x;

					rcWnd.top  +=ptLT.y;
					rcWnd.bottom +=ptLT.y;

					 CPoint pts[4];
					 CPoint pts2[4];
					 CPoint OPt;
                     GetCenterPt2(OPt, &rcWnd);
					 GetRegionbyRotate(&rcWnd,OPt,m_nRotate, pts,4);
					 pts2[0] = pts[0];
					 pts2[1] = pts[1];
					 if(CheckMagnetisRect(pts, 4,m_sMagnetRegion))
					 {
						 int height = rcWnd.Height();
						 int width  = rcWnd.Width();

						       int ofx =0, ofy=0;
							   ofx= pts[0].x - pts2[0].x;
							   ofy= pts[0].y - pts2[0].y;
							   rcWnd.left += ofx;
							   rcWnd.top += ofy;
							   rcWnd.right += ofx;
							   rcWnd.bottom += ofy;
					
					 }
					 xx =rcOld.left - rcWnd.left;
					 yy =rcOld.top - rcWnd.top;
					
					
					OnRelayout(&rcWnd);
					Pos.Format("%d,%d,@%d,@%d",rcWnd.left,rcWnd.top,rcWnd.Width(),rcWnd.Height());
					this->SetAttribute("pos", Pos);
					this->GetParent()->UpdateChildrenPosition();
					GetContainer()->GetIUIMessage()->UISetCursor("sizeall");
			}
		}else{
			
			CShapeMask* old=m_pHoverMask;
		    m_pHoverMask = TestPtInMask(pt);
			if(m_pHoverMask){
				int index=0;
				m_pHoverMask->PtInSegment(pt,rcWnd,index);
				this->Invalidate();
			}
			if( m_pHoverMask!=old){
			    this->Invalidate();
			}
		
		
		}
	}


	void       CUIYUV::OnMouseLeave(unsigned int nFlags)
	{
		     if(m_bDraging ==EDrag_NcClient)
			   return;  /**/
		  
			 m_bDraging =0;
			 m_pHoverMask=0; 
#ifdef _DEBUG
			 ::OutputDebugStringKK("OnMouseLeave \n");
#endif
	}

	  void            CUIYUV::OnNcMouseLeave(unsigned int nFlags)
	  {
	          m_bDraging =0;
			  m_pHoverMask=0; 
#ifdef _DEBUG
			  ::OutputDebugStringKK("OnMouseLeave \n");
#endif
	  }
    void      CUIYUV::OnNcLButtonDown(unsigned int nFlags,CPoint pt)
	{
		CPoint OPt;
		CRect rcWnd;
		CUIWindow::GetWindowRect(&rcWnd);
		GetScalRect(rcWnd,m_fDisplayScale);
		GetCenterPt2(OPt,&rcWnd);

		if(!m_nAdjustSize)
            return;
		 int nc;
		 m_nPtIndex= IsPtInRegion(rcWnd,pt,nc);
		 if(!nc)
		 {
			 m_nPtIndex= 0;
		    return ;
		 }
		 assert(m_nPtIndex);
			
		/* if(!m_nIsRect)
		 {
			 	CPoint OutPts[4];
		        GetRegionbyRotate(&rcWnd,OPt,m_nRotate, OutPts,4);
				if(m_nPtIndex==1||m_nPtIndex==2||m_nPtIndex==3||m_nPtIndex==4)
				{
				   m_ptClick = OutPts[m_nPtIndex-1];
				}
		 }*/
	    SetCapture();
		if(!m_nIsLock)
		{
           m_bDraging =EDrag_NcClient;
		   m_ptClick = pt;
		}
		this->Invalidate();
	}
	void      CUIYUV::OnNcLButtonUp(unsigned int nFlags,CPoint pt)
	{
		    CPoint OPt;
		   	CRect rcWnd;
		    CUIWindow::GetWindowRect(&rcWnd);
			GetScalRect(rcWnd,m_fDisplayScale);
		    GetCenterPt2(OPt,&rcWnd);
		    if(m_nRotate){
			    kkPtbyRotate(pt,OPt,-m_nRotate);
		    }
			m_bDraging = 0;
            m_nPtIndex= 0;

			UIEvent ev;
			ev.Handle = this;
			ev.type = E_Click;
			ev.pa1 = 0;

			GetContainer()->GetIUIMessage()->OnUIEvent(&ev);
			this->Invalidate();
	}
    void     CUIYUV::OnNcMouseMove(unsigned int nFlags,CPoint pt)
	{
		    if(m_nIsLock)
			   return ;

		    
			#ifdef _DEBUG
			  ::OutputDebugStringKK("NcMouseMove \n");
           #endif
	        CPoint OPt;
			CRect rcWnd;
		    CUIWindow::GetWindowRect(&rcWnd);
			GetScalRect(rcWnd,m_fDisplayScale);
		    GetCenterPt2(OPt,&rcWnd);
          
              
            CPoint SrcPt = pt;
            //还原到未旋转的点
		   /* if(m_nRotate){
			    kkPtbyRotate(pt,OPt,-m_nRotate);
		    }*/
	        if(!m_nIsRect)
			{
				 CPoint ptLT = pt - m_ptClick;
				 if(ptLT.x==0&&ptLT.y==0)
					 return;
				 kkPtbyRotate(pt,OPt,-m_nRotate);
				if(m_bDraging==EDrag_NcClient)
				{
					 int width  = rcWnd.right  - rcWnd.left;
	                 int height = rcWnd.bottom - rcWnd.top;
				     if(m_nPtIndex==1)
					 {
					   
						float w2 = pt.x-rcWnd.left;
						float  y2 = pt.y-rcWnd.top;
						m_fPosX1 =  w2 /width;
						m_fPosY1 =  y2 /height;
					 }else if(m_nPtIndex==2)
					 {
					   
						float w2 = pt.x-rcWnd.left;
						float  y2 = pt.y-rcWnd.top;
						m_fPosX2 =  w2 /width;
						m_fPosY2 =  y2 /height;
					 }else if(m_nPtIndex==3)
					 {
					   
						float w2 = pt.x-rcWnd.left;
						float  y2 = pt.y-rcWnd.top;
						m_fPosX3 =  w2 /width;
						m_fPosY3 =  y2 /height;
					 }else if(m_nPtIndex==4)
					 {
					   
						float w2 = pt.x-rcWnd.left;
						float  y2 = pt.y-rcWnd.top;
						m_fPosX4 =  w2 /width;
						m_fPosY4 =  y2 /height;
					 }
					 this->Invalidate();
				}
				return;
			}

		
	     if(m_bDraging==EDrag_NcClient){
            SetCapture();
			CRect rcWindow;
		    CUIWindow::GetWindowRect(&rcWnd);
            CPoint ptLT = pt - m_ptClick;
			ptLT.x *=  m_fDisplayScale;
			ptLT.y *=  m_fDisplayScale;
			if (ptLT.x == 0)
			{
				//ptLT.x=1;
			}

			if (ptLT.y == 0)
			{
				//ptLT.y=1;
			}
			
			{
					m_ptClick = pt;
					CRect rt = rcWnd;
					if(m_nPtIndex==4)
					{
						rt.left += ptLT.x;
					}else if(m_nPtIndex== 2){
					   rt.right += ptLT.x;
					}else if(m_nPtIndex == 1)
					{
						rt.top+=ptLT.y;
					}else if(m_nPtIndex == 3)
					{
						rt.bottom +=ptLT.y;
					}

					int Width=rt.Width();
					int Height = rt.Height();
					if(Width==0)
						 Width=2;
					if(Height ==0)
                          Height=2;
					SStringA Pos;
					Pos.Format("%d,%d,@%d,@%d",rt.left,rt.top,Width,Height);
					this->SetAttribute("pos", Pos);
					::OutputDebugStringKK(Pos.GetBuffer(64));
					OnRelayout(&rt);
					this->GetParent()->UpdateChildrenPosition();

					 UIEvent ev;
					 ev.Handle = this;
					 ev.type   = E_CtlSize;
					 ev.pa1    = 0;

					 GetContainer()->GetIUIMessage()->OnUIEvent(&ev);
			}
		}
	  

	}
	int      CUIYUV::OnSetCursor(const CPoint &pt)
	{
	    if(m_bFloat&&m_bDraging == EDrag_Type::EDrag_Client ){
		    GetContainer()->GetIUIMessage()->UISetCursor("sizeall");
			return 1;
		}else if( m_bDraging ==EDrag_NcClient)
		{
			/*if(m_nIsRect){
			     GetContainer()->GetIUIMessage()->UISetCursor("sizeall");
			     return 1;
			}*/
			GetContainer()->GetIUIMessage()->UISetCursor("arrow");
			return 1;
		}

		//char temp[128]="";
		//sprintf(temp,"m_bDraging:%d \n",m_bDraging);
		//OutputDebugStringKK(temp);
		return CUIWindow::OnSetCursor(pt);
	}
	bool      CUIYUV::CanResponseMouseMsg()
	{
		

	    CUIWindow*  winp =this->GetParent();
		if(winp){
			if(winp->IsClass("uimsview")){

				CUIMsView *ms=(CUIMsView*)winp ;
				if(ms->GetActiveYUV()==this)
				{
				   int ii=0;
				   ii++;
				}
				 return true;
			}
		}
		return true;
	}
	void        CUIYUV::OnLButtonDBLClk(unsigned int nFlags,CPoint pt)
	{
	    UIEvent ev;
		ev.Handle = this;
		ev.type = E_CltLBDBLClk;
		ev.pa1 = 0;
		GetContainer()->GetIUIMessage()->OnUIEvent(&ev);
	}
	void       CUIYUV::OnLButtonDown(unsigned int nFlags,CPoint pt)
	{
		CRect rt;
		CPoint OPt;
		CUIWindow*  winp = 0;
		if(m_nIsLock)
			goto endx;
		
        if(!CanResponseMouseMsg())
		    goto endx;
	    
	
        SetCapture();

		
		CUIWindow::GetWindowRect(&rt);
		m_OldRect = rt;
        GetScalRect(rt,m_fDisplayScale);
	   
        winp =this->GetParent();
		if(winp){
			if(winp->IsClass("uimsview")){

				if(nFlags&0x0004)
				{
                      CUIMsView* msView = (CUIMsView*)winp;
					  CUIMoveState* stage = msView->GetStageByRect(rt);
					  if(stage)
					  {
						  CRect StageRt;
						  stage->GetWindowRect(&StageRt);

						  OnRelayout(&StageRt);
						  SStringA Pos;
						  Pos.Format("%d,%d,@%d,@%d",StageRt.left,StageRt.top,StageRt.Width(),StageRt.Height());
						  this->SetAttribute("pos", Pos);
						  this->GetParent()->UpdateChildrenPosition();
						 
					  }
				       goto endx;
				}
				CUIMsView *ms=(CUIMsView*)winp ;
				if(ms->GetActiveYUV()!=this)
				{
				     ms->SetActiveYUV(this);
					  Invalidate();
				}
			}
		}

		m_bDraging =EDrag_Type::EDrag_Client;
		
		m_pHoverMask = TestPtInMask(pt);
		if(m_pHoverMask){

			 if(m_pHoverMask ->PtInVert(pt,rt,8,m_nVertIndex)){
				   m_bDraging =EDrag_Type::EDrag_Vect;
			 }else if(m_pHoverMask->m_nPtCount<10&&m_pHoverMask->PtInSegment(pt,rt,m_nVertIndex)) 
			 {
				   CUIWindow::GetWindowRect(&rt);
                   GetScalRect(rt,m_fDisplayScale);
				   SUVInfo uv;
				   uv.x = (float)(pt.x-rt.left)/rt.Width();
				   uv.y = (float)(pt.y-rt.top)/rt.Height();
			       m_pHoverMask->InsertVert(uv,m_nVertIndex);
				   m_nVertIndex++;
				   m_bDraging = EDrag_Type::EDrag_Vect;
			 }else{
			     m_ptClick = pt;
			 }
             
		} else {
	
			m_ptClick = pt;
		}

	
		if(m_bFloat&&m_bDraging==EDrag_Type::EDrag_Client)
		     GetContainer()->GetIUIMessage()->UISetCursor("sizeall");

endx:
		UIEvent ev;
		ev.Handle = this;
		ev.type = E_Click;
		ev.pa1 = 0;
		GetContainer()->GetIUIMessage()->OnUIEvent(&ev);

	}
	void  CUIYUV::OnLButtonUp(unsigned int nFlags,CPoint pt)
    {
		
         //ReleaseCapture();
        

		 UIEvent ev;
		 ev.Handle = this;
		 ev.type   = E_Click;
		 ev.pa1    = 0;
		 if(m_bDraging==EDrag_Client&&m_nMoveing){
		     ev.type =E_CltMoveOver;
			 ev.pos1.Left=m_OldRect.left;
			 ev.pos1.Top=m_OldRect.top;

			 ev.pos1.Right=m_OldRect.right;
			 ev.pos1.Bottom=m_OldRect.bottom;

			
			 CRect rcWnd;
			 GetWindowRect(&rcWnd);

			 ev.pos2.Left   = rcWnd.left;
			 ev.pos2.Top    = rcWnd.top;

			 ev.pos2.Right  = rcWnd.right;
			 ev.pos2.Bottom = rcWnd.bottom;
			 m_nMoveing = 0;
		 }

         m_bDraging = 0;
		 GetContainer()->GetIUIMessage()->OnUIEvent(&ev);
		 
    }

	void       CUIYUV::OnRButtonUp(unsigned int nFlags,CPoint pt)
	{
	     UIEvent ev;
		 ev.Handle = this;
		 ev.type   = E_RClick;
		 ev.pa1    = pt.x;
         ev.pa2    = pt.y;
		 GetContainer()->GetIUIMessage()->OnUIEvent(&ev);
	}
	int        CUIYUV::GetMaskCount()
	{
		return m_MaskVect.size();
	}
	int        CUIYUV::GetMaskInfo(int id, SUVInfo* pPts,int ptCount,int* HidePixel)
	{
	
	   std::vector<CShapeMask*>::iterator It= m_MaskVect.begin();
		for(;It != m_MaskVect.end();++It){
			 CShapeMask* Item=*It;
		     if(Item->GetId()==id){
				 if(Item->m_nPtCount>ptCount)
					 return 0;
			     for(int i =0;i< Item->m_nPtCount;i++){
					 pPts[i].x = Item->m_UVS[i].x;
					 pPts[i].y = Item->m_UVS[i].y;
				 }
				 *HidePixel=Item->m_nHidePx;
				 return  Item->m_nPtCount;
				 break;
			 }
		}/* */
		return 0;
	}
	int        CUIYUV::SetMaskInfo(int id, SUVInfo* pPts,int ptCount)
	{
	//	assert(0);
	    std::vector<CShapeMask*>::iterator It= m_MaskVect.begin();
		for(;It != m_MaskVect.end();++It){
			 CShapeMask* Item=*It;
		     if(Item->GetId()==id){
				  Item->m_nPtCount = ptCount;
			     for(int i =0;i<ptCount;i++){
					 Item->m_UVS[i].x = pPts[i].x;
					 Item->m_UVS[i].y = pPts[i].y;
				 }
				 Item->m_UVS[Item->m_nPtCount].x=pPts[0].x;
				 Item->m_UVS[Item->m_nPtCount].y=pPts[0].y;
				
				 return 1;
			 }
		}/**/
		return 0;
	}
	int        CUIYUV::AddFilter(SinkFilter& Filter)
	{
	
		Filter.Id = ++m_nMaskId;
		
		SinkFilter* pFilter = new SinkFilter();
		memset(pFilter,0,sizeof(SinkFilter));
		*pFilter = Filter;
		pFilter->Next=0;
		pFilter->Pre = 0;
		if(m_pHeaderFilter==0)
		{
		   m_pHeaderFilter =  m_pTailFilter =  pFilter ;

         
		}else{
			 m_pTailFilter->Next=pFilter;
			 pFilter->Pre = m_pTailFilter;
			 m_pTailFilter =pFilter;
		}
		

		return Filter.Id;
	}
	SinkFilter*  CUIYUV::GetFristFilter()
	{
	      return m_pHeaderFilter;
	}
	void    CUIYUV::DelFilter(int Id)
	{
	    if(m_pHeaderFilter==0)
		    return;
		SinkFilter* pFilter = m_pHeaderFilter;
		while(pFilter)
		{
		    if(pFilter->Id==Id)
			{
			     SinkFilter* Pre  = pFilter->Pre;
				 SinkFilter* Next = pFilter->Next;
				 if(Next)
				 {
				     Next->Pre =  Pre ;
				 }
				 if(Pre)
				 {
				     Pre->Next = Next;
				 }

				 if(m_pHeaderFilter==pFilter)
				 {
				     m_pHeaderFilter=Next;
				 }

				 if(m_pTailFilter==pFilter)
				 {
				     m_pTailFilter=Pre;
				 }
				 delete  pFilter;
				 break;
			}
            pFilter =pFilter->Next;
		}
	}
	int        CUIYUV::AddMask(EMaskType type)
	{
	   int  Id=0;
	   if(type==SquareMask)
	   {
		   Id =++m_nMaskId;
		   CSquareMask* p = new CSquareMask(this);
		   p->SetId(Id);
 

		   CRect rt;
		   CRect Winrt;
		   this->GetWindowRect(&Winrt);
		   GetScalRect(Winrt,m_fDisplayScale);
		   rt = Winrt;
		   int Height = rt.Height()/4;
		   int Width  = rt.Width()/4;

		   rt.left =  Width+ Width/2;
		   rt.right = rt.left + Width;
		   rt.top   =  Height+ Height/2;
		   rt.bottom = rt.top+Height;

		   p->m_UVS[0].x = (float)rt.left/Winrt.Width();
		   p->m_UVS[0].y = (float)rt.top/ Winrt.Height();

		   p->m_UVS[1].x = (float)rt.right/ Winrt.Width();
		   p->m_UVS[1].y = (float)rt.top/ Winrt.Height();

		   p->m_UVS[2].x = (float)rt.right/ Winrt.Width();
		   p->m_UVS[2].y = (float)rt.bottom/ Winrt.Height();

		   p->m_UVS[3].x = (float)rt.left/ Winrt.Width();
		   p->m_UVS[3].y = (float)rt.bottom/ Winrt.Height();
           p->m_UVS[4]   = p->m_UVS[0];

		   m_MaskVect.push_back(p);
	   }else if(type==TriangleMask){
	       Id =++m_nMaskId;
		   CTriangleMask* p = new CTriangleMask(this);
		   p->SetId(Id);
 

		   CRect rt;
		   CRect Winrt;
		   this->GetWindowRect(&Winrt);
		   GetScalRect(Winrt, m_fDisplayScale);
		   rt = Winrt;
		   int Height = rt.Height()/4;
		   int Width  = rt.Width()/4;
		   rt.left  =   Width*2;
		   rt.right =  rt.left + Width/2;
		   rt.top   =  Height+ Height/2;
		   rt.bottom = rt.top+Height;

		   p->m_UVS[0].x = (float)rt.left/ Winrt.Width();
		   p->m_UVS[0].y = (float)rt.top/ Winrt.Height();

		
		   p->m_UVS[1].x = (float)rt.right/ Winrt.Width();
		   p->m_UVS[1].y = (float)rt.bottom/ Winrt.Height();

		   p->m_UVS[2].x = (float)(rt.left-Width/2)/ Winrt.Width();;
		   p->m_UVS[2].y = (float)rt.bottom/ Winrt.Height();
           p->m_UVS[3]   = p->m_UVS[0];
		   m_MaskVect.push_back(p);
	   
	   }
	   this->Invalidate();

	   return Id;
	}
	void       CUIYUV::MaskChangePixel(int Id, int HidePixel)
	{
	    std::vector<CShapeMask*>::iterator It= m_MaskVect.begin();
		for(;It != m_MaskVect.end();++It){
			 CShapeMask* Item=*It;
		     if(Item->GetId()==Id){
				 Item->m_nHidePx = HidePixel;
				 break;
			 }
		}
		  this->Invalidate();
	}
	void       CUIYUV::DelMask(int Id)
	{
		std::vector<CShapeMask*>::iterator It= m_MaskVect.begin();
		for(;It != m_MaskVect.end();++It){
			 CShapeMask* Item=*It;
		     if(Item->GetId()==Id){
				 delete Item;
				 m_MaskVect.erase(It);
				 break;
			 }
		}
		this->Invalidate();
	}
	void       CUIYUV::SavePreFrame(bool Save)
	{
		if(!m_pQuoteYuvCtl)
		{
			m_pIPicYuv420p->SavePreFrame(Save);
		}
	}
	void       CUIYUV::GetPicClip(SUVInfo* pPts,int ptCount)
	{
	    if(pPts==0 || ptCount <4)
		   return;
	    pPts->x     = m_fPicX1;
	    pPts->y     = m_fPicY1;

	   (pPts+1)->x = m_fPicX2;
	   (pPts+1)->y = m_fPicY2;

	   (pPts+2)->x = m_fPicX3;
	   (pPts+2)->y = m_fPicY3;

	   (pPts+3)->x = m_fPicX4;
	   (pPts+3)->y = m_fPicY4;
	}
	void    CUIYUV::SetPicClip(SUVInfo* pPts,int ptCount)
	{
	    
	    m_fPicX1  = pPts->x;
	    m_fPicY1  = pPts->y;

	    m_fPicX2 =  (pPts+1)->x;
	    m_fPicY2 =  (pPts+1)->y;;

	    m_fPicX3 = (pPts+2)->x;
	    m_fPicY3 = (pPts+2)->y;

	    m_fPicX4 = (pPts+3)->x;
	   m_fPicY4  = (pPts+3)->y;
	}
	CShapeMask*  CUIYUV::TestPtInMask(CPoint pt)
	{ 
		int Out=0;
	    std::vector<CShapeMask*>::iterator It= m_MaskVect.begin();
		CRect rc;
		GetWindowRect(&rc);
		GetScalRect(rc,m_fDisplayScale);


		for(;It != m_MaskVect.end();++It){
			 CShapeMask* Item=*It;
			
			 if(Item->PnPoly(pt,rc) || Item->PtInVert(pt,rc,8, Out)|| Item->PtInSegment(pt,rc,Out)){
				return Item;
			 }
		}
		return NULL;
	}
	int             CUIYUV::LoadYUV(kkPicInfo *info)
	{
		if(!info)
			return 0;
		if(info->picformat ==13)
			assert(0);
		if(m_pIPicYuv420p)
		   m_pIPicYuv420p->Load(info);
	   
	    return 0;
	}
	
	int        CUIYUV::IsContainPoint(const kkPoint &pt,int bClientOnly)
	{
		if(m_bDraging ==EDrag_NcClient)
			   return 1;
	    int bRet = 0;
	    kkPoint pt2=pt;
		CRect rc;
		if(bClientOnly){
				GetClientRect(&rc);
				GetScalRect(rc,m_fDisplayScale);
		}else{
				GetWindowRect(&rc);
				GetScalRect(rc,m_fDisplayScale);
				rc.left-=1;
				rc.top-=1;
				rc.right+=1;
				rc.bottom+=1;
		}


		if(m_nIsRect)
		{
			if(m_nRotate){
				kkPoint OPt ={0,0};
				OPt.x = rc.left + (rc.right -rc.left)/2;
				OPt.y = rc.top  + (rc.bottom -rc.top)/2;
				kkPtbyRotate(pt2,OPt,-m_nRotate);
			}
			bRet = rc.PtInRect(pt2);
			if(!bRet&&!bClientOnly )
			{
				int nc =0;
				bRet = IsPtInRegion(rc,pt2,nc);
			}
		}else{
		 
		    rc.left+=1;
			rc.top+=1;
			rc.right-=1;
			rc.bottom-=1;

			int nc =0;
		    bRet = IsPtInRegion(rc,pt2,nc);
			if(bClientOnly){
				if(nc)
					return 0;
			}

		}
		return bRet;
	}
	int      CUIYUV::RectInterRegion(CRect rt)
	{
		CRect WinRc;
		GetWindowRect(&WinRc);
		GetScalRect(WinRc, m_fDisplayScale);
		CPoint OPt;
		GetCenterPt2(OPt, &WinRc);
		CPoint OutPts[4];
		GetRegionbyRotate(&WinRc, OPt, m_nRotate, OutPts, 4);
		
		OPt.x = rt.left;
		OPt.y = rt.top;
		if (PtInPoly(OPt, OutPts, 4));
		   return 1;

	   OPt.x = rt.left;
	   OPt.y = rt.bottom;
	   if (PtInPoly(OPt, OutPts, 4));
	   return 1;

	   OPt.x = rt.right;
	   OPt.y = rt.top;
	   if (PtInPoly(OPt, OutPts, 4));
	   return 1;

	   OPt.x = rt.right;
	   OPt.y = rt.bottom;
	   if (PtInPoly(OPt, OutPts, 4));
	   return 1;
	   return 0;
	}

	void  CUIYUV::SetFRegion(SUVInfo* Pts, int ptCount)
	{
	    m_fPosX1  = Pts->x;
		m_fPosY1 = Pts->y;

		m_fPosX2 = (Pts+1)->x;
		m_fPosY2 = (Pts+1)->y;

		m_fPosX3 = (Pts+2)->x;
		m_fPosY3 = (Pts+2)->y;

		m_fPosX4 = (Pts+3)->x;
		m_fPosY4 = (Pts+3)->y;
	}
	int   CUIYUV::GetFRegion(SUVInfo* Pts, int ptCount)
	{
		Pts->x = m_fPosX1;
		Pts->y = m_fPosY1;

		(Pts+1)->x = m_fPosX2;
		(Pts+1)->y = m_fPosY2;

		(Pts+2)->x = m_fPosX3;
		(Pts+2)->y = m_fPosY3;

		(Pts+3)->x = m_fPosX4;
		(Pts+3)->y = m_fPosY4;
		return 4;
	}
	int      CUIYUV::GetRegion(CPoint* Pts,int count)
	{
		if(Pts==0 || count<4)
			return 0;
	    CRect WinRc;
		GetWindowRect(&WinRc);
		CPoint OPt;
		GetCenterPt2(OPt, &WinRc);
		
		GetRegionbyRotate(&WinRc, OPt, m_nRotate, Pts, 4);
	
		return 4;
	}
	int      CUIYUV::OnNcHitTest(kkPoint pt)
	{
		if( m_bDraging ==EDrag_NcClient)
			return 1;

		CRect rt ;
        this->GetClientRect(&rt);
        GetScalRect(rt,m_fDisplayScale);

		int nc = 0;
		int bInNc = 0;
		if(m_nRotate|| !m_nIsRect) {
				CPoint OPt;
				GetCenterPt2(OPt,&rt);
				kkPoint OutPts[4]={0,0};
				GetRegionbyRotate(&rt,OPt,m_nRotate, OutPts,4);
				bInNc= !kkPtInPolygon(pt,OutPts,4);
		}else{
			    bInNc=  !rt.PtInRect(pt);
		}/**/

		this->GetWindowRect(&rt);
        GetScalRect(rt,m_fDisplayScale);
		

		int bInNc2 =   IsPtInRegion(rt,pt,nc);
		if (bInNc2){
			if(nc)
				return nc;
			return bInNc;
		}
		else
			return 0;
	}

	  //点是否在可以调节
	  void            CUIYUV::GetJustPtsInNcRect(kkRect rt,kkPoint* PtNc)
      {
			
			CPoint OPt;
			GetCenterPt2(OPt, &rt);
			kkPoint OutPts[4] = { 0,0 };
			GetRegionbyRotate(&rt, OPt, m_nRotate, OutPts, 4);
			PtNc ->x = OutPts[0].x+(OutPts[1].x -OutPts[0].x ) /2;
			PtNc ->y = OutPts[0].y+(OutPts[1].y -OutPts[0].y ) /2;

			(PtNc+1) ->x =  OutPts[1].x + (OutPts[2].x - OutPts[1].x)/2;
			(PtNc+1) ->y =  OutPts[1].y + (OutPts[2].y - OutPts[1].y)/2;

			(PtNc+2) ->x =  OutPts[3].x + (OutPts[2].x - OutPts[3].x)/2;
			(PtNc+2) ->y =  OutPts[3].y + (OutPts[2].y - OutPts[3].y)/2;

			(PtNc+3) ->x = OutPts[0].x + (OutPts[3].x - OutPts[0].x)/2;
			(PtNc+3) ->y = OutPts[0].y + (OutPts[3].y - OutPts[0].y)/2;

	  }
      int      CUIYUV::IsPtInRegion(CRect& rt,kkPoint pt,int& nc)
	  {
			int bInNc2 = 0;
			nc = 0;
			
			{
				CPoint OPt;
				GetCenterPt2(OPt, &rt);
				kkPoint OutPts[4] = { 0,0 };
				GetRegionbyRotate(&rt, OPt, m_nRotate, OutPts, 4);
				bInNc2 = kkPtInPolygon(pt, OutPts, 4);


				if(!m_nIsRect)
				{
							if(PtInCircle(&pt,&OutPts[0],8))
							{
								nc=1;
								bInNc2 =1;
							}else if(PtInCircle(&pt,&OutPts[1],8))
							{
							   nc=1;
								bInNc2 =2;
							}

							if(PtInCircle(&pt,&OutPts[2],8))
							{
							   nc=1;
								bInNc2 =3;
							}

							if(PtInCircle(&pt,&OutPts[3],8))
							{
								nc=1;
								bInNc2 =4;
							}

				}else{
                          kkPoint PtNc[4]={0,0};
						  GetJustPtsInNcRect(rt,PtNc);
						  CRect TempRt;
						  GetRectByPt( PtNc[0],TempRt,4);
						  if(TempRt.PtInRect(pt))
						  {
						        nc=1;
								bInNc2 =1;
						  }
						  GetRectByPt( PtNc[1],TempRt,4);
						   if(TempRt.PtInRect(pt))
						  {
						        nc=1;
								bInNc2 =2;
						  }

						  GetRectByPt( PtNc[2],TempRt,4);
						  if(TempRt.PtInRect(pt))
						  {
						        nc=1;
								bInNc2 =3;
						  }

						  GetRectByPt( PtNc[3],TempRt,4);
						  if(TempRt.PtInRect(pt))
						  {
						        nc=1;
								bInNc2 =4;
						  }
				}
			}
            return bInNc2;
	  }
	CUIWindow*      CUIYUV::WndFromPoint( kkPoint  ptHitTest, int bOnlyText)
	{
	
		 if(m_bDraging==EDrag_NcClient)
			 return this;
		if(!CanResponseMouseMsg())
		     return 0;
		

		if (!IsContainPoint(ptHitTest, 0))
			return NULL;

		if (!IsContainPoint(ptHitTest, 1))
			return this;//只在鼠标位于客户区时，才继续搜索子窗口


		return this;

	}
	
	void     CUIYUV::GetCenterPt2(CPoint& pt,kkRect* rt)
	{
	        pt.x = rt->left + (rt->right  - rt->left)/2;
			pt.y = rt->top  + (rt->bottom - rt->top)/2;
	}
   

	 void      CUIYUV::CreatePicCache(int width,int height,int format)
	 {
	    assert(m_pIPicYuv420p);
        m_pIPicYuv420p->CreatePicCache(width,height,format);
	 }
    void       CUIYUV::ClearPicData(unsigned int Color)
	{
	     if(m_pIPicYuv420p&&!m_pQuoteYuvCtl)
		 {
		     m_pIPicYuv420p->ClearPicData(Color);
		 }
	}
	void       CUIYUV::Active()
	{
	    CUIWindow*  winp =this->GetParent();
		if(winp){
			if(winp->IsClass("uimsview")){
				CUIMsView *ms=(CUIMsView*)winp ;
				ms->SetActiveYUV(this);
				Invalidate();
			}
		}
    }
	void       CUIYUV::SetTxt(std::wstring strTxt)
	{
	     m_strTxt=strTxt;
	}

	

	
}