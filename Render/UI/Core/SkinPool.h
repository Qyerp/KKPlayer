#ifndef SkinPool_H_
#define SkinPool_H_
#include <string>
#include <list>
#include <algorithm>

#include "Singleton.h"
#include "ObjRef.h"
#include "ISkinObj.h"
#include "Units.h"


#define GETSKIN(p1,scale) CSkinPoolMgr::getSingleton().GetSkin(p1,scale)
///Ƥ������
namespace KKUI{

	typedef ISkinObj *   PSkinPtr;


	class CSkinKey
	{
		public:
			CSkinKey();
			std::string  strName;
			int		     scale;
			inline bool operator < (const  CSkinKey &ps) const
			{
				if(scale<ps.scale)
					return 1;
				if(strName!=ps.strName)	
				{
				   if(strName < ps.strName)
					   return 1;
				  
				   if(scale<ps.scale)
					return 1;
				}
				return 0;
			}
			inline bool operator == (const CSkinKey  &ps) const
			{
				return scale==ps.scale&&strName == ps.strName;
			}
	};
	class CSkinPool:public CCmnMap<PSkinPtr,CSkinKey>,public CObjRef
	{
	  public:
	          CSkinPool();
              virtual ~CSkinPool();
			  ISkinObj*   GetSkin(SStringA strSkinName,int nScale);
			
			  int         LoadSkins(pugi::xml_node xmlNode);
      protected:
          static void OnKeyRemoved(const PSkinPtr & obj);
	};
	class CSkinPoolMgr : public CSingleton<CSkinPoolMgr> 
	{
	    public:
		        CSkinPoolMgr();
		       ~CSkinPoolMgr();

			   ISkinObj*   GetSkin(const  SStringA& strSkinName,int nScale);
               void        PushSkinPool(CSkinPool *pSkinPool);
               CSkinPool * PopSkinPool(CSkinPool *pSkinPool);

			  
	protected:
		       std::list<CSkinPool *> m_lstSkinPools;
			   CAutoRefPtr<CSkinPool> m_bulitinSkinPool;
	};
}
#endif