#include "dxdec.h"


typedef struct TextureWrapper {
	IDXTexture* pCtx;
	AVFrame *pPic;
	LPDIRECT3DTEXTURE9   texture;
} TextureWrapper;

typedef struct TextureWrapper2 {
	AVFrame *pPic;
} TextureWrapper2;

static void ffmpeg_ReleaseFrameBuf( void *p1,uint8_t *p2 )
{
	
	TextureWrapper *sw = (TextureWrapper *)p1;
	AVFrame *p_ff_pic  = (AVFrame *)sw->pPic;
	IDXTexture* pCtx    = sw->pCtx ;


	if(!pCtx->ReleaseTexture(sw->texture)){
	 
	}
	delete sw;	
}

static int ffmpeg_GetFrameBuf( struct AVCodecContext *p_context,AVFrame *p_ff_pic,int flags)
{
	IDXTexture* pCtx = (IDXTexture *)p_context->opaque;
	AVPixelFormat chroma;
	if(pCtx)
	{
		int ix= pCtx->GetTextureIndex(p_ff_pic, pCtx->GetCurFormat(),p_context->coded_width,p_context->coded_height);
		if( ix<0 )
		{
			av_log(NULL, AV_LOG_ERROR, "DxTexture failed" );
			return -1;
		}
		if (!p_ff_pic->data[0])
			return -1;
		
		TextureWrapper *textureWrapper = new TextureWrapper();
		textureWrapper->pCtx = pCtx ;
		textureWrapper->pPic = p_ff_pic;
		textureWrapper->texture=(LPDIRECT3DTEXTURE9)p_ff_pic->data[0];
		p_ff_pic->data[4] = (uint8_t *)ix;
		
		
		p_ff_pic->buf[0] = av_buffer_create(NULL, 0, ffmpeg_ReleaseFrameBuf, textureWrapper, 0);
		return 0;
	}
	return -1;
}



static enum AVPixelFormat ffmpeg_GetFormat( AVCodecContext *p_context,const enum AVPixelFormat *pi_fmt )
{
	

	return AV_PIX_FMT_D3D11;
	return avcodec_default_get_format( p_context, pi_fmt );
}




int BindDxModule(	AVCodecContext  *pCodecCtx,IDXTexture* pDx)
{
	
	pCodecCtx->opaque = pDx;
	pCodecCtx->pix_fmt         = AV_PIX_FMT_D3D11;
	pCodecCtx->hwaccel_context = pDx;
	pCodecCtx->get_format      = ffmpeg_GetFormat;
	pCodecCtx->get_buffer2     = ffmpeg_GetFrameBuf;
	pCodecCtx->thread_count    = 1;
	pCodecCtx->slice_flags   |= SLICE_FLAG_ALLOW_FIELD;
	
	int res = 0;

	if (!pCodecCtx->hwaccel_context){
	    return 1;
	}
	return 0;
}




static void ffmpeg_ReleaseFrameBuf2( void *p1,uint8_t *p2 )
{
	
	TextureWrapper2 *sw = (TextureWrapper2 *)p1;
	AVFrame *p_ff_pic  = (AVFrame *)sw->pPic;
	

	delete sw;	
}

static int ffmpeg_GetFrameBuf2( struct AVCodecContext *p_context,AVFrame *p_ff_pic,int flags)
{
		av_frame_get_buffer(p_ff_pic, 64);
		//p_ff_pic->buf[0] = av_buffer_create(NULL, 0, ffmpeg_ReleaseFrameBuf, textureWrapper, 0);
		return 0;
	
}

int BindProresModule(	AVCodecContext  *pCodecCtx)
{
	
	
	
	pCodecCtx->get_buffer2     = ffmpeg_GetFrameBuf2;
	pCodecCtx->slice_flags   |= SLICE_FLAG_ALLOW_FIELD;
	return 1;
}