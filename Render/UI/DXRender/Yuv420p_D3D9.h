#ifndef Yuv420p_D3D9_H_
#define Yuv420p_D3D9_H_
#include <map>
#include <string>
#include <d3d9.h>
#include "ObjRef.h"
#include "Core/ObjectImp.h"
#include "Core/IRender.h"
#include "RttCache_D3D9.h"
namespace KKUI
{
	class SRenderFactory_D3D9;
	class CYuv420p_D3D9:public CRttCache_D3D9,public TObjRefImpl<IPicYuv420p>
	{
	        public:
				    CYuv420p_D3D9(	SRenderFactory_D3D9* pRenderFactory);
					~CYuv420p_D3D9();
                    void                   Release2();
					void                   ReLoadRes();
					kkSize                 Size() const;
					
					
					void                   ClearPicData(unsigned int Color);
					void                   SavePreFrame(bool save);
					int                    HasPreFrame();
					//设置引用
					void                   SetQuoteObj(IPicYuv420p*    pParentObj);
                    int	                   Load(const kkPicInfo* data);
                 
					void                   CreatePicCache(int width,int height,int format);
					IDirect3DTexture9*     GetTexture();
					IDirect3DTexture9*     GetLastTexture();
					IDirect3DTexture9*     LoadToClipTexture(SUVInfo xy1,SUVInfo xy2,SUVInfo xy3,SUVInfo xy4,int clipwidth,int clipheight);
	        private:
				    int                    LoadDxTexture(const kkPicInfo* data);
				    int                    LoadDxva2    (const kkPicInfo* data);
				    int                    LoadYuv      (const kkPicInfo* data);
					int                    LoadRGBA     (const kkPicInfo* data,int IsBGRA=0);
                    
				   //引用对象
				   CYuv420p_D3D9*          m_pQuoteObj;	
				   
				   //呈现的纹理,当前呈现
				   IDirect3DTexture9*      m_pRenderTexture;
				   kkSize                  m_Size;
				   IDirect3DTexture9*      m_pLastRenderTexture; 
				   kkSize                  m_LastSize;

				   //用于渲染RGBA,ARGB
				   IDirect3DTexture9*      m_pARGBTexture;
				  

				   int	                   LoadData(const kkPicInfo* data);
				   SRenderFactory_D3D9*    m_pRenderFactory;
				
				 

				

					//剪切的纹理
				   IDirect3DTexture9*      m_pClipTexture;
				   kkSize                  m_ClipSize;
				   int                     m_nPreOk;
	};
}
#endif