#ifndef UIApp_H_
#define UIApp_H_
#include <string>
#include "Singleton.h"
#include "ObjectFactory.h"
#include "pugixml/pugixml.hpp"
//获取资源文件大小
typedef int          (*KKUI_GetRawBufferSize)   (char* strType,char* pszResName);

//获取资源
typedef int          (*KKUI_GetRawBuffer)       (char* strType,char* pszResName,void* pBuf,int size);


#define LOADIMAGE2(p1)      KKUI::CUIApp::getSingleton().LoadImage2(p1)
///UIApp 管理，全局唯一
namespace KKUI{
	class  CUIWindow;
	class  ISkinObj;
	class  IRenderFactory;
	class  IBitmap;
	class  CUIApp:public CSingleton<CUIApp>,public CObjectFactoryMgr
	{
	   public:
               CUIApp();
			   ~CUIApp();

               void        SetRenderFactory(IRenderFactory* pRenderFactory);

			   CUIWindow*  CreateUIByName(char* classname) const;
			   ISkinObj*   CreateSkinByName(char*  pszSkinClass) const;
			   //加载一个xml文件
			   bool        LoadXmlDocmentFile( std::string xmlpath,pugi::xml_document & xmlDoc);

               IBitmap*    LoadImage2(const SStringA & strImgID);

			   int        LoadXmlSkin(char *xmlSkin);
               //设置资源回调
			   int         SetGetResCallInfo(KKUI_GetRawBufferSize fp1,KKUI_GetRawBuffer fp2);
			   int         IniDefSkin();
	   ///注册操作
	   public:
			    ///注册皮肤
				template<class T>
				bool RegisterSkinClass()
				{
					if (T::GetClassType() != Skin) 
						return false;
					return TplRegisterFactory<T>();
				}
				 ///注册Ctl
				template<class T>
				bool RegisterCtlClass()
				{
					if (T::GetClassType() != UICtl) 
						return false;
					return TplRegisterFactory<T>();
				}
	private:
		 IRenderFactory*              m_pRenderFactory;
		 KKUI_GetRawBufferSize        m_fpGetRawBufferSize;
		 KKUI_GetRawBuffer            m_fpGetRawBuffer;
	}; 
}
#endif