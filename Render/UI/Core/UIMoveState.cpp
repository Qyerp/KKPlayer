#include "UIMoveState.h"
#include "UIMsView.h"
#include "SkinPool.h"
#include "Warp/WarpImpl.h"
void OutputDebugStringKK(char* lpOutputString);
void charTowchar2(const char *chr, wchar_t *wchar, int size) ;
namespace KKUI
{
	void GetScalRect(kkRect& rt,const float& f);
	void GetScalRect(CRect& rt,const float& f);
	
	CUIMoveState::CUIMoveState(void):
       m_pBitmapLefTop(0),
	   m_pBitmapRightBu(0), 
	   m_pBitmapCenter(0),
	   m_nNeedData(0),
	   m_pFont(0),
	   m_fDisplayScale(1.0),
	   m_nEnbleIWarp(0),
	   m_pBkSkin(0),
	   m_nLigature(0)
	{
	   m_pIWarp = new CWarpImpl();

	    SMagnetRegion Mgegion;
		Mgegion.LeftTop     = EMagnetState::EMS_LEFT_TOP;
	    Mgegion.RightTop    = EMagnetState::EMS_LEFT_TOP;

	    Mgegion.LeftBottom  = EMagnetState::EMS_LEFT_TOP;
	    Mgegion.RightBottom = EMagnetState::EMS_LEFT_TOP;
	}
	CUIMoveState::~CUIMoveState(void)
	{
	           ClearIntersectCtl();

			   if(m_pBitmapLefTop)
				 m_pBitmapLefTop->Release();
			   m_pBitmapLefTop = 0;
			   
			   if(m_pBitmapRightBu)
				   m_pBitmapRightBu->Release();
			   m_pBitmapRightBu = 0;

			   if(m_pBitmapCenter)
				   m_pBitmapCenter->Release();
			    m_pBitmapCenter = 0;
               delete  m_pIWarp;
			    m_pIWarp = 0;
	}
	std::vector<CUIWindow*>  CUIMoveState::GetIntersectCtlVec()
	{
		return m_IntersectCtlVec;
	}
	int   CUIMoveState::GetCtlAttr(const SStringA& attrName,SStringA& OutAttr)
	{
	     if(CUIMoveWindow::GetCtlAttr(attrName,OutAttr))
			 return 1;
		 else  if(attrName.Compare("windowrect")==0){

		 }
		 return 0;

	}
	int   CUIMoveState::OnCreate( int)
   {    
	    
	     CUIWindow* pa =this->GetParent();
		 if(pa->IsClass("uimsview"))
		 {
			 CUIMsView* view = (CUIMsView*)pa;
			 float f=  view->GetDisplayScale();
			 m_fDisplayScale =f;
		 }
         return 0;
   }
    IWarp*          CUIMoveState::GetIWarp()
    {
		if(m_nEnbleIWarp)
			 return m_pIWarp;
		return 0;
	}
	void        CUIMoveState::SetXYDiv(int x,int y)
	{
	      m_pIWarp->SetXYDiv(x,y);
	}
    void        CUIMoveState::GetXYDiv(int &x,int &y)
	{
	      m_pIWarp->GetXYDiv(x,y);
	}
	int   CUIMoveState::GetWarpInfos(SUVInfo* infos,int count)
	{
	       return m_pIWarp->GetWarpInfos(infos,count);
	}
	int   CUIMoveState::SetWarpInfos(SUVInfo* infos,int count)
	{
	   return m_pIWarp->SetWarpInfos(infos,count);
	}
	std::vector<IFusionzone*> CUIMoveState::GetIFusionzoneVect() 
	{
			return m_FusionzoneVec;
	}
	int CUIMoveState::AddFusionzone(CUIFusionzone* Fusionzone)
	{
		IFusionzone* ff =  (IFusionzone*)Fusionzone;
		std::vector<IFusionzone*>::iterator It= m_FusionzoneVec.begin();
		for(;It!= m_FusionzoneVec.end();It++)
		{
		     if( *It==ff )
				 return 0;
		}
		Fusionzone->AddRef();
		
		m_FusionzoneVec.push_back(Fusionzone);
		return 1;
	}
    void CUIMoveState::RemoveFusionzone(CUIFusionzone* Fusionzone)
	{
		IFusionzone* ff =  (IFusionzone*)Fusionzone;
	    std::vector<IFusionzone*>::iterator It= m_FusionzoneVec.begin();
		for(;It!= m_FusionzoneVec.end();It++)
		{
		     if( *It==ff ){
			      Fusionzone->Release();
				  m_FusionzoneVec.erase(It);
				  break;
			 }
				
		}
		
	
	}
	void  CUIMoveState::ClearIntersectCtl()
	{
		std::vector<CUIWindow*>::iterator It= m_IntersectCtlVec.begin();
		for(;It!=m_IntersectCtlVec.end();++It)
		{
		     CUIWindow* Item=*It;
			 Item->Release();
		}
		m_IntersectCtlVec.clear();
	}
	void   CUIMoveState::AddIntersectCtl(CUIWindow* ctl)
	{
		ctl->AddRef();
		m_IntersectCtlVec.push_back( ctl);
		//m_IntersectCtlVec.insert(m_IntersectCtlVec.begin(), ctl);
	}
	void   CUIMoveState::DelIntersectCtl(CUIWindow* ctl)
	{
	    std::vector<CUIWindow*>::iterator It= m_IntersectCtlVec.begin();
		for(;It!=m_IntersectCtlVec.end();++It)
		{
		     CUIWindow* Item=*It;
			 if(ctl ==Item){
				 ctl->Release();
			    m_IntersectCtlVec.erase(It);
				break;
			 }
		}
	}
    void  CUIMoveState::OnUpdateFloatPosition(const  CSize&  szView,const CRect & rcParent)
	{
		CUIMoveWindow::OnUpdateFloatPosition(szView,rcParent);
	}
	int   CUIMoveState::OnNcHitTest(kkPoint pt)
	{
		CRect rcWindow;
		GetWindowRect(&rcWindow);
        GetScalRect(rcWindow,m_fDisplayScale);
      

	    int rowOut = 0, colOut = 0;
		CPoint ptx;
		ptx.x = pt.x;
		ptx.y = pt.y;
		int ret=0;

		{
		   CRect rcClient;
		   GetClientRect(&rcClient);
		   GetScalRect(rcClient,m_fDisplayScale);
		   ret=!rcClient.PtInRect(pt);
		}
	
		if (ret==1 &&m_pIWarp->TestPtInMestWarping(ptx, rowOut, colOut, rcWindow))
		{
			return 0;
		}else {
			if (m_bDraging == EDrag_WarpVect) {
				return 0;
			}
		}

		return ret;
	}
	int     CUIMoveState::IsContainPoint(const kkPoint &pt,int bClientOnly)
	{

        if (m_bDraging == EDrag_WarpVect) {
			return 1;
		}
		CPoint Pt;
		Pt.x = pt.x;
		Pt.y = pt.y;
		
	    int bRet = 0;
		if(bClientOnly){
			CRect rcClient;
			GetClientRect(&rcClient);
			GetScalRect(rcClient,m_fDisplayScale);
			bRet = rcClient.PtInRect(Pt);
		}else{
			CRect rcWindow;
		    GetWindowRect(&rcWindow);
			GetScalRect(rcWindow,m_fDisplayScale);
			bRet = rcWindow.PtInRect(Pt);
		}
		
		return bRet;

	}
    CUIWindow*   CUIMoveState::WndFromPoint( kkPoint  ptHitTest, int bOnlyText)
	{

	    CRect rcWindow;
		GetWindowRect(&rcWindow);
		GetScalRect(rcWindow,m_fDisplayScale);
       

		int rowOut = 0, colOut = 0;

		CPoint ptx;
		ptx.x = ptHitTest.x;
		ptx.y = ptHitTest.y;
		
		if (m_pIWarp->TestPtInMestWarping(ptx, rowOut, colOut, rcWindow))
		{
			return this;
		}else if(m_bDraging == EDrag_WarpVect) {
			return this;
		}
		
	
		if(!rcWindow.PtInRect(ptx))
			return NULL;

	

		
		return this;
	}
	void  CUIMoveState::OnSize(unsigned int nType, int x,int y)
	{
		    m_nNeedData=1;
	}

	CUIWindow*   CUIMoveState::SetCapture()
	{
		CUIWindow*  Par = this->GetParent();

		if(Par)
		{
			 if(this->GetParent()->IsClass("uimsview"))
			 {
                 (( CUIMsView*)Par)->SetActiveStage(this);
			 }
		}
		return GetContainer()->OnSetUIWndCapture(this);
	}

	void  CUIMoveState::OnLButtonDown(unsigned int nFlags,CPoint pt)
	{
	 
	    SetCapture();
		if(m_nIsLock)
			return;
		
	    m_bDraging =EDrag_Client;
	    CRect rt;
		CUIWindow::GetWindowRect(&rt);
		m_OldRect=rt;
		GetScalRect(rt,m_fDisplayScale);
		
		m_ptClick = pt - rt.TopLeft();
	
		int rowOut = 0 , colOut = 0;
		if(m_pIWarp->TestPtInMestWarping(pt,m_CurWarpInfo.rowId,m_CurWarpInfo.colId,rt))
		{
			 m_bDraging =EDrag_WarpVect;
		}else{
		    GetContainer()->GetIUIMessage()->UISetCursor("sizeall");
		}
	}
	void  CUIMoveState::OnLButtonDBLClk(unsigned int nFlags,CPoint pt)
	{
	    SetCapture();
		if(m_nIsLock)
			return;
	  
		UIEvent ev;
		ev.Handle = this;
		ev.type = E_CltLBDBLClk;
		ev.pa1 = 0;
		GetContainer()->GetIUIMessage()->OnUIEvent(&ev);

	}
	void  CUIMoveState::OnRButtonUp(unsigned int nFlags,CPoint pt)
	{
	     UIEvent ev;
		 ev.Handle = this;
		 ev.type   = E_RClick;
		 ev.pa1    = pt.x;
         ev.pa2    = pt.y;
		 GetContainer()->GetIUIMessage()->OnUIEvent(&ev);

	}
	void   CUIMoveState::OnLButtonUp(UINT nFlags, CPoint point)
	{
		UIEvent ev;
		ev.Handle = this;
		ev.type = E_Click;
		ev.pa1 = 0;
		 if(m_bDraging==EDrag_Client&&m_nMoveing)
		 {
		      ev.type =E_CltMoveOver;
			 ev.pos1.Left=m_OldRect.left;
			 ev.pos1.Top=m_OldRect.top;

			 ev.pos1.Right=m_OldRect.right;
			 ev.pos1.Bottom=m_OldRect.bottom;

			
			 CRect rcWnd;
			 GetWindowRect(&rcWnd);

			 ev.pos2.Left   = rcWnd.left;
			 ev.pos2.Top    = rcWnd.top;

			 ev.pos2.Right  = rcWnd.right;
			 ev.pos2.Bottom = rcWnd.bottom;
			 m_nMoveing = 0;

		 }
		GetContainer()->GetIUIMessage()->OnUIEvent(&ev);
		this->Invalidate();
	  m_bDraging =0;
	}
	void   CUIMoveState::OnNcLButtonUp(UINT nFlags,CPoint pt)
	{
	  m_bDraging =0;
	}
	int   CUIMoveState::OnStateName(const SStringA& strValue, int bLoading)
	{
		    m_strStateName= strValue;
	    	m_nNeedData=1;
			return 1;
	}
	 void  CUIMoveState::OnNcLButtonDown(unsigned int nFlags,CPoint pt)
	 {
		 CRect rt;
		 CUIWindow::GetWindowRect(&rt);
		 GetScalRect(rt,m_fDisplayScale);
		

		 m_ptClick = pt - rt.TopLeft();
	
		 int rowOut = 0, colOut = 0;
		 if (m_pIWarp->TestPtInMestWarping(pt, m_CurWarpInfo.rowId, m_CurWarpInfo.colId, rt))
		 {
			 m_bDraging = EDrag_WarpVect;
		 }
	 }
	 void  CUIMoveState::SetDisplayScale(float fDisplayScale)
	 {
	    
		m_fDisplayScale= fDisplayScale;
	 }

	 void   CUIMoveState::OnMouseMove(unsigned int nFlags,CPoint pt)
	 {
		
		m_nMoveing=0;
	    if(m_nIsLock)
			return ;//!m_nLigature&&
        if(m_bDraging&&  nFlags & 0x0001 ==0x0001)
        {
			if(m_bDraging==EDrag_Client)
			{
				    m_nMoveing =1;
					CPoint ptLT =pt - m_ptClick;
					ptLT.x *= m_fDisplayScale;
					ptLT.y *= m_fDisplayScale;
					CRect rcWnd;
					CUIWindow::GetWindowRect(&rcWnd);
					rcWnd.MoveToXY(ptLT);
		            
					
					 CPoint pts[4];

					 pts[0].y = rcWnd.top;
					 pts[0].x = rcWnd.left;

					 pts[1].y = rcWnd.top;
					 pts[1].x = rcWnd.right;

					 pts[2].y = rcWnd.bottom;
					 pts[2].x = rcWnd.right;

					 pts[3].y = rcWnd.bottom;
					 pts[3].x = rcWnd.left;
					 
					 if(CheckMagnetisRect(pts, 4,m_sMagnetRegion))
					 {
						 int height = rcWnd.Height();
						 int width  = rcWnd.Width();

						
						   rcWnd.left= pts[0].x;
						   rcWnd.top =  pts[0].y;
						   rcWnd.right =   rcWnd.left + width;
						   rcWnd.bottom =  rcWnd.top  + height;
						 
					 }

					
					OnRelayout(&rcWnd);
					SStringA Pos;
					Pos.Format("%d,%d,@%d,@%d",rcWnd.left,rcWnd.top,rcWnd.Width(),rcWnd.Height());
					this->SetAttribute("pos", Pos);
					this->GetParent()->UpdateChildrenPosition();

					/* UIEvent ev;
					 ev.Handle = this;
					 ev.type   = E_CltMove;
					 ev.pa1    = 0;
					 GetContainer()->GetIUIMessage()->OnUIEvent(&ev);*/
			}else if(m_bDraging ==EDrag_WarpVect)
			{
				CRect rcWnd;
				GetWindowRect(&rcWnd);
				GetScalRect(rcWnd, m_fDisplayScale);
				SUVInfo Out;
			  
				float xf = (float)(pt.x-rcWnd.left)/rcWnd.Width();
				float yf = (float)(pt.y-rcWnd.top)/rcWnd.Height();
				Out.x =xf;
				Out.y =yf;
				m_pIWarp->SetPtInfo(m_CurWarpInfo.rowId,m_CurWarpInfo.colId, Out);
				this->Invalidate();

				//	char temp[64];
	          //   sprintf(temp,"xx%3f-%3f,%d-%d",xf,yf,m_CurWarpInfo.rowId,m_CurWarpInfo.colId );
				//	OutputDebugStringKK(temp);
			}

        }else
		{
		
		   m_bDraging = 0;
		}
	 }
	 
	 void   CUIMoveState::OnKeyMove(EKEY_MOVE KeyMove)
	 {
	    CRect rt;
		CUIWindow::GetWindowRect(&rt);
	
		/*if(!m_nLigature)
			return ;*/
		int ps = m_fDisplayScale*1;
		if(KeyMove == EKM_Left)
		{
			rt.left  -=ps;
			rt.right -=ps;
		}
		else if(KeyMove == EKM_Top){
			rt.top    -=ps;
			rt.bottom -=ps;
		}else if(KeyMove == EKM_Right)
		{
		    rt.left  +=ps;
			rt.right +=ps;
		}
        else if(KeyMove == EKM_Bottom)
		{
		    rt.top    +=ps;
			rt.bottom +=ps;
		}
		
		SStringA Pos;
		OnRelayout(&rt);
		Pos.Format("%d,%d,@%d,@%d",rt.left,rt.top,rt.Width(),rt.Height());
		this->SetAttribute("pos", Pos);
		this->GetParent()->UpdateChildrenPosition();

		UIEvent ev;
		ev.Handle = this;
		ev.type   = E_CltMove;
		ev.pa1    = 0;
		GetContainer()->GetIUIMessage()->OnUIEvent(&ev);

		m_bDraging =0;
		this->Invalidate();
	 }
	 void CUIMoveState::OnNcMouseMove(unsigned int nFlags,CPoint pt)
	 {
		 int id = 0;
		 id++;
	 }
	 void   CUIMoveState::OnNcMouseLeave(unsigned int nFlags)
	 {
	         m_bDraging =0;
	 }
	int    CUIMoveState::OnAttrBkSkin(SStringA strValue,BOOL bLoading)
	{
	   if(strValue.Compare("")){
			ISkinObj *pSbSkin=GETSKIN(strValue,GetScale());
			assert(pSbSkin);
			if(!pSbSkin->IsClass(CSkinImgFrame::GetClassName()))
				return E_FAIL;
			m_pBkSkin = pSbSkin;
		}else{
		    m_pBkSkin = 0;
		}
		return S_OK;
	}
	void   CUIMoveState::LoadData( IRenderTarget  *pRT)
	{
	    kkRect rt;
		this->GetWindowRect(&rt);
		int L = rt.left;
		int R = rt.top;
		GetScalRect(rt,m_fDisplayScale);
		
        //left
	    wchar_t temp[128] = L"";
		L  +=m_szView.cx;
	    R  +=m_szView.cy;
	  

		swprintf_s( temp,128,L"(%d,%d)",L,R);
		
		kkSize sz;
		pRT->MeasureText(m_pFont,temp,&sz);  //D3D9  54,12;   GL   60,16
		rt.top  += 1;
		rt.left += 1;
		rt.right=rt.left+sz.cx;
		rt.bottom = rt.top+sz.cy;
		pRT->DrawText(m_pFont,temp,&rt);
      
		
		this->GetWindowRect(&rt);
		int B = rt.bottom;
		R = rt.right;
		GetScalRect(rt,m_fDisplayScale);
		memset(temp,0,128);
		
        B +=m_szView.cy;
	    R +=m_szView.cx;
		swprintf_s(temp,128,L"(%d,%d)", R, B);	
		pRT->MeasureText(m_pFont,temp,&sz);
        this->GetWindowRect(&rt);
		GetScalRect(rt, m_fDisplayScale);
		rt.right-=5;
		rt.left=rt.right-sz.cx;
		rt.bottom -=2;
		rt.top = rt.bottom - sz.cy;
		
        pRT->DrawText(m_pFont,temp,&rt);

		
		//center;
		memset(temp, 0, 128);
		char* lx=(char*)m_strStateName.GetBuffer(64);
		charTowchar2(lx, temp, 128);
        
		pRT->MeasureText(m_pFont,temp,&sz);
        this->GetWindowRect(&rt);
		GetScalRect(rt, m_fDisplayScale);
		rt.left = rt.left+(rt.right-rt.left)/2-sz.cx/2;
		rt.top = rt.top+(rt.bottom-rt.top)/2 -sz.cy/2;
		rt.right=rt.left+sz.cx;
		rt.bottom = rt.top+sz.cy;
		
        pRT->DrawText(m_pFont,temp,&rt);
		
	}
	void  CUIMoveState::OnStatgePaint( IRenderTarget  *pRT)
	{	
		unsigned int testColor = 0xFFFFFFFF;
		CRect StageRt;
		CRect ClRt;
		StageRt.left = 0;
		StageRt.top =0;
		StageRt.right  = pRT->GetWidth();
		StageRt.bottom = pRT->GetHeight();
        pRT->FillRect(&StageRt, 0xFF000000);
		
		GetWindowRect(&ClRt);
		int left = ClRt.left;
		int top  = ClRt.top;

		ClRt.MoveToXY(0,0);
		
		if(m_nLigature){
			std::vector<CUIWindow*>::iterator It= m_IntersectCtlVec.begin();
			for(;It!=m_IntersectCtlVec.end();++It)
			{
				 CUIWindow* Item=*It;
				 if(Item->IsVisible(0)){
						 CRect rt;
						 Item->GetWindowRect(&rt);
						 int ox= rt.left - left;
						 int oy= rt.top - top;
						 rt.MoveToXY(ox,oy);
						 //超出了区域则会有线条出现
						 /*if(rt.right>ClRt.right)
						    rt.right=ClRt.right-1;

						 if (rt.bottom>ClRt.bottom)
							 rt.bottom = ClRt.bottom - 1;

						 if (rt.top<ClRt.top)
							 rt.top = ClRt.top ;

						 if (rt.left<ClRt.left)
							 rt.left = ClRt.left;*/

						 Item->SendMessage(UI_PAINT_EX, (uint_ptr)pRT, (uint_ptr)&rt);
				 }
			}
		}else{
		   
			if(m_pBkSkin){
			         kkSize sz=m_pBkSkin->GetSkinSize();
					 CRect rt =StageRt;
					 rt.left+=rt.Width()/2-sz.cx/2;
					 rt.right=rt.left+sz.cx;
					 
					 rt.top  =rt.Height()/2 -sz.cy/2;
					 rt.bottom=rt.top+sz.cy;
					 m_pBkSkin->Draw(pRT,rt,0);
			}
		}
	    
		//网格
		/*StageRt.right  = pRT->GetWidth();
		StageRt.bottom = pRT->GetHeight();
		for(int i=0;i<StageRt.bottom;i+=100){
			pRT->DrawLine(0,i,StageRt.right,i,testColor );
		}

		for(int i=0;i<StageRt.right;i+=100){
			pRT->DrawLine(i, 0,i, StageRt.bottom,testColor );
		}*/
		//m_IntersectCtlVec.clear();
	}
	void  CUIMoveState::OnStatgeEditorPaint(IRenderTarget  *pRT, CRect& rcParent)
	{
	    unsigned int testColor = 0xFFFFFFFF;
		
      
		CRect StageRt;
		StageRt.left = 0;
		StageRt.top = 0;
		StageRt.right = pRT->GetWidth();
		StageRt.bottom = pRT->GetHeight();  
	
		GetWindowRect(&StageRt);

		float fDisplayScale= (float)StageRt.Width()/ rcParent.Width();
		float fDisplayScale2 = (float)StageRt.Height() / rcParent.Height();
		GetScalRect(StageRt, fDisplayScale);

	//	StageRt.MoveToXY(rcParent.left, rcParent.top);
		int left = StageRt.left- rcParent.left;
		int top  = StageRt.top- rcParent.top;

	    std::vector<CUIWindow*>::iterator It= m_IntersectCtlVec.begin();
		for(;It!=m_IntersectCtlVec.end();++It)
		{
		     CUIWindow* Item=*It;
			 if(Item->IsVisible(0)){
					 CRect rt;
					 Item->GetWindowRect(&rt);
					 GetScalRect(rt, fDisplayScale);
					 
					 int ox= rt.left - left;
					 int oy= rt.top - top;
					 rt.MoveToXY(ox,oy);

					 Item->SendMessage(UI_PAINT_EX, (uint_ptr)pRT, (uint_ptr)&rt);
			 }
		}
		
		
		
		

	
	}
	void  CUIMoveState::OnPaint( IRenderTarget  *pRT)
	{
		if(m_pFont==0){
			this->GetContainer()->GetIRenderFactory()->CreateFont(&m_pFont,12,L"宋体");
		}
		LoadData( pRT);


		CRect Rt;
		this->GetClientRect(&Rt);
		GetWindowRect(&Rt);
	    GetScalRect(Rt,m_fDisplayScale);

		bool ac=false;

	    if(this->GetParent()->IsClass("uimsview"))
		 {
              CUIMsView* Par=(CUIMsView* )this->GetParent();
             if(Par->GetActiveStage()==this)
			 {
			    ac =true;
			 }
		 }

		if(m_nEnbleIWarp&&(this->GetContainer()->GetUIWndCapture()==this|| ac))
		    m_pIWarp->OnPaintMesh(pRT,Rt,this->m_style.m_crBorder);
	}
	void  CUIMoveState::OnEraseBkgnd( IRenderTarget  *pRT)
	{
	
		
	  /*  CRect rt;
		GetClientRect(&rt);
        GetScalRect(rt,m_fDisplayScale);*/

		/*if(this->m_style.m_crBg!= CR_INVALID)
			pRT->FillRect(&rt,this->m_style.m_crBg);*/

		CRect rcWindow;
		GetWindowRect(&rcWindow);
		GetScalRect(rcWindow,m_fDisplayScale);

		if(!m_style.GetMargin().IsRectNull())
		{
			if(this->m_style.m_crBorder!= CR_INVALID)
			{
				  //left
				  if(this->GetContainer()->GetUIWndCapture()==this){
					  pRT->DrawLine( rcWindow.left+1, rcWindow.top, rcWindow.left+ 1, rcWindow.bottom,m_style.m_crBorder);
				  } 
				 	  pRT->DrawLine( rcWindow.left, rcWindow.top, rcWindow.left, rcWindow.bottom,m_style.m_crBorder);

				
				  //right
				  if(this->GetContainer()->GetUIWndCapture()==this){
                       pRT->DrawLine( rcWindow.right-1, rcWindow.top, rcWindow.right-1, rcWindow.bottom,m_style.m_crBorder);
				  } 
				  pRT->DrawLine( rcWindow.right, rcWindow.top, rcWindow.right, rcWindow.bottom,m_style.m_crBorder);

				
				  //top
				  if(this->GetContainer()->GetUIWndCapture()==this){
					  pRT->DrawLine( rcWindow.left, rcWindow.top+1, rcWindow.right, rcWindow.top+1,m_style.m_crBorder);
				  } 
				   pRT->DrawLine( rcWindow.left, rcWindow.top, rcWindow.right, rcWindow.top,m_style.m_crBorder);


				 //bottom
				  if(this->GetContainer()->GetUIWndCapture()==this){
					  pRT->DrawLine( rcWindow.left, rcWindow.bottom-1, rcWindow.right,rcWindow.bottom-1,m_style.m_crBorder);
				  } 
				  pRT->DrawLine( rcWindow.left, rcWindow.bottom, rcWindow.right,rcWindow.bottom,m_style.m_crBorder);

			}
			   
		}
	}
}
