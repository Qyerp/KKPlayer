#ifndef BezierSpline_H_
#define BezierSpline_H_

#include <vector>
#include "Core/IRender.h"

namespace KKUI
{
	//2阶曲线
	class CBezierSpline
	{
	   public:
				 CBezierSpline();
				 ~CBezierSpline();   
				 //根据点获取控制点(2阶)
				 static void    GetCurveControlPoints(const std::vector<SUVInfo>& knots,std::vector<SUVInfo>& firstCtrlPt,std::vector<SUVInfo>& secondCtrlPt);
				 static void    GetBezier(SUVInfo xy1, SUVInfo xy2, SUVInfo cp1, SUVInfo cp2, std::vector<SUVInfo>& XyVec);
	    private:
				 static void    GetFirstControlPoints(const std::vector<float>& rhs, std::vector<float>& x);
	};

}
#endif