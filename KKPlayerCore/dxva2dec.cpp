#include "dxva2dec.h"


//#include "UI/DXRender/IDXVA2Ctx.h"

extern const char *ppPixFmtName[AV_PIX_FMT_NB];
typedef struct SurfaceWrapper {
	IDXVA2Ctx* pCtx;
	AVFrame *pPic;
	LPDIRECT3DSURFACE9   surface;
	IDirectXVideoDecoder* pDXDecoder;
} SurfaceWrapper;



static void ffmpeg_ReleaseFrameBuf( void *p1,uint8_t *p2 )
{
	
	SurfaceWrapper *sw = (SurfaceWrapper *)p1;
	AVFrame *p_ff_pic  = (AVFrame *)sw->pPic;
	IDXVA2Ctx* pCtx    = sw->pCtx ;


	if(!pCtx->ReleaseSurface(sw->surface, sw->pDXDecoder)){
	   // assert(0);
	}
	delete sw;	
}

static int ffmpeg_GetFrameBuf( struct AVCodecContext *p_context,AVFrame *p_ff_pic,int flags)
{
	IDXVA2Ctx* pCtx = (IDXVA2Ctx *)p_context->opaque;
	AVPixelFormat chroma;
	if(pCtx)
	{
		LPDIRECT3DSURFACE9 d3dSurface = NULL;
		int ix= pCtx->GetSurfaceIndex(p_ff_pic);
		if( ix<0 )
		{
			av_log(NULL, AV_LOG_ERROR, "VaGrabSurface failed" );
			return -1;
		}
		if (!p_ff_pic->data[0])
			return -1;
		SurfaceWrapper *surfaceWrapper = new SurfaceWrapper();
		surfaceWrapper->pCtx = pCtx ;
		surfaceWrapper->pPic = p_ff_pic;
		surfaceWrapper->surface=(LPDIRECT3DSURFACE9 )p_ff_pic->data[0];
		p_ff_pic->data[4] = (uint8_t *)ix;
		
		
		p_ff_pic->buf[0] = av_buffer_create(NULL, 0, ffmpeg_ReleaseFrameBuf, surfaceWrapper, 0);
		return 0;
	}
	return -1;
}



static enum AVPixelFormat ffmpeg_GetFormat( AVCodecContext *p_context,const enum AVPixelFormat *pi_fmt )
{
	

	IDXVA2Ctx *Ctx = (IDXVA2Ctx *)p_context->opaque;
    
	/* Try too look for a supported hw acceleration */
	for( int i = 0; pi_fmt[i] != AV_PIX_FMT_NONE; i++ )
	{
		/* Only VLD supported */
		if( pi_fmt[i] == AV_PIX_FMT_DXVA2_VLD )
		{
			//av_log(p_context, AV_LOG_DEBUG, "Trying DXVA2\n" );
			return pi_fmt[i];
		}
	}

	return avcodec_default_get_format( p_context, pi_fmt );
}



static int SetupCtx(AVCodecContext  *pCodecCtx, AVPixelFormat *chroma,int width, int height)
{

}


int BindDxva2Module2(	AVCodecContext  *pCodecCtx,IDXVA2Ctx* pIDxva)
{
	int fmt = pIDxva->GetPixFmt();
	pCodecCtx->opaque = pIDxva;
	pCodecCtx->pix_fmt         = ( AVPixelFormat )fmt;
	pCodecCtx->hwaccel_context = pIDxva->GetDxvaContext();
	pCodecCtx->get_format      = ffmpeg_GetFormat;
	pCodecCtx->get_buffer2     = ffmpeg_GetFrameBuf;
	pCodecCtx->thread_count    = 1;
	pCodecCtx->slice_flags   |= SLICE_FLAG_ALLOW_FIELD;
	
	int res = 0;

	if (!pCodecCtx->hwaccel_context){
	    return 1;
	}
	return 0;
}