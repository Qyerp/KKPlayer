#include "UIStageEditor.h"
#include <math.h>
namespace KKUI
{
	
	CUIStageEditor::CUIStageEditor():m_pStage (0),m_bDraging(0)
	{
		
	}
	CUIStageEditor::~CUIStageEditor()
	{
		if(m_pStage ){
		   m_pStage ->Release();
		}
	}

   
	void    CUIStageEditor::SetStage (CUIMoveState*  pStage)
	{
		if( pStage !=0)
		{
	        m_pStage = pStage;
			m_pStage->AddRef();
		}
	}
	
	void CUIStageEditor::OnPaint( IRenderTarget  *pRT)
	{

		CRect WinRt;
		this->GetWindowRect(&WinRt);
		
		pRT->FillRect(&WinRt,m_style.m_crBg);
        
		 //��̨����
		if(m_pStage)
	      m_pStage->OnStatgeEditorPaint(pRT,WinRt);

	}
    
	void CUIStageEditor::OnEraseBkgnd( IRenderTarget  *pRT)
	{
		

	}
	void CUIStageEditor::OnSize(unsigned int nType, int x,int y)
	{
	
	}

	void  CUIStageEditor::OnLButtonDown  (unsigned int nFlags,CPoint pt)
	{
		if(m_pStage){
		    CRect rt;
		    CUIWindow::GetWindowRect(&rt);
			if(m_pStage->m_pIWarp->TestPtInMestWarping(pt,m_CurWarpInfo.rowId,m_CurWarpInfo.colId,rt))
			{
				 m_bDraging =EDrag_WarpVect;
			}else{
			 m_bDraging =0 ;
			}
		}

	}
	void   CUIStageEditor::OnLButtonUp(UINT nFlags, CPoint point)
	{
		
	  m_bDraging =0;
	}
	void  CUIStageEditor::OnMouseMove    (unsigned int nFlags,CPoint pt)
	{
	  if(m_pStage){
			if(m_bDraging ==EDrag_WarpVect)
			{
				CRect rcWnd;
				GetWindowRect(&rcWnd);
				SUVInfo Out;
			  
				float xf = (float)(pt.x-rcWnd.left)/rcWnd.Width();
				float yf = (float)(pt.y-rcWnd.top)/rcWnd.Height();
				Out.x =xf;
				Out.y =yf;
				m_pStage->m_pIWarp->SetPtInfo(m_CurWarpInfo.rowId,m_CurWarpInfo.colId, Out);
				this->Invalidate();
			}
		}
	}
	void   CUIStageEditor::OnNcMouseMove(unsigned int nFlags,CPoint pt)
	{
	
	}

	int    CUIStageEditor::OnNcHitTest(kkPoint pt)
	{
		CRect rcWindow;
		GetWindowRect(&rcWindow);
       
	    int rowOut = 0, colOut = 0;
		CPoint ptx;
		ptx.x = pt.x;
		ptx.y = pt.y;
		int ret=0;

		{
		   CRect rcClient;
		   GetClientRect(&rcClient);
		   ret=!rcClient.PtInRect(pt);
		}
	
		if( m_pStage){
			if (ret==1 &&m_pStage->m_pIWarp->TestPtInMestWarping(ptx, rowOut, colOut, rcWindow))
			{
				return 0;
			}else {
				if (m_bDraging == EDrag_WarpVect) {
					return 0;
				}
			}
		}

		return ret;
	}
	int    CUIStageEditor::IsContainPoint(const kkPoint &pt,int bClientOnly)
	{

        if (m_bDraging == EDrag_WarpVect) {
			return 1;
		}
		CPoint Pt;
		Pt.x = pt.x;
		Pt.y = pt.y;
		
	    int bRet = 0;
		if(bClientOnly){
			CRect rcClient;
			GetClientRect(&rcClient);
			bRet = rcClient.PtInRect(Pt);
		}else{
			CRect rcWindow;
		    GetWindowRect(&rcWindow);
			bRet = rcWindow.PtInRect(Pt);
		}
		
		return bRet;

	}
    CUIWindow*   CUIStageEditor::WndFromPoint( kkPoint  ptHitTest, int bOnlyText)
	{

	    CRect rcWindow;
		GetWindowRect(&rcWindow);
		
		int rowOut = 0, colOut = 0;

		CPoint ptx;
		ptx.x = ptHitTest.x;
		ptx.y = ptHitTest.y;
		
		if( m_pStage)
		{
			if (m_pStage->m_pIWarp->TestPtInMestWarping(ptx, rowOut, colOut, rcWindow))
			{
				return this;
			}else if(m_bDraging == EDrag_WarpVect) {
				return this;
			}
		}
		
	
		if(!rcWindow.PtInRect(ptx))
			return NULL;

	

		
		return this;
	}
}