#ifndef KKUI_UIMisc_H_
#define KKUI_UIMisc_H_
#include "IkkDraw.h"
namespace KKUI
{
#ifndef COLORREF
    #define   COLORREF unsigned long 
#endif
	class CPoint:public  kkPoint
	{
		public:
			   CPoint()
			   {
			        x=0;
					y=0;
			   }
			   CPoint(int x1,int y1)
			   {
			        x=x1;
					y=y1;
			   }
			   CPoint(kkPoint& point)
			   {
				   x=point.x;
					y=point.y;
			   }
			    void operator -=(CPoint point)
				{
					x -= point.x;
					y -= point.y;
				}
				CPoint operator -(CPoint point) 
				{
					CPoint xx;
			    		 xx.x =x- point.x;
						 xx.y =y- point.y;
						return xx;
				}

				CPoint operator =(kkPoint& point) 
				{
					    x = point.x;
						y =point.y;
						return *this;
				}

				int operator !=(kkPoint point) const
				{
					return (x != point.x || y != point.y);
				}

	};

	class CSize:  public kkSize
	{
	public:
		CSize()
		{
		   cx = 0;
		   cy = 0;
		}
		CSize(int x,int y)
		{
		    cx = x;
			cy = y;
		}
		int operator !=( kkSize size) const
		{
			return (cx != size.cx || cy != size.cy);
		}

		  bool operator ==(kkSize size) const
    {
        return (cx == size.cx && cy == size.cy);
    }
	};
    class CRect : public     kkRect
	{
			public:
			
				CRect()
				{
					left = 0;
					top = 0;
					right = 0;
					bottom = 0;
				}

				CRect(int l, int t, int r, int b)
				{
					left = l;
					top = t;
					right = r;
					bottom = b;
				}

				CRect(const  kkRect& srcRect)
				{
					this->bottom = srcRect.bottom;
					this->left = srcRect.left;
					this->right = srcRect.right;
					this->top = srcRect.top;
				}

				CRect( kkRect* lpSrcRect)
				{
					this->bottom = lpSrcRect->bottom;
					this->left = lpSrcRect->left;
					this->right =lpSrcRect->right;
					this->top = lpSrcRect->top;
				}
				CRect(kkPoint point, kkSize size)
				{
					right = (left = point.x) + size.cx;
					bottom = (top = point.y) + size.cy;
				}
				void InflateRect(lpkkRect lpRect)
				{
					left -= lpRect->left;
					top -= lpRect->top;
					right += lpRect->right;
					bottom += lpRect->bottom;
				}
				void DeflateRect(lpkkRect lpRect)
				{
					left += lpRect->left;
					top += lpRect->top;
					right -= lpRect->right;
					bottom -= lpRect->bottom;
				}
				void DeflateRect(CRect& lpRect)
				{
					left += lpRect.left;
					top += lpRect.top;
					right -= lpRect.right;
					bottom -= lpRect.bottom;
				}

				 void DeflateRect(int x, int y)
				 {
					/* x=-x;
					 y=-y;*/
				    left += x;
					top  += y;
					right += x;
					bottom += y;
				 }
				void OffsetRect(int x,int y)
				{
					left += x;
					right += x;

					bottom += y;
					top += y;
				}
				
				int EqualRect(lpkkRect lpSrcRect) const
				{
					if(this->bottom ==lpSrcRect->bottom &&
					this->left == lpSrcRect->left &&
					this->right == lpSrcRect->right &&
					this->top == lpSrcRect->top)
					    return 1;
					else
						return 0;
				}

				int Width()
				{
				    return right - left;
				}
				int Height(){
				   return bottom-top;
				}

				bool PtInRect(const kkPoint &pt){

					  kkRect* rt=this;
	                  return kkPtInRect(pt, *rt);
                }

				void MoveToY(int y)
				{
					bottom = Height() + y;
					top = y;
				}

				void MoveToX(int x)
				{
					right = Width() + x;
					left = x;
				}

				void MoveToXY(int x, int y)
				{
					MoveToX(x);
					MoveToY(y);
				}

				void MoveToXY(kkPoint pt)
				{
					MoveToX(pt.x);
					MoveToY(pt.y);
				}

				void OffsetXY(int x, int y)
				{
					right += x;
					left  += x;

					top += y;
					bottom  += y;
				}

				void SetRect(int x1, int y1, int x2, int y2)
				{
					left = x1;
					top = y1;
					right = x2;

					bottom = y2;
					
				}

				int IsRectNull() const
				{
					return (left == 0 && right == 0 && top == 0 && bottom == 0);
				}
				CPoint CenterPoint() const
				{
					return CPoint((left + right) / 2, (top + bottom) / 2);
				}

				 CPoint& TopLeft()
				{
					return *((CPoint*)this);
				}
				bool IsRectEmpty()
				{
				        if(right-left==0)
							return 1;
						if(bottom - top ==0)
							return 1;
						return 0;
				}
				CSize Size() const
				{
					return CSize(right - left, bottom - top);
				}
	};


}
#endif