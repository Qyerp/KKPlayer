#include "UICurve.h"
#include "Math/BezierSpline.h"

namespace KKUI
{
	void GetScalRect(kkRect& rt,const float& f);
	void GetScalRect(CRect& rt,const float& f);
	
	CUICurve::CUICurve(void)
	{
		m_FirstCtrl1.x  = 0.0;
		m_FirstCtrl1.y  = 0.0;

		m_SecondCtrl2.x = 0.0;
		m_SecondCtrl2.y = 0.0;
	}
	CUICurve::~CUICurve(void)
	{
	         
	}
	
	
	int   CUICurve::OnCreate( int)
    {    
	    

         return 0;
    }
    

	void  CUICurve::OnSize(unsigned int nType, int x,int y)
	{
		InitCtlInfo();
	}

    void    CUICurve::InitCtlInfo()
	{
	     CRect Rt;
		 GetClientRect(&Rt);
		

		/* Rt.left+=10;
		 Rt.top +=10;

		 Rt.bottom -= 10;
		 Rt.right  -= 10;*/

		 int width = Rt.right -Rt.left;
		 int height = Rt.bottom - Rt.top;

		 m_Uv1.x = Rt.left;
		 m_Uv1.y = Rt.top;
		 m_Uv2.x = Rt.right;
		 m_Uv2.y = Rt.bottom;


		 m_FirstCtrlPt  = m_Uv1;
		 m_FirstCtrlPt.x+=100;
		 m_FirstCtrlPt.y+=10;
         
		 m_SecondCtrlPt = m_Uv2;
		 m_SecondCtrlPt.x -=100;
		 m_SecondCtrlPt.y -=10;

		 CalScale();
	}
	void   CUICurve::CalScale()
	{
	     CRect Rt;
		 GetClientRect(&Rt);


		  //����
		 m_FirstCtrl1.x = m_FirstCtrlPt.x / Rt.Width(); 
		 m_FirstCtrl1.y = m_FirstCtrlPt.y / Rt.Height();

		 m_SecondCtrl2.x = m_SecondCtrlPt.x / Rt.Width(); 
		 m_SecondCtrl2.y = m_SecondCtrlPt.y / Rt.Height();
	}

	void CUICurve::SetScale( SUVInfo fUv1,SUVInfo fUv2)
	{
	       m_FirstCtrl1  = fUv1;
		   m_SecondCtrl2 = fUv2;


		   CRect Rt;
		   GetClientRect(&Rt);
		   m_FirstCtrlPt.x  =   Rt.left + m_FirstCtrl1.x * Rt.Width(); 
		   m_FirstCtrlPt.y  =   Rt.top  + m_FirstCtrl1.y * Rt.Height();


		   m_SecondCtrlPt.x =  Rt.left + m_SecondCtrl2.x * Rt.Width(); 
		   m_SecondCtrlPt.y =  Rt.top  + m_SecondCtrl2.y * Rt.Height();


		   if(fUv1.x==0.00000&&fUv1.y==0.00000&& fUv2.x==0.00000&&fUv2.y==0.00000)
		   {
			 std::vector<SUVInfo> knots;
			 std::vector<SUVInfo> firstCtrlPt( 1);
			 std::vector<SUVInfo> secondCtrlPt( 1);

			 knots.push_back(m_Uv1);
			 knots.push_back(m_Uv2);
			

		     CBezierSpline::GetCurveControlPoints(knots,firstCtrlPt,secondCtrlPt);
             m_FirstCtrlPt = firstCtrlPt[0];
			 m_SecondCtrlPt = secondCtrlPt[0];
		   }
		
	}
	void   CUICurve::GetScale( SUVInfo& fUv1,SUVInfo& fUv2)
	{
	    fUv1 = m_FirstCtrl1;
		fUv2 = m_SecondCtrl2;
	}
	 void   CUICurve::OnPaint( IRenderTarget  *pRT)
	{
		
		 {
			
			 kkPoint pt={m_Uv1.x,m_Uv1.y};
		     pRT->FillRectByPt(&pt,4,4,0xFFFF0000);
			 pRT->DrawLine(m_Uv1.x,m_Uv1.y,m_FirstCtrlPt.x,m_FirstCtrlPt.y ,0xFFFF0000);
		 }


	
		 {
			 kkPoint pt={m_Uv2.x,m_Uv2.y};
			 pRT->FillRectByPt(&pt, 4, 4, 0xFFFF0000);
			  pRT->DrawLine(m_Uv2.x,m_Uv2.y, m_SecondCtrlPt.x, m_SecondCtrlPt.y ,0xFFFF0000);
		 }

		 

		 {
			
			 kkPoint pt = { m_FirstCtrlPt.x,m_FirstCtrlPt.y };
			 pRT->FillRectByPt(&pt, 6, 6, 0xFF0000FF);
		 }
		 
		 {
			
			 kkPoint pt = {  m_SecondCtrlPt.x, m_SecondCtrlPt.y };
			 pRT->FillRectByPt(&pt, 6, 6, 0xFF00FF00);
		 }


		 
		 {
			  std::vector<SUVInfo> XyVec;
		      CBezierSpline::GetBezier(m_Uv1,m_Uv2,m_FirstCtrlPt,m_SecondCtrlPt,XyVec);
		      pRT-> DrawLines(&XyVec[0],XyVec.size(),0xFFFF0000);
		 }		

	 }
	 void   CUICurve::OnEraseBkgnd( IRenderTarget  *pRT)
	 {
	
	
	 }

	 void   CUICurve::OnLButtonDown  (unsigned int nFlags,CPoint pt)
	 {
	 
		 kkPoint InPt={m_FirstCtrlPt.x, m_FirstCtrlPt.y };
		 kkPoint InPt2={m_SecondCtrlPt.x,m_SecondCtrlPt.y };
		 if( kkPtInPt(pt, InPt,10,10)){
		 
			 m_bDraging = EDrag_Cp1;
			
		 }else if( kkPtInPt(pt, InPt2,10,10)){
		 
			 m_bDraging = EDrag_Cp2;
			
		 }else{
		     m_bDraging =  EDrag_None;
		 }

	 }
	 void   CUICurve::OnLButtonUp    (UINT nFlags, CPoint point)
	 { 
		  m_bDraging =  EDrag_None;
		  CalScale();
	 }
	 void   CUICurve::OnMouseMove    (unsigned int nFlags,CPoint pt)
	 {

			if(m_bDraging&&  nFlags & 0x0001 ==0x0001)
			{
					if( m_bDraging == EDrag_Cp1){
						m_FirstCtrlPt.x = pt.x;
						m_FirstCtrlPt.y = pt.y;
					}else if( m_bDraging == EDrag_Cp2){
						m_SecondCtrlPt.x = pt.x;
						m_SecondCtrlPt.y = pt.y;
					}
		            
					CalScale();
					UIEvent ev;
					ev.Handle = this;
					ev.type   = E_CltMove;
					ev.pa1    = 0;
					GetContainer()->GetIUIMessage()->OnUIEvent(&ev);
			}
	 }
}
