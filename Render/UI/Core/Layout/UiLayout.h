﻿#ifndef KKUI_UiLayout_H_
#define KKUI_UiLayout_H_
#include "ILayout.h"
#include "LayoutParamStruct.h"
#include "Core/AttrCracker.h"
#include "UIMisc.h"
#include <list>
namespace KKUI{


   
	class CUILayoutParam: public CLayoutParam, public LayoutParamStruct
	{
			KKUI_CLASS_NAME(CLayoutParam,CUILayoutParam,"uilayoutparam")
			public:
				   CUILayoutParam();
				   ~CUILayoutParam();

					virtual bool IsMatchParent(ORIENTATION orientation) const;

					virtual bool IsSpecifiedSize(ORIENTATION orientation) const;

					virtual bool IsWrapContent(ORIENTATION orientation) const;

					virtual CLayoutSize  GetSpecifiedSize(ORIENTATION orientation) const;

					virtual void Clear();

					virtual void SetMatchParent(ORIENTATION orientation);

					virtual void SetWrapContent(ORIENTATION orientation);

					virtual void SetSpecifiedSize(ORIENTATION orientation, const CLayoutSize& layoutSize);

					virtual void * GetRawData();
			public:
						bool IsOffsetRequired(ORIENTATION orientation) const;
						int  GetExtraSize(ORIENTATION orientation,int nScale) const;
			protected:
						int OnAttrWidth(const SStringA & strValue,int bLoading);

						int OnAttrHeight(const SStringA & strValue,int bLoading);

						int OnAttrSize(const SStringA & strValue,int bLoading);

						int OnAttrPos(const SStringA & strValue,int bLoading);

						int OnAttrOffset(const SStringA & strValue,int bLoading);

						KKUI_ATTRS_BEGIN()
							ATTR_CUSTOM("width",OnAttrWidth)
							ATTR_CUSTOM("height",OnAttrHeight)
							ATTR_CUSTOM("pos",OnAttrPos)
							ATTR_CUSTOM("size",OnAttrSize)
							ATTR_CUSTOM("offset",OnAttrOffset)
						KKUI_ATTRS_END()

			protected:
						//将字符串描述的坐标转换成POSITION_ITEM
						int StrPos2ItemPos(const SStringA &strPos,POS_INFO & posItem);

						//解析在pos中定义的前两个位置
						int ParsePosition12(const SStringA & pos1, const SStringA &pos2);

						//解析在pos中定义的后两个位置
						int ParsePosition34(const SStringA & pos3, const SStringA &pos4);		
	};

	
	class CUILayout: public CLayout
	{
			KKUI_CLASS_NAME_EX(CLayout,CUILayout,"uilayout",Layout)
			public:
					CUILayout(void);
					~CUILayout(void);

					virtual bool IsParamAcceptable(ILayoutParam *pLayoutParam) const;
					virtual void LayoutChildren(CUIWindow * pParent);
					virtual ILayoutParam * CreateLayoutParam() const;
					CSize MeasureChildren(CUIWindow* pParent,int nWidth,int nHeight) const;
	      protected:
			  struct WndPos{
					CUIWindow *pWnd;
					CRect      rc;
					bool       bWaitOffsetX;
					bool	   bWaitOffsetY;
			  };
			  void CalcPositionEx(std::list<WndPos> *pListChildren,int nWidth,int nHeight) const;
			  int CalcPostion(std::list<WndPos> *pListChildren,int nWidth,int nHeight) const;
			  int IsWaitingPos( int nPos ) const;
			  int PositionItem2Value(std::list<WndPos> *pLstChilds,std::list<WndPos>::iterator position,const POS_INFO &pos , int nMax,int bX,int nScale) const;
    };
}

#endif