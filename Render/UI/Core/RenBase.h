#ifndef RenBase_H_
#define RenBase_H_

//UV坐标信息
typedef struct SUVInfo
{
	float  x;
	float  y;
} SUVInfo;
typedef struct SinkFilter;
typedef void(*FnHandleSFData)(void* userData, SinkFilter* SF);
typedef struct SinkFilter {
	int   Id;
	//滤镜名称
	char   FilterName[32];
	int    Par1;
	int    Par2;
	int    Par3;
	int    Par4;
	int    Par5;

	float  Par6;
	float  Par7;
	float  Par8;
	float  Par9;
	float  Par10;
	const void* pridata;
	//处理滤镜数据
	FnHandleSFData fpHandleSFData;
	SinkFilter* Pre;
	SinkFilter* Next;
}SinkFilter;

#endif