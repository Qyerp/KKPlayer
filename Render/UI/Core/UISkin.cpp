#include "UISkin.h"
#define WINBASEAPI __declspec(dllimport)
#define WINAPI      __stdcall
extern "C"{                                                      
	WINBASEAPI int __stdcall MulDiv(int nNumber, int nNumerator,int nDenominator);
}

namespace KKUI
{
    //////////////////////////////////////////////////////////////////////////
		//CSkinImgList
		CSkinImgList::CSkinImgList()
		:m_nStates(1)
		,m_bTile(FALSE)
		,m_bVertical(FALSE)
		,m_filterLevel(kNone_FilterLevel)
		,m_bAutoFit(TRUE)
		{

		}

		CSkinImgList::~CSkinImgList()
		{
		}

		kkSize CSkinImgList::GetSkinSize()
		{
			kkSize ret = {0, 0};
			if(m_pImg) 
				ret=m_pImg->Size();
			if(m_bVertical) 
				ret.cy/=m_nStates;
			else 
				ret.cx/=m_nStates;
			return ret;
		}

		int CSkinImgList::IgnoreState()
		{
			return GetStates()==1;
		}

		int CSkinImgList::GetStates()
		{
			return m_nStates;
		}

		void CSkinImgList::_Draw(IRenderTarget *pRT,  kkRect* rcDraw,  unsigned int dwState,BYTE byAlpha)
		{
			if(!m_pImg)
				return;
			kkSize sz=GetSkinSize();
			kkRect rcSrc={0,0,sz.cx,sz.cy};
			if(m_bVertical) 
				 kkOffsetRect(&rcSrc,0, dwState * sz.cy);
			else
				 kkOffsetRect(&rcSrc, dwState * sz.cx, 0);

			pRT->DrawBitmapEx(rcDraw,m_pImg,&rcSrc,GetExpandMode(),byAlpha);
		}

		unsigned int CSkinImgList::GetExpandMode()
		{
			if(m_bAutoFit)
				return MAKELONG(m_bTile?EM_TILE:EM_STRETCH,m_filterLevel);
			else
				return MAKELONG(EM_NULL,m_filterLevel);/**/
			return 0;
		}

		void CSkinImgList::OnColorize(COLORREF cr)
		{
			if(!m_bEnableColorize)
				return;
			if(cr == m_crColorize) 
				return;
			m_crColorize = cr;

			//if(m_imgBackup)
			//{//restore
			//	LPCVOID pSrc = m_imgBackup->GetPixelBits();
			//	LPVOID pDst = m_pImg->LockPixelBits();
			//	memcpy(pDst,pSrc,m_pImg->Width()*m_pImg->Height()*4);
			//	m_pImg->UnlockPixelBits(pDst);
			//}else
			//{
			//	if(S_OK != m_pImg->Clone(&m_imgBackup)) return;
			//}
		 //   
			//if(cr!=0)
			//	SDIBHelper::Colorize(m_pImg,cr);
			//else
			//	m_imgBackup = NULL;//free backup
		}

		void CSkinImgList::_Scale(ISkinObj * skinObj, int nScale)
		{
			__super::_Scale(skinObj,nScale);
			CSkinImgList *pRet = (CSkinImgList*)(skinObj);
			pRet->m_nStates = m_nStates;
			pRet->m_bTile = m_bTile;
			pRet->m_bVertical = m_bVertical;
			pRet->m_filterLevel = m_filterLevel;
			pRet->m_bAutoFit = m_bAutoFit;
			
			kkSize szSkin = GetSkinSize();
			szSkin.cx = MulDiv(szSkin.cx,nScale,100);
			szSkin.cy = MulDiv(szSkin.cy,nScale,100);
			if(m_bVertical)
			{
				szSkin.cy *= GetStates();
			}else
			{
				szSkin.cx *= GetStates();
			}

			if(m_imgBackup)
			{
			//	m_imgBackup->Scale(&pRet->m_imgBackup,szSkin.cx,szSkin.cy,kHigh_FilterLevel);
			}
			if(m_pImg)
			{
			//	m_pImg->Scale(&pRet->m_pImg,szSkin.cx,szSkin.cy,kHigh_FilterLevel);
			}
		}



		///////*******************************************/////////////
		//  SSkinImgFrame
		CSkinImgFrame::CSkinImgFrame()
			: m_rcMargin(0,0,0,0)
		{

		}

		void CSkinImgFrame::_Draw(IRenderTarget *pRT, kkRect* rcDraw, unsigned int dwState,BYTE byAlpha)
		{
			if(!m_pImg) 
				return;
			kkSize sz=GetSkinSize();
			CPoint pt;
			if(IsVertical())
				pt.y=sz.cy*dwState;
			else
				pt.x=sz.cx*dwState;
			CRect rcSour;
			rcSour.left = pt.x;
			rcSour.top  = pt.y;

			rcSour.right  = rcSour.left + sz.cx;
			rcSour.bottom = rcSour.top  + sz.cy;

			pRT->DrawBitmap9Patch(rcDraw,m_pImg,&rcSour,&m_rcMargin,GetExpandMode(),byAlpha);
		}

		unsigned int CSkinImgFrame::GetExpandMode()
		{
			return  MAKELONG(m_bTile?EM_TILE:EM_STRETCH,m_filterLevel);
		}

		void CSkinImgFrame::_Scale(ISkinObj *skinObj, int nScale)
		{
			CSkinImgList::_Scale(skinObj,nScale);
			CSkinImgFrame * pClone = UIObj_cast<CSkinImgFrame>(skinObj);
			pClone->m_rcMargin.left = MulDiv(m_rcMargin.left , nScale, 100);
			pClone->m_rcMargin.top = MulDiv(m_rcMargin.top , nScale, 100);
			pClone->m_rcMargin.right = MulDiv(m_rcMargin.right , nScale, 100);
			pClone->m_rcMargin.bottom = MulDiv(m_rcMargin.bottom , nScale, 100);
		}



		/////////////***************ScrollbarSkin********************///////////////////////
		CSkinScrollbar::CSkinScrollbar():
		       m_nMargin(0),
			   m_bHasGripper(FALSE),
			   m_bHasInactive(FALSE)
		{
		    
		}

		CRect CSkinScrollbar::GetPartRect(int nSbCode, int nState,BOOL bVertical)
		{
			kkSize sz=GetSkinSize();
			CSize szFrame(sz.cx/9,sz.cx/9);
			if(nSbCode==SB_CORNOR)
			{
				return CRect(CPoint(szFrame.cx*8,0),szFrame);
			}else if(nSbCode==SB_THUMBGRIPPER)
			{
				return CRect(CPoint(szFrame.cx*8,(1+(bVertical?0:1))*szFrame.cy),szFrame);
			}else
			{
				if(nState==SBST_INACTIVE && !m_bHasInactive)
				{
					nState=SBST_NORMAL;
				}
				CRect rcRet;
				int iPart=-1;
				switch(nSbCode)
				{
				case SB_LINEUP:
					iPart=0;
					break;
				case SB_LINEDOWN:
					iPart=1;
					break;
				case SB_THUMBTRACK:
					iPart=2;
					break;
				case SB_PAGEUP:
				case SB_PAGEDOWN:
					iPart=3;
					break;
				}
				if(!bVertical) iPart+=4;
		        
				return CRect(CPoint(szFrame.cx*iPart,szFrame.cy*nState),szFrame);
			}
		}

		void CSkinScrollbar::_Draw(IRenderTarget *pRT,kkRect* prcDraw,unsigned int dwState,BYTE byAlpha)
		{
			if(!m_pImg) 
				return;
			int nSbCode=LOWORD(dwState);
			int nState=LOBYTE(HIWORD(dwState));
			BOOL bVertical=HIBYTE(HIWORD(dwState));
			CRect rcMargin(0,0,0,0);
			if(bVertical)
				rcMargin.top=m_nMargin,rcMargin.bottom=m_nMargin;
			else
				rcMargin.left=m_nMargin,rcMargin.right=m_nMargin;

			CRect rcSour=GetPartRect(nSbCode,nState,bVertical);
		    
			pRT->DrawBitmap9Patch(prcDraw,m_pImg,&rcSour,&rcMargin,m_bTile?EM_TILE:EM_STRETCH,byAlpha);
		    
			if(nSbCode==SB_THUMBTRACK && m_bHasGripper)
			{
				rcSour=GetPartRect(SB_THUMBGRIPPER,0,bVertical);
				CRect rcDraw=*prcDraw;
		        
				if (bVertical)
					rcDraw.top+=(rcDraw.Height()-rcSour.Height())/2,rcDraw.bottom=rcDraw.top+rcSour.Height();
				else
					rcDraw.left+=(rcDraw.Width()-rcSour.Width())/2,rcDraw.right=rcDraw.left+rcSour.Width();
				pRT->DrawBitmap9Patch(&rcDraw,m_pImg,&rcSour,&rcMargin,m_bTile?EM_TILE:EM_STRETCH,byAlpha);
			}
		}

		void CSkinScrollbar::_Scale(ISkinObj *skinObj, int nScale)
		{
			__super::_Scale(skinObj,nScale);

			CSkinScrollbar *pRet = UIObj_cast<CSkinScrollbar>(skinObj);
			pRet->m_nMargin = MulDiv(m_nMargin,nScale,100);
			pRet->m_bHasInactive = m_bHasInactive;
			pRet->m_bHasGripper = m_bHasGripper;
		}



}