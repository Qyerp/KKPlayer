/************dx �°����*************/
#ifndef dxdec_H_
#define dxdec_H_
#include "render/render.h"
extern "C" {
	#include "libavformat/avformat.h"
	#include "libavcodec/avcodec.h"
	#include "libavcodec/dxva2.h"
	#include "libswscale/swscale.h"
	#include "libavutil/buffer.h"
}
int BindDxModule(	AVCodecContext  *pCodecCtx,IDXTexture* pDx);
int BindProresModule(	AVCodecContext  *pCodecCtx);
#endif