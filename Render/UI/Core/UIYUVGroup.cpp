﻿#include "UIYUVGroup.h"
#include "UIYUV.h"
namespace KKUI
{
	CUIYUVGroup:: CUIYUVGroup(CUIMsView* msview):m_pMsView(msview),m_pPrevSibling(0), m_pNextSibling(0)
	{
	

	}
	CUIYUVGroup:: ~CUIYUVGroup()
	{
       std::vector<CUIYUV*>::iterator It= m_YuvVector.begin();
		for(;It!= m_YuvVector.end(); It++)
		{
			CUIYUV* Item = *It;
			m_pMsView->DestroyChild(Item);
		}
		m_YuvVector.clear();
	}

	CUIYUVGroup*  CUIYUVGroup::GetPrevGroup()
	{
	     return m_pPrevSibling;
	}
	CUIYUVGroup*  CUIYUVGroup::GetNextGroup()
	{
	   return m_pNextSibling;
	}

	void     CUIYUVGroup::SetPrevNextGroup(CUIYUVGroup* group,bool Prev)
	{
		if(Prev){
             m_pPrevSibling=group;
		}else{
             m_pNextSibling=group;
		}
	}

    void     CUIYUVGroup::SwopYuv(CUIYUV* srcYuv,CUIYUV* DestYuv,int pre)
	{
	    
		std::vector<CUIYUV*>::iterator It= m_YuvVector.begin();
		for(;It!= m_YuvVector.end(); It++)
		{
			if((*It)==srcYuv)
			{
				m_YuvVector.erase(It);
				break;
			}
		}

		It= m_YuvVector.begin();
		for(;It!= m_YuvVector.end(); It++)
		{
			if((*It)==DestYuv)
			{
				m_YuvVector.insert(It + pre,srcYuv);
				break;
			}
		}
	  

	}
	int      CUIYUVGroup::SetVisible(int visible)
	{
		/*std::vector<CUIYUV*>::iterator It= m_YuvVector.begin();
		for(;It!= m_YuvVector.end(); It++)
		{
			(*It)->SetVisible(visible);
		}*/
	    m_nVisible = visible;
		return m_nVisible;
	}

	int            CUIYUVGroup::GetVisible()
	{
		return m_nVisible;
	}
	void          CUIYUVGroup::AddYuv( CUIYUV* yuv)
	{
		yuv->SetVisible(m_nVisible);
		m_YuvVector.push_back(yuv);
	}
	void          CUIYUVGroup::InsertYuv(CUIYUV* yuv,int indexId)
	{
	
	
		if ( m_YuvVector.size() <=  indexId)
		{
			m_YuvVector.push_back(yuv);
			
		}
		else
		{
			std::vector<CUIYUV*>::iterator It= m_YuvVector.begin();
			for (int i = 0; i < m_YuvVector.size(); It++, i++)
			{
				if (i == indexId) {
					m_YuvVector.push_back(yuv);
					break;
				}
					
			}
		}
		

	}
	void          CUIYUVGroup::DelYuv( CUIYUV* yuv)
	{
		int ret = 0;
	    std::vector<CUIYUV*>::iterator It= m_YuvVector.begin();
		for(;It!= m_YuvVector.end(); It++)
		{
			CUIYUV* Item = *It;
			if(Item==yuv)
			{
				Item->SendMessage(UI_DESTROY);
				m_YuvVector.erase(It);
				m_pMsView->DestroyChild(Item);
				ret  =1;
				break;
			}
		}
	    if(ret)
			m_pMsView->Invalidate();
	}



	CUIWindow*     CUIYUVGroup::WndFromPoint( kkPoint  ptHitTest, int bOnlyText)
	{
		if(!m_nVisible )
			return 0;
	    std::vector<CUIYUV*>::iterator It= m_YuvVector.begin();
		for(;It!= m_YuvVector.end(); It++)
		{
			CUIWindow* pChild=*It;
			if (pChild->IsVisible(0) && !pChild->IsMsgTransparent())
			{
				CUIWindow*  Item =pChild->WndFromPoint(ptHitTest,bOnlyText);
				if(Item)
					return Item;
			}
		}
		return 0;
	}

	
}