﻿#ifndef UIYUVGroup_H_
#define UIYUVGroup_H_
#include "UIWindow.h"
#include "UIYUV.h"
#include "UIMsView.h"
#include <string>
#include <vector>
namespace KKUI
{
	//YUVCTL 分组
    class CUIYUVGroup
    {
		
			public:
						CUIYUVGroup(CUIMsView* msview);
						virtual      ~ CUIYUVGroup();
						CUIYUVGroup*   GetPrevGroup();
						CUIYUVGroup*   GetNextGroup();
						void           SetPrevNextGroup(CUIYUVGroup* group,bool Prev=false);
						int            SetVisible(int visible);
						int            GetVisible();
						void           AddYuv( CUIYUV* yuv);
						void           DelYuv( CUIYUV* yuv);
						void           InsertYuv(CUIYUV* yuv,int indexId);
                        void           SwopYuv(CUIYUV* srcYuv,CUIYUV* DestYuv,int pre);
						std::vector<CUIYUV*>  GetYuvVector(){return m_YuvVector;}
                        CUIWindow*     WndFromPoint( kkPoint  ptHitTest, int bOnlyText);
			protected:
						CUIMsView*            m_pMsView;
						std::vector<CUIYUV*>  m_YuvVector;
                        int                   m_nVisible;
						CUIYUVGroup*          m_pPrevSibling;
		                CUIYUVGroup*          m_pNextSibling;
    };
}
#endif