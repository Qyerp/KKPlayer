package com.ic70.kkplayer.sdk;

import java.lang.reflect.Array;
import java.util.Arrays;

/*****
 * 用于存储播放器信息势力信息
 */
public class CKKPlayerCtx
{

    public byte[] PlayerIns = new byte[16];
    public int    PlayerId=0;
    public boolean equals(Object that)
    {
        if(this == that)
            return true;
        if(that == null)
            return false;	//能调用这个方法，this肯定不为null，所以不判断this
        if(this.getClass() != that.getClass())
            return false; //如果不死同一个类，则必然false
        CKKPlayerCtx That = (CKKPlayerCtx)that;

        if(Arrays.equals(PlayerIns,That.PlayerIns))
          return true;
        return  false;
    }
}