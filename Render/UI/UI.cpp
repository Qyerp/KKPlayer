#include "Core/IUI.h"
#include "Core/UIWindow.h"
#include "Core/UIApp.h"
#include "Core/UIContainerImpl.h"
#include "Core/UIMoveState.h"
#include "Core/UIYUV.h"
#include "Core/UIMsView.h"
#include "Core/UISliceEditor.h"
#include "Core/UIStageEditor.h"
#include "Core/UIYUVGridPanel.h"
#include "Core/UIFusionzone.h"
#include "Core/UICurve.h"
#include "Core/UIYUVGroup.h"
//#include "GnText/GnTextMgr.h"
KKUI::CUIApp*  G_pUIApp=0;



extern "C"
{

	void __declspec(dllexport) UI_Ini(KKUI::IRenderFactory* rf)
	{
		if(G_pUIApp==0){
			G_pUIApp= new KKUI::CUIApp();
			G_pUIApp->SetRenderFactory(rf);
			//CGnTextMgr::GetIns();
		}
	}

	int __declspec(dllexport) UI_LoadXmlSkin(char *xmlSkin)
	{
		if(G_pUIApp==0 || !xmlSkin){
			return 0;
		}
		return G_pUIApp->LoadXmlSkin(xmlSkin);
	}

	///创建一个UI控件
	kkhandle __declspec(dllexport) UI_CreateCtl(char* uiclassname)
	{
		 return KKUI::CUIApp::getSingleton().CreateUIByName(uiclassname);
	}

	void      __declspec(dllexport) UI_InsertChild(KKUIHandle root,KKUIHandle ctl)
	{
	          KKUI::CUIWindow*  uiroot = static_cast<KKUI::CUIWindow*>(root);
			  KKUI::CUIWindow*  uictl = static_cast<KKUI::CUIWindow*>(ctl);
			  uiroot->InsertChild(uictl);
	}
    bool     __declspec(dllexport) UI_RemoveChild(KKUIHandle root,KKUIHandle ctl)
	{
	          KKUI::CUIWindow*  uiroot = static_cast<KKUI::CUIWindow*>(root);
			  KKUI::CUIWindow*  uictl = static_cast<KKUI::CUIWindow*>(ctl);
			  return	uiroot->RemoveChild(uictl);
	}

	//获取控件属性
    int      __declspec(dllexport)   UI_GetCtlAttr(KKUIHandle handle,char* AttrName ,char* OutAttr,int Attrlen)
    {
	      if(OutAttr == 0 || Attrlen == 0 || handle == 0 )
			  return 0;
		   KKUI::CUIWindow*  ui = (KKUI::CUIWindow*)(handle);
		   KKUI::SStringA strOutAttr="";
		   int ret=ui->GetCtlAttr(AttrName,strOutAttr);
		   if(ret){
		      strcpy(OutAttr,strOutAttr.GetBuffer(64));
		   }
		   return ret;
	}

	void __declspec(dllexport) UI_SetVisible(KKUIHandle handle,int visible)
	{
	   KKUI::CUIWindow*  ui = static_cast<KKUI::CUIWindow*>(handle);
	   ui->SetVisible( visible);
	   ui->Invalidate();
	}

	void __declspec(dllexport) UI_SetUIMessage (KKUIContainer handle ,KKUI::IUIMessage* call)
	{
	      KKUI::CUIContainerImpl* Container = static_cast<KKUI::CUIContainerImpl*>(handle);

		  Container->SetMessage(call);
	}
	
    void  __declspec(dllexport)  UI_SetIRenderFactory(KKUIContainer handle,KKUI::IRenderFactory* pRenderFactory)
	{
			  KKUI::CUIContainerImpl* ui= static_cast<KKUI::CUIContainerImpl*>(handle);
			  if(ui){
				  ui-> SetIRenderFactory(pRenderFactory);
			  }	  
	}

   void  __declspec(dllexport)   UI_Opain(KKUIContainer handle,KKUI::IRenderTarget* Render)
   {
	  KKUI::CUIContainerImpl* ui= static_cast<KKUI::CUIContainerImpl*>(handle);
	  if(ui){
		  ui->OnPrint(Render);
	  }
   }

   //获取与舞台相交的控件
   int   __declspec(dllexport)   UI_GetStageIntersectCtls(KKUIHandle handle,unsigned char **Ctls,int Len)
   {
           KKUI:: CUIMoveState* ui= static_cast<KKUI::CUIMoveState*>(handle);
           if(ui){
			   std::vector<KKUI::CUIWindow*> CtlsVec = ui->GetIntersectCtlVec();
			   if(CtlsVec.size()>Len)
				   return 0;
			   for(int i=0;i<CtlsVec.size();i++)
			   {
			      unsigned char **Ctl=Ctls+i;
				  *Ctl = ( unsigned char *)CtlsVec .at(i);
			   }
			   return CtlsVec.size();
		   }
           return 0;
   }
  
   //添加融合控件
   void   __declspec(dllexport)   UI_StageAddFusionzone (KKUIHandle handle,KKUIHandle Fusionzone)
   {
           KKUI::CUIMoveState*  ui      = static_cast<KKUI::CUIMoveState*>(handle);
		   KKUI::CUIFusionzone* fFusion = static_cast<KKUI::CUIFusionzone*>(Fusionzone);
           if(ui){
		      ui->AddFusionzone(fFusion);
		   }
   }
   void   __declspec(dllexport)   UI_StageRemoveFusionzone (KKUIHandle handle,KKUIHandle Fusionzone)
   {
           KKUI::CUIMoveState* ui= static_cast<KKUI::CUIMoveState*>(handle);
		   KKUI::CUIFusionzone* fFusion = static_cast<KKUI::CUIFusionzone*>(Fusionzone);
           if(ui){
		     ui->RemoveFusionzone(fFusion );
		   }
   }
   int   __declspec(dllexport)   UI_FusionzoneInitRegion(KKUIHandle handle,SUVInfo* infos,int count)
   {
             KKUI::CUIFusionzone* fFusion = static_cast<KKUI::CUIFusionzone*>(handle);

			 if(fFusion)
			  return  fFusion->InitRegion(infos,count);
			 
			 return 0;
   }

   void    __declspec(dllexport)   UI_FusionzoneGetScale   (KKUIHandle handle,SUVInfo& fUv1,SUVInfo& fUv2)
   {
             KKUI::CUIFusionzone* fFusion = static_cast<KKUI::CUIFusionzone*>(handle);
			 if(fFusion)
			  fFusion->GetScale(fUv1,fUv2);
   }
   void    __declspec(dllexport)   UI_FusionzoneSetScale   (KKUIHandle handle,SUVInfo& fUv1,SUVInfo& fUv2)
   {
             KKUI::CUIFusionzone* fFusion = static_cast<KKUI::CUIFusionzone*>(handle);
			 if(fFusion)
			  fFusion->SetScale(fUv1,fUv2);
   }

   void    __declspec(dllexport) UI_CurveGetScale       (KKUIHandle handle,SUVInfo& fUv1,SUVInfo& fUv2)
   {
             KKUI::CUICurve* fCurve = static_cast<KKUI::CUICurve*>(handle);
			 if(fCurve)
			    return  fCurve->GetScale(fUv1,fUv2);
   }
   void    __declspec(dllexport) UI_CurveSetScale        (KKUIHandle handle,SUVInfo& fUv1,SUVInfo& fUv2)
   {
             KKUI::CUICurve* fCurve = static_cast<KKUI::CUICurve*>(handle);
			 if(fCurve)
			  return  fCurve->SetScale(fUv1,fUv2);
   }

   void   __declspec(dllexport)   UI_StageOpain(KKUIHandle handle,KKUI::IRenderTarget* ren)
   {
           KKUI:: CUIMoveState* ui= static_cast<KKUI::CUIMoveState*>(handle);
           if(ui){
		     ui->OnStatgePaint(ren);
		   }
   }
   void   __declspec(dllexport)   *UI_StageGetIWarp(KKUIHandle handle,int* enable)
   {
	     *enable=0;
         KKUI:: CUIMoveState* ui= static_cast<KKUI::CUIMoveState*>(handle);
           if(ui){
			   if(ui->GetEnbleIWarp())
			   {
				  *enable=1;
		          return ui-> GetIWarp();
			   }
		   }
		  return 0;
   }
   void             __declspec(dllexport) UI_StageSetWarping              (KKUIHandle handle,int Warping)
   {
           KKUI:: CUIMoveState* ui=  static_cast<KKUI::CUIMoveState*>(handle);
		   if(ui){
	       ui->SetEnbleIWarp(Warping);
		   if(Warping==0)
               ui->Invalidate();
		   }
   }

   int               __declspec(dllexport) UI_StageGetWarpInfos(KKUIHandle handle,SUVInfo* infos,int count)
   {
                      KKUI:: CUIMoveState* ui=  static_cast<KKUI::CUIMoveState*>(handle);
					  if(!ui)
						 return 0;
					  return ui->GetWarpInfos(infos,count);
   }
   int              __declspec(dllexport) UI_StageSetWarpInfos(KKUIHandle handle,SUVInfo* infos,int count)
   {
                     KKUI:: CUIMoveState* ui=  static_cast<KKUI::CUIMoveState*>(handle);
					  if(!ui)
						 return 0;
					  return ui->SetWarpInfos(infos,count);
   }


   void            __declspec(dllexport) UI_StageSetXYDiv          (KKUIHandle handle,int x,int y)
   {
				   KKUI:: CUIMoveState* ui=  static_cast<KKUI::CUIMoveState*>(handle);
				   if(ui){
				   ui->SetXYDiv(x,y);
				   ui->Invalidate();
				   }
   }
   void            __declspec(dllexport) UI_StageGetXYDiv          (KKUIHandle handle,int *Outx,int *Outy)
   {
				   KKUI:: CUIMoveState* ui=  static_cast<KKUI::CUIMoveState*>(handle);
				   if(ui)
				   ui->GetXYDiv(*Outx,*Outy);
   }

   int          __declspec(dllexport) UI_StageEditorSetStageRef (KKUIHandle StageEditor,KKUIHandle StageClt)
   {
				 KKUI::CUIStageEditor* pStageEditor =  static_cast<KKUI::CUIStageEditor*>(StageEditor);
				 KKUI::CUIMoveState*   pState       =  static_cast<KKUI::CUIMoveState*>(StageClt);
				 if(pStageEditor&&pState ){
					 pStageEditor->SetStage( pState );
					 return 1;
				 }
				 return  0;
   }
  
   

   void            __declspec(dllexport)   UI_Relayout                (KKUIContainer handle,int x,int y,int cx,int cy)
   {
	    if (cx==0 || cy==0)
            return;

       KKUI::CUIContainerImpl* ui=  static_cast<  KKUI::CUIContainerImpl*>(handle);
	   if(ui){
		   KKUI::CRect rt;
		   rt.left =x;
		   rt.top=y;
		   rt.bottom=y+cy;
		   rt.right=x+cx;
		   ui->OnRelayout(rt);
	   }
   }
   int              __declspec(dllexport)  UI_FrameEvent              (KKUIContainer handle,unsigned int uMsg,unsigned int wParam,long lParam)
   {
       KKUI::CUIContainerImpl* ui=  static_cast<KKUI::CUIContainerImpl*>(handle);
	   if(ui)
	   return ui->DoFrameEvent(uMsg,wParam,lParam);
	   return 0;
   }

   KKUIHandle       __declspec(dllexport) UI_FindCtlName             (KKUIHandle handle,const char* strValue, int bLoading)
   {
	     KKUI::CUIWindow*  ui =  static_cast<KKUI::CUIWindow*>(handle);
		 if(ui)
		    return ui->FindChildByName(strValue,bLoading);
		 return 0;
   }

   //获取控件名称
   int      __declspec(dllexport)  UI_GetCtlName                    (KKUIHandle handle,char* OutName, int NameLen)
   {
          KKUI::CUIWindow*  ui =  static_cast<KKUI::CUIWindow*>(handle);  
		  if(!ui)
			  return -1;
		  int len = ui->GetName().GetLength();
          if( len> NameLen)
			  return -1;
		  strcpy(OutName,ui->GetName().GetBuffer(10));
		  return 1;
   }

   //获取控件window大小
   void      __declspec(dllexport)  UI_GetCtlWindowRect              (KKUIHandle handle,kkRect* OutRect)
   {
		   if(!OutRect)
			   return ;

           KKUI::CUIWindow*  ui =  static_cast<KKUI::CUIWindow*>(handle);  
		   if(ui)
		   ui->GetWindowRect(OutRect);
   }

   //设置控件属性
   void      __declspec(dllexport)  UI_SetCtlAttr                    (KKUIHandle handle,const char* attr,const char* attrvalue)
   {
            KKUI::CUIWindow*  ui =  static_cast<KKUI::CUIWindow*>(handle);  
			if(ui)
		    ui->SetAttribute(attr,attrvalue);
   }

   //获取用户数据
   void       __declspec(dllexport) *UI_GetCtlUserData     (KKUIHandle handle)
   {
         KKUI::CUIWindow*  ui =  static_cast<KKUI::CUIWindow*>(handle);  
		 if(ui)
		 return ui->GetUserData();
		 return 0;
   }
   //设置用户数据
   void        __declspec(dllexport) UI_SetCtlUserData     (KKUIHandle handle,void* UserData)
   {
          KKUI::CUIWindow*  ui =  static_cast<KKUI::CUIWindow*>(handle);  
		  if(ui)
		  ui->SetUserData(UserData);
   }


   //窗口销毁调用
   void              __declspec(dllexport) UI_OnDestroy                (KKUIContainer handle)
   {
            KKUI::CUIContainerImpl* ui=  static_cast<KKUI::CUIContainerImpl*>(handle);
			if(ui){
				ui->SendMessage(UI_DESTROY);
				delete ui;
			}
   }

   KKUIHandle        __declspec(dllexport) UI_CreateChildren          (KKUIHandle handle,char* strxml)
   {
				   KKUI::CUIWindow* ui= static_cast<KKUI::CUIWindow*>(handle);
				   if(ui)
				   { 
					   KKUIHandle h = ui->CreateChildren(strxml);
					   ui->UpdateLayout();
					   return h;
				   }
				   
				   return 0;
   }
   //移动控件位置
   int             __declspec(dllexport)  UI_MoveChild                (KKUIHandle handle,KKUIHandle  pChild,KKUIHandle pMoveAfter)
   {
                   KKUI::CUIWindow* ui          =  static_cast< KKUI::CUIWindow*>(handle);
				   KKUI::CUIWindow* UIChild     =  static_cast< KKUI::CUIWindow*>(pChild);
				   KKUI::CUIWindow* UIMoveAfter =  static_cast< KKUI::CUIWindow*>(pMoveAfter);
                   if(!ui   || !UIChild ||  ! UIMoveAfter )
					   return 0;

				   return ui->MoveChild(UIChild,UIMoveAfter);
   }

   int                __declspec(dllexport) UI_DeleteCtl              (KKUIHandle RootHandle,KKUIHandle Handle)
   {
                    KKUI::CUIWindow*  ui = static_cast<KKUI::CUIWindow*>(RootHandle);  
					KKUI::CUIWindow*  ll = static_cast<KKUI::CUIWindow*>(Handle);  

					if(!ui || !ll)
						return 0;
					if(!ui->DestroyChild(ll))
						return 0;
					ui->Invalidate();
					return 1;
   }
   int                __declspec(dllexport) UI_YUVLoad                (KKUIYUVHandle handle,kkPicInfo *info)
   { 
                      KKUI::CUIYUV*  ui = static_cast<KKUI::CUIYUV*>(handle);
					  if(ui)
					  ui-> LoadYUV(info);
					  return 0;
   }
   void              __declspec(dllexport) UI_YUVSavePreFrame         (KKUIYUVHandle handle,bool Save)
   {
                      KKUI::CUIYUV*  ui = static_cast<KKUI::CUIYUV*>(handle);
					  if(ui)
					  ui->SavePreFrame(Save);
					  return;
   }
   void             __declspec(dllexport)  UI_YuvCtlGetPicClip        (KKUIHandle handle,SUVInfo* pPts,int ptCount)
   {
                      KKUI::CUIYUV*  ui = static_cast<KKUI::CUIYUV*>(handle);
					  if(ui)
					  ui->GetPicClip(pPts,ptCount);
					  return;
   }

   void           __declspec(dllexport)  UI_YuvCtlCreatePicCache(KKUIHandle handle,int width,int height,int format)
   {
	                  KKUI::CUIYUV*  ui = static_cast<KKUI::CUIYUV*>(handle);
					  if(ui)
					  ui->CreatePicCache(width,height,format);
					  return;
	
   }
   void            __declspec(dllexport)  UI_YuvCtlSetPicClip        (KKUIHandle handle,SUVInfo* pPts,int ptCount)
   {
	                  KKUI::CUIYUV*  ui = static_cast<KKUI::CUIYUV*>(handle);
					  if(ui)
					  ui->SetPicClip(pPts,ptCount);
					  return;
	}


   int               __declspec(dllexport) UI_YUVGetMaskCount(KKUIYUVHandle handle)
   {
                      KKUI::CUIYUV*  ui = static_cast<KKUI::CUIYUV*>(handle);
					  if(ui)						 
					      return  ui->GetMaskCount();
					  return 0;
   }

   /**************添加滤镜****************/
   int                __declspec(dllexport) UI_YUVAddFilter(KKUIYUVHandle handle,SinkFilter* filter)
   {
                      KKUI::CUIYUV*  ui = static_cast<KKUI::CUIYUV*>(handle);
					  if(ui)						 
						  return  ui->AddFilter(*filter);
					  return 0;
   }
   SinkFilter       __declspec(dllexport) *UI_YUVGetFristFilter(KKUIYUVHandle handle)
   {
   
	                  KKUI::CUIYUV*  ui = static_cast<KKUI::CUIYUV*>(handle);
					  if(ui)						 
						  return  ui->GetFristFilter();
					  return 0;
   }

   //删除滤镜
   void              __declspec(dllexport) UI_YUVDelFilter(KKUIYUVHandle handle,int Id)
   {
                      KKUI::CUIYUV*  ui = static_cast<KKUI::CUIYUV*>(handle);
					  if(ui)						 
						  return  ui->DelFilter(Id);
   
   }
   int               __declspec(dllexport) UI_YUVGetMaskInfo(KKUIYUVHandle handle,int id, SUVInfo* pPts,int ptCount,int* HidePixel)
   {
                      KKUI::CUIYUV*  ui = static_cast<KKUI::CUIYUV*>(handle);
					  if(ui)
					     return ui->GetMaskInfo(id,pPts,ptCount,HidePixel);
					  return 0;
   }
   int              __declspec(dllexport) UI_YUVSetMaskInfo(KKUIYUVHandle handle,int id, SUVInfo* pPts,int ptCount)
   {
	                  KKUI::CUIYUV*  ui = static_cast<KKUI::CUIYUV*>(handle);
					  if(ui)	
					     return ui->SetMaskInfo(id,pPts,ptCount); 
					  return 0;
   } 
   void              __declspec(dllexport)  UI_YUVSetIsRect(KKUIYUVHandle handle,int IsRect)
   {
                      KKUI::CUIYUV*  ui = static_cast<KKUI::CUIYUV*>(handle);
					  if(ui)	
					     return ui->SetIsRect(IsRect);
   }
   int               __declspec(dllexport)  UI_YUVGetIsRect(KKUIYUVHandle handle)
   {
                      KKUI::CUIYUV*  ui = static_cast<KKUI::CUIYUV*>(handle);
					  if(ui)	
					     return ui->GetIsRect();
					  return 1;
   }

   void             __declspec(dllexport) UI_YUVSetFRegion (KKUIYUVHandle handle,SUVInfo* pPts,int ptCount)
   {
                      KKUI::CUIYUV*  ui = static_cast<KKUI::CUIYUV*>(handle);
					  if(ui)	
					  {
						ui->SetFRegion(pPts,ptCount);
						  
					  }
   }
   int              __declspec(dllexport) UI_YUVGetFRegion    (KKUIYUVHandle handle,SUVInfo* pPts,int ptCount)
   {
                      KKUI::CUIYUV*  ui = static_cast<KKUI::CUIYUV*>(handle);
					  if(ui)	
					  {
						 
						  int count= ui->GetFRegion(pPts,ptCount);
						   return count;
					  }
					  return 0;
   }
 

    int           __declspec(dllexport) UI_GridPanelLoadYUV(void* handle,int row,int col,kkPicInfo *info,wchar_t* strTxt)
	{
	                 KKUI::CUIYUVGridPanel* panel = static_cast< KKUI::CUIYUVGridPanel*>(handle);
					if(panel)	
					   return panel->LoadYUV(row,col,info,strTxt);
					return 0;
	}
	int            __declspec(dllexport) UI_GridPanelSetItemTxt(void* handle,int row,int col,wchar_t* strTxt)
	{
	                KKUI::CUIYUVGridPanel* panel = static_cast<KKUI::CUIYUVGridPanel*>(handle);
					if(!panel ||!strTxt)
						return 0;
					return panel->SetItemTxt(row,col,strTxt);
	}

	int           __declspec(dllexport) UI_GridPanelSetItemUserData (void* handle,int row,int col,void* userData)
	{
	               KKUI::CUIYUVGridPanel* panel = static_cast<KKUI::CUIYUVGridPanel*>(handle);
				   if(panel )	
					return panel->SetItemUserData(row,col,userData);
				   return 0;
	}
	int          __declspec(dllexport) UI_GridPanelSetRowCol         (void* handle,int nRow,int nCol)
	{
	                KKUI::CUIYUVGridPanel* panel = static_cast<KKUI::CUIYUVGridPanel*>(handle);
					if(panel )
					   return panel->SetRowCol(nRow,nCol);	
					return 0;
	}
	int          __declspec(dllexport) UI_GridPanelItemClearData         (void* handle,int nRow,int nCol,unsigned int cr)
	{
	                KKUI::CUIYUVGridPanel* panel = static_cast<KKUI::CUIYUVGridPanel*>(handle);
					if(panel )
					{
						panel->ClearItemData( nRow,nCol,cr);
						return 1;
					}
					return 0;
	}

    int          __declspec(dllexport) UI_GridPanelClearData         (void* handle,unsigned int cr)
	{
	                KKUI::CUIYUVGridPanel* panel =static_cast<KKUI::CUIYUVGridPanel*>(handle);
					if(panel )
					{	
					    panel->ClearData(cr);
					    return 1;
					}
					return 0;
	}
    void        __declspec(dllexport) *UI_GetYUVGridPanelItem          (void* handle,int row,int col)
	{
	                KKUI::CUIYUVGridPanel* panel = static_cast<KKUI::CUIYUVGridPanel*>(handle);
					if(panel )
					return panel->GetPanelItem(row,col);
					return 0;
	}



   //设置焦点控件
   void              __declspec(dllexport) UI_SetCapture         (KKUIContainer handle,KKUIHandle clt)
   {
                  KKUI::CUIWindow*  uictl = static_cast<KKUI::CUIWindow*>(clt);  
				  KKUI::CUIContainerImpl* ui= static_cast<KKUI::CUIContainerImpl*>(handle);
				  if(uictl&&ui)
				  ui->OnSetUIWndCapture(uictl);
   }

   //激活舞台控件
   void             __declspec(dllexport) UI_ActiveYUVCtl         (KKUIHandle clt)
   {
                  KKUI::CUIYUV*  yuvctl    = static_cast<KKUI::CUIYUV*>(clt);  
				  if(yuvctl)
				  yuvctl->Active();
   }

   //MsView 操作
   float            __declspec(dllexport)   UI_GetMsViewDisplayScale         (KKUIHandle  MsCtl)
   {
   				  KKUI::CUIMsView* MsView = static_cast<KKUI::CUIMsView*>(MsCtl);
				  if(MsView)
				  return MsView->GetDisplayScale();
				  return 0.0;
   }
   void           __declspec(dllexport)     UI_SetMsViewDisplayScale         (KKUIHandle  MsCtl,float Scale)
   {
                  KKUI::CUIMsView* MsView = static_cast<KKUI::CUIMsView*>(MsCtl);
				  if(MsView)
				  MsView-> SetDisplayScale(Scale);
   }

   //创建组
   void          __declspec(dllexport) *UI_CreateYuvGroupMsView         (KKUIHandle  MsCtl)
   {
                  KKUI::CUIMsView* MsView = static_cast<KKUI::CUIMsView*>(MsCtl);
				  if(MsView)
				  return MsView->CreateYuvGroup();
				  return 0;
   }
   //删除组 
   int           __declspec(dllexport) UI_DelYuvGroupMsView            (KKUIHandle  MsCtl,void* group)
   {
                  KKUI::CUIMsView* MsView = static_cast<KKUI::CUIMsView*>(MsCtl);
				  if(MsView)
				  return MsView->DelYuvGroup((KKUI::CUIYUVGroup*)group);
				  return 0;
   }
   //设置Group是否可见
   int          __declspec(dllexport) UI_SetMsViewGroupVisible         (KKUIHandle  MsCtl,void* Group, int visble)
   {
	             KKUI::CUIMsView* MsView = static_cast<KKUI::CUIMsView*>(MsCtl);
				 if(MsView)
                 return  MsView->SetGroupVisible(Group, visble);
				 return 0;
   }
   //交换两个控件的位置
   void         __declspec(dllexport) UI_MsViewGroupSwopYuv           (KKUIHandle  MsCtl,void* Group,KKUIHandle srcYuv,KKUIHandle DestYuv,int pre)
   {
                 KKUI::CUIMsView* MsView = static_cast<KKUI::CUIMsView*>(MsCtl);
				 KKUI::CUIYUV*  yuvctl1     = static_cast<KKUI::CUIYUV*>(srcYuv); 
				 KKUI::CUIYUV*  yuvctl2    = static_cast<KKUI::CUIYUV*>(DestYuv); 
				 KKUI::CUIYUVGroup* pGroup = (KKUI::CUIYUVGroup*)Group;
				 if(MsView&&pGroup && yuvctl1&& yuvctl2)
                    MsView-> GroupSwopYuv(pGroup, yuvctl1, yuvctl2,pre);
   }
   void         __declspec(dllexport) UI_MsViewGroupAddYuv           (KKUIHandle  MsCtl,void* Group,KKUIHandle yuv)
   {
                 KKUI::CUIMsView* MsView = static_cast<KKUI::CUIMsView*>(MsCtl);
				 KKUI::CUIYUV*  yuvctl     = static_cast<KKUI::CUIYUV*>(yuv); 
				    if(MsView&&yuvctl)
                 MsView->AddYuvByGroup(Group, yuvctl);
   }

   void         __declspec(dllexport)  UI_MsViewGroupDelYuv           (KKUIHandle  MsCtl,void* Group,KKUIHandle yuv)
   {
                 KKUI::CUIMsView* MsView   = static_cast<KKUI::CUIMsView*>(MsCtl);
				 KKUI::CUIYUV*  yuvctl     = static_cast<KKUI::CUIYUV*>   (yuv); 
	             if(MsView&&yuvctl)
                    MsView->DelYuvByGroup(Group, yuvctl);
   }
   void         __declspec(dllexport)  UI_MsViewDelGroup              (KKUIHandle  MsCtl,void* Group)
   {
               KKUI::CUIMsView* MsView     = static_cast<KKUI::CUIMsView*>(MsCtl);
               KKUI::CUIYUVGroup* YuvGroup = static_cast<KKUI::CUIYUVGroup*>(Group);
			   if(MsView&&YuvGroup)
			   MsView->DelYuvGroup(YuvGroup);
   }
   void         __declspec(dllexport)  UI_MsViewInsertYuvByGroup (KKUIHandle  MsCtl,void* Group,KKUIHandle yuv,int indexId)
   {
                 KKUI::CUIMsView* MsView   = static_cast<KKUI::CUIMsView*>(MsCtl);
				 KKUI::CUIYUV*  yuvctl     = static_cast<KKUI::CUIYUV*>   (yuv); 
	             if(MsView&&yuvctl)
                    MsView->InsertYuvByGroup(Group, yuvctl,indexId);
   }


   void         __declspec(dllexport)  UI_MsViewEditModel             (KKUIHandle  MsCtl,int EditModel)
   {
               KKUI::CUIMsView* MsView     = static_cast<KKUI::CUIMsView*>(MsCtl);
			   if(MsView)
               MsView->SetEditorModel(EditModel);
   }
   void        __declspec(dllexport)   UI_SrocllViewGetOffXY           (KKUIHandle  MsCtl,int* x,int* y,int* WinWidth,int* WinHeight)
   {
	           KKUI::CUIScrollView* pScrollView = static_cast<KKUI::CUIScrollView*>(MsCtl);
			   if(pScrollView)
               pScrollView->GetOffXY( *x,*y,*WinWidth,*WinHeight);
   }



   int            __declspec(dllexport)   UI_YUVCtlAddMark        (KKUIHandle clt,int MarkType)
   {
                    KKUI::CUIYUV*  yuvctl    = static_cast<KKUI::CUIYUV*>(clt);  
					if(yuvctl)
					return yuvctl->AddMask( (KKUI::EMaskType)MarkType);
					return 0;
   }
   void           __declspec(dllexport) UI_YUVMaskChangePixel(KKUIHandle clt, int maskId, int HidePixel)
   {
                    KKUI::CUIYUV*  yuvctl    = static_cast<KKUI::CUIYUV*>(clt);  
					if(yuvctl)
					yuvctl->MaskChangePixel(maskId, HidePixel);
   }
   void            __declspec(dllexport)  UI_YUVCtlDelMark       (KKUIHandle clt,int Id)
   {
                    KKUI::CUIYUV*  yuvctl    = static_cast<KKUI::CUIYUV*>(clt);  
					if(yuvctl)
					return yuvctl->DelMask(Id);
   }
   int             __declspec(dllexport)  UI_SliceEditorAddIYuvCtlRef(KKUIHandle SliceEditor,KKUIHandle YUVclt)
   {
               KKUI::CUIWindow*  ui = static_cast<KKUI::CUIWindow*>(SliceEditor);  
			   if(!ui)
				   return 0;
			   if(!ui->IsClass("uisliceeditor")){
			      return 0;
			   }

			   ui = static_cast<KKUI::CUIWindow*>(YUVclt); 
			   if(!ui)
				   return 0;
			   if(!ui->IsClass("uiyuv")){
			      return 0;
			   }
			   KKUI::CUISliceEditor* pSliceEditor=( KKUI::CUISliceEditor* )SliceEditor;
			   KKUI::CUIYUV*         pYUV=(KKUI::CUIYUV*)YUVclt;
               if(!pSliceEditor || !pYUV)
				   return 0;

			   if(pYUV){
                 pSliceEditor->SetUIYUV(pYUV);
				 return 1;
			   }
			   return 0;
   }
   int     __declspec(dllexport) UI_YuvCtlAddIYuvCtlRef           (KKUIHandle YUVclt1,    KKUIHandle YUVclt2)
   {
           KKUI::CUIWindow*  ui = static_cast<KKUI::CUIWindow*>(YUVclt1);  
		   if(!ui)
			   return 0;

		   if(!ui->IsClass("uiyuv")){
		      return 0;
		   }

		   ui = static_cast<KKUI::CUIWindow*>(YUVclt2); 
		    if(!ui)
			   return 0;

		   if(!ui->IsClass("uiyuv")){
		      return 0;
		   }
		   KKUI::CUIYUV*      pYUV1=(KKUI::CUIYUV*)YUVclt1;
		   KKUI::CUIYUV*      pYUV2=(KKUI::CUIYUV*)YUVclt2;
           if(! pYUV1 || !pYUV2)
			   return 0;
		  // KKUI::IPicYuv420p* pI4201 = pYUV1->GetIPicYuv420p();
         //  KKUI::IPicYuv420p* pI420  = pYUV2->GetIPicYuv420p();

		   pYUV1->SetQuoteYuv(pYUV2);

		   return 1;
   }
   
   int             __declspec(dllexport) UI_YuvCtlSetTxt                  (KKUIHandle YuvClt,wchar_t* strTxt)
   {
           KKUI::CUIWindow*  ui = static_cast<KKUI::CUIWindow*>(YuvClt);  
		   if(ui){
			   if(!ui->IsClass("uiyuv")){
				  return 0;
			   }
			   KKUI::CUIYUV*    pYUV1=(KKUI::CUIYUV*) ui;
			   pYUV1->SetTxt(strTxt);
			   return 1;
		   }

		   return 0;
   }
   void          __declspec(dllexport) UI_YuvCtlSetRotate               (KKUIHandle YuvClt,int Rotate)
   {
           KKUI::CUIWindow*  ui = static_cast<KKUI::CUIWindow*>(YuvClt);  
		   if(ui){
			   if(!ui->IsClass("uiyuv")){
				  return;
			   }
			   KKUI::CUIYUV*    pYUV1=(KKUI::CUIYUV*) ui;
			   pYUV1->SetRotate(Rotate);
		   }
   }
   int           __declspec(dllexport) UI_YuvCtlGetRotate              (KKUIHandle YuvClt)
   {
           KKUI::CUIWindow*  ui = static_cast<KKUI::CUIWindow*>(YuvClt);  
		   if(ui){
			   if(!ui->IsClass("uiyuv")){
				  return 0;
			   }
			   KKUI::CUIYUV*    pYUV1=(KKUI::CUIYUV*) ui;
			   return pYUV1->GetRotate();
		   }
		   return 0;
   }
   void          __declspec(dllexport)  UI_YuvCtlClearPicData           (KKUIHandle YuvClt,unsigned int Color)
   {
           KKUI::CUIWindow*  ui = static_cast<KKUI::CUIWindow*>(YuvClt);  
		   if(ui){
			   if(!ui->IsClass("uiyuv")){
				  return;
			   }
			   KKUI::CUIYUV*    pYUV1=(KKUI::CUIYUV*) ui;
			   pYUV1->ClearPicData(Color);
		   }

   }





   void            __declspec(dllexport) UI_Invalidate         (KKUIHandle handle)
   {
					 KKUI::CUIWindow*  ui = (KKUI::CUIWindow*)(handle);  
					 ui->Invalidate();
   }

   int             __declspec(dllexport) UI_AddRef                 (KKUIHandle handle)
   {
					  KKUI::CUIWindow*  ui = static_cast<KKUI::CUIWindow*>(handle);
					  if(ui)
					  return ui->AddRef();
					  return 0;
   }

   int                __declspec(dllexport) UI_Release                (KKUIHandle handle)
   {
					  KKUI::CUIWindow*  ui = static_cast<KKUI::CUIWindow*>(handle);
					  if(ui)
					  return ui->Release();
					  return 0;
   }
   int               __declspec(dllexport) UI_SetGetResCallInfo(KKUI_GetRawBufferSize fp1,KKUI_GetRawBuffer fp2)
   {
	                 G_pUIApp->SetGetResCallInfo(fp1,fp2);
                     return 0;
   }

   int         __declspec(dllexport) UI_InitDefSkin()
   {
                  return G_pUIApp->IniDefSkin();
   }
}