﻿#ifndef ObjectImp_H_
#define ObjectImp_H_
#include "IObject.h"


namespace KKUI
{

	template<class T>
	class TObjectImpl : public T
	{
	public:

		virtual char* GetObjectClass() const
		{
			return 0;
		}

		virtual int GetObjectType() const
		{
			return None;
		}
		virtual int IsClass(char* lpszName) const
		{
		    return 0;
		}

		int InitFromXml(pugi::xml_node xmlNode)
		{
			if (!xmlNode) 
				return 0;
#ifdef _DEBUG
			{
				/*pugi::xml_writer_buff writer;
				xmlNode.print(writer, L"\t", pugi::format_default, pugi::encoding_utf8);
				m_strXml = SStringA(writer.buffer(), writer.size());*/
			}
#endif

			//设置当前对象的属性

			for (pugi::xml_attribute attr = xmlNode.first_attribute(); attr; attr = attr.next_attribute())
			{
				if (T::IsAttributeHandled(attr)) continue;   //忽略已经被预处理的属性
				SetAttribute(attr.name(), attr.value(), 1);
			}
			//调用初始化完成接口
			OnInitFinished(xmlNode);
			return 1;
		}
		virtual int             SetAttribute(const SStringA &  strAttribName, const SStringA &  strValue, int bLoading)
		{
		  return DefAttributeProc(strAttribName, strValue, bLoading);
		}
	    virtual int             AfterAttribute(const SStringA & strAttribName,const SStringA & strValue, int bLoading,int hr)
		{
	
			return hr;
		}
		virtual int             DefAttributeProc(const SStringA & strAttribName,const SStringA & strValue, int bLoading)
		{
		     return -1;
		}
        void OnInitFinished(pugi::xml_node xmlNode) {
			
		}
		#ifdef    _DEBUG
	public:
		SStringA m_strXml;  //<** XML字符串，用于在调试时观察对象*/
#endif//_DEBUG

	};

	//template class SObjectImpl<IObject>;
	typedef TObjectImpl<IObject> CObject;
}

#endif