#include "GetResource.h"
CGetResource::CGetResource(HMODULE hModule, std::wstring Name,  std::wstring Type):m_hDllModule(hModule),m_hRR(0)
{
   m_strName = Name;
   m_strType = Type;
}

CGetResource::~CGetResource()
{
  if(m_hRR){

	   UnlockResource(m_hG);
	   FreeResource(m_hG);
  }
}

void*  CGetResource::GetResourceBuffer(UINT& bufflen)
{
	if(!m_hRR){
        m_hRR = ::FindResource(m_hDllModule,m_strName.c_str(), m_strType.c_str());

	    m_nResSize = ::SizeofResource(m_hDllModule, m_hRR);  // Data size/length  
		m_hG= ::LoadResource(m_hDllModule,  m_hRR);  
		m_pData= (void*)::LockResource(m_hG); 
      }
    bufflen = m_nResSize;
	return m_pData;

}