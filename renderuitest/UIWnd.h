#ifndef msui_UIWnd_H_
#define msui_UIWnd_H_
#include "SimpleWnd.h"
#include "IRender.h"
#include "kkmisc.h"
#include "msgcrack.h"
#include "IUI.h"
#include "IUIMessage.h"
#define _WTYPES_NS KKUI
namespace KKUI
{
	class CUIWnd: public CSimpleWnd ,public IUIMessage
	{
	     public:
                 static void SetRenderFactory(IRenderFactory*  pIRenderFactory );
		 public:
				 CUIWnd(KKUIHandle    pUIRoot);
				 ~CUIWnd();
				
				 HWND Create(HWND hWndParent,int x = 0, int y = 0, int nWidth = 0, int nHeight = 0);
				 HWND Create(HWND hWndParent,DWORD dwStyle,DWORD dwExStyle, int x = 0, int y = 0, int nWidth = 0, int nHeight = 0);
				

				 KKUIHandle  FindCtlName(const char* pszName , int nDeep=-1);
        protected:
			      void          OnSize(UINT nType, CSize size);
				  void          OnPaint(HDC dc); 
				  BOOL          OnEraseBkgnd(HDC dc);
				  LRESULT       OnMouseEvent(UINT uMsg, WPARAM wParam, LPARAM lParam);
				  BOOL          OnSetCursor(HWND hWnd, UINT nHitTest, UINT message);
                  void          OnDestroy();
				  BEGIN_MSG_MAP_EX(CUIWnd)
					
                      MSG_WM_SIZE       (OnSize)
					  MSG_WM_PAINT      (OnPaint)
					  MSG_WM_ERASEBKGND (OnEraseBkgnd)
					  MSG_WM_SETCURSOR(OnSetCursor)
					  MSG_WM_DESTROY         (OnDestroy)
					  MESSAGE_RANGE_HANDLER_EX(WM_MOUSEFIRST, WM_MOUSELAST, OnMouseEvent)
				  END_MSG_MAPX()
	protected:
		      virtual int     UISendMessage(unsigned int Msg, unsigned int wParam, unsigned long lParam);
			  virtual void    UIInvalidateRect(int l,int r,int t,int top);
			  virtual void    UIInvalidate();
			          void    UISetCursor(const char* name);
			  virtual void    OnUIEvent(const UIEvent* ev);
	   protected:
		        KKUIHandle                 m_pUIRoot;
		        static IRenderFactory*     G_pIRenderFactory;   
		        IRenderTarget*             m_pIRenderTarget;
				MSG                        m_msgMouseLast;         /**<上一次鼠标按下消息*/

	};
}
#endif