#ifndef UpDateCheckTask_H_
#define UpDateCheckTask_H_
#include "../ThMgr/kkThread.h"
class UpDateCheckTask:public kkBase::CkkThread
{
  public:
	   UpDateCheckTask(void);
	   ~UpDateCheckTask(void);
protected:
	   void run();
	   void TaskWork();
};
#endif
