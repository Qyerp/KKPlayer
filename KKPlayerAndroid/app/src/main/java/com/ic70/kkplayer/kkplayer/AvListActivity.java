package com.ic70.kkplayer.kkplayer;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.SimpleAdapter;

import com.hjm.bottomtabbar.BottomTabBar;
import com.ic70.kkplayer.sdk.CJniKKPlayer;
import com.ic70.kkplayer.sdk.CKKPlayerCtx;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;

import static android.R.attr.name;

public class AvListActivity extends AppCompatActivity {

    CKKPlayerCtx PlayerCtx= null;
    private CAvListAdapter             m_AvAdapter = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        WindowManager wm1 = this.getWindowManager();

        Point pt = new Point();
        wm1.getDefaultDisplay().getSize(pt);

        CGlobalInfo.SetScreenheight(pt.y);
        CGlobalInfo.SetScreenWidth (pt.x);



        List<CAVListModel> array = new ArrayList<CAVListModel>();
        for(int i=0;i<10;i++)
        {
            CAVListModel listmodel = new CAVListModel();
            listmodel.m_txtTitleRegion="测试数据"+i;
             for(int j=0;j<6;j++) {
                CAVInfoModel itemmodel = new CAVInfoModel();
                itemmodel.m_stravurl="https://vip.okzybo.com/20180319/Fp2fvuXV/index.m3u8";
                itemmodel.m_strpicurl="http://pic.pic-img.com/pic/upload/vod/2018-03/15212982580.jpg";
                itemmodel.m_stravname="av"+i+","+j;
                listmodel.AddAv(itemmodel);
            } /**/
            array.add(listmodel);
        }
        m_AvAdapter = new CAvListAdapter(this,array);


        RecyclerView view =(RecyclerView)findViewById(R.id.AvRecyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        view.setLayoutManager(layoutManager);
        view.setAdapter(m_AvAdapter);

        Ini();

     /*   try {
            Thread.sleep(2000);
        }catch (Exception e ){

        }*/

        //jj.IniKK(0);

        //jj.SetDecoderMethod(PlayerCtx,0);
    }
    protected void Ini(){

        Button btn_LocalPage=(Button) this.findViewById(R.id.btn_LocalPage);
        btn_LocalPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent intent = new Intent();
                intent.setClass(v.getContext(), LocalListActivity.class);//(context.this, Activity02.class);
                v.getContext().startActivity(intent);
            }
        });
    }
}
