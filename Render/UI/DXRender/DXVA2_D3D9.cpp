#include "DXVA2_D3D9.h"
#include "DXVA2_Decoder.h"
#include "RenderTarget_D3D9.h"




/* XXX Prefered format must come first */
static const d3d_format_t d3d_formats[] = {
	{ "YV12",   (D3DFORMAT)MAKEFOURCC('Y','V','1','2'),    AV_PIX_FMT_YUV420P },
	{ "NV12",   (D3DFORMAT)MAKEFOURCC('N','V','1','2'),    AV_PIX_FMT_NV12 },
	{ NULL, (D3DFORMAT)0, AV_PIX_FMT_NONE }
};



namespace KKUI
{

	CDeviceDXVA2Manager9_D3D9::CDeviceDXVA2Manager9_D3D9( SRenderFactory_D3D9*  pRenderFactory):m_pRenderFactory(pRenderFactory),
		m_pDevToken(0),
		m_pDevmgr(0),
		m_pVideoDecodeService(0),
		m_hDevice(0)
	{
	
	    m_pDevmgr    = m_pRenderFactory->CreateDevMgr(m_pDevToken);
		if( m_pDevmgr ){
		
		
			HRESULT hr = m_pDevmgr->OpenDeviceHandle(&m_hDevice);
			if (FAILED(hr)) {
				return ;
			}

			hr = m_pDevmgr->GetVideoService(m_hDevice,IID_IDirectXVideoDecoderService,(void **)&m_pVideoDecodeService);
			if (FAILED(hr)) 
			{
				m_pDevmgr->CloseDeviceHandle(m_hDevice);
				return;
			}
		}
	}
	CDeviceDXVA2Manager9_D3D9::~CDeviceDXVA2Manager9_D3D9()
	{
	
	
	}
	CDXVA2VD_D3D9* CDeviceDXVA2Manager9_D3D9::CreateVideoDecoder(int CodecId,int Width,int Height)
	{
		if(!m_pVideoDecodeService)
			return 0;
		
		
		CDXVA2VD_D3D9* pDXVd  = new CDXVA2VD_D3D9(m_pRenderFactory,this);	
		pDXVd->m_fmt.i_width  = Width;
		pDXVd->m_fmt.i_height = Height;
		pDXVd->m_nCodecId     = CodecId;
		//找到Codec Id,
		if(!DxFindVideoServiceConversion(CodecId,&pDXVd->m_InputGuid,&pDXVd->m_OutputFormat))
		{
			delete pDXVd;
			pDXVd = 0;
		    return 0;
		}

		if(!InitVideoDecoder(pDXVd))
		{
		    delete pDXVd;
			pDXVd = 0;
		    return 0;
		}
		m_VDObjSet.insert(pDXVd);
		return pDXVd;
	}

	int           CDeviceDXVA2Manager9_D3D9::DelVideoDecoder(CDXVA2VD_D3D9* pVD)
	{
		std::set<CDXVA2VD_D3D9*>::iterator It=  m_VDObjSet.find(pVD);
		if(It!= m_VDObjSet.end()){
			m_VDObjSet.erase(It);
			delete pVD;
			return 1;
		}
		return 0;
	}
	static const d3d_format_t *D3dFindFormat(D3DFORMAT format)
	{
		for (unsigned i = 0; d3d_formats[i].name; i++) {
			if (d3d_formats[i].format == format)
				return &d3d_formats[i];
		}
		return NULL;
	}

    int   CDeviceDXVA2Manager9_D3D9::InitVideoDecoder(CDXVA2VD_D3D9* pDXVd)
	{
	   if (!DxCreateVDecoder(pDXVd,pDXVd->m_nCodecId, &pDXVd->m_fmt))
		     return 0;
			
		pDXVd->m_Dxva_Context.decoder = pDXVd->m_pDecoder;
		pDXVd->m_Dxva_Context.cfg     = &pDXVd->m_VideoCfg;
		pDXVd->m_Dxva_Context.surface_count = pDXVd->m_nSurfaceCount;
	    pDXVd->m_Dxva_Context.surface = pDXVd->m_HwSurface;

		for (unsigned i = 0; i < pDXVd->m_nSurfaceCount; i++)
		   pDXVd->m_Dxva_Context.surface[i] =pDXVd->m_SurfaceInfos[i].d3d;

		pDXVd->m_D3dFormat = *D3dFindFormat(pDXVd->m_OutputFormat);
		return 1;
	}
    int   CDeviceDXVA2Manager9_D3D9::DxCreateVDecoder(CDXVA2VD_D3D9* pVideoDecodec,int codec_id, const SVideoFormat *fmt)
	{      
		int nSurfaceCount = 0;
		pVideoDecodec->m_nSurfaceCount = 0;
		IDirect3DDevice9* pDevice=m_pRenderFactory->GetDevice();
	    pVideoDecodec->m_nSurfaceWidth  = (fmt->i_width  + 15) & ~15;
	    pVideoDecodec->m_nSurfaceHeight = (fmt->i_height + 15) & ~15;
		switch (codec_id) 
		{
            default:
				nSurfaceCount = 17;
			break;
		}
	    LPDIRECT3DSURFACE9 surface_list[VA_DXVA2_MAX_SURFACE_COUNT];
		if (FAILED(m_pVideoDecodeService->CreateSurface(
			pVideoDecodec->m_nSurfaceWidth ,
			pVideoDecodec->m_nSurfaceHeight,
			nSurfaceCount - 1,
			pVideoDecodec->m_OutputFormat,
			D3DPOOL_DEFAULT,
			0,
			DXVA2_VideoDecoderRenderTarget,
			surface_list,
			NULL))) 
		{
				//av_log(NULL, AV_LOG_ERROR, "IDirectXVideoAccelerationService_CreateSurface failed\n");
			
				nSurfaceCount = 0;
				return 0;
		}

	
		for (unsigned i = 0; i < nSurfaceCount; i++)
		{
			SSurfaceInfo * surface = &pVideoDecodec->m_SurfaceInfos[i];
			surface->d3d = surface_list[i];
			surface->age = UINT64_MAX;
			surface->index = i;
			surface->used=0;
			surface->usedCount=0;

			// fill the surface in black, to avoid the "green screen" in case the first frame fails to decode.
			pDevice->ColorFill(surface->d3d, NULL, D3DCOLOR_XYUV(0, 128, 128));
		}
	    //av_log(NULL, AV_LOG_DEBUG, "IDirectXVideoAccelerationService_CreateSurface succeed with %d surfaces (%dx%d)\n",va->surface_count, fmt->i_width, fmt->i_height);

		/* */
		DXVA2_VideoDesc dsc;
		ZeroMemory(&dsc, sizeof(dsc));
		dsc.SampleWidth     = fmt->i_width;
		dsc.SampleHeight    = fmt->i_height;
		dsc.Format          = pVideoDecodec->m_OutputFormat;
		if (fmt->i_frame_rate > 0 && fmt->i_frame_rate_base > 0) {
			dsc.InputSampleFreq.Numerator   = fmt->i_frame_rate;
			dsc.InputSampleFreq.Denominator = fmt->i_frame_rate_base;
		} else {
			dsc.InputSampleFreq.Numerator   = 0;
			dsc.InputSampleFreq.Denominator = 0;
		}
		dsc.OutputFrameFreq = dsc.InputSampleFreq;
		dsc.UABProtectionLevel = FALSE;
		dsc.Reserved = 0;

		/* FIXME I am unsure we can let unknown everywhere */
		DXVA2_ExtendedFormat *ext     = &dsc.SampleFormat;
		ext->SampleFormat             = DXVA2_SampleUnknown;
		ext->VideoChromaSubsampling   = DXVA2_VideoChromaSubsampling_Unknown;
		ext->NominalRange             = DXVA2_NominalRange_Unknown;
		ext->VideoTransferMatrix      = DXVA2_VideoTransferMatrix_Unknown;
		ext->VideoLighting            = DXVA2_VideoLighting_Unknown;
		ext->VideoPrimaries           = DXVA2_VideoPrimaries_Unknown;
		ext->VideoTransferFunction    = DXVA2_VideoTransFunc_Unknown;

		/* List all configurations available for the decoder */
		UINT                      cfg_count = 0;
		DXVA2_ConfigPictureDecode *cfg_list = NULL;
		if (
			FAILED(m_pVideoDecodeService->GetDecoderConfigurations(
			pVideoDecodec->m_InputGuid,
			&dsc,	
			NULL,
			&cfg_count,
			&cfg_list))
		)
		{
			    assert(0);
				//av_log(NULL, AV_LOG_ERROR, "IDirectXVideoDecoderService_GetDecoderConfigurations failed\n");
				return 0;
		}
		//av_log(NULL, AV_LOG_DEBUG, "we got %d decoder configurations\n", cfg_count);

		/* Select the best decoder configuration */
		int cfg_score = 0;
		for (unsigned i = 0; i < cfg_count; i++)
		{
			const DXVA2_ConfigPictureDecode *cfg = &cfg_list[i];
			
			//av_log(NULL, AV_LOG_DEBUG, "configuration[%d] ConfigBitstreamRaw %d\n",i, cfg->ConfigBitstreamRaw);

			/* */
			int score;
			if (cfg->ConfigBitstreamRaw == 1)
				score = 1;
			else if (codec_id == AV_CODEC_ID_H264 && cfg->ConfigBitstreamRaw == 2)
				score = 2;
			else
				continue;
			if (IsEqualGUID(cfg->guidConfigBitstreamEncryption, DXVA_NoEncrypt))
				score += 16;

			if (cfg_score < score)
			{
				pVideoDecodec->m_VideoCfg= *cfg;
				cfg_score = score;
			}
		}
		CoTaskMemFree(cfg_list);
		cfg_list = NULL;

		if (cfg_score <= 0)
		{
			//av_log(NULL, AV_LOG_ERROR, "Failed to find a supported decoder configuration\n");
			return 0;
		}

		/* Create the decoder */
		IDirectXVideoDecoder *decoder;
		if (
			FAILED(m_pVideoDecodeService->CreateVideoDecoder(pVideoDecodec->m_InputGuid,
			&dsc,
			&pVideoDecodec->m_VideoCfg,
			surface_list,
			nSurfaceCount,
			&decoder))) 
		{
				//av_log(NULL, AV_LOG_ERROR, "IDirectXVideoDecoderService_CreateVideoDecoder failed\n");
				return 0;
		}
		pVideoDecodec->m_pDecoder      = decoder;
		pVideoDecodec->m_nSurfaceCount = nSurfaceCount;
		//av_log(NULL, AV_LOG_DEBUG, "IDirectXVideoDecoderService_CreateVideoDecoder succeed\n");
		return 1;
	     
	
	}


    void  CDeviceDXVA2Manager9_D3D9::Release2()
	{	
		HRESULT  hr = S_OK;
		std::set<CDXVA2VD_D3D9*>::iterator It=   m_VDObjSet.begin();
		for(;It!=m_VDObjSet.end();++It)
		{
			(*It)->Release2();
		}
	
		if (m_pVideoDecodeService) {
			hr = m_pVideoDecodeService->Release();
			while (hr>S_OK)
			{
				hr = m_pVideoDecodeService->Release();
				if (FAILED(hr)) {
					printf("Error!\n");
				}
			}
			m_pVideoDecodeService = 0;
		}


		if (m_pDevmgr) {
			hr = m_pDevmgr->CloseDeviceHandle(m_hDevice);
			m_pDevmgr = NULL;
		}/**/
	//	HRESULT  hr = va->devmng->CloseDeviceHandle(va->device);
		/*if (va->devmng)
		{
			HRESULT  hr = va->devmng->Release();
			va->devmng = NULL;
		}*/
	}
	void  CDeviceDXVA2Manager9_D3D9::ReLoadRes()
	{


		m_pDevmgr = m_pRenderFactory->CreateDevMgr(m_pDevToken);
		if (m_pDevmgr) {
			HRESULT hr = m_pDevmgr->OpenDeviceHandle(&m_hDevice);
			if (FAILED(hr)) {
				return;
			}
		} /**/
		m_pDevmgr->GetVideoService(m_hDevice, IID_IDirectXVideoDecoderService, (void **)&m_pVideoDecodeService);
	    std::set<CDXVA2VD_D3D9*>::iterator It=   m_VDObjSet.begin();
		for(;It!=m_VDObjSet.end();++It)
		{
			(*It)->ReLoadRes();
		}

		 
	}

	
	static const dxva2_mode_t *Dxva2FindMode(const GUID& guid)
	{
		for (unsigned i = 0; dxva2_modes[i].name; i++) {
			if (IsEqualGUID(dxva2_modes[i].guid, guid))
				return &dxva2_modes[i];
		}
		return NULL;
	}

    /***********
	* Find the best suited decoder mode GUID and render format.
	*************/
	int CDeviceDXVA2Manager9_D3D9::DxFindVideoServiceConversion(int nCodecId, GUID *input, D3DFORMAT *output)
	{
		/* Retrieve supported modes from the decoder service */
		UINT input_count = 0;
		GUID *input_list = NULL;
		HRESULT hr = m_pVideoDecodeService->GetDecoderDeviceGuids(&input_count, &input_list);
		if (FAILED(hr)) {
			//av_log(NULL, AV_LOG_ERROR, "IDirectXVideoDecoderService_GetDecoderDeviceGuids failed\n");
			return 0;
		}
		for (unsigned i = 0; i < input_count; i++) 
		{
			const GUID &g = input_list[i];
			const dxva2_mode_t *mode = Dxva2FindMode(g);
			if (mode) {
				//av_log(NULL, AV_LOG_INFO, "- '%s' is supported by hardware\n", mode->name);
			} else {
				//av_log(NULL, AV_LOG_WARNING, "- Unknown GUID = %08X-%04x-%04x-XXXX\n",(unsigned)g.Data1, g.Data2, g.Data3);
			}
		}

		/* Try all supported mode by our priority */
		for (unsigned i = 0; dxva2_modes[i].name; i++)
		{
			const dxva2_mode_t *mode = &dxva2_modes[i];
			if (!mode->codec || mode->codec != nCodecId)
				continue;

			/* */
			bool is_suported = false;
			for (unsigned count = 0; !is_suported && count < input_count; count++) 
			{
				const GUID &g = input_list[count];
				is_suported = IsEqualGUID(mode->guid, g) == 0;
			}
			if (!is_suported)
				continue;

			/* */
			//av_log(NULL, AV_LOG_DEBUG, "Trying to use '%s' as input\n", mode->name);
			UINT      output_count = 0;
			D3DFORMAT *output_list = NULL;
			if (FAILED(m_pVideoDecodeService->GetDecoderRenderTargets( mode->guid,&output_count,&output_list))) 
			{
					//av_log(NULL, AV_LOG_ERROR, "IDirectXVideoDecoderService_GetDecoderRenderTargets failed\n");
					continue;
			}
			for (unsigned j = 0; j < output_count; j++) 
			{
				const D3DFORMAT f = output_list[j];
				const d3d_format_t *format = D3dFindFormat(f);
				if (format) {
					//av_log(NULL, AV_LOG_DEBUG, "%s is supported for output\n", format->name);
				} else {
					//av_log(NULL, AV_LOG_DEBUG, "%d is supported for output (%4.4s)\n", f, (const char*)&f);
				}
			}

			/* */
			for (unsigned j = 0; d3d_formats[j].name; j++)
			{
				const d3d_format_t *format = &d3d_formats[j];

				/* */
				bool is_suported = false;
				for (unsigned k = 0; !is_suported && k < output_count; k++) 
				{
					is_suported = format->format == output_list[k];
				}
				if (!is_suported)
					continue;

				/* We have our solution */
				//av_log(NULL, AV_LOG_DEBUG, "Using '%s' to decode to '%s'\n", mode->name, format->name);
				*input  = mode->guid;
				*output = format->format;
				CoTaskMemFree(output_list);
				CoTaskMemFree(input_list);
				return 1;
			}
			CoTaskMemFree(output_list);
		}
		CoTaskMemFree(input_list);
		return 0;
	}



	
	
	
	
	/****************解码器*******************/
	CDXVA2VD_D3D9::CDXVA2VD_D3D9(SRenderFactory_D3D9*  pRenderFactory, CDeviceDXVA2Manager9_D3D9* pDxva2Mgr):
	m_pRenderFactory(pRenderFactory),
	m_pDxva2Mgr(pDxva2Mgr),
	m_nSurfaceCount  (0),
	m_nSurfaceOrder  (0),
	m_nSurfaceWidth  (0),
	m_nSurfaceHeight (0),
	m_OutputFormat   (D3DFMT_UNKNOWN ),
	m_pDecoder       (0)
	{
	 	memset(&m_fmt, 0, sizeof(m_fmt));  
		memset(&m_Dxva_Context,0,sizeof(m_Dxva_Context));
        memset(m_SurfaceInfos, 0, sizeof( m_SurfaceInfos));
		memset(&m_VideoCfg,0,sizeof(m_VideoCfg));
		memset(&m_D3dFormat,0,sizeof(SD3dFormat));
		                      
	}
	CDXVA2VD_D3D9:: ~CDXVA2VD_D3D9()
	{
	    Release2();
	}

	void*  CDXVA2VD_D3D9::GetDxvaContext()
	{
	   return &m_Dxva_Context;
	}
	int    CDXVA2VD_D3D9::GetPixFmt()
	{
		return m_D3dFormat.format;
	}
	int    CDXVA2VD_D3D9::GetSurfaceIndex(void *ffFrame)
	{
	        unsigned i=0;
			AVFrame* ff =(AVFrame*)ffFrame;
			int old, old_unused;
			//m_pRenderFactory->Lock();
			if (m_pRenderFactory->LostDevice() || m_nSurfaceCount < 1) {
			//	m_pRenderFactory->UnLock();
				return -1;
			}
				

			for (i = 0, old = 0, old_unused = -1; i < m_nSurfaceCount; i++) 
			{
				SSurfaceInfo*   surface = &m_SurfaceInfos[i];
				if (
					 !surface->used && (old_unused == -1 || surface->age < m_SurfaceInfos[old_unused].age)
					 )
				{
					old_unused = i;
				}
				if (surface->age < m_SurfaceInfos[i].age)
					old = i;
			}
			if (old_unused == -1) {
				//DbgLog((LOG_TRACE, 10, L"No free surface, using oldest"));
				i = old;
			} else {
				i = old_unused;
			}

			SSurfaceInfo* surface = &m_SurfaceInfos[i];

			surface->used = true;
			surface->age = m_nSurfaceOrder++;
			surface->usedCount++;

			
			memset(ff->data, 0, sizeof(ff->data));
			memset(ff->linesize, 0, sizeof(ff->linesize));
			memset(ff->buf, 0, sizeof(ff->buf));

			ff->data[0]=ff->data[3] = (uint8_t *)surface->d3d;
			surface->d3d->AddRef();
			//m_pRenderFactory->UnLock();
			return i;
	}
	int    CDXVA2VD_D3D9::ReleaseSurface(void *Surface,void* Decoder)
	{
		    IDirectXVideoDecoder*  pDecoder =(IDirectXVideoDecoder*) Decoder;
	       	LPDIRECT3DSURFACE9 pSurface = (LPDIRECT3DSURFACE9)Surface;
			//m_pRenderFactory->Lock();
			for (int i = 0; i < m_nSurfaceCount; i++)
			{
				if (m_SurfaceInfos[i].d3d == pSurface) {
					m_SurfaceInfos[i].used = false;
					m_SurfaceInfos[i].usedCount--;
					pSurface->Release();

				    if(m_pDecoder!=0&& pDecoder==m_pDecoder)
		               m_pDecoder->Release();
					//m_pRenderFactory->UnLock();
					return 1;
				}
			}
			//m_pRenderFactory->UnLock();
			return 0;
	}
	int    CDXVA2VD_D3D9::IsSurfaceValid(void *Surface,int SurfaceId)
	{
		 	LPDIRECT3DSURFACE9 pSurface = (LPDIRECT3DSURFACE9)Surface;
	         if(SurfaceId >=m_nSurfaceCount)
				 return 0;
			 if (m_SurfaceInfos[SurfaceId].d3d == pSurface&&pSurface!=0) {
				 return 1;
			 }
			 return 0;
	
	}
    int    CDXVA2VD_D3D9::GetLostDevice()
	{
	        return m_pRenderFactory->LostDevice();
	}
	void* CDXVA2VD_D3D9::GetDecoder()
	{
	      return m_pDecoder;
	}
	void   CDXVA2VD_D3D9::Release2()
	{
		 HRESULT  hr=  S_OK ;
	
		 //释放Surface资源
	     for(unsigned i = 0; i < m_nSurfaceCount; i++)
		 {
             hr= m_SurfaceInfos[i].d3d->Release();		 
			 while(hr>S_OK)
			 {
				hr =  m_SurfaceInfos[i].d3d->Release();
				if (FAILED(hr)){
					printf("Error!\n"); 
				}
			 }
		     m_SurfaceInfos[i].d3d=NULL;
			 m_SurfaceInfos[i].used = 0;
			 m_SurfaceInfos[i].usedCount=0;
			 m_HwSurface[i]=0;
	     }


		if (m_pDecoder){
			 hr = m_pDecoder->Release();
			 while(hr>S_OK)
			 {
				 hr= m_pDecoder->Release();
				 if (FAILED(hr)){
					printf("Error!\n"); 
				 }
			 }
		
			 if (FAILED(hr)){
					printf("Error!\n"); 
			}
			m_pDecoder= NULL;
		}
	    m_nSurfaceCount = 0;
	}
	void  CDXVA2VD_D3D9::ReLoadRes()
	{
		m_pDxva2Mgr->InitVideoDecoder(this);
	}
}