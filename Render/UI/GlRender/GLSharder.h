

static const char* texture_vertex_shader = 
"varying vec4 v_color;\n"                                       \
"varying vec2 v_texCoord;\n"                                    \
"\n"                                                            \
"void main()\n"                                                 \
"{\n"                                                           \
"    gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;\n" \
"    v_color = gl_Color;\n"                                     \
"    v_texCoord = vec2(gl_MultiTexCoord0);\n"                   \
"}";     

static const char* texture_vertex_shader2 = 
"#version 330 core;\n"\
"layout (location = 0) in vec3 aPos;\n"\
"layout (location = 1) in vec3 aColor;\n"\
"layout (location = 2) in vec2 aTexCoord;\n"\
"\n"\
"out vec3 ourColor;\n"\
"out vec2 TexCoord;\n"\
"\n"\
"void main()\n"\
"{\n"\
"	gl_Position = vec4(aPos, 1.0);\n"\
"	ourColor = aColor;\n"\
"	TexCoord = vec2(aTexCoord.x, aTexCoord.y);\n"\
"}";

static const char* fragment_shader = 
"#version 330 core;\n"\
"out vec4 FragColor;\n"\
"\n"\
"in vec3 ourColor;\n"\
"in vec2 TexCoord;\n"\
"\n"\
"uniform sampler2D texture1;\n"\
"\n"\
"void main()\n"\
"{\n"\
"	FragColor = texture(texture1, TexCoord);\n"\
"}";
