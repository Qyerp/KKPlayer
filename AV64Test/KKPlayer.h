#ifndef CKKPlayer_H_
#define CKKPlayer_H_
#include <string>

enum AVPALYERSTATE
{
	UnKnow=0,
	AVOpenFailed=1,    //打开失败
    AVOver=2,          //视频结束
    AVLoadingOver=3,   //视频等待结束
};
///视频图像信息,RAW
typedef struct kkAVPicRAWInfo
{
    unsigned char *data[8];
    int      linesize[8];
	int      picformat;
	int      width;
	int      height;

}kkAVPicRAWInfo;

class IAVPlayerNotify
{
	public:
			virtual void OnPlayerState(void* UserData,AVPALYERSTATE state)=0;
};

typedef  void (*fpRenderImgCall) (kkAVPicRAWInfo* data,void* UserData);


class CKKPlayer
{
public:
	    CKKPlayer();
	    ~CKKPlayer();

		HWND CreateKKPlayer(HWND h,RECT Rt,DWORD style,bool yuv420);
		void CreateDuiKKPlayer(HWND hAudio,fpRenderImgCall fpCall,void *UserData);
		void CreateDuiRawKKPlayer(HWND hAudio,int imgType,fpRenderImgCall fpCall,void *UserData);
public:
	    
		///设置错误回调信息
		void    SetAVPlayerNotify(IAVPlayerNotify* nty);
		//设置用户数据
		void    SetUserData(void* UserData);
		int     OpenMedia(std::string url);
		void    CloseMedia();
        void    SetVlcVolume(int volume,bool tip=true);
		void    RefreshDuiPlayer();
        void    DuiSize(int w,int h);

		void    SetMaxRealtimeDelay(double Delay);
public:
	    static  void init();
	    static  void GetFileMediaInfo(const std::string& path,char** OutJson,int NeePic,int sec,int &NeedWidth,int &NeedHeight,char** OutBGRA,int* BGRAlen);
		static  void KKFree(void* ptr);
public:
	    HWND m_hWnd;
private:
	   static void ErrNotify(void *UserData,int errcode);
	   void*            m_UserData;
	   int              m_nDui;
	   IAVPlayerNotify* m_pNty;
	   void *m_pPlayerh;
	   fpRenderImgCall   m_FnRenderImgCal;
};
#endif