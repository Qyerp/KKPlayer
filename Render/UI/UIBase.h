///这里的UI只是一个简单UI，目的是为了方便视频渲染
#ifndef UIBase_H_
#define UIBase_H_
#include <string>
#include "Core/IRender.h"
#include "UIEvent.h"
#include "ObjRef.h"
#include "Core/ObjectImp.h"
namespace KKUI{

	typedef unsigned int                    UI_WND;

	#define UI_CREATE                       0x0001
	#define UI_DESTROY                      0x0002

	#define UI_ERASEBKGND                   0x0014
	#define UI_PAINT                        0x000F

	#define UI_SIZE                         0x0005
    #define UI_NCPAINT                      0x0085
    #define UI_NCCALCSIZE                   0x0083

	#define UI_TIMER                        0x0113

	#define UI_MOUSEFIRST                   0x0200
	#define UI_MOUSEMOVE                    0x0200

	#define UI_MOUSEHOVER                   0x02A1
    #define UI_MOUSELEAVE                   0x02A3

	#define UI_NCMOUSEHOVER                 0x02A0
    #define UI_NCMOUSELEAVE                 0x02A2

	#define UI_LBUTTONDOWN                  0x0201
	#define UI_LBUTTONUP                    0x0202

	#define UI_LBUTTONDBLCLK                0x0203
	#define UI_RBUTTONDOWN                  0x0204
	#define UI_RBUTTONUP                    0x0205
	#define UI_RBUTTONDBLCLK                0x0206
	#define UI_MBUTTONDOWN                  0x0207
	#define UI_MBUTTONUP                    0x0208
	#define UI_MBUTTONDBLCLK                0x0209
	#define UI_MOUSEWHEEL                   0x020A
	#define UI_MOUSEHWHEEL                  0x020E
	#define UI_MOUSELAST                    0x020D

	#define UI_MOUSEHOVER                   0x02A1
    #define UI_MOUSELEAVE                   0x02A3

	#define UI_NCMOUSEMOVE                  0x00A0
	#define UI_NCLBUTTONDOWN                0x00A1
    #define UI_NCLBUTTONUP                  0x00A2
    #define UI_NCMBUTTONDBLCLK              0x00A9

    #define UI_KEYDOWN                      0x0100
	#define UI_KEYUP                        0x0101
    #define UI_CHAR                         0x0102

	#define UI_SETCURSOR                    0x0020

	#define UI_HSCROLL                      0x0114
	#define UI_VSCROLL                      0x0115
  
	//扩展绘制
    #define UI_PAINT_EX                     0xF000
    #define TIMER_CARET                     1
    #define TIMER_NEXTFRAME                 2
    typedef int                             *LPINT;
	typedef unsigned int                    UINT;
    typedef unsigned char                   BYTE;
	typedef unsigned long                   ULONG_PTR;
	typedef ULONG_PTR                       DWORD_PTR;
	typedef unsigned short                  WORD;
    typedef unsigned long                   DWORD;
	typedef long                            LONG_PTR;
	typedef LONG_PTR                        LPARAM;
    typedef void*                           LPCVOID;

    #define MAKEWORD(a, b)      ((WORD)(((BYTE)(((DWORD_PTR)(a)) & 0xff)) | ((WORD)((BYTE)(((DWORD_PTR)(b)) & 0xff))) << 8))
	#define MAKELONG(a, b)      ((long)(((WORD)(((DWORD_PTR)(a)) & 0xffff)) | ((DWORD)((WORD)(((DWORD_PTR)(b)) & 0xffff))) << 16))
	#define LOWORD(l)           ((WORD)(((DWORD_PTR)(l)) & 0xffff))
	#define HIWORD(l)           ((WORD)((((DWORD_PTR)(l)) >> 16) & 0xffff))
	#define LOBYTE(w)           ((BYTE)(((DWORD_PTR)(w)) & 0xff))
	#define HIBYTE(w)           ((BYTE)((((DWORD_PTR)(w)) >> 8) & 0xff))
    
	#define MAKELPARAM(l, h)      ((LPARAM)(DWORD)MAKELONG(l, h))
    
	#ifndef FALSE
	#define FALSE   0
	#endif

    #define BOOL int                
	#ifndef TRUE
	#define TRUE    1
	#endif
#ifndef  COLORREF  
    #define COLORREF             unsigned int    
#endif
	#define RGB(r,g,b)          ((COLORREF)(((BYTE)(b)|((WORD)((BYTE)(g))<<8))|(((DWORD)(BYTE)(r))<<16)))
	#define ARGB(r,g,b,a)       (RGB(r,g,b)|(a<<24))

    #define CR_INVALID           0x00FFFFFF


	#ifndef GET_X_LPARAM
	     #define GET_X_LPARAM(lParam)    ((int)(short)LOWORD(lParam))
	#endif
	#ifndef GET_Y_LPARAM
	    #define GET_Y_LPARAM(lParam)    ((int)(short)HIWORD(lParam))
	#endif

	typedef enum tagGW_CODE
    {
        GSW_FIRSTCHILD=0,
        GSW_LASTCHILD,
        GSW_PREVSIBLING,
        GSW_NEXTSIBLING,
        GSW_PARENT,
        GSW_OWNER,
    } GW_CODE;

	enum PtSize
	{
		sUnkown,
	    sLeft,
		sRight,
		sTop,
        sBottom,
	};

	/*
 * Scroll Bar Commands
 */
	#define SB_LINEUP           0
	#define SB_LINELEFT         0
	#define SB_LINEDOWN         1
	#define SB_LINERIGHT        1
	#define SB_PAGEUP           2
	#define SB_PAGELEFT         2
	#define SB_PAGEDOWN         3
	#define SB_PAGERIGHT        3
	#define SB_THUMBPOSITION    4
	#define SB_THUMBTRACK       5
	#define SB_TOP              6
	#define SB_LEFT             6
	#define SB_BOTTOM           7
	#define SB_RIGHT            7
	#define SB_ENDSCROLL        8

	#define SIF_RANGE           0x0001
	#define SIF_PAGE            0x0002
	#define SIF_POS             0x0004
	#define SIF_DISABLENOSCROLL 0x0008
	#define SIF_TRACKPOS        0x0010
	#define SIF_ALL             (SIF_RANGE | SIF_PAGE | SIF_POS | SIF_TRACKPOS)

	class CUIBase:public CObject, public CObjRef
	{

		KKUI_CLASS_NAME_EX(CObject,CUIBase,"uibase",0)
		public:
				CUIBase();
				virtual ~CUIBase();
	};
}
#endif