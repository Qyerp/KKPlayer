#include "UIApp.h"
#include "UIBase.h"
#include "UIWindow.h"
#include "UIPanel.h"
#include "UIScrollView.h"
#include "UIMsView.h"
#include "UISkin.h"
#include "UIMoveWindow.h"
#include "UIContainerImpl.h"
#include "UIYUV.h"
#include "UISliceEditor.h"
#include "UIStageEditor.h"
#include "UIMoveState.h"
#include "UIFusionzone.h"
#include "UICurve.h"
#include "UIYUVGridPanel.h"


#include "SkinPool.h"
#include "SplitString.h"
 char SkinDef[] =
	 "<scrollbar name = \"_skin.sys.scrollbar\" src = \"PNG:ID_PNG_CMN_SCROLLBAR\" margin = \"3\" hasgripper = \"0\" /> " \
	 "<imgframe name=\"_skin.StageBk\" src=\"PNG:ID_PNG_StageBk\" /> " \
	 "<imgframe name=\"_skin.VCircle\" src=\"PNG:ID_PNG_CircleShape\" /> ";
	
namespace KKUI{
    template<> CUIApp * CSingleton<CUIApp>::m_Singleton=0;
	CUIApp::CUIApp():m_pRenderFactory(0),
		m_fpGetRawBufferSize(0),
		m_fpGetRawBuffer(0)
	{

		   new CSkinPoolMgr();
		   //ע��ؼ�
		   RegisterCtlClass<CUIBase>();
		   RegisterCtlClass<CUIWindow>();
		   RegisterCtlClass<CUIPanel>();
		   RegisterCtlClass<CUIScrollView>();
		   RegisterCtlClass<CUIMsView>();
           RegisterCtlClass<CUIContainerImpl>();
           RegisterCtlClass<CUIMoveWindow>();  
		   RegisterCtlClass<CUIYUV>();
		   RegisterCtlClass<CUISliceEditor>();
		   RegisterCtlClass<CUIStageEditor>();
           RegisterCtlClass<CUIMoveState>();
		   RegisterCtlClass<CUIYUVGridPanel>();
		   RegisterCtlClass<CUIFusionzone>();
		  RegisterCtlClass< CUICurve>();
		   //ע��Ƥ��
		   RegisterSkinClass<CSkinImgList>();
		   RegisterSkinClass<CSkinImgFrame>();
	       RegisterSkinClass<CSkinScrollbar>();

	}
	CUIApp::~CUIApp()
	{

	}

	void        CUIApp::SetRenderFactory(IRenderFactory* pRenderFactory)
	{
	      if(m_pRenderFactory==0)
			  m_pRenderFactory = pRenderFactory;
	}
    CUIWindow*  CUIApp::CreateUIByName(char* classname) const
	{
		return (CUIWindow*)CreateObject(CObjectInfo(classname, UICtl));
	}

	ISkinObj *   CUIApp::CreateSkinByName(char*  pszSkinClass) const
	{
		return (ISkinObj*)CreateObject(CObjectInfo(pszSkinClass, Skin));
	}


	bool CUIApp::LoadXmlDocmentFile( std::string xmlpath,pugi::xml_document & xmlDoc)
	{
		    pugi::xml_parse_result result= xmlDoc.load_file(xmlpath.c_str(),pugi::parse_default,pugi::encoding_utf8);
           
            return result;
	}

	IBitmap * CUIApp::LoadImage2(const SStringA & strImgID)
    {
     
        SStringAList strLst;
        int nSegs = ParseResID(strImgID,strLst);
        if(nSegs == 2) 
		{
			  IBitmap *pBitmap=0;
		    
			  char*  p1 =(char*) strLst.at(0).GetBuffer(64);
			  char*  p2 =(char*) strLst.at(1).GetBuffer(64);
			  int len=m_fpGetRawBufferSize(p1,p2);
			  if(len>0){ 
				  m_pRenderFactory->CreateBitmap(&pBitmap);
				  void* buffer = ::malloc(len);
                  m_fpGetRawBuffer(p1,p2, buffer ,len);
                  pBitmap->LoadFromMemory(buffer,len);
				  delete buffer;
			  }

			  return pBitmap;
		}
        else /**/
			return NULL;
    }

	 int   CUIApp::SetGetResCallInfo(KKUI_GetRawBufferSize fp1,KKUI_GetRawBuffer fp2)
	 {
	    m_fpGetRawBufferSize = fp1;
		m_fpGetRawBuffer = fp2;
		return 0;
	 }
	 int    CUIApp::IniDefSkin()
	 {

		 pugi::xml_document xmlDoc;
		 if(!xmlDoc.load_buffer(SkinDef,strlen(SkinDef),pugi::parse_default,pugi::encoding_utf8)) 
			return NULL;

		 CSkinPool* pool = new CSkinPool();
		 pool->LoadSkins( xmlDoc);
	     CSkinPoolMgr* mgr=CSkinPoolMgr::getSingletonPtr();
		 mgr->PushSkinPool(pool);
		 return 1;
	 }
	 int   CUIApp::LoadXmlSkin(char *xmlSkin)
	 {
	     pugi::xml_document xmlDoc;
		 if(!xmlDoc.load_buffer(xmlSkin,strlen(xmlSkin),pugi::parse_default,pugi::encoding_utf8)) 
			return NULL;

		 CSkinPool* pool = new CSkinPool();
		 pool->LoadSkins( xmlDoc);
	     CSkinPoolMgr* mgr=CSkinPoolMgr::getSingletonPtr();
		 mgr->PushSkinPool(pool);
		 return 1;
	 }
	  
}
