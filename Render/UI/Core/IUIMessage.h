#ifndef  KKUI_IUIMessage_H_
#define  KKUI_IUIMessage_H_
#include "TypeDef.h"
namespace KKUI
{

  
    
	enum UIEventType
	{
	    E_Click     = 0,
		E_RClick    = 1,
		E_CtlSize   = 2,
		E_CltMove   = 3,
		//���˫��
		E_CltLBDBLClk = 4,	
		E_CltMoveOver,
	};
	struct UIPosInfo
	{
	    int Left;
		int Right;
        int Top;
		int Bottom;
		

	};

	struct UIFPosInfo
	{
	    float x1;
		float  y1;

		float  x2;
		float  y2;

		float  x3;
		float  y3;

		float  x4;
		float  y4;
	};

	struct UIEvent
	{
	    void*         Handle;
		UIEventType   type;
		uint_ptr      pa1;
		uint_ptr      pa2;
		UIPosInfo     pos1;
		UIPosInfo     pos2;

		UIFPosInfo    fpos1;
		UIFPosInfo    fpos2;
	};
	
    class IUIMessage
	{
	   public:
		      virtual int     UISendMessage(unsigned int Msg, uint_ptr wParam, int_ptr lParam) =0;
			  virtual void    UIInvalidateRect(int l,int r,int t,int b) = 0;
			  virtual void    UISetCursor(const char* name) =0;
              virtual void    OnUIEvent(const UIEvent* ev) =0;
	};
}
#endif