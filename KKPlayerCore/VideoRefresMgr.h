#ifndef VideoRefresMgr_H_
#define VideoRefresMgr_H_
#include <set>
#include "../KKPlayer/MainFrm.h"
///视频统一刷新
class    CVideoRefresMgr
{
	public:
			CVideoRefresMgr();
			~CVideoRefresMgr();
			void Init();
			void AddPlayer(CMainFrame* player);
			void RemovePlayer(CMainFrame* player);
	protected:
		 /*********视频刷新线程********/
			static unsigned __stdcall VideoRefreshthread(LPVOID lpParameter);
			void OnVideoRefresh();
	private:
		    //视频刷新线程
			SKK_ThreadInfo                    m_VideoRefreshthreadInfo;
		    std::set<CMainFrame*>             m_PlayerSet;
			CKKLock                           m_Lock;
};
#endif