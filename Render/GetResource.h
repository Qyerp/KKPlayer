#ifndef GetResource_H_
#define GetResource_H_
#include <Windows.h>
#include <string>
class   CGetResource
{
	public:
		   CGetResource(HMODULE hModule, std::wstring Name,  std::wstring Type);
		   ~CGetResource();
           void*  GetResourceBuffer(UINT& bufflen);
    private:
		   std::wstring   m_strName;
		   std::wstring   m_strType;
		   HMODULE m_hDllModule;
		   UINT    m_nResSize;
		   HRSRC   m_hRR; 
		   HGLOBAL m_hG;
		   void*   m_pData ;
};
#endif