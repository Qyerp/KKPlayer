// renderuitest.cpp : 定义应用程序的入口点。
//

#include "stdafx.h"
#include "renderuitest.h"
#include "../Render/UI/Core/IRender.h"
#include "msWnd.h"
#include "MsgLoop.h"

typedef void*    (*CreateRender)(HWND h,char *Oput,int cpu_flags);
typedef char     (*DelRender)(void *p,char RenderType);
typedef void     (*CreateRenderFactoryD3D9)(KKUI::IRenderFactory** ff);
typedef void     (*CreateRenderFactoryGl)(KKUI::IRenderFactory** ff);

CreateRender               fpCreateRender=0;
DelRender                  fpDelRender=0;



#include "IUI.h"

KKUI_Ini                           fpUI_Ini        = 0;

KKUI_CreateCtl                     fpUI_CreateCtl  = 0;

KKUI_FindCtlName                   fpUI_FindCtlName = 0 ;

KKUI_GetCtlName                    fpUI_GetCtlName  = 0 ;

KKUI_GetCtlWindowRect              fpUI_GetCtlWindowRect = 0 ;

KKUI_SetCtlAttr                    fpUI_SetCtlAttr = 0 ;

//获取用户数据
KKUI_GetCtlUserData                fpUI_GetCtlUserData = 0;
//设置用户数据
KKUI_SetCtlUserData                fpUI_SetCtlUserData = 0;

KKUI_SetUIMessage                  fpUI_SetUIMessage = 0;

KKUI_SetIRenderFactory             fpUI_SetIRenderFactory = 0;

KKUI_Opain                         fpUI_Opain      = 0;

KKUI_StageOpain                    fpUI_StageOpain = 0;

KKUI_Relayout                      fpUI_Relayout   = 0;

KKUI_FrameEvent                    fpUI_FrameEvent = 0;

KKUI_OnDestroy                     fpUI_OnDestroy  =0;

KKUI_CreateChildren                fpUI_CreateChildren = 0;

KKUI_YUVLoad                       fpUI_YUVLoad = 0;

CreateRenderFactoryD3D9            fpCreateRenderFactoryD3D9 = 0;
CreateRenderFactoryD3D11           fpCreateRenderFactoryD3D11 = 0;

KKUI_SetGetResCallInfo             fpUI_SetGetResCallInfo = 0;
KKUI_InitDefSkin                   fpUI_InitDefSkin = 0;
KKUI_AddRef                        fpUI_AddRef = 0 ;
KKUI_Release                       fpUI_Release = 0;

CreateRenderFactoryGl				fpCreateRenderFactoryGl = 0;

static HMODULE   hSysResource=0;
int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	
    int lx = 26738%2048;
	if(hSysResource==NULL)
	    hSysResource=LoadLibrary(_T("render.dll"));
	
    
    fpCreateRender      = (CreateRender)GetProcAddress(hSysResource, "CreateRender");
    fpDelRender         = (DelRender)GetProcAddress(hSysResource, "DelRender");
   

	fpUI_Ini                  = (KKUI_Ini)GetProcAddress(hSysResource,                     "UI_Ini");
	fpUI_CreateCtl            = (KKUI_CreateCtl)GetProcAddress(hSysResource,               "UI_CreateCtl"); 
	fpUI_FindCtlName          = (KKUI_FindCtlName)GetProcAddress(hSysResource,             "UI_FindCtlName"); 
    fpUI_InitDefSkin          = (KKUI_InitDefSkin)GetProcAddress(hSysResource,             "UI_InitDefSkin"); 
    fpUI_GetCtlName           = (KKUI_GetCtlName)GetProcAddress(hSysResource,              "UI_GetCtlName");   
    fpUI_GetCtlWindowRect     = (KKUI_GetCtlWindowRect)GetProcAddress(hSysResource,        "UI_GetCtlWindowRect");
    fpUI_SetCtlAttr           = (KKUI_SetCtlAttr)GetProcAddress(hSysResource,              "UI_SetCtlAttr");
    
	fpUI_GetCtlUserData       = (KKUI_GetCtlUserData)GetProcAddress(hSysResource,          "UI_GetCtlUserData");
    fpUI_SetCtlUserData       = (KKUI_SetCtlUserData)GetProcAddress(hSysResource,          "UI_SetCtlUserData");

	fpUI_SetUIMessage         = (KKUI_SetUIMessage)GetProcAddress(hSysResource,            "UI_SetUIMessage"); 
	fpUI_SetIRenderFactory    = (KKUI_SetIRenderFactory)GetProcAddress(hSysResource,       "UI_SetIRenderFactory");

	fpUI_Opain                = (KKUI_Opain)GetProcAddress(hSysResource,                   "UI_Opain");
	fpUI_StageOpain           = (KKUI_StageOpain)GetProcAddress(hSysResource,                   "UI_Opain");

    fpUI_Relayout             = (KKUI_Relayout)GetProcAddress(hSysResource,                "UI_Relayout");
    fpUI_FrameEvent           = (KKUI_FrameEvent)GetProcAddress(hSysResource,              "UI_FrameEvent");
	fpUI_OnDestroy            = (KKUI_OnDestroy)GetProcAddress(hSysResource,               "UI_OnDestroy");
	fpUI_CreateChildren       = (KKUI_CreateChildren)GetProcAddress(hSysResource,          "UI_CreateChildren");
	fpUI_YUVLoad              = (KKUI_YUVLoad)GetProcAddress(hSysResource,                 "UI_YUVLoad");
    fpCreateRenderFactoryD3D9 = (CreateRenderFactoryD3D9)GetProcAddress(hSysResource,      "CreateRenderFactoryD3D9");
	//fpCreateRenderFactoryD3D9 = (CreateRenderFactoryD3D9)GetProcAddress(hSysResource,      "CreateRenderFactoryGl");
    //fpCreateRenderFactoryD3D11= (CreateRenderFactoryD3D9)GetProcAddress(hSysResource,      "CreateRenderFactoryD3D11");
	//fpCreateRenderFactoryGl   = (CreateRenderFactoryGl)GetProcAddress(hSysResource, "CreateRenderFactoryGl");

    fpUI_SetGetResCallInfo    = (KKUI_SetGetResCallInfo)GetProcAddress(hSysResource,        "UI_SetGetResCallInfo");
	fpUI_AddRef               = (KKUI_AddRef)GetProcAddress(hSysResource,                  "UI_AddRef");
    fpUI_Release              = (KKUI_Release)GetProcAddress(hSysResource,                 "UI_Release");

	
	//创建一个呈现工厂
	KKUI::IRenderFactory* pRenderFactory=0;
  
	fpCreateRenderFactoryD3D9(&pRenderFactory);
	//fpCreateRenderFactoryGl(&pRenderFactory);

	pRenderFactory->init();
	
	//初始化UI
	fpUI_Ini (pRenderFactory);

	
	
	KKUIHandle uiroot=fpUI_CreateCtl("uiroot");
	//pRenderFactory->Release();
 	
    fpUI_SetIRenderFactory(uiroot, pRenderFactory);
	KKUI::CUIWnd::SetRenderFactory(pRenderFactory);
	KKUI::CSimpleWndHelper::Init(hInstance,L"kkui");

	KKUI::CMsWnd ms(uiroot);

	fpUI_SetUIMessage(uiroot,&ms);
	ms.Create(NULL, WS_MINIMIZEBOX | WS_MAXIMIZEBOX|WS_CAPTION /*| WS_CLIPCHILDREN | WS_CLIPSIBLINGS*/, 0, 0, 0, 800,500);

	ms.CenterWindow(ms.m_hWnd);
	
	 
	ms.ShowWindow(SW_SHOW);/**/
	ms.IniCtl();
	::SetFocus(ms.m_hWnd);
	KKUI::SMessageLoop loop;
    loop.Run();

	return 0;
}

