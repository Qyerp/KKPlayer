#ifndef renderD3D11_H_
#define renderD3D11_H_
#include "../KKPlayerCore/render/render.h"
class CRenderD3D11 : public IkkRender
{
public:
     CRenderD3D11(int         cpu_flags);
	 ~CRenderD3D11();
	 ERenderType GetRenderType(){return D3D11_RENDER;}
	 IDXVA2Ctx* CreateDxva2Ctx(int CodecId,int Width,int Height) {return 0;}
	void       FreeDxva2Ctx (IDXVA2Ctx* ctx){}

	virtual IDXTexture* CReateDXTexture()  {return 0;}
	virtual void       FreeDXTexture(IDXTexture* ctx) {}
};
#endif