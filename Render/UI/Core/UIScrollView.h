#ifndef  KKUI_UIScrollView_H_
#define  KKUI_UIScrollView_H_
#include "UIPanel.h"
#include "UIContainerImpl.h"
#include "UISkin.h"
namespace KKUI
{
	class CUIScrollView: public CUIPanel
	{
	    KKUI_CLASS_NAME(CUIPanel,CUIScrollView,"uiscrollsview")
	    public:
			  CUIScrollView();
			  ~CUIScrollView();
			  
			  void GetOffXY(int& x,int& y,int& WinWidth,int& WinHeight);
	    protected:
			  void  OnSize(unsigned int nType, int x,int y);
			  void  OnEraseBkgnd( IRenderTarget  *pRT);
			  KKUI_MSG_MAP_BEGIN()   
					MSG_UI_SIZE(OnSize)
			  KKUI_MSG_MAP_END(CUIPanel)

	    protected:
		     void UpdateScrollBar();
			 virtual void OnViewSizeChanged(CSize szOld,CSize szNew);
           
			         void SetViewSize(CSize szView);
		     virtual void UpdateChildrenPosition();
                     void CalViewSz();
	     protected:
		     int         m_bAutoViewSize;
			 CSize       m_szView;
			 CPoint      m_ptOrigin;
	};
}
#endif