#ifndef DXVA2_D3D9_H_
#define DXVA2_D3D9_H_
extern "C" {
#include "libavformat/avformat.h"
}
#include <set>
#include <string>
#include <d3d9.h>
#include <initguid.h>
#include <dxva2api.h>
#include "ObjRef.h"
#include "Core/ObjectImp.h"
#include "Core/IRender.h"
#include "libavcodec/dxva2.h"
#include "../../KKPlayerCore/render/render.h"
/***************
*DXVA2 硬件解码，配合ffmpeg
**********/
 typedef  struct SD3dFormat
		{
			const char   *name;
			D3DFORMAT    format;
			AVPixelFormat  codec;
		} d3d_format_t;
namespace KKUI
{
       #define VA_DXVA2_MAX_SURFACE_COUNT       64
	   class SRenderFactory_D3D9;
	   class CDXVA2VD_D3D9;
       
	   struct SVideoFormat{

			AVPixelFormat  i_chroma;                               /**< picture chroma */
			unsigned int i_width;                                 /**< picture width */
			unsigned int i_height;                               /**< picture height */
			unsigned int i_x_offset;               /**< start offset of visible area */
			unsigned int i_y_offset;               /**< start offset of visible area */
			unsigned int i_visible_width;                 /**< width of visible area */
			unsigned int i_visible_height;               /**< height of visible area */
		 
			unsigned int i_bits_per_pixel;             /**< number of bits per pixel */
		 
			unsigned int i_sar_num;                   /**< sample/pixel aspect ratio */
			unsigned int i_sar_den;
		 
			unsigned int i_frame_rate;                     /**< frame rate numerator */
			unsigned int i_frame_rate_base;              /**< frame rate denominator */
		 
			uint32_t i_rmask, i_gmask, i_bmask;          /**< color masks for RGB chroma */
			int i_rrshift, i_lrshift;
			int i_rgshift, i_lgshift;
			int i_rbshift, i_lbshift;
	   };

	   

	   struct SSurfaceInfo
	   {
			int index;
			bool used;
			LPDIRECT3DSURFACE9 d3d;
			uint64_t age;
			uint64_t usedCount;
       };


	   class CDeviceDXVA2Manager9_D3D9: public TObjRefImpl<IRenderOthert>
	   {
		    friend class  CDXVA2VD_D3D9;
			public:
				   CDeviceDXVA2Manager9_D3D9(SRenderFactory_D3D9*  pRenderFactory);
				   ~CDeviceDXVA2Manager9_D3D9();
				   void  Release2();
				   void  ReLoadRes();

				   CDXVA2VD_D3D9* CreateVideoDecoder(int CodecId,int Width,int Height);
				   int            DelVideoDecoder(CDXVA2VD_D3D9* pVD);
				   
	        private:
				   int            InitVideoDecoder(CDXVA2VD_D3D9* pVideoDecodec);
				   int            DxFindVideoServiceConversion(int nCodecId,  GUID *input, D3DFORMAT *output);
				   
				   int            DxCreateVDecoder(CDXVA2VD_D3D9* pVideoDecodec,int codec_id, const SVideoFormat *fmt);

				   SRenderFactory_D3D9*         m_pRenderFactory;
				   IDirect3DDeviceManager9*     m_pDevmgr;
				   IDirectXVideoDecoderService* m_pVideoDecodeService;
				   HANDLE                       m_hDevice;
				   unsigned int                 m_pDevToken;
				   std::set<CDXVA2VD_D3D9*>     m_VDObjSet;
	   };

	   //video 解码器
       class CDXVA2VD_D3D9: public IDXVA2Ctx, public TObjRefImpl<IRenderOthert>
	   {
		   friend class  CDeviceDXVA2Manager9_D3D9;
		   public:
				  CDXVA2VD_D3D9(SRenderFactory_D3D9*  pRenderFactory,CDeviceDXVA2Manager9_D3D9* pDxva2Mgr);
				  ~CDXVA2VD_D3D9();

				  void*    GetDxvaContext();
		          int      GetPixFmt();
				  int      GetSurfaceIndex(void *ffFrame);
				  int      ReleaseSurface (void *Surface,void* Decoder);
				  int      IsSurfaceValid(void *Surface,int SurfaceId);
				  int      GetLostDevice();
				  void*    GetDecoder();

				  void     Release2();
				  void     ReLoadRes();
	       private:
			      SRenderFactory_D3D9*           m_pRenderFactory;
				  CDeviceDXVA2Manager9_D3D9*     m_pDxva2Mgr;
				  int                            m_nCodecId;
				  //dxva 环境
				  dxva_context                   m_Dxva_Context;
				  GUID                           m_InputGuid;
				  D3DFORMAT                      m_OutputFormat;
				  SVideoFormat                   m_fmt;
                  SD3dFormat                     m_D3dFormat;
	              /* Video decoder */
				  IDirectXVideoDecoder*          m_pDecoder;
	              DXVA2_ConfigPictureDecode      m_VideoCfg;
				  unsigned   int                 m_nSurfaceCount;
				  unsigned   int                 m_nSurfaceOrder;
				  int                            m_nSurfaceWidth;
				  int                            m_nSurfaceHeight;
				  SSurfaceInfo                   m_SurfaceInfos[VA_DXVA2_MAX_SURFACE_COUNT];
				  LPDIRECT3DSURFACE9             m_HwSurface   [VA_DXVA2_MAX_SURFACE_COUNT];

	   };
}
#endif