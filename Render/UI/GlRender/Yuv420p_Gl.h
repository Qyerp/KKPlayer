#ifndef Yuv420p_D3D9_H_
#define Yuv420p_D3D9_H_
#include <map>
#include <string>
#include <d3d9.h>
#include "ObjRef.h"
#include "Core/ObjectImp.h"
#include "Core/IRender.h"
#include "glad.h"
namespace KKUI
{
	class SRenderFactory_Gl;
	class CYuv420p_Gl : public TObjRefImpl<IPicYuv420p>
	{
	public:
		CYuv420p_Gl(SRenderFactory_Gl* pRenderFactory);
		~CYuv420p_Gl();
		void                   Release2();
		void                   ReLoadRes();
		kkSize                 Size() const;

		void                   ClearPicData(unsigned int Color);
		//��������
		void                   SetQuoteObj(IPicYuv420p* pParentObj);
		int	                   Load(const kkPicInfo* data);
		GLuint				   GetRttTexture();
		void                   SavePreFrame(bool save){}
		int                    HasPreFrame(){return 0;}
		void                   CreatePicCache(int width,int height,int format){}
	private:
		int                    LoadYuv      (const kkPicInfo* data);
		int						LoadARGB   (const kkPicInfo* data);
		SRenderFactory_Gl*    m_pRenderFactory;
		GLuint		m_nY;
		GLuint		m_nU;
		GLuint		m_nV;
		GLuint		m_nRtt;
		GLuint		m_nRttTex;
		GLuint		m_nRgb;
		kkSize		m_Size;
		HGLRC		m_hRC2;
	};
}
#endif