#include  "VideoRefresMgr.h"
CVideoRefresMgr::CVideoRefresMgr()
{

}
CVideoRefresMgr::~CVideoRefresMgr()
{

}
void CVideoRefresMgr::Init()
{
	static int  binit=1;
	if(binit)
	{
		#ifdef WIN32
			m_VideoRefreshthreadInfo.ThreadHandel  = (HANDLE)_beginthreadex(NULL, NULL, VideoRefreshthread, (LPVOID)this, 0,&m_VideoRefreshthreadInfo.Addr);   
		#else
			m_VideoRefreshthreadInfo.Addr = pthread_create(&m_VideoRefreshthreadInfo.Tid_task, NULL, (void* (*)(void*))VideoRefreshthread, (LPVOID)this);
		#endif
		binit=0;
	}
}
unsigned __stdcall CVideoRefresMgr::VideoRefreshthread(LPVOID lpParameter)
{
#ifdef WIN32
	 ::SetThreadPriority(::GetCurrentThread(), THREAD_PRIORITY_HIGHEST);
#endif

        CVideoRefresMgr* mgr = (CVideoRefresMgr*)lpParameter;
		mgr->OnVideoRefresh();
        return 0;
}
void  CVideoRefresMgr::AddPlayer(CMainFrame* player)
{
            m_Lock.Lock();
		    std::set<CMainFrame*>::iterator It= m_PlayerSet.find(player);
			if(It==m_PlayerSet.end())
			{
				m_PlayerSet.insert(player);
			}
			m_Lock.Unlock();
}
void  CVideoRefresMgr::RemovePlayer(CMainFrame* player)
{
           m_Lock.Lock();
		    std::set<CMainFrame*>::iterator It= m_PlayerSet.find(player);
			if(It!=m_PlayerSet.end())
			{
				m_PlayerSet.erase(It);
			}
			m_Lock.Unlock();
}
void  CVideoRefresMgr::OnVideoRefresh()
{
	double remaining_time = REFRESH_RATE;
	
	while(true)
	{
		    remaining_time = REFRESH_RATE;
			m_Lock.Lock();
			std::set<CMainFrame*>::iterator It= m_PlayerSet.begin();
			for(;It!=m_PlayerSet.end();++It)
			{
			     remaining_time = FFMIN(remaining_time, (*It)->OnVideoRefresh());
			}
			m_Lock.Unlock();

			if (remaining_time > 0.0)
			{
				int64_t ll=(int64_t)(remaining_time* 1000000.0);
				av_usleep(ll);
	        }
	}
}