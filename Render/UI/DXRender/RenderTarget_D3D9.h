#ifndef RenderTarget_D3D9_H_
#define RenderTarget_D3D9_H_

#include <map>
#include <set>
#include <string>
#include <vector>
#include <d3d9.h>
#include <d3dx9core.h>
#include <dxva2api.h>
#include "ObjRef.h"
#include "Core/ObjectImp.h"
#include "Core/IRender.h"
#include "RendLock.h"
#include "RttCache_D3D9.h"
namespace KKUI
{

    class CPixelShader_D3D9;
	class CYUVPixelShader_D3D9;
	class CARGBPixelShader_D3D9;
	class CDeviceDXVA2Manager9_D3D9;
	class CRenderTarget_D3D9;

	//渲染缓冲文理
	typedef struct  SRender3DTexture
	{
	   int Width;
	   int Height;
	   IDirect3DTexture9*      pTexture;
	   int Used;
	};
	class SRenderFactory_D3D9:public CRttCache_D3D9, public  TObjRefImpl<IRenderFactory>
    {
		friend class CRenderTarget_D3D9;
		public:
				SRenderFactory_D3D9();
				~SRenderFactory_D3D9();
				bool                        init();
				//初始化内置着色器
				int                          InitShader();
				//创建一个呈现对象
				int                          CreateRenderTarget(IRenderTarget ** ppRenderTarget,void* Handle,int nWid,int nHei);
				int                          CreateBitmap(IBitmap ** ppBitmap);
				int                          CreateYUV420p(IPicYuv420p ** ppBitmap);
                int                          BeforePaint(IRenderTarget* pRenderTarget);
				void                         EndPaint(IRenderTarget* pRenderTarget,bool WarpNet=0);
			    void                         CreateFont(IFont** ppFont,int FontSize,wchar_t* FontName);
				IDirect3DDeviceManager9*     CreateDevMgr(unsigned int& token);

                void                         Lock();
				void                         UnLock();

                void*                        CreateVideoDecoder(int CodecId,int Width,int Height);
				void                         DeleteVideoDecoder(void* pDXVA2VD);

				void*                        CreateDxTexture();
				void                         DeleteDxTexturer(void* DxTexture);


				IDirect3DDevice9*            GetDevice(){return m_pDevice;}
				bool                         LostDevice();
				bool                         LostDeviceRestore();
				
                //获取默认像素着色器
				CPixelShader_D3D9*           GetDefPixelShader(){return m_pDefPixelShader;}
				CYUVPixelShader_D3D9*        GetDefYuvPixelShader(){ return  m_pDefYuvPixelShader;}
				CARGBPixelShader_D3D9*       GetDefARGBPixelShader(){ return  m_pDefARGBPixelShader;}
				//移除创建的对象
				void                         RemoveObj(IRenderObj* obj,int lock=1);
                IDirect3DTexture9*           GetFromFileInMemory(unsigned char* data,int len);
                void                         SaveFile(wchar_t *Path,IDirect3DTexture9* textrue);

				//获取像素着色器
                CPixelShader_D3D9*           GetPsByName(char* Name);
				void                         CopyToTex(IDirect3DTexture9* text1,int Width,int Height,IDirect3DTexture9* text2,char* PsName);

				

	    private:
			     IDirect3D9*               m_pD3D;
                 IDirect3DDevice9*         m_pDevice;
				 CRendLock                 m_lock;
				 //呈现对象管理器
				 std::set<IRenderObj*>        m_IRenderObjSet;
				 /********像素着色器**********/
				 std::map<std::string ,CPixelShader_D3D9*> m_IPsObjMap;

				 //渲染缓冲对象
                 std::map<unsigned long long, std::vector<SRender3DTexture*> >   m_BufferRenderMap;
				 
				 CPixelShader_D3D9*         m_pDefPixelShader;
				 CYUVPixelShader_D3D9*      m_pDefYuvPixelShader;
				 CARGBPixelShader_D3D9*     m_pDefARGBPixelShader;
				 CDeviceDXVA2Manager9_D3D9* m_pDeviceDXVA2Manager9;

    };

	class CD3D9DevGurd
	{
	  public:
				CD3D9DevGurd(IDirect3DDevice9*     pDevice)
				{
					m_pDevice=pDevice;
					if(m_pDevice!=NULL)
					  m_nhr= pDevice->BeginScene();
				}
				
				~CD3D9DevGurd()
				{
					if(m_pDevice!=NULL&&SUCCEEDED(m_nhr)){
						m_pDevice->EndScene();
					}
				}
	private:
		        HRESULT  m_nhr;
			   void* operator new(size_t size );
			   void  operator delete(void *ptr);
				CD3D9DevGurd(const CD3D9DevGurd& cs);
				CD3D9DevGurd& operator = (CD3D9DevGurd& cs);
	private:
			IDirect3DDevice9*      m_pDevice;
	};
	class CFont_D3D9:public TObjRefImpl<IFont>
	{
		public:
			CFont_D3D9(SRenderFactory_D3D9* pRenderFactory,int FontSize,wchar_t* FontName);
			~CFont_D3D9();
			void          Release2();
			void          ReLoadRes();
			int           TextSize();
			ID3DXFont*    GetD3DXFont();
		private:
			ID3DXFont*              m_pD3DXFont;
			SRenderFactory_D3D9*    m_pRenderFactory;
			int                     m_nTextSize;
			wchar_t                 m_nFontName[64];
	};

	class CBitmap_D3D9:public TObjRefImpl<IBitmap>
	{
		public:
			CBitmap_D3D9(SRenderFactory_D3D9* pRenderFactory);
			~CBitmap_D3D9();
			void          Release2();
			void          ReLoadRes();
			virtual kkSize    Size() const;
			virtual int       Width();
			virtual int       Height();
			int               LoadFromFile(const char*FileName);
            int	              LoadFromMemory(const void* pBuf,int szLen);
		
			IDirect3DTexture9*  GetTexture(){ return m_pTexture;}
		private:
			IDirect3DTexture9*      m_pTexture;
			SRenderFactory_D3D9*    m_pRenderFactory;
			//暂且先缓存
			void*                   m_pSrcBuffer;
			int                     m_pSrcBufferLen;
			kkSize                  m_sz;
	};
	class CRenderTarget_D3D9: public TObjRefImpl<IRenderTarget>
	{
		  public:
			     CRenderTarget_D3D9(SRenderFactory_D3D9* pRenderFactory,HWND h,int nWidth, int nHeight);
			     ~CRenderTarget_D3D9();
				 void  Release2();
				 void  ReLoadRes();
				 void  ReSize(int nWidth, int nHeight);
				 //绘制位图
				 int   DrawBitmap(kkRect* pRcDest,IBitmap *pBitmap,int xSrc,int ySrc,int cx,int cy,unsigned char byAlpha=0xFF);
				 int   DrawBitmapEx(kkRect*  pRcDest,IBitmap *pBitmap,kkRect* pRcSrc,unsigned int expendMode,unsigned char byAlpha=0xFF/**/ );
				 int   DrawBitmap9Patch(kkRect* pRcDest,IBitmap *pBitmap,kkRect* pRcSrc,kkRect* pRcSourMargin,unsigned expendMode,unsigned char byAlpha=0xFF);

				 int   DrawTexture(IDirect3DTexture9* pTex);
				
				 int   DrawYuv420p(IPicYuv420p *pYuv420p,SinkFilter*Filter,const SPalette& Palette,SVertInfo* VertInfo,int VertCount,int UseLast=0,SMaskInfo* maskinfo=0,int maskcount=0);
				 IDirect3DTexture9*   FilterYuv420p2(IPicYuv420p *pYuv420p,SinkFilter* Filter);

                 int   FilterYuv420p(IPicYuv420p *pYuv420p,SinkFilter* Filter);


                 void  DrawLine(int startx,int starty, int endx,int endy, unsigned int color);
				 void  DrawLines(kkPoint* pts,int count, unsigned int color);
				 void  DrawLines(SUVInfo* pts,int count, unsigned int color);

                 void  DrawText(IFont* pFont,wchar_t *text,kkRect* pRcDest,unsigned int color=0xFFFFFFFF);
				 void  MeasureText(IFont* pFont,wchar_t *text,kkSize *pSz );

				 //垂直绘制文本
				 void  TextOutV(IFont* pFont,int x,int y , wchar_t* strText,unsigned int color=0xFFFFFFFF);
				 void  DrawTextV(IFont* pFont,wchar_t *text,kkRect* pRcDest,unsigned int color=0xFFFFFFFF);
                 void  MeasureTextV(IFont* pFont,wchar_t *text,kkSize *pSz );


                 void  Rectangle(kkRect* rt,unsigned int cr);
				
				 void  RectangleByPt(kkPoint* pt,int width,int height,unsigned int cr);
				 int   FillRect(kkRect* rt,unsigned int cr);
				 void  FillRect(kkRect* rt,unsigned int cr1,unsigned int cr2,unsigned int cr3,unsigned int cr4);
				 void  FillRect(SUVInfo* pts1,SUVInfo* pts2,SUVInfo* pts3,SUVInfo* pts4,unsigned int cr1,unsigned int cr2,unsigned int cr3,unsigned int cr4);
				 void  FillRectByPt(kkPoint* pt,int width,int height,unsigned int cr);

				 void  DrawCircle(int xCenter, int yCenter, int nRadius,unsigned int cr,int fillmodel=0);
				 int   GetWidth(){return    m_nWidth;}
				 int   GetHeight(){return   m_nHeight;}

				  //是否启用变形
				 void  SetEnableWarp(void*  pIWarp);
				 int   GetEnableWarp(){return m_nWarp;}
				 void* GetIWarp(){return m_pIWarp;}
				 void  SetFusionzone(void* Fusionzone);
				 //是否启用融合带
				 int   GetEnableFusion(){return m_nEnableFusion;}
				 void  SetEnableFusionzone(int Fusionzone){m_nEnableFusion = Fusionzone; }
				  //当有变形是通过这个来绘制
				 void  DrawWarp(SVertInfo* VertInfo,int VertCount);

				 //绘制融合带
				 void  OnFusionzone();

				 IDirect3DSurface9*     GetSurface(){return m_pSwapChainSurface9;}
				 IDirect3DSwapChain9*   GetSwapChain(){return  m_pSwapChain;}
				 IDirect3DTexture9*     GetRenderTexture(){ return m_pRenderTexture;}
	     private:
			     //调整通道指定区域的ARGB值
				 int   AdjustARGB(const SPalette& Palette,SMaskInfo* maskinfo,int maskcount);
				 
			     void RecreateSwapChain();
			     int m_nWidth;
				 int m_nHeight;
		         HWND m_hView;
				 SRenderFactory_D3D9*     m_pRenderFactory;
				 IDirect3DSwapChain9*     m_pSwapChain;
				 
	             IDirect3DSurface9 *      m_pSwapChainSurface9;
				  //呈现的纹理
				 IDirect3DTexture9*       m_pRenderTexture;
				 int                      m_nWarp;
				 //启用融合带
				 int                      m_nEnableFusion; 
				 //融合带
                 void*                    m_pFusionzone;
				 void*                    m_pIWarp;

	};

	////顶点色器
	//class CVertShader_D3D9: public TObjRefImpl<IRenderOthert>
	//{
	//     public:
	//		     CVertShader_D3D9(SRenderFactory_D3D9* pRenderFactory,const DWORD* psCode,const char* psName);
	//			 ~CVertShader_D3D9();
	//			 void  Release2();
	//			 void  ReLoadRes();

	//			
	//			 IDirect3DVertexShader9	  GetVertShader(){return m_pPixelShader;}
	//			 virtual void IniPs();
	//			 virtual const char* GetVertName() { return "";}
	//  protected:
	//		    
	//		     IDirect3DVertexShader9	  m_pPixelShader;
	//		     SRenderFactory_D3D9*     m_pRenderFactory;
	//			 LPD3DXCONSTANTTABLE      m_pConstantTable;
	//			 LPD3DXBUFFER             m_pPsCode;
	//};

	//像素这色器
	class CPixelShader_D3D9: public TObjRefImpl<IRenderOthert>
	{
	     public:
			     CPixelShader_D3D9(SRenderFactory_D3D9* pRenderFactory);
				 ~CPixelShader_D3D9();
				 void  Release2();
				 void  ReLoadRes();

				 LPD3DXCONSTANTTABLE      GetConstantTable(){return m_pConstantTable;}
				 LPDIRECT3DPIXELSHADER9	  GetPixelShader(){return m_pPixelShader;}
				 D3DXHANDLE	    GetSamplerHandel(char *name);
				 HRESULT        SetMatrix(char* MatrixName, D3DXMATRIX	&Matrix);
				 HRESULT        SetBool(char* Name, bool b);
				 HRESULT        SetInt(char* Name, int b);
				 HRESULT        SetFloat(char* Name, float b);
				 HRESULT        SetVectorArray(char* Name,CONST D3DXVECTOR4* pVector, UINT Count);
                 HRESULT        SetIntArray( char* Name,CONST INT* pn, UINT Count) ; 
				 HRESULT        GetConstantDesc(D3DXHANDLE hConstant, D3DXCONSTANT_DESC *pConstantDesc, UINT *pCount);
				 virtual void IniPs();
				 virtual const char* GetPsName() { return "";}
	  protected:
			    
			     LPDIRECT3DPIXELSHADER9	  m_pPixelShader;
			     SRenderFactory_D3D9*     m_pRenderFactory;
				 LPD3DXCONSTANTTABLE      m_pConstantTable;
				 LPD3DXBUFFER             m_pPsCode;
	};

	/*******yuv着色器******/
	class CYUVPixelShader_D3D9:public CPixelShader_D3D9
	{
	     public:
				   CYUVPixelShader_D3D9(SRenderFactory_D3D9* pRenderFactory);
				   ~CYUVPixelShader_D3D9(); 
				   void IniPs();
				   void  ReLoadRes();
				   const char* GetPsName(){return "yuvps";}
	};

	//亮度，明亮度调节
	class CARGBPixelShader_D3D9:public CPixelShader_D3D9
	{
	     public:
				   CARGBPixelShader_D3D9(SRenderFactory_D3D9* pRenderFactory);
				   ~CARGBPixelShader_D3D9(); 
				   void IniPs();
				   void  ReLoadRes();
			       const char* GetPsName(){return "argbps";}
	};
	//通用着色器
	class CCommPixelShader_D3D9:public CPixelShader_D3D9
	{
	     public:
				   CCommPixelShader_D3D9(SRenderFactory_D3D9* pRenderFactory,const DWORD*  m_psCode,const char* psName );
				   ~CCommPixelShader_D3D9(); 
				   void IniPs();
				   void  ReLoadRes();
				   const char* GetPsName();
	     private:
			       const DWORD*  m_pPsCode;
				   std::string   m_strPsName;
			      
	};


}

#endif