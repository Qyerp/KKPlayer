#ifndef KKUI_IRender_H_
#define KKUI_IRender_H_
#include "IObjRef.h"
#include "IkkDraw.h"
#include "RenBase.h"
#include <string>
namespace KKUI
{

	enum OBJTYPE
    {
        OT_NULL=0,
		OT_FONT,
        OT_BITMAP,
		OT_PICYUV420P,
		OT_RenderTarget,
		OT_Other,
    };

    enum EXPEND_MODE
	{
		EM_NULL=0,      /*<不变*/
		EM_STRETCH,     /*<拉伸*/
		EM_TILE,        /*<平铺*/
	};


	enum FilterLevel {
		kNone_FilterLevel = 0,
		kLow_FilterLevel,
		kMedium_FilterLevel,
		kHigh_FilterLevel
	};

	enum EMaskType
	{   
		CircleMask,
		SquareMask,
		TriangleMask,
	};
	

	
    enum EMagnetState
	{
	    EMS_NO = 0,
		EMS_LEFT_TOP,
		EMS_RIGHT_TOP,
		EMS_LEFT_BOTTOM,
		EMS_RIGHT_BOTTOM,
	};
	//磁吸四个点的状态
	typedef struct SMagnetRegion
	{
	   EMagnetState LeftTop;
	   EMagnetState RightTop;

	   EMagnetState LeftBottom;
	   EMagnetState RightBottom;

	}SMagnetRegion;

	typedef struct SMaskInfo
	{
	    SUVInfo* uvinfo;
		int      count;
		int      masktype;
		int      hidepx;
	}SMaskInfo;

	//顶点坐标及UV信息
	typedef struct SVertInfo
	{
	    float    x;
		float    y;
		float    z;
		SUVInfo  uv;
	} SVertInfo;

	//调试板
	typedef struct SPalette
	{
	   int Alpha;  
	   int R;
	   int G;
	   int B;
	}SPalette;


	

	 /**
    * @struct     IRenderObj
    * @brief      渲染对象基类
    * 
    * Describe    所有渲染对象全部使用引用计数管理生命周期
    */
    class IRenderObj : public IObjRef 
    {
		public:
			    virtual const OBJTYPE ObjectType() const = 0;

				//由于dx可能丢失设备，这里用来清理资源
				virtual void  Release2() = 0;
				//重新加载资源
				virtual void  ReLoadRes() = 0;
    };

	class  IFont : public IRenderObj
    {
	   public:
		       virtual int TextSize()=0;
			   virtual const OBJTYPE ObjectType() const
			{
				return OT_FONT;
			}
	};
	//位图
    class IBitmap: public IRenderObj
    {
		public:
			virtual const OBJTYPE ObjectType() const
			{
				return OT_BITMAP;
			}
			virtual kkSize    Size() const =0;
			virtual int       Width() = 0;
			virtual int       Height() = 0;
			virtual int       LoadFromFile(const char*FileName)=0;
            virtual int	      LoadFromMemory(const void* pBuf,int szLen) = 0;
	};

	//创建一个YUV420p
	class IPicYuv420p: public IRenderObj
    {
		public:
			virtual const OBJTYPE ObjectType() const
			{
				return OT_PICYUV420P;
			}
			virtual kkSize    Size() const =0;
		    //加载YUV数据
            virtual int	      Load(const kkPicInfo* data) = 0;
			//设置引用
			virtual void      SetQuoteObj(IPicYuv420p*  pParentObj) = 0;

			//将当前移到上一帧
			virtual void      SavePreFrame(bool save) = 0;
			virtual int       HasPreFrame() = 0;
			//
			virtual void      ClearPicData(unsigned int Color) = 0;
			//创建图片缓冲
			virtual void      CreatePicCache(int width,int height,int format) = 0;
	};
	
	//渲染引擎接口
    class IRenderTarget: public IRenderObj
	{
	   public:
				 virtual const OBJTYPE ObjectType() const{return OT_RenderTarget;}

				 virtual void  ReSize(int nWidth, int nHeight) = 0;
				 virtual int   GetWidth()  = 0;
				 virtual int   GetHeight() = 0;
				 //绘制位图
				 virtual int   DrawBitmap(kkRect* pRcDest,IBitmap *pBitmap,int xSrc,int ySrc,int cx,int cy,unsigned char byAlpha=0xFF) = 0;

				 virtual int   DrawBitmapEx(kkRect*  pRcDest,IBitmap *pBitmap,kkRect* pRcSrc,unsigned int expendMode,unsigned char byAlpha=0xFF/**/ ) = 0 ;
				 //绘制九宫格
				 virtual int   DrawBitmap9Patch(kkRect* pRcDest,IBitmap *pBitmap,kkRect* pRcSrc,kkRect* pRcSourMargin,unsigned expendMode,unsigned char byAlpha=0xFF) = 0;
				 
				 //绘制YUV, VertInfo按照顺时针排，从矩形的左上角开始排列。SPalette:调色板
				 virtual int   DrawYuv420p(IPicYuv420p *pYuv420p,SinkFilter*Filter,const SPalette& Palette,SVertInfo* VertInfo,int VertCount,int UseLast=0,SMaskInfo* maskinfo=0,int maskcount=0) = 0;
				 
				 //滤镜处理
				 virtual int   FilterYuv420p(IPicYuv420p *pYuv420p,SinkFilter* Filter) = 0;
				
				 //绘制线段
				 virtual void  DrawLine (int startx,int starty, int endx,int endy, unsigned int color) =0;
				 virtual void  DrawLines(kkPoint* pts,int count, unsigned int color) =0;
				 virtual void  DrawLines(SUVInfo* pts,int count, unsigned int color) =0;

				 virtual void  DrawText(IFont* pFont,wchar_t *text,kkRect* pRcDest,unsigned int color=0xFFFFFFFF) =0;
				 virtual void  MeasureText(IFont* pFont,wchar_t *text,kkSize *pSz ) = 0;

				  
				 virtual  void  DrawTextV(IFont* pFont,wchar_t *text,kkRect* pRcDest,unsigned int color=0xFFFFFFFF)=0;
				 virtual  void  MeasureTextV(IFont* pFont,wchar_t *text,kkSize *pSz )=0;

				 //绘制矩形
				 virtual void  Rectangle(kkRect* rt,unsigned int cr) = 0;
				 virtual void  RectangleByPt(kkPoint* pt,int width,int height,unsigned int cr) = 0;

				 virtual int   FillRect(kkRect* rt,unsigned int cr)=0;
				 virtual void  FillRect(kkRect* rt,unsigned int cr1,unsigned int cr2,unsigned int cr3,unsigned int cr4) = 0 ;
				 virtual void  FillRect(SUVInfo* pts1,SUVInfo* pts2,SUVInfo* pts3,SUVInfo* pts4,unsigned int cr1,unsigned int cr2,unsigned int cr3,unsigned int cr4) = 0 ;
                 virtual void  FillRectByPt(kkPoint* pt,int width,int height,unsigned int cr) = 0;

				 virtual void  DrawCircle(int xCenter, int yCenter, int nRadius,unsigned int cr,int fillmodel=0)= 0;
   

				 //是否启用变形
				 virtual void  SetEnableWarp(void* IWarp) = 0;
				 virtual int   GetEnableWarp() = 0;
                 //当有变形是通过这个来绘制
				 virtual void  DrawWarp(SVertInfo* VertInfo,int VertCount) = 0;
				 virtual void  SetFusionzone(void* Fusionzone) = 0;
				 virtual void  SetEnableFusionzone(int Fusionzone) = 0;
	};
	//其它杂物
    class IRenderOthert: public IRenderObj
	{
	    public:
		   virtual const OBJTYPE ObjectType() const{return OT_Other;}
	};

	
	class  IRenderFactory : public IObjRef
    {
	   public:
		        virtual bool   init() = 0;
				virtual int    CreateRenderTarget(IRenderTarget ** ppRenderTarget,void* Handle,int nWid,int nHei) =0 ;
				virtual int    CreateBitmap(IBitmap ** ppBitmap) = 0 ;
				virtual int    CreateYUV420p(IPicYuv420p ** ppPicYuv420p) = 0 ;

				virtual int    BeforePaint(IRenderTarget* pRenderTarget) = 0;
				virtual void   EndPaint(IRenderTarget* pRenderTarget,bool WarpNet=0) = 0;
				virtual void   CreateFont(IFont** ppFont,int FontSize,wchar_t* FontName) = 0;
				virtual void   Lock() = 0;
				virtual void   UnLock() = 0;
				//与KKPlayer 对应
				virtual void*  CreateVideoDecoder(int CodecId,int Width,int Height) = 0;
				virtual void   DeleteVideoDecoder(void* pDXVA2VD) = 0;

				virtual void*  CreateDxTexture() = 0;
				virtual void   DeleteDxTexturer(void* DxTexture) = 0;

	};

}
#endif