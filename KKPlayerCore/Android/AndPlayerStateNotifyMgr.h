#ifndef AndPlayerStateNotifyMgr_H_
#define AndPlayerStateNotifyMgr_H_
#include <map>
#include <queue>
#include <android/log.h>
#include <pthread.h>
#include <unistd.h>
#include <jni.h>
#include "../stdafx.h"
#include "../KKLock.h"

///播放器状态通知管理器
typedef std::pair<void*,int> playerstate;
typedef struct SJavaObjInfo
{
	jobject    JCall;
	jclass     Obj_class;
	jobject    JavaObj;
	jmethodID  OnOMSNotifyId;
	SJavaObjInfo()
	{
		JavaObj = 0;
		JCall = 0;
	    Obj_class = 0;
	    OnOMSNotifyId=0;
	}
}SJavaObjInfo;
class DLL_KK_LOCAL CAndPlayerStateNotifyMgr
{
public:
	   CAndPlayerStateNotifyMgr();
	   ~CAndPlayerStateNotifyMgr();
	   void PutAState(void* player,int state);
	   void SetPlayerNotify(JNIEnv *env,jobject JavaObj,void* obj,jobject notify);
       void RemovePlayerNotify(JNIEnv *env, void* obj);
	   void Start();
	   void Stop();
private:
	 static unsigned          handleState_thread(LPVOID lpParameter);
	 void                     handleState();
	 pthread_t                m_thTid_task;
     int                      m_nAddr;
	 int                      m_nThOver;
	 std::map<void*,SJavaObjInfo>    m_PlayerNotifyMap;
	 std::queue<playerstate>  m_playerstateQue;
	 CKKLock                  m_Lock;
};
#endif