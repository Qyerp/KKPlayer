/**************************kkplayer*********************************************/
/*******************Copyright (c) Saint ******************/
/******************Author: Saint *********************/
/*******************Author e-mail: lssaint@163.com ******************/
/*******************Author qq: 784200679 ******************/
/*******************KKPlayer  WWW: http://www.70ic.com/KKplayer ********************************/
/*************************date��2015-6-25**********************************************/
#include "stdafx.h"
#include "RendLock.h"
#include <assert.h>
/***���ֵ***/
 CRendLock::CRendLock()
 {
	 m_crisec = new  CRITICAL_SECTION ();
	 InitializeCriticalSection((CRITICAL_SECTION*)m_crisec);
 }
CRendLock::~CRendLock()
{
   ::DeleteCriticalSection((CRITICAL_SECTION*)m_crisec); 
}
void CRendLock::Lock()
{
#ifdef DEBUG
	    DWORD threadId = ::GetCurrentThreadId();
		HANDLE h=(HANDLE)threadId ;
		CRITICAL_SECTION* xx=(CRITICAL_SECTION*)m_crisec;
		if (h != xx->OwningThread)
			::EnterCriticalSection(xx);
		else
			assert(0);
#else
             CRITICAL_SECTION* xx=(CRITICAL_SECTION*)m_crisec;
			::EnterCriticalSection(xx);
#endif
}
void CRendLock::Unlock()
{
	CRITICAL_SECTION* xx = (CRITICAL_SECTION*)m_crisec;
   ::LeaveCriticalSection(xx);
}
int CRendLock::TryLock()
{
	return ::TryEnterCriticalSection((CRITICAL_SECTION*)m_crisec);
}