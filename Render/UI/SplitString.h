﻿#ifndef  KKUI_SplitString_H_
#define  KKUI_SplitString_H_
#include <vector>
#include <TString.h>
namespace KKUI
{
    template<class T,class TC>
	int SplitString(const T &str, TC cSep ,std::vector<T> & strLst)
    {
        int nBegin=0;
        int nEnd=0;
        while(nEnd!=str.GetLength())
        {
            if(str[nEnd]==cSep)
            {
                if(nEnd>nBegin)
                {
                    strLst.push_back(str.Mid(nBegin,nEnd-nBegin));
                }
                nBegin=nEnd+1;
            }
            nEnd++;
        }
        if(nEnd>nBegin)
        {
            strLst.push_back(str.Mid(nBegin,nEnd-nBegin));
        }
        return strLst.size();
    }

	typedef std::vector<SStringA> SStringAList;
    template int SplitString<SStringA,char>(const SStringA & str,char cSep, SStringAList & strLst);

	inline int ParseResID(const SStringA & str,SStringAList & strLst)
    {
        int nPos = str.Find(':');
        if(nPos==-1)
        {
            strLst.push_back(str);
        }else
        {
            strLst.push_back(str.Left(nPos));
            strLst.push_back(str.Right(str.GetLength()-nPos-1));
        }
		return (int)strLst.size();
    }
}
#endif