#ifndef DX9_Text_H_
#define DX9_Text_H_
extern "C"
{
   #include "libavformat/avformat.h"
}
#include <set>
#include <string>
#include <d3d9.h>
#include <initguid.h>
#include <dxva2api.h>
#include "ObjRef.h"
#include "Core/ObjectImp.h"
#include "Core/IRender.h"
#include "libavcodec/dxva2.h"
#include "../../KKPlayerCore/render/render.h"

namespace KKUI
{
	 class SRenderFactory_D3D9;

	 struct DX9TextureInfo
	{
			int index;
			bool used;
			LPDIRECT3DTEXTURE9 d3d;
			uint64_t           usedCount;
			int                format;
     };
	 class CDX9Texture: public IDXTexture, public TObjRefImpl<IRenderOthert>
	 {
		     public:
				    CDX9Texture(SRenderFactory_D3D9*  pRenderFactory);
				    ~CDX9Texture();

					 void     Release2();
				     void     ReLoadRes();
			/**************IDXTexture***********/
	        public:
					int    GetTextureIndex(void *ffFrame,int format,int Width,int Height);
		            int    ReleaseTexture(void *Texture);
		   
		            //检查设备是否丢失
		            int    GetLostDevice();
		            //Surface 是否有效
		            int    IsTextureValid(void *Texture,int SurfaceId,int &Outformat);
					void   SetCurFormat(int ff);
		            int    GetCurFormat();
					void*  Decode(void *Texture,int index,int Width,int Height,FpDecompress fp,void *userctx);
	        private:
				    IDirect3DTexture9*             CreateTexture(int format,int Width,int Height);
		            SRenderFactory_D3D9*           m_pRenderFactory;
					DX9TextureInfo                 m_DX9TextureInfos[64];
					int                            m_nTextureCount;
					int                            m_nTextureOrder;
					int                            m_nCurformat;
	 };
}
#endif