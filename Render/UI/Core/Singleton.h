﻿#ifndef Singleton_H_
#define Singleton_H_
#include <assert.h>

namespace KKUI
{

    /**
    * @class      Singleton
    * @brief      单模板
    * 
    * Describe    
    */
    template <typename T> 
    class CSingleton
    {
    protected:
        static    T* m_Singleton;

    public:
        CSingleton( void )
        {
            assert( !m_Singleton );
            m_Singleton = static_cast<T*>(this);
        }
        virtual ~CSingleton()
        {
            assert( m_Singleton );
            m_Singleton = 0;
        }
        static T& getSingleton( void )
        {
            assert( m_Singleton );
            return ( *m_Singleton );
        }
        static T* getSingletonPtr( void )
        {
            return ( m_Singleton );
        }

    private:
        CSingleton& operator=(const CSingleton&)
        {
            return *this;
        }
        CSingleton(const CSingleton&) {
		
		}
    };

}
#endif