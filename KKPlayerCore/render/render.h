
#ifndef RENDER_H
#define RENDER_H
#include "../stdafx.h"
#include "../KKPlayerInfo.h"
#include "IkkDraw.h"
#ifndef  LOBYTE
#define LOBYTE(w)           ((unsigned char)(((unsigned long)(w)) & 0xff))
#endif
#ifndef GetRValue
#define GetRValue(rgb)      (LOBYTE(rgb))
#define GetGValue(rgb)      (LOBYTE(((unsigned short)(rgb)) >> 8))
#define GetBValue(rgb)      (LOBYTE((rgb)>>16))
#endif


typedef void (*fpRenderImgCall) (kkAVPicInfo* picinfo,void* UserData);

typedef void (*FpRenderLock)  ();
typedef void (*FpRenderUnLock)();
//有些视频是无损压缩为了，提高速度，进行与解压
typedef void (*FnPrepareDecompress)(void *avctx, void **avpkt);
/*******
State = 0  需要释放资源
State = 1  重置失败
State = 2  重置成功
*****/
typedef void (*fpResetDevCall) (void* UserData,int State);
/*******DXVA2解码环境***********/
class IDXVA2Ctx
{
    public:
		   virtual void*  GetDxvaContext() = 0;
		   virtual int    GetPixFmt() = 0;
		   virtual int    GetSurfaceIndex(void *ffFrame) = 0;
		   virtual int    ReleaseSurface(void *Surface,void*decoder) = 0;
		   virtual void*  GetDecoder() = 0;
		   //检查设备是否丢失
		   virtual int    GetLostDevice() = 0;
		   //Surface 是否有效
		   virtual int    IsSurfaceValid(void *Surface,int SurfaceId) = 0;
};
typedef IDXVA2Ctx* (*FpCreateDxva2Ctx)  (int CodecId,int Width,int Height);
typedef void (*FpFreeDxva2Ctx)(IDXVA2Ctx* ctx);

/**********用于DXV和Hap解码**************/
typedef void (*FpDecompress)(int nRowBytes,int nNumRows,char* buf,void *userctx);
class IDXTexture
{
  public:
		   virtual int    GetTextureIndex(void *ffFrame,int format,int Width,int Height) = 0;
		   virtual int    ReleaseTexture(void *Texture) = 0;
		   
		   //检查设备是否丢失
		   virtual int    GetLostDevice() = 0;
		   //Surface 是否有效
		   virtual int    IsTextureValid(void *Texture,int SurfaceId, int &Outformat) = 0;
		   virtual void*  Decode(void *Texture,int index,int Width,int Height,FpDecompress fp,void *userctx) = 0;
		   virtual void   SetCurFormat(int ff) = 0;
		   virtual int   GetCurFormat() = 0;
};
//
typedef IDXTexture* (*FpCreateDXTexture) ();
typedef void (*FpFreeDXTexture)(IDXTexture* ctx);
enum ERenderType{
        GDI_RENDER,
        D3D9_RENDER,
		D3D11_RENDER,
		OpenGL_RENDER,
        RAW_RENDER,
};
class IkkRender
{
public:
   
    virtual bool init(HWND hView) = 0;
    virtual void destroy() = 0;
    virtual void resize(unsigned int w, unsigned int h) = 0;
    //virtual void WinSize(unsigned int w, unsigned int h) = 0;

	virtual void render(kkAVPicInfo *Picinfo,bool wait)=0;
	//呈现背景图片
	virtual void renderBk(unsigned char* buf,int len)=0;
	virtual void SetWaitPic(unsigned char* buf,int len)=0;

	virtual void LoadCenterLogo(unsigned char* buf,int len)=0;

	virtual void SetErrPic(unsigned char* buf,int len)=0;
	virtual void ShowErrPic(bool show)=0;
	virtual void FillRect(kkBitmap img,kkRect rt,unsigned int color)=0;
    virtual ERenderType GetRenderType() = 0;
	//设置错误提示
	virtual void SetLeftPicStr(const char *str)=0;
	virtual void SetRenderImgCall(fpRenderImgCall fp,void* UserData)=0;

	///用于获取d3d信息
	virtual bool GetHardInfo(void** pd3d,void** pd3ddev,int *ver)=0;
	virtual void SetResetHardInfoCall(fpResetDevCall call,void* UserData)=0;
    virtual long long GetOnSizeTick()=0;
	virtual void RetSetSizeTick()=0;
	virtual void renderLock()=0;
	virtual void renderUnLock()=0;
#ifdef WIN32
    virtual IDXVA2Ctx* CreateDxva2Ctx(int CodecId,int Width,int Height) = 0;
    virtual void       FreeDxva2Ctx(IDXVA2Ctx* ctx) = 0;

	virtual IDXTexture* CReateDXTexture() = 0;
	virtual void       FreeDXTexture(IDXTexture* ctx) = 0;
#endif
};

#endif