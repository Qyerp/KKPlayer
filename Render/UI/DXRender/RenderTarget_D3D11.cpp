
#include <wchar.h>
#include <stdio.h>
#include <assert.h>
#include <string>



#include <core\SkCanvas.h>
#include <core\SkBitmap.h>
#include <core\SkTypeface.h>
#include <core\SkImageDecoder.h>
#include <core\SkStream.h>
#include <vector>
#include "RenderTarget_D3D11.h"
#include "GetResource.h"
#include "D3d9Sharder.h"
#include "Core/Warp/IWarp.h"
#include "../Core/Fusionzone/IFusionzone.h"
#include "Core/UIMoveState.h"
#include "Yuv420p_D3D11.h"
#include "Dx11Shader.h"
#ifndef SAFE_RELEASE
#define SAFE_RELEASE(p)      { if (p) { (p)->Release(); (p)=NULL; } }
#endif

//#pragma comment(lib,"D3DX11.lib")
///向量数据
	static  HMODULE hModD3D11   = NULL;
    typedef HRESULT  (WINAPI *D3D11_CreateDevice)(__in_opt IDXGIAdapter* pAdapter,
		D3D_DRIVER_TYPE DriverType,
		HMODULE Software,
		UINT Flags,__in_ecount_opt( FeatureLevels ) CONST D3D_FEATURE_LEVEL* pFeatureLevels,
		UINT FeatureLevels,
		UINT SDKVersion,
		__out_opt ID3D11Device** ppDevice,__out_opt D3D_FEATURE_LEVEL* pFeatureLevel,
		__out_opt ID3D11DeviceContext** ppImmediateContext );

	D3D11_CreateDevice pfnDirect3DCreate11=0;

inline static void GetARGB(unsigned int crValue, unsigned char &r, unsigned char &g, unsigned char &b, unsigned char& a)
	{
		a = (crValue & 0xFF000000) >> 24;
		r = (crValue & 0x00FF0000) >> 16;
		g = (crValue & 0x0000FF00) >> 8;
		b = (crValue & 0x000000FF);
	}
 void GetfColor(Float4& color,unsigned int cr)
   {
      unsigned char r, g, b, a;
      GetARGB(cr,r, g, b, a);

	 
      int fr, fg, fb, fa;

	  color.x = (float)(r / 255.0f);
      color.y =(float)(g / 255.0f);
      color.z = (float)(b / 255.0f);
      color.w = (float)(a / 255.0f);
   }
namespace KKUI
{
	


	SRenderFactory_D3D11::SRenderFactory_D3D11():
		m_pDevice(0),
		m_pDeviceContext(0),
		m_pDefPixelShader(0),
		m_pDefVertexShader(0),
		m_pDefSamplerState(0)
	{
	   
	}
	SRenderFactory_D3D11::~SRenderFactory_D3D11()
	{
	
	    SAFE_RELEASE(m_pDevice);
		SAFE_RELEASE(m_pDeviceContext);
	}
	ID3D11Texture2D*     SRenderFactory_D3D11::GetFromFileInMemory(unsigned char* data,int len)
	{
		return 0;
	}
	void SRenderFactory_D3D11::SaveFile(wchar_t *Path,ID3D11Texture2D* textrue)
	{
	
	}
	
	bool SRenderFactory_D3D11::init()
	{
	    if(hModD3D11==0)
			 hModD3D11 = LoadLibraryA("d3d11.dll");
		
		if (hModD3D11)
		{
			pfnDirect3DCreate11 = (D3D11_CreateDevice)GetProcAddress(hModD3D11, "D3D11CreateDevice");
		}else{
		
		}
		HMODULE hDXGIMod = LoadLibraryA("dxgi.dll");

		typedef HRESULT(WINAPI *PFN_CREATE_DXGI_FACTORY)(REFIID riid, void **ppFactory);
		PFN_CREATE_DXGI_FACTORY CreateDXGIFactoryFunc;
		CreateDXGIFactoryFunc = (PFN_CREATE_DXGI_FACTORY)GetProcAddress(hDXGIMod, "CreateDXGIFactory");
		if (!CreateDXGIFactoryFunc) {
			
		}
	    GUID __IID_IDXGIFactory = { 0x50c83a1c, 0xe072, 0x4c48,{ 0x87, 0xb0, 0x36, 0x30, 0xfa, 0x36, 0xa6, 0xd0 } };
		IDXGIFactory *dxgiFactory;
		HRESULT result = CreateDXGIFactoryFunc(__IID_IDXGIFactory, (void **)&dxgiFactory);
		if (FAILED(result)) {

		}

		IDXGIAdapter  *dxgiAdapter;
		result = dxgiFactory->EnumAdapters( 0, &dxgiAdapter);

		UINT createDeviceFlags = D3D11_CREATE_DEVICE_BGRA_SUPPORT;
#ifdef _DEBUG
        createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif
		
		//驱动类型数组
		D3D_DRIVER_TYPE driverTypes[] = 
		{
			D3D_DRIVER_TYPE_HARDWARE,
			D3D_DRIVER_TYPE_WARP,
			D3D_DRIVER_TYPE_REFERENCE
		};
		UINT numDriverTypes = ARRAYSIZE(driverTypes);

		//特征级别数组
		D3D_FEATURE_LEVEL featureLevels[] =
		{
			D3D_FEATURE_LEVEL_11_0,
			D3D_FEATURE_LEVEL_10_1,
			D3D_FEATURE_LEVEL_10_0,
			D3D_FEATURE_LEVEL_9_3,
			D3D_FEATURE_LEVEL_9_2,
			D3D_FEATURE_LEVEL_9_1
		};

		UINT numFeatureLevels = ARRAYSIZE(featureLevels);
        D3D_FEATURE_LEVEL featureLevel = D3D_FEATURE_LEVEL_11_0;            //特征等级  
		HRESULT hResult =S_OK;
		      
					hResult = pfnDirect3DCreate11
						(dxgiAdapter,
							D3D_DRIVER_TYPE_UNKNOWN,
					NULL,
					createDeviceFlags,
					featureLevels,
					numFeatureLevels ,
					D3D11_SDK_VERSION,   
					&m_pDevice,
					&featureLevel,
                    &m_pDeviceContext);

		


		const D3D11_INPUT_ELEMENT_DESC vertexDesc[] = 
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 20, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		};
	    

		/* Load in SDL's one and only vertex shader: */
	   hResult  = m_pDevice->CreateVertexShader(
			D3D11_VertexShader,
			sizeof(D3D11_VertexShader),
			NULL,
			&m_pDefVertexShader
			);
   
	   //颜色着色器
	   m_pDevice->CreatePixelShader(
        D3D11_PixelShader_Colors,
        sizeof(D3D11_PixelShader_Colors),
        NULL,
        &m_pColorPixelShader
        );

	   m_pDevice->CreatePixelShader(
		   D3D11_PixelShader_Textures,
		   sizeof(D3D11_PixelShader_Textures),
		   NULL,
		   &m_pTextPixelShader
	   );
	   

	   m_pDefYuvPixelShader = new CYUVPixelShader_D3D11(this);
	   m_pDefYuvPixelShader->IniPs();
	   

	  

	   hResult  =    m_pDevice->CreateInputLayout(
        vertexDesc,
        ARRAYSIZE(vertexDesc),
        D3D11_VertexShader,
        sizeof(D3D11_VertexShader),
        &m_pDefInputLayout
        );


	   D3D11_BUFFER_DESC constantBufferDesc;
	   memset(&constantBufferDesc,0,sizeof(constantBufferDesc));
       constantBufferDesc.ByteWidth = sizeof(VertexShaderConstants);
       constantBufferDesc.Usage = D3D11_USAGE_DEFAULT;
       constantBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
        hResult = m_pDevice->CreateBuffer(
        &constantBufferDesc,
        NULL,
        &m_pDefVsShaderConstants
        );


		D3D11_SAMPLER_DESC sampDesc;
		ZeroMemory(&sampDesc, sizeof(sampDesc));
		sampDesc.Filter =   D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
		sampDesc.MinLOD = 0;
		sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
		m_pDevice->CreateSamplerState(&sampDesc, &m_pDefSamplerState);

	
		D3D11_RASTERIZER_DESC rasterDesc;
		rasterDesc.AntialiasedLineEnable = false;
		rasterDesc.CullMode = D3D11_CULL_NONE; //背面剔除
		rasterDesc.DepthBias = 0;
		rasterDesc.DepthBiasClamp = 0.0f;
		rasterDesc.DepthClipEnable = TRUE; //深度裁剪开启
		rasterDesc.FillMode = D3D11_FILL_SOLID; //实体渲染
		rasterDesc.FrontCounterClockwise = false; //顺时针
		rasterDesc.MultisampleEnable = false;
		rasterDesc.ScissorEnable = false;
		rasterDesc.SlopeScaledDepthBias = 0.0f;

		m_pDevice->CreateRasterizerState(&rasterDesc, &m_pRasterizerState);
		m_pDeviceContext->RSSetState(m_pRasterizerState);

		return 0;
	}

    CPixelShader_D3D11*   SRenderFactory_D3D11::GetPsByName(char* Name)
	{
		std::map<std::string ,CPixelShader_D3D11*>::iterator ItPs =  m_IPsObjMap.find(Name);
        if(ItPs!= m_IPsObjMap.end()){
					return ItPs->second;
		}
        return 0;
   }
   void   SRenderFactory_D3D11::CopyToTex(ID3D11Texture2D* text1,int Width,int Height,ID3D11Texture2D* text2,char* PsName)
   {
          
   }
   int    SRenderFactory_D3D11::InitShader()
   {

	   
        return 0;
   }
	int  SRenderFactory_D3D11::CreateRenderTarget(IRenderTarget ** ppRenderTarget,void* Handle,int nWid,int nHei)
	{
		CRendGurd gd(m_lock);

		CRenderTarget_D3D11* re = new CRenderTarget_D3D11(this,(HWND)Handle,nWid,nHei);
		*ppRenderTarget = re;
		m_IRenderObjSet.insert((IRenderObj*)re);

		
	    return 0;
	}


	void         SRenderFactory_D3D11::RemoveObj(IRenderObj* obj,int lock)
	{
		if(lock)
		  Lock();
		std::set<IRenderObj*>::iterator It= m_IRenderObjSet.find(obj);
		if (It != m_IRenderObjSet.end())
		{
			IRenderObj* xxx = *It;
			m_IRenderObjSet.erase(It);
		}
			
		if(lock)
		UnLock();
	}

bool  SRenderFactory_D3D11::LostDevice()
{
	
 
    return false;
}

bool     SRenderFactory_D3D11::LostDeviceRestore()
{
				return true;
}
    int    SRenderFactory_D3D11::CreateBitmap(IBitmap ** ppBitmap)
    {
		CRendGurd gd( m_lock);
		CBitmap_D3D11 *p = new CBitmap_D3D11(this);
        *ppBitmap = p;
		m_IRenderObjSet.insert(p);
       return 0;
    }

	int    SRenderFactory_D3D11::CreateYUV420p(IPicYuv420p ** ppBitmap)
	{
		   CRendGurd gd( m_lock);
             
		   CYuv420p_D3D11* yuv = new  CYuv420p_D3D11(this);
		    *ppBitmap=yuv;
		    m_IRenderObjSet.insert(yuv);
	        return 1;
	}
	int   SRenderFactory_D3D11::BeforePaint(IRenderTarget* pRenderTarget)
	{
	     HRESULT hr=0;
		 
		CRenderTarget_D3D11* pRenderTarget11=(CRenderTarget_D3D11*)pRenderTarget;

		 
		D3D11_VIEWPORT viewport={};
		Float4X4 projection= MatrixIdentity();
        Float4X4       view={};
		view.m[0][0] = 2.0f / pRenderTarget11->GetWidth();
		view.m[0][1] = 0.0f;
		view.m[0][2] = 0.0f;
		view.m[0][3] = 0.0f;
		view.m[1][0] = 0.0f;
		view.m[1][1] = -2.0f / pRenderTarget11->GetHeight();
		view.m[1][2] = 0.0f;
		view.m[1][3] = 0.0f;
		view.m[2][0] = 0.0f;
		view.m[2][1] = 0.0f;
		view.m[2][2] = 1.0f;
		view.m[2][3] = 0.0f;
		view.m[3][0] = -1.0f;
		view.m[3][1] = 1.0f;
		view.m[3][2] = 0.0f;
		view.m[3][3] = 1.0f;

		m_VsShaderConstantsData.model = MatrixIdentity();
		m_VsShaderConstantsData.projectionAndView= MatrixMultiply(view,projection);
		viewport.TopLeftX = 0;
        viewport.TopLeftY = 0;
        viewport.Width = pRenderTarget11->GetWidth();
        viewport.Height = pRenderTarget11->GetHeight();
        viewport.MinDepth = 0.0f;
        viewport.MaxDepth = 1.0f;

		Lock();
		std::set<IRenderObj*>::iterator It = m_IRenderObjSet.begin();
		for(;It != m_IRenderObjSet.end();It++)
		{
			IRenderObj* pRenderObj = *It;
			if(pRenderObj->ObjectType()==OT_PICYUV420P)
			{
				((CYuv420p_D3D11*)pRenderObj)->RenderToTex();
			}
			
		}
		//m_pDeviceContext->ClearState();
		 ID3D11DepthStencilView*   pDSV= pRenderTarget11->GetDSV();

		ID3D11RenderTargetView* view22 = pRenderTarget11->GetRTView();

		//https://social.msdn.microsoft.com/Forums/onedrive/zh-CN/4d66737f-57d1-4423-a15a-16c346caf112?forum=winstoreappzhcn
		float ClearColor[4] = { 0.0f, 1.0f, 0.0f, 1.0f }; // red,green,blue,alpha
		m_pDeviceContext->ClearRenderTargetView(pRenderTarget11->GetRTView(), ClearColor);
		//清除深度
		//m_pDeviceContext->ClearDepthStencilView(pDSV, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

		m_pDeviceContext->OMSetRenderTargets(
			1,
			&view22,
			0//pDSV
		);
		m_pDeviceContext->OMSetBlendState(0, 0, 0xFFFFFFFF);
		m_pDeviceContext->RSSetViewports(1,&viewport);

		m_pDeviceContext->UpdateSubresource(
        (ID3D11Resource *)m_pDefVsShaderConstants,
        0,
        NULL,
        &m_VsShaderConstantsData,
        0,
        0
        );

       

     

		//顶点色器
		m_pDeviceContext->VSSetShader( m_pDefVertexShader ,NULL,0);
		m_pDeviceContext->VSSetConstantBuffers( 0, 1, &m_pDefVsShaderConstants);
		return 1;
		
	}
	void   SRenderFactory_D3D11::EndPaint(IRenderTarget* pRenderTarget,bool WarpNet)
	{

		
		CRenderTarget_D3D11* pRenderTarget11=(CRenderTarget_D3D11*)pRenderTarget;
		IDXGISwapChain*  pSwapChain= pRenderTarget11->GetSwapChain();
		pSwapChain->Present( 0, 0 );	
		UnLock();
		
	}
	void   SRenderFactory_D3D11::CreateFont(IFont** ppFont,int FontSize,wchar_t* FontName)
	{
		 
			CFont_D3D11* ff = new CFont_D3D11(this,FontSize,FontName);
			*ppFont=ff;
            m_IRenderObjSet.insert(ff);
	}
   
	void   SRenderFactory_D3D11:: Lock()
	{
		  m_lock.Lock();
	}
	void   SRenderFactory_D3D11::UnLock()
	{
		  m_lock.Unlock();
	}
    void*   SRenderFactory_D3D11::CreateVideoDecoder(int CodecId,int Width,int Height)
	{
	   return 0;
	}
    void    SRenderFactory_D3D11::DeleteVideoDecoder(void* pVD)
	{
	     
    }

	void*      SRenderFactory_D3D11::CreateDxTexture()
	{
	    
		 return 0;
	}
	void       SRenderFactory_D3D11::DeleteDxTexturer(void* DxTexture)
	{
		 
    }





	CFont_D3D11::CFont_D3D11(SRenderFactory_D3D11* pRenderFactory,int FontSize,wchar_t* FontName):
		m_nTextSize(FontSize),
		m_pRenderFactory(pRenderFactory)
	{
		memset(m_nFontName,0,128);
		if(FontName)
		    wcscpy( m_nFontName,FontName);
	         
		 m_pRenderFactory->AddRef();
		ReLoadRes();	   
					
	}
	CFont_D3D11::~CFont_D3D11()
	{
	   m_pRenderFactory->Release();
	   Release2();
	   m_pRenderFactory->RemoveObj(this);
	}
	void  CFont_D3D11::Release2()
	{
	   
	}
	void CFont_D3D11::ReLoadRes()
	{

		 
	}
	int  CFont_D3D11::TextSize()
	{
	   return 0;
	}

	 CBitmap_D3D11::CBitmap_D3D11(SRenderFactory_D3D11* pRenderFactory):
	        m_pTexture(0),
			m_pRenderFactory( pRenderFactory),
			m_pSrcBuffer(0),
			m_pSrcBufferLen(0)
	{
	   m_pRenderFactory->AddRef();
	   memset(&m_sz,0,sizeof(m_sz));
	}

	 CBitmap_D3D11::~CBitmap_D3D11()
	 {
	     m_pRenderFactory->Release();
		 Release2();
		 if(m_pSrcBuffer)
			free(m_pSrcBuffer);
		 m_pSrcBuffer =0;
		  m_pRenderFactory->RemoveObj(this);
	 }
	 void   CBitmap_D3D11::Release2()
	 {
	      SAFE_RELEASE(m_pTexture);
	 }
	 void   CBitmap_D3D11::ReLoadRes()
	 {
		   if(!m_pSrcBuffer)
			 return;

	       SAFE_RELEASE(m_pTexture);
	 }
	 kkSize    CBitmap_D3D11::Size() const
	 {
	    return m_sz;
	 }

     int       CBitmap_D3D11::Width()
	 {
		 return m_sz.cx;
	 }
	 int       CBitmap_D3D11::Height()
	 {
	     return m_sz.cy;
	 }
	
    int   CBitmap_D3D11::LoadFromFile(const char*FileName)
	{
		    SAFE_RELEASE(m_pTexture);
		
			return  0;

			
	}
    int	CBitmap_D3D11::LoadFromMemory(const void* pBuf,int szLen)
	{
	        SAFE_RELEASE(m_pTexture);

			return  0;
	}


   CRenderTarget_D3D11::CRenderTarget_D3D11( SRenderFactory_D3D11* pRenderFactory,HWND h,int nWidth, int nHeight): 
	    m_hView( h),
	    m_pRenderFactory(pRenderFactory), 
		m_nWidth(nWidth),
		m_nHeight(nHeight),
		m_pSwapChain(0),
		m_pRenderTexture(0),
		m_nWarp(0),
		m_pIWarp(0),
		m_pFusionzone(0),
		m_nEnableFusion(0),
		m_pRTView(0),
		m_pDSV(0),
		m_pVertexBuffer(0),
		m_nStride(),
		m_nOffset(0)
   {
	   m_pRenderFactory->AddRef();
       RecreateSwapChain();   
   }

  
   CRenderTarget_D3D11::~CRenderTarget_D3D11()
   {
	   Release2();
       m_pRenderFactory->Release();
	   m_pRenderFactory->RemoveObj(this);	  
   }
   void  CRenderTarget_D3D11::SetEnableWarp(void*  pIWarp)
   {

	   m_nWarp =pIWarp!=0 ? 1:0;
	   
	   m_pIWarp=pIWarp;
   }
   void  CRenderTarget_D3D11::SetFusionzone(void* Fusionzone)
   {
      m_pFusionzone=Fusionzone;
   }
   void  CRenderTarget_D3D11::Release2()
   {
	   std::map<unsigned long long,std::vector<SRender3DTexture11*> >::iterator It=m_BufferRenderMap.begin();
	   for(; It!=m_BufferRenderMap.end();It++)
	   {
		   std::vector<SRender3DTexture11*>::iterator Sit = It->second.begin();
		   for(; Sit!=It->second.end();Sit++)
		   {
			   SRender3DTexture11* ItTx=*Sit;
		       SAFE_RELEASE(ItTx->pTexture);
		   }
	   }
	  // m_BufferRenderMap.clear();

	   SAFE_RELEASE(m_pRenderTexture);
	   SAFE_RELEASE(m_pSwapChain);
  }
    void  CRenderTarget_D3D11::ReLoadRes()
	{
	      RecreateSwapChain(); 

		  std::map<unsigned long long,std::vector<SRender3DTexture11*> >::iterator It=m_BufferRenderMap.begin();
	     for(; It!=m_BufferRenderMap.end();It++)
	     {
		   std::vector<SRender3DTexture11*>::iterator Sit = It->second.begin();
		   for(; Sit!=It->second.end();Sit++)
		   {
			   SRender3DTexture11* tx=*Sit;
		   
			 

		   }
	     }

	}
   void   CRenderTarget_D3D11::ReSize(int nWidth, int nHeight)
   {
	   if( m_nWidth == nWidth && m_nHeight==nHeight)
		   return;
         m_nWidth = nWidth;
		 m_nHeight= nHeight;
		 m_pRenderFactory->Lock();
		 RecreateSwapChain(); 
		 m_pRenderFactory->UnLock();
   }
   void CRenderTarget_D3D11::RecreateSwapChain()
    {
        HRESULT hr= 0;
	    if(m_pRTView){
	        hr=m_pRTView->Release();
	 		m_pRTView=0;
	    }
      
		if(m_pDSV){
			hr = m_pDSV->Release();
			m_pDSV	=0;
		}
		
		if (m_pSwapChain) {
			hr = m_pSwapChain->Release();
			m_pSwapChain = NULL;
		}
			
		

         if(m_nWidth <=0 || m_nHeight <=0 )
		   return ;



	    ID3D11Device*  pDevice = m_pRenderFactory->GetDevice();
	    IDXGIDevice * pDXGIDevice = 0;
		 hr = pDevice->QueryInterface(__uuidof(IDXGIDevice), (void **)&pDXGIDevice);

		IDXGIAdapter * pDXGIAdapter = 0;
		hr = pDXGIDevice->GetAdapter( &pDXGIAdapter );

		IDXGIFactory * pIDXGIFactory = 0;
		pDXGIAdapter->GetParent(__uuidof(IDXGIFactory), (void **)&pIDXGIFactory);

		//交换链
		DXGI_SWAP_CHAIN_DESC sd;
		ZeroMemory(&sd, sizeof(DXGI_SWAP_CHAIN_DESC));//填充
		sd.BufferCount = 1;                              //我们只创建一个后缓冲（双缓冲）因此为1
		sd.BufferDesc.Width  =   m_nWidth;
		sd.BufferDesc.Height =   m_nHeight;
		sd.BufferDesc.Format =   DXGI_FORMAT_R8G8B8A8_UNORM;
		sd.BufferDesc.RefreshRate.Numerator = 60;
		sd.BufferDesc.RefreshRate.Denominator = 1;
		sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		sd.OutputWindow = m_hView;
		sd.SampleDesc.Count = 1;                      //1重采样
		sd.SampleDesc.Quality = 0;                      //采样等级
		sd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;      //常用参数
		sd.Windowed = TRUE;                              //是否全屏


		if(m_pSwapChain)
		{
		     m_pSwapChain->ResizeBuffers(1, m_nWidth, m_nHeight, DXGI_FORMAT_B8G8R8A8_UNORM, 0);
		}else{
		    //创建交换链接
		    hr =pIDXGIFactory->CreateSwapChain(pDevice,&sd,&m_pSwapChain);
		}
	
		
		ID3D11Texture2D* pBackBuffer = NULL;
		hr = m_pSwapChain->GetBuffer( 0, __uuidof( ID3D11Texture2D ), ( LPVOID* )&pBackBuffer );
		if( FAILED( hr ) )
			return ;

		//创建背后缓存视图
		hr = pDevice->CreateRenderTargetView( pBackBuffer, NULL, &m_pRTView);
		pBackBuffer->Release();
		if( FAILED( hr ) )
			return;

		
		D3D11_TEXTURE2D_DESC depthStencilDesc;
	    depthStencilDesc.Width = m_nWidth;
	    depthStencilDesc.Height = m_nHeight;
	    depthStencilDesc.MipLevels = 1;
	    depthStencilDesc.ArraySize = 1;
	    depthStencilDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
		depthStencilDesc.SampleDesc.Count = 1;
		depthStencilDesc.SampleDesc.Quality = 0;
	    depthStencilDesc.Usage = D3D11_USAGE_DEFAULT;
	    depthStencilDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	    depthStencilDesc.CPUAccessFlags = 0;
	    depthStencilDesc.MiscFlags = 0;

		ID3D11Texture2D* pDepthStencilBuffer;
	    // 创建深度缓冲区以及深度模板视图
	    hr = pDevice->CreateTexture2D(&depthStencilDesc, 0, &pDepthStencilBuffer);
	    hr = pDevice->CreateDepthStencilView(pDepthStencilBuffer, 0, &m_pDSV);
		hr = pDepthStencilBuffer->Release();


		
   }
  

   int  CRenderTarget_D3D11::DrawBitmap(kkRect* pRcDest,IBitmap *pBitmap,int xSrc,int ySrc,int cx,int cy,unsigned char byAlpha)
   {
	 
		 
		 return 1;
   }
   int  CRenderTarget_D3D11::DrawBitmapEx(kkRect*  pRcDest,IBitmap *pBitmap,kkRect* pRcSrc,unsigned int expendMode,unsigned char byAlpha/**/ )
   {
	   
        return S_OK;
   
   }
   int  CRenderTarget_D3D11::DrawBitmap9Patch(kkRect* pRcDest,IBitmap *pBitmap,kkRect* pRcSrc,kkRect* pRcSourMargin,unsigned expendMode,unsigned char byAlpha)
   {
     
        return S_OK;
   }
   
	 //当有变形是通过这个来绘制
    void  CRenderTarget_D3D11::DrawWarp(SVertInfo* VertInfo,int VertCount)
	{
	  
		
	}



	

	void  CRenderTarget_D3D11::OnFusionzone()
	{
		
	}
    int  CRenderTarget_D3D11::AdjustARGB(const SPalette& Palette,SMaskInfo* maskinfo,int maskcount)
	{
	     return 0;
	}

	//
	int   CRenderTarget_D3D11::DrawTexture(ID3D11Texture2D* pTex)
	{
		  return 0;
	}

	ID3D11Texture2D*    CRenderTarget_D3D11::FilterYuv420p2(IPicYuv420p *pYuv420p,SinkFilter* Filter)
	{
		 return 0;
	}

    int   CRenderTarget_D3D11::DrawYuv420p(IPicYuv420p *pYuv420p,SinkFilter*Filter,const SPalette& Palette,SVertInfo* VertInfo,int VertCount,int UseLast,SMaskInfo* maskinfo,int maskcount)
	{
		 
		 CYuv420p_D3D11* pYUV=(CYuv420p_D3D11*)pYuv420p; 
		 ID3D11ShaderResourceView* SRView =  pYUV->GetShaderView();
		
		 if(SRView){
		       VertexPositionColor* pVertices = new VertexPositionColor[VertCount];
			   for(int i=0;i<VertCount;i++)
			   {
					pVertices[i].pos.x =  VertInfo[i].x;
					pVertices[i].pos.y =  VertInfo[i].y;
					pVertices[i].pos.z = 0.0f;
					pVertices[i].tex.x = VertInfo[i].uv.x;
					pVertices[i].tex.y = VertInfo[i].uv.y;
					pVertices[i].color.w=1.0;
					pVertices[i].color.x=1.0;
					pVertices[i].color.y=1.0;
					pVertices[i].color.z=1.0;
			   }


			  // static ID3D11ShaderResourceView*  pShaderResourceView = 0;
			  ID3D11Device*  pDevice = m_pRenderFactory->GetDevice();
			 // if(!pShaderResourceView)
			  //    D3DX11CreateShaderResourceViewFromFile(pDevice , L"C:\\Program Files (x86)\\Microsoft DirectX SDK (June 2010)\\Samples\\C++\\Direct3D11\\Tutorials\\Tutorial07\\seafloor.dds", NULL, NULL, &pShaderResourceView, NULL);
			  ID3D11DeviceContext*   pDevContext= m_pRenderFactory->GetContext();
			  //更新顶点坐标及像素着色器
			  UpdateVertexBuffer(D3D11PS_TEXTURE,pVertices, sizeof(VertexPositionColor)*VertCount);

			  ID3D11SamplerState*    pSamplerState= m_pRenderFactory->GetDefSamplerState();
			  pDevContext->PSSetSamplers(0,1, &pSamplerState);

			  //ID3D11ShaderResourceView* SRViews[1] = { pShaderResourceView };// { SRView };
			  ID3D11ShaderResourceView* SRViews[1] = { SRView };
			  pDevContext->PSSetShaderResources(0, 1, SRViews);
			  pDevContext->IASetInputLayout(m_pRenderFactory->GetDefInputLayout());
			  pDevContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
			  pDevContext->Draw(VertCount,0);
			  pDevContext->PSSetShaderResources(0, 0, 0);
			   delete pVertices;
		 }
	   
	 

	  
		 return 1;
	}
    int   CRenderTarget_D3D11::FilterYuv420p(IPicYuv420p *pYuv420p,SinkFilter* Filter)
	{
		
		 
	     return 0;
	}
	

	int CRenderTarget_D3D11::UpdateVertexBuffer(D3D11PSType pstype, const void * vertexData, size_t dataSizeInBytes)
    {
    
		D3D11_BUFFER_DESC vertexBufferDesc={};
		HRESULT result = S_OK;
		D3D11_SUBRESOURCE_DATA vertexBufferData;
		

		if (m_pVertexBuffer) {
			m_pVertexBuffer->GetDesc(&vertexBufferDesc);
			//ID3D11Buffer_GetDesc(m_pVertexBuffer, &vertexBufferDesc);
		} 

		ID3D11DeviceContext* devCtx  = m_pRenderFactory->GetContext();
		ID3D11Device*        pDevice = m_pRenderFactory->GetDevice();
		ID3D11PixelShader*   ps = 0;
		if(pstype== D3D11PSType::D3D11PS_COLOR)
			ps = m_pRenderFactory->GetColorPs();
		else if (pstype == D3D11PSType::D3D11PS_TEXTURE)
			ps = m_pRenderFactory->GetTextPs();
		ID3D11VertexShader*  vs =m_pRenderFactory->GetDefVertexShader();
		if (m_pVertexBuffer && vertexBufferDesc.ByteWidth >= dataSizeInBytes) {
			  D3D11_MAPPED_SUBRESOURCE mappedResource;

			 result = devCtx->Map(
				 (ID3D11Resource *)m_pVertexBuffer,
				  0,
				  D3D11_MAP_WRITE_DISCARD,
				  0,
				  &mappedResource
				);
			if (FAILED(result)) {
	           
				return 0;
			}
			memcpy(mappedResource.pData, vertexData, dataSizeInBytes);
			devCtx->Unmap((ID3D11Resource *)m_pVertexBuffer, 0);
		}else {
        
             SAFE_RELEASE(m_pVertexBuffer);
             vertexBufferDesc.ByteWidth = (UINT) dataSizeInBytes*2;
             vertexBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
             vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
             vertexBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

             memset(&vertexBufferData,0,sizeof(vertexBufferData));
			 vertexBufferData.pSysMem = vertexData;
			 vertexBufferData.SysMemPitch = 0;
			 vertexBufferData.SysMemSlicePitch = 0;

			result = pDevice->CreateBuffer(
				&vertexBufferDesc,
				&vertexBufferData,
				&m_pVertexBuffer
				);
			if (FAILED(result)) {
				//WIN_SetErrorFromHRESULT(SDL_COMPOSE_ERROR("ID3D11Device1::CreateBuffer [vertex buffer]"), result);
				return -1;
			}

			

			
			
    }
	m_nStride = sizeof(VertexPositionColor);
	devCtx->IASetVertexBuffers(
		0,
		1,
		&m_pVertexBuffer,
		&m_nStride,
		&m_nOffset
	);
    //设置像素着色器
	devCtx->PSSetShader(ps,NULL,0);
    return 0;
}

  
   void   CRenderTarget_D3D11::DrawLine(int startx,int starty, int endx,int endy, unsigned int color)
   {
	  ID3D11Device*  pDevice = m_pRenderFactory->GetDevice();
	  ID3D11DeviceContext*   pDevContext= m_pRenderFactory->GetContext();

	  unsigned char r, g, b, a;

      GetARGB(color,r, g, b, a);

	 
      float fr, fg, fb, fa;

      fr = (float)(r / 255.0f);
      fg =(float)(g / 255.0f);
      fb = (float)(b / 255.0f);
      fa = (float)(a / 255.0f);

//	  vertices = (VertexPositionColor*)::malloc(sizeof(VertexPositionColor)* cou);

	  VertexPositionColor vertices[2]={
	   
	   { {  startx,  starty, 0.0f }, { 0.0f, 0.0f }, { fr, fg, fb, fa } },
	   { {  endx,    endy, 0.0f }, { 0.0f, 0.0f }, { fr, fg, fb, fa } }
   };

	/*  for (int i = 0; i < count; ++i) {
        const VertexPositionColor v = { { points[i].x + 0.5f, points[i].y + 0.5f, 0.0f }, { 0.0f, 0.0f }, { r, g, b, a } };
        vertices[i] = v;
      }*/

	  UpdateVertexBuffer(D3D11PS_COLOR,vertices, sizeof(VertexPositionColor)*2);
      pDevContext->IASetInputLayout(m_pRenderFactory->GetDefInputLayout());

	

	  pDevContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
	  pDevContext->Draw(2,0);
		

   }
   void   CRenderTarget_D3D11::DrawLines(kkPoint* pts,int count, unsigned int color)
   {
       VertexPositionColor* pVertices = new VertexPositionColor[count];
   
	   for(int i=0;i<count;i++)
	   {
		    pVertices[i].pos.x = pts[i].x;
			pVertices[i].pos.y = pts[i].y;
			pVertices[i].pos.z = 0.0f;
			pVertices[i].tex.x=0.0;
			pVertices[i].tex.y=0.0;
			GetfColor(pVertices[i].color, color);		
	   }

	   
	  ID3D11Device*  pDevice = m_pRenderFactory->GetDevice();
	  ID3D11DeviceContext*   pDevContext= m_pRenderFactory->GetContext();
	  UpdateVertexBuffer(D3D11PS_COLOR,pVertices, sizeof(VertexPositionColor)*count);
      pDevContext->IASetInputLayout(m_pRenderFactory->GetDefInputLayout());
	  pDevContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
	  pDevContext->Draw(count,0);
	  delete pVertices;
   }
   void  CRenderTarget_D3D11::DrawLines(SUVInfo* pts,int count, unsigned int color)
   {
       VertexPositionColor* pVertices = new VertexPositionColor[count];
   
	   for(int i=0;i<count;i++)
	   {
		    pVertices[i].pos.x = pts[i].x;
			pVertices[i].pos.y = pts[i].y;
			pVertices[i].pos.z = 0.0f;
			pVertices[i].tex.x=0.0;
			pVertices[i].tex.y=0.0;
			GetfColor(pVertices[i].color, color);		
	   }

	   
	  ID3D11Device*  pDevice = m_pRenderFactory->GetDevice();
	  ID3D11DeviceContext*   pDevContext= m_pRenderFactory->GetContext();
	  UpdateVertexBuffer(D3D11PS_COLOR,pVertices, sizeof(VertexPositionColor)*count);
      pDevContext->IASetInputLayout(m_pRenderFactory->GetDefInputLayout());
	  pDevContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
	  pDevContext->Draw(count,0);

   }
   void   CRenderTarget_D3D11::DrawText(IFont* pFont,wchar_t *text,kkRect* pRcDest,unsigned int color)
   {
		  
		   
   }
  
   void  CRenderTarget_D3D11::MeasureText(IFont* pFont,wchar_t *text,kkSize *pSz )
   {
	
   }



    void CRenderTarget_D3D11::TextOutV(IFont* pFont,int x,int y , wchar_t* strText,unsigned int color)
	{
		
		wchar_t* p = strText;
		while(*p)
		{
			wchar_t*  p2 = p+1;
			kkSize szWord;
			wchar_t temp[2]={*p};
			MeasureText(pFont,temp,&szWord);

			kkRect Rt = {0,0};
			Rt.left = x;
			Rt.top  = y;
			Rt.bottom  = Rt.top  + szWord.cy;
			Rt.right  = Rt.left + szWord.cx;
			DrawText(pFont, temp,&Rt,color);
			p = p2;
			y += szWord.cy;
		}
	}


    //垂直绘制文本
   void  CRenderTarget_D3D11::DrawTextV(IFont* pFont,wchar_t *text,kkRect* pRcDest,unsigned int color)
   {
	   TextOutV(pFont,
		   pRcDest->left,
		   pRcDest->top ,
           text,
		   color
        );
   }
   void  CRenderTarget_D3D11::MeasureTextV(IFont* pFont,wchar_t *text,kkSize *pSz )
   {
		kkSize szRet={0,0};
		wchar_t* p = text;
		while(*p)
		{
			wchar_t* p2 = p+1;
			kkSize szWord;
			wchar_t temp[2]={*p};
			MeasureText(pFont,temp,&szWord);
			szRet.cx = szRet.cx > szWord.cx ?szRet.cx:szWord.cx;
			szRet.cy += szWord.cy;
			p = p2;
		}
		 *pSz=szRet;
   }


   void  CRenderTarget_D3D11::Rectangle(kkRect* rt,unsigned int cr)
   {
	   
             
   }
  
   void   CRenderTarget_D3D11::RectangleByPt(kkPoint* pt,int width,int height,unsigned int cr)
   {
       
		

   }
  
   int   CRenderTarget_D3D11::FillRect(kkRect* rt,unsigned int cr)
   {
	  ID3D11Device*  pDevice = m_pRenderFactory->GetDevice();
	  ID3D11DeviceContext*   pDevContext= m_pRenderFactory->GetContext();

	  unsigned char r, g, b, a;
      GetARGB(cr,r, g, b, a);

	 
      int fr, fg, fb, fa;

      fr = (float)(r / 255.0f);
      fg =(float)(g / 255.0f);
      fb = (float)(b / 255.0f);
      fa = (float)(a / 255.0f);

//	  vertices = (VertexPositionColor*)::malloc(sizeof(VertexPositionColor)* cou);

	  VertexPositionColor vertices[6]=
	  {
	   
		  { {  rt->left,   rt->bottom,   0.0f },   { 0.0f, 0.0f }, { fr, fg, fb, fa } },
		  { {  rt->left,   rt->top,      0.0f },   { 0.0f, 0.0f }, { fr, fg, fb, fa } },
		  { {  rt->right,  rt->top,      0.0f },   { 0.0f, 0.0f }, { fr, fg, fb, fa } },
		  
		  { {  rt->left,   rt->bottom,   0.0f },   { 0.0f, 0.0f }, { fr, fg, fb, fa } },
		  { {  rt->right,  rt->top,      0.0f },   { 0.0f, 0.0f }, { fr, fg, fb, fa } },
		  { {  rt->right,  rt->bottom,   0.0f },   { 0.0f, 0.0f }, { fr, fg, fb, fa } }

      };


	  UpdateVertexBuffer(D3D11PS_COLOR,vertices, sizeof(vertices));
      pDevContext->IASetInputLayout(m_pRenderFactory->GetDefInputLayout());

	

	  pDevContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	  pDevContext->Draw(6,0);
		return 1;
	}

    void CRenderTarget_D3D11::FillRect(kkRect* rt,unsigned int cr1,unsigned int cr2,unsigned int cr3,unsigned int cr4)
	{
	   
		return ;
	}
    void  CRenderTarget_D3D11::FillRect(SUVInfo* pts1,SUVInfo* pts2,SUVInfo* pts3,SUVInfo* pts4,unsigned int cr1,unsigned int cr2,unsigned int cr3,unsigned int cr4)
	{
	    
		return ;
	
	}
	void  CRenderTarget_D3D11::FillRectByPt(kkPoint* pt,int width,int height,unsigned int cr)
	{
	   
	}

    void CRenderTarget_D3D11::DrawCircle(int xCenter, int yCenter, int r,unsigned int FrameColor,int fillmodel)
	{
	 
         int lxxx = 2 * D3D11_PI * r + 32;
		 VertexPositionColor* pVertices = new VertexPositionColor[lxxx];
   
		 int x,y,delta,delta1,delta2,direction;
		 x=0;y=r;
		 delta=2*(1-r);
		 int i=0;

		if(fillmodel==1){
            i=1;
			pVertices[0].pos.x = xCenter;
			pVertices[0].pos.y = yCenter;

		}
		while(y>=0){
			//(x,y)
			pVertices[i].pos.x = x + xCenter;
			pVertices[i].pos.y = y + yCenter;
			pVertices[i].pos.z = 0.0f;
			pVertices[i].tex.x=0.0;
			pVertices[i].tex.y=0.0;
			GetfColor(pVertices[i].color, FrameColor);		
			++i;
	 
	 
	       //(x,-y)
			pVertices[i].pos.x = x + xCenter;
			pVertices[i].pos.y = -y + yCenter;
			pVertices[i].pos.z = 0.0f;
			pVertices[i].tex.x=0.0;
			pVertices[i].tex.y=0.0;
			GetfColor(pVertices[i].color, FrameColor);	
			++i;
	 
			//(-x, y)
			pVertices[i].pos.x = -x + xCenter;
			pVertices[i].pos.y = y + yCenter;
			pVertices[i].pos.z = 0.0f;
			pVertices[i].tex.x=0.0;
			pVertices[i].tex.y=0.0;
			GetfColor(pVertices[i].color, FrameColor);	
			++i;
			//// //(-x, y)
			pVertices[i].pos.x = -x + xCenter;
			pVertices[i].pos.y = -y + yCenter;
			pVertices[i].pos.z = 0.0f; 
		    pVertices[i].tex.x=0.0;
			pVertices[i].tex.y=0.0;
			GetfColor(pVertices[i].color , FrameColor);	
	         ++i;
			


			if(delta<0)
			{   
				delta1=2*(delta+y)-1;
				if(delta1<=0)
					direction=1; 
				else 
					direction=2; 
			}else if(delta>0)
			{   
				delta2=2*(delta-x)-1;
				if(delta2<=0)
					direction=2; 
				else 
					direction=3; 
			}else  
				direction=2;

			if (direction == 1)
			{
				x++; 
				delta += 2 * x + 1;
			}
			else if (direction == 2) {
				 x++; 
				 y--; 
				 delta+=2*(x-y+1); 
			}
			else if (direction == 3) {
                 y--;
				 delta+=(-2*y+1);
			}
		}

		 ID3D11Device*  pDevice = m_pRenderFactory->GetDevice();
	     ID3D11DeviceContext*   pDevContext= m_pRenderFactory->GetContext();

		 UpdateVertexBuffer(D3D11PS_COLOR,pVertices, lxxx*sizeof(VertexPositionColor));

		 
         pDevContext->IASetInputLayout(m_pRenderFactory->GetDefInputLayout());

	     pDevContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
	     pDevContext->Draw(i,0);

	 
		delete [] pVertices;


   }
   





   CPixelShader_D3D11:: CPixelShader_D3D11(SRenderFactory_D3D11* pRenderFactory):
	   m_pRenderFactory(pRenderFactory),m_pPixelShader(0)
   {
     
	  
   }
	
   void CPixelShader_D3D11::IniPs()
   {
	   
	
		ReLoadRes();
   }
   CPixelShader_D3D11::~CPixelShader_D3D11()
   {

	  
  }
  void   CPixelShader_D3D11::Release2()
  {
	  
  }
  void   CPixelShader_D3D11::ReLoadRes()
  {
	    
 
  }

	 CYUVPixelShader_D3D11::CYUVPixelShader_D3D11(SRenderFactory_D3D11* pRenderFactory):CPixelShader_D3D11(pRenderFactory)
	 {
	    
	 }
	 CYUVPixelShader_D3D11::~CYUVPixelShader_D3D11()
	 {
	 
	 }

	 void CYUVPixelShader_D3D11::IniPs()
	 {
		
		     ReLoadRes();
		
	 }
	 void  CYUVPixelShader_D3D11::ReLoadRes()
	 {
		 if(m_pPixelShader)
			 m_pPixelShader->Release();
		 ID3D11Device*  pDevice = m_pRenderFactory->GetDevice();
	     HRESULT   hResult  =  pDevice->CreatePixelShader(
			D3D11_Ps_Yuv,
			sizeof(D3D11_Ps_Yuv),
			NULL,
			&m_pPixelShader
			);

	 }

	
	CCommPixelShader_D3D11::CCommPixelShader_D3D11(SRenderFactory_D3D11* pRenderFactory,const DWORD*  m_psCode,const char* psName):
	 CPixelShader_D3D11(pRenderFactory),
	 m_pPsCode(m_psCode),
	 m_strPsName(psName)
    {
   
    }
   CCommPixelShader_D3D11::~CCommPixelShader_D3D11()
   {
   
   }
   void CCommPixelShader_D3D11::IniPs()
   {
       ReLoadRes();		
   }
   void  CCommPixelShader_D3D11::ReLoadRes()
   {
	
   }
   const char* CCommPixelShader_D3D11::GetPsName()
   {
	   return m_strPsName.c_str();
   }
}

	
extern "C"
{
	void  __declspec(dllexport) CreateRenderFactoryD3D11(KKUI::IRenderFactory** ff)
	{
		KKUI::SRenderFactory_D3D11* f = new KKUI::SRenderFactory_D3D11();
		*ff=f;
	}
}