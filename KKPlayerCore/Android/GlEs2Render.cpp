#include "GlEs2Render.h"
#define  LOG_TAG    "libgl2jni"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)
enum {
    ATTRIB_VERTEX,
    ATTRIB_TEXTURE,
};

static const char GSurfaceVertexShader[] =
                "attribute vec4 aPosition;\n"
                "attribute vec4 aTexCoordinate;\n"
                "uniform mat4 texTransform;\n"
                "varying vec2 v_TexCoordinate;\n"
                "void main() {\n"
                "v_TexCoordinate = (texTransform * aTexCoordinate).xy;\n"
                "gl_Position = aPosition;\n"
                "}\n";


static const char GSurfaceFragmentShader[] =
                "#extension GL_OES_EGL_image_external : require\n"
                "precision mediump float;\n"
                "uniform samplerExternalOES  texture;\n"
                "varying vec2 v_TexCoordinate;\n"
                "void main() {\n"
                "vec4 color = texture2D(texture,v_TexCoordinate);\n"
                "gl_FragColor = color;\n"
                "}\n";
typedef struct KK_GLES_Matrix
{
    GLfloat m[16];
} KK_GLES_Matrix;

static void printGLString(const char *name, GLenum s)
{
    const char *v = (const char *)::glGetString(s);
    LOGI("GL %s = %s\n", name, v);
}

void checkGlError(const char* op)
{
    for (GLint error = glGetError(); error; error = glGetError()) {
        LOGI("GlError after %s() glError (0x%x)\n", op, error);
    }
}
static GLuint buildShader(const char* source, GLenum shaderType)
{
    GLuint shaderHandle = glCreateShader(shaderType);

    if (shaderHandle)
    {
        glShaderSource(shaderHandle, 1, &source, 0);
        glCompileShader(shaderHandle);

        GLint compiled = 0;
        glGetShaderiv(shaderHandle, GL_COMPILE_STATUS, &compiled);
        if (!compiled)
        {
            GLint infoLen = 0;
            glGetShaderiv(shaderHandle, GL_INFO_LOG_LENGTH, &infoLen);
            if (infoLen)
            {
                char* buf = (char*) malloc(infoLen);
                if (buf)
                {
                    glGetShaderInfoLog(shaderHandle, infoLen, NULL, buf);
                    LOGE("error::Could not compile shader %d:\n%s\n", shaderType, buf);
                    free(buf);
                }
                glDeleteShader(shaderHandle);
                shaderHandle = 0;
            }
        }

    }

    return shaderHandle;
}
GLuint buildProgram(const char* StrVShader,const char* StrFragShader,GLuint& nVShader,GLuint& nFraShader)
{
    nVShader = buildShader(StrVShader, GL_VERTEX_SHADER);
    nFraShader = buildShader(StrFragShader, GL_FRAGMENT_SHADER);
    GLuint programHandle = glCreateProgram();

    if (programHandle)
    {
        glAttachShader(programHandle, nVShader);
        checkGlError("glAttachShader");
        glAttachShader(programHandle, nFraShader);
        checkGlError("glAttachShader");
        glLinkProgram(programHandle);

        GLint linkStatus = GL_FALSE;
        glGetProgramiv(programHandle, GL_LINK_STATUS, &linkStatus);
        if (linkStatus != GL_TRUE) {
            GLint bufLength = 0;
            glGetProgramiv(programHandle, GL_INFO_LOG_LENGTH, &bufLength);
            if (bufLength) {
                char* buf = (char*) malloc(bufLength);
                if (buf) {
                    glGetProgramInfoLog(programHandle, bufLength, NULL, buf);
                    LOGE("error::Could not link program:\n%s\n", buf);
                    free(buf);
                }
            }
            glDeleteProgram(programHandle);
            programHandle = -1;
			nVShader      = -1;
			nFraShader    = -1;
        }

    }

    return programHandle;
}






static void AVTexCoords_reloadVertex(GLuint av2_texcoord,const void *texcoords )
{
    glVertexAttribPointer(av2_texcoord, 2, GL_FLOAT, GL_FALSE, 0, texcoords);
    glEnableVertexAttribArray(av2_texcoord);
}

static void AVVertices_reloadVertex(GLuint av4_position,const void *vertices)
{
    glVertexAttribPointer(av4_position, 2, GL_FLOAT, GL_FALSE, 0, vertices);
    glEnableVertexAttribArray(av4_position);
}

static const GLfloat texTransform[16] = {
       1.0f,   0.0f,   0.0f,   0.0f,
       0.0f,   -1.0f,  0.0f,   0.0f,
       0.0f,   0.0f,   1.0f,   0.0f,
       0.0f,   1.0f,   0.0f,   1.0f
};

//PlayVideo-OpenGL
GlEs2Render::GlEs2Render(KKPlayer* pPlayer):m_pGLHandle(0),gvPositionHandle(0),m_Screen_Width(0)
,m_Screen_Height(0),m_nTextureID(0),m_bAdJust(false)
,m_RenderWidth(0),m_RenderHeight(0),m_Picwidth(0)
,m_Picheight(0),m_pPlayer(pPlayer),m_nKeepRatio(1),m_nLastKeepRatio(-1)
,g_SurfaceTextVId(0)
,updateTexImageMethodId(0)
,getTimestampMethodId(0)
,getTransformMtxId(0)
,m_penv(0)
,javaSurfaceTextureObj(0)
,javaViewSurfaceObj(0)
,m_bfameAvailable(false)
,m_bAvPicLoaded(0)
,m_UsedViewSurfaceed(0)
,setDefaultBufferSizeMethodId(0)
,m_pBackGlCtx(0)
{
	

   
	
}
GlEs2Render::~GlEs2Render()
{
	GLES2_Renderer_reset();
	 if (m_penv&&javaSurfaceTextureObj) {
        m_penv->DeleteGlobalRef( javaSurfaceTextureObj );
        javaSurfaceTextureObj = 0;
		
		if(javaViewSurfaceObj){
			m_penv->DeleteGlobalRef(javaViewSurfaceObj);
			javaViewSurfaceObj=0;
		}
    }
}
void  GlEs2Render::SetKeepRatio(int KeepRatio)
{
	m_nKeepRatio=KeepRatio;
}
void GlEs2Render::GLES2_Renderer_reset()
{
	///gl view
	///软解码使用
	{
		
	}
	
	m_bAvPicLoaded=0;
}



void KK_GLES2_loadOrtho(KK_GLES_Matrix *matrix, GLfloat left, GLfloat right, GLfloat bottom, GLfloat top, GLfloat near, GLfloat far)
{
    GLfloat r_l = right - left;
    GLfloat t_b = top - bottom;
    GLfloat f_n = far - near;
    GLfloat tx = - (right + left) / (right - left);
    GLfloat ty = - (top + bottom) / (top - bottom);
    GLfloat tz = - (far + near) / (far - near);

    matrix->m[0] = 2.0f / r_l;
    matrix->m[1] = 0.0f;
    matrix->m[2] = 0.0f;
    matrix->m[3] = 0.0f;

    matrix->m[4] = 0.0f;
    matrix->m[5] = 2.0f / t_b;
    matrix->m[6] = 0.0f;
    matrix->m[7] = 0.0f;

    matrix->m[8] = 0.0f;
    matrix->m[9] = 0.0f;
    matrix->m[10] = -2.0f / f_n;
    matrix->m[11] = 0.0f;

    matrix->m[12] = tx;
    matrix->m[13] = ty;
    matrix->m[14] = tz;
    matrix->m[15] = 1.0f;
}

int GlEs2Render::IniGl()
{

    printGLString("Version", GL_VERSION);
    printGLString("Vendor", GL_VENDOR);
    printGLString("Renderer", GL_RENDERER);
    printGLString("Extensions", GL_EXTENSIONS);


	///软件使用的gl代码块
	{
			
			

		//	g_av4_position = glGetAttribLocation(g_glProgram, "av4_Position");
		//	g_av2_texcoord = glGetAttribLocation(g_glProgram, "av2_Texcoord");
		//	GLuint  um4_mvp      = glGetUniformLocation(g_glProgram, "um4_ModelViewProjection");


		//	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		//	glEnable(GL_CULL_FACE);
		//	glCullFace(GL_BACK);
		//	glDisable(GL_DEPTH_TEST);


		//	KK_GLES_Matrix modelViewProj;
		//	KK_GLES2_loadOrtho(&modelViewProj, -1.0f, 1.0f, -1.0f, 1.0f, -1.0f, 1.0f);
		//	glUniformMatrix4fv(um4_mvp, 1, GL_FALSE, modelViewProj.m);
    }
   //	glUseProgram(0);
    

   
    return m_pGLHandle;
}


void GlEs2Render::setFrameAvailable(bool const available) 
{
    m_bfameAvailable = available;
}
jobject GlEs2Render::GenerateSurface()
{
	const char *stClassPath = "android/view/Surface";
    const jclass ViewSurfaceClass = m_penv->FindClass(stClassPath);
    if (ViewSurfaceClass == 0) {
       LOGE("FindClass (%s) failed", stClassPath);
    }

    const jmethodID constructor = m_penv->GetMethodID( ViewSurfaceClass, "<init>", "(Landroid/graphics/SurfaceTexture;)V" );
    if (constructor == 0) {
      LOGE("GetMethonID(<init>) View/Surface failed");
    }
    jobject  obj = m_penv->NewObject(ViewSurfaceClass, constructor, javaSurfaceTextureObj);
    if (obj == 0) {
       LOGE("NewObject() View/Surface failed");
    }
	
	javaViewSurfaceObj = m_penv->NewGlobalRef(obj);
    if (javaViewSurfaceObj == 0) {
       LOGE("NewGlobalRef() graphics/SurfaceTexture failed");
    }

    //Now that we have a globalRef, we can free the localRef
    m_penv->DeleteLocalRef(obj);
   
}
jobject GlEs2Render::SetSurfaceTexture(JNIEnv *env)
{
	m_penv=env;
	
    const char *stClassPath = "android/graphics/SurfaceTexture";
    const jclass surfaceTextureClass = env->FindClass(stClassPath);
    if (surfaceTextureClass == 0) {
       LOGE("FindClass (%s) failed", stClassPath);
    }

//    // find the constructor that takes an int
    const jmethodID constructor = env->GetMethodID( surfaceTextureClass, "<init>", "(I)V" );
    if (constructor == 0) {
      LOGE("GetMethonID(<init>) graphics/SurfaceTexture failed");
    }

    jobject  obj = env->NewObject(surfaceTextureClass, constructor, g_SurfaceTextVId);
    if (obj == 0) {
       LOGE("NewObject() graphics/SurfaceTexture failed");
    }

    javaSurfaceTextureObj = env->NewGlobalRef(obj);
    if (javaSurfaceTextureObj == 0) {
       LOGE("NewGlobalRef() graphics/SurfaceTexture failed");
    }

    //Now that we have a globalRef, we can free the localRef
   env->DeleteLocalRef(obj);

    updateTexImageMethodId = env->GetMethodID( surfaceTextureClass, "updateTexImage", "()V");
    if ( !updateTexImageMethodId ) {
       LOGE("couldn't get updateTexImageMethonId");
    }
  setDefaultBufferSizeMethodId= env->GetMethodID(surfaceTextureClass, "setDefaultBufferSize","(II)V");
    getTimestampMethodId =env->GetMethodID(surfaceTextureClass, "getTimestamp", "()J");
    if (!getTimestampMethodId) {
       LOGE("couldn't get TimestampMethodId");
    }

    getTransformMtxId = env->GetMethodID(surfaceTextureClass, "getTransformMatrix", "([F)V");
    if (!getTransformMtxId) {
        LOGE("couldn't get getTransformMtxId");
    }

    // jclass objects are loacalRefs that need to be free;
    env->DeleteLocalRef( surfaceTextureClass );  /**/
	
	GenerateSurface();
	glUseProgram(0);
	return javaSurfaceTextureObj;
}
jobject  GlEs2Render::GetViewSurface()
{
	
	return javaViewSurfaceObj;
}
jobject  GlEs2Render::GetSurfaceTexture()
{
	
	return javaSurfaceTextureObj;
}

ERenderType GlEs2Render::GetRenderType()
{
	return OpenGL_RENDER;
}
void GlEs2Render::GlViewRender(bool ReLoad,bool bSurfaceDisplay)
{
   
	glClear(GL_COLOR_BUFFER_BIT);
	if(bSurfaceDisplay&&0)
	   m_UsedViewSurfaceed=1;
   else
	   m_UsedViewSurfaceed=0;
	
	//LOGE_KK("m_GlEsYuv.Init \n");
    m_GlEsYuv.Init();
	//return;
   /* if(m_bfameAvailable&&m_penv&&javaSurfaceTextureObj){
	  m_penv->CallVoidMethod(javaSurfaceTextureObj, updateTexImageMethodId);
	}*/
	
	//glUseProgram(g_glProgram);
	
    if( m_RenderWidth==0|| m_RenderHeight==0)
	{
		LOGE_KK("GlViewRender w%d -%d \n",m_RenderWidth,m_RenderHeight);
         return;
	}

	bool t1=m_Picwidth!=0&& m_Picheight!=0;
	bool  t2 = (!m_bAdJust || m_nKeepRatio!=m_nLastKeepRatio)&&t1;
	
        float width2     = m_Picwidth;
        float height2    = m_Picheight;
		
		
		m_nLastKeepRatio=m_nKeepRatio;
		
		//if(m_penv&&bSurfaceDisplay){
		  // m_penv->CallVoidMethod(javaSurfaceTextureObj, setDefaultBufferSizeMethodId, m_Picwidth,m_Picheight);
		//}
		
		//LOGE("dwidth:%d dheight:%d --w:%f-h:%f\n",m_RenderWidth,m_RenderHeight,width2, height2);
		if(m_nKeepRatio==1)
		{
			 width2     = m_Picwidth;
             height2    = m_Picheight;
		}else  if(m_nKeepRatio==2){
			///4:3
			height2    =(float) (m_Picwidth*3.0)/4.0;
			width2     = m_Picwidth;
		}else if(m_nKeepRatio==3){
			///16:9
			height2    =(float) (m_Picwidth*9.0)/16.0;
			width2     = m_Picwidth;
		}
		
		float width      = m_RenderWidth;
        float height     = width * height2 / width2;
		//LOGE(" width:%.2f height:%.2f \n",width,height);		
		if(m_nKeepRatio==1|| m_nKeepRatio==2||m_nKeepRatio==3){
			float fff = 0.9;
			while(height>m_RenderHeight)
			{
				width  =  width*fff;
				height =  height*fff;
				fff -= 0.1;
			}		
		}
		
        float dW  = m_RenderWidth	/ width;
        float dH  = m_RenderHeight / height;
        float nW        = 1.0f;
        float nH        = 1.0f;


	
		if(m_nKeepRatio!=0){
           nW = (width   / (float)m_RenderWidth);
           nH = (height  / (float)m_RenderHeight);
		}

        LOGE("Ratio:%d width:%.2f height:%.2f \n",m_nKeepRatio,width,height);
		//LOGE("nw :%.2f  nH :%.2f \n",nW, nH );
		
 
 
        GLfloat AVPos[8]={0.0};
        AVPos[0] = - nW;
        AVPos[1] = - nH;
        AVPos[2] =   nW;
        AVPos[3] = - nH;
        AVPos[4] = - nW;
        AVPos[5] =   nH;
        AVPos[6] =   nW;
        AVPos[7] =   nH;
   

	
    m_pPlayer->OnLoadVideo(this);
	

    KK_GLES_Matrix modelViewProj;
	KK_GLES2_loadOrtho(&modelViewProj, -1.0f, 1.0f, -1.0f, 1.0f, -1.0f, 1.0f);
	
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    m_GlEsYuv.OnRender(modelViewProj.m, AVPos);



	glUseProgram(0);
    if(m_Picwidth==0|| m_Picheight==0)
         return;
   
    return;
}


bool GlEs2Render::init(HWND hView)
{
	IniGl();
	return true;
}
void GlEs2Render::destroy()
{
	
}
void GlEs2Render::resize(unsigned int w, unsigned int h)
{
	m_RenderWidth=w;
    m_RenderHeight=h;
	
	LOGE("Onsize w:%d h:%d \n",m_RenderWidth,m_RenderHeight);
    // 重置当前的视口
	glViewport(0, 0,w, h);
    m_bAdJust= false;
}
void GlEs2Render::render(kkAVPicInfo *Picinfo,bool wait)
{
	m_GlEsYuv.LoadDtata(Picinfo);
}
void GlEs2Render::renderBk(unsigned char* buf,int len)
{
	
}
void GlEs2Render::SetWaitPic(unsigned char* buf,int len)
{
	
}
void GlEs2Render::LoadCenterLogo(unsigned char* buf,int len)
{
	
}
void GlEs2Render::SetErrPic(unsigned char* buf,int len)
{
	
}
void GlEs2Render::ShowErrPic(bool show)
{
	
}
void GlEs2Render::SetLeftPicStr(const char *str)
{

}
void GlEs2Render::FillRect(kkBitmap img,kkRect rt,unsigned int color)
{
	
	
}
void GlEs2Render::SetRenderImgCall(fpRenderImgCall fp,void* UserData)
{
	
	
}
bool GlEs2Render::GetHardInfo(void** pd3d,void** pd3ddev,int *ver)
{			
	return 0;
}
void GlEs2Render::SetResetHardInfoCall(fpResetDevCall call,void* UserData)
{
	
}
long long GlEs2Render::GetOnSizeTick()
{
 return 0;
}
void GlEs2Render::RetSetSizeTick()
{

}
void GlEs2Render::renderLock()
{
	
}
void GlEs2Render::renderUnLock()
{
	
}