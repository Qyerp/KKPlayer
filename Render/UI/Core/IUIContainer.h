///这里的UI只是一个简单UI，目的是为了方便视频渲染
#ifndef KKUI_IUIContainer_H_
#define KKUI_IUIContainer_H_
#include "IUIMessage.h"
namespace KKUI{
	class CUIWindow;
	class IRenderFactory;
	struct ITimelineHandler
    {
        virtual void OnNextFrame()=0;
    };
    class IUIContainer
	{
		public:
			      virtual  void          ClearHover(CUIWindow  *pChild) = 0;
			      virtual int            OnReleaseUIWndCapture() = 0 ;
				  virtual CUIWindow*    GetUIWndCapture() = 0 ;
				  virtual CUIWindow*     OnSetUIWndCapture( CUIWindow* swnd) =0;
			      virtual void           SetMessage(IUIMessage* mess) = 0;

				  virtual void           SetIRenderFactory(IRenderFactory* pRenderFactory) = 0;
                  virtual IRenderFactory* GetIRenderFactory() = 0 ;
				  virtual IUIMessage*    GetIUIMessage() = 0 ;
			      virtual int            DoFrameEvent(unsigned int uMsg,unsigned int wParam,long lParam) = 0;
				  virtual void           OnFrameKeyDown(unsigned int nChar,unsigned int  nRepCnt,unsigned int nFlags) = 0;
				  virtual void           OnFrameMouseMove(unsigned int uFlag,const int& x,const int& y)  = 0;
				  virtual void           OnFrameMouseLeave(unsigned int uFlag) = 0;
				  virtual int            OnFrameSetCursor(const int& x,const int& y) = 0 ;
				  virtual void           OnFrameMouseEvent(unsigned int  uMsg,unsigned int wParam,long lParam) = 0;
				  virtual void           OnFrameMouseWheel(unsigned int  uMsg,unsigned int wParam,long lParam) = 0;

				  virtual void           OnNextFrame() = 0;
				  virtual bool           RegisterTimelineHandler( ITimelineHandler *pHandler ) = 0;
				  virtual bool           UnregisterTimelineHandler( ITimelineHandler *pHandler ) = 0;
	};
}
#endif