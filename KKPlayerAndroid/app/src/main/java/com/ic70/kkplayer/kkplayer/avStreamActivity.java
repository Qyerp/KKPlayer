package com.ic70.kkplayer.kkplayer;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.DrawableRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import  com.ic70.kkplayer.sdk.IKKPlayerErrNotify;
import  com.ic70.kkplayer.sdk.GLES2_0_SurfaceView;
import  com.ic70.kkplayer.sdk.CKKPlayerGlRender;
import  com.ic70.kkplayer.sdk.CKKMoviePath;
import  com.ic70.kkplayer.sdk.CkkMediaInfo;
public class avStreamActivity extends AppCompatActivity implements IKKPlayerErrNotify {
    enum  EnumPlayerStata
    {
        IniFirst,
        OpenFailure,
        Stop,
        Play,
        PAUSE
    }
    private  GLES2_0_SurfaceView m_glView;
    private  CKKPlayerGlRender   m_kkPlayer=null;
    private  int     m_nScreenMethod=0;
    private  int    m_oldHeight=0;
    private EnumPlayerStata PlayerStata= EnumPlayerStata.IniFirst;
    private Lock m_Avtivitylock = new ReentrantLock();
    boolean         m_bSeekPlayer=false;
    private  int    m_nWhereInTime=0;
    private  int    m_nCurTime=0;
    private  String         m_strCurTimeStr = new String();
    private  String         m_strTotalTimeStr = new String();
    private  Timer          m_Activitytimer = new Timer();
    private  TimerTask      m_TimerTask = new TimerTask() {
        @Override
        public void run() {
            Message message = new Message();
            message.what = 1;
            Activityhandler.sendMessage(message);
        }
    }; /* */


    Handler Activityhandler = new Handler()
    {
        public void handleMessage(Message msg)
        {
            m_Avtivitylock.lock();
            m_nWhereInTime=1;
            m_Avtivitylock.unlock();
            if (msg.what == 1) {
                if( m_kkPlayer!=null&&!m_bSeekPlayer) {
                    CkkMediaInfo info = m_kkPlayer.GetCMediaInfo();

                    SeekBar MovieSeekBar = (SeekBar) findViewById(R.id.MovieSeekbar);
                    if (info.Serial1 == info.Serial&&info.Serial1!=-1) {
                        MovieSeekBar.setProgress(info.CurTime);
                        MovieSeekBar.setMax(info.TotalTime);
                        TextView CurTimetextView = (TextView) findViewById(R.id.CurTimetextView);
                        TextView TotalTimetextView = (TextView) findViewById(R.id.TotalTimetextView);
                        m_nCurTime = info.CurTime;

                        int h = (info.CurTime / (60 * 60));
                        int m = (info.CurTime % (60 * 60)) / (60);
                        int s = ((info.CurTime % (60 * 60)) % (60));
                        if (h < 10)
                            m_strCurTimeStr = "0" + h + ":";
                        else
                            m_strCurTimeStr = h + ":";
                        if (m < 10)
                            m_strCurTimeStr += "0" + m + ":";
                        else
                            m_strCurTimeStr += m + ":";
                        if (s < 10)
                            m_strCurTimeStr += "0" + s + "";
                        else
                            m_strCurTimeStr += s + "";


                        h = info.TotalTime / (60 * 60);
                        m = (info.TotalTime % (60 * 60)) / 60;
                        s = ((info.TotalTime % (60 * 60)) % 60);
                        if (h < 10)
                            m_strTotalTimeStr = "0" + h + ":";
                        else
                            m_strTotalTimeStr = h + ":";
                        if (m < 10)
                            m_strTotalTimeStr += "0" + m + ":";
                        else
                            m_strTotalTimeStr += m + ":";
                        if (s < 10)
                            m_strTotalTimeStr += "0" + s + "";
                        else
                            m_strTotalTimeStr += s + "";

                       CurTimetextView.setText(m_strCurTimeStr);
                        TotalTimetextView.setText(m_strTotalTimeStr);
                    }
                }
            }else if (msg.what == 2){
                int err=msg.arg2;
                if(err ==CKKPlayerGlRender.KKAVOver) {
                    Button NetButton = (Button) findViewById(R.id.NetButton);
                    NetButton.setText("视频播放结束");
                    NetButton.setVisibility(View.VISIBLE);
                }else  if( err== CKKPlayerGlRender.KKOpenUrlOkFailure ) {
                    PlayerStata =EnumPlayerStata.OpenFailure;
                    Button NetButton = (Button) findViewById(R.id.NetButton);
                    NetButton.setText("文件打开失败");
                    NetButton.setVisibility(View.VISIBLE);
                   ImageView ImageV = (ImageView) findViewById(R.id.WaitRImageView);
                    ImageV.setVisibility(View.GONE);
                    ImageV.setAnimation(null);
                    PlayerStata= EnumPlayerStata.OpenFailure;
                }else if (err== CKKPlayerGlRender.KKAVWait) {
                    ///需要缓冲
                    ImageView ImageV = (ImageView) findViewById(R.id.WaitRImageView);
                    if (ImageV.getVisibility() != View.VISIBLE)
                        ShowWaitGif();
                } else if (err==CKKPlayerGlRender.KKAVReady) {
                   ImageView ImageV = (ImageView) findViewById(R.id.WaitRImageView);
                    ImageV.setVisibility(View.GONE);
                    ImageV.setAnimation(null);
                    Button NetButton = (Button) findViewById(R.id.NetButton);
                    NetButton.setVisibility(View.GONE);
                }else if (err==CKKPlayerGlRender.KKSeekOk || err==CKKPlayerGlRender.KKSeekErr) {

                }else if(err !=CKKPlayerGlRender.KKOpenUrlOk){


                }
            }
            super.handleMessage(msg);
            m_Avtivitylock.lock();
            m_nWhereInTime=0;
            m_Avtivitylock.unlock();
        };
    };

    public avStreamActivity()
    {
        m_kkPlayer = new CKKPlayerGlRender();
    }

    class MediaSeekBarChangeListener implements SeekBar.OnSeekBarChangeListener
    {
        avStreamActivity m_PlayerActivity;
        public  MediaSeekBarChangeListener(avStreamActivity PlayerActivity)
        {
            m_PlayerActivity=PlayerActivity;
        }
        public void onProgressChanged(SeekBar var1, int var2, boolean var3)
        {

        }
        public void onStartTrackingTouch(SeekBar var1)
        {
            m_bSeekPlayer=true;
        }
        public void onStopTrackingTouch(SeekBar var1)
        {
            if(PlayerStata!= EnumPlayerStata.Stop)
            {
                if(PlayerStata== EnumPlayerStata.PAUSE)
                {
                    SetPalyState();
                }
                int ll=var1.getProgress();
                m_kkPlayer.Seek(ll);
                m_bSeekPlayer=false;
            }

        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.av_stream_main);

        ///禁止休眠
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        FrameLayout PlayerFrameLayout = (FrameLayout) findViewById(R.id.IDStreamPlayer);
        ViewGroup.LayoutParams lp = PlayerFrameLayout.getLayoutParams();
        m_oldHeight =lp.height;

        m_glView =  (GLES2_0_SurfaceView) findViewById(R.id.GlsurfaceView);
        m_glView.setRenderer(m_kkPlayer);

        //MovieFrameLayout.addView(glView, 0);

        ///设置全屏大小监听
        ImageButton ImgBtn= ( ImageButton) findViewById(R.id.btnFullScreen);
        ImgBtn.setOnClickListener( new ImageBtnClick(this));

        ///设置seek监听
        SeekBar SeekBtn=(SeekBar)findViewById(R.id.MovieSeekbar);
        SeekBtn.setOnSeekBarChangeListener(new MediaSeekBarChangeListener(this));

        ///设置播放器监听
        m_kkPlayer.SetPlayerStateNotify(this);
        ///开始定时器
        m_Activitytimer.schedule(m_TimerTask, 500, 500);
        ShowWaitGif();
    }

    class ImageBtnClick implements   View.OnClickListener
    {
        avStreamActivity m_Activity;
        public ImageBtnClick(avStreamActivity PlayerActivity) {
            m_Activity=PlayerActivity;
        }

        public void onClick(View var1) {
            if(m_nScreenMethod==0){
                m_nScreenMethod=1;
                m_Activity.PlayerFullScreen();
            }else {
                m_nScreenMethod=0;
                RecoverScreen();
            }

        }
    }
    @Override
    protected void onStart() {
        super.onStart();

    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        // TODO Auto-generated method stub
        super.onWindowFocusChanged(hasFocus);
        if(hasFocus){
            ///https://www.okzy.co/
            if(PlayerStata== EnumPlayerStata.IniFirst) {
                ShowWaitGif();
                m_kkPlayer.OpenMedia("https://163.com-www-letv.com/20180510/513_8afacebc/index.m3u8");
                m_nCurTime = 0;
                PlayerStata = EnumPlayerStata.Play;
            }
        }
    }

    public void onStop()
    {
        super.onStop();
        if(PlayerStata== EnumPlayerStata.Play)
        {
           SetPalyState();
        }
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            Log.v("KEYCODE_BACK", "KEYCODE_BACK");
            m_Activitytimer.cancel();
            PlayerStata=EnumPlayerStata.Stop;
            m_Avtivitylock.lock();
            while(m_nWhereInTime==1)
            {
                ;
            }
            m_Avtivitylock.unlock();
            CKKPlayerGlRender KKPlayer= m_kkPlayer;
            m_kkPlayer=null;
            KKPlayer.KKDel();
            KKPlayer=null;
            Log.v("MoviePath", "Del");
            finish();
            Log.v("MoviePath", "退出 over mmm");
        }
        return false;
    }
    ///
    void RecoverScreen(){
        Log.v("Screen", "竖屏");
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);//,//竖屏
        FrameLayout PlayerFrameLayout = (FrameLayout) findViewById(R.id.IDStreamPlayer);

        WindowManager wm = (WindowManager) this.getSystemService(WINDOW_SERVICE);
        int width = wm.getDefaultDisplay().getWidth();
        int height =  m_oldHeight;

        ViewGroup.LayoutParams lp = PlayerFrameLayout.getLayoutParams();
        lp.height=height;
        lp.width=width;
        PlayerFrameLayout.setLayoutParams(lp);

        ImageButton btn=(ImageButton)findViewById(R.id.btnFullScreen);
        btn.setImageResource(R.drawable.max_screen);
    }
    void PlayerFullScreen()
    {
         Log.v("Screen", "全屏");
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
         setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);//,//横屏

        //隐藏状态栏
        //定义全屏参数
        int flag=WindowManager.LayoutParams.FLAG_FULLSCREEN;
        //设置当前窗体为全屏显示
      //  setFlags(flag, flag);

         FrameLayout PlayerFrameLayout = (FrameLayout) findViewById(R.id.IDStreamPlayer);
         WindowManager wm = (WindowManager) this.getSystemService(WINDOW_SERVICE);
         int width = wm.getDefaultDisplay().getWidth();
         int height = wm.getDefaultDisplay().getHeight();

         ViewGroup.LayoutParams lp = PlayerFrameLayout.getLayoutParams();
         lp.height=height;
         lp.width=width;
         PlayerFrameLayout.setLayoutParams(lp);



        ImageButton btn=(ImageButton)findViewById(R.id.btnFullScreen);
        btn.setImageResource(R.drawable.small_screen);
         //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
         //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);//,//竖屏
    }

    ///播放器状态通知
    public void OpenMediaStateNotify(int obj,int err) {

        Log.v("libgl2jni","errcode:"+err);
        Message message = new Message();
        message.what = 2;
        message.arg1 =obj;
        message.arg2 = err;
        Activityhandler.sendMessage(message);
    }

    protected void SetPalyState()
    {
        ImageButton btn=(ImageButton)findViewById(R.id.StartButton);
        if(PlayerStata==EnumPlayerStata.Play) {
            btn.setImageResource(R.drawable.play1);
            PlayerStata= EnumPlayerStata.PAUSE;
            m_kkPlayer.Pause(1);
        }else if(PlayerStata== EnumPlayerStata.PAUSE)
        {
            btn.setImageResource(R.drawable.pause1);
            PlayerStata= EnumPlayerStata.Play;
            m_kkPlayer.Pause(0);
        }
    }

    public void ShowWaitGif()
    {
        Log.v("libgl2jni","WaitGif");
        ImageView infoOperatingIV = (ImageView)findViewById(R.id.WaitRImageView);
        Animation operatingAnim = AnimationUtils.loadAnimation(this, R.anim.dirtiprotate);
        LinearInterpolator lin = new LinearInterpolator();
        operatingAnim.setInterpolator(lin);
        if (operatingAnim != null) {
            infoOperatingIV.startAnimation(operatingAnim);
            infoOperatingIV.bringToFront();
            infoOperatingIV.setVisibility(View.VISIBLE);
        } /**/
    }
}
