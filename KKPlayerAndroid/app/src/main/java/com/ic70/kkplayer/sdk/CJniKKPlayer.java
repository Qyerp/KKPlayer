package com.ic70.kkplayer.sdk;
import android.view.Surface;
import android.view.SurfaceView;
import android.graphics.SurfaceTexture;
/**
 * Created by saint on 2016/2/26.
 * JNI 层，共sdk调用，不建议直接使用，请使用 CKKPlayerGlRender
 */
public class CJniKKPlayer {
    public class CapImgInfo
    {
        public CapImgInfo()
        {

        }
        public byte[] ImgData=null;
        public String ImgPixfmt=null;
        public int imgHeight=0;
        public  int imgWidth=0;
        public int dataSize=0;
    }

    static{
        System.loadLibrary("z");
        System.loadLibrary("KKPlayerCore");
    }
    //初始化
    public native void GIni();
    /**
     * 初始化一个KKplayer实例 目前只支持glview。surfaceview后续支持
     * @param RenderType 0 glview 1 surfaceview
     * @return 返回一个播放器对象
     */
    public native CKKPlayerCtx  IniKK(int RenderType);

    ///设置播放器状态通知函数
    public native void SetIPlayerStateNotify(CKKPlayerCtx  obj,IKKPlayerErrNotify notify);
    //检查Gl环境，初始化glview环境
    public native int IniGl(CKKPlayerCtx obj);

    //设置解码方式 0 默认解码， 1 mediacodec 解码(只对h264,h265 有效)
    public native void SetDecoderMethod(CKKPlayerCtx  obj, int method);

    /***
     *
     * @param obj
     * @return android/graphics/SurfaceTexture
     */
    public native SurfaceTexture GetSurfaceTexture(CKKPlayerCtx  obj);

    public native void OnSurfaceTextureFrameAailable(CKKPlayerCtx  obj);
    /***
     * 尺寸调整
     * @param obj 播放器实例
     * @param w
     * @param h
     * @return
     */
    public native int OnSize(CKKPlayerCtx  obj,int w, int h);

    /***
     * 是否保持视频比例
     * @param KeepRatio 0拉伸， 1保持比例，2 4:3 ，3 16:9
     */
    public native void SetKeepRatio(CKKPlayerCtx  obj,int KeepRatio);
    /**
     * glview 显示调用
     * @param obj 播放器实例
     * @return
     */
    public native int GlRender(CKKPlayerCtx  obj);

    /***
     * 删除播放器
     * @param obj 播放器实例
     */
    public native void DelKK(CKKPlayerCtx  obj);


    public native int  KKOpenMedia(String str,CKKPlayerCtx obj);

    public native int  KKIsNeedReConnect(CKKPlayerCtx  obj);

    public native  int  KKCloseMedia(CKKPlayerCtx  obj);

    //KKOpenUrlOk=0,          /***播发器打开成功**/
    //KKOpenUrlOkFailure=1,   /**播发器打开失败***/
    //KKAVNotStream=2,
    //KKAVReady=3,            ///缓冲已经准备就绪
    //KKAVWait=4,             ///需要缓冲
    //KKRealTimeOver=5,
    //KKEOF=6,                ///文件结束了。
    //KKAVOver=7              ///视频播放结束
    //KKSeekOk=8,              ///Seek成功
    //KKSeekErr=9,             ///seek失败
    public native  int  KKGetPlayerState (CKKPlayerCtx   obj);
    public native  int  KKIsReady(CKKPlayerCtx  obj);
    public native  int  KKGetRealtime(CKKPlayerCtx   obj);

    //得到延迟
    public native  int KKGetRealtimeDelay(CKKPlayerCtx  obj);
    //设置延迟
    public native  int KKSetMinRealtimeDelay(CKKPlayerCtx  obj,int value);
    //强制刷新缓存队列
    public native  void KKForceFlushQue(CKKPlayerCtx  obj);
    /***********UI调用***********/
    //Movie Pause
    public native void Pause(CKKPlayerCtx   obj,int pause);

    //seek 时间单位秒
    public native void Seek(CKKPlayerCtx  obj,int value);
    //获取播放信息
    public native int GetKKMediaInfo(CKKPlayerCtx  obj, CkkMediaInfo Info);


    //创建一个音频转pcm
    public native int   CreateAudioToPcm();
    public native int   SetAudioToPcmCall(int  obj,IKKAudioToPcm icall);
    public native void  AudioToPcmSetVol(int obj,int vol);
    //打开文件开始转码成pcm，支持16bit。阻塞
    public native int   AudioToPcmOpenFile(int  obj,String Filepath,int ch_layout,int sample_rate);
    public native int   GetAudioToPcmState(int obj);
    //暂停或者开始
    public native int   AudioToPcmPause(int  obj,int pause);
    //删除
    public native int   DelAudioToPcm(int  obj);
}