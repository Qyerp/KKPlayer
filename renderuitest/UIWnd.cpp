#include "UIWnd.h"



namespace KKUI
{
	IRenderFactory*     CUIWnd::G_pIRenderFactory = 0;
	void CUIWnd::SetRenderFactory(IRenderFactory*  pIRenderFactory )
	{
	    G_pIRenderFactory = pIRenderFactory;
	}
	CUIWnd::CUIWnd(KKUIHandle    pUIRoot):m_pUIRoot(pUIRoot),m_pIRenderTarget(0)
	{
	     assert(m_pUIRoot);
		
	//	 assert(m_pIRenderTarget);
	}
	CUIWnd::~CUIWnd()
	{
	
	}

	HWND CUIWnd::Create(HWND hWndParent,int x, int y , int nWidth, int nHeight)
	{
	      return Create(hWndParent, WS_CLIPCHILDREN | WS_TABSTOP | WS_OVERLAPPED,0,x,y,nWidth,nHeight);
	}
	HWND CUIWnd::Create(HWND hWndParent,DWORD dwStyle,DWORD dwExStyle, int x, int y, int nWidth, int nHeight)
	{
		 if (NULL != m_hWnd)
			return m_hWnd;

		HWND hWnd = CSimpleWnd::Create(L"HOSTWND",dwStyle,dwExStyle, x,y,nWidth,nHeight,hWndParent,NULL);
		if(!hWnd) 
			return NULL;


    
		if(nWidth==0 || nHeight==0) 
			CenterWindow(hWndParent);

		G_pIRenderFactory->CreateRenderTarget(&m_pIRenderTarget,(void*)hWnd,nWidth,nHeight);
		 m_pIRenderTarget->ReSize(nWidth,nHeight);
        return hWnd;
	}

     KKUIHandle  CUIWnd::FindCtlName(const char* pszName , int nDeep)
	 {
	     return fpUI_FindCtlName(m_pUIRoot,pszName ,nDeep);
	 }
	 void CUIWnd::OnSize(UINT nType, CSize size)
	 {
		
        
		 if(m_pIRenderTarget&&size.cx>0&&size.cy>0){
			 m_pIRenderTarget->ReSize(size.cx,size.cy); 
			 fpUI_Relayout(m_pUIRoot,0,0,size.cx,size.cy);
		 }
		  
		
	 }
	 void CUIWnd::OnPaint(HDC dc)
	 {
		 PAINTSTRUCT ps;
         dc=::BeginPaint(m_hWnd, &ps);
	     if(G_pIRenderFactory->BeforePaint(m_pIRenderTarget))
		     fpUI_Opain(m_pUIRoot,m_pIRenderTarget);
		 G_pIRenderFactory->EndPaint(m_pIRenderTarget);
		 ::EndPaint(m_hWnd, &ps);
	 }
	  BOOL  CUIWnd::OnEraseBkgnd(HDC dc)
	  {
	     return 1;
	  }
	 LRESULT CUIWnd::OnMouseEvent(UINT uMsg, WPARAM wParam, LPARAM lParam)
	 {
		     SetMsgHandled(TRUE);

			 /*if(uMsg==WM_MOUSEMOVE)
						::OutputDebugString(L"MC\n");*/
	         switch(uMsg)
			 {
					case WM_LBUTTONDOWN:
					case WM_LBUTTONDBLCLK:
					case WM_RBUTTONDOWN:
					case WM_RBUTTONDBLCLK:
					case WM_MBUTTONDOWN:
					case WM_MBUTTONDBLCLK:
						m_msgMouseLast.message = uMsg;
						m_msgMouseLast.wParam = wParam;
						m_msgMouseLast.lParam = lParam;
						if(CSimpleWnd::GetStyle()&WS_CHILD) 
							CSimpleWnd::SetFocus();//子窗口情况下才自动获取焦点
						break;
					case WM_LBUTTONUP:
					case WM_MBUTTONUP:
					case WM_RBUTTONUP:
						m_msgMouseLast.message = 0;
						break;
					case WM_MOUSEMOVE:
						m_msgMouseLast.message = 0;
						m_msgMouseLast.wParam = wParam;
						m_msgMouseLast.lParam   = lParam;
						break;
					case WM_MOUSELEAVE:
						m_msgMouseLast.message = uMsg;
						m_msgMouseLast.wParam = 0;
						m_msgMouseLast.lParam   = 0;
						break;
					default:
						break;
			}

			 fpUI_FrameEvent(m_pUIRoot,uMsg,wParam,lParam);
				//DoFrameEvent(uMsg,wParam,lParam);    //将鼠标消息转发到SWindow处理
			return 0;
	 }

	 BOOL    CUIWnd::OnSetCursor(HWND hWnd, UINT nHitTest, UINT message)
	 {
			if(hWnd!=m_hWnd) 
				  return FALSE;
			if(nHitTest==HTCLIENT)
			{
				CPoint pt;
				GetCursorPos(&pt);
				ScreenToClient(&pt);
				return  fpUI_FrameEvent(m_pUIRoot,WM_SETCURSOR,0,MAKELPARAM(pt.x,pt.y))!=0;
			}
			return DefWindowProc()!=0;
	 }
	 void    CUIWnd::OnDestroy()
	 {
		 if(m_pUIRoot)
	          fpUI_OnDestroy(m_pUIRoot);
			  m_pUIRoot=0;
	 }
	 int     CUIWnd::UISendMessage(unsigned int Msg, unsigned int wParam, unsigned long lParam)
	 {
		
	     return SendMessage( Msg,wParam,lParam);
	 }
	 void    CUIWnd::UIInvalidateRect(int l,int r,int t,int b)
	 {
		
	   RECT rt;
	   rt.left  =  l ;
	   rt.right =  r ;
	   rt.bottom = b;
	   rt.top =t;
	   InvalidateRect(&rt,1);
	 }
	 void    CUIWnd::UIInvalidate()
	 {
            Invalidate(1);
	 }
	 void    CUIWnd::UISetCursor(const char* name)
	 {
		 CSimpleWndHelper* ins = CSimpleWndHelper::GetInstance();
		 HCURSOR h = 0;
		 if (strcmp(name, "sizeall")==0){
			 h = ::LoadCursor(NULL, IDC_SIZEALL);
		 }
		 else if (strcmp(name, "arrow") == 0)
		 {
			 h = ::LoadCursor(NULL, IDC_ARROW);
		 }
		 else if (strcmp(name, "sizewe") == 0)
		 {
			 h = ::LoadCursor(NULL, IDC_SIZEWE);
		 }else if (strcmp(name, "sizens") == 0)
		 {
			 h = ::LoadCursor(NULL, IDC_SIZENS);
		 }else
		 {
			 h = ::LoadCursorA(ins->GetAppInstance(), name);
		 }
		 ::SetCursor(h);
	 }
	  void    CUIWnd::OnUIEvent(const UIEvent* ev)
	  {
	    int ii=0;
		ii++;
	  }
}