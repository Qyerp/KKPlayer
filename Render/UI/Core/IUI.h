#ifndef IUI_H_
#define IUI_H_
#include "IRender.h"
#include "IUIMessage.h"
//UI控件
typedef void*        KKUIHandle;
//容器控件
typedef void*        KKUIContainer;

//YUV控件
typedef void*        KKUIYUVHandle;

//初始化
typedef void         (*KKUI_Ini)(KKUI::IRenderFactory* rf);
//加载skin
typedef int          (*KKUI_LoadXmlSkin)(char *xmlSkin);
///创建一个UI控件
typedef KKUIHandle   (*KKUI_CreateCtl)         (char* uiname);

typedef  void        (*KKUI_InsertChild)(KKUIHandle root,KKUIHandle ctl);
typedef  bool        (*KKUI_RemoveChild)(KKUIHandle root,KKUIHandle ctl); 

//获取控件属性
typedef int          (*KKUI_GetCtlAttr)        (KKUIHandle handle,char* AttrName ,char* OutAttr,int Attrlen);
typedef void         (*KKUI_SetVisible)        (KKUIHandle handle,int visible);
//设置容器message回调
typedef void         (*KKUI_SetUIMessage)      (KKUIContainer handle,KKUI::IUIMessage* call);

//设置呈现工厂
typedef void         (*KKUI_SetIRenderFactory) (KKUIContainer handle,KKUI::IRenderFactory* pRenderFactory);


//查找控件，根据控件名称
typedef KKUIHandle   (*KKUI_FindCtlName)       (KKUIHandle handle,const char* strValue, int bLoading);


//获取控件名称
typedef int          (*KKUI_GetCtlName)         (KKUIHandle handle,char* OutName, int NameLen);

//获取控件window大小
typedef void         (*KKUI_GetCtlWindowRect)   (KKUIHandle handle,kkRect* OutRect);

//设置控件属性
typedef void         (*KKUI_SetCtlAttr)         (KKUIHandle handle,const char* attr,const char* attrvalue);


//获取用户数据
typedef void*        (*KKUI_GetCtlUserData)     (KKUIHandle handle);

//设置用户数据
typedef void         (*KKUI_SetCtlUserData)     (KKUIHandle handle,void* UserData);

///绘制，绘制的控件必须为容器控件
typedef void         (*KKUI_Opain)              (KKUIContainer handle,KKUI::IRenderTarget* ren);



//舞台控件绘制，必须为舞台控件
typedef void         (*KKUI_StageOpain)           (KKUIHandle handle,KKUI::IRenderTarget* ren);
//添加融合控件
typedef int          (*KKUI_StageAddFusionzone)   (KKUIHandle handle,KKUIHandle Fusionzone);
//移除融合
typedef void         (*KKUI_StageRemoveFusionzone)(KKUIHandle handle,KKUIHandle Fusionzone);
//
typedef int          (*KKUI_FusionzoneInitRegion) (KKUIHandle handle,SUVInfo* infos,int count);
typedef void         (*KKUI_FusionzoneGetScale)   (KKUIHandle handle,SUVInfo& fUv1,SUVInfo& fUv2);
typedef void         (*KKUI_FusionzoneSetScale)   (KKUIHandle handle,SUVInfo& fUv1,SUVInfo& fUv2);
//Curve控制点设置
typedef void         (*KKUI_CurveGetScale)        (KKUIHandle handle,SUVInfo& fUv1,SUVInfo& fUv2);
typedef void         (*KKUI_CurveSetScale)        (KKUIHandle handle,SUVInfo& fUv1,SUVInfo& fUv2);
//获取相交的控件
typedef int          (*KKUI_GetStageIntersectCtls)(KKUIHandle handle,unsigned char **Ctls,int Len);
//获取舞台控件的IWarp;
typedef void*        (*KKUI_StageGetIWarp)      (KKUIHandle handle,int* enable);
typedef int          (*KKUI_StageGetWarpInfos)  (KKUIHandle handle,SUVInfo* infos,int count);
typedef int          (*KKUI_StageSetWarpInfos)  (KKUIHandle handle,SUVInfo* infos,int count);
typedef void         (*KKUI_StageSetWarping)    (KKUIHandle handle,int Warp);
typedef void         (*KKUI_StageSetXYDiv)      (KKUIHandle handle,int x,int y);
typedef void         (*KKUI_StageGetXYDiv)      (KKUIHandle handle,int *Outx,int *Outy);
//设置一个Stage的引用
typedef int          (*KKUI_StageEditorSetStageRef) (KKUIHandle StageEditor,KKUIHandle StageClt);

//请求布局
typedef void         (*KKUI_Relayout)           (KKUIContainer handle,int x,int y,int cx,int cy);
//事件处理
typedef int          (*KKUI_FrameEvent)         (KKUIContainer handle,unsigned int uMsg,unsigned int wParam,long lParam);
//窗口销毁时调用
typedef void         (*KKUI_OnDestroy)          (KKUIContainer handle);

//用xml描述创建控件
typedef KKUIHandle   (*KKUI_CreateChildren)     (KKUIHandle handle,char* strxml);
//移动控件位置
typedef int          (*KKUI_MoveChild)          (KKUIHandle handle, KKUIHandle  *pChild,KKUIHandle *pMoveAfter);

//删除文件
typedef int          (*KKUI_DeleteCtl)          (KKUIHandle RootHandle,KKUIHandle Handle);

//加载YUV数据
typedef int          (*KKUI_YUVLoad)                (KKUIYUVHandle handle,kkPicInfo *info);
/**************添加滤镜****************/
typedef int          (*KKUI_YUVAddFilter)           (KKUIYUVHandle handle,SinkFilter* filter);
typedef SinkFilter*  (*KKUI_YUVGetFristFilter)      (KKUIYUVHandle handle);
typedef void         (*KKUI_YUVDelFilter)           (KKUIYUVHandle handle,int Id);

typedef int          (*KKUI_YUVGetMaskCount)    (KKUIYUVHandle handle);
typedef int          (*KKUI_YUVGetMaskInfo)     (KKUIYUVHandle handle,int id, SUVInfo* pPts,int ptCount,int* HidePixel);
typedef int          (*KKUI_YUVSetMaskInfo)     (KKUIYUVHandle handle,int id, SUVInfo* pPts,int ptCount);

//获取yuv的控件位置（相对坐标），
typedef int          (*KKUI_YUVGetFRegion)       (KKUIYUVHandle handle,SUVInfo* pPts,int ptCount);
typedef void         (*KKUI_YUVSetFRegion)       (KKUIYUVHandle handle,SUVInfo* pPts,int ptCount);

typedef void         (*KKUI_YUVSetIsRect)       (KKUIYUVHandle handle,int IsRect);
typedef int          (*KKUI_YUVGetIsRect)       (KKUIYUVHandle handle);

typedef void         (*KKUI_YUVSavePreFrame)    (KKUIYUVHandle handle,bool Save);


//设置焦点控件
typedef void         (*KKUI_SetCapture)         (KKUIContainer handle,KKUIHandle clt);
typedef void         (*KKUI_Invalidate)         (KKUIHandle handle);

//激活图层控件,如果父控件是MsView
typedef void         (*KKUI_ActiveYUVCtl)                  (KKUIHandle clt);

//获取显示比列
typedef float         (*KKUI_GetMsViewDisplayScale)        (KKUIHandle  MsCtl);
typedef void          (*KKUI_SetMsViewDisplayScale)        (KKUIHandle  MsCtl,float Scale);
typedef void          (*KKUI_SrocllViewGetOffXY)           (KKUIHandle  MsCtl,int* x,int* y,int* WinWidth,int* WinHeight);


//创建组
typedef void*        (*KKUI_CreateYuvGroupMsView)          (KKUIHandle  MsCtl);
//删除组
typedef int          (*KKUI_DelYuvGroupMsView)             (KKUIHandle  MsCtl,void* group);
//设置Group是否可见
typedef int          (*KKUI_SetMsViewGroupVisible)         (KKUIHandle  MsCtl,void* Group, int visble);
typedef void         (*KKUI_MsViewGroupAddYuv)             (KKUIHandle  MsCtl,void* Group,KKUIHandle yuv);
typedef void         (*KKUI_MsViewGroupDelYuv)             (KKUIHandle  MsCtl,void* Group,KKUIHandle yuv);
typedef void         (*KKUI_MsViewDelGroup)                (KKUIHandle  MsCtl,void* Group);
typedef void         (*KKUI_MsViewGroupSwopYuv)            (KKUIHandle  MsCtl,void* Group,KKUIHandle srcYuv,KKUIHandle DestYuv,int pre);
typedef void         (*KKUI_MsViewInsertYuvByGroup)        (KKUIHandle  MsCtl,void* Group,KKUIHandle srcYuv,int indexId);
//设置编辑模式
typedef void         (*KKUI_MsViewEditModel)               (KKUIHandle  MsCtl,int EditModel);

//
typedef int          (*KKUI_YUVCtlAddMark)                 (KKUIHandle YUVclt,int MarkType);
typedef void         (*KKUI_YUVMaskChangePixel)            (KKUIHandle YUVclt, int maskId, int HidePixel);
typedef void         (*KKUI_YUVCtlDelMark)                 (KKUIHandle YUVclt,int MarkId);

//设置一个YUV的引用
typedef int          (*KKUI_SliceEditorAddIYuvCtlRef)      (KKUIHandle SliceEditor,KKUIHandle YUVclt);
//设置一个YUV的引用
typedef int          (*KKUI_YuvCtlAddIYuvCtlRef)           (KKUIHandle YUVclt1,    KKUIHandle YUVclt2);
typedef int          (*KKUI_YuvCtlSetTxt)                  (KKUIHandle YuvClt,wchar_t* strTxt);
typedef	void         (*KKUI_YuvCtlSetRotate)               (KKUIHandle YuvClt,int Rotate);
typedef int          (*KKUI_YuvCtlGetRotate)               (KKUIHandle YuvClt);
typedef	void         (*KKUI_YuvCtlClearPicData)            (KKUIHandle YuvClt,unsigned int Color);
//获取图层的剪切，单位坐标
typedef void         (*KKUI_YuvCtlGetPicClip)              (KKUIHandle YuvClt,SUVInfo* pPts,int ptCount);
//创建缓冲
typedef void         (*KKUI_YuvCtlCreatePicCache)          (KKUIHandle handle,int width,int height,int format);
//设置剪切坐标
typedef void         (*KKUI_YuvCtlSetPicClip)               (KKUIHandle YuvClt,SUVInfo* pPts,int ptCount);


typedef int          (*KKUI_GridPanelLoadYUV)              (void* handle,int row,int col,kkPicInfo *info,wchar_t* strTxt);
typedef int          (*KKUI_GridPanelSetItemTxt)           (void* handle,int row,int col,wchar_t* strTxt);
typedef int          (*KKUI_GridPanelSetItemUserData)      (void* handle,int row,int col,void* userData);
typedef int          (*KKUI_GridPanelSetRowCol)            (void* handle,int nRow,int nCol);
typedef int          (*KKUI_GridPanelClearData)            (void* handle,unsigned int cr);
typedef int          (*KKUI_GridPanelItemClearData)        (void* handle,int row,int col,unsigned int cr);
typedef void*        (*KKUI_GetYUVGridPanelItem)           (void* handle,int row,int col);

//创建d3d9呈现器
typedef void         (*CreateRenderFactoryD3D9) (KKUI::IRenderFactory** ff);
//创建d3d11呈现器
typedef void         (*CreateRenderFactoryD3D11) (KKUI::IRenderFactory** ff);


//增加引用计数
typedef int          (*KKUI_AddRef)             (KKUIHandle handle);

//释放引计数
typedef int          (*KKUI_Release)            (KKUIHandle handle);

//获取资源文件大小
typedef int          (*KKUI_GetRawBufferSize)   (char* strType,char* pszResName);

//获取资源
typedef int          (*KKUI_GetRawBuffer)       (char* strType,char* pszResName,void* pBuf,int size);


typedef int          (*KKUI_SetGetResCallInfo)  (KKUI_GetRawBufferSize fp1,KKUI_GetRawBuffer fp2);

//初始化默认皮肤
typedef int          (*KKUI_InitDefSkin)        ();


extern KKUI_Ini                           fpUI_Ini        ;
extern KKUI_LoadXmlSkin                   fpUI_LoadXmlSkin;
extern KKUI_CreateCtl                     fpUI_CreateCtl  ;
extern KKUI_InsertChild                   fpUI_InsertChild;
extern KKUI_RemoveChild                   fpUI_RemoveChild;
extern KKUI_GetCtlAttr                    fpUI_GetCtlAttr ;

extern KKUI_SetVisible                    fpUI_SetVisible;

extern KKUI_SetUIMessage                  fpUI_SetUIMessage;

extern KKUI_SetIRenderFactory             fpUI_SetIRenderFactory;

extern KKUI_FindCtlName                   fpUI_FindCtlName;

extern KKUI_GetCtlName                    fpUI_GetCtlName;

extern KKUI_GetCtlWindowRect              fpUI_GetCtlWindowRect;

extern KKUI_SetCtlAttr                    fpUI_SetCtlAttr;

//获取用户数据
extern KKUI_GetCtlUserData                fpUI_GetCtlUserData;
//设置用户数据
extern KKUI_SetCtlUserData                fpUI_SetCtlUserData;

extern KKUI_Opain                         fpUI_Opain      ;

//舞台控件
extern KKUI_StageOpain                    fpUI_StageOpain;
extern KKUI_StageAddFusionzone            fpUI_StageAddFusionzone;
extern KKUI_StageRemoveFusionzone         fpUI_StageRemoveFusionzone;


extern KKUI_FusionzoneInitRegion          fpUI_FusionzoneInitRegion;
extern KKUI_FusionzoneGetScale            fpUI_FusionzoneGetScale;
extern KKUI_FusionzoneSetScale            fpUI_FusionzoneSetScale;

extern KKUI_CurveGetScale                 fpUI_CurveGetScale;
extern KKUI_CurveSetScale                 fpUI_CurveSetScale;

extern KKUI_GetStageIntersectCtls         fpUI_GetStageIntersectCtls;
extern KKUI_StageGetIWarp                 fpUI_StageGetIWarp;
extern KKUI_StageGetWarpInfos             fpUI_StageGetWarpInfos;
extern KKUI_StageSetWarpInfos             fpUI_StageSetWarpInfos;
extern KKUI_StageSetWarping               fpUI_StageSetWarping;
extern KKUI_StageSetXYDiv                 fpUI_StageSetXYDiv;
extern KKUI_StageGetXYDiv                 fpUI_StageGetXYDiv;
extern KKUI_StageEditorSetStageRef        fpUI_StageEditorSetStageRef;

extern KKUI_Relayout                      fpUI_Relayout   ;

extern KKUI_FrameEvent                    fpUI_FrameEvent ;

extern KKUI_OnDestroy                     fpUI_OnDestroy;

extern KKUI_CreateChildren                fpUI_CreateChildren;
extern KKUI_MoveChild                     fpUI_MoveChild;

extern KKUI_DeleteCtl                     fpUI_DeleteCtl;

//加载YUV数据
extern KKUI_YUVLoad                       fpUI_YUVLoad;
extern KKUI_YUVGetMaskCount               fpUI_YUVGetMaskCount;
extern KKUI_YUVGetMaskInfo                fpUI_YUVGetMaskInfo;
extern KKUI_YUVSetMaskInfo                fpUI_YUVSetMaskInfo;
extern KKUI_YUVGetFRegion                 fpUI_YUVGetFRegion;
extern KKUI_YUVSetFRegion                 fpUI_YUVSetFRegion;
extern KKUI_YUVSetIsRect                  fpUI_YUVSetIsRect;
extern KKUI_YUVGetIsRect                  fpUI_YUVGetIsRect;

extern KKUI_GridPanelLoadYUV              fpUI_GridPanelLoadYUV;
extern KKUI_GridPanelSetItemTxt           fpUI_GridPanelSetItemTxt;
extern KKUI_GridPanelSetItemUserData      fpUI_GridPanelSetItemUserData;
extern KKUI_GridPanelSetRowCol            fpUI_GridPanelSetRowCol;
extern KKUI_GridPanelClearData            fpUI_GridPanelClearData;
extern KKUI_GetYUVGridPanelItem           fpUI_GetYUVGridPanelItem;

//设置焦点控件
extern KKUI_SetCapture                    fpUI_SetCapture;
extern KKUI_Invalidate                    fpUI_Invalidate;
extern KKUI_ActiveYUVCtl                  fpUI_ActiveYUVCtl;

extern KKUI_GetMsViewDisplayScale         fpUI_GetMsViewDisplayScale;
extern KKUI_SetMsViewDisplayScale         fpUI_SetMsViewDisplayScale;
extern KKUI_SrocllViewGetOffXY            fpUI_SrocllViewGetOffXY;


//创建组
extern KKUI_CreateYuvGroupMsView          fpUI_CreateYuvGroupMsView;
//删除组
extern KKUI_DelYuvGroupMsView             fpUI_DelYuvGroupMsView;
extern KKUI_SetMsViewGroupVisible         fpUI_SetMsViewGroupVisible;
extern KKUI_MsViewGroupAddYuv             fpUI_MsViewGroupAddYuv;
extern KKUI_MsViewGroupDelYuv             fpUI_MsViewGroupDelYuv;
extern KKUI_MsViewDelGroup                fpUI_MsViewDelGroup;
extern KKUI_MsViewGroupSwopYuv            fpUI_MsViewGroupSwopYuv;
extern KKUI_MsViewEditModel               fpUI_MsViewEditModel;
extern KKUI_MsViewInsertYuvByGroup        fpUI_MsViewInsertYuvByGroup;


extern KKUI_YUVCtlAddMark                 fpUI_YUVCtlAddMark;
extern KKUI_YUVMaskChangePixel            fpUI_YUVMaskChangePixel;
extern KKUI_YUVCtlDelMark                 fpUI_YUVCtlDelMark;

extern KKUI_SliceEditorAddIYuvCtlRef      fpUI_SliceEditorAddIYuvCtlRef;
extern KKUI_YuvCtlAddIYuvCtlRef           fpUI_YuvCtlAddIYuvCtlRef;


extern KKUI_YuvCtlSetTxt                  fpUI_YuvCtlSetTxt;
extern KKUI_YuvCtlSetRotate               fpUI_YuvCtlSetRotate;
extern KKUI_YuvCtlGetRotate               fpUI_YuvCtlGetRotate;
extern KKUI_YuvCtlClearPicData            fpUI_YuvCtlClearPicData;
extern KKUI_YUVSavePreFrame               fpUI_YUVSavePreFrame;
extern KKUI_YuvCtlGetPicClip              fpUI_YuvCtlGetPicClip;
extern KKUI_YuvCtlCreatePicCache          fpUI_YuvCtlCreatePicCache;
extern KKUI_YuvCtlSetPicClip              fpUI_YuvCtlSetPicClip;

extern KKUI_YUVAddFilter                  fpUI_YUVAddFilter;
extern KKUI_YUVGetFristFilter             fpUI_YUVGetFristFilter;
extern KKUI_YUVDelFilter                  fpUI_YUVDelFilter;


extern CreateRenderFactoryD3D9            fpCreateRenderFactoryD3D9 ;

extern KKUI_AddRef                        fpUI_AddRef;

extern KKUI_Release                       fpUI_Release;

extern KKUI_SetGetResCallInfo             fpUI_SetGetResCallInfo;
extern KKUI_InitDefSkin                   fpUI_InitDefSkin;
#endif