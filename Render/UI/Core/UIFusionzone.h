#ifndef UIFusionzone_H_
#define UIFusionzone_H_
#include "UIMoveWindow.h"
#include "IDisplayScale.h"
#include <vector>
#include "Warp/IWarp.h"
#include "Fusionzone/IFusionzone.h"
//融合带
namespace KKUI
{
	enum EFusionType
	{
	   EFT_LEFT     = 0,//左边
	   EFT_RIGHT    = 1,//右边
	   EFT_TOP      = 2,//顶部
	   EFT_BOTTOM   = 3,//底部部
	};
    class  CUIFusionzone: public  CUIMoveWindow,public IDisplayScale,public IFusionzone
    {
        KKUI_CLASS_NAME( CUIMoveWindow,CUIFusionzone,"uifusionzone")
		public:
				 CUIFusionzone(void);
				 ~CUIFusionzone(void);
				 int            GetCtlAttr(const SStringA& attrName,SStringA& OutAttr);
				 void           OnUpdateFloatPosition(const  CSize&  szView,const CRect & rcParent);
				 
				 CUIWindow*     WndFromPoint( kkPoint  ptHitTest, int bOnlyText);
				 int            IsContainPoint(const kkPoint &pt,int bClientOnly);
				 int            OnNcHitTest(kkPoint pt);
				 int            InitRegion(SUVInfo* infos,int count);
				 void           GetScale(SUVInfo& fUv1,SUVInfo& fUv2);
				 void           SetScale(SUVInfo& fUv1,SUVInfo& fUv2);
				 void           DrawFusionzone(IRenderTarget* pRT,kkRect& rt,int syn);
				 float          GetDisplayScale(){return  m_fDisplayScale;}
		protected:
			     CUIWindow*     SetCapture();
				 int            OnCreate( int);
				 void           OnPaint( IRenderTarget  *pRT);
				 void           OnEraseBkgnd( IRenderTarget  *pRT);
				 void           OnSize(unsigned int nType, int x,int y);


				 void           OnLButtonDown  (unsigned int nFlags,CPoint pt);
				 void           OnLButtonDBLClk(unsigned int nFlags,CPoint pt);

				 void           OnLButtonUp(UINT nFlags, CPoint point);
				 void           OnRButtonUp(unsigned int nFlags,CPoint pt);
			
			     void           OnNcLButtonUp(UINT nFlags,CPoint pt);

				 void           OnNcLButtonDown(unsigned int nFlags,CPoint pt);
				 void           SetDisplayScale(float fDisplayScale);
				 void           OnMouseMove(unsigned int nFlags,CPoint pt);
				 void           OnKeyMove(EKEY_MOVE KeyMove);
				 void           OnNcMouseMove (unsigned int nFlags,CPoint pt);
				 void           OnNcMouseLeave(unsigned int nFlags);
                 void          OnMouseLeave(unsigned int nFlags);
			
				 KKUI_MSG_MAP_BEGIN()
				         MSG_UI_CREATE(OnCreate)
						 MSG_UI_PAINT(OnPaint)
						 MSG_UI_SIZE(OnSize)

                         MSG_UI_LBUTTONDOWN  (OnLButtonDown)
						 MSG_UI_LBUTTONDBLCLK(OnLButtonDBLClk)
						 MSG_UI_NCLBUTTONDOWN(OnNcLButtonDown)


						 	MSG_UI_MOUSELEAVE(OnMouseLeave)
						 MSG_UI_MOUSEMOVE(OnMouseMove)
						 MSG_UI_NCMOUSEMOVE(OnNcMouseMove)
						 MSG_UI_NCMOUSELEAVE(OnNcMouseLeave)
						 MSG_UI_RBUTTONUP(OnRButtonUp)
						 MSG_UI_NCLBUTTONUP(OnNcLButtonUp)
			             MSG_UI_LBUTTONUP(OnLButtonUp)
						 MSG_UI_ERASEBKGND(OnEraseBkgnd)
				 KKUI_MSG_MAP_END(CUIMoveWindow)

				 KKUI_ATTRS_BEGIN()
							ATTR_INT("Balance", m_nBalance,0)
							ATTR_INT("FusionType",m_nFusionType,0)
							ATTR_COLOR("ChannelColor",m_nChannelColor,0);
				 KKUI_ATTRS_ENDEX(CUIMoveWindow)

		 private: 

			     void              OnDraw(IRenderTarget  *pRT,CRect& Rt,int syn=1);
			     SUVInfo           m_fUv1;
			     SUVInfo           m_fUv2;
				 //平衡模式
                 int               m_nBalance;
				 

			     float             m_fDisplayScale;
			     int               m_nFusionType;
			     unsigned  int     m_nChannelColor;

				 PtSize            m_PtSize;
				 
			 
    };

}

#endif