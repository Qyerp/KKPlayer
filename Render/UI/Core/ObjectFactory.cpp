#include "ObjectFactory.h"
#include <assert.h>
namespace KKUI
{

	CObjectFactoryMgr::CObjectFactoryMgr(void)
	{
	
	}
    bool CObjectFactoryMgr::RegisterFactory(CObjectFactory & objFactory, bool bReplace)
    {
		CObjectInfo info=objFactory.GetObjectInfo();
        if (HasKey(info)){
			if (!bReplace) 
				return false;
			RemoveKeyObject(objFactory.GetObjectInfo());
		}
		AddKeyObject(objFactory.GetObjectInfo(), objFactory.Clone());
		return true;
    }
	bool CObjectFactoryMgr::UnregisterFactory(const CObjectInfo & objInfo)
	{
	    return  RemoveKeyObject(objInfo);
	}
	IObject* CObjectFactoryMgr::CreateObject(const CObjectInfo & objInfo) const
	{
			if(!HasKey(objInfo))
			{
				assert(0);
				return NULL;
			}
			IObject * pRet = GetKeyObject(objInfo)->NewObject();
			return pRet;
	}
}