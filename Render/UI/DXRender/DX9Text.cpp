#include "DX9Text.h"
#include "RenderTarget_D3D9.h"
#include "intreadwrite.h"

extern "C" {
#include "libavformat/avformat.h"
}
namespace KKUI
{
	
	
	CDX9Texture::CDX9Texture(SRenderFactory_D3D9*  pRenderFactory):
		m_pRenderFactory(pRenderFactory),m_nTextureCount(8),m_nTextureOrder(0)
	{
	     memset(m_DX9TextureInfos, 0, sizeof(m_DX9TextureInfos));
	}
	CDX9Texture::~CDX9Texture()
	{
		int ii = 0;
		ii++;
	}
    void      CDX9Texture::Release2()
	{
	     HRESULT  hr=  S_OK ;
		 //释放Surface资源
	     for(unsigned i = 0; i < m_nTextureCount; i++)
		 {
			 if(m_DX9TextureInfos[i].d3d)
			 {
				 hr= m_DX9TextureInfos[i].d3d->Release();		 
				 while(hr>S_OK)
				 {
					hr =  m_DX9TextureInfos[i].d3d->Release();
					m_DX9TextureInfos[i].used = 0;
					if (FAILED(hr)){
						printf("Error!\n"); 
					}
				 }
			 }
		     m_DX9TextureInfos[i].d3d=NULL;
	     }
	}
	void      CDX9Texture::ReLoadRes()
	{
	
		
	}
	
	int    CDX9Texture::GetTextureIndex(void *ffFrame,int format,int Width,int Height)
	{
	        int i=0, unused=-1;
			AVFrame* ff =(AVFrame*)ffFrame;
			if (m_pRenderFactory->LostDevice() || m_nTextureCount < 1) {
				return -1;
			}
				

			for (i = 0; i <m_nTextureCount; i++) 
			{
				DX9TextureInfo*   surface = &m_DX9TextureInfos[i];
				if (!surface->used )
				{
					unused = i;
				}
				
			}
			if (unused == -1) {
				//::MessageBoxA(0, "unused", "unused", 0);
				return -1;
			}
			DX9TextureInfo* surface = &m_DX9TextureInfos[unused];

			surface->used = true;
			surface->usedCount++;

			
			memset(ff->data, 0, sizeof(ff->data));
			memset(ff->linesize, 0, sizeof(ff->linesize));
			memset(ff->buf, 0, sizeof(ff->buf));

			if(!surface->d3d){
			   surface->d3d = CreateTexture(format,Width,Height);
			   if(surface->d3d)
               surface->d3d->AddRef();
			   surface->format = format;
			}else if(surface->format!=format)
			{
			   if(surface->d3d)
			   surface->d3d->Release();
			   surface->d3d = CreateTexture(format,Width,Height);
               surface->d3d->AddRef();
			   surface->format = format;
			}
			ff->data[0]=ff->data[3] = (uint8_t *)surface->d3d;
			surface->d3d->AddRef();
			
			return unused;
	}
	IDirect3DTexture9* CDX9Texture::CreateTexture(int format,int Width,int Height)
	{
		D3DFORMAT ff = (D3DFORMAT)format;
	    IDirect3DTexture9*   pTexture = 0;// m_pRenderFactory->GetFromFileInMemory(data, len);
		HRESULT hr =m_pRenderFactory->GetDevice()->CreateTexture(Width,Height,
				1,
			    D3DUSAGE_DYNAMIC,
			ff,//D3DFMT_R5G6B5,
				D3DPOOL_DEFAULT,
				&pTexture,
				NULL) ;
		return pTexture;
	}
	int    CDX9Texture::ReleaseTexture(void *Texture)
	{
	        LPDIRECT3DTEXTURE9 pSurface = (LPDIRECT3DTEXTURE9)Texture;
			for (int i = 0; i < m_nTextureCount; i++)
			{
				if (m_DX9TextureInfos[i].d3d == pSurface&&Texture) {
					m_DX9TextureInfos[i].used = false;
					m_DX9TextureInfos[i].usedCount--;
					int ii = pSurface->Release();
					if (ii == 0)
						ii++;
					return 1;
				}
			}
			return 0;
	}
		   
	//检查设备是否丢失
	int    CDX9Texture::GetLostDevice()
	{
	   return m_pRenderFactory->LostDevice();
	}
	//Surface 是否有效
	int    CDX9Texture::IsTextureValid(void *Texture,int SurfaceId,int &Outformat)
	{
	         LPDIRECT3DTEXTURE9 pSurface = (LPDIRECT3DTEXTURE9)Texture;
	         if(SurfaceId >=m_nTextureCount)
				 return 0;
			 if (m_DX9TextureInfos[SurfaceId].d3d == Texture&&Texture!=0) {
				 Outformat=m_DX9TextureInfos[SurfaceId].format;
				 return 1;
			 }
			 return 0;
	}
	void   CDX9Texture::SetCurFormat(int ff)
	{
	      m_nCurformat=ff;
	}
	int    CDX9Texture::GetCurFormat()
	{
	return m_nCurformat;
	
	}
	#define FFALIGN(x, a) (((x)+(a)-1)&~((a)-1))

	static UINT BitsPerPixel(D3DFORMAT fmt)
	{
		UINT fmtU = (UINT)fmt;
		switch (fmtU)
		{
		case D3DFMT_A32B32G32R32F:
			return 128;

		case D3DFMT_A16B16G16R16:
		case D3DFMT_Q16W16V16U16:
		case D3DFMT_A16B16G16R16F:
		case D3DFMT_G32R32F:
			return 64;

		case D3DFMT_A8R8G8B8:
		case D3DFMT_X8R8G8B8:
		case D3DFMT_A2B10G10R10:
		case D3DFMT_A8B8G8R8:
		case D3DFMT_X8B8G8R8:
		case D3DFMT_G16R16:
		case D3DFMT_A2R10G10B10:
		case D3DFMT_Q8W8V8U8:
		case D3DFMT_V16U16:
		case D3DFMT_X8L8V8U8:
		case D3DFMT_A2W10V10U10:
		case D3DFMT_D32:
		case D3DFMT_D24S8:
		case D3DFMT_D24X8:
		case D3DFMT_D24X4S4:
		case D3DFMT_D32F_LOCKABLE:
		case D3DFMT_D24FS8:
		case D3DFMT_INDEX32:
		case D3DFMT_G16R16F:
		case D3DFMT_R32F:
			return 32;

		case D3DFMT_R8G8B8:
			return 24;

		case D3DFMT_A4R4G4B4:
		case D3DFMT_X4R4G4B4:
		case D3DFMT_R5G6B5:
		case D3DFMT_L16:
		case D3DFMT_A8L8:
		case D3DFMT_X1R5G5B5:
		case D3DFMT_A1R5G5B5:
		case D3DFMT_A8R3G3B2:
		case D3DFMT_V8U8:
		case D3DFMT_CxV8U8:
		case D3DFMT_L6V5U5:
		case D3DFMT_G8R8_G8B8:
		case D3DFMT_R8G8_B8G8:
		case D3DFMT_D16_LOCKABLE:
		case D3DFMT_D15S1:
		case D3DFMT_D16:
		case D3DFMT_INDEX16:
		case D3DFMT_R16F:
		case D3DFMT_YUY2:
			return 16;

		case D3DFMT_R3G3B2:
		case D3DFMT_A8:
		case D3DFMT_A8P8:
		case D3DFMT_P8:
		case D3DFMT_L8:
		case D3DFMT_A4L4:
			return 8;

		case D3DFMT_DXT1:
			return 4;

		case D3DFMT_DXT2:
		case D3DFMT_DXT3:
		case D3DFMT_DXT4:
		case D3DFMT_DXT5:
			return  8;

			// From DX docs, reference/d3d/enums/d3dformat.asp
			// (note how it says that D3DFMT_R8G8_B8G8 is "A 16-bit packed RGB format analogous to UYVY (U0Y0, V0Y1, U2Y2, and so on)")
		case D3DFMT_UYVY:
			return 16;

			// http://msdn.microsoft.com/library/default.asp?url=/library/en-us/directshow/htm/directxvideoaccelerationdxvavideosubtypes.asp
		case MAKEFOURCC('A', 'I', '4', '4'):
		case MAKEFOURCC('I', 'A', '4', '4'):
			return 8;

		case MAKEFOURCC('Y', 'V', '1', '2'):
			return 12;

#if !defined(D3D_DISABLE_9EX)
		case D3DFMT_D32_LOCKABLE:
			return 32;

		case D3DFMT_S8_LOCKABLE:
			return 8;

		case D3DFMT_A1:
			return 1;
#endif // !D3D_DISABLE_9EX

		default:
			assert(FALSE); // unhandled format
			return 0;
		}
	}
	static void GetSurfaceInfo(UINT width, UINT height, D3DFORMAT fmt, UINT* pNumBytes, UINT* pRowBytes, UINT* pNumRows)
	{
		UINT numBytes = 0;
		UINT rowBytes = 0;
		UINT numRows = 0;

		if (fmt == D3DFMT_DXT1 || fmt == D3DFMT_DXT2 || fmt == D3DFMT_DXT3 || fmt == D3DFMT_DXT4 || fmt == D3DFMT_DXT5)
		{
			int numBlocksWide = 0;
			if (width > 0)
				numBlocksWide = max(1, width / 4);
			int numBlocksHigh = 0;
			if (height > 0)
				numBlocksHigh = max(1, height / 4);
			int numBytesPerBlock = (fmt == D3DFMT_DXT1 ? 8 : 16);
			rowBytes = numBlocksWide * numBytesPerBlock;
			numRows = numBlocksHigh;
		}
		else
		{
			UINT bpp = BitsPerPixel(fmt);
			rowBytes = (width * bpp + 7) / 8; // round up to nearest byte
			numRows = height;
		}
		numBytes = rowBytes * numRows;
		if (pNumBytes != NULL)
			*pNumBytes = numBytes;
		if (pRowBytes != NULL)
			*pRowBytes = rowBytes;
		if (pNumRows != NULL)
			*pNumRows = numRows;
	}


	void*  CDX9Texture::Decode(void *Texture,int index,int width,int height,FpDecompress fp,void *userctx)
	{
	
		int format = 0;
		if(IsTextureValid(Texture,index, format))
		{
			int cwidth  = FFALIGN(width,16);
			int cheight = FFALIGN(height,16);
			IDirect3DTexture9*   pTexture =(IDirect3DTexture9* ) Texture;
			
			
			D3DLOCKED_RECT d3d_rect;
			HRESULT   hr = pTexture->LockRect(0, &d3d_rect, 0,  D3DLOCK_DISCARD);
			if (FAILED( hr)){
						 assert(0);
			}


		UINT RowBytes = 0;
			UINT NumRows = 0;
			GetSurfaceInfo(width, height, ( D3DFORMAT)format, NULL, &RowBytes, &NumRows);
			if(fp&&d3d_rect.pBits)
				   fp(RowBytes, NumRows,(char*)d3d_rect.pBits,userctx);	/**/
			 

		   pTexture->UnlockRect(0);
		}

		//   m_pRenderFactory->SaveFile(L"D:/text.bmp", pTexture);
		   return 0;
	}
}
