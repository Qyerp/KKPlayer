#include "GlEsYuv.h"
#define  LOG_TAG    "libgl2jni"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)
extern const char* g_Yuv_Vertex_shader;
extern const char* g_Yuv_Frag_shader;
void checkGlError(const char* op);
GLuint buildProgram(const char* StrVShader,const char* StrFragShader,GLuint& nVShader,GLuint& nFraShader);

CGlEsYuv::CGlEsYuv():
 m_nTexYId   (0),
 m_nTexUId   (0),
 m_nTexVId   (0),
 m_nProgram  (0),
 m_nVShader  (0),
 m_nFraShader(0),
 m_nAv4_pos  (0),
 m_nAv2_Texpos(0)
{
	
	
}
CGlEsYuv::~CGlEsYuv()
{
	
	
}
void CGlEsYuv::Init()
{
	if(m_nProgram==0)
	{
		m_nProgram = buildProgram(g_Yuv_Vertex_shader, g_Yuv_Frag_shader, m_nVShader,m_nFraShader);
		
		if(m_nProgram>0)
		{
			m_nTex_y = glGetUniformLocation(m_nProgram, "tex_y");
			m_nTex_u  = glGetUniformLocation(m_nProgram, "tex_u");
			m_nTex_v  = glGetUniformLocation(m_nProgram, "tex_v");
			
			m_Mvp         = glGetUniformLocation(m_nProgram, "um4_ModelViewProjection");
			glUniform1i(m_nTex_y, 0);
			glUniform1i(m_nTex_u, 1);
			glUniform1i(m_nTex_v, 2);
			glGenTextures(1, &m_nTexYId);
			glGenTextures(1, &m_nTexUId);
			glGenTextures(1, &m_nTexVId);
					
			GLuint txtId[3]={m_nTexYId,m_nTexUId,m_nTexVId};
			for(int i=0;i<3;i++)
			{
				 glActiveTexture(GL_TEXTURE0+i);
				 glBindTexture(GL_TEXTURE_2D, txtId[i]);
				 
				 glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
				 glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
				 glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
				 glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			}
			m_nAv4_pos    = glGetAttribLocation(m_nProgram, "av4_Position");
		    m_nAv2_Texpos = glGetAttribLocation(m_nProgram, "av2_Texcoord");
			LOGE_KK("m_nProgram:%d,Av4_pos:%d Texpos:%d \n",m_nProgram,m_nAv4_pos,m_nAv2_Texpos);
	  
		}
		
	}
	
}
void CGlEsYuv::LoadDtata(kkAVPicInfo *Picinfo)
{
	if(m_nProgram<0)
		return;
	if(Picinfo!=NULL&&Picinfo->width!=0&&Picinfo->height!=0)
    {
				LOGI("Picinfo w:%d,h:%d \n",Picinfo->width,Picinfo->height);
				
                if(Picinfo->picformat==(int)AV_PIX_FMT_MEDIACODEC)
					LOGI("MEDIACODEC  AV_PIX_FMT_MEDIACODEC \n");
				
				if(Picinfo->picformat!=(int)AV_PIX_FMT_MEDIACODEC)
				{
					
					int     planes[3]          = { 0, 1, 2 };
					const GLsizei widths[3]    = { Picinfo->linesize[0], Picinfo->linesize[1], Picinfo->linesize[2] };
				    //const GLsizei widths[3]    = { Picinfo->width, (Picinfo->width+1)/2, (Picinfo->width+1)/2};
					const GLsizei heights[3]   = { Picinfo->height,Picinfo->height / 2,     Picinfo->height / 2 };
					//***********************************Y***********************U**************************************V
					const GLubyte *pixels[3]   = {(GLubyte *)Picinfo->data[0], (GLubyte *)Picinfo->data[1] ,  (GLubyte *)Picinfo->data[2] };
					GLuint  plane_textures[]   = {m_nTexYId,m_nTexUId,m_nTexVId};
					
					//LOGI(" yuv %d,%d,%d wh:%d-%d\n",Picinfo->linesize[0], Picinfo->linesize[1], Picinfo->linesize[2], Picinfo->width,Picinfo->height);
					 for (int i = 0; i < 3; ++i) 
					 {
						
						glActiveTexture(GL_TEXTURE0+i);
						glBindTexture(GL_TEXTURE_2D, plane_textures[i]);

						//glPixelStorei(GL_UNPACK_ROW_LENGTH, widths2[i])
						glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
						glTexImage2D(GL_TEXTURE_2D,
									 0,
									 GL_LUMINANCE,
									 widths[i],
									 heights[i],
									 0,
									 GL_LUMINANCE,
									 GL_UNSIGNED_BYTE,
									 pixels[i]);
						checkGlError("glTexImage2D");
				     }
	            }
    }else if(Picinfo){
		LOGI("Picinfo err  %d,%d \n",Picinfo->width,Picinfo->height);
	}else {
		
		LOGI("Picinfo err  null \n");
	}
}
//zsalx  an123456
void CGlEsYuv::OnRender(GLfloat* m, GLfloat* AVPos)
{

	   if(m_nProgram<1)
		return;
	   glUseProgram(m_nProgram);
	   
	    glUniformMatrix4fv(m_Mvp, 1, GL_FALSE, m);	
	    glVertexAttribPointer(m_nAv4_pos, 2, GL_FLOAT, GL_FALSE, 0, AVPos);
        glEnableVertexAttribArray(m_nAv4_pos);
	    checkGlError("m_nAv4_pos");
		GLfloat AVTexcoords[8]={0.0};
		AVTexcoords[0]=0.0f;
		AVTexcoords[1]=1.0f;
		
		AVTexcoords[2]=1.0f;
		AVTexcoords[3]=1.0f;
		
		AVTexcoords[4]=0.0f;
		AVTexcoords[5]=0.0f;
		
		AVTexcoords[6]=1.0f;
		AVTexcoords[7]=0.0f;
	
	   glVertexAttribPointer(m_nAv2_Texpos, 2, GL_FLOAT, GL_FALSE, 0,AVTexcoords);
       glEnableVertexAttribArray(m_nAv2_Texpos);
	   checkGlError("m_nAv2_Texpos");
	
	   //LOGI("m_nProgram:%d,Av4_pos:%d Texpos:%d \n",m_nProgram,m_nAv4_pos,m_nAv2_Texpos);
	  
	   glActiveTexture(GL_TEXTURE0);
	   glBindTexture(GL_TEXTURE_2D, m_nTexYId);
	   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
       glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
       glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
       glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	   glUniform1i(m_nTex_y, 0);
	 
	   
	   glActiveTexture(GL_TEXTURE1);
	   glBindTexture(GL_TEXTURE_2D, m_nTexUId);
	   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
       glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
       glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
       glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
       glUniform1i(m_nTex_u, 1);
  
	   glActiveTexture(GL_TEXTURE2);
	   glBindTexture(GL_TEXTURE_2D,m_nTexVId);
	   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
       glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
       glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
       glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	   glUniform1i(m_nTex_v, 2);
		
		//checkGlError(" glBindTexture");	
	   glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	   checkGlError("glDrawArrays");
}
