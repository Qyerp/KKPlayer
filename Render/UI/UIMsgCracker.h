﻿#ifndef KKUI_UIMsgCracker_H_
#define KKUI_UIMsgCracker_H_
#include "Core/TypeDef.h"

#define KKUI_MSG_MAP_BEGIN()                                       \
protected:                                                          \
    virtual int ProcessUIWndMessage(                              \
    unsigned int uMsg,  uint_ptr wParam,                        \
    int_ptr lParam, long& lResult)                            \
    {


#define KKUI_MSG_MAP_END(parentclass)                                        \
    if (!IsMsgHandled())                                        \
    return parentclass::ProcessUIWndMessage(                   \
    uMsg, wParam, lParam, lResult);                     \
    return 1;                                                \
    }

#define KKUI_MSG_MAP_END_BASE()                                    \
    return UIWndProc(uMsg,wParam,lParam,lResult);               \
    }


#define MSG_UI_DESTROY(func) \
    if (uMsg == UI_DESTROY) \
    { \
        SetMsgHandled(1); \
        func(); \
        lResult = 0; \
        if(IsMsgHandled()) \
            return 1; \
    }


// void OnPaint(KKUI::IRenderTarget * pRT)
#define MSG_UI_PAINT(func) \
    if (uMsg == UI_PAINT ) \
    { \
    SetMsgHandled(1); \
    func((KKUI::IRenderTarget *)wParam); \
    lResult = 0; \
    if(IsMsgHandled()) \
    return 1; \
    }

// void OnPaint(KKUI::IRenderTarget * pRT,const CRect* rt)
#define MSG_UI_PAINT_EX(func) \
    if (uMsg == UI_PAINT_EX ) \
    { \
    SetMsgHandled(1); \
    func((KKUI::IRenderTarget *)wParam,(CRect*)lParam); \
    lResult = 0; \
    if(IsMsgHandled()) \
    return 1; \
    }

#define MSG_UI_ERASEBKGND(func) \
    if (uMsg == UI_ERASEBKGND) \
	{ \
		SetMsgHandled(1); \
		func((KKUI::IRenderTarget *)wParam); \
		lResult = 0; \
		if(IsMsgHandled()) \
		return 1; \
	}

#define MSG_UI_NCPAINT(func) \
    if (uMsg == UI_NCPAINT) \
{ \
    SetMsgHandled(1); \
    func((KKUI::IRenderTarget *)wParam); \
    lResult = 0; \
    if(IsMsgHandled()) \
    return 1; \
}


#define MSG_UI_SIZE(func) \
    if (uMsg == UI_SIZE) \
    { \
        SetMsgHandled(1); \
        func((uint_ptr)wParam, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam)); \
        lResult = 0; \
        if(IsMsgHandled()) \
            return 1; \
    }

#define MSG_UI_MOUSEMOVE(func) \
    if (uMsg == UI_MOUSEMOVE) \
    { \
        SetMsgHandled(1); \
        func((uint_ptr)wParam, KKUI::CPoint(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam))); \
        lResult = 0; \
        if(IsMsgHandled()) \
            return 1; \
    }

#define MSG_UI_NCMOUSEMOVE(func) \
    if (uMsg == UI_NCMOUSEMOVE) \
    { \
        SetMsgHandled(1); \
        func((uint_ptr)wParam, KKUI::CPoint(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam))); \
        lResult = 0; \
        if(IsMsgHandled()) \
            return 1; \
    }

// void OnNcMouseLeave((unsigned int)wParam)
#define MSG_UI_NCMOUSELEAVE(func) \
    if(uMsg==UI_NCMOUSELEAVE)\
{\
    SetMsgHandled(TRUE); \
    func((uint_ptr)wParam); \
    lResult = 0; \
    if(IsMsgHandled()) \
    return TRUE; \
}

// void OnNcMouseLeave(unsigned int uFlag)
#define MSG_UI_MOUSELEAVE(func) \
    if(uMsg==UI_MOUSELEAVE)\
{\
    SetMsgHandled(TRUE); \
    func((uint_ptr)wParam); \
    lResult = 0; \
    if(IsMsgHandled()) \
    return TRUE; \
}


// void OnLButtonDown(UINT nFlags, CPoint point)
#define MSG_UI_LBUTTONDOWN(func) \
    if (uMsg == UI_LBUTTONDOWN) \
    { \
        SetMsgHandled(1); \
        func((uint_ptr)wParam, KKUI::CPoint(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam))); \
        lResult = 0; \
        if(IsMsgHandled()) \
            return 1; \
    }
//
#define MSG_UI_LBUTTONDBLCLK(func) \
    if (uMsg == UI_LBUTTONDBLCLK) \
    { \
        SetMsgHandled(1); \
        func((uint_ptr)wParam, KKUI::CPoint(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam))); \
        lResult = 0; \
        if(IsMsgHandled()) \
            return 1; \
    }


// void OnLButtonUp(UINT nFlags, CPoint point)
#define MSG_UI_LBUTTONUP(func) \
    if (uMsg == UI_LBUTTONUP) \
    { \
        SetMsgHandled(1); \
        func((uint_ptr)wParam, KKUI::CPoint(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam))); \
        lResult = 0; \
        if(IsMsgHandled()) \
            return 1; \
    }

#define MSG_UI_NCLBUTTONDOWN(func) \
    if (uMsg == UI_NCLBUTTONDOWN) \
    { \
        SetMsgHandled(1); \
        func((uint_ptr)wParam, KKUI::CPoint(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam))); \
        lResult = 0; \
        if(IsMsgHandled()) \
            return 1; \
    }
#define MSG_UI_NCLBUTTONUP(func) \
    if (uMsg == UI_NCLBUTTONUP) \
    { \
        SetMsgHandled(1); \
        func((uint_ptr)wParam, KKUI::CPoint(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam))); \
        lResult = 0; \
        if(IsMsgHandled()) \
            return 1; \
    }


#define MSG_UI_RBUTTONUP(func) \
    if (uMsg == UI_RBUTTONUP) \
    { \
        SetMsgHandled(TRUE); \
        func((UINT)wParam, KKUI::CPoint(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam))); \
        lResult = 0; \
        if(IsMsgHandled()) \
            return TRUE; \
    }

#define MSG_UI_VSCROLL(func) \
    if (uMsg == UI_VSCROLL) \
    { \
        SetMsgHandled(TRUE); \
        func((int)LOWORD(wParam), (short)HIWORD(wParam), (int)lParam); \
        lResult = 0; \
        if(IsMsgHandled()) \
            return TRUE; \
    }

#define MSG_UI_HSCROLL(func) \
    if (uMsg == UI_HSCROLL) \
    { \
        SetMsgHandled(TRUE); \
        func((int)LOWORD(wParam), (short)HIWORD(wParam), (int)lParam); \
        lResult = 0; \
        if(IsMsgHandled()) \
            return TRUE; \
    }

#define MSG_UI_CREATE(func) \
    if (uMsg == UI_CREATE) \
    { \
        SetMsgHandled(TRUE); \
        lResult = (int)func((int)lParam); \
        if(IsMsgHandled()) \
            return TRUE; \
    }

#define MSG_UI_NCCALCSIZE(func) \
    if (uMsg == UI_NCCALCSIZE) \
    { \
        SetMsgHandled(TRUE); \
        lResult = func((BOOL)wParam, lParam); \
        if(IsMsgHandled()) \
            return TRUE; \
    }

// void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
#define MSG_UI_KEYDOWN(func) \
    if (uMsg == UI_KEYDOWN) \
    { \
        SetMsgHandled(TRUE); \
        func((UINT)wParam, (UINT)lParam & 0xFFFF, (UINT)((lParam & 0xFFFF0000) >> 16)); \
        lResult = 0; \
        if(IsMsgHandled()) \
            return TRUE; \
    }

#endif