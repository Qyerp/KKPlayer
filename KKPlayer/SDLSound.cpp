#include "SDLSound.h"

#include "../KKPlayerCore/Includeffmpeg.h"

#include "../KKPlayerCore/KKInternal.h"
#include <ObjBase.h>
static   CKKLock S_SDLLock;
CSDLSound::CSDLSound():
m_pSdlAudio(0),
m_Vol(100),

m_UserData(NULL),
m_pFun(NULL)
{
   
}
void  CSDLSound::SetPlayer(KKPlayer* pPlayer)
{
m_pPlayer = pPlayer;
}
CSDLSound::~CSDLSound()
{
   CloseAudio();
}
void CSDLSound::SetWindowHAND(int m_hwnd)
{

	CKKGurd gd(S_SDLLock);
	static int IniSdl =0;
	if(!IniSdl){
		SDL_setenv("SDL_AUDIODRIVER","directsound",1);
		//SDL_LogSetPriority(SDL_LOG_CATEGORY_APPLICATION, SDL_LOG_PRIORITY_INFO);
		// HRESULT err =  CoInitialize(0);
		int    flags =  SDL_INIT_AUDIO | SDL_INIT_TIMER;
		if (SDL_Init (flags)) 
		{
		   const char* pp= SDL_GetError();
		   int i=0;
		   i++;
		}

		
#ifndef USE_SDL2
		SDL_HandleAudio(m_hwnd);
#else
		

#endif
		IniSdl=1;
	}
 
}

	 /********设置音频回到函数*********/
void CSDLSound::SetAudioCallBack(pfun fun,void* UserData)
{
	CKKGurd gd(m_lock);
	m_UserData=UserData;
	m_pFun=fun;
}
	 /***********初始化音频设备*********/

void sdl_audio_callback(void *userdata, Uint8 *stream, int len)
{
	 CSDLSound* m_pFun=(CSDLSound* )userdata;
	 m_pFun->KKSDLCall( stream,len);
}

void  CSDLSound::KKSDLCall(Uint8 *stream, int len)
{
	CKKGurd gd(m_lock);
	void* UserData =  m_pPlayer->GetVideoState();
	SKK_VideoState* info =   m_pPlayer->GetVideoState();
	if(m_pFun!=NULL&&info )
	{
		//memset(buf,0,buf_len);
		m_pFun(info,(char*)stream,len);
		if(m_Vol!=100)
		{
			double ff=(double)m_Vol/100;
			RaiseVolume((char*)stream, len, 1, ff);
		}
	}else if(UserData!=UserData){
	   int ii=9;
	   ii++;
	}
}
int  CSDLSound::OpenAudio( int &wanted_channel_layout, int &wanted_nb_channels,                     int &wanted_sample_rate)
{


	CKKGurd gd(S_SDLLock);
	if(m_pSdlAudio){

#ifdef USE_SDL2
	   SDL_PauseAudioDevice(m_pSdlAudio,1);
	   SDL_CloseAudioDevice(m_pSdlAudio);
#else
	   SDL_PauseAudio(m_pSdlAudio,1);
	   SDL_CloseAudio(m_pSdlAudio);
#endif
	   m_pSdlAudio=0;

	  
	}
	/*int wanted_sample_rate=32000;
	int wanted_nb_channels =2;
	int wanted_channel_layout =0;*/
	SDL_AudioSpec wanted_spec, spec;
	const char *env;
	static const int next_nb_channels[] = {0, 0, 1, 6, 2, 6, 4, 6};
	static const int next_sample_rates[] = {0, 44100, 48000, 96000, 192000};
	int next_sample_rate_idx = FF_ARRAY_ELEMS(next_sample_rates) - 1;

//
#ifdef USE_SDL2
	const char* name = SDL_GetCurrentAudioDriver();
#endif
//	wanted_nb_channels = 2;
//   wanted_channel_layout = av_get_default_channel_layout(wanted_nb_channels);
//#else
	env = SDL_getenv("SDL_AUDIO_CHANNELS");
	if (env) 
	{
		wanted_nb_channels = atoi(env);
		wanted_channel_layout = av_get_default_channel_layout(wanted_nb_channels);
	}
//#endif
	if (!wanted_channel_layout || wanted_nb_channels != av_get_channel_layout_nb_channels(wanted_channel_layout)) {
		wanted_channel_layout = av_get_default_channel_layout(wanted_nb_channels);
		wanted_channel_layout &= ~AV_CH_LAYOUT_STEREO_DOWNMIX;
	}
	
	
	wanted_spec.channels = wanted_nb_channels;
	wanted_spec.freq = wanted_sample_rate;
	if (wanted_spec.freq <= 0 || wanted_spec.channels <= 0) 
	{
		return -1;
	}
	while (next_sample_rate_idx && next_sample_rates[next_sample_rate_idx] >= wanted_spec.freq)
		next_sample_rate_idx--;
	wanted_spec.format = AUDIO_S16SYS;
	wanted_spec.silence = 0;
	int ll=SDL_AUDIO_MIN_BUFFER_SIZE;
	int xx= 2 << av_log2(wanted_spec.freq / SDL_AUDIO_MAX_CALLBACKS_PER_SEC);
	wanted_spec.samples = FFMAX(SDL_AUDIO_MIN_BUFFER_SIZE, 2 << av_log2(wanted_spec.freq / SDL_AUDIO_MAX_CALLBACKS_PER_SEC));
	//wanted_spec.samples /=4;
	wanted_spec.callback =sdl_audio_callback;
	wanted_spec.userdata = this;
	
#ifndef USE_SDL2
	while ( (m_pSdlAudio=SDL_OpenAudio(&wanted_spec, &spec)) ==NULL) {
	
		wanted_spec.channels = next_nb_channels[FFMIN(7, wanted_spec.channels)];
		if (!wanted_spec.channels) {
			wanted_spec.freq = next_sample_rates[next_sample_rate_idx--];
			wanted_spec.channels = wanted_nb_channels;
			if (!wanted_spec.freq) {
				return -1;
			}
		}
		wanted_channel_layout = av_get_default_channel_layout(wanted_spec.channels);
	}
#else

	//while ( (m_pSdlAudio=SDL_OpenAudioDevice("directsound", 0, &wanted_spec, &spec,SDL_AUDIO_ALLOW_FREQUENCY_CHANGE | SDL_AUDIO_ALLOW_CHANNELS_CHANGE)) ==NULL) {
	{
		
	
		m_pSdlAudio= SDL_OpenAudioDevice(0, 0, &wanted_spec, &spec,SDL_AUDIO_ALLOW_FREQUENCY_CHANGE | SDL_AUDIO_ALLOW_CHANNELS_CHANGE);
		while (!m_pSdlAudio ) {
	
			wanted_spec.channels = next_nb_channels[FFMIN(7, wanted_spec.channels)];
			if (!wanted_spec.channels) {
				wanted_spec.freq = next_sample_rates[next_sample_rate_idx--];
				wanted_spec.channels = wanted_nb_channels;
				if (!wanted_spec.freq) {
					return -1;
				}
			}
			wanted_channel_layout = av_get_default_channel_layout(wanted_spec.channels);
			m_pSdlAudio= SDL_OpenAudioDevice(0, 0, &wanted_spec, &spec,SDL_AUDIO_ALLOW_FREQUENCY_CHANGE | SDL_AUDIO_ALLOW_CHANNELS_CHANGE);
		}
	}

	 //while (!(audio_dev = )) {
#endif


	
  

#ifndef USE_SDL2
	SDL_PauseAudio(m_pSdlAudio,1);
#else
	SDL_PauseAudioDevice(m_pSdlAudio,1);
#endif

	if (spec.format != AUDIO_S16SYS) 
	{   
		return -1;
	}	

	return spec.size;
}
 /*******读取音频数据********/
 bool CSDLSound::ReadAudio()
 {
	 return false;
 }
 void CSDLSound::Start()
 {
	  CKKGurd gd(m_lock);
	  if(m_pSdlAudio)
#ifdef USE_SDL2
	  SDL_PauseAudioDevice(m_pSdlAudio,0);
#else
	  SDL_PauseAudio(m_pSdlAudio,0);
#endif
 } 
 void CSDLSound::Stop()
 {
	   CKKGurd gd(m_lock);
	  if(m_pSdlAudio)
#ifdef USE_SDL2
	 SDL_PauseAudioDevice(m_pSdlAudio,1);
#else
	 SDL_PauseAudio(m_pSdlAudio,1);
#endif
 }   
 /*********关闭**********/
 void CSDLSound::CloseAudio()
 {
	 CKKGurd gd(S_SDLLock);
	 if(m_pSdlAudio)
	 {
#ifdef USE_SDL2
	 SDL_CloseAudioDevice(m_pSdlAudio);
#else
     SDL_CloseAudio(m_pSdlAudio);
#endif
	 m_pSdlAudio = 0;
	 m_UserData  = 0;
	 }

 }	
 /*********设置音量************/
 void CSDLSound::SetVolume(long value)
 {
        m_Vol=value;
 }
 long CSDLSound::GetVolume()
 {
	 return m_Vol;
 }