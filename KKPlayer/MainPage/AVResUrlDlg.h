#ifndef AVResUrlDlg_H
#define AVResUrlDlg_H
#include "../stdafx.h"
#include "../../KKPlayerCore/KKPlayer.h"
#include "core/SHostDialog.h"
namespace SOUI
{
	class CMainDlg;
	class CAVResUrlDlg: public SHostDialog
	{
	   public:
		      CAVResUrlDlg(CMainDlg *m_pDlgMain);
		       ~CAVResUrlDlg();
	   protected:
			    
				void OnClose();
				void OnOk();
				///�ļ�����
				void OnBtnAvFile();
				void SelectStoreDir();
				void OnbtnSaveDir();
				BOOL OnInitDialog( HWND hWnd, LPARAM lParam );
				EVENT_MAP_BEGIN()
					EVENT_NAME_COMMAND(L"btn_close",OnClose)
					EVENT_NAME_COMMAND(L"btnCancell",OnClose)
					EVENT_NAME_COMMAND(L"btnOk",OnOk)
					EVENT_NAME_COMMAND(L"btnAvFile",OnBtnAvFile)
					EVENT_NAME_COMMAND(L"btnStoreDir",SelectStoreDir);
				    EVENT_NAME_COMMAND(L"btnSaveDir", OnbtnSaveDir);
				EVENT_MAP_END()	
				BEGIN_MSG_MAP_EX(CAVResUrlDlg)
					MSG_WM_INITDIALOG(OnInitDialog)
					CHAIN_MSG_MAP(SHostDialog)
					REFLECT_NOTIFICATIONS_EX()
				END_MSG_MAP()/**/
				CMainDlg *m_pDlgMain;


	};
}
#endif