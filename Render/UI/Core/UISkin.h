#ifndef KKUI_UISkin_H_
#define KKUI_UISkin_H_
#include "UIBase.h"
#include "SkinObjBase.h"

#include "UIMisc.h"
///皮肤对象
namespace KKUI{
	// State Define
	enum
	{
		WndState_Normal       = 0x00000000UL,
		WndState_Hover        = 0x00000001UL,
		WndState_PushDown     = 0x00000002UL,
		WndState_Check        = 0x00000004UL,
		WndState_Invisible    = 0x00000008UL,
		WndState_Disable      = 0x00000010UL,
	};
	class CSkinImgList: public CSkinObjBase
	{
		  KKUI_CLASS_NAME( CSkinObjBase,CSkinImgList, "imglist")
		public:
			CSkinImgList();
			virtual ~CSkinImgList();

			virtual kkSize     GetSkinSize();
			virtual int        IgnoreState();
		    
			virtual int        GetStates();
			virtual void       SetStates(int nStates){m_nStates=nStates;}



			virtual bool SetImage(IBitmap *pImg)
			{
				m_pImg=pImg;
				return true;
			}

			virtual IBitmap * GetImage()
			{
				return m_pImg;
			}

			virtual void SetTile(int bTile) {m_bTile=bTile;}
			virtual int  IsTile()           {return m_bTile;}

			virtual void SetVertical(int bVertical){m_bVertical=bVertical;}
			virtual int  IsVertical(){return m_bVertical;}
		    
			virtual void OnColorize(COLORREF cr);

		protected:
			virtual void _Scale(ISkinObj *skinObj, int nScale);
			virtual void _Draw(IRenderTarget *pRT, kkRect* rcDraw,  unsigned int  dwState,BYTE byAlpha);

			virtual unsigned int GetExpandMode();
		    
			CAutoRefPtr<IBitmap> m_pImg;
			int                  m_nStates;
			int                  m_bTile;
			int                  m_bAutoFit;
			int                  m_bVertical;
			CAutoRefPtr<IBitmap> m_imgBackup;   //色调调整前的备分

		
		    FilterLevel          m_filterLevel;
			KKUI_ATTRS_BEGIN()
				ATTR_IMAGEAUTOREF("src", m_pImg,FALSE)            //skinObj引用的图片文件定义在uires.idx中的name属性。
				ATTR_INT("tile", m_bTile, FALSE)                  //绘制是否平铺,0--位伸（默认），其它--平铺
				ATTR_INT("autoFit",m_bAutoFit,FALSE)              //autoFit为0时不自动适应绘图区大小
				ATTR_INT("vertical", m_bVertical, FALSE)          //子图是否垂直排列，0--水平排列(默认), 其它--垂直排列
				ATTR_INT("states",m_nStates,FALSE)                //子图数量,默认为1
				ATTR_ENUM_BEGIN("filterLevel",FilterLevel,FALSE)
					ATTR_ENUM_VALUE("none",kNone_FilterLevel)
					ATTR_ENUM_VALUE("low",kLow_FilterLevel)
					ATTR_ENUM_VALUE("medium",kMedium_FilterLevel)
					ATTR_ENUM_VALUE("high",kHigh_FilterLevel)
                ATTR_ENUM_END(m_filterLevel)
			KKUI_ATTRS_END()
	};


	///////*************************CSkinImgFrame******************************////////
	class  CSkinImgFrame : public CSkinImgList
	{

		KKUI_CLASS_NAME(CSkinImgList,CSkinImgFrame, "imgframe")
		public:
			CSkinImgFrame();


			void SetMargin(const CRect rcMargin){m_rcMargin=rcMargin;}

			CRect GetMargin(){return m_rcMargin;}

		protected:
			virtual void _Draw(IRenderTarget *pRT, kkRect* rcDraw, unsigned int dwState,BYTE byAlpha);
			virtual unsigned int GetExpandMode();
			virtual void _Scale(ISkinObj *skinObj, int nScale);

			CRect m_rcMargin;

			KKUI_ATTRS_BEGIN()
				ATTR_RECT("margin" ,m_rcMargin,FALSE)           //九宫格4周
				ATTR_INT("left", m_rcMargin.left, FALSE)        //九宫格左边距
				ATTR_INT("top", m_rcMargin.top, FALSE)          //九宫格上边距
				ATTR_INT("right", m_rcMargin.right, FALSE)      //九宫格右边距
				ATTR_INT("bottom", m_rcMargin.bottom, FALSE)    //九宫格下边距
				ATTR_INT("margin-x", m_rcMargin.left=m_rcMargin.right, FALSE)//九宫格左右边距
				ATTR_INT("margin-y", m_rcMargin.top=m_rcMargin.bottom, FALSE)//九宫格上下边距
			KKUI_ATTRS_END()
	};



	/////////////////////////////////////////////////////////////////////////
	
	
	typedef struct tagSCROLLINFO
	{
		UINT    cbSize;
		UINT    fMask;
		int     nMin;
		int     nMax;
		UINT    nPage;
		int     nPos;
		int     nTrackPos;
	}   SCROLLINFO, *LPSCROLLINFO;

	enum SBSTATE{
		SBST_NORMAL=0,    //正常状态
		SBST_HOVER,        //hover状态
		SBST_PUSHDOWN,    //按下状态
		SBST_DISABLE,    //禁用状态
		SBST_INACTIVE,    //失活状态,主要针对两端的箭头
	};

	#define MAKESBSTATE(sbCode,nState1,bVertical) MAKELONG((sbCode),MAKEWORD((nState1),(bVertical)))
	#define SB_CORNOR        10
	#define SB_THUMBGRIPPER    11    //滚动条上的可拖动部分

	#define THUMB_MINSIZE    18

	class CSkinScrollbar : public CSkinImgList
	{
		    KKUI_CLASS_NAME(CSkinImgList,CSkinScrollbar, "scrollbar")
			public:

				CSkinScrollbar();

				//指示滚动条皮肤是否支持显示上下箭头
				virtual int HasArrow(){
					return TRUE;
				}
			    
				virtual int GetIdealSize()
				{
					if(!m_pImg) 
						return 0;
					return m_pImg->Width()/9;
				}

			protected:
				virtual void _Draw(IRenderTarget *pRT, kkRect* prcDraw, unsigned int dwState,BYTE byAlpha);
				//返回源指定部分在原位图上的位置。
				virtual CRect GetPartRect(int nSbCode, int nState,BOOL bVertical);
				virtual void _Scale(ISkinObj *skinObj, int nScale);

				int         m_nMargin;
				BOOL        m_bHasGripper;
				BOOL        m_bHasInactive;//有失活状态的箭头时，滚动条皮肤有必须有5行，否则可以是3行或者4行

				KKUI_ATTRS_BEGIN()
					ATTR_INT("margin",m_nMargin,FALSE)             //边缘不拉伸大小
					ATTR_INT("hasGripper",m_bHasGripper,FALSE)     //滑块上是否有帮手(gripper)
					ATTR_INT("hasInactive",m_bHasInactive,FALSE)   //是否有禁用态
				KKUI_ATTRS_END()
	};


}
#endif