#ifndef uiInfo_H_
#define uiInfo_H_
#include <string>
///用来记录一些UI的配置信息
class  CUiInfo
{
public:
	///获取
	static int                      GetStoreVolume();
	static void                     SetStoreVolume(int vol);
	static int                      GetMainTab();
	static void                     SetMainTab(int tab);
	static void                     SetResUrl(const char* url);
	static std::string              GetResUrl();
	///获取存储目录
	static std::string              GetStoreDir();
	static void                     SetStoreDir(const char* dir);
};
#endif