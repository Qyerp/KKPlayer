package com.ic70.kkplayer.kkplayer;
import com.ic70.kkplayer.sdk.CKKMoviePath;

import android.os.Environment;
import android.os.Message;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
//import java.util.logging.Handler;

/**
 * Created by saint on 2016/4/11.
 */

public class CFileManage extends java.lang.Thread
{
    private COs_KKHander m_MsgHander;
    private Object m_UserObj;
    private boolean m_Start=false;
    HashMap<String,String> m_PathMap=new HashMap<String,String>();
    public synchronized void start(COs_KKHander handel,Object userobj) {
        m_Start=true;
        m_MsgHander=handel;
        m_UserObj =userobj;
        super.start();
    }
    public void run()
    {
        String Path="/";
        String Extension="mp4";
        String state;
        state = Environment.getExternalStorageState();
        if(state.equals(Environment.MEDIA_MOUNTED))
        {
            Path = Environment.getExternalStorageDirectory().getAbsolutePath();
        }
        GetFiles(Path, Extension);
        Message message = new Message();


        COs_KKHander.Os_Message obj = m_MsgHander.new  Os_Message();
        message.what = COs_KKHander.MSG_LoadFile;
        message.obj= obj;
        obj.obj=lstFile;
        obj.obj2 =m_UserObj;

        m_MsgHander.sendMessage(message);
    }

    private List<CKKMoviePath> lstFile =new ArrayList<CKKMoviePath>(); //结果 List
    public void GetFiles(String Path, String Extension) //搜索目录，扩展名，是否进入子文件夹
    {
        if(m_PathMap.containsKey(Path))
        {
            return;
        }else
        {
            m_PathMap.put(Path,Path);
        }
        File[] files =new File(Path).listFiles();

        if(files!=null) {
            for (int i = 0; i < files.length; i++) {
                File f = files[i];
                if (f.isFile())
                {
                    String name=f.getPath().substring(f.getPath().length() - Extension.length());
                    if(name!=null)
                    {
                       name=name.toLowerCase();
                        if ( name.equals(Extension)) //判断扩展名
                        {
                            CKKMoviePath KKPath = new CKKMoviePath();
                            KKPath.MovieName=f.getName();
                            KKPath.MoviePath=f.getPath();
                            lstFile.add(KKPath);
                        }
                    }
                } else if (f.isDirectory()&&f.getPath().indexOf("/.") == -1&& !f.isHidden()) //忽略点文件（隐藏文件/文件夹）
                {
                        String name=f.getName().toLowerCase();

                        if(name=="tencent"||name=="data"|| (name.contains("com.")&&name.indexOf(".")>1))
                        {
                            return;
                        }
                        GetFiles(f.getPath(), Extension);
                }

            }
        }
    }
}
