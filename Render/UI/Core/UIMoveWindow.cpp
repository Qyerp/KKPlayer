#include "UIMoveWindow.h"
#include "SplitString.h"
#include "UIMsView.h"
namespace KKUI
{
    CUIMoveWindow::CUIMoveWindow(void):
			m_nFloatHAlign(HALIGN_RIGHT),
			m_nFloatVAlign(VALIGN_BOTTOM),
			m_nDistX(10),
			m_nDistY(10),
			m_bDraging(0),
			m_nIsLock(0),
			m_nMoveing(0)
    {
        m_bFloat =1;
		m_szView.cx = 0;
		m_szView.cy = 0;
    }

    CUIMoveWindow::~CUIMoveWindow(void)
    {
		if (this->GetContainer()) {
			if (this->GetContainer()->GetUIWndCapture() == this)
				ReleaseCapture();
		}
			
    }

	void CUIMoveWindow::GetWindowRect(kkRect* pRect)  const
	{
	    assert(pRect);
		CRect rcWnd = m_rcWindow;
		*pRect = rcWnd;
	}
	
	int  CUIMoveWindow::GetCtlAttr(const SStringA& attrName,SStringA& OutAttr)
	{
		   if(attrName.Compare("windowrect")==0){
			   CRect rt;
			   GetWindowRect(&rt);
			   rt.left  +=m_szView.cx;
			   rt.right +=m_szView.cx;

			   rt.top   +=m_szView.cy;
			   rt.bottom+=m_szView.cy;

		       OutAttr.Format("%d,%d,%d,%d",rt.left,rt.top,rt.right,rt.bottom);
			   return 1;
		   }else if(attrName.Compare("crborder")==0)
		   { 
			   OutAttr.Format("%d",this->m_style.m_crBorder);
		       return 1;
		   }
	       return  0;
	}

	int CUIMoveWindow::CheckMagnetisRect(CPoint* pt,int ptCount,SMagnetRegion& Mgegion)
	{
		
		 CUIWindow* uiParent = this->GetParent();
		 if(uiParent&&uiParent->IsClass("uimsview"))
		 {
              CUIMsView* msView = (CUIMsView* )uiParent;
              return msView->CheckMagnetisRect(this,pt,ptCount,Mgegion);
		 }

		 return 0;
	}
    void CUIMoveWindow::OnUpdateFloatPosition(const  CSize&  szView,const CRect & rcParent)
    {
		
		
		CRect rt;
		CUIWindow::GetWindowRect(&rt);
		int x=m_szView.cx -szView.cx;
		int y=m_szView.cy -szView.cy;
        m_szView = szView;
		
        CSize sz(GetLayoutParam()->GetSpecifiedSize(Horz).toPixelSize(GetScale()),GetLayoutParam()->GetSpecifiedSize(Vert).toPixelSize(GetScale()));

		
		CRect rcWnd(0,0,sz.cx,sz.cy);
		rcWnd.MoveToX(rt.left+x);
		rcWnd.MoveToY(rt.top+y);

		
		
       // Move(&rcWnd);
		OnRelayout(&rcWnd);
		
    }

    void CUIMoveWindow::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
	{
		EKEY_MOVE KeyMove;

		if(nChar == 37 ||  nChar == 38 || nChar == 39 || nChar == 40){
					if(nChar == 37){
						KeyMove = EKM_Left;
						//Left
					}else if(nChar == 38){
						//Up
						KeyMove = EKM_Top;
					}else if(nChar == 39){
						//Right
						KeyMove = EKM_Right;
					}else if(nChar == 40){
						// Down
						KeyMove = EKM_Bottom;
					}
					
					OnKeyMove(KeyMove);
		}
	}
	void  CUIMoveWindow::OnKeyMove(EKEY_MOVE KeyMove)
	{
	    CRect rt;
		CUIWindow::GetWindowRect(&rt);
		CPoint pt;
		pt.x =rt.left;
		pt.y =rt.top;
		m_ptClick = pt - rt.TopLeft();
        m_bDraging =1;
		if(KeyMove == EKM_Left)
		    pt.x -= 1;
		else if(KeyMove == EKM_Top)
            pt.y -= 1;
		else if(KeyMove == EKM_Right)
		     pt.x += 1;
        else if(KeyMove == EKM_Bottom)
			 pt.y += 1;
		OnMouseMove(0x0001,pt);
		m_bDraging =0;
		this->Invalidate();
	}
    void CUIMoveWindow::OnLButtonDown(unsigned int nFlags,CPoint pt)
    {
	  
		//ReleaseCapture();
		/*if (!WndFromPoint(pt, 0))
		{
			if(this->GetContainer()->GetUIWndCapture()==this)
				ReleaseCapture();
			return;
		}*/
        SetCapture();
		
		if(!m_nIsLock)
		{
		    m_bDraging =1;
		    CRect rt;
			CUIWindow::GetWindowRect(&rt);
			m_ptClick = pt - rt.TopLeft();

			GetContainer()->GetIUIMessage()->UISetCursor("sizeall");
		}
			
		
       // ::SetCursor(GETRESPROVIDER->LoadCursor(_T("sizeall")));
    }

	int CUIMoveWindow::OnSetCursor(const CPoint &pt)
	{
		if(m_bDraging ==1){
		    GetContainer()->GetIUIMessage()->UISetCursor("sizeall");
			return 1;
		}
		return CUIWindow::OnSetCursor(pt);
	}


    void CUIMoveWindow::OnLButtonUp(unsigned int nFlags,CPoint pt)
    {
		
		 UIEvent ev;
		 ev.Handle = this;
		 ev.type   = E_Click;
		 ev.pa1    = 0;
         
		 if(m_bDraging==EDrag_Client)
		 {
		     ev.type =E_CltMoveOver;
			 ev.pos1.Left=m_OldRect.left;
			 ev.pos1.Top=m_OldRect.top;

			 ev.pos1.Right=m_OldRect.right;
			 ev.pos1.Bottom=m_OldRect.bottom;

			
			 CRect rcWnd;
			 GetWindowRect(&rcWnd);

			 ev.pos2.Left   = rcWnd.left;
			 ev.pos2.Top    = rcWnd.top;

			 ev.pos2.Right  = rcWnd.right;
			 ev.pos2.Bottom = rcWnd.bottom;
		 }
         m_bDraging = 0;

		

		 GetContainer()->GetIUIMessage()->OnUIEvent(&ev);
		 
    }

	void  CUIMoveWindow::OnNcLButtonUp   (unsigned int nFlags,CPoint pt)
	{

	    ReleaseCapture();
        m_bDraging = 0;


		 UIEvent ev;
		 ev.Handle = this;
		 ev.type   = E_Click;
		 ev.pa1    = 0;

		 GetContainer()->GetIUIMessage()->OnUIEvent(&ev);
	}

    void CUIMoveWindow::OnNcLButtonDown  (unsigned int nFlags,CPoint pt)
	{
		
	    SetCapture();
		
		if(!m_nIsLock)
		{
           m_bDraging =EDrag_NcClient;
		   m_ptClick = pt;
		}
	}
	
    void CUIMoveWindow::OnMouseMove(unsigned int nFlags,CPoint pt)
    {
		if(m_nIsLock)
			return ;
        if(m_bDraging&&  nFlags & 0x0001 ==0x0001)
        {
            CPoint ptLT =pt - m_ptClick;
            CRect rcWnd;
			CUIWindow::GetWindowRect(&rcWnd);
            rcWnd.MoveToXY(ptLT);
            
            //Move(&rcWnd);
			OnRelayout(&rcWnd);
            SStringA Pos;
			Pos.Format("%d,%d,@%d,@%d",rcWnd.left,rcWnd.top,rcWnd.Width(),rcWnd.Height());
			this->SetAttribute("pos", Pos);
			this->GetParent()->UpdateChildrenPosition();

			 UIEvent ev;
			 ev.Handle = this;
			 ev.type   = E_CltMove;
			 ev.pa1    = 0;

		     GetContainer()->GetIUIMessage()->OnUIEvent(&ev);
        }else
		{
		   m_bDraging = 0;
		}
    }

	
	 void CUIMoveWindow::OnNcMouseMove(unsigned int nFlags,CPoint pt)
	 {
		 if(m_nIsLock)
			return ;
		 CRect rcWindow;
		 CUIWindow::GetWindowRect(&rcWindow);

		  
	     if(m_bDraging==2){


            CPoint ptLT = pt - m_ptClick;
			
			if (ptLT.x != 0)
			{
				int i = 0;
				i++;
			}
			if(ptLT.x!=0 || 1)
			{
					m_ptClick = pt;
					CRect rt = rcWindow;
					/*if(tt==sLeft)
					{
						rt.left += ptLT.x;
					}else if(tt== sRight){
					   rt.right += ptLT.x;
					}else if(tt == sTop)
					{
						rt.top+=ptLT.y;
					}else if(tt == sBottom)
					{
						rt.bottom +=ptLT.y;
					}*/

					SStringA Pos;
					Pos.Format("%d,%d,@%d,@%d",rt.left,rt.top,rt.Width(),rt.Height());
					this->SetAttribute("pos", Pos);
					OnRelayout(&rt);
					this->GetParent()->UpdateChildrenPosition();

					 UIEvent ev;
					 ev.Handle = this;
					 ev.type   = E_CtlSize;
					 ev.pa1    = 0;

					 GetContainer()->GetIUIMessage()->OnUIEvent(&ev);
			}
		
		}
	 

	 }

	 void CUIMoveWindow::OnEraseBkgnd( IRenderTarget  *pRT)
	 {
	 	CUIWindow::OnEraseBkgnd( pRT);
	 }
	  int  CUIMoveWindow::OnJustSize(const SStringA& strValue, int bLoading)
	  {

		  SStringAList strLst;
          SplitString(strValue,L',',strLst);
		  if(strLst.size()==4){
			  CRect rcWnd;
			  rcWnd.left   =  atoi(strLst[0]);
			  rcWnd.top    =  atoi(strLst[1]);
			  rcWnd.right  =  rcWnd.left  +  atoi(strLst[2]);
			  rcWnd.bottom =  rcWnd.top  +   atoi(strLst[3]);

			     OnRelayout(&rcWnd);
				SStringA Pos;
				Pos.Format("%d,%d,@%d,@%d",rcWnd.left,rcWnd.top,rcWnd.Width(),rcWnd.Height());
				this->SetAttribute("pos", Pos);
				this->GetParent()->UpdateChildrenPosition();

		  }
	     return 1;
	  }
}
