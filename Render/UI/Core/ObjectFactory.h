#ifndef ObjectFactory_H_
#define ObjectFactory_H_
#include <string>
#include "Units.h"
#include "IObject.h"
///参考soui
namespace KKUI
{


	///对象信息
	class CObjectInfo
	{
	public:
		CObjectInfo(const std::string name, int type = None) :m_strName(name), m_nType(type)
		{
			
		}

		bool operator == (const CObjectInfo & src) const
		{
			return src.m_nType == m_nType && src.m_strName==m_strName == 0;
		}

		bool operator < (const CObjectInfo & src) const
		{
			/*if(src.m_nType>m_nType)
				return 1;*/
			if( src.m_strName> m_strName)
				return 1;
			return 0;
		}

		bool IsValid() const
		{
			return m_nType >= None && !m_strName.length()>0;
		}

		
		
		int Compare(const CObjectInfo & src) const
		{
			if (m_nType == src.m_nType)
				return m_strName==src.m_strName;
			else
				return m_nType - src.m_nType;
		}

		std::string m_strName;
		int         m_nType;
	};

	class CObjectFactory
	{
		public:
				virtual ~CObjectFactory() {}
				virtual IObject * NewObject() const = 0;
				virtual char* BaseClassName() const =0;
				virtual CObjectInfo GetObjectInfo() const = 0;
				virtual CObjectFactory* Clone() const =0;
	};


	template <typename T>
    class TplCObjectFactory : public CObjectFactory
    {
    public:
        //! Default constructor.
		TplCObjectFactory()
        {
        }

        char*BaseClassName() const {
			return T::BaseClassName();
		}

        virtual IObject* NewObject() const
        {
            return new T;
		}
		virtual CObjectInfo GetObjectInfo() const
		{
			return CObjectInfo(T::GetClassName(),T::GetClassType());
		}
		virtual TplCObjectFactory* Clone() const
        {
            return new TplCObjectFactory<T>();
        }
	};


	///工厂管理者
	typedef CObjectFactory* CObjectFactoryPtr;
    class CObjectFactoryMgr :public CCmnMap<CObjectFactoryPtr, CObjectInfo>
    {
		public:
			CObjectFactoryMgr(void);
			bool RegisterFactory(CObjectFactory & objFactory, bool bReplace = false);
            bool UnregisterFactory(const CObjectInfo & objInfo);
            ///创建对象
			IObject *CreateObject(const CObjectInfo & objInfo) const;
	    public:
			template<class T>
			bool TplRegisterFactory()
			{
				return RegisterFactory(TplCObjectFactory<T>());
			}
    };
}
#endif