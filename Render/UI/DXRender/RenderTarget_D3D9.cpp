//#include <d3dx9tex.h>
#include <wchar.h>
#include <stdio.h>
#include <assert.h>
#include <string>

#include <d3dx9math.h>

#include <dxva2api.h>
#include <core\SkCanvas.h>
#include <core\SkBitmap.h>
#include <core\SkTypeface.h>
#include <core\SkImageDecoder.h>
#include <core\SkStream.h>
#include <vector>

#include "RenderTarget_D3D9.h"
#include "DXVA2_D3D9.h"
#include "Yuv420p_D3D9.h"
#include "GetResource.h"
#include "D3d9Sharder.h"
#include "Core/Warp/IWarp.h"
#include "../Core/Fusionzone/IFusionzone.h"
#include "Core/UIMoveState.h"
#include "DX9Text.h"
extern HMODULE hDllModule;
#ifndef SAFE_RELEASE
#define SAFE_RELEASE(p)      { if (p) { (p)->Release(); (p)=NULL; } }
#endif

typedef IDirect3D9* (WINAPI* LPDIRECT3DCREATE9)( UINT );

typedef HRESULT (WINAPI *DX9_D3DXCreateSprite)( LPDIRECT3DDEVICE9   pDevice, LPD3DXSPRITE*       ppSprite);

typedef HRESULT (WINAPI *DX9_DX9CTFromFileInMemory)(LPDIRECT3DDEVICE9 pDevice,LPCVOID pSrcData,UINT  SrcDataSize,LPDIRECT3DTEXTURE9*  ppTexture);
typedef HRESULT (WINAPI *DX9_D3DXCreateTextureFromFile)( LPDIRECT3DDEVICE9  pDevice,LPCTSTR            pSrcFile,LPDIRECT3DTEXTURE9 *ppTexture);

typedef HRESULT (WINAPI *DX9_D3DXSaveTextureToFileW)(
        LPCWSTR                   pDestFile,
        D3DXIMAGE_FILEFORMAT      DestFormat,
        LPDIRECT3DBASETEXTURE9    pSrcTexture,
        CONST PALETTEENTRY*       pSrcPalette);
//x
typedef D3DXMATRIX* (WINAPI *DX9_D3DXMatrixRotationY)(D3DXMATRIX *pOut, FLOAT Angle );
typedef D3DXMATRIX* (WINAPI *DX9_D3DXMatrixRotationZ)(D3DXMATRIX *pOut, FLOAT Angle );

typedef D3DXMATRIX* (WINAPI *DX9_D3DXMatrixScaling)( D3DXMATRIX *pOut, FLOAT sx, FLOAT sy, FLOAT sz );
typedef D3DXMATRIX* (WINAPI *DX9_D3DXMatrixTranslation)( D3DXMATRIX *pOut, FLOAT x, FLOAT y, FLOAT z );
typedef D3DXMATRIX* (WINAPI *DX9_D3DXMatrixMultiply)( D3DXMATRIX *pOut, CONST D3DXMATRIX *pM1, CONST D3DXMATRIX *pM2 );
typedef D3DXMATRIX* (WINAPI *DX9_D3DXMatrixLookAtLH)( D3DXMATRIX *pOut, CONST D3DXVECTOR3 *pEye, CONST D3DXVECTOR3 *pAt,CONST D3DXVECTOR3 *pUp );
typedef D3DXMATRIX* (WINAPI *DX9_D3DXMatrixPerspectiveFovLH)( D3DXMATRIX *pOut, FLOAT fovy, FLOAT Aspect, FLOAT zn, FLOAT zf );
typedef D3DXMATRIX* (WINAPI *DX9_D3DXMatrixOrthoLH)( D3DXMATRIX *pOut, FLOAT w, FLOAT h, FLOAT zn, FLOAT zf );
typedef HRESULT     (WINAPI *DX9_D3DXCreateFontIndirect)( LPDIRECT3DDEVICE9       pDevice, CONST D3DXFONT_DESCW*   pDesc, LPD3DXFONT*             ppFont);

typedef HRESULT     (WINAPI *DX9_D3DXCompileShaderFromFile)(LPCWSTR pSrcFile,CONST D3DXMACRO* pDefines,LPD3DXINCLUDE pInclude,LPCSTR   pFunctionName,
                                                             LPCSTR         pProfile,   DWORD                  Flags,LPD3DXBUFFER* ppShader,
                                                             LPD3DXBUFFER*  ppErrorMsgs,LPD3DXCONSTANTTABLE*   ppConstantTable);

typedef LPCSTR      (WINAPI *DX9_D3DXGetPixelShaderProfile)(LPDIRECT3DDEVICE9    pDevice);


typedef HRESULT     (WINAPI *DX9_D3DXCompileShader)(LPCSTR  pSrcData,UINT SrcDataLen, CONST D3DXMACRO*  pDefines, LPD3DXINCLUDE pInclude,LPCSTR pFunctionName, LPCSTR   pProfile, DWORD Flags, LPD3DXBUFFER*  ppShader, LPD3DXBUFFER*   ppErrorMsgs, LPD3DXCONSTANTTABLE*  ppConstantTable);


typedef LPCSTR      (WINAPI *DX9_D3DXGetPixelShaderProfile)(LPDIRECT3DDEVICE9    pDevice);
typedef HRESULT     (WINAPI *DX9_D3DXCreateBuffer)(DWORD NumBytes, LPD3DXBUFFER *ppBuffer);
typedef HRESULT     (WINAPI *DX9_D3DXGetShaderConstantTable)(CONST DWORD* pFunction, LPD3DXCONSTANTTABLE*  ppConstantTable);

static DX9_D3DXMatrixOrthoLH                  fpDX9_D3DXMatrixOrthoLH              = NULL;
static DX9_D3DXMatrixTranslation              fpDX9_D3DXMatrixTranslation          = NULL;
static DX9_D3DXMatrixScaling                  fpDX9_D3DXMatrixScaling              = NULL;
static DX9_D3DXMatrixRotationZ                fpDX9_D3DXMatrixRotationZ            = NULL;
static DX9_D3DXMatrixRotationY                fpDX9_D3DXMatrixRotationY            = NULL;
static DX9_D3DXCreateTextureFromFile          fpD3DXCreateTextureFromFile          = NULL;
static DX9_DX9CTFromFileInMemory              fpDX9CTFromFileInMemory              = NULL;
static DX9_D3DXSaveTextureToFileW             fpDX9D3DXSaveTextureToFileW         = NULL;
static DX9_D3DXMatrixMultiply                 fpDX9_D3DXMatrixMultiply             = NULL;
static DX9_D3DXMatrixLookAtLH                 fpDX9_D3DXMatrixLookAtLH             = NULL;
static DX9_D3DXMatrixPerspectiveFovLH         fpDX9_D3DXMatrixPerspectiveFovLH     = NULL;
static DX9_D3DXCreateSprite                   fpDX9_D3DXCreateSprite               = NULL;
static DX9_D3DXCreateFontIndirect             fpDX9_D3DXCreateFontIndirect         = NULL;
static LPDIRECT3DCREATE9                      pfnDirect3DCreate9                   = NULL;
static DX9_D3DXGetPixelShaderProfile          fpDX9_D3DXGetPixelShaderProfile      = NULL;
static DX9_D3DXCompileShaderFromFile          fpDX9_D3DXCompileShaderFromFile      = NULL; 
static DX9_D3DXCreateBuffer                   fpDX9_D3DXCreateBuffer               = NULL;
static DX9_D3DXCompileShader                  fpDX9_D3DXCompileShader              = NULL;
static DX9_D3DXGetShaderConstantTable         fpDX9_D3DXGetShaderConstantTable     = NULL;



/**************************DXVA2**************************************/
typedef HRESULT (WINAPI *DX9_CreateDXVA2VideoService_func)  (IDirect3DDevice9 *,REFIID riid,void **ppService);
typedef HRESULT (WINAPI *DX9_CreateDXVA2DeviceManager9_func)(UINT *pResetToken, IDirect3DDeviceManager9 **);
static  DX9_CreateDXVA2DeviceManager9_func     fpDX9_CreateDXVA2DeviceManager9_func    = NULL;
static  DX9_CreateDXVA2VideoService_func       fpDX9_CreateDXVA2VideoService_func = NULL;

static  HMODULE hModD3D9   = NULL;
static  HMODULE hD3dx9_43  = NULL;
static  HMODULE hDxva2_dll = NULL;


LPCSTR GetUTF8String(LPCWSTR str);
extern "C"
{
    D3DXMATRIX* WINAPI D3DXMatrixMultiply
    ( D3DXMATRIX *pOut, CONST D3DXMATRIX *pM1, CONST D3DXMATRIX *pM2 )
	{
	          return fpDX9_D3DXMatrixMultiply(pOut ,pM1, pM2);
	}
}

 void charTowchar2(const char *chr, wchar_t *wchar, int size)  
      {     
         MultiByteToWideChar( CP_ACP, 0, chr,  
         strlen(chr)+1, wchar, size/sizeof(wchar[0]) );  
      }

#define USE_RHW
namespace KKUI
{
	///向量数据
	struct KKUI_Vertex
	{
		float x, y, z;
		#ifdef USE_RHW
			float  w;
       
        #endif
	
		float u, v;
       
		enum 
		{
			//FVF = D3DFVF_XYZRHW|D3DFVF_DIFFUSE|D3DFVF_TEX1,
		//	FVF = D3DFVF_DIFFUSE|D3DFVF_TEX1
			#ifdef USE_RHW
					FVF = D3DFVF_TEX1|D3DFVF_XYZRHW,
            #else
			     FVF =D3DFVF_XYZ | D3DFVF_TEX1 ,
			#endif
		};
	};

	struct KKUI_2DVertex
	{
		float x, y, z;
		float  w;
		DWORD diffuse;
		float u, v;
		enum 
		{
					FVF =D3DFVF_DIFFUSE|D3DFVF_TEX1|D3DFVF_XYZRHW,
		};
	};


	//用于2d
	void   GetUIRHWVertex(KKUI_2DVertex* vec,const kkRect& rt,DWORD diffuse,int xSrc,int ySrc,int NeedWidth,int NeedHeight,int SWidth,int SHeight)
	{
		int w=rt.right-rt.left;
		int h=rt.bottom -rt.top;
	
		vec[0].x =rt.left ;  //A     A(leftTop)->B(rightTop)->C(leftBu)->D->(rightBu)
		vec[0].y =rt.top;
		vec[0].z = 0.0f;
		vec[0].w = 1.f;
		vec[0].u = (float)xSrc/SWidth;  //x
		vec[0].v = (float)ySrc/SHeight; //y
		vec[0].diffuse=diffuse;

		vec[1].x =  rt.right;  //B
		vec[1].y =  rt.top;
		vec[1].z = 0.0f;
		vec[1].w = 1.f;
		vec[1].u = (float)(xSrc+ NeedWidth)/ SWidth;
		vec[1].v =(float)ySrc/SHeight;
        vec[1].diffuse=diffuse;

		vec[2].x =rt.left;   //C
		vec[2].y =rt.bottom ;
		vec[2].z = 0.0f;
		vec[2].w = 1.0f;
		vec[2].u = (float)xSrc/SWidth;
		vec[2].v = (float)(ySrc+NeedHeight)/SHeight;
        vec[2].diffuse=diffuse;

		vec[3].x =rt.right ; //D
		vec[3].y =rt.bottom;
		vec[3].z = 0.0f;
		vec[3].w = 1.0f;
		vec[3].u = (float)(xSrc+ NeedWidth)/ SWidth;
		vec[3].v = (float)(ySrc+NeedHeight)/SHeight;
		vec[3].diffuse=diffuse;
	}
    static D3DPRESENT_PARAMETERS GetPresentParams(HWND hView)
	{
		D3DPRESENT_PARAMETERS PresentParams;
		ZeroMemory(&PresentParams, sizeof(PresentParams));
		PresentParams.BackBufferFormat =D3DFMT_UNKNOWN;
		//D3DFMT_A8R8G8B8; 
		PresentParams.BackBufferCount=0;
		PresentParams.MultiSampleType = D3DMULTISAMPLE_NONE;
		PresentParams.MultiSampleQuality = 0;
		PresentParams.SwapEffect = D3DSWAPEFFECT_DISCARD;
		PresentParams.hDeviceWindow = hView;
		PresentParams.Windowed = TRUE;
		PresentParams.EnableAutoDepthStencil = TRUE;
		PresentParams.AutoDepthStencilFormat = D3DFMT_D24X8;/**/
		PresentParams.Flags = D3DPRESENTFLAG_VIDEO;
		PresentParams.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
		PresentParams.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;
		return PresentParams; 
    }

	SRenderFactory_D3D9::SRenderFactory_D3D9():
		m_pDevice(0),
		m_pD3D(0),
		m_pDefPixelShader(0),
		m_pDefYuvPixelShader(0),
		m_pDefARGBPixelShader(0),
		m_pDeviceDXVA2Manager9(0),
		CRttCache_D3D9(this)
	{
	   
	}
	SRenderFactory_D3D9::~SRenderFactory_D3D9()
	{
		//SAFE_RELEASE(m_pSprite);
	    SAFE_RELEASE(m_pDevice);
		SAFE_RELEASE(m_pD3D);
	}
	IDirect3DTexture9*     SRenderFactory_D3D9::GetFromFileInMemory(unsigned char* data,int len)
	{
		IDirect3DTexture9* pTexture=0;
		//(LPDIRECT3DDEVICE9 pDevice,LPCVOID pSrcData,UINT  SrcDataSize,LPDIRECT3DTEXTURE9*  ppTexture);
		HRESULT hr= fpDX9CTFromFileInMemory(m_pDevice,data, len,&pTexture);
			return  pTexture;
	}
	void SRenderFactory_D3D9::SaveFile(wchar_t *Path,IDirect3DTexture9* textrue)
	{
	HRESULT hr = fpDX9D3DXSaveTextureToFileW(Path,(D3DXIMAGE_FILEFORMAT)4,textrue,NULL);
	int i=0;
	i++;
	}
	struct FilterItem
	{	
		const  char* Name;
	    const  DWORD* ps;
	
	};
	bool SRenderFactory_D3D9::init()
	{
	    if(hModD3D9==0)
			 hModD3D9 = LoadLibraryA("d3d9.dll");
		
		if (hModD3D9)
		{
			pfnDirect3DCreate9 = (LPDIRECT3DCREATE9)GetProcAddress(hModD3D9, "Direct3DCreate9");
			if(hD3dx9_43==NULL)
				hD3dx9_43 = LoadLibraryA("D3dx9_43.dll");
			if(hD3dx9_43)
			{
				 fpDX9_D3DXCreateFontIndirect = (DX9_D3DXCreateFontIndirect)      GetProcAddress(hD3dx9_43, "D3DXCreateFontIndirectW");
				 fpDX9_D3DXCreateSprite       = (DX9_D3DXCreateSprite)      GetProcAddress(hD3dx9_43, "D3DXCreateSprite");
				 fpDX9CTFromFileInMemory      = (DX9_DX9CTFromFileInMemory)      GetProcAddress(hD3dx9_43, "D3DXCreateTextureFromFileInMemory");
				 fpD3DXCreateTextureFromFile  = (DX9_D3DXCreateTextureFromFile)  GetProcAddress(hD3dx9_43, "D3DXCreateTextureFromFile");
				 fpDX9D3DXSaveTextureToFileW  = (DX9_D3DXSaveTextureToFileW)  GetProcAddress(hD3dx9_43, "D3DXSaveTextureToFileW");
				 fpDX9_D3DXMatrixRotationY    = (DX9_D3DXMatrixRotationY)    GetProcAddress(hD3dx9_43, "D3DXMatrixRotationY");
				 fpDX9_D3DXMatrixRotationZ    = (DX9_D3DXMatrixRotationZ)    GetProcAddress(hD3dx9_43, "D3DXMatrixRotationZ");

				 fpDX9_D3DXMatrixTranslation  = (DX9_D3DXMatrixTranslation)    GetProcAddress(hD3dx9_43, "D3DXMatrixTranslation");
                 fpDX9_D3DXMatrixScaling      = (DX9_D3DXMatrixScaling)    GetProcAddress(hD3dx9_43, "D3DXMatrixScaling");
				 fpDX9_D3DXMatrixMultiply     = (DX9_D3DXMatrixMultiply  )    GetProcAddress(hD3dx9_43, "D3DXMatrixMultiply");
				 fpDX9_D3DXMatrixLookAtLH     = (DX9_D3DXMatrixLookAtLH  )    GetProcAddress(hD3dx9_43, "D3DXMatrixLookAtLH");
				 fpDX9_D3DXMatrixPerspectiveFovLH = (DX9_D3DXMatrixPerspectiveFovLH  )    GetProcAddress(hD3dx9_43, "D3DXMatrixPerspectiveFovLH");
				 fpDX9_D3DXMatrixOrthoLH      = (DX9_D3DXMatrixOrthoLH )    GetProcAddress(hD3dx9_43, "D3DXMatrixOrthoLH");
				 fpDX9_D3DXGetPixelShaderProfile  = (DX9_D3DXGetPixelShaderProfile)    GetProcAddress(hD3dx9_43, "D3DXGetPixelShaderProfile");
				 fpDX9_D3DXCompileShaderFromFile  = (DX9_D3DXCompileShaderFromFile)GetProcAddress(hD3dx9_43, "D3DXCompileShaderFromFileW");

				 fpDX9_D3DXGetShaderConstantTable = (DX9_D3DXGetShaderConstantTable)GetProcAddress(hD3dx9_43, "D3DXGetShaderConstantTable");
				  fpDX9_D3DXCreateBuffer           = (DX9_D3DXCreateBuffer)GetProcAddress(hD3dx9_43, "D3DXCreateBuffer");
				  fpDX9_D3DXCompileShader          = (DX9_D3DXCompileShader)GetProcAddress(hD3dx9_43, "D3DXCompileShader");
				// D3DXCreateSphere
			}else{
				/* std::wstring path=GetModulePath();
				 path+=L"\\dx\\D3dx9_43.dll";
				 hD3dx9_43 = LoadLibrary(path.c_str());
				 if(hD3dx9_43)
				 {
					  fpDX9CTFromFileInMemory= (DX9CTFromFileInMemory)GetProcAddress(hD3dx9_43, "D3DXCreateTextureFromFileInMemory");
				 }*/
			}
			
		}else{
			return false;
		}

		if(pfnDirect3DCreate9==NULL)
		{
		
			return false;
		}
		m_pD3D=pfnDirect3DCreate9(D3D_SDK_VERSION);

		DWORD BehaviorFlags =D3DCREATE_FPU_PRESERVE | D3DCREATE_PUREDEVICE | D3DCREATE_HARDWARE_VERTEXPROCESSING|D3DCREATE_NOWINDOWCHANGES;

		HWND hView = GetShellWindow();
		D3DPRESENT_PARAMETERS PresentParams = GetPresentParams(hView);
       
		HRESULT hr = m_pD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL,hView, BehaviorFlags, &PresentParams, &m_pDevice);
	   
		if (FAILED(hr) && hr != D3DERR_DEVICELOST)
		{

			return false;
		}

		if(!hDxva2_dll)
	        hDxva2_dll = LoadLibraryA("DXVA2.DLL");

		if(hDxva2_dll){
			fpDX9_CreateDXVA2VideoService_func     = (DX9_CreateDXVA2VideoService_func)  GetProcAddress(hDxva2_dll, "DXVA2CreateVideoService");
			fpDX9_CreateDXVA2DeviceManager9_func   = (DX9_CreateDXVA2DeviceManager9_func)GetProcAddress(hDxva2_dll, "DXVA2CreateDirect3DDeviceManager9");

			m_pDeviceDXVA2Manager9 = new CDeviceDXVA2Manager9_D3D9(this);
		}
		m_pDefPixelShader = new CPixelShader_D3D9(this);
		m_pDefPixelShader->IniPs();
		m_pDefYuvPixelShader = new  CYUVPixelShader_D3D9(this);
		m_pDefYuvPixelShader->IniPs();

		m_pDefARGBPixelShader = new CARGBPixelShader_D3D9(this);
        m_pDefARGBPixelShader->IniPs();


		//CCommPixelShader_D3D9* ps = new CCommPixelShader_D3D9(this,D3D9_SHUI_POWEN__Ps,"水波纹");
		//ps->IniPs();
		//m_IPsObjMap.insert(std::pair<std::string ,CPixelShader_D3D9*>(ps->GetPsName() ,ps));


		//CCommPixelShader_D3D9* ps2 = new CCommPixelShader_D3D9(this,D3D9_BIANYUAN_YUHUA__Ps,"边缘羽化");
		//ps2->IniPs();
		//m_IPsObjMap.insert(std::pair<std::string ,CPixelShader_D3D9*>(ps2->GetPsName() ,ps2));

		
		FilterItem   item[] ={ 
			{"左右镜像",D3D9_LeftRight_Mirror__Ps},
			{"上下镜像",D3D9_UpDown_Mirror__Ps},
			{"边缘羽化",D3D9_BIANYUAN_YUHUA__Ps},
			{"Sin波形",D3D9_SinWave__Ps},
			{"闪屏", D3D9_SplashScreen__Ps},
			{"二值化",D3D9_Binarization__Ps},
			{"亮度/对比度",D3D9_LingDu_DuiBidu__Ps},
			{"色相/饱和度/亮度",D3D9_HSV__Ps},
			{"色阶",D3D9_ColorLevels__Ps},
			{"RGBATOARGB",D3D9_RGBA_TO_ARGB__Ps},
		};

		int count=sizeof(item)/sizeof(FilterItem);
		for(int i=0;i<count;i++)
		{
			    CCommPixelShader_D3D9* ps3 = new CCommPixelShader_D3D9(this, item[i].ps, item[i].Name);
				ps3->IniPs();
				m_IPsObjMap.insert(std::pair<std::string ,CPixelShader_D3D9*>(ps3->GetPsName() ,ps3));
		}
		


		return 1;
	}

    CPixelShader_D3D9*   SRenderFactory_D3D9::GetPsByName(char* Name)
	{
		std::map<std::string ,CPixelShader_D3D9*>::iterator ItPs =  m_IPsObjMap.find(Name);
        if(ItPs!= m_IPsObjMap.end()){
					return ItPs->second;
		}
        return 0;
   }
    void   SRenderFactory_D3D9::CopyToTex(IDirect3DTexture9* text1,int Width,int Height,IDirect3DTexture9* text2,char* PsName)
   {
          
		 
			    IDirect3DSurface9*  OldbufferSurface;
				HRESULT hr = m_pDevice->GetRenderTarget(0, &OldbufferSurface);

			    IDirect3DSurface9* pSurface;
		        hr = text2->GetSurfaceLevel(0, &pSurface);
                hr = m_pDevice->SetRenderTarget(0, pSurface);
				hr = pSurface->Release();



				
				if(PsName)
				{
					 CPixelShader_D3D9*  pPixelShader_D3D9= GetPsByName(PsName); 
					 if(pPixelShader_D3D9){
					        LPDIRECT3DPIXELSHADER9	  pPixelShader   =pPixelShader_D3D9 ->GetPixelShader();
				            m_pDevice->SetPixelShader(pPixelShader);
					 }
				}
				m_pDevice->SetTexture(0, text1);
				//放大系数 ,放大采样方式
				m_pDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_NONE);
				
				//缩小采样，方式
				m_pDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_NONE);
				m_pDevice->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR);
				m_pDevice->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
				m_pDevice->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);

			     KKUI_Vertex Vec[4];
				 Vec[0].x = 0.0;
				 Vec[0].y = 0.0;
				 Vec[0].z = 1.0;
				 Vec[0].u = 0.0;
				 Vec[0].v = 0.0;
				 Vec[0].w = 1.0;

				 Vec[1].x = Width;
				 Vec[1].y = 0.0;
				 Vec[1].z = 1.0;
				 Vec[1].u = 1.0;
				 Vec[1].v = 0.0;
				 Vec[1].w = 1.0;

				 Vec[2].x = 0.0;
				 Vec[2].y =  Height;
				 Vec[2].z = 1.0;
				 Vec[2].u = 0.0;
				 Vec[2].v = 1.0;
				 Vec[2].w = 1.0;

				 Vec[3].x = Width;
				 Vec[3].y = Height;
				 Vec[3].z = 1.0;
				 Vec[3].u = 1.0;
				 Vec[3].v = 1.0;
				 Vec[3].w = 1.0;

			     m_pDevice->SetFVF( D3DFVF_TEX3|D3DFVF_XYZRHW);
				 m_pDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2,  Vec, sizeof(KKUI_Vertex));/**/ 
                 m_pDevice->SetPixelShader( 0 );	

                 hr = m_pDevice->SetRenderTarget(0, OldbufferSurface);
		  
   }
    int    SRenderFactory_D3D9::InitShader()
   {

	   	LPCSTR PixelShaderVver = fpDX9_D3DXGetPixelShaderProfile(m_pDevice);
        return 0;
   }
	int    SRenderFactory_D3D9::CreateRenderTarget(IRenderTarget ** ppRenderTarget,void* Handle,int nWid,int nHei)
	{
		Lock();
		CRenderTarget_D3D9* re = new CRenderTarget_D3D9(this,(HWND)Handle,nWid,nHei);
		*ppRenderTarget = re;
		m_IRenderObjSet.insert((IRenderObj*)re);
		UnLock();
	   return 0;
	}

    
	void         SRenderFactory_D3D9::RemoveObj(IRenderObj* obj,int lock)
	{
		if(lock)
		Lock();
		std::set<IRenderObj*>::iterator It= m_IRenderObjSet.find(obj);
		if (It != m_IRenderObjSet.end())
		{
			IRenderObj* xxx = *It;
			m_IRenderObjSet.erase(It);
		}
			
		if(lock)
		UnLock();
	}

bool  SRenderFactory_D3D9::LostDevice()
{
	
    HRESULT hr = m_pDevice->TestCooperativeLevel();
    if (hr == D3DERR_DEVICELOST)
    {
		return true;
       
    }else if (hr == D3DERR_DEVICENOTRESET)
    { 
		return true;
    }


    return false;
}

bool     SRenderFactory_D3D9::LostDeviceRestore()
{
    HRESULT hr = m_pDevice->TestCooperativeLevel();
    if (hr == D3DERR_DEVICELOST)
    {
		return true;
       
    }else if (hr == D3DERR_DEVICENOTRESET)
    {           
		        if(m_pDefPixelShader)
				    m_pDefPixelShader->Release2();
				if(m_pDefYuvPixelShader)
					m_pDefYuvPixelShader->Release2();
                if(m_pDeviceDXVA2Manager9)
				    m_pDeviceDXVA2Manager9->Release2();

				

			    std::set<IRenderObj*>::iterator It= m_IRenderObjSet.begin();
				for(;It!= m_IRenderObjSet.end();++It){
				   (*It)->Release2();
				}
				
				std::map<std::string ,CPixelShader_D3D9*>::iterator ItPs =  m_IPsObjMap.begin();
                for(;ItPs!= m_IPsObjMap.end();++ItPs){
					ItPs->second->Release2();
				}
                ReleaseX();
				

				HWND hView = GetShellWindow();
				D3DPRESENT_PARAMETERS PresentParams = GetPresentParams(hView);
				hr = m_pDevice->Reset(&PresentParams);
				if(FAILED(hr))
				{
					return false;
				}

				
                if(m_pDefPixelShader)
                    m_pDefPixelShader->ReLoadRes();
				if(m_pDefYuvPixelShader)
					m_pDefYuvPixelShader->ReLoadRes();

				It= m_IRenderObjSet.begin();
				for(;It!= m_IRenderObjSet.end();++It){
				   (*It)->ReLoadRes();
				}
				
				ItPs =  m_IPsObjMap.begin();
                for(;ItPs!= m_IPsObjMap.end();++ItPs){
				   ItPs->second->ReLoadRes();
				}


				if (m_pDeviceDXVA2Manager9)
					m_pDeviceDXVA2Manager9->ReLoadRes();

				ReLoadResX();
				return true;
    }


    return false;
}
    int    SRenderFactory_D3D9::CreateBitmap(IBitmap ** ppBitmap)
    {
		Lock();
		CBitmap_D3D9 *p = new CBitmap_D3D9(this);
        *ppBitmap = p;
		m_IRenderObjSet.insert(p);
		UnLock();
       return 0;
    }

	int    SRenderFactory_D3D9::CreateYUV420p(IPicYuv420p ** ppBitmap)
	{
		     Lock();
              CYuv420p_D3D9* pD = new CYuv420p_D3D9(this);
			  *ppBitmap=pD;
			  m_IRenderObjSet.insert(pD);
		      UnLock();
	        return 0;
	}
	int   SRenderFactory_D3D9::BeforePaint(IRenderTarget* pRenderTarget)
	{
	     HRESULT hr=0;
		 Lock();
		 if(LostDeviceRestore()){
			 UnLock();
		    return 0;
		 }
		 CRenderTarget_D3D9* pRenderTargetDx=(CRenderTarget_D3D9*)pRenderTarget;

		 if(pRenderTargetDx->GetEnableWarp() || pRenderTargetDx->GetEnableFusion() )
		 {
		     //相当一个缓冲
			 //渲染到纹理
			 IDirect3DTexture9* pRenderTexture    = pRenderTargetDx->GetRenderTexture();
			 IDirect3DSurface9*     pRenderSurface = 0;
			 hr = pRenderTexture->GetSurfaceLevel(0, &pRenderSurface) ;
			 if (FAILED(hr)){
					  assert(0);
					  UnLock();
					  return 0;
			 }

		   
			m_pDevice->SetRenderTarget(0, pRenderSurface);
            hr =  pRenderSurface->Release();
			m_pDevice->ColorFill(pRenderSurface,NULL,D3DCOLOR_XRGB(0,0,0));
			
		 }else{
			 IDirect3DSurface9  *pSurface=pRenderTargetDx->GetSurface();
			 hr =m_pDevice->SetRenderTarget(0,pSurface);
			 if (!pSurface) {
				 UnLock();
				 return 0;
			 }
				 
			 pSurface->Release(); 
			
			// m_pDevice->Clear(0, NULL, D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0,0,0), 1.0f, 0);
			 m_pDevice->ColorFill(pSurface,NULL,D3DCOLOR_XRGB(0,0,0));
		 }
		 
		 //面剪切
		 m_pDevice->SetRenderState(D3DRS_CULLMODE,D3DCULL_NONE);
         m_pDevice->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);
		 m_pDevice->SetRenderState(D3DRS_ZENABLE,  FALSE) ; 
		 m_pDevice->SetRenderState(D3DRS_ANTIALIASEDLINEENABLE, TRUE);
		 m_pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE); 
		 m_pDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD );
		 m_pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
		 m_pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA );  

		 //  pDevice->SetRenderState( D3DRS_MULTISAMPLEANTIALIAS, TRUE) ;


		 //禁用光照
		 m_pDevice->SetRenderState(D3DRS_LIGHTING, 0);
		

		 {

			 D3DVIEWPORT9 viewport;
			 m_pDevice->GetViewport(&viewport);
			 D3DMATRIX matrix;
			 matrix.m[0][0] = 1.0f;
			 matrix.m[0][1] = 0.0f;
			 matrix.m[0][2] = 0.0f;
			 matrix.m[0][3] = 0.0f;
			 matrix.m[1][0] = 0.0f;
			 matrix.m[1][1] = 1.0f;
			 matrix.m[1][2] = 0.0f;
			 matrix.m[1][3] = 0.0f;
			 matrix.m[2][0] = 0.0f;
			 matrix.m[2][1] = 0.0f;
			 matrix.m[2][2] = 1.0f;
			 matrix.m[2][3] = 0.0f;
			 matrix.m[3][0] = 0.0f;
			 matrix.m[3][1] = 0.0f;
			 matrix.m[3][2] = 0.0f;
			 matrix.m[3][3] = 1.0f;
			 m_pDevice->SetTransform(D3DTS_WORLD, &matrix);
			 m_pDevice->SetTransform(D3DTS_VIEW, &matrix);


			 matrix.m[0][0] = 2.0f / viewport.Width;
			 matrix.m[0][1] = 0.0f;
			 matrix.m[0][2] = 0.0f;
			 matrix.m[0][3] = 0.0f;
			 matrix.m[1][0] = 0.0f;
			 matrix.m[1][1] = -2.0f / viewport.Height;
			 matrix.m[1][2] = 0.0f;
			 matrix.m[1][3] = 0.0f;
			 matrix.m[2][0] = 0.0f;
			 matrix.m[2][1] = 0.0f;
			 matrix.m[2][2] = 1.0f;
			 matrix.m[2][3] = 0.0f;
			 matrix.m[3][0] = -1.0f;
			 matrix.m[3][1] = 1.0f;
			 matrix.m[3][2] = 0.0f;
			 matrix.m[3][3] = 1.0f;
			 m_pDevice->SetTransform(D3DTS_PROJECTION, &matrix);

		 }

         if(SUCCEEDED(m_pDevice->BeginScene()))
		    return 1;

		  UnLock();
		 return 0;
		
	}
	void   SRenderFactory_D3D9::EndPaint(IRenderTarget* pRenderTarget,bool WarpNet)
	{

		if(LostDeviceRestore()){
			UnLock();
		    return;
		 }

		 HRESULT hr=0;
		 CRenderTarget_D3D9* pRenderTargetDx=(CRenderTarget_D3D9*)pRenderTarget;
		 if(pRenderTargetDx->GetEnableWarp()|| pRenderTargetDx->GetEnableFusion())
		 {
               m_pDevice->EndScene();

	       
			   if(pRenderTargetDx->GetEnableFusion())
			   {
			      pRenderTargetDx->OnFusionzone();
			   }

			   IDirect3DSurface9  *pSurface=pRenderTargetDx->GetSurface();
			   hr =m_pDevice->SetRenderTarget(0,pSurface);
			   if(!pSurface)
				 assert(0);
			   pSurface->Release();
			   m_pDevice->ColorFill(pSurface,NULL,D3DCOLOR_XRGB(0,0,0));
			   
			   
			
			   hr =m_pDevice->BeginScene();
			   IDirect3DTexture9* pRenderTexture    = pRenderTargetDx->GetRenderTexture();
			   CRect rt;
			   rt.left   = 0;
			   rt.right  = pRenderTargetDx->GetWidth();
			   rt.top    = 0;
			   rt.bottom = pRenderTargetDx->GetHeight();


			  

			   
			   void* wq= pRenderTargetDx->GetIWarp();
			   if(wq){
				   //放大系数 ,放大采样方式
				   m_pDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_NONE);
				   //缩小采样，方式
				   m_pDevice->SetSamplerState(0, D3DSAMP_MINFILTER,   D3DTEXF_LINEAR);
				   m_pDevice->SetSamplerState(0,D3DSAMP_MIPFILTER,   D3DTEXF_LINEAR);
				   m_pDevice->SetSamplerState(0, D3DSAMP_ADDRESSU,D3DTADDRESS_CLAMP);
				   m_pDevice->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
				   IWarp* pIWarp=(IWarp*)wq;
				   pIWarp->OnPaint(pRenderTarget, rt);;
				   if(WarpNet){
					   rt.left+=5;
					   rt.top+=5;
					   rt.right-=5;
					   rt.bottom-=5;
					  pIWarp->OnPaintMesh(pRenderTarget,rt,0xFFFF0000);
				   }
			   }else{
				   hr =m_pDevice->BeginScene();
                   pRenderTargetDx->DrawTexture( pRenderTexture);
			   }
		 }else{
		 
		 
		 }

	     
		 m_pDevice->EndScene();
		 IDirect3DSwapChain9* pSwapChain= pRenderTargetDx->GetSwapChain();
		 if(pSwapChain)
		     hr=pSwapChain->Present(NULL, NULL, NULL,NULL , NULL);


		 UnLock();
	}
	void   SRenderFactory_D3D9::CreateFont(IFont** ppFont,int FontSize,wchar_t* FontName)
	{
		 
			CFont_D3D9* ff = new CFont_D3D9(this,FontSize,FontName);
			*ppFont=ff;
            m_IRenderObjSet.insert(ff);
	}
    IDirect3DDeviceManager9*     SRenderFactory_D3D9::CreateDevMgr(unsigned int& token)
	{
		IDirect3DDeviceManager9*  devMgr = 0;
	//	Lock();
		if(fpDX9_CreateDXVA2DeviceManager9_func){
				if (FAILED(fpDX9_CreateDXVA2DeviceManager9_func(&token, &devMgr)))
				{
					UnLock();
					return 0;
				}
				HRESULT hr = devMgr->ResetDevice(m_pDevice, token);
				if (FAILED(hr))
				{
					devMgr->Release();
					UnLock();
					return 0;
				}

		}
	//	UnLock();
	    return  devMgr;
	}
	void   SRenderFactory_D3D9:: Lock()
	{
		  m_lock.Lock();
	}
	void   SRenderFactory_D3D9::UnLock()
	{
		  m_lock.Unlock();
	}
    void*   SRenderFactory_D3D9::CreateVideoDecoder(int CodecId,int Width,int Height)
	{
	    if(!m_pDeviceDXVA2Manager9)
			return 0;
		return m_pDeviceDXVA2Manager9->CreateVideoDecoder( CodecId,Width,Height);
	}
    void    SRenderFactory_D3D9::DeleteVideoDecoder(void* pVD)
	{
	      CDXVA2VD_D3D9*  pDXVA2VD = static_cast< CDXVA2VD_D3D9*>(pVD);
		  if( pDXVA2VD){
			  if(!m_pDeviceDXVA2Manager9->DelVideoDecoder(pDXVA2VD)){
			      assert(0);
			  }
		  }
    }

	void*      SRenderFactory_D3D9::CreateDxTexture()
	{
	     CDX9Texture* dx = new CDX9Texture(this);
         //Lock();
		 	m_IRenderObjSet.insert((IRenderObj*)dx);
	    // UnLock();
		 return dx;
	}
	void       SRenderFactory_D3D9::DeleteDxTexturer(void* DxTexture)
	{
		  CDX9Texture*  pDXVA2VD = static_cast<  CDX9Texture*>(DxTexture);

		 RemoveObj(pDXVA2VD,0);
		 pDXVA2VD->Release2();
		 pDXVA2VD->Release();
     }





	CFont_D3D9::CFont_D3D9(SRenderFactory_D3D9* pRenderFactory,int FontSize,wchar_t* FontName):m_pD3DXFont(0), 
		m_nTextSize(FontSize),
		m_pRenderFactory(pRenderFactory)
	{
		memset(m_nFontName,0,128);
		if(FontName)
		    wcscpy( m_nFontName,FontName);
	         
		 m_pRenderFactory->AddRef();
		ReLoadRes();	   
					
	}
	CFont_D3D9::~CFont_D3D9()
	{
	   m_pRenderFactory->Release();
	   Release2();
	   m_pRenderFactory->RemoveObj(this);
	}
	void  CFont_D3D9::Release2()
	{
	    SAFE_RELEASE(m_pD3DXFont);
	}
	void CFont_D3D9::ReLoadRes()
	{

		    IDirect3DDevice9* pDevice=m_pRenderFactory->GetDevice();
			D3DXFONT_DESC df;
			ZeroMemory(&df, sizeof(D3DXFONT_DESC));
			df.Height = m_nTextSize;
			//df.Width = 12;
			df.MipLevels = D3DX_DEFAULT;
			df.Italic = false;
			df.CharSet = DEFAULT_CHARSET;
			df.OutputPrecision = 0;
			df.Quality = 0;
			df.PitchAndFamily = 0;
			if(m_nFontName){
                wcscpy(df.FaceName, m_nFontName);
			}
			//创建ID3DXFont对象
			fpDX9_D3DXCreateFontIndirect(pDevice, &df, &m_pD3DXFont); 
	}
	int  CFont_D3D9::TextSize()
	{
	   return 0;
	}
	ID3DXFont*    CFont_D3D9::GetD3DXFont()
	{
		return m_pD3DXFont;
	}
	

	 
	 CBitmap_D3D9::CBitmap_D3D9(SRenderFactory_D3D9* pRenderFactory):
	        m_pTexture(0),
			m_pRenderFactory( pRenderFactory),
			m_pSrcBuffer(0),
			m_pSrcBufferLen(0)
	{
	   m_pRenderFactory->AddRef();
	   memset(&m_sz,0,sizeof(m_sz));
	}

	 CBitmap_D3D9::~CBitmap_D3D9()
	 {
	     m_pRenderFactory->Release();
		 Release2();
		 if(m_pSrcBuffer)
			free(m_pSrcBuffer);
		 m_pSrcBuffer =0;
		  m_pRenderFactory->RemoveObj(this);
	 }
	 void   CBitmap_D3D9::Release2()
	 {
	      SAFE_RELEASE(m_pTexture);
	 }
	 void   CBitmap_D3D9::ReLoadRes()
	 {
		    if(!m_pSrcBuffer)
			 return;

	        SAFE_RELEASE(m_pTexture);
			if(fpDX9CTFromFileInMemory!=NULL)
			{
				fpDX9CTFromFileInMemory(m_pRenderFactory->GetDevice(),m_pSrcBuffer,  m_pSrcBufferLen, &m_pTexture);
				
			}
	 }
	 kkSize    CBitmap_D3D9::Size() const
	 {
	    return m_sz;
	 }

     int       CBitmap_D3D9::Width()
	 {
		 return m_sz.cx;
	 }
	 int       CBitmap_D3D9::Height()
	 {
	     return m_sz.cy;
	 }
	 static void charTowchar(const char *chr, wchar_t *wchar, int size)  
     {     
         MultiByteToWideChar( CP_ACP, 0, chr,  
         strlen(chr)+1, wchar, size/sizeof(wchar[0]) );  
     }
    int   CBitmap_D3D9::LoadFromFile(const char*FileName)
	{
		    SAFE_RELEASE(m_pTexture);
			if( fpD3DXCreateTextureFromFile!=NULL&&FileName!=0)
			{
				wchar_t path[512]={};
				charTowchar(FileName,path,512);
				fpD3DXCreateTextureFromFile(m_pRenderFactory->GetDevice(),path, &m_pTexture);
				if(!m_pTexture)
					return 1;

				D3DSURFACE_DESC desc;
			    m_pTexture->GetLevelDesc(0,&desc);
			    m_sz.cx = desc.Width;
			    m_sz.cy = desc.Height;
				return 1;
			}
			return  0;

			
	}
    int	CBitmap_D3D9::LoadFromMemory(const void* pBuf,int szLen)
	{
	        SAFE_RELEASE(m_pTexture);

			SkBitmap Bkbitmap;
		    SkMemoryStream stream(pBuf,szLen,true);
		    SkImageDecoder::DecodeStream(&stream, &Bkbitmap);
			m_sz.cx = Bkbitmap.width();
			m_sz.cy = Bkbitmap.height();
			if(fpDX9CTFromFileInMemory!=NULL)
			{
				fpDX9CTFromFileInMemory(m_pRenderFactory->GetDevice(),pBuf, szLen, &m_pTexture);
				if(!m_pTexture)
					return 0;

				if(m_pSrcBuffer)
					free(m_pSrcBuffer);
				//为了防止设备丢失，暂且先缓存数据
				m_pSrcBuffer = (char*)::malloc(szLen);
				memcpy(m_pSrcBuffer, pBuf,szLen);
			    m_pSrcBufferLen =szLen;
				return 1;
			}
			return  0;
	}


   CRenderTarget_D3D9::CRenderTarget_D3D9( SRenderFactory_D3D9* pRenderFactory,HWND h,int nWidth, int nHeight): 
	    m_hView( h),
	    m_pRenderFactory(pRenderFactory), 
		m_nWidth(nWidth),
		m_nHeight(nHeight),
		m_pSwapChainSurface9(0),
		m_pSwapChain(0),
		m_pRenderTexture(0),
		m_nWarp(0),
		m_pIWarp(0),
		m_pFusionzone(0),
		m_nEnableFusion(0)
   {
	   m_pRenderFactory->AddRef();
       RecreateSwapChain();   
   }

  
   CRenderTarget_D3D9::~CRenderTarget_D3D9()
   {
	   Release2();
       m_pRenderFactory->Release();
	   m_pRenderFactory->RemoveObj(this);	  
   }
   void  CRenderTarget_D3D9::SetEnableWarp(void*  pIWarp)
   {

	   m_nWarp =pIWarp!=0 ? 1:0;
	   
	   m_pIWarp=pIWarp;
   }
   void  CRenderTarget_D3D9::SetFusionzone(void* Fusionzone)
   {
      m_pFusionzone=Fusionzone;
   }
   void  CRenderTarget_D3D9::Release2()
   {
	  
	  // m_BufferRenderMap.clear();

	   SAFE_RELEASE(m_pRenderTexture);
	   SAFE_RELEASE(m_pSwapChainSurface9);
	   SAFE_RELEASE(m_pSwapChain);
  }
    void  CRenderTarget_D3D9::ReLoadRes()
	{
	      RecreateSwapChain(); 

		 
	}
   void   CRenderTarget_D3D9::ReSize(int nWidth, int nHeight)
   {
         m_nWidth = nWidth;
		 m_nHeight= nHeight;
		 m_pRenderFactory->Lock();
		 RecreateSwapChain(); 
		 m_pRenderFactory->UnLock();
   }
   void CRenderTarget_D3D9::RecreateSwapChain()
   {
	  
	   HRESULT hr= 0;
	   if(m_pRenderTexture)
	   {
			hr= m_pRenderTexture->Release();
			m_pRenderTexture=0;
		}
       if(m_pSwapChainSurface9)
	   {
			hr= m_pSwapChainSurface9->Release();
			//int xxx =m_pSwapChainSurface9->AddRef();
			m_pSwapChainSurface9=0;
		}
		if(m_pSwapChain)
		{
			hr= m_pSwapChain->Release();
			m_pSwapChain=0;
		}
 
		

         if(m_nWidth <=0 || m_nHeight <=0 )
		   return ;

		D3DPRESENT_PARAMETERS PresentParams = GetPresentParams(m_hView);
		PresentParams.SwapEffect= D3DSWAPEFFECT_COPY;
		PresentParams.hDeviceWindow = m_hView;
		PresentParams.BackBufferHeight = m_nHeight;
		PresentParams.BackBufferWidth = m_nWidth;

		 hr= m_pRenderFactory->GetDevice()->CreateAdditionalSwapChain(&PresentParams,&m_pSwapChain);
        if(FAILED(hr) )
		{
		   assert(0);
		}

		hr =m_pSwapChain->GetBackBuffer(0,D3DBACKBUFFER_TYPE_MONO,&m_pSwapChainSurface9);
		if(FAILED(hr) )
		{
		  assert(0);
		}
		m_pSwapChainSurface9->Release();

		
		//创建一个呈现纹理
		if(m_nWarp || m_nEnableFusion){
		        hr =  m_pRenderFactory->GetDevice()->CreateTexture(
			    m_nWidth, m_nHeight,
				1,
				D3DUSAGE_RENDERTARGET,
				D3DFMT_A8R8G8B8,//D3DFMT_R5G6B5,
				D3DPOOL_DEFAULT,
				&m_pRenderTexture,
				NULL) ;
				if (FAILED(hr))
				{
					 assert(0);
				}
		 }
   }
  

   int  CRenderTarget_D3D9::DrawBitmap(kkRect* pRcDest,IBitmap *pBitmap,int xSrc,int ySrc,int cx,int cy,unsigned char byAlpha)
   {
	 
		 kkSize sz=pBitmap->Size();
	     KKUI_2DVertex vec[4];
		
		 GetUIRHWVertex(vec, *pRcDest,D3DCOLOR_ARGB( 255, 255, 255, 255 ), xSrc,ySrc,cx,cy,sz.cx,sz.cy);
		
	     CBitmap_D3D9* bit =( CBitmap_D3D9*) pBitmap;
	     IDirect3DDevice9* pDevice=m_pRenderFactory->GetDevice();
		 IDirect3DTexture9*  tex = bit->GetTexture();
		 if (tex == 0)
			 return 0;
         int hh= pDevice->SetTexture(0, bit->GetTexture());
         pDevice->SetFVF(KKUI_2DVertex::FVF);
		

		 pDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2,  vec, sizeof(KKUI_2DVertex));/**/
		 pDevice->SetTexture(0, NULL) ;
		 return 1;
   }
   int  CRenderTarget_D3D9::DrawBitmapEx(kkRect*  pRcDest,IBitmap *pBitmap,kkRect* pRcSrc,unsigned int expendMode,unsigned char byAlpha/**/ )
   {
	    unsigned int expandModeLow = LOWORD(expendMode);
        
		kkSize sz=pBitmap->Size();
        if(expandModeLow == EM_NULL)
            return DrawBitmap(pRcDest,pBitmap,pRcSrc->left,pRcSrc->top,sz.cx,sz.cy,byAlpha);

       
        
        if(expandModeLow == EM_STRETCH)
        {
            //::AlphaBlend(m_hdc,pRcDest->left,pRcDest->top,pRcDest->right-pRcDest->left,pRcDest->bottom-pRcDest->top,
           //hmemdc,pRcSrc->left,pRcSrc->top,pRcSrc->right-pRcSrc->left,pRcSrc->bottom-pRcSrc->top,bf);

			DrawBitmap(pRcDest,pBitmap,pRcSrc->left,pRcSrc->top,pRcSrc->right-pRcSrc->left,pRcSrc->bottom-pRcSrc->top,byAlpha);
        }else
        {
           
           
            //int nWid=pRcSrc->right-pRcSrc->left;
            //int nHei=pRcSrc->bottom-pRcSrc->top;
            //for(int y=pRcDest->top ;y<pRcDest->bottom;y+=nHei)
            //{
            //    for(int x=pRcDest->left; x<pRcDest->right; x+=nWid)
            //    {
            //       // ::AlphaBlend(m_hdc,x,y,nWid,nHei,
            //         //   hmemdc,pRcSrc->left,pRcSrc->top,nWid,nHei,
            //        //    bf);                    
            //    }
            //}
     
        }
       
        return S_OK;
   
   }
   int  CRenderTarget_D3D9::DrawBitmap9Patch(kkRect* pRcDest,IBitmap *pBitmap,kkRect* pRcSrc,kkRect* pRcSourMargin,unsigned expendMode,unsigned char byAlpha)
   {
      
        int xDest[4] = {pRcDest->left,pRcDest->left+pRcSourMargin->left,pRcDest->right-pRcSourMargin->right,pRcDest->right};
        int xSrc[4] = {pRcSrc->left,pRcSrc->left+pRcSourMargin->left,pRcSrc->right-pRcSourMargin->right,pRcSrc->right};
        int yDest[4] = {pRcDest->top,pRcDest->top+pRcSourMargin->top,pRcDest->bottom-pRcSourMargin->bottom,pRcDest->bottom};
        int ySrc[4] = {pRcSrc->top,pRcSrc->top+pRcSourMargin->top,pRcSrc->bottom-pRcSourMargin->bottom,pRcSrc->bottom};

        //首先保证九宫分割正常
        if(!(xSrc[0] <= xSrc[1] && xSrc[1] <= xSrc[2] && xSrc[2] <= xSrc[3])) 
			return S_FALSE;
        if(!(ySrc[0] <= ySrc[1] && ySrc[1] <= ySrc[2] && ySrc[2] <= ySrc[3])) 
			return S_FALSE;

        //调整目标位置
        int nDestWid=pRcDest->right-pRcDest->left;
        int nDestHei=pRcDest->bottom-pRcDest->top;

        if((pRcSourMargin->left + pRcSourMargin->right) > nDestWid)
        {//边缘宽度大于目标宽度的处理
            if(pRcSourMargin->left >= nDestWid)
            {//只绘制左边部分
                xSrc[1] = xSrc[2] = xSrc[3] = xSrc[0]+nDestWid;
                xDest[1] = xDest[2] = xDest[3] = xDest[0]+nDestWid;
            }else if(pRcSourMargin->right >= nDestWid)
            {//只绘制右边部分
                xSrc[0] = xSrc[1] = xSrc[2] = xSrc[3]-nDestWid;
                xDest[0] = xDest[1] = xDest[2] = xDest[3]-nDestWid;
            }else
            {//先绘制左边部分，剩余的用右边填充
                int nRemain=xDest[3]-xDest[1];
                xSrc[2] = xSrc[3]-nRemain;
                xDest[2] = xDest[3]-nRemain;
            }
        }

        if(pRcSourMargin->top + pRcSourMargin->bottom > nDestHei)
        {
            if(pRcSourMargin->top >= nDestHei)
            {//只绘制上边部分
                ySrc[1] = ySrc[2] = ySrc[3] = ySrc[0]+nDestHei;
                yDest[1] = yDest[2] = yDest[3] = yDest[0]+nDestHei;
            }else if(pRcSourMargin->bottom >= nDestHei)
            {//只绘制下边部分
                ySrc[0] = ySrc[1] = ySrc[2] = ySrc[3]-nDestHei;
                yDest[0] = yDest[1] = yDest[2] = yDest[3]-nDestHei;
            }else
            {//先绘制左边部分，剩余的用右边填充
                int nRemain=yDest[3]-yDest[1];
                ySrc[2] = ySrc[3]-nRemain;
                yDest[2] = yDest[3]-nRemain;
            }
        }

        //定义绘制模式
        unsigned int mode[3][3]={
            {EM_NULL,expendMode,EM_NULL},
            {expendMode,expendMode,expendMode},
            {EM_NULL,expendMode,EM_NULL}
        };

        for(int y=0;y<3;y++)
        {
            if(ySrc[y] == ySrc[y+1]) 
				continue;
            for(int x=0;x<3;x++)
            {
                if(xSrc[x] == xSrc[x+1])
					continue;
                kkRect rcSrc = {xSrc[x],ySrc[y],xSrc[x+1],ySrc[y+1]};
                kkRect rcDest ={xDest[x],yDest[y],xDest[x+1],yDest[y+1]};
                DrawBitmapEx(&rcDest,pBitmap,&rcSrc,mode[y][x],byAlpha);
            }
        }

        return S_OK;
   }
   
    const float lumR = 0.3086f;
	const float lumG = 0.6094f;
	const float lumB = 0.0820f;



	 //当有变形是通过这个来绘制
    void  CRenderTarget_D3D9::DrawWarp(SVertInfo* VertInfo,int VertCount)
	{
	  
		 IDirect3DDevice9*   pDevice=m_pRenderFactory->GetDevice();
		 IDirect3DTexture9*  pTexture = this->GetRenderTexture();
		 int                 TriangleCount=2;
		 int h= 0;
		 if(pTexture){
			 //RHW模式，坐标变换无效
			 KKUI_Vertex vec[512],*pVecs=vec;
			
			 if( VertInfo!=0 && VertCount > 2 ){
				 for(int i=0;i< VertCount;i++){
					 vec[i].x = VertInfo[i].x;
					 vec[i].y = VertInfo[i].y;
					 vec[i].u = VertInfo[i].uv.x;
					 vec[i].v = VertInfo[i].uv.y;
					 vec[i].z = VertInfo[i].z;
					 #ifdef USE_RHW
					     vec[i].w = 1.0;
                     #endif
				 }
				TriangleCount= VertCount-2;
			 }else{
			    assert(0);
			 }
		
           
			 h= pDevice->SetTexture(0, pTexture);
			 h= pDevice->SetFVF(KKUI_Vertex::FVF);

			// pDevice->SetVertexShader( 0 );
             pDevice->SetPixelShader(0);
			
			 h= pDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, TriangleCount,  pVecs, sizeof(KKUI_Vertex));/**/
			 //pDevice->SetVertexShader( 0 );
             pDevice->SetPixelShader( 0 );	
			 pDevice->SetTexture(0, NULL) ;
		 }
	}

	
	
	void  CRenderTarget_D3D9::OnFusionzone()
	{
		 if(!m_pFusionzone)
            return ;

		  CUIMoveState* ss= static_cast<CUIMoveState*>(m_pFusionzone);
		 IFusionzoneGather*    pFusionGather = ss;

		std::vector<IFusionzone*> FuVect= pFusionGather->GetIFusionzoneVect();
		
		kkRect rt;
		rt.left = 0;
		rt.top  = 0;
		rt.right  = this->m_nWidth;
		rt.bottom = this->m_nHeight;
	    int syn	=GetEnableFusion()& 0x002;
		for(int i=0;i<FuVect.size();i++)
		{
			IFusionzone* ff = FuVect.at(i);
		    ff->DrawFusionzone(this,rt,syn);
		}

	     /*SPalette Palette;
		 Palette.Alpha=0;

		 SMaskInfo Info;
		 SUVInfo   UVS[20]; 
		 Info.uvinfo = UVS;
		 Info.count  = 4;

         UVS[0].x = 0.0;
		 UVS[0].y = 0.0;

		 UVS[1].x = 0.5;
		 UVS[1].y = 0.0;

		 UVS[2].x = 0.5;
		 UVS[2].y = 1.0;


		 UVS[3].x = 0.0;
		 UVS[3].y = 1.0;

	     AdjustARGB(Palette,&Info,1);*/
	}
    int  CRenderTarget_D3D9::AdjustARGB(const SPalette& Palette,SMaskInfo* maskinfo,int maskcount)
	{
		assert(0);
		//return 0;
		     if(!m_pRenderTexture)
			     return 0;
		     IDirect3DDevice9*   pDevice=m_pRenderFactory->GetDevice();
	       
			 IDirect3DTexture9*   bufferTexture;
			 IDirect3DSurface9*  pbufferSurface =m_pRenderFactory->GetBufferRenderTexture(m_nWidth,m_nHeight,&bufferTexture);

			 
			
			 pDevice->ColorFill(pbufferSurface, 0, D3DCOLOR_XRGB(0, 0, 0));
		   
		     pDevice->SetRenderTarget(0, pbufferSurface);
             HRESULT hr =  pbufferSurface->Release();

             
			 KKUI_Vertex Vec[4];
             pDevice->BeginScene();
			
			// pDevice->SetVertexShader(0);
			 CARGBPixelShader_D3D9*  pPixelShader_D3D9 = m_pRenderFactory->GetDefARGBPixelShader();

			 if(pPixelShader_D3D9)
			 {
							 LPDIRECT3DPIXELSHADER9	  pPixelShader=pPixelShader_D3D9 ->GetPixelShader();
							 hr= pDevice->SetPixelShader(pPixelShader);
							 

							 hr= pDevice->SetTexture(0, m_pRenderTexture);
							 //放大系数 ,放大采样方式
							 pDevice->SetSamplerState(0, D3DSAMP_MAGFILTER,  D3DTEXF_NONE);
							   //缩小采样，方式
							 pDevice->SetSamplerState(0, D3DSAMP_MINFILTER,  D3DTEXF_LINEAR);
							 pDevice->SetSamplerState(0, D3DSAMP_MIPFILTER,  D3DTEXF_LINEAR);
							 pDevice->SetSamplerState(0, D3DSAMP_ADDRESSU,   D3DTADDRESS_CLAMP);
							 pDevice->SetSamplerState(0, D3DSAMP_ADDRESSV,   D3DTADDRESS_CLAMP);
							 pDevice->SetFVF(KKUI_Vertex::FVF);

							 INT  PsMaskVect[30]={0,0};
							 D3DXVECTOR4 Vector[20];
							 int lx = 0;
							 
							SMaskInfo& info=*maskinfo;
								  
							for(int i=0;i<info.count;i++){
								Vector[i].x=info.uvinfo[i].x;
								Vector[i].y=info.uvinfo[i].y;
							} 


							pPixelShader_D3D9->SetInt("PsMaskCount",4);
							pPixelShader_D3D9->SetVectorArray("PsMaskVert",Vector,20);
							pPixelShader_D3D9->SetInt("nAlp",Palette.Alpha);		 
			 }
           

			 Vec[0].x = 0.0;
			 Vec[0].y = 0.0;
			 Vec[0].z = 1.0;
			 Vec[0].u = 0.0;
			 Vec[0].v = 0.0;
			 Vec[1].w = 1.0;

			 Vec[1].x = m_nWidth;
			 Vec[1].y = 0.0;
			 Vec[1].z = 1.0;
			 Vec[1].u = 1.0;
			 Vec[1].v = 0.0;
			 Vec[1].w = 1.0;

			 Vec[2].x = 0.0;
			 Vec[2].y = m_nHeight;
			 Vec[2].z = 1.0;
			 Vec[2].u = 0.0;
			 Vec[2].v = 1.0;
			 Vec[2].w = 1.0;

			 Vec[3].x = m_nWidth;
			 Vec[3].y = m_nHeight;
			 Vec[3].z = 1.0;
			 Vec[3].u = 1.0;
			 Vec[3].v = 1.0;
			 Vec[3].w = 1.0;

			 hr= pDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2,  Vec, sizeof(KKUI_Vertex));
           //  pDevice->SetVertexShader( 0 );
             pDevice->SetPixelShader( 0 );	
			 pDevice->EndScene();

            // return 0;

			 IDirect3DTexture9*  pRenderTexture = this->GetRenderTexture();
			 IDirect3DSurface9*  pRenderSurface = 0;
             hr = pRenderTexture->GetSurfaceLevel(0, &pRenderSurface) ;
			 if (FAILED(hr)){
					  assert(0);
					  return 0;
			 }
			 hr =  pRenderSurface->Release();

		  

			 pDevice->StretchRect(pbufferSurface,0,pRenderSurface,0,D3DTEXF_NONE);
			 
			 
			 


			 return 1;
	}

	//
	int   CRenderTarget_D3D9::DrawTexture(IDirect3DTexture9* pTex)
	{
		   IDirect3DDevice9*   pDevice=m_pRenderFactory->GetDevice();


		     IDirect3DSurface9  *pSurface;
			 pTex->GetSurfaceLevel(0,&pSurface);

 
			 pDevice->StretchRect(pSurface,0,m_pSwapChainSurface9,0,D3DTEXF_NONE );
			 return 1;
	}

	 IDirect3DTexture9*    CRenderTarget_D3D9::FilterYuv420p2(IPicYuv420p *pYuv420p,SinkFilter* Filter)
	{
		 CYuv420p_D3D9*      pYUV     = (CYuv420p_D3D9*)pYuv420p; 
		 IDirect3DDevice9*   pDevice  = m_pRenderFactory->GetDevice();
		 IDirect3DTexture9*  pSrcTexture = pYUV->GetTexture();

		 kkSize     sz=       pYUV ->Size();
		 
         KKUI_Vertex Vec[4];
	     Vec[0].x = 0.0;
		 Vec[0].y = 0.0;
		 Vec[0].z = 1.0;
		 Vec[0].u = 0.0;
		 Vec[0].v = 0.0;
		 Vec[0].w = 1.0;

		 Vec[1].x =  sz.cx;
		 Vec[1].y = 0.0;
		 Vec[1].z = 1.0;
		 Vec[1].u = 1.0;
		 Vec[1].v = 0.0;
		 Vec[1].w = 1.0;

		 Vec[2].x = 0.0;
		 Vec[2].y =  sz.cy;
		 Vec[2].z = 1.0;
		 Vec[2].u = 0.0;
		 Vec[2].v = 1.0;
		 Vec[2].w = 1.0;

		 Vec[3].x = sz.cx;
		 Vec[3].y = sz.cy;;
		 Vec[3].z = 1.0;
		 Vec[3].u = 1.0;
		 Vec[3].v = 1.0;
		 Vec[3].w = 1.0;
		
           

	     IDirect3DTexture9* pTexture = pSrcTexture ;
		 if(pSrcTexture)
		 {
			             IDirect3DTexture9*  BufferTexture=0;
						 IDirect3DTexture9*  BufferTextureLast=0;
						 IDirect3DSurface9*  OldbufferSurface;
						 HRESULT hr = pDevice->GetRenderTarget(0, &OldbufferSurface);
						 while(Filter)
						 {
								if(BufferTexture)
                                       BufferTextureLast=BufferTexture;
								IDirect3DSurface9*  pbufferSurface =m_pRenderFactory->GetBufferRenderTexture(sz.cx,sz.cy,&BufferTexture);
								pDevice->ColorFill(pbufferSurface, 0, D3DCOLOR_ARGB(0,0, 0, 0));
								hr =  pDevice->SetRenderTarget(0, pbufferSurface);
								hr = pbufferSurface->Release();

								CPixelShader_D3D9*    ps=  m_pRenderFactory->GetPsByName(Filter->FilterName);
								hr =  pDevice->SetPixelShader(ps->GetPixelShader());

								ps->SetInt("Par1", Filter->Par1);
								ps->SetInt("Par2", Filter->Par2);
								ps->SetInt("Par3", Filter->Par3);
								ps->SetInt("Par4", Filter->Par4);
								ps->SetInt("Par5", Filter->Par5);

								ps->SetFloat("Par6", Filter->Par6);
								ps->SetFloat("Par7", Filter->Par7);
								ps->SetFloat("Par8", Filter->Par8);
								ps->SetFloat("Par9", Filter->Par9);
								ps->SetFloat("Par10", Filter->Par10);


								hr = pDevice->SetTexture(0, pTexture);
								hr = pDevice->SetFVF(KKUI_Vertex::FVF);
								//放大系数 ,放大采样方式
								pDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_NONE);
								//缩小采样，方式
								pDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_NONE);
								pDevice->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR);
								pDevice->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
								pDevice->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);


								hr= pDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2,  Vec, sizeof(KKUI_Vertex));
								//pDevice->SetVertexShader( 0 );
								pDevice->SetPixelShader( 0 );	

								Filter=Filter->Next;
								pTexture =BufferTexture;
								

								if(  BufferTextureLast){
								    m_pRenderFactory->ReleaseBufferRenderTexture(sz.cx,sz.cy,BufferTextureLast);
									BufferTextureLast = 0;
								}
						 }
						 pDevice->SetRenderTarget(0,OldbufferSurface);
		 }
	     return pTexture ;
	}

    int   CRenderTarget_D3D9::DrawYuv420p(IPicYuv420p *pYuv420p,SinkFilter*Filter,const SPalette& Palette,SVertInfo* VertInfo,int VertCount,int UseLast,SMaskInfo* maskinfo,int maskcount)
	{
		 CYuv420p_D3D9* pYUV=(CYuv420p_D3D9*)pYuv420p; 
		 IDirect3DDevice9*   pDevice=m_pRenderFactory->GetDevice();
		 IDirect3DTexture9*  pTexture = 0;
		 if(UseLast)
			 pTexture =pYUV->GetLastTexture();
		 else
		      pTexture =pYUV->GetTexture();
		 int                 TriangleCount=2;
		 int h= 0;
		 if(pTexture){
			 KKUI_Vertex vec[24],*pVecs=vec;
			 if( VertInfo!=0 && VertCount > 2 ){
				 for(int i=0;i< VertCount;i++){
					 vec[i].x = VertInfo[i].x;
					 vec[i].y = VertInfo[i].y;
					 vec[i].u = VertInfo[i].uv.x;
					 vec[i].v = VertInfo[i].uv.y;
					 vec[i].z = 0.0;// VertInfo[i].z;
					 #ifdef USE_RHW
					 vec[i].w = 1.0;
                   
                     #endif
				 }
				TriangleCount= VertCount;
                 
				

			 }else{
			    assert(0);
			 }
		
			 if(Filter){
			   pTexture = FilterYuv420p2(pYuv420p,Filter);
			 }

			h= pDevice->SetTexture(0, pTexture);
			h= pDevice->SetFVF(KKUI_Vertex::FVF);
			h = pDevice->SetVertexShader( 0 );

			 CPixelShader_D3D9*  pPixelShader_D3D9 = m_pRenderFactory->GetDefPixelShader();

			 if(pPixelShader_D3D9)
			 {
			    
				 LPDIRECT3DPIXELSHADER9	  pPixelShader   =    pPixelShader_D3D9 ->GetPixelShader();
				 HRESULT hr = pDevice->SetPixelShader(pPixelShader);
				 int index =0;
				 pDevice->SetTexture(0, pTexture);
				 float fff = (float)Palette.Alpha / 255;
				 pPixelShader_D3D9->SetFloat("fAlp", fff);
						if(maskinfo!=0&&maskcount!=0)
						{
							 
							 
							  INT  PsMaskVect[30]={0,0};
							  D3DXVECTOR4 Vector[20];
							  int lx = 0;
							  for(int j=0;j<maskcount ;j++)
							  {
								  SMaskInfo& info=maskinfo[j];
								  
								  PsMaskVect[lx]  = info.masktype;
                                  PsMaskVect[lx +1] = info.count;
								  PsMaskVect[lx +2] = info.hidepx;
								  lx += 3;
								  for(int i=0;i<info.count;i++){
									  Vector[i].x=info.uvinfo[i].x;
									  Vector[i].y=info.uvinfo[i].y;
								  } 

								  if(j==0)
								      pPixelShader_D3D9->SetVectorArray("PsMaskVert0",Vector,20);
								  else if (j == 1)
									  pPixelShader_D3D9->SetVectorArray("PsMaskVert1", Vector, 20);
								  else if (j == 2)
									  pPixelShader_D3D9->SetVectorArray("PsMaskVert2", Vector, 20);
								  else if (j == 3)
									  pPixelShader_D3D9->SetVectorArray("PsMaskVert3", Vector, 20);
								  else if (j == 4)
									  pPixelShader_D3D9->SetVectorArray("PsMaskVert4", Vector, 20);
								  else if (j == 5)
									  pPixelShader_D3D9->SetVectorArray("PsMaskVert5", Vector, 20);
							  }
						      pPixelShader_D3D9->SetIntArray("PsMaskVect",PsMaskVect,30);
							  pPixelShader_D3D9->SetInt("PsMaskCount",maskcount);
						}else{
						      pPixelShader_D3D9->SetInt("PsMaskCount",0);
						}
			 }else
			 {
		 
			 }
			
			
			 h= pDevice->DrawPrimitiveUP(D3DPT_TRIANGLELIST, TriangleCount,  pVecs, sizeof(KKUI_Vertex));/**/
			 //pDevice->SetVertexShader( 0 );
             pDevice->SetPixelShader( 0 );	
			 pDevice->SetTexture(0, NULL) ;

			 if(Filter&&pTexture){

				 	 kkSize     sz=       pYUV ->Size();
					 m_pRenderFactory->ReleaseBufferRenderTexture(sz.cx,sz.cy,pTexture);
			 }
		 }else{
			
		 }
		 return 1;
	}
    int   CRenderTarget_D3D9::FilterYuv420p(IPicYuv420p *pYuv420p,SinkFilter* Filter)
	{
		 CYuv420p_D3D9*      pYUV     = (CYuv420p_D3D9*)pYuv420p; 
		 IDirect3DDevice9*   pDevice  = m_pRenderFactory->GetDevice();
		 IDirect3DTexture9*  pSrcTexture = pYUV->GetTexture();

		 kkSize     sz=       pYUV ->Size();
		 
         KKUI_Vertex Vec[4];
	     Vec[0].x = 0.0;
			 Vec[0].y = 0.0;
			 Vec[0].z = 1.0;
			 Vec[0].u = 0.0;
			 Vec[0].v = 0.0;
			 Vec[0].w = 1.0;

			 Vec[1].x =  sz.cx;
			 Vec[1].y = 0.0;
			 Vec[1].z = 1.0;
			 Vec[1].u = 1.0;
			 Vec[1].v = 0.0;
			 Vec[1].w = 1.0;

			 Vec[2].x = 0.0;
			 Vec[2].y =  sz.cy;
			 Vec[2].z = 1.0;
			 Vec[2].u = 0.0;
			 Vec[2].v = 1.0;
			 Vec[2].w = 1.0;

			 Vec[3].x = sz.cx;
			 Vec[3].y = sz.cy;;
			 Vec[3].z = 1.0;
			 Vec[3].u = 1.0;
			 Vec[3].v = 1.0;
			 Vec[3].w = 1.0;
		
           

	     IDirect3DTexture9* pTexture = pSrcTexture ;
		 if(pSrcTexture)
		 {
						 while(Filter)
						 {
								IDirect3DSurface9*  OldbufferSurface;
								HRESULT hr =  pDevice-> GetRenderTarget(0,&OldbufferSurface);
								IDirect3DTexture9*  bufferTexture;
								IDirect3DSurface9*  pbufferSurface =m_pRenderFactory->GetBufferRenderTexture(sz.cx,sz.cy,&bufferTexture);
								pDevice->ColorFill(pbufferSurface, 0, D3DCOLOR_XRGB(0, 0, 0));
								hr =  pDevice->SetRenderTarget(0, pbufferSurface);
								hr = pbufferSurface->Release();

								CPixelShader_D3D9*    ps=  m_pRenderFactory->GetPsByName(Filter->FilterName);
								hr =  pDevice->SetPixelShader(ps->GetPixelShader());

								hr = pDevice->SetTexture(0, pTexture);
								hr = pDevice->SetFVF(KKUI_Vertex::FVF);
								//放大系数 ,放大采样方式
								pDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_NONE);
								//缩小采样，方式
								pDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
								pDevice->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR);
								pDevice->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
								pDevice->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);


								hr= pDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2,  Vec, sizeof(KKUI_Vertex));
								//pDevice->SetVertexShader( 0 );
								pDevice->SetPixelShader( 0 );	

								Filter=Filter->Next;
								if(!Filter)
								{
									 IDirect3DSurface9  *pSurface;
									 hr = pSrcTexture->GetSurfaceLevel(0,&pSurface);
									 hr = bufferTexture->GetSurfaceLevel(0, &pbufferSurface);
                    
					 
									 hr = pDevice->StretchRect(pbufferSurface,0,pSurface,0,D3DTEXF_NONE );
									 hr = pbufferSurface->Release();
									 hr = pSurface->Release();
								}
				
								pTexture =bufferTexture;
								pDevice->SetRenderTarget(0,OldbufferSurface);
						 }
		 }else {
			 return 0;
		 }
		 
	     return 0;
	}
	
	struct CUSTOMVERTEX
	{
		float x, y, z, rhw; // The transformed position for the vertex
		DWORD color;        // The vertex color
	};

	#define D3DFVF_CUSTOMVERTEX (D3DFVF_XYZRHW|D3DFVF_DIFFUSE)
   void   CRenderTarget_D3D9::DrawLine(int startx,int starty, int endx,int endy, unsigned int color)
   {
        IDirect3DDevice9* pDevice=m_pRenderFactory->GetDevice();
	
        CUSTOMVERTEX vertices[] =
		{
			{ startx,  starty, 0.0f, 1.0f, color}, // x, y, z, rhw, color
			{ endx,    endy, 0.0f, 1.0f, color },
			
		};

		
     
        pDevice->SetFVF(D3DFVF_CUSTOMVERTEX );
        pDevice->DrawPrimitiveUP( D3DPT_LINESTRIP, 1,vertices,sizeof(CUSTOMVERTEX) );
		
   }
   void   CRenderTarget_D3D9::DrawLines(kkPoint* pts,int count, unsigned int color)
   {
        IDirect3DDevice9* pDevice=m_pRenderFactory->GetDevice();
	    

		
		CUSTOMVERTEX* vertices = (CUSTOMVERTEX*)::malloc(sizeof(CUSTOMVERTEX)*count);

		for(int i=0;i< count;i++)
		{
			vertices[i].x = pts[i].x;
			vertices[i].y = pts[i].y;
			vertices[i].z =0.0;
			vertices[i].rhw =1.0;
			vertices[i].color =color;
		}
		
   
        pDevice->SetFVF(D3DFVF_CUSTOMVERTEX );
        pDevice->DrawPrimitiveUP( D3DPT_LINESTRIP, count-1,vertices,sizeof(CUSTOMVERTEX) );
		free( vertices );
   }
   void  CRenderTarget_D3D9::DrawLines(SUVInfo* pts,int count, unsigned int color)
   {
       IDirect3DDevice9* pDevice=m_pRenderFactory->GetDevice();
	    

		
		CUSTOMVERTEX* vertices = (CUSTOMVERTEX*)::malloc(sizeof(CUSTOMVERTEX)*count);
		for(int i=0;i< count;i++)
		{
			vertices[i].x = pts[i].x;
			vertices[i].y = pts[i].y;
			vertices[i].z =0.0;
			vertices[i].rhw =1.0;
			vertices[i].color =color;
		}
        pDevice->SetFVF(D3DFVF_CUSTOMVERTEX );
        pDevice->DrawPrimitiveUP( D3DPT_LINESTRIP, count-1,vertices,sizeof(CUSTOMVERTEX) );
		free( vertices );
   }
   void   CRenderTarget_D3D9::DrawText(IFont* pFont,wchar_t *text,kkRect* pRcDest,unsigned int color)
   {
		   CFont_D3D9* ff =(CFont_D3D9*)pFont;
		   ID3DXFont* Font= ff->GetD3DXFont();
				 
		   RECT rt={pRcDest->left,pRcDest->top,pRcDest->right,pRcDest->bottom };
		   Font->DrawTextW(0,text,lstrlen(text),&rt,DT_LEFT, color);
		   
   }
  
   void  CRenderTarget_D3D9::MeasureText(IFont* pFont,wchar_t *text,kkSize *pSz )
   {
	   CFont_D3D9* ff =(CFont_D3D9*)pFont;
	   ID3DXFont* Font= ff->GetD3DXFont();

	   HDC hdc=Font->GetDC();

	   SIZE sz;
	   GetTextExtentPoint32(hdc, text, lstrlen(text), &sz);
	   pSz->cx = sz.cx;
	   pSz->cy = sz.cy;
				   // IDirect3DDevice9* pDevice=m_pRenderFactory->GetDevice();
   }



    void CRenderTarget_D3D9::TextOutV(IFont* pFont,int x,int y , wchar_t* strText,unsigned int color)
	{
		
		wchar_t* p = strText;
		while(*p)
		{
			wchar_t*  p2 = p+1;
			kkSize szWord;
			wchar_t temp[2]={*p};
			MeasureText(pFont,temp,&szWord);

			kkRect Rt = {0,0};
			Rt.left = x;
			Rt.top  = y;
			Rt.bottom  = Rt.top  + szWord.cy;
			Rt.right  = Rt.left + szWord.cx;
			DrawText(pFont, temp,&Rt,color);
			p = p2;
			y += szWord.cy;

			
		}
	}


    //垂直绘制文本
   void  CRenderTarget_D3D9::DrawTextV(IFont* pFont,wchar_t *text,kkRect* pRcDest,unsigned int color)
   {

	   
	   TextOutV(pFont,
		   pRcDest->left,
		   pRcDest->top ,
           text,
		   color
        );
   }
   void  CRenderTarget_D3D9::MeasureTextV(IFont* pFont,wchar_t *text,kkSize *pSz )
   {
		kkSize szRet={0,0};
		wchar_t* p = text;
		while(*p)
		{
			wchar_t* p2 = p+1;
			kkSize szWord;
			wchar_t temp[2]={*p};
			MeasureText(pFont,temp,&szWord);
			szRet.cx = szRet.cx > szWord.cx ?szRet.cx:szWord.cx;
			szRet.cy += szWord.cy;
			p = p2;
		}
		 *pSz=szRet;
   }


   void  CRenderTarget_D3D9::Rectangle(kkRect* rt,unsigned int cr)
   {
	    IDirect3DDevice9* pDevice=m_pRenderFactory->GetDevice();
        CUSTOMVERTEX vertices[5] =
		{
			{ rt->left,  rt->top,    0.0f, 1.0f, cr  }, // x, y, z, rhw, color
			{ rt->right, rt->top,    0.0f, 1.0f, cr  },
			{ rt->right, rt->bottom, 0.0f, 1.0f, cr  },
			{ rt->left,  rt->bottom, 0.0f, 1.0f, cr  },
			{ rt->left,  rt->top,    0.0f, 1.0f, cr  },
		};
     
        pDevice->SetFVF(D3DFVF_CUSTOMVERTEX );
		pDevice->DrawPrimitiveUP ( D3DPT_LINESTRIP,4, vertices, sizeof(  CUSTOMVERTEX) );
   }
  
   void   CRenderTarget_D3D9::RectangleByPt(kkPoint* pt,int width,int height,unsigned int cr)
   {
         kkRect rt;
		 int w = width/2;
		 int h = height/2;

		 rt.left = pt->x-w;
		 rt.right = pt->x+w;
		 rt.top = pt->y-h;
		 rt.bottom = pt->y+h;

		

		IDirect3DDevice9* pDevice=m_pRenderFactory->GetDevice();
        CUSTOMVERTEX vertices[5] =
		{
			{ rt.left,  rt.top,    0.0f, 1.0f, cr  }, // x, y, z, rhw, color
			{ rt.right, rt.top,    0.0f, 1.0f, cr  },
			{ rt.right, rt.bottom, 0.0f, 1.0f, cr  },
			{ rt.left,  rt.bottom, 0.0f, 1.0f, cr  },
			{ rt.left,  rt.top,    0.0f, 1.0f, cr  },
		};
  
        pDevice->SetFVF(D3DFVF_CUSTOMVERTEX );
		pDevice->DrawPrimitiveUP ( D3DPT_LINESTRIP,4, vertices, sizeof(  CUSTOMVERTEX) );
		

   }
   int   CRenderTarget_D3D9::FillRect(kkRect* rt,unsigned int cr)
   {
	  // return 0;
	    //cr=0xffff0000;
	    IDirect3DDevice9* pDevice=m_pRenderFactory->GetDevice();
		pDevice->SetTexture(0, NULL) ;
		LPDIRECT3DVERTEXBUFFER9  pVB = NULL;
        CUSTOMVERTEX vertices[] =
		{
			{ rt->left,  rt->bottom,  1.0, 1.0f, cr, },
			{ rt->left,   rt->top,   1.0, 1.0f, cr, }, // x, y, z, rhw, color
			{ rt->right,  rt->bottom, 1.0, 1.0f, cr, },
			{ rt->right,  rt->top,    1.0, 1.0f, cr, }
		};
		pDevice->SetFVF(D3DFVF_CUSTOMVERTEX);
		pDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, vertices, sizeof(CUSTOMVERTEX));
		
		return 1;
	}

    void CRenderTarget_D3D9::FillRect(kkRect* rt,unsigned int cr1,unsigned int cr2,unsigned int cr3,unsigned int cr4)
	{
	    IDirect3DDevice9* pDevice=m_pRenderFactory->GetDevice();
		pDevice->SetTexture(0, NULL) ;
		LPDIRECT3DVERTEXBUFFER9  pVB = NULL;
        CUSTOMVERTEX vertices[] =
		{
			{ rt->left,  rt->bottom,  1.0, 1.0f, cr2, },
			{ rt->left,   rt->top,   1.0,  1.0f,  cr1, }, // x, y, z, rhw, color
			{ rt->right,  rt->bottom, 1.0, 1.0f, cr4, },
			{ rt->right,  rt->top,    1.0, 1.0f, cr3, }
		};
		pDevice->SetFVF(D3DFVF_CUSTOMVERTEX);
		pDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, vertices, sizeof(CUSTOMVERTEX));
		
		return ;
	}
    void  CRenderTarget_D3D9::FillRect(SUVInfo* pts1,SUVInfo* pts2,SUVInfo* pts3,SUVInfo* pts4,unsigned int cr1,unsigned int cr2,unsigned int cr3,unsigned int cr4)
	{
	    IDirect3DDevice9* pDevice=m_pRenderFactory->GetDevice();
		pDevice->SetTexture(0, NULL) ;
		LPDIRECT3DVERTEXBUFFER9  pVB = NULL;
        CUSTOMVERTEX vertices[] =
		{
			{  pts1->x,    pts1->y,   1.0, 1.0f,   cr1, },
			{  pts2->x,    pts2->y,   1.0,  1.0f,  cr2, }, // x, y, z, rhw, color
			{  pts3->x,    pts3->y,   1.0, 1.0f,   cr3, },
			{  pts4->x,    pts4->y,   1.0, 1.0f,   cr4, }
		};
		pDevice->SetFVF(D3DFVF_CUSTOMVERTEX);
		pDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, vertices, sizeof(CUSTOMVERTEX));
		
		return ;
	
	}
	void  CRenderTarget_D3D9::FillRectByPt(kkPoint* pt,int width,int height,unsigned int cr)
	{
	     kkRect rt;
		 int w = width/2;
		 int h = height/2;

		 rt.left = pt->x-w;
		 rt.right = pt->x+w;
		 rt.top = pt->y-h;
		 rt.bottom = pt->y+h;
		 FillRect(&rt,cr);
	}

    void CRenderTarget_D3D9::DrawCircle(int xCenter, int yCenter, int r,unsigned int FrameColor,int fillmodel)
	{
	 
		int lxxx = 2 * D3DX_PI * r + 32;
		CUSTOMVERTEX* pVertices = new CUSTOMVERTEX[lxxx];
   
		 int x,y,delta,delta1,delta2,direction;
		x=0;y=r;
		delta=2*(1-r);
		int i=0;

		if(fillmodel==1){
            i=1;
		    pVertices[0].x = xCenter;
			pVertices[0].y = yCenter;
		}
		while(y>=0){
			//(x,y)
			pVertices[i].x = x + xCenter;
			pVertices[i].y = y + yCenter;
			pVertices[i].z = 0.0f;
			pVertices[i].rhw = 1.0f;
			pVertices[i].color = FrameColor;		
			++i;
	 
	 
	       //(x,-y)
			pVertices[i].x = x + xCenter;
			pVertices[i].y = -y + yCenter;
			pVertices[i].z = 0.0f;
			pVertices[i].rhw  = 1.0f;
			pVertices[i].color = FrameColor;	
			++i;
	 
			//(-x, y)
			pVertices[i].x = -x + xCenter;
			pVertices[i].y = y + yCenter;
			pVertices[i].z = 0.0f;
			pVertices[i].rhw  = 1.0f;
			pVertices[i].color = FrameColor;	
			++i;
			//// //(-x, y)
			pVertices[i].x = -x + xCenter;
			pVertices[i].y = -y + yCenter;
			pVertices[i].z = 0.0f; 
			pVertices[i].rhw  = 1.0f;
			pVertices[i].color = FrameColor;	
	         ++i;
			


			if(delta<0)
			{   
				delta1=2*(delta+y)-1;
				if(delta1<=0)
					direction=1; 
				else 
					direction=2; 
			}else if(delta>0)
			{   
				delta2=2*(delta-x)-1;
				if(delta2<=0)
					direction=2; 
				else 
					direction=3; 
			}else  
				direction=2;

			if (direction == 1)
			{
				x++; 
				delta += 2 * x + 1;
			}
			else if (direction == 2) {
				 x++; 
				 y--; 
				 delta+=2*(x-y+1); 
			}
			else if (direction == 3) {
                 y--;
				 delta+=(-2*y+1);
			}
		}

		IDirect3DDevice9* pDevice=m_pRenderFactory->GetDevice();
	
		pDevice->SetFVF(D3DFVF_CUSTOMVERTEX);
	 
		if(fillmodel)
           pDevice->DrawPrimitiveUP(D3DPT_TRIANGLEFAN, i-2, pVertices, sizeof(CUSTOMVERTEX));
		else
		   pDevice->DrawPrimitiveUP(D3DPT_POINTLIST, i, pVertices, sizeof(CUSTOMVERTEX));
	 
		delete [] pVertices;

   }
   





   CPixelShader_D3D9:: CPixelShader_D3D9(SRenderFactory_D3D9* pRenderFactory):
	   m_pRenderFactory(pRenderFactory),m_pConstantTable(0),m_pPsCode(0),
		   m_pPixelShader(0)
   {
       CPixelShader_D3D9:: m_pRenderFactory->AddRef();
	  
   }
	   void OutHex(int cont,unsigned char* lp)
	   {
	    
		      ::OutputDebugStringA("\n");
		      char temp[64]="";
			  int jj=0;
			  for(int i=0;i<cont;i++)
			  {

				if(jj%5==0)
						::OutputDebugStringA("\n");
					jj++;

					 lp+=3;
					sprintf(temp,"%s", "0x");
					if(*lp<16)
						sprintf(temp+2,"0%x",*lp);
					else
						sprintf(temp+2,"%x",*lp);
					lp--;

					if(*lp<16)
						sprintf(temp+4,"0%x",*lp);
					else
						sprintf(temp+4,"%x",*lp);
					lp--;
					if(*lp<16)
						sprintf(temp+6,"0%x",*lp);
					else
						sprintf(temp+6,"%x",*lp);
					lp--;
					if(*lp<16)
						sprintf(temp+8,"0%x,",*lp);
					else
						sprintf(temp+8,"%x,",*lp);
	                 
					lp+=4;
	                 
					::OutputDebugStringA(temp);
					

			 }
			::OutputDebugStringA("\n");/**/
	   }
   void CPixelShader_D3D9::IniPs()
   {
	   if(0){
			CGetResource Resource(hDllModule,L"IDR_D3D9HSL_TX",L"TXT");

			UINT nResSize = 0;
			LPCSTR pData = (LPCSTR)Resource.GetResourceBuffer(nResSize);
		    
	 	   
			//ID3DXBuffer* ErrBuffer=0;
			//fpDX9_D3DXCreateBuffer(1024,&ErrBuffer);
			IDirect3DDevice9* pDevice=m_pRenderFactory->GetDevice();
			LPCSTR psver = fpDX9_D3DXGetPixelShaderProfile(pDevice);
			HRESULT hr=fpDX9_D3DXCompileShader(pData,nResSize, NULL, NULL, "hslMain",psver, 0, &m_pPsCode,NULL, &m_pConstantTable ); /**/
			if (FAILED(hr))
			{
				//char* xx=ErrBuffer->GetBufferPointer();
				assert(0);
			}
			int xx    = m_pPsCode->GetBufferSize()/4;
		    unsigned char* lp = (unsigned char*)m_pPsCode->GetBufferPointer();
			OutHex(xx ,lp);
	   }
		
	
		ReLoadRes();
   }
   CPixelShader_D3D9::~CPixelShader_D3D9()
   {

	   if(m_pConstantTable)
		   m_pConstantTable->Release();
	   if(m_pPsCode)
		   m_pPsCode->Release();
	   m_pRenderFactory->Release();
   }
  void   CPixelShader_D3D9::Release2()
  {
	  int lx=0;
	  if(m_pPixelShader){
		  lx=m_pPixelShader->Release();
		  m_pPixelShader = 0;
	  }
  }
  void   CPixelShader_D3D9::ReLoadRes()
  {
	      IDirect3DDevice9* pDevice=m_pRenderFactory->GetDevice();

		  HRESULT hr=fpDX9_D3DXGetShaderConstantTable(D3D9_HLS_Ps,&m_pConstantTable);
		  if(FAILED(pDevice->CreatePixelShader( D3D9_HLS_Ps,&m_pPixelShader)))
			{
				assert(0);
			}

 
  }
  D3DXHANDLE CPixelShader_D3D9::GetSamplerHandel(char *name)
  {
      if(m_pConstantTable)
	      return m_pConstantTable->GetConstantByName(0,name);

	
      return 0;
  }
HRESULT     CPixelShader_D3D9::GetConstantDesc(D3DXHANDLE hConstant, D3DXCONSTANT_DESC *pConstantDesc, UINT *pCount)
{
   return m_pConstantTable->GetConstantDesc(hConstant, pConstantDesc, pCount);
}
   HRESULT        CPixelShader_D3D9:: SetMatrix(char* MatrixName, D3DXMATRIX	&Matrix)
   {
       IDirect3DDevice9* pDevice=m_pRenderFactory->GetDevice();
	   return m_pConstantTable->SetMatrix( pDevice, MatrixName, &Matrix );
   }
   HRESULT        CPixelShader_D3D9::SetBool(char* Name, bool b)
   {
       IDirect3DDevice9* pDevice=m_pRenderFactory->GetDevice();
	   return m_pConstantTable->SetBool( pDevice,Name,  b );
   }
   HRESULT        CPixelShader_D3D9::SetInt(char* Name, int b)
   {
      IDirect3DDevice9* pDevice=m_pRenderFactory->GetDevice();
	   return m_pConstantTable->SetInt( pDevice,Name,  b );
   }

   HRESULT       CPixelShader_D3D9::SetFloat(char* Name, float b)
   {
	   IDirect3DDevice9* pDevice=m_pRenderFactory->GetDevice();
	   return m_pConstantTable->SetFloat( pDevice,Name,  b );
   }
   HRESULT        CPixelShader_D3D9::SetVectorArray(char* Name,CONST D3DXVECTOR4* pVector, UINT Count)
   {
       IDirect3DDevice9* pDevice=m_pRenderFactory->GetDevice();
	   return m_pConstantTable->SetVectorArray( pDevice,Name,  pVector, Count);
   }

	 HRESULT        CPixelShader_D3D9::SetIntArray( char* Name,CONST INT* pn, UINT Count) 
	 {
		   IDirect3DDevice9* pDevice=m_pRenderFactory->GetDevice();
		   return m_pConstantTable->SetIntArray( pDevice,Name,  pn, Count);
	 }

	 CYUVPixelShader_D3D9::CYUVPixelShader_D3D9(SRenderFactory_D3D9* pRenderFactory):CPixelShader_D3D9(pRenderFactory)
	 {
	    
	 }
	 CYUVPixelShader_D3D9::~CYUVPixelShader_D3D9()
	 {
	 
	 }

	 void CYUVPixelShader_D3D9::IniPs()
	 {
		 if(0){
			CGetResource Resource(hDllModule,L"IDR_D3D9HSLYUV_TX",L"TXT");
			UINT nResSize = 0;
			LPCSTR pData = (LPCSTR)Resource.GetResourceBuffer(nResSize);
		    
	 	   
			//ID3DXBuffer* ErrBuffer=0;
			//fpDX9_D3DXCreateBuffer(1024,&ErrBuffer);
			IDirect3DDevice9* pDevice=m_pRenderFactory->GetDevice();
			LPCSTR psver = fpDX9_D3DXGetPixelShaderProfile(pDevice);
			HRESULT hr=fpDX9_D3DXCompileShader(pData,nResSize, NULL, NULL, "hslMain",psver, 0, &m_pPsCode,NULL, &m_pConstantTable ); /**/
			if (FAILED(hr))
			{
				assert(0);
			}

			int xx    = m_pPsCode->GetBufferSize()/4;
		    unsigned char* lp = (unsigned char*)m_pPsCode->GetBufferPointer();
			OutHex(xx ,lp);
		 }
		     ReLoadRes();
		
	 }
	 void  CYUVPixelShader_D3D9::ReLoadRes()
	 {
	      IDirect3DDevice9* pDevice=m_pRenderFactory->GetDevice();

		  if(m_pPsCode&&0)
		  {
			if(FAILED(pDevice->CreatePixelShader( (DWORD*)m_pPsCode->GetBufferPointer(),&m_pPixelShader)))
			{
				assert(0);
			}
		  }else{

			  HRESULT hr=fpDX9_D3DXGetShaderConstantTable(D3D9_YUV420_Ps,&m_pConstantTable);
			  if(FAILED(pDevice->CreatePixelShader( D3D9_YUV420_Ps,&m_pPixelShader)))
			  {
					assert(0);
			  }
		  }
	 }

	 CARGBPixelShader_D3D9::CARGBPixelShader_D3D9(SRenderFactory_D3D9* pRenderFactory):CPixelShader_D3D9(pRenderFactory)
	 {
	    
	 }
	 CARGBPixelShader_D3D9::~CARGBPixelShader_D3D9()
	 {
	 
	 }

	 void CARGBPixelShader_D3D9::IniPs()
	 {
	     ReLoadRes();		
	 }
	 void  CARGBPixelShader_D3D9::ReLoadRes()
	 {
	        IDirect3DDevice9* pDevice=m_pRenderFactory->GetDevice();

			HRESULT hr=fpDX9_D3DXGetShaderConstantTable(D3D9_ARGB_Ps,&m_pConstantTable);
			if (FAILED(pDevice->CreatePixelShader(D3D9_ARGB_Ps, &m_pPixelShader)))
			{
				assert(0);
			}
	 }




	CCommPixelShader_D3D9::CCommPixelShader_D3D9(SRenderFactory_D3D9* pRenderFactory,const DWORD*  m_psCode,const char* psName):
	 CPixelShader_D3D9(pRenderFactory),
	 m_pPsCode(m_psCode),
	 m_strPsName(psName)
    {
   
    }
   CCommPixelShader_D3D9::~CCommPixelShader_D3D9()
   {
   
   }
   void CCommPixelShader_D3D9::IniPs()
   {
       ReLoadRes();		
   }
   void  CCommPixelShader_D3D9::ReLoadRes()
   {
	
	        IDirect3DDevice9* pDevice=m_pRenderFactory->GetDevice();
			HRESULT hr=fpDX9_D3DXGetShaderConstantTable(m_pPsCode,&m_pConstantTable);
			if (FAILED(pDevice->CreatePixelShader(m_pPsCode, &m_pPixelShader)))
			{
				assert(0);
			}
   }
   const char* CCommPixelShader_D3D9::GetPsName()
   {
	   return m_strPsName.c_str();
   }

}

	
extern "C"
{
	void  __declspec(dllexport) CreateRenderFactoryD3D9(KKUI::IRenderFactory** ff)
	{
		KKUI::SRenderFactory_D3D9* f = new KKUI::SRenderFactory_D3D9();
		*ff=f;
	}
}