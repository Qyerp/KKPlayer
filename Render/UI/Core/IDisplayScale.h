#ifndef IDisplayScale_H_
#define IDisplayScale_H_
//支持缩放的控件支持，非dpi的
namespace KKUI
{
	class IDisplayScale
	{
	  public:
		    virtual ~IDisplayScale(){}
			virtual void SetDisplayScale(float fDisplayScale) = 0;
	};
}
#endif