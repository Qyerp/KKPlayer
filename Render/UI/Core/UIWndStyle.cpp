#include "UIWndStyle.h"
#include "SplitString.h"
namespace KKUI
{

	bool ParseValue(const SStringA & strValue, unsigned int & value)
	{
		int r = 255, g = 255, b = 255, a = 255;
		int nSeg=0;
		SStringA strValueL = strValue;
		strValueL.MakeLower();
		if(strValueL.Left(1)=="#")
		{
			nSeg = sscanf(strValueL.GetBuffer(100),"#%02x%02x%02x%02x",&r,&g,&b,&a);
		}else if(strValueL.Left(4).CompareNoCase("rgba")==0)
		{
			nSeg = sscanf(strValueL,"rgba(%d,%d,%d,%d)",&r,&g,&b,&a);                
		}else if(strValueL.Left(3).CompareNoCase("rgb")==0)
		{
			nSeg = sscanf(strValueL,"rgb(%d,%d,%d)",&r,&g,&b);                
		}
		if(nSeg!=3 && nSeg != 4)
		{
	    
			return false;
		}else
		{
			value = ARGB(r,g,b,a);
		//	value = ARGB(a,r,g,b);
			return true;
		}
	}
unsigned int  GetColor(const SStringA & strValue)
{
	unsigned int  value=CR_INVALID ;
      if( ParseValue(strValue, value))
	  {
	    
	  }
	  return value;
}
	CUIWndStyle::CUIWndStyle():
        m_nScale(100),
		m_bTrackMouseEvent(0),
		m_crBg(CR_INVALID),                /**<������ɫ */
		m_crBorder(CR_INVALID)
	{
	
	}
    int  CUIWndStyle::GetScale() const 
	{
		return m_nScale;
	}

	CRect CUIWndStyle::GetMargin() const
	{
		CRect rcRet;
		rcRet.left = m_rcMargin[0].toPixelSize(GetScale());
		rcRet.top = m_rcMargin[1].toPixelSize(GetScale());
		rcRet.right = m_rcMargin[2].toPixelSize(GetScale());
		rcRet.bottom = m_rcMargin[3].toPixelSize(GetScale());
		return rcRet;
	}

	CRect CUIWndStyle::GetPadding() const
	{
		CRect rcRet;
		rcRet.left = m_rcInset[0].toPixelSize(GetScale());
		rcRet.top = m_rcInset[1].toPixelSize(GetScale());
		rcRet.right = m_rcInset[2].toPixelSize(GetScale());
		rcRet.bottom = m_rcInset[3].toPixelSize(GetScale());
		return rcRet;
	}
	int CUIWndStyle::OnAttrMargin(const SStringA &strValue,int bLoading)
	{
		_ParseLayoutSize4(strValue, m_rcMargin);
		return !bLoading?S_OK:S_FALSE;
	}
	void CUIWndStyle::_ParseLayoutSize4(const SStringA & strValue, CLayoutSize layoutSizes[])
	{
		std::vector< SStringA> values;
		size_t nValues = SplitString(strValue, ',', values);
		if (nValues == 1)
		{
			layoutSizes[0] = layoutSizes[1] = layoutSizes[2] = layoutSizes[3] = CLayoutSize::fromString(values[0]);
		}
		else if (nValues == 2)
		{
			layoutSizes[0] = layoutSizes[2] = CLayoutSize::fromString(values[0]);
			layoutSizes[1] = layoutSizes[3] = CLayoutSize::fromString(values[1]);
		}
		else if (nValues == 4)
		{
			layoutSizes[0] = CLayoutSize::fromString(values[0]);
			layoutSizes[1] = CLayoutSize::fromString(values[1]);
			layoutSizes[2] = CLayoutSize::fromString(values[2]);
			layoutSizes[3] = CLayoutSize::fromString(values[3]);
		}
	}
}