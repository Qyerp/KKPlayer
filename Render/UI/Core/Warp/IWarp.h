#ifndef IWarp_H_
#define IWarp_H_
#include "Core/IRender.h"
#include "UIMisc.h"
#include <vector>
namespace KKUI
{
		typedef struct WarpInfo
		{
			 int rowId;
			 int colId;
			 WarpInfo()
			 {
				 rowId =-1;
				 colId =-1;
			 }
		}WarpInfo;

		//变形操作,只对输出通道有效
		class IWarp
		{
		   public:
				   IWarp( ):m_nXDivision(0),m_nYDivision(0)
				   {
				   
				   }
				   virtual  ~IWarp(){};  
				 
				   virtual  void  OnPaint(IRenderTarget  *pRT,const CRect& rt) = 0;
				     //绘制网格
                   virtual  void  OnPaintMesh(IRenderTarget  *pRT,const CRect& rt,unsigned int cr) = 0;
				   virtual  void  GetPtInfo(int RowId,int ColId, SUVInfo& Out) = 0;
				   virtual  void  SetPtInfo(int RowId,int ColId,const SUVInfo& Out) = 0;
				   //测试是否在控制点上
				   virtual  bool  TestPtInMestWarping(CPoint pt,int &rowOut,int& colOut,const CRect& rt) = 0;

				   //设置分割
				   virtual  void  SetXYDiv(int x, int y) = 0;
				   virtual  void  GetXYDiv(int &x, int &y) = 0;
				   virtual  int   GetWarpInfos(SUVInfo* infos,int count) = 0;
	               virtual  int   SetWarpInfos(SUVInfo* infos,int count) =0;
		   protected:
				  //分割 1 不分割
				  int                 m_nXDivision;
				  int                 m_nYDivision;
				  //原始点信息,顺时针，都映射成0到~1.UV信息事先映射好
				  SVertInfo              m_OriginalInfo[4];
				  std::vector<SVertInfo> m_OriginalVector;

		};

}
#endif