#include "RenderTarget_D3D9.h"
#include "Yuv420p_D3D9.h"
#include "DXVA2_D3D9.h"
#include "DX9Text.h"
#include "FastMemcpy.h"

namespace KKUI
{
	struct KKUI_Vertex
	{
		float x, y, z;
	    float  w;
		float u, v;

		enum 
		{
		   FVF = D3DFVF_TEX1|D3DFVF_XYZRHW,
		};
	};

	struct KKYUVUI_Vertex
	{
		float x, y, z;
	    float w;
		float u0, v0;
        float u1, v1;
		float u2, v2;
		enum 
		{
		   FVF = D3DFVF_TEX3|D3DFVF_XYZRHW,
		};
	};

	CYuv420p_D3D9::CYuv420p_D3D9(SRenderFactory_D3D9* pRenderFactory):
        CRttCache_D3D9(pRenderFactory),
		m_pRenderFactory(pRenderFactory),
		m_pRenderTexture(0),
		m_pLastRenderTexture(0),
		m_pClipTexture(0),
		m_pQuoteObj(0),
		m_pARGBTexture(0),
		m_nPreOk(1)
	{
		m_pRenderFactory->AddRef();
		m_Size.cx = 0;
		m_Size.cy = 0;
		m_LastSize.cx = 0;
		m_LastSize.cy = 0;
	}
	
	CYuv420p_D3D9::~CYuv420p_D3D9()
	{
		 if(m_pQuoteObj)
			m_pQuoteObj->Release();

	    Release2();
		m_pRenderFactory->RemoveObj(this);
		m_pRenderFactory->Release();

		
	}
    void CYuv420p_D3D9:: Release2()
	{
		  m_nPreOk = 0;
		  int lx=0;
		  

		  if(m_pARGBTexture)
			  m_pARGBTexture->Release();
		  m_pARGBTexture = 0;

		  if(m_pClipTexture)
				lx=m_pClipTexture->Release();
		  assert(lx == 0);
		  m_pClipTexture=0;

		  if (m_pRenderTexture){
			  ReleaseBufferRenderTexture(m_Size.cx,m_Size.cy,m_pRenderTexture);
			  m_pRenderTexture = 0;
		  }

		  if(m_pLastRenderTexture){
			  ReleaseBufferRenderTexture(m_LastSize.cx,m_LastSize.cy,m_pLastRenderTexture);
		      m_pLastRenderTexture = 0;
		  }
		  ReleaseX();
	}
	void  CYuv420p_D3D9::ReLoadRes()
	{
		  m_nPreOk = 1;
		  ReLoadResX();


		  if(  m_Size.cx!=0&&  m_Size.cy!=0)
		  {
		      IDirect3DSurface9*         pSurface = GetBufferRenderTexture(m_Size.cx,m_Size.cy,&m_pRenderTexture );
			  pSurface->Release();
		  }
	}
	//http://adec.altervista.org/blog/yuv-422-v210-10-bit-packed-decoder-in-glsl/
	int	  CYuv420p_D3D9::LoadData(const kkPicInfo* Picinfo)
	{
		     
			return 1;
	}
	

	void   CYuv420p_D3D9::SavePreFrame(bool save)
	{
			m_pRenderFactory->Lock();
		if(save)
		{
			  if(m_pLastRenderTexture){
				  ReleaseBufferRenderTexture(m_LastSize.cx,m_LastSize.cy,m_pLastRenderTexture);
				  m_pLastRenderTexture = 0;
			  }
			  if(m_pRenderTexture){
				   m_pLastRenderTexture = m_pRenderTexture;
				   m_LastSize=m_Size;

				   m_pRenderTexture = 0;
				   m_Size.cx=0;
				   m_Size.cy=0;
			  }
		}else{
		      if(m_pLastRenderTexture){
				  ReleaseBufferRenderTexture(m_LastSize.cx,m_LastSize.cy,m_pLastRenderTexture);
				  m_pLastRenderTexture = 0;
			  }
		
		}
			m_pRenderFactory->UnLock();
	}
	int     CYuv420p_D3D9::HasPreFrame()
	{
	     if(m_pLastRenderTexture)
			 return 1;
		 return 0;
	}
	int    CYuv420p_D3D9::Load(const kkPicInfo* Picinfo)
	{

        int ret = 0;
		m_pRenderFactory->Lock();
		if(m_Size.cx != Picinfo->width || m_Size.cy != Picinfo->height||!m_pRenderTexture)
		{

              if (m_pRenderTexture) 
			  {
				  ReleaseBufferRenderTexture(m_Size.cx,m_Size.cy,m_pRenderTexture);
				  m_pRenderTexture = 0;
			  }

			  IDirect3DSurface9*         pSurface = GetBufferRenderTexture(Picinfo->width,Picinfo->height,&m_pRenderTexture );
			  pSurface->Release();
			  m_Size.cx = Picinfo->width;
			  m_Size.cy = Picinfo->height; 
		 }
		if(!m_pRenderTexture || m_nPreOk==0){
			m_pRenderFactory->UnLock();
			return 0;
		}
  
         IDirect3DDevice9*    pDevice = m_pRenderFactory->GetDevice();
		 if(SUCCEEDED(pDevice->BeginScene()))
		 {
				if(Picinfo->picformat == 0||  Picinfo->picformat == 2 || Picinfo->picformat == 74)
					ret = LoadYuv(Picinfo);
				else if(Picinfo->picformat == 1)
					 ret =  LoadRGBA(Picinfo,1);
				else if(Picinfo->picformat ==3)
					ret = LoadDxva2(Picinfo);
				else if(Picinfo->picformat ==4)
					ret = LoadDxTexture(Picinfo);
				else if(Picinfo->picformat == 28)
					ret =   LoadRGBA(Picinfo);
				pDevice->EndScene();
		 }
		m_pRenderFactory->UnLock();
		return ret;
	}
	
	int   CYuv420p_D3D9::LoadDxTexture(const kkPicInfo* Picinfo)
	{
	       HRESULT hr =0;
		   IDirect3DDevice9* pDevice= m_pRenderFactory->GetDevice();
	      
		   IDXTexture* pCtx = (   IDXTexture* )Picinfo->data[5];
		   if (!pCtx)
			     return 0;

		   int format=0;
		   if(!pCtx->IsTextureValid(Picinfo->data[3],(int) Picinfo->data[4],format)){
				  
			     return 0;
		   }
						 

		    

		  
		    //��Ⱦ������
			IDirect3DSurface9*        pRenderSurface    = NULL ;
			hr = m_pRenderTexture->GetSurfaceLevel(0, &pRenderSurface) ;
			if (FAILED(hr))
			{
				  assert(0);
		          return 0;
			}
			pDevice->ColorFill(pRenderSurface, 0, D3DCOLOR_ARGB(0, 0, 0, 0));
			int lx = 0;// pRenderSurface->Release();

			IDirect3DSurface9*        pOldRenderSurface    = NULL ;
			pDevice->GetRenderTarget(0,&pOldRenderSurface);
			pDevice->SetRenderTarget(0, pRenderSurface);
		
			IDirect3DTexture9* pTextrue = ( IDirect3DTexture9*) Picinfo->data[3];
			
			{
				pDevice->SetTexture(0, pTextrue);
				KKYUVUI_Vertex vec[4];

				vec[0].x = 0;  //A     A(leftTop)->B(rightTop)->C(leftBu)->D->(rightBu)
				vec[0].y = 0;
				vec[0].z = 0.0f;
				vec[0].w = 1.f;

				vec[0].u0 = 0.0;
				vec[0].v0 = 0.0;

				vec[0].u1 = 0.0;
				vec[0].v1 = 0.0;

				vec[0].u2 = 0.0;
				vec[0].v2 = 0.0;


				vec[1].x = Picinfo->width;  //B
				vec[1].y = 0;
				vec[1].z = 0.0f;
				vec[1].w = 1.0f;

				vec[1].u0 = 1.0;
				vec[1].v0 = 0.0;

				vec[1].u1 = 1.0;
				vec[1].v1 = 0.0;

				vec[1].u2 = 1.0;
				vec[1].v2 = 0.0;

				vec[2].x = 0;   //C
				vec[2].y = Picinfo->height;
				vec[2].z = 0.0f;
				vec[2].w = 1.0f;

				vec[2].u0 = 0.0;
				vec[2].v0 = 1.0;

				vec[2].u1 = 0.0;
				vec[2].v1 = 1.0;

				vec[2].u2 = 0.0;
				vec[2].v2 = 1.0;

				vec[3].x = Picinfo->width;
				vec[3].y = Picinfo->height;
				vec[3].z = 0.0f;
				vec[3].w = 1.0f;

				vec[3].u0 = 1.0;
				vec[3].v0 = 1.0;

				vec[3].u1 = 1.0;
				vec[3].v1 = 1.0;

				vec[3].u2 = 1.0;
				vec[3].v2 = 1.0;

				pDevice->SetFVF(D3DFVF_TEX1 | D3DFVF_XYZRHW);
				pDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, vec, sizeof(KKYUVUI_Vertex));/**/
			}
         

			pRenderSurface->Release();
			if(pOldRenderSurface){
				pDevice->SetRenderTarget(0, pOldRenderSurface) ;
				pOldRenderSurface->Release();
			}
		    return 1;
	}
	int   CYuv420p_D3D9::LoadDxva2(const kkPicInfo* Picinfo)
	{
		
		   HRESULT hr =0;
		   IDirect3DDevice9* pDevice = m_pRenderFactory->GetDevice();
		   
		   IDXVA2Ctx* pCtx = (   IDXVA2Ctx* )Picinfo->data[5];
		   if (pCtx == 0)
			   return 0;

		   if(!pCtx->IsSurfaceValid(Picinfo->data[3],(int) Picinfo->data[4])){
			     return 0;
		   }
						 

		  
		    

		 
		    //��Ⱦ������
			IDirect3DSurface9*        pRenderSurface    = NULL ;
			hr = m_pRenderTexture->GetSurfaceLevel(0, &pRenderSurface) ;
			if (FAILED(hr)){
				  assert(0);
		          return 0;
			}
			pDevice->ColorFill(pRenderSurface, 0, D3DCOLOR_ARGB(0, 0, 0, 0));
			int lx = 0;// pRenderSurface->Release();

			IDirect3DSurface9*        pOldRenderSurface    = NULL ;
			pDevice->GetRenderTarget(0,&pOldRenderSurface);
			pDevice->SetRenderTarget(0, pRenderSurface);
		
			IDirect3DSurface9* pSur = (IDirect3DSurface9*) Picinfo->data[3];

			RECT rtView={0,0,Picinfo->width,Picinfo->height};
            pDevice->StretchRect(pSur,NULL,pRenderSurface,&rtView, D3DTEXF_LINEAR);  


			pRenderSurface->Release();
			if(pOldRenderSurface){
				pDevice->SetRenderTarget(0, pOldRenderSurface) ;
				pOldRenderSurface->Release();
			}
		    return 1;
	}
	int   CYuv420p_D3D9::LoadYuv(const kkPicInfo* Picinfo)
	{
			 IDirect3DTexture9*     pYTexture;
			 IDirect3DTexture9*     pUTexture;
			 IDirect3DTexture9*     pVTexture;
			 int yuvFormat=0;
			
			 if(Picinfo->picformat==0){
					yuvFormat=0;
			 }else if(Picinfo->picformat==2 || Picinfo->picformat==1){
					yuvFormat=1;
			 }else if(Picinfo->picformat==74) {
					yuvFormat=2;
			 }else {
				 assert(0);
			 }	 
			
             GetCacheYuvInfo(Picinfo->width, Picinfo->height,yuvFormat,&pYTexture,&pUTexture,&pVTexture);
			
			 
			  int pixel_h =  m_Size.cy;
			  int pixel_u =  pixel_h;
			  int pixel_v =  pixel_h;

			  if(Picinfo->picformat==0)
			  {
				   pixel_u =  pixel_h/2;
			       pixel_v =  pixel_h/2;
			  }

			   //��������
			   {
					byte *pY=Picinfo->data[0];
					byte *pU=Picinfo->data[1];
					byte *pV=Picinfo->data[2];

					
					D3DLOCKED_RECT d3d_rect;
					LRESULT lRet = pYTexture->LockRect(0, &d3d_rect, 0, 0);
					if (FAILED(lRet)){
						 assert(0);
						 return 0;
					}
					//Y
					byte *pDest = (byte *)d3d_rect.pBits;
					int stride = d3d_rect.Pitch; 
					int stride2 = Picinfo->linesize[0];
					if(stride< stride2)
					{
					  stride2 = stride;
					}

					for( int i=0;i < pixel_h;i ++){  
						   memcpy(pDest + i * stride,pY + i * Picinfo->linesize[0],stride2);  
					} 
					pYTexture->UnlockRect(0);

					//U
					D3DLOCKED_RECT d3d_rectu;
					lRet =  pUTexture->LockRect(0, &d3d_rectu, 0, 0);
					if (FAILED(lRet)){
						 assert(0);
						 return 0;
					}
					
					pDest = (byte *)d3d_rectu.pBits;
					stride = d3d_rectu.Pitch; 
					stride2 = Picinfo->linesize[1];
					if(stride< stride2)
					{
					  stride2 = stride;
					}
					for(int i=0;i <  pixel_u;i ++){  
						 memcpy(pDest+ i * stride,pU + i * Picinfo->linesize[1], stride2);  
					}  
					pUTexture->UnlockRect(0);


					//V
					D3DLOCKED_RECT d3d_rectv;
					lRet =  pVTexture->LockRect(0, &d3d_rectv, 0, 0);
					if (FAILED(lRet)){
						assert(0);
						return 0;
					}
			
					pDest  = (byte *)d3d_rectv.pBits;
					stride = d3d_rectv.Pitch; 
					stride2 = Picinfo->linesize[2];
					if(stride<stride2){
						stride2 = stride;
					}

					for(int i=0;i < pixel_v;i ++){ 
						 memcpy(pDest +  i * stride,pV + i * Picinfo->linesize[2], stride2);  
					}
					pVTexture->UnlockRect(0);
			   }

     
            HRESULT hr =0;
		    IDirect3DDevice9* pDevice= m_pRenderFactory-> GetDevice();
		    //��Ⱦ������
			IDirect3DSurface9*        pRenderSurface    = NULL ;
			hr = m_pRenderTexture->GetSurfaceLevel(0, &pRenderSurface) ;
			if (FAILED(hr))
			{
				  assert(0);
		          return 0;
			}
			
			pDevice->ColorFill(pRenderSurface,0, D3DCOLOR_ARGB(0,0, 0, 0));
			int lx = 0;// pRenderSurface->Release();

			IDirect3DSurface9*        pOldRenderSurface    = NULL ;
			pDevice->GetRenderTarget(0,&pOldRenderSurface);

			pDevice->SetRenderTarget(0, pRenderSurface);
			
			if(pYTexture)
			{
			//  pDevice->Clear( 0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 0xff000000, 1.0f, 0 );
			  CYUVPixelShader_D3D9*  pPixelShader_D3D9 = m_pRenderFactory->GetDefYuvPixelShader();
			 if(pPixelShader_D3D9)
			 {
			     LPD3DXCONSTANTTABLE      pConstantTable =      pPixelShader_D3D9 ->GetConstantTable();
				 LPDIRECT3DPIXELSHADER9	  pPixelShader   =      pPixelShader_D3D9 ->GetPixelShader();
				 D3DXHANDLE	              YHandle          =    pPixelShader_D3D9 ->GetSamplerHandel("YTex");
				 D3DXHANDLE	              UHandle          =    pPixelShader_D3D9 ->GetSamplerHandel("UTex");
				 D3DXHANDLE	              VHandle          =    pPixelShader_D3D9 ->GetSamplerHandel("VTex");

				// pPixelShader_D3D9->SetInt()
				 HRESULT hr= pDevice->SetPixelShader(pPixelShader);
				  int index = pConstantTable->GetSamplerIndex( VHandle);
				 pDevice->SetTexture(0, pYTexture);
				 pDevice->SetTexture(1, pUTexture);
				 pDevice->SetTexture(2, pVTexture);
				
				 if(Picinfo->picformat==0)
					  pPixelShader_D3D9->SetInt("PicFormat",0);
				 else  if (Picinfo->picformat == 74)
					 pPixelShader_D3D9->SetInt("PicFormat", 3);
				 else 
                      pPixelShader_D3D9->SetInt("PicFormat",1);
				
				/**/for(int i=0;i<3;i++)
				 {
				        pDevice->SetSamplerState(i, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
						pDevice->SetSamplerState(i, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
						pDevice->SetSamplerState(i, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR);
						pDevice->SetSamplerState(i, D3DSAMP_ADDRESSU,D3DTADDRESS_CLAMP);
						pDevice->SetSamplerState(i, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
				 }

				    KKYUVUI_Vertex vec[4];
					
					vec[0].x =0 ;  //A     A(leftTop)->B(rightTop)->C(leftBu)->D->(rightBu)
					vec[0].y =0;
					vec[0].z = 0.0f;
					vec[0].w = 1.f;

					vec[0].u0 = 0.0;
					vec[0].v0 = 0.0;

	                vec[0].u1 = 0.0;
					vec[0].v1 = 0.0;

					vec[0].u2 = 0.0;
					vec[0].v2 = 0.0;


					vec[1].x = Picinfo->width;  //B
					vec[1].y =  0;
					vec[1].z = 0.0f;
					vec[1].w = 1.0f;

					vec[1].u0 = 1.0;
					vec[1].v0 = 0.0;

                    vec[1].u1 = 1.0;
					vec[1].v1 = 0.0;

					vec[1].u2 = 1.0;
					vec[1].v2 = 0.0;			      

					vec[2].x = 0;   //C
					vec[2].y = Picinfo->height;
					vec[2].z = 0.0f;
					vec[2].w = 1.0f;

					vec[2].u0 = 0.0;
					vec[2].v0 = 1.0;

					vec[2].u1 = 0.0;
					vec[2].v1 = 1.0;

			        vec[2].u2 = 0.0;
					vec[2].v2 = 1.0;

					vec[3].x = Picinfo->width;
					vec[3].y = Picinfo->height;
					vec[3].z = 0.0f;
					vec[3].w = 1.0f;

					vec[3].u0 = 1.0;
					vec[3].v0 = 1.0;

				    vec[3].u1 = 1.0;
					vec[3].v1 = 1.0;

					vec[3].u2 = 1.0;
					vec[3].v2 = 1.0;

			        pDevice->SetFVF( D3DFVF_TEX3|D3DFVF_XYZRHW);
					pDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2,  vec, sizeof(KKYUVUI_Vertex));/**/ 
					pDevice->SetVertexShader( 0 );
                    pDevice->SetPixelShader( 0 );	
					pDevice->SetTexture(0, 0);
				    pDevice->SetTexture(1, 0);
				    pDevice->SetTexture(2, 0);

			 }
		}



			
		lx=pRenderSurface->Release();
		if(pOldRenderSurface){
			pDevice->SetRenderTarget(0, pOldRenderSurface) ;
			pOldRenderSurface->Release();
		}
	    return 1;
	}
	int   CYuv420p_D3D9::LoadRGBA   (const kkPicInfo* Picinfo,int IsBGRA)
	{ 
		  HRESULT hr =0;
		  IDirect3DDevice9* pDevice= m_pRenderFactory-> GetDevice();
         
          if(!m_pARGBTexture){
		        hr =  pDevice->CreateTexture(
			    Picinfo->width, Picinfo->height,
				1,
				D3DUSAGE_DYNAMIC,
				D3DFMT_A8R8G8B8,//D3DFMT_R5G6B5,
				D3DPOOL_DEFAULT,
				&m_pARGBTexture,
				NULL) ;
				if (FAILED(hr))
				{
					 assert(0);
				}
				
		   }
		   
		   
		   byte* imgBuf = (byte *)Picinfo->data[0];
		   if (!imgBuf)
			   return 0;

          
		   D3DLOCKED_RECT d3d_rect;
		   hr = m_pARGBTexture->LockRect(0, &d3d_rect, 0,  D3DLOCK_DISCARD);
		   if (FAILED( hr)){
					 assert(0);
					 return 0;
		   }
		  
		   byte * pDest = (byte *)d3d_rect.pBits;
		   int stride = d3d_rect.Pitch; 
		   int stride2 = Picinfo->linesize[0];
		   if(stride< stride2){
			  stride2 = stride;
			}

		   for(int i=0;i<Picinfo->height;i++)
		   {
			    memcpy_fast(pDest+ i * stride,imgBuf + i * Picinfo->linesize[0], stride2);    
		   }
			
		   m_pARGBTexture->UnlockRect(0);

		   if(IsBGRA)
			   m_pRenderFactory->CopyToTex(m_pARGBTexture,m_Size.cx,m_Size.cy,m_pRenderTexture,0);
		   else
		       m_pRenderFactory->CopyToTex(m_pARGBTexture,m_Size.cx,m_Size.cy,m_pRenderTexture,"RGBATOARGB");
		   return 1;
	}

	IDirect3DTexture9*   CYuv420p_D3D9::LoadToClipTexture(SUVInfo xy1,SUVInfo xy2,SUVInfo xy3,SUVInfo xy4,int clipwidth,int clipheight)
	{
		  HRESULT hr =0;
		  IDirect3DDevice9* pDevice= m_pRenderFactory-> GetDevice();
		  if(m_ClipSize.cx!=clipwidth || m_ClipSize.cy!=clipheight ){

			   if(m_pClipTexture){
				   m_pClipTexture->Release();
				   m_pClipTexture = 0;
			   }

				m_ClipSize.cx = clipwidth;
				m_ClipSize.cy = clipheight ;

		        hr =  pDevice->CreateTexture(
			    clipwidth, clipheight,
				1,
				D3DUSAGE_RENDERTARGET,
				D3DFMT_A8R8G8B8,//D3DFMT_R5G6B5,
				D3DPOOL_DEFAULT,
				&m_pClipTexture,
				NULL) ;
				if (FAILED(hr))
				{
					 assert(0);
					 return 0;
				}
		   }


		   IDirect3DSurface9*        pOldRenderSurface    = NULL ;
		   pDevice->GetRenderTarget(0,&pOldRenderSurface);

		   IDirect3DSurface9*        pRenderSurface    = NULL ;
		   hr = m_pClipTexture->GetSurfaceLevel(0,&pRenderSurface);
		   if (FAILED(hr)){
				 assert(0);
			}


			hr = pDevice->SetRenderTarget(0,pRenderSurface);

			// if( SUCCEEDED( pDevice->BeginScene() ) )
           {
                   //  pDevice->Clear( 0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 0xffff0000, 1.0f, 0 );
 
					 KKUI_Vertex vec[4];
					
					vec[0].x =0 ;  //A     A(leftTop)->B(rightTop)->C(leftBu)->D->(rightBu)
					vec[0].y =0;
					vec[0].z = 0.0f;
					vec[0].w = 1.0f;
					vec[0].u = xy1.x;
					vec[0].v = xy1.y;
				

					vec[1].x =  clipwidth;  //B
					vec[1].y =  0;
					vec[1].z = 0.0f;
					vec[1].w = 1.0f;
					vec[1].u = xy2.x;
					vec[1].v = xy2.y;
			      

					vec[2].x = 0;   //C
					vec[2].y = clipheight;
					vec[2].z = 0.0f;
					vec[2].w = 1.0f;
					vec[2].u = xy3.x;
					vec[2].v = xy3.y;
			    

					vec[3].x = clipwidth;
					vec[3].y = clipheight;
					vec[3].z = 0.0f;
					vec[3].w = 1.0f;
					vec[3].u = xy4.x;
					vec[3].v = xy4.y;

				 /*   pDevice->SetVertexShader( 0 );
                    pDevice->SetPixelShader( 0 );	*/
					if(m_pQuoteObj!=0){
			             pDevice->SetTexture(0, m_pQuoteObj->GetTexture());
					}else{
					     pDevice->SetTexture(0, m_pRenderTexture);
					}
			        pDevice->SetFVF(KKUI_Vertex::FVF);
					pDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2,  vec, sizeof(KKUI_Vertex));/**/

		         // pDevice->EndScene();
			       m_pClipTexture->Release();
					if(pOldRenderSurface){
						pDevice->SetRenderTarget(0, pOldRenderSurface) ;
						pOldRenderSurface->Release();
					}
			}
		    return m_pClipTexture;
	}

    
	kkSize  CYuv420p_D3D9::Size() const
	{
		if(m_pQuoteObj)
			return m_pQuoteObj->Size();

		return  m_Size;
	}
	void    CYuv420p_D3D9::CreatePicCache(int width,int height,int format)
	{
		m_pRenderFactory->Lock();
	    CreateRttCache(width,height);
		if(format==74)
		{
		          IDirect3DTexture9*      pYTexture;
				  IDirect3DTexture9*      pUTexture;
		    IDirect3DTexture9*      pVTexture;

			int yuvFormat = 0;

			if (format == 0) {
				yuvFormat = 0;
			}
			else if (format == 2 || format == 1) {
				yuvFormat = 1;
			}
			else if (format == 74) {
				yuvFormat = 2;
			}
			else {
				assert(0);
			}

			this->GetCacheYuvInfo(width, height, yuvFormat, &pYTexture, &pUTexture, &pVTexture);

		}
	    m_pRenderFactory->UnLock();
	}
	IDirect3DTexture9*  CYuv420p_D3D9::GetTexture()
	{   
		if(m_pQuoteObj)
			return m_pQuoteObj->GetTexture();

		if(m_pLastRenderTexture==m_pRenderTexture&&m_pRenderTexture!=0)
			assert(0);
		return m_pRenderTexture; 
	}
	IDirect3DTexture9*     CYuv420p_D3D9::GetLastTexture()
	{
	   if(m_pQuoteObj)
			return m_pQuoteObj->GetLastTexture();

	   if (m_pLastRenderTexture == m_pRenderTexture&&m_pRenderTexture != 0)
		   assert(0);
		return m_pLastRenderTexture; 
	}
	void          CYuv420p_D3D9::ClearPicData(unsigned int cr)
	{ 

           HRESULT hr =0;
	       m_pRenderFactory->Lock();
		
		   if (m_pRenderTexture) {

			      IDirect3DDevice9* pDevice= m_pRenderFactory-> GetDevice();

				  if(SUCCEEDED(pDevice->BeginScene()))
				  {
					  IDirect3DSurface9*        pRenderSurface    = NULL ;
					  m_pRenderTexture->GetSurfaceLevel(0, &pRenderSurface) ;
				    
					  hr =pDevice->ColorFill(pRenderSurface , 0, D3DCOLOR_ARGB(0, 0, 0, 0));
					  pRenderSurface ->Release();
					  pDevice->EndScene();

				  }
				  
				  ReleaseBufferRenderTexture(m_Size.cx,m_Size.cy,m_pRenderTexture);
				  m_pRenderTexture = 0;
		   }
		  
          m_pRenderFactory->UnLock();
		
	}
	void          CYuv420p_D3D9::SetQuoteObj(IPicYuv420p*     pParentObj)
	{
	    if(m_pQuoteObj)
			m_pQuoteObj->Release();

		pParentObj->AddRef();
		m_pQuoteObj=(CYuv420p_D3D9*)pParentObj;
	}
}