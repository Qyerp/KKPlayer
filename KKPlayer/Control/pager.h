#pragma once
#include "OnlineResources.h"
#include "../PublicFunction/sigslot.h"
using namespace sigslot; 

class CPager:public DuiLib::CContainerUI
{
public:
	CPager(DuiLib::CPaintManagerUI* ppm, CBookLibrary* pBookLibrary, int count = 6, int pageSize = 1);
	void Init(int count = 6, int pageSize = 4, int pageIndex = 1);
	void PreBtn();
	void NextBtn();
	~CPager(void);
	signal0<> Clicked;

private:

	bool OnTheEvent (void* pParam);
	void OnClick( DuiLib::TEventUI* pEvent, DuiLib::CContainerUI* pContainer );
	void AddEllipsisButton( int posX );
	void AddButton( int i, int posX, int pageIndex );

private:
	vector<DuiLib::CButtonUI*> m_pBtnList;
	DuiLib::CPaintManagerUI* m_ppm;
	CBookLibrary* m_pBookLibrary;
	int m_count;/*列表总共多少条记录*/
	int m_pageSize;/*每页多少条记录*/
	int m_pageCount;/*总共多少页*/
	int m_pageIndex;/*当前第几页*/

};
