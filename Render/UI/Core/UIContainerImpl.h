///这里的UI只是一个简单UI，目的是为了方便视频渲染
#ifndef KKUI_IUIContainerImpl_H_
#define KKUI_IUIContainerImpl_H_
#include "IUIContainer.h"
#include "UIWindow.h"
#include <set>
namespace KKUI{

	class CUIContainerImpl:public CUIWindow,public IUIContainer
	{
		 KKUI_CLASS_NAME(CUIWindow,CUIContainerImpl,"uiroot")
		 public:
		 	      CUIContainerImpl();
				  ~CUIContainerImpl();
				  virtual void           ClearHover(CUIWindow  *pChild);
                  virtual int            OnReleaseUIWndCapture();
				  virtual CUIWindow*     OnSetUIWndCapture( CUIWindow* swnd);
				  virtual CUIWindow*     GetUIWndCapture();
                  virtual void           SetMessage(IUIMessage*         mess);
                  virtual void           SetIRenderFactory(IRenderFactory* pRenderFactory);
                  IRenderFactory*        GetIRenderFactory();
				  virtual IUIMessage*  GetIUIMessage();
				  virtual int  DoFrameEvent(unsigned int uMsg,unsigned int wParam,long lParam);
				  virtual void OnFrameKeyDown(unsigned int nChar,unsigned int  nRepCnt,unsigned int nFlags);
				  virtual void OnFrameMouseMove(unsigned int uFlag,const int& x,const int& y);
				  virtual void OnFrameMouseLeave(unsigned int uFlag);
				  virtual int  OnFrameSetCursor(const int& x,const int& y);
				  virtual void OnFrameMouseEvent(unsigned int  uMsg,unsigned int wParam,long lParam);
				  virtual void OnFrameMouseWheel(unsigned int  uMsg,unsigned int wParam,long lParam);
				  virtual void OnNextFrame();
				  virtual bool RegisterTimelineHandler( ITimelineHandler *pHandler );
				  virtual bool UnregisterTimelineHandler( ITimelineHandler *pHandler);
	     protected:
			       void OnUiTimer( char cTimerID);
				   std::set<ITimelineHandler*> m_lstTimelineHandler;
			       IUIMessage*         m_pIUIMessage;
				   IRenderFactory*     m_pRenderFactory;
			       CUIWindow*          m_pCapture;
                   CUIWindow*          m_pHover;
				   int                 m_bNcHover;
	};
}
#endif