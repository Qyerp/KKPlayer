﻿#include "UiLayout.h"
#include "SplitString.h"
#include "Core/UIWindow.h"
#include <math.h>
#include <list>
#include <algorithm>
namespace KKUI
{
	 enum
    {
        POS_INIT=0x11000000,    //坐标的初始化值
        POS_WAIT=0x12000000,    //坐标的计算依赖于其它窗口的布局
    };


	CUILayoutParam::CUILayoutParam()
	{
		Clear();
	}
    CUILayoutParam::~CUILayoutParam()
    {
	
	}
    bool CUILayoutParam::IsMatchParent(ORIENTATION orientation) const
    {
        switch(orientation)
        {
        case Horz:
            return width.isMatchParent();
        case Vert:
            return height.isMatchParent();
        case Any:
            return IsMatchParent(Horz) || IsMatchParent(Vert);
        case Both:
        default:
            return IsMatchParent(Horz) && IsMatchParent(Vert);
        }
    }

    bool CUILayoutParam::IsSpecifiedSize(ORIENTATION orientation) const
    {
        switch(orientation)
        {
        case Horz:
            return width.isSpecifiedSize();
        case Vert:
            return height.isSpecifiedSize();
        case Any:
            return IsSpecifiedSize(Horz) || IsSpecifiedSize(Vert);
        case Both:
        default:
            return IsSpecifiedSize(Horz) && IsSpecifiedSize(Vert);
        }
    }

    bool CUILayoutParam::IsWrapContent(ORIENTATION orientation) const
    {
        switch(orientation)
        {
        case Horz:
            return width.isWrapContent() || (nCount == 0 && !width.isValid());
        case Vert:
            return height.isWrapContent()|| (nCount == 0 && !height.isValid());
        case Any:
            return IsWrapContent(Horz) || IsWrapContent(Vert);
        case Both:
        default:
            return IsWrapContent(Horz) && IsWrapContent(Vert);
        }
    }

	CLayoutSize CUILayoutParam::GetSpecifiedSize(ORIENTATION orientation) const
    {
		if (orientation == Vert)
			return height;
		else
			return width;
    }



    int CUILayoutParam::OnAttrOffset(const SStringA & strValue,int bLoading)
    {
        float fx,fy;
        if(2!=sscanf(strValue,"%f,%f",&fx,&fy))
        {
            return S_FALSE;
        }
        fOffsetX = fx;
        fOffsetY = fy;
        return S_OK;
    }

    int CUILayoutParam::ParsePosition12( const SStringA & strPos1, const SStringA &strPos2 )
    {
        if(strPos1.IsEmpty() || strPos2.IsEmpty()) 
            return 0;
        POS_INFO pos1,pos2;
        if(!StrPos2ItemPos(strPos1,pos1) || !StrPos2ItemPos(strPos2,pos2) )
            return 0;
        if(pos1.pit == PIT_SIZE || pos2.pit == PIT_SIZE)//前面2个属性不能是size类型
            return 0;
        posLeft = pos1;
        posTop = pos2;
        nCount = 2;
        return 1;
    }

    int CUILayoutParam::ParsePosition34( const SStringA & strPos3, const SStringA &strPos4 )
    {
        if(strPos3.IsEmpty() || strPos4.IsEmpty()) 
			return 0;
        POS_INFO pos3,pos4;
        if(!StrPos2ItemPos(strPos3,pos3) || !StrPos2ItemPos(strPos4,pos4) ) 
			return 0;

        posRight = pos3;
        posBottom = pos4;
        nCount = 4;
        return 1;
    }

    int CUILayoutParam::StrPos2ItemPos( const SStringA &strPos,POS_INFO & pos )
    {
        if(strPos.IsEmpty()) 
			return 0;

        if(strPos.Left(4)=="sib.")
        {
            int nOffset = 0;
            if(strPos.Mid(4,5) == "left@")
            {
                pos.pit = PIT_SIB_LEFT;
                nOffset = 5;

            }else if(strPos.Mid(4,6) == "right@")
            {
                pos.pit = PIT_SIB_RIGHT;
                nOffset = 6;
            }else if(strPos.Mid(4,4) == "top@")
            {
                pos.pit = PIT_SIB_TOP;
                nOffset = 4;
            }else if(strPos.Mid(4,7) == "bottom@")
            {
                pos.pit = PIT_SIB_BOTTOM;
                nOffset = 7;
            }else
            {
                return 0;
            }
            SStringA strValue = strPos.Mid(4+nOffset);
			SStringAList values;
			if(SplitString(strValue,':',values) != 2)
                return 0;
			pos.nRefID = atoi(values[0]);
            if(pos.nRefID == 0) 
                return 0;
			pos.nPos.parseString(values[1]);

            if(pos.nPos.fSize < 0)
            {
                pos.nPos.fSize *= -1;
                pos.cMinus = -1;
            }else
            {
                pos.cMinus = 1;
            }
        }else
        {
			 SStringA strPos2= strPos;
            char* pszPos = strPos2.GetBuffer(100);
            switch(pszPos[0])
            {
            case POSFLAG_REFCENTER: pos.pit=PIT_CENTER,pszPos++;break;
            case POSFLAG_PERCENT: pos.pit=PIT_PERCENT,pszPos++;break;
            case POSFLAG_REFPREV_NEAR: pos.pit=PIT_PREV_NEAR,pszPos++;break;
            case POSFLAG_REFNEXT_NEAR: pos.pit=PIT_NEXT_NEAR,pszPos++;break;
            case POSFLAG_REFPREV_FAR: pos.pit=PIT_PREV_FAR,pszPos++;break;
            case POSFLAG_REFNEXT_FAR: pos.pit=PIT_NEXT_FAR,pszPos++;break;
            case POSFLAG_SIZE:pos.pit=PIT_SIZE,pszPos++;break;
            default: pos.pit=PIT_NORMAL;break;
            }

            pos.nRefID = -1;//not ref sibling using id
            if(pszPos [0] == L'-')
            {
                pos.cMinus = -1;
                pszPos ++;
            }else
            {
                pos.cMinus = 1;
            }
            pos.nPos.parseString(pszPos);
        }

        return 1;
    }

    int CUILayoutParam::OnAttrPos(const SStringA & strValue,int bLoading)
    {
        SStringAList strLst;
        SplitString(strValue,L',',strLst);
        if(strLst.size() != 2 && strLst.size() != 4) 
        {
           // SASSERT_FMTW(L"Parse pos attribute failed, strPos=%s",strValue);
            return S_FALSE;
        }
        //增加pos属性中的空格兼容。
        for(size_t i=0;i<strLst.size();i++)
        {
            strLst[i].TrimBlank();
        }
        int bRet = 1;

        bRet = ParsePosition12(strLst[0],strLst[1]);
        if(strLst.size() == 4)
        {
            bRet = ParsePosition34(strLst[2],strLst[3]);
        }
		if(bRet && nCount == 4)
		{//检测X,Y方向上是否为充满父窗口
			if((posLeft.pit == PIT_NORMAL && posLeft.nPos.isZero() && posLeft.cMinus==1)
				&&(posRight.pit == PIT_NORMAL && posRight.nPos.isZero() && posRight.cMinus==-1))
			{
				width.setMatchParent();
			}else if(posRight.pit == PIT_SIZE)
			{   
				if(posRight.cMinus == -1)
					width.setWrapContent();
				else
					width = posRight.nPos;
			}else
			{
				width.setInvalid();
			}

			if((posTop.pit == PIT_NORMAL && posTop.nPos.isZero() && posTop.cMinus==1)
				&&(posBottom.pit == PIT_NORMAL && posBottom.nPos.isZero() && posBottom.cMinus==-1))
			{
				height.setMatchParent();
			}
			else if(posBottom.pit == PIT_SIZE)
			{
				if(posBottom.cMinus == -1)
					height.setWrapContent();
				else
					height = posBottom.nPos;
			}else
			{
				height.setInvalid();
			}
		}else
		{
			if(!width.isValid()) 
				SetWrapContent(Horz);
			if(!height.isValid()) 
				SetWrapContent(Vert);
		}


        return S_OK;
    }

    int CUILayoutParam::OnAttrSize(const SStringA & strValue,int bLoading)
    {
		SStringAList szStr ;
		if(2!=SplitString(strValue,',',szStr)) 
			return S_FALSE;

		OnAttrWidth(szStr[0],bLoading);
		OnAttrHeight(szStr[1],bLoading);
        return S_OK;
    }

    int CUILayoutParam::OnAttrHeight(const SStringA & strValue,int bLoading)
    {
        if(strValue.CompareNoCase("matchParent") == 0 || strValue.CompareNoCase("full") == 0)
            height.setMatchParent();
        else if(strValue.CompareNoCase("wrapContent") == 0)
            height.setWrapContent();
        else
			height.parseString(strValue);
        return S_OK;
    }

    int CUILayoutParam::OnAttrWidth(const SStringA & strValue,int bLoading)
    {
        if(strValue.CompareNoCase("matchParent") == 0 || strValue.CompareNoCase("full") == 0)
            width.setMatchParent();
        else if(strValue.CompareNoCase("wrapContent") == 0)
            width.setWrapContent();
        else
			width.parseString(strValue);
        return S_OK;
    }

    bool CUILayoutParam::IsOffsetRequired(ORIENTATION orientation) const
    {
        return fabs(orientation==Vert?fOffsetY:fOffsetX) > 0.00000001f;
    }

    int GetPosExtra(const POS_INFO &pos,int nScale)
    {
        return pos.cMinus==-1?pos.nPos.toPixelSize(nScale):0;
    }

    int CUILayoutParam::GetExtraSize(ORIENTATION orientation,int nScale) const
    {
		if(nCount!=4) return 0;
        if(orientation == Horz)
            return GetPosExtra(posRight, nScale);
        else
            return GetPosExtra(posBottom, nScale);
    }

	void CUILayoutParam::Clear()
	{
		nCount = 0;
		fOffsetX = fOffsetY = 0.0f;

		width.setWrapContent();
		height.setWrapContent();
	}

	void CUILayoutParam::SetMatchParent(ORIENTATION orientation)
	{
        switch(orientation)
        {
        case Horz:
            width.setMatchParent();
            break;
        case Vert:
            height.setMatchParent();
            break;
        case Both:
            width.setMatchParent();
			height.setMatchParent();
            break;
        }
	}

	void CUILayoutParam::SetWrapContent(ORIENTATION orientation)
	{
		switch(orientation)
        {
        case Horz:
			width.setWrapContent();
            break;
        case Vert:
			height.setWrapContent();
            break;
        case Both:
            width.setWrapContent();
			height.setWrapContent();
            break;
        }
	}

	void CUILayoutParam::SetSpecifiedSize(ORIENTATION orientation, const CLayoutSize& layoutSize)
	{
        switch(orientation)
        {
        case Horz:
            width = layoutSize;
            break;
        case Vert:
            height = layoutSize;
            break;
        case Both:
            width = height = layoutSize;
            break;
        }
	}

	void * CUILayoutParam::GetRawData()
	{
		return (LayoutParamStruct*)this;
	}

	CUILayout::CUILayout(void)
	{
	
	}
	CUILayout::~CUILayout(void)
	{
	
	}
	bool CUILayout::IsParamAcceptable(ILayoutParam *pLayoutParam) const
	{
	     return !!pLayoutParam->IsClass(CUILayoutParam::GetClassName());
	}
	void CUILayout::LayoutChildren(CUIWindow * pParent)
	{
		std::list<WndPos> lstWndPos;
	   
		CUIWindow *pChild=pParent->GetNextLayoutChild(NULL);
		while(pChild)
		{
			WndPos wndPos;
			wndPos.pWnd = pChild;
			wndPos.rc = CRect(POS_INIT,POS_INIT,POS_INIT,POS_INIT);
			CUILayoutParam* pParam = pChild->GetLayoutParamT<CUILayoutParam>();
			wndPos.bWaitOffsetX = pParam->IsOffsetRequired(Horz);
			wndPos.bWaitOffsetY = pParam->IsOffsetRequired(Vert);
			lstWndPos.push_back(wndPos);

			pChild=pParent->GetNextLayoutChild(pChild);
		}

		if(lstWndPos.empty())
			return;

		CRect rcParent = pParent->GetChildrenLayoutRect();
		//计算子窗口位置
		CalcPostion(&lstWndPos,rcParent.Width(),rcParent.Height());

		//偏移窗口坐标
		std::list<WndPos>::iterator pos=lstWndPos.begin();
		while(pos!=lstWndPos.end())
		{
			WndPos wp = *pos;
			wp.rc.OffsetRect(rcParent.left,rcParent.top);
			pos++;
			wp.pWnd->OnRelayout(wp.rc);
		}
	}
	ILayoutParam * CUILayout::CreateLayoutParam() const
	{
	   return 0;
	}

	CSize CUILayout::MeasureChildren(CUIWindow* pParent,int nWidth,int nHeight) const
	{
		std::list<WndPos>       lstWndPos;

			CUIWindow*  pChild= pParent->GetNextLayoutChild(NULL);
			while(pChild)
			{
				int lx = pChild->IsFloat();
				int lx2 = pChild->IsVisible(0);
				int lx3 = pChild->IsDisplay();
				if(!pChild->IsFloat() && (pChild->IsVisible(0) || pChild->IsDisplay()))
				{//不显示且不占位的窗口不参与计算
					WndPos wndPos;
					wndPos.pWnd = pChild;
					wndPos.rc = CRect(POS_INIT,POS_INIT,POS_INIT,POS_INIT);
					CUILayoutParam *pParam = pChild->GetLayoutParamT<CUILayoutParam>();
					wndPos.bWaitOffsetX = pParam->IsOffsetRequired(Horz);
					wndPos.bWaitOffsetY = pParam->IsOffsetRequired(Vert);
					lstWndPos.push_back(wndPos);
				}
				pChild=pParent->GetNextLayoutChild(pChild);
			}
	        
			//计算子窗口位置
			CalcPositionEx(&lstWndPos,nWidth,nHeight);

			//计算子窗口范围
			int nMaxX = 0,nMaxY = 0;
			std::list<WndPos>::iterator pos=lstWndPos.begin();
			while(pos!=lstWndPos.end())
			{
				WndPos wndPos =*pos;
				CUILayoutParam *pParam = wndPos.pWnd->GetLayoutParamT<CUILayoutParam>();
				int nScale = wndPos.pWnd->GetScale();
				if(!IsWaitingPos(wndPos.rc.right))
				{
					nMaxX = (std::max)(nMaxX,(int)(wndPos.rc.right + pParam->GetExtraSize(Horz,nScale)));
				}
				if(!IsWaitingPos(wndPos.rc.bottom))
				{
					nMaxY = (std::max)(nMaxY,(int)(wndPos.rc.bottom + pParam->GetExtraSize(Vert, nScale)));
				}
				pos++;
			}

			if(!IsWaitingPos(nWidth)) 
				nWidth = nMaxX;
			if(!IsWaitingPos(nHeight)) 
				nHeight = nMaxY;
			return CSize(nWidth,nHeight);
	}
	int  CUILayout::IsWaitingPos( int nPos ) const
    {
        return nPos == POS_INIT || nPos == POS_WAIT;
    }
		static const POS_INFO posRefLeft={PIT_PREV_NEAR,-1,1};
	static const POS_INFO posRefTop={PIT_PREV_FAR,-1,1};
	int  CUILayout::CalcPostion(std::list<WndPos> *pListChildren,int nWidth,int nHeight) const
	{
	    int nResolvedAll=0;

        int nResolvedStep1 = 0;
        int nResolvedStep2 = 0;
        do{
            nResolvedStep1 = 0;
            nResolvedStep2 = 0;

            //step 1:计算出所有不需要计算窗口大小就可以确定的坐标
            int nResolved = 0;
            do{
                nResolved = 0;
			   std::list<WndPos>::iterator pos=pListChildren->begin();
                for(;pos!=pListChildren->end();pos++)
                {
                    WndPos &wndPos = *pos;
                    CUILayoutParam *pLayoutParam = wndPos.pWnd->GetLayoutParamT<CUILayoutParam>();
					int nScale = wndPos.pWnd->GetScale();
                    if(IsWaitingPos(wndPos.rc.left)) 
                    {
						const POS_INFO &posRef = pLayoutParam->nCount>=2 ? pLayoutParam->posLeft:posRefLeft;
						wndPos.rc.left = PositionItem2Value(pListChildren,pos,posRef,nWidth,1, nScale);
                        if(wndPos.rc.left != POS_WAIT) 
							nResolved ++;
                    }
                    if(IsWaitingPos(wndPos.rc.top)) 
                    {
						const POS_INFO &posRef = pLayoutParam->nCount>=2 ? pLayoutParam->posTop:posRefTop;
						wndPos.rc.top = PositionItem2Value(pListChildren,pos,posRef,nHeight,0, nScale);
                        if(wndPos.rc.top != POS_WAIT) 
							nResolved ++;
                    }
                    if(IsWaitingPos(wndPos.rc.right)) 
                    {
						if(pLayoutParam->IsMatchParent(Horz))
						{
							wndPos.rc.right = nWidth;
						}else if(pLayoutParam->IsSpecifiedSize(Horz))
                        {
                            if(!IsWaitingPos(wndPos.rc.left))
                            {
                                wndPos.rc.right = wndPos.rc.left + pLayoutParam->GetSpecifiedSize(Horz).toPixelSize(nScale);
                                nResolved ++;
                            }
                        }else if(!pLayoutParam->IsWrapContent(Horz) && pLayoutParam->nCount==4)
                        {
                            wndPos.rc.right = PositionItem2Value(pListChildren,pos,pLayoutParam->posRight,nWidth,1, nScale);
                            if(wndPos.rc.right != POS_WAIT) 
								nResolved ++;
                        }
                    }
                    if(IsWaitingPos(wndPos.rc.bottom)) 
                    {
						if(pLayoutParam->IsMatchParent(Vert))
						{
							wndPos.rc.bottom = nHeight;
						}else if(pLayoutParam->IsSpecifiedSize(Vert))
                        {
                            if(!IsWaitingPos(wndPos.rc.top))
                            {
                                wndPos.rc.bottom = wndPos.rc.top + pLayoutParam->GetSpecifiedSize(Vert).toPixelSize(nScale);
                                nResolved ++;
                            }
                        }else if(!pLayoutParam->IsWrapContent(Vert) && pLayoutParam->nCount==4)
                        {
                            wndPos.rc.bottom = PositionItem2Value(pListChildren,pos,pLayoutParam->posBottom,nHeight,0, nScale);

                            if(wndPos.rc.bottom != POS_WAIT) 
								nResolved ++;
                        }
                    }

                }

                nResolvedStep1 += nResolved;
            }while(nResolved);

            if(nResolvedStep1>0)
            {
                int nResolved = 0;
                //step 2:计算出自适应大小窗口的Size,对于可以确定的窗口完成offset操作
                do{
                    nResolved = 0;
					
					for(std::list<WndPos>::iterator pos = pListChildren->begin();pos!=pListChildren->end();pos++)
                    {
                        WndPos &wndPos = *pos;
                        CUILayoutParam *pLayoutParam = wndPos.pWnd->GetLayoutParamT<CUILayoutParam>();
                        if(IsWaitingPos(wndPos.rc.left) || IsWaitingPos(wndPos.rc.top)) 
							continue;//至少确定了一个点后才开始计算

                        if((IsWaitingPos(wndPos.rc.right) && pLayoutParam->IsWrapContent(Horz)) 
							|| (IsWaitingPos(wndPos.rc.bottom) && pLayoutParam->IsWrapContent(Vert)))
                        {//
                            int nWid = IsWaitingPos(wndPos.rc.right)? nWidth : (wndPos.rc.right - wndPos.rc.left);
                            int nHei = IsWaitingPos(wndPos.rc.bottom)? nHeight : (wndPos.rc.bottom - wndPos.rc.top);
                            kkSize szWnd = wndPos.pWnd->GetDesiredSize(nWid,nHei);
                            if(pLayoutParam->IsWrapContent(Horz)) 
                            {
                                wndPos.rc.right = wndPos.rc.left + szWnd.cx;
                                nResolved ++;
                            }
                            if(pLayoutParam->IsWrapContent(Vert)) 
                            {
                                wndPos.rc.bottom = wndPos.rc.top + szWnd.cy;
                                nResolved ++;
                            }
                        }
                        if(!IsWaitingPos(wndPos.rc.right) && wndPos.bWaitOffsetX)
                        {
                            wndPos.rc.OffsetRect((int)(wndPos.rc.Width()*pLayoutParam->fOffsetX),0);
                            wndPos.bWaitOffsetX=false;
                        }
                        if(!IsWaitingPos(wndPos.rc.bottom) && wndPos.bWaitOffsetY)
                        {
                            wndPos.rc.OffsetRect(0,(int)(wndPos.rc.Height()*pLayoutParam->fOffsetY));
                            wndPos.bWaitOffsetY=false;
                        }
                    }
                    nResolvedStep2 += nResolved;
                }while(nResolved);
            }//end if(nResolvedStep1>0)

            nResolvedAll += nResolvedStep1 + nResolvedStep2;
        }while(nResolvedStep2 || nResolvedStep1);
	
        return nResolvedAll;
	} 
	int  CUILayout::PositionItem2Value(std::list<WndPos> *pLstChilds,std::list<WndPos>::iterator position,const POS_INFO &pos , int nMax,int bX,int nScale) const
	{
	     	int nRet=POS_WAIT;

			switch(pos.pit)
			{
				case PIT_CENTER: //参考中心
					if(nMax != SIZE_WRAP_CONTENT) 
						nRet=pos.nPos.toPixelSize(nScale) * pos.cMinus + nMax/2;
					break;
				case PIT_NORMAL: 
					if(pos.cMinus == -1)
					{//参考右边或者下边
						if(nMax != SIZE_WRAP_CONTENT) 
							nRet=nMax-pos.nPos.toPixelSize(nScale);
					}else
					{
						nRet=pos.nPos.toPixelSize(nScale);
					}
					break;
				case PIT_PERCENT: 
					if(nMax != SIZE_WRAP_CONTENT)
					{
						float fPercent = pos.nPos.fSize;
						if(fPercent<0.0f) 
							fPercent = 0.0f;
						if(fPercent>100.0f) 
							fPercent = 100.0f;
						if(pos.cMinus == -1)
							nRet=(int)((100.0f-fPercent)*nMax/100);
						else
							nRet=(int)(fPercent*nMax/100);
					}
					break;
				case PIT_PREV_NEAR:
				case PIT_PREV_FAR:
					{
						std::list<WndPos>::iterator positionPrev = --position;
						int nRef = POS_WAIT;
						if(positionPrev!=pLstChilds->end())
						{
							WndPos wndPos = *positionPrev;
							if(bX)
							{
								if(!wndPos.bWaitOffsetX) 
									nRef = (pos.pit == PIT_PREV_NEAR)?wndPos.rc.right:wndPos.rc.left;
							}
							else
							{
								if(!wndPos.bWaitOffsetY) 
									nRef = (pos.pit == PIT_PREV_NEAR)?wndPos.rc.bottom:wndPos.rc.top;
							}
						}else
						{
							nRef = 0;
						}
						if(!IsWaitingPos(nRef))
							nRet=nRef+pos.nPos.toPixelSize(nScale)*pos.cMinus;
					}
					break;
				case PIT_NEXT_NEAR:
				case PIT_NEXT_FAR:
					{
						std::list<WndPos>::iterator positionNext = ++position;
						int nRef = nMax;
						if(positionNext!=pLstChilds->end())
						{
							nRef = POS_WAIT;
							WndPos wndPos = *positionNext;
							if(bX)
							{
								if(!wndPos.bWaitOffsetX) 
									nRef = (pos.pit == PIT_NEXT_NEAR)?wndPos.rc.left:wndPos.rc.right;
							}
							else
							{
								if(!wndPos.bWaitOffsetY) 
									nRef = (pos.pit == PIT_NEXT_NEAR)?wndPos.rc.top:wndPos.rc.bottom;
							}
						}
						if(!IsWaitingPos(nRef))
							nRet=nRef+pos.nPos.toPixelSize(nScale)*pos.cMinus;
					}
					break;
				case PIT_SIB_LEFT:// PIT_SIB_LEFT == PIT_SIB_TOP
				case PIT_SIB_RIGHT://PIT_SIB_RIGHT == PIT_SIB_BOTTOM
					{
						WndPos wndPos = *position;
						assert(pos.nRefID>0);

						WndPos wndPosRef = {0};
						std::list<WndPos>::iterator posTmp = pLstChilds->begin();
						while(posTmp!=pLstChilds->end())
						{
							WndPos wp = *(++posTmp);
							if(wp.pWnd->GetID() == pos.nRefID)
							{
								wndPosRef = wp;
								break;
							}
						}
						if(!wndPosRef.pWnd)
						{//没有找到时,使用父窗口信息
							wndPosRef.rc = CRect(0,0,nMax,nMax);
							wndPosRef.bWaitOffsetX = wndPosRef.bWaitOffsetY = false;
						}
						CRect rcRef = wndPosRef.rc;

						if(bX)
						{
							if(!wndPosRef.bWaitOffsetX)
							{
								long refPos = (pos.pit == PIT_SIB_LEFT)?rcRef.left:rcRef.right;
								if(IsWaitingPos(refPos))
									nRet=POS_WAIT;
								else
									nRet=refPos+pos.nPos.toPixelSize(nScale)*pos.cMinus;
							}
						}else
						{
							if(!wndPosRef.bWaitOffsetY)
							{
								long refPos = (pos.pit == PIT_SIB_TOP)?rcRef.top:rcRef.bottom;//PIT_SIB_TOP == PIT_SIB_LEFT
								if(IsWaitingPos(refPos))
									nRet=POS_WAIT;
								else
									nRet=refPos+pos.nPos.toPixelSize(nScale)*pos.cMinus;
							}
						}
					}       
					break;
			}

			return nRet;
	}

	void CUILayout::CalcPositionEx(std::list<WndPos> *pListChildren,int nWidth,int nHeight) const
	{
	    CalcPostion(pListChildren,nWidth,nHeight);

        //将参考父窗口右边或者底边的子窗口设置为wrap_content并计算出大小

        int nResolved = 0;
		for(std::list<WndPos>::iterator pos = pListChildren->begin();pos!= pListChildren->end();pos++)
        {
            WndPos &wndPos = *pos;
            CUILayoutParam *pLayoutParam = wndPos.pWnd->GetLayoutParamT<CUILayoutParam>();
            if(!IsWaitingPos(wndPos.rc.left) &&
                !IsWaitingPos(wndPos.rc.top) &&
                (IsWaitingPos(wndPos.rc.right) && IsWaitingPos(nWidth) || 
                IsWaitingPos(wndPos.rc.bottom) && IsWaitingPos(nHeight)))
            {
                int nWid = IsWaitingPos(wndPos.rc.right)? nWidth : (wndPos.rc.right - wndPos.rc.left);
                int nHei = IsWaitingPos(wndPos.rc.bottom)? nHeight : (wndPos.rc.bottom - wndPos.rc.top);
                CSize szWnd = wndPos.pWnd->GetDesiredSize(nWid,nHei);
                if(pLayoutParam->IsWrapContent(Horz)) 
                {
                    wndPos.rc.right = wndPos.rc.left + szWnd.cx;
                    if(wndPos.bWaitOffsetX)
                    {
                        wndPos.rc.OffsetRect((int)(wndPos.rc.Width()*pLayoutParam->fOffsetX),0);
                        wndPos.bWaitOffsetX=false;
                    }
                    nResolved ++;
                }
                if(pLayoutParam->IsWrapContent(Vert)) 
                {
                    wndPos.rc.bottom = wndPos.rc.top + szWnd.cy;
                    if(wndPos.bWaitOffsetY)
                    {
                        wndPos.rc.OffsetRect(0,(int)(wndPos.rc.Height()*pLayoutParam->fOffsetY));
                        wndPos.bWaitOffsetY=false;
                    }
                    nResolved ++;
                }
            }
        } 
	}
}

 
