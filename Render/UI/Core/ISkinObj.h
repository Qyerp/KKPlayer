#ifndef ISkinObj_H_
#define ISkinObj_H_
#include "IRender.h"
#include "IkkDraw.h"
#include "ObjectImp.h"
///皮肤对象接口
///主要参考 soui
namespace KKUI{
	class ISkinObj: public CObject, public IObjRef
	{
		public:
			///绘制
			virtual void      Draw(IRenderTarget *pRT,kkRect &rcDraw, unsigned int dwState)=0;
			virtual kkSize    GetSkinSize()=0;
			virtual           SStringA GetName() const =0;
			virtual int       GetScale() const =0;
	};
	typedef TObjRefImpl<ISkinObj> CSkinObj;
}

#endif