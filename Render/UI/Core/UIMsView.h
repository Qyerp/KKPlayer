#ifndef  KKUI_UIMsView_H_
#define  KKUI_UIMsView_H_
#include "UIMoveState.h"
#include "UIScrollView.h"
#include "UIContainerImpl.h"
#include "UISkin.h"
#include "UIYUV.h"

#include <vector>
//多媒体操作试图,
namespace KKUI
{
	class  CUIYUVGroup;
	class CUIMsView: public CUIScrollView
	{
	    KKUI_CLASS_NAME(CUIScrollView,CUIMsView,"uimsview")
	    public:
			  CUIMsView();
			  ~CUIMsView();
			  void          InsertChild(CUIWindow *pNewChild,CUIWindow *pInsertAfter=UICWND_LAST);
			  void          DeleteStage(CUIMoveState *Child);
			  bool          DestroyChild(CUIWindow  *pChild);
			  //创建一个组
              CUIYUVGroup*  CreateYuvGroup();
			  //删除一个组
			  int           DelYuvGroup     (CUIYUVGroup* group);
			  //交换控件位置
			  void          GroupSwopYuv(CUIYUVGroup* Group,CUIYUV* srcYuv,CUIYUV* DestYuv,int pre);
              int           SetGroupVisible (void* pGroup, int visble);

			  void          SetActiveYUV( CUIYUV*  pCurActiveYUV);
			  CUIYUV*       GetActiveYUV();

			  void          SetActiveStage(CUIMoveState* stage);
			  CUIMoveState* GetActiveStage();
			  //舞台所在区域完全包含Rt
			  CUIMoveState* GetStageByRect(CRect rt);

              void          AddYuvByGroup         (void* Group,CUIYUV* yuv);
			  void          DelYuvByGroup         (void* Group,CUIYUV* yuv);
			  void          InsertYuvByGroup      (void* Group,CUIYUV* yuv,int indexId);
              void          SetEditorModel        (int nEditorModel);
			  float         GetDisplayScale       (){return m_fDisplayScale;}
			  void          SetDisplayScale       (float Scale);
			  //先出一个简单的矩形磁性吸附
			  int           CheckMagnetisRect( CUIWindow* desctl , CPoint* pt,int ptCount,SMagnetRegion& M);
	    protected:
			  //刷新舞台数据
			  void          RrefreshStageCtls();
			  void          ClcMagnetisRect(CPoint* pt,CPoint* DestPts,SMagnetRegion& M);
			  void          OnSize(unsigned int nType, int x,int y);
			  void          OnEraseBkgnd( IRenderTarget  *pRT);
			  void          OnNcPaint(IRenderTarget *pRT);
			  void          OnMouseLeave(unsigned int nFlags);
              int           OnDisplayScale( SStringA strValue,BOOL bLoading );

			  CUIWindow*    WndFromPoint( kkPoint  ptHitTest, int bOnlyText);
			  CUIWindow*    WndFromPointGroup( kkPoint  ptHitTest, int bOnlyText);
              CUIWindow*    WndFromPointStage( kkPoint  ptHitTest, int bOnlyText);

			  void   _PaintRegion(IRenderTarget *pRT,unsigned int iZorderBegin,unsigned int iZorderEnd);
			  void   PaintStage(IRenderTarget *pRT,CUIWindow **pActuvwChild ,unsigned int iZorderBegin,unsigned int iZorderEnd);
			  void   PaintLayer(IRenderTarget *pRT,CUIWindow **pActuvwChild ,unsigned int iZorderBegin,unsigned int iZorderEnd);
			  KKUI_ATTRS_BEGIN()
				            ATTR_CUSTOM("displayScale",OnDisplayScale)
			  KKUI_ATTRS_ENDEX(CUIScrollView)

			  KKUI_MSG_MAP_BEGIN()   
					MSG_UI_SIZE(OnSize)
					MSG_UI_ERASEBKGND(OnEraseBkgnd)
					MSG_UI_NCPAINT(OnNcPaint)
					MSG_UI_MOUSELEAVE(OnMouseLeave)
			  KKUI_MSG_MAP_END(CUIScrollView)
	   protected:
		     CSize      GetScrollPos();
		     void       UpdateChildrenPosition();
			 void       OnScrollPosChanged();
			 void       OnAdjustDisplayScale();
	   private:
		     //显示刻度
		     float                       m_fDisplayScale;
		     //舞台个数
		     int                         m_nStageCount;
			 std::vector<CUIMoveState*>  m_StageVec;
			 //当前激活的图层控件
			 CUIYUV*                     m_pCurActiveYUV;
			 //当前激活的舞台
			 CUIMoveState*               m_pCurActiveStage;

			 //yuv控件分组
			 CUIYUVGroup*                m_pFirstGroup;
			 CUIYUVGroup*                m_pLastGroup;
			 CUIYUVGroup*                m_pActiveGroup;
			 //分组个数
			 int                         m_nGroupCount;
			 //模式编辑 0 图层编辑， 1 舞台编辑
			 int                         m_nEditorModel;


	};
}
#endif