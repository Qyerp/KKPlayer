#include "UIContainerImpl.h"
#include "UIWindow.h"
void OutputDebugStringKK(char* lpOutputString);
namespace KKUI{
	  
	CUIContainerImpl::CUIContainerImpl():m_pIUIMessage(0),
			       m_pCapture(0),
                   m_pHover(0),
				   m_bNcHover(0),
				   m_pRenderFactory(0)
	 {
		 SetContainer(this);
		 
	 }
	 
	 CUIContainerImpl::~CUIContainerImpl()
	 {
		 int i=0;
		 i++;
	 }

	 void      CUIContainerImpl::  ClearHover(CUIWindow  *pChild)
	 {             
		           if(pChild==m_pHover ){
	                   m_pHover =0;
				       m_bNcHover =0;
	               }
				   if(m_pCapture == pChild){
				       OnReleaseUIWndCapture();
				   }
	 }
	 int      CUIContainerImpl::OnReleaseUIWndCapture()
	 {
		 /*if(m_pCapture)
			 m_pCapture->Release();*/
		 m_pCapture=NULL;
		 return 1;
	 }
	 void      CUIContainerImpl::SetIRenderFactory(IRenderFactory* pRenderFactory)
	 {
	            m_pRenderFactory = pRenderFactory;
	 }

	 IRenderFactory*   CUIContainerImpl::GetIRenderFactory()
	 {
	 
		 return m_pRenderFactory;
	 }
     CUIWindow*  CUIContainerImpl::OnSetUIWndCapture( CUIWindow* swnd)
	 {
			 if(swnd->IsDisabled(1)) 
				  return 0;
			m_pCapture=swnd;
			
			  /*if(m_pCapture)
			     m_pCapture->AddRef();*/
			return swnd;
	 }
	 CUIWindow* CUIContainerImpl::GetUIWndCapture()
	 {
	    return m_pCapture;
	 }
	 void CUIContainerImpl::SetMessage(IUIMessage*  mess)
	 {
	   m_pIUIMessage = mess;
	 }

	IUIMessage*  CUIContainerImpl::GetIUIMessage()
	{
	   return m_pIUIMessage;
	}
	void CUIContainerImpl::OnUiTimer( char cTimerID)
	{
		if(cTimerID==TIMER_NEXTFRAME ){
		      OnNextFrame();
		}
	}
	int CUIContainerImpl::DoFrameEvent(unsigned int uMsg,unsigned int wParam,long lParam)
	{
	    int lRet=0;
 
		switch(uMsg)
		{
		    case UI_TIMER:
                OnUiTimer(wParam);
				break;
			case UI_MOUSEMOVE:
			    OnFrameMouseMove(wParam,GET_X_LPARAM(lParam),GET_Y_LPARAM(lParam));
			    break;
			case UI_MOUSEHOVER:
				OnFrameMouseEvent(uMsg,wParam,lParam);
				break;
			case UI_MOUSELEAVE:
				OnFrameMouseLeave(wParam);
				break;
			case UI_SETCURSOR :
				lRet=OnFrameSetCursor(GET_X_LPARAM(lParam),GET_Y_LPARAM(lParam));
				if(!lRet){
					this->GetIUIMessage()->UISetCursor("arrow");
				}
				break;
			case UI_KEYDOWN:
                 OnFrameKeyDown((UINT)wParam,(UINT)lParam & 0xFFFF, (UINT)((lParam & 0xFFFF0000) >> 16));
		    default:
			   if(uMsg>=UI_MOUSEFIRST && uMsg <= UI_MOUSELAST)
                  OnFrameMouseEvent(uMsg,wParam,lParam);
			   break;
		}
        return lRet;
	}
    void CUIContainerImpl::OnFrameKeyDown(unsigned int nChar,unsigned int  nRepCnt,unsigned int nFlags)
	{
	    CUIWindow* pCapture=m_pCapture;
		if (pCapture)
		{
	         BOOL bMsgHandled=FALSE;
             pCapture->SendMessage(UI_KEYDOWN,nChar,MAKELPARAM(nRepCnt,nFlags),&bMsgHandled);
		}
	}
	void CUIContainerImpl::OnFrameMouseMove(unsigned int uFlag, const int& x,const int& y)
	{

	//	OutputDebugStringKK("MouseMove \n");
		if (uFlag != 0)
		{
			int ii = 0;
			ii++;
		}
	      //没有设置鼠标捕获
		kkPoint pt={x,y};
		CUIWindow* pCapture=m_pCapture;
		if (pCapture)
		{
			if (!pCapture->IsVisible(0))
			{
				m_pCapture = 0;
				pCapture = 0;
			}
			if(pCapture&&pCapture->GetParent())
			{
				if(pCapture->GetParent()->IsPanel())
				{
					if(pCapture->GetParent()->OnNcHitTest(pt)){
                        
					 	pCapture = pCapture->GetParent();
					}
				}
			}
				
		}
		if(pCapture)
		{   
				/*if((uFlag & 0x0001) ==0x0001 ){
                    ::OutputDebugStringKK("ww2\n");
	            }*/

				//有窗口设置了鼠标捕获,不需要判断是否有TrackMouseEvent属性,也不需要判断客户区与非客户区的变化
				CUIWindow * pHover=pCapture->IsContainPoint(pt,0) ?  pCapture : NULL;
                
				if(pCapture->IsClass("uimsview")){
					if(uFlag & 0x0001 !=0x0001)
					   m_bNcHover = pCapture->OnNcHitTest(pt);
				}else{
				      m_bNcHover = pCapture->OnNcHitTest(pt);
				}
				if(m_bNcHover ==NULL&&pHover !=m_pHover&&m_pHover!=pCapture){
					//检测鼠标是否在捕获窗口间移动
					CUIWindow *pOldHover=m_pHover;
					m_pHover=pHover;
					if(pOldHover)
					{
						if(m_bNcHover) 
							pOldHover->SendMessage(UI_NCMOUSELEAVE);

						pOldHover->SendMessage(UI_MOUSELEAVE);/**/
					}
					if(pHover && !(pHover->GetState()&WndState_Hover))    
					{
						if(m_bNcHover) 
							pHover->SendMessage(UI_NCMOUSEHOVER,uFlag,MAKELPARAM(pt.x,pt.y));

						pHover->SendMessage(UI_MOUSEHOVER,uFlag,MAKELPARAM(pt.x,pt.y));/**/
					}
				}
			
				if (m_bNcHover) {
					int ii = 0;
					ii++;
				}
				
                pCapture->SendMessage(m_bNcHover?UI_NCMOUSEMOVE:UI_MOUSEMOVE , uFlag, MAKELPARAM(pt.x,pt.y));
		
				
		}else{
				CUIWindow* pHover=WndFromPoint(pt,0);
				if(m_pHover!=pHover)
				{
					//hover窗口发生了变化
					CUIWindow *pOldHover=m_pHover;
					m_pHover=pHover;
					if(pOldHover)
					{
						int bLeave=1;
						if(pOldHover->GetStyle().m_bTrackMouseEvent){
							//对于有监视鼠标事件的窗口做特殊处理
							bLeave = !pOldHover->IsContainPoint(pt,0);
						}

						if(bLeave){
							if(m_bNcHover) 
								pOldHover->SendMessage(UI_NCMOUSELEAVE);
							pOldHover->SendMessage(UI_MOUSELEAVE);
						}
					}

					if(pHover && !pHover->IsDisabled(1) && !(pHover->GetState() & WndState_Hover))
					{
						m_bNcHover=pHover->OnNcHitTest(pt);
						if(m_bNcHover) 
							pHover->SendMessage(UI_NCMOUSEHOVER,uFlag,MAKELPARAM(pt.x,pt.y));
						pHover->SendMessage(UI_MOUSEHOVER,uFlag,MAKELPARAM(pt.x,pt.y));
					}
				}
				else if(pHover && !pHover->IsDisabled(1)){

					//窗口内移动，检测客户区和非客户区的变化
					int bNcHover=pHover->OnNcHitTest(pt);
					if(bNcHover!=m_bNcHover)
					{
						m_bNcHover=bNcHover;
						if(m_bNcHover)
						{
						    pHover->SendMessage(UI_NCMOUSEHOVER,uFlag,MAKELPARAM(pt.x,pt.y));
						}
						else
						{
						    pHover->SendMessage(UI_NCMOUSELEAVE);
						}
					}
				}
			   if(pHover && !pHover->IsDisabled(1))
					pHover->SendMessage(m_bNcHover?UI_NCMOUSEMOVE:UI_MOUSEMOVE , uFlag, MAKELPARAM(pt.x,pt.y));
		}
	}
	void CUIContainerImpl::OnFrameMouseLeave(unsigned int uFlag){
		CUIWindow* pCapture = m_pCapture;
		if (pCapture)
		{
			pCapture->SendMessage(UI_MOUSELEAVE,uFlag);/**/
		}
	}
	int  CUIContainerImpl::OnFrameSetCursor(const int& x,const int& y)
	{
		CPoint pt;
		pt.x =x;
		pt.y = y;
	     CUIWindow *pCapture= m_pCapture;
		if(pCapture) 
			return pCapture->OnSetCursor(pt);
		else
		{
			CUIWindow *pHover= m_pHover;
			if(pHover && !pHover->IsDisabled(1)) 
				return pHover->OnSetCursor(pt);
		}
		return 0;
	}

	#define UI_NCMOUSEFIRST UI_NCMOUSEMOVE
    #define UI_NCMOUSELAST  UI_NCMBUTTONDBLCLK
	void CUIContainerImpl::OnFrameMouseEvent(unsigned int  uMsg,unsigned int wParam,long lParam)
	{	
		CPoint pt(GET_X_LPARAM(lParam),GET_Y_LPARAM(lParam));

		CUIWindow *pCapture=m_pCapture;
		if(m_pCapture)
		{
		
			if (!m_pCapture->WndFromPoint(pt, 0))
			{
				pCapture=0;
				m_pCapture = 0;
			}
		}

	
		if(pCapture)
		{
			m_bNcHover = pCapture->OnNcHitTest(pt);
			if(m_bNcHover) 
			{
				uMsg += (unsigned int)UI_NCMOUSEFIRST - UI_MOUSEFIRST;//转换成NC对应的消息
				//::OutputDebugStringKK("MM\n");
				
			}
			int bMsgHandled = 0;
			pCapture->SendMessage(uMsg,wParam,lParam,&bMsgHandled);
			SetMsgHandled(bMsgHandled);
		}
		else
		{
			//CPoint pt(GET_X_LPARAM(lParam),GET_Y_LPARAM(lParam));
			m_pHover=WndFromPoint( pt ,0);
			CUIWindow *pHover=m_pHover;
			if(pHover  && !pHover->IsDisabled(1))
			{
				m_bNcHover = pHover->OnNcHitTest(pt);
				int bMsgHandled = 0;
				if(m_bNcHover) 
					uMsg += (unsigned int)UI_NCMOUSEFIRST - UI_MOUSEFIRST;//转换成NC对应的消息
				pHover->SendMessage(uMsg,wParam,lParam,&bMsgHandled);
				SetMsgHandled(bMsgHandled);
			}else
			{
				SetMsgHandled(0);
			}
		}
	}
	void CUIContainerImpl::OnFrameMouseWheel(unsigned int  uMsg,unsigned int wParam,long lParam)
	{
	
	}
	void  CUIContainerImpl::OnNextFrame()
	{
	    std::set<ITimelineHandler*> lstCopy= m_lstTimelineHandler;
		std::set<ITimelineHandler*>::iterator It = lstCopy.begin();
		for(; It != lstCopy.end();It++)
		{
			ITimelineHandler* Item =  *It;
			Item->OnNextFrame();
		}
	}
	bool CUIContainerImpl::RegisterTimelineHandler( ITimelineHandler*  pHandler )
	{
	   	std::set<ITimelineHandler*>::iterator It =m_lstTimelineHandler.find(pHandler);
		if(It !=m_lstTimelineHandler.end()){
		   return 0;
		}
		m_lstTimelineHandler.insert(pHandler);
		return 1;
	}
	bool CUIContainerImpl::UnregisterTimelineHandler( ITimelineHandler *pHandler)
	{
	 	std::set<ITimelineHandler*>::iterator It =m_lstTimelineHandler.find(pHandler);
		if(It !=m_lstTimelineHandler.end())
		{
			m_lstTimelineHandler.erase( It);
		    return 1;
		}
		return 0;
	}
}