#ifndef RenderTarget_D3D11_H_
#define RenderTarget_D3D11_H_

#include <map>
#include <set>
#include <string>
#include <vector>
#include <d3d11.h>
#include <dxgi.h>
#include <D3DX11core.h>
#include "ObjRef.h"
#include "Core/ObjectImp.h"
#include "Core/IRender.h"
#include "RendLock.h"
#include "d3d11math.h"

namespace KKUI
{

    class CPixelShader_D3D11;
	class CYUVPixelShader_D3D11;
	class CRenderTarget_D3D11;
	typedef struct  SRender3DTexture11
	{
	   int Width;
	   int Height;
	   ID3D11Texture2D*      pTexture;
	   int Used;
	};
	

	class SRenderFactory_D3D11:public  TObjRefImpl<IRenderFactory>
    {
		friend class CRenderTarget_D3D11;
		public:
				SRenderFactory_D3D11();
				~SRenderFactory_D3D11();
				bool                        init();
				//初始化内置着色器
				int                          InitShader();
				int                          CreateRenderTarget(IRenderTarget ** ppRenderTarget,void* Handle,int nWid,int nHei);
				int                          CreateBitmap(IBitmap ** ppBitmap);
				int                          CreateYUV420p(IPicYuv420p ** ppBitmap);
                int                          BeforePaint(IRenderTarget* pRenderTarget);
				void                         EndPaint(IRenderTarget* pRenderTarget,bool WarpNet=0);
			    void                         CreateFont(IFont** ppFont,int FontSize,wchar_t* FontName);
				

                void                         Lock();
				void                         UnLock();

                void*                        CreateVideoDecoder(int CodecId,int Width,int Height);
				void                         DeleteVideoDecoder(void* pDXVA2VD);

				void*                        CreateDxTexture();
				void                         DeleteDxTexturer(void* DxTexture);


				ID3D11DeviceContext*         GetContext(){ return m_pDeviceContext;};
				ID3D11Device*                GetDevice(){return m_pDevice;}
				bool                         LostDevice();
				bool                         LostDeviceRestore();
				
                //获取默认像素着色器
				CPixelShader_D3D11*          GetDefPixelShader(){return m_pDefPixelShader;}
				CYUVPixelShader_D3D11*       GetDefYuvPixelShader(){ return  m_pDefYuvPixelShader;}
			
				//移除创建的对象
				void                         RemoveObj(IRenderObj* obj,int lock=1);
                ID3D11Texture2D*             GetFromFileInMemory(unsigned char* data,int len);
                void                         SaveFile(wchar_t *Path,ID3D11Texture2D* textrue);

                CPixelShader_D3D11*          GetPsByName(char* Name);
				void                         CopyToTex(ID3D11Texture2D* text1,int Width,int Height,ID3D11Texture2D* text2,char* PsName);

				ID3D11PixelShader*           GetTextPs() { return m_pTextPixelShader; }
				ID3D11InputLayout*           GetDefInputLayout(){return m_pDefInputLayout;}
				ID3D11VertexShader*          GetDefVertexShader(){return m_pDefVertexShader;}
				ID3D11PixelShader*           GetColorPs(){return m_pColorPixelShader;}
				ID3D11SamplerState*          GetDefSamplerState(){return m_pDefSamplerState;}
	    private:
			     ID3D11DeviceContext*        m_pDeviceContext;
                 ID3D11Device*               m_pDevice;
				 CRendLock                   m_lock;
				
				 //呈现对象管理器
				 std::set<IRenderObj*>        m_IRenderObjSet;
				 /********像素着色器**********/
				 std::map<std::string ,CPixelShader_D3D11*> m_IPsObjMap;

				 ID3D11RasterizerState*       m_pRasterizerState;
				 //纹理着色器
				 ID3D11PixelShader*           m_pTextPixelShader;
				 //颜色着色器
				 ID3D11PixelShader*           m_pColorPixelShader;
				 ID3D11InputLayout*           m_pDefInputLayout;
				 ID3D11VertexShader*          m_pDefVertexShader;
				 //定点这色器常量
				 ID3D11Buffer*                m_pDefVsShaderConstants;
				 VertexShaderConstants        m_VsShaderConstantsData;
				 CPixelShader_D3D11*          m_pDefPixelShader;
				 CYUVPixelShader_D3D11*       m_pDefYuvPixelShader;
				 ID3D11SamplerState*          m_pDefSamplerState;
				
    };


	class CFont_D3D11:public TObjRefImpl<IFont>
	{
		public:
			CFont_D3D11(SRenderFactory_D3D11* pRenderFactory,int FontSize,wchar_t* FontName);
			~CFont_D3D11();
			void          Release2();
			void          ReLoadRes();
			int           TextSize();
			
		private:
		
			SRenderFactory_D3D11*    m_pRenderFactory;
			int                      m_nTextSize;
			wchar_t                  m_nFontName[64];
	};

	class CBitmap_D3D11:public TObjRefImpl<IBitmap>
	{
		public:
			CBitmap_D3D11(SRenderFactory_D3D11* pRenderFactory);
			~CBitmap_D3D11();
			void          Release2();
			void          ReLoadRes();
			virtual kkSize       Size() const;
			virtual int          Width();
			virtual int          Height();
			int                  LoadFromFile(const char*FileName);
            int	                 LoadFromMemory(const void* pBuf,int szLen);
		
			ID3D11Texture2D*  GetTexture(){ return m_pTexture;}

		private:
			ID3D11Texture2D*         m_pTexture;
			SRenderFactory_D3D11*    m_pRenderFactory;
			//暂且先缓存
			void*                   m_pSrcBuffer;
			int                     m_pSrcBufferLen;
			kkSize                  m_sz;
	};
	class CRenderTarget_D3D11: public TObjRefImpl<IRenderTarget>
	{
		  public:
			     CRenderTarget_D3D11(SRenderFactory_D3D11* pRenderFactory,HWND h,int nWidth, int nHeight);
			     ~CRenderTarget_D3D11();
				 void  Release2();
				 void  ReLoadRes();
				 void  ReSize(int nWidth, int nHeight);
				 //绘制位图
				 int   DrawBitmap(kkRect* pRcDest,IBitmap *pBitmap,int xSrc,int ySrc,int cx,int cy,unsigned char byAlpha=0xFF);
				 int   DrawBitmapEx(kkRect*  pRcDest,IBitmap *pBitmap,kkRect* pRcSrc,unsigned int expendMode,unsigned char byAlpha=0xFF/**/ );
				 int   DrawBitmap9Patch(kkRect* pRcDest,IBitmap *pBitmap,kkRect* pRcSrc,kkRect* pRcSourMargin,unsigned expendMode,unsigned char byAlpha=0xFF);

				 int   DrawTexture(ID3D11Texture2D* pTex);
				
				 int   DrawYuv420p(IPicYuv420p *pYuv420p,SinkFilter*Filter,const SPalette& Palette,SVertInfo* VertInfo,int VertCount,int UseLast=0,SMaskInfo* maskinfo=0,int maskcount=0);
				 ID3D11Texture2D*   FilterYuv420p2(IPicYuv420p *pYuv420p,SinkFilter* Filter);

                 int   FilterYuv420p(IPicYuv420p *pYuv420p,SinkFilter* Filter);


                 void  DrawLine(int startx,int starty, int endx,int endy, unsigned int color);
				 void  DrawLines(kkPoint* pts,int count, unsigned int color);
				 void  DrawLines(SUVInfo* pts,int count, unsigned int color);

                 void  DrawText(IFont* pFont,wchar_t *text,kkRect* pRcDest,unsigned int color=0xFFFFFFFF);
				 void  MeasureText(IFont* pFont,wchar_t *text,kkSize *pSz );

				 //垂直绘制文本
				 void  TextOutV(IFont* pFont,int x,int y , wchar_t* strText,unsigned int color=0xFFFFFFFF);
				 void  DrawTextV(IFont* pFont,wchar_t *text,kkRect* pRcDest,unsigned int color=0xFFFFFFFF);
                 void  MeasureTextV(IFont* pFont,wchar_t *text,kkSize *pSz );


                 void  Rectangle(kkRect* rt,unsigned int cr);
				
				 void  RectangleByPt(kkPoint* pt,int width,int height,unsigned int cr);
				 int   FillRect(kkRect* rt,unsigned int cr);
				 void  FillRect(kkRect* rt,unsigned int cr1,unsigned int cr2,unsigned int cr3,unsigned int cr4);
				 void  FillRect(SUVInfo* pts1,SUVInfo* pts2,SUVInfo* pts3,SUVInfo* pts4,unsigned int cr1,unsigned int cr2,unsigned int cr3,unsigned int cr4);
				 void  FillRectByPt(kkPoint* pt,int width,int height,unsigned int cr);

				 void  DrawCircle(int xCenter, int yCenter, int nRadius,unsigned int cr,int fillmodel=0);
				 int   GetWidth(){return    m_nWidth;}
				 int   GetHeight(){return   m_nHeight;}

				  //是否启用变形
				 void  SetEnableWarp(void*  pIWarp);
				 int   GetEnableWarp(){return m_nWarp;}
				 void* GetIWarp(){return m_pIWarp;}
				 void  SetFusionzone(void* Fusionzone);
				 //是否启用融合带
				 int   GetEnableFusion(){return m_nEnableFusion;}
				 void  SetEnableFusionzone(int Fusionzone){m_nEnableFusion = Fusionzone; }
				  //当有变形是通过这个来绘制
				 void  DrawWarp(SVertInfo* VertInfo,int VertCount);

				 //绘制融合带
				 void  OnFusionzone();

				
				 IDXGISwapChain*            GetSwapChain(){return  m_pSwapChain;}
				 ID3D11RenderTargetView*    GetRTView(){return   m_pRTView;}
				 ID3D11DepthStencilView*    GetDSV(){return m_pDSV;	}

				 ID3D11Texture2D*           GetRenderTexture(){ return m_pRenderTexture;}
	     private:
			     //调整通道指定区域的ARGB值
				 int   AdjustARGB(const SPalette& Palette,SMaskInfo* maskinfo,int maskcount);
			     int   UpdateVertexBuffer(D3D11PSType pstype,const void * vertexData, size_t dataSizeInBytes);

			     void RecreateSwapChain();
			     int m_nWidth;
				 int m_nHeight;
		         HWND m_hView;
				 ID3D11Buffer*            m_pVertexBuffer;
                 UINT                     m_nStride;
                 UINT                     m_nOffset;

				 SRenderFactory_D3D11*     m_pRenderFactory;
				 //交换连
				 IDXGISwapChain*           m_pSwapChain;				
				 //渲染视图
				 ID3D11RenderTargetView*   m_pRTView;
				 
				 // 深度/模板视图
				 ID3D11DepthStencilView*    m_pDSV;	
                 std::map<unsigned long long, std::vector<SRender3DTexture11*> >   m_BufferRenderMap;
	           
				  //呈现的纹理
				 ID3D11Texture2D*         m_pRenderTexture;
				 int                      m_nWarp;
				 //启用融合带
				 int                      m_nEnableFusion; 
				 //融合带
                 void*                    m_pFusionzone;
				 void*                    m_pIWarp;

	};

	

	//像素这色器
	class CPixelShader_D3D11: public TObjRefImpl<IRenderOthert>
	{
	     public:
			     CPixelShader_D3D11(SRenderFactory_D3D11* pRenderFactory);
				 ~CPixelShader_D3D11();
				 void  Release2();
				 void  ReLoadRes();

				 ID3D11PixelShader*   GetPs(){return m_pPixelShader;}
				 virtual void IniPs();
				 virtual const char* GetPsName() { return "";}
	  protected:
			    SRenderFactory_D3D11* m_pRenderFactory;
				ID3D11PixelShader*    m_pPixelShader;
	};

	/*******yuv着色器******/
	class CYUVPixelShader_D3D11:public CPixelShader_D3D11
	{
	     public:
				   CYUVPixelShader_D3D11(SRenderFactory_D3D11* pRenderFactory);
				   ~CYUVPixelShader_D3D11(); 
				   void IniPs();
				   void  ReLoadRes();
				   const char* GetPsName(){return "yuvps";}
	};

	
	//通用着色器
	class CCommPixelShader_D3D11:public CPixelShader_D3D11
	{
	     public:
				   CCommPixelShader_D3D11(SRenderFactory_D3D11* pRenderFactory,const DWORD*  m_psCode,const char* psName );
				   ~CCommPixelShader_D3D11(); 
				   void IniPs();
				   void  ReLoadRes();
				   const char* GetPsName();
	     private:
			       const DWORD*  m_pPsCode;
				   std::string   m_strPsName;
			      
	};


}

#endif