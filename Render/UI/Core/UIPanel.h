﻿#ifndef  KKUI_UIPanel_H_
#define  KKUI_UIPanel_H_
#include "UIWindow.h"
#include "UISkin.h"
#include "Layout/LayoutSize.h"
namespace KKUI
{
	#define SSB_NULL    0
	#define SSB_HORZ    1
	#define SSB_VERT    2
	#define SSB_BOTH    (SSB_HORZ|SSB_VERT)

	typedef struct tagSBHITINFO
    {
        DWORD uSbCode:16;   //HIT位置
        DWORD bVertical:16; //是否为vertical

    } SBHITINFO,*PSBHITINFO;
    inline bool operator !=(const SBHITINFO &a, const SBHITINFO &b)
    {
        return memcmp(&a,&b,sizeof(SBHITINFO))!=0;
    }

	enum PanelMouseState
	{
	    PMS_No,
		PMS_InScroll,
		PMS_InScrollMove,
		PMS_Leave,
		PMS_LBDown,
		PMS_LBUp1,
		PMS_LBUp2,
	};
    class CUIPanel: public CUIWindow
    {
			KKUI_CLASS_NAME(CUIWindow,CUIPanel, "uipanel")
		public:
				CUIPanel();
				virtual ~CUIPanel() {}

				BOOL       ShowScrollBar(int wBar, BOOL bShow);
				BOOL       EnableScrollBar(int wBar,BOOL bEnable);

				BOOL       IsScrollBarEnable(BOOL bVertical);

				void       SetScrollInfo(SCROLLINFO si,BOOL bVertical);

				BOOL       SetScrollPos(BOOL bVertical, int nNewPos,BOOL bRedraw);

				int        GetScrollPos(BOOL bVertical);

				BOOL       SetScrollRange(BOOL bVertical,int nMinPos,int nMaxPos,BOOL bRedraw);

				BOOL       GetScrollRange(BOOL bVertical,    LPINT lpMinPos,    LPINT lpMaxPos);
				BOOL       HasScrollBar(BOOL bVertical);    
				SBHITINFO  HitTest(CPoint pt);

				int        OnNcHitTest(kkPoint pt);
				void       GetClientRect(kkRect* pRect) const;
				virtual    CRect   GetClientRect() const;
	            virtual int        IsPanel(){return 1;}
    protected:
			CRect        GetSbPartRect(BOOL bVertical,UINT uSBCode);
			CRect        GetSbRailwayRect(BOOL bVertical);
			CRect        GetScrollBarRect(BOOL bVertical);
			int          OnCreate(int);
			void         OnNcPaint(IRenderTarget *pRT);
			virtual BOOL OnNcHitTest(CPoint pt);
			void         OnNcLButtonDown(UINT nFlags, CPoint point);
			void         OnLButtonUp(UINT nFlags, CPoint point);
			void         OnNcLButtonUp(UINT nFlags,CPoint pt);
			void         OnNcMouseMove(UINT nFlags, CPoint point);
			void         OnNcMouseLeave(unsigned int wParam);

			//滚动条显示或者隐藏时发送该消息
			int          OnNcCalcSize(BOOL bCalcValidRects, LPARAM lParam);
			BOOL         OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
			void         OnTimer(char cTimerID);
			void         OnShowWindow(BOOL bShow, UINT nStatus);
			void         OnVScroll(UINT nSBCode, UINT nPos, int);
			void         OnHScroll(UINT nSBCode, UINT nPos,  int);
    protected:
			virtual int  GetScrollLineSize(BOOL bVertical);
			virtual BOOL OnScroll(BOOL bVertical,UINT uCode,int nPos);
			virtual void OnColorize(COLORREF cr);
			virtual void OnScaleChanged(int nScale);

			int          GetSbArrowSize();
			int          GetSbWidth();
			int          GetSbSlideLength(BOOL bVertical);
			CRect        GetSbSlideRectByPos(BOOL bVertical,int nPos);
			void         ScrollUpdate();
			virtual void OnScrollPosChanged();
			int          OnAttrScrollbarSkin(SStringA strValue,BOOL bLoading);

			IFont*                m_pFont;
			SCROLLINFO            m_siVer;
			SCROLLINFO            m_siHoz;
			CSkinScrollbar*       m_pSkinSb;

	private:	//修改为私有，派生类只能使用GetSbArrowSize(),GetSbWidth()获取
			CLayoutSize    m_nSbArrowSize;
			CLayoutSize    m_nSbWid;

	protected:
		CLayoutSize	   m_nSbLeft;   //滚动条距离左边距离
		CLayoutSize    m_nSbRight;  //滚动条距离边边距离
		CLayoutSize	   m_nSbTop;    //滚动条距离上边距离
		CLayoutSize    m_nSbBottom; //滚动条距离下边距离
        CPoint         m_ptDragSb;
        
        enum {
        DSB_NULL=0,
        DSB_VERT,
        DSB_HORZ,
        }               m_dragSb;

		PanelMouseState  m_MouseState;
		SBHITINFO        m_uCurHit;
		SBHITINFO        m_uOldHit;
        SBHITINFO       m_HitInfo;
        int             m_nDragPos;

        CRect           m_rcClient;
        int            m_wBarVisible;    //滚动条显示信息
        int            m_wBarEnable;    //滚动条可操作信息

        DWORD        m_dwUpdateTime;    //记录调用UpdateSWindow的时间
        DWORD        m_dwUpdateInterval;
        
		short		 m_zDelta;
        int          m_nScrollSpeed;
        KKUI_ATTRS_BEGIN()
            ATTR_CUSTOM("sbSkin",OnAttrScrollbarSkin)
			ATTR_LAYOUTSIZE("sbArrowSize", m_nSbArrowSize, FALSE)
			ATTR_LAYOUTSIZE("sbWid", m_nSbWid, TRUE)
            ATTR_INT("sbEnable", m_wBarEnable, TRUE)
            ATTR_UINT("updateInterval", m_dwUpdateInterval, FALSE)
            ATTR_UINT("scrollSpeed",m_nScrollSpeed,FALSE)

			ATTR_LAYOUTSIZE("sbLeft", m_nSbLeft, TRUE)
			ATTR_LAYOUTSIZE("sbRight", m_nSbRight, TRUE)
			ATTR_LAYOUTSIZE("sbTop", m_nSbTop, TRUE)
			ATTR_LAYOUTSIZE("sbBottom", m_nSbBottom, TRUE)
        KKUI_ATTRS_END()

        KKUI_MSG_MAP_BEGIN()
            MSG_UI_CREATE(OnCreate)
			MSG_UI_NCPAINT(OnNcPaint)
			MSG_UI_NCLBUTTONDOWN(OnNcLButtonDown)
			MSG_UI_NCLBUTTONUP(OnNcLButtonUp)
			MSG_UI_LBUTTONUP(OnLButtonUp)
		    MSG_UI_NCMOUSEMOVE(OnNcMouseMove)
			MSG_UI_NCMOUSELEAVE(OnNcMouseLeave)
			MSG_UI_NCCALCSIZE(OnNcCalcSize)
            MSG_UI_VSCROLL(OnVScroll)
            MSG_UI_HSCROLL(OnHScroll)
        KKUI_MSG_MAP_END(CUIWindow)
    };
}
#endif