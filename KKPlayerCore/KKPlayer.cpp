/**************************kkplayer*********************************************/
/*******************Copyright (c) Saint ******************/
/******************Author: Saint *********************/
/*******************Author e-mail: lssaint@163.com ******************/
/*******************Author qq: 784200679 ******************/
/*******************KKPlayer  WWW: http://www.70ic.com/KKplayer ********************************/
/*************************date：2015-6-25**********************************************/
//http://www.2cto.com/kf/201504/390386.html mediacodec aac 解码。
#include <stdlib.h> 
#include <stdio.h>
//#include "kk2dop.h"
#include "KKPlayer.h"
#include "KKInternal.h"
#include "rtmp/libRtmpPlugin.h"
#include "MD5/md5.h"
#include <string>
#define MaxTimeOutStr "50000000"
#define MaxTimeOut    45
static AVPacket flush_pkt;
static int decoder_reorder_pts = -1;
static int framedrop = -1;
static int fast = 0;
static int lowres = 0;
static int64_t sws_flags = SWS_BICUBIC;
static int av_sync_type =AV_SYNC_AUDIO_MASTER;//AV_SYNC_EXTERNAL_CLOCK;//AV_SYNC_AUDIO_MASTER;//AV_SYNC_VIDEO_MASTER;// AV_SYNC_AUDIO_MASTER;
double rdftspeed = 0.02;
#define PRINT_CODEC_INFO

#ifndef ERROR_INVALID_DATA 
#define ERROR_INVALID_DATA               13L
#endif

fpPrepareDecompress  KKPlayer::G_fpPrepareDecompress=0;
char**  KKCommandLineToArgv(const char* CmdLine,int* _argc);
//extern AVPixelFormat DstAVff;//=AV_PIX_FMT_YUV420P;//AV_PIX_FMT_BGRA;
//解码成BGRA格式
void KKPlayer::SetBGRA()
{
	m_DstAVff=AV_PIX_FMT_BGRA;
}
bool KKPlayer::GrabAvPicBGRA(void* buf,int len,int &w,int &h,bool keepscale)
{
	bool Ok=false;

	if(len<w*h*4)
		return false;

	
	if(m_pVideoInfo!=NULL&&m_pVideoInfo->pFormatCtx!=NULL&&m_pVideoInfo->video_st!=NULL&&buf!=NULL){
		   m_pVideoInfo->pictq.mutex->Lock();
		   SKK_Frame *vp =frame_queue_peek_last(&m_pVideoInfo->pictq);
		  
		   if(vp!=NULL&&vp->Bmp.data[0]!=NULL&&vp->width>0)
			{
				if(keepscale)
				{
				   h= vp->height*w/vp->width;
				}
				   int lxx=w*h*4;
				   if(lxx>len){
					   m_pVideoInfo->pictq.mutex->Unlock();
				     	return Ok;
				   }
				
				AVPixelFormat srcFF=m_pVideoInfo->DstAVff;
				SwsContext * imgctx = NULL;
				imgctx = sws_getCachedContext( imgctx ,
				   vp->width,  vp->height ,srcFF ,
				   w,       h,               AV_PIX_FMT_BGRA,                
				 SWS_FAST_BILINEAR,
				 NULL, NULL, NULL);

				if( imgctx  !=NULL){
					 AVPicture               Bmp;
					 avpicture_fill((AVPicture *)&Bmp, ( uint8_t *)buf,AV_PIX_FMT_BGRA, w,h);
					 sws_scale( imgctx ,vp->Bmp.data, vp->Bmp.linesize,0,vp->height,Bmp.data, Bmp.linesize);
					 Ok=true;
					 sws_freeContext( imgctx );
					 imgctx=NULL;
				}
				
			}
			m_pVideoInfo->pictq.mutex->Unlock();
	}
	return Ok;
///DstAVff
}
//如果定义安卓平台。
#ifdef Android_Plat
void* kk_jni_attach_env();
int kk_jni_detach_env();
void  KKPlayer::SetViewSurface(void* surface)
{
   m_pViewSurface=surface;
   LOGE_KK("ViewSurface= %d \n",(int)surface);  
}
bool KKPlayer::IsMediacodecSurfaceDisplay()
{
	return m_bSurfaceDisplay;
    
}
#endif
std::list<KKPluginInfo>  KKPlayer::KKPluginInfoList;
void KKPlayer::AddKKPluginInfo(KKPluginInfo& info)
{
     KKPluginInfoList.push_back(info);
}

void KKPlayer::RegisterInputFormat(AVInputFormat *format)
{
      av_register_input_format(format);
}
void KKPlayer::RegisterAvDecoder(AVCodec* decoder)
{
   avcodec_register(decoder);
}
void KKPlayer::SetStoreInfo(const char *strstoredir,int downspeedlimit,int upspeedlimit)
{
	std::list<KKPluginInfo>::iterator It=KKPluginInfoList.begin();
	for(;It!=KKPluginInfoList.end();++It)
	{
		if(It->KKSetStoreInfo)
		It->KKSetStoreInfo(strstoredir, downspeedlimit,upspeedlimit);
	}
}
std::list<KKPluginInfo>&   KKPlayer::GetKKPluginInfoList()
{
	return KKPluginInfoList;
}
void register_Kkv();

void AV_Log_callback(void *avcl, int level, const char *fmt,va_list vl)
{

}
const char *ppPixFmtName[AV_PIX_FMT_NB] = {NULL};
//初始化ffmpeg
void IniFF()
{
    static bool registerFF=true;
	if(registerFF)
	{
		
		ppPixFmtName[AV_PIX_FMT_VDPAU_H264] = "PIX_FMT_VDPAU_H264";
		ppPixFmtName[AV_PIX_FMT_VAAPI_IDCT] = "PIX_FMT_VAAPI_IDCT";
		ppPixFmtName[AV_PIX_FMT_VAAPI_VLD]  = "PIX_FMT_VAAPI_VLD";
		ppPixFmtName[AV_PIX_FMT_VAAPI_MOCO] = "PIX_FMT_VAAPI_MOCO";
		ppPixFmtName[AV_PIX_FMT_DXVA2_VLD]  = "PIX_FMT_DXVA2_VLD";
		ppPixFmtName[AV_PIX_FMT_YUYV422]    = "PIX_FMT_YUYV422";
		ppPixFmtName[AV_PIX_FMT_YUV420P]    = "PIX_FMT_YUV420P";


		av_log_set_callback(AV_Log_callback);
		LOGE_KK("Debug", "len:%d \n",strlen("我们"));
		/*std::string strPort="";
		const char *pp=strPort.c_str();
		char* const aaa=(char* const)pp;*/
		av_register_all();
		avfilter_register_all();
		avformat_network_init();
       // register_Kkv();
		
		registerFF=false;
		AddlibRtmpPluginInfo();
	}
	

#ifdef PRINT_CODEC_INFO
//#ifdef _DEBUG
//	AVInputFormat *ff=av_iformat_next(NULL);
//    while(ff!=NULL)
//	{
//		
//		LOGE_KK("--%s \n",ff->name);
//		ff=av_iformat_next(ff);
//    }
//#endif
	/*AVCodec *codec=av_codec_next(NULL);
    while(codec!=NULL)
	{
		
		LOGE_KK("%s \n",codec->name);
		codec=av_codec_next(codec);
    }*/
	int i=0;
	AVHWAccel *hwaccel=av_hwaccel_next(NULL);
	while(hwaccel!=NULL)
	{
		const char *aa=hwaccel->name;
		LOGE_KK("Debug", "%d,%s \n",i++,aa);
		hwaccel=av_hwaccel_next(hwaccel);
	}
#endif
	

	  
	 /*const  AVPixFmtDescriptor *frist=av_pix_fmt_desc_next(NULL);
     while(frist!=NULL)
	{
		
		LOGE_KK("pfm  %s \n",frist->name);
		frist=av_pix_fmt_desc_next(frist);
    }*/
	
}
KKPlayer::KKPlayer(IKKPlayUI* pPlayUI,IKKAudio* pSound):
m_pSound(pSound),
m_pPlayUI(pPlayUI),
m_ePlayerState(EPS_Ini)
,m_pAudioPicBuf(NULL)
,m_AudioPicBufLen(0)
,m_bRender(1)
,m_nNeedReset(0)
,m_CacheAvCounter(0)
,m_TotalTime(0)
,m_nStartTime(AV_NOPTS_VALUE)
,m_fCurTime(0.000)
,m_nInterCurTime(0)
,m_pVideoInfo(0)
,m_bOpen(false)
,m_nhasVideoAudio(0)
,m_DstAVff(AV_PIX_FMT_YUV420P)
,m_pKKPlayerGetUrl(NULL)
,m_nEStreamMediaType(ESMT_No)
,m_nOverNotify(0)
,m_nPlayAvModel(0)
,m_nAvEndTime(0)
,m_nUniformLoad(0)
,m_pLoadFrame(0)
,m_nUnUsedFrame(1)
{
	//m_pVideoInfo = (SKK_VideoState*)KK_Malloc_(sizeof(SKK_VideoState));
	//memset(m_pVideoInfo,0,sizeof(SKK_VideoState));
	
	m_ReadThreadInfo.ThOver        = true;
	m_VideoRefreshthreadInfo.ThOver=true;
	m_AudioCallthreadInfo.ThOver   =true;

	IniFF();
}
KKPlayer::~KKPlayer(void)
{
	if(m_pLoadFrame)
	   av_frame_free(&m_pLoadFrame);
}
void FreeKKIo(SKK_VideoState *kkAV);
void WillCloseKKIo(AVIOContext *io);
void KKPlayer::CloseMedia()
{
	
	LOGE_KK("Debug", "m_pVideoInfo:%d \n",m_pVideoInfo);
    //检查环境变量
	{
			CKKGurd gd(m_PlayerLock);
			LOGE_KK("Debug", "11 \n");
			if(m_pVideoInfo==NULL)
			{
			   CKKGurd gd(m_StateLock);
			   m_ePlayerState= EPS_Ini;
			   m_bOpen=false;
			   return;
			}else if(m_pVideoInfo!=NULL)
			{
				m_pVideoInfo->abort_request=1;
			}
		
			//avformat_open_input 可能带来阻塞，调用这个来中断阻塞
			if(m_pVideoInfo->pFormatCtx!=NULL&&m_pVideoInfo->ioflags==AVFMT_FLAG_CUSTOM_IO)
			{
				WillCloseKKIo(m_pVideoInfo->pFormatCtx->pb);
			}
	

			//检查读取线程和渲染线程是否关闭
			while(1)
			{
				if(m_ReadThreadInfo.ThOver==true&&m_VideoRefreshthreadInfo.ThOver==true&&m_AudioCallthreadInfo.ThOver==true)
				{
					break;
				}
				/*LOGE_KK("thread Over1 m_ReadThreadInfo%d,m_VideoRefreshthreadInfo%d \n",m_ReadThreadInfo.ThOver
					,m_VideoRefreshthreadInfo.ThOver
					);*/
				Sleep(10);
			}
			
			if(m_pSound!=NULL)
			{
				  m_pSound->Stop();
				  m_pSound->SetAudioCallBack(0,0);
			}
			
			#ifdef WIN32_KK
				if(m_ReadThreadInfo.ThreadHandel)
					CloseHandle(m_ReadThreadInfo.ThreadHandel);
				m_ReadThreadInfo.ThreadHandel = 0;

				if(m_VideoRefreshthreadInfo.ThreadHandel)
				   CloseHandle(m_VideoRefreshthreadInfo.ThreadHandel);
				m_VideoRefreshthreadInfo.ThreadHandel = 0;

				if(m_AudioCallthreadInfo.ThreadHandel)
				   CloseHandle(m_AudioCallthreadInfo.ThreadHandel);
				m_AudioCallthreadInfo.ThreadHandel = 0;

				
            #else
			        if(m_ReadThreadInfo.Tid_task)
			           pthread_join(m_ReadThreadInfo.Tid_task,0);
					m_ReadThreadInfo.Tid_task = 0; 

					if(m_VideoRefreshthreadInfo.Tid_task)
					   pthread_join(m_VideoRefreshthreadInfo.Tid_task,0);
					m_VideoRefreshthreadInfo.Tid_task = 0;

					if(m_AudioCallthreadInfo.Tid_task)
					   pthread_join(m_AudioCallthreadInfo.Tid_task,0);
					m_AudioCallthreadInfo.Tid_task = 0;

					

			#endif	
			OnResetQue();
			
           
		    //检查解码携程是否关闭	
			while(1)
			{
				if(m_pVideoInfo->viddec.decoder_tid.ThOver==true&&m_pVideoInfo->auddec.decoder_tid.ThOver==true&&m_pVideoInfo->subdec.decoder_tid.ThOver==true)
				{
					break;
				}
				/*LOGE_KK("thread Over2 viddec%d,auddec%d,subdec%d \n",m_pVideoInfo->viddec.decoder_tid.ThOver
					,m_pVideoInfo->auddec.decoder_tid.ThOver
					,m_pVideoInfo->subdec.decoder_tid.ThOver
					);*/
				if(!m_pVideoInfo->viddec.decoder_tid.ThOver){
					m_pVideoInfo->videoq.m_pWaitCond->SetCond();
					m_pVideoInfo->pictq.m_pWaitCond->SetCond();
				}
				Sleep(10);
			}

			//LOGE_KK("thread Over 2 \n");
			
		#ifdef WIN32_KK
			
			//关闭相关解码线程

			if(m_pVideoInfo->viddec.decoder_tid.ThreadHandel)
			    ::CloseHandle(m_pVideoInfo->viddec.decoder_tid.ThreadHandel);
			

			if(m_pVideoInfo->auddec.decoder_tid.ThreadHandel)
			   ::CloseHandle(m_pVideoInfo->auddec.decoder_tid.ThreadHandel);
			
			if(m_pVideoInfo->subdec.decoder_tid.ThreadHandel!=0)
			   ::CloseHandle(m_pVideoInfo->subdec.decoder_tid.ThreadHandel);
		#else
		        
			if(m_pVideoInfo->viddec.decoder_tid.Tid_task!=0)
				pthread_join(m_pVideoInfo->viddec.decoder_tid.Tid_task,0);
			
			if(m_pVideoInfo->auddec.decoder_tid.Tid_task!=0)
				pthread_kill(m_pVideoInfo->auddec.decoder_tid.Tid_task,0);
			
			if(m_pVideoInfo->subdec.decoder_tid.Tid_task!=0)
				 pthread_kill(m_pVideoInfo->subdec.decoder_tid.Tid_task,0);
		#endif	
			
			
			 
			av_frame_unref(m_pLoadFrame);
			m_nUnUsedFrame = 1;
		
			//LOGE_KK("PacketQueuefree1 \n");
			PacketQueuefree();
			//LOGE_KK("PacketQueuefree OK \n");

			
			if(m_pVideoInfo->swr_ctx!=NULL)
			{
			   swr_free(&m_pVideoInfo->swr_ctx);
			   m_pVideoInfo->swr_ctx=NULL;
			   LOGE_KK("Debug", "swr_free OK \n");
			}

			if(m_pVideoInfo->img_convert_ctx!=NULL)
			{
				sws_freeContext(m_pVideoInfo->img_convert_ctx);
				m_pVideoInfo->img_convert_ctx=NULL;
				LOGE_KK("Debug", "m_pVideoInfo->img_convert_ctx \n");
			}
			


			if(m_pVideoInfo->sub_convert_ctx!=NULL)
			{
				sws_freeContext(m_pVideoInfo->sub_convert_ctx);
				m_pVideoInfo->sub_convert_ctx=NULL;
				LOGE_KK("Debug", "sub_convert_ctx \n");
			}
			


			if(m_pVideoInfo->InAudioSrc!=NULL)
			{
				avfilter_free(m_pVideoInfo->InAudioSrc);
				LOGE_KK("Debug", "InAudioSrc \n");
			}
			if(m_pVideoInfo->OutAudioSink!=NULL)
			{
				avfilter_free(m_pVideoInfo->OutAudioSink);
				LOGE_KK("Debug", "OutAudioSink \n");
			}

			if(m_pVideoInfo->AudioGraph!=NULL)
			{
				avfilter_graph_free(&m_pVideoInfo->AudioGraph);
				LOGE_KK("Debug", "m_pVideoInfo->AudioGraph \n");
			}
			
			if(m_pVideoInfo->auddec.avctx!=NULL){
				 LOGE_KK("Debug", "avcodec_close(m_pVideoInfo->auddec.avctx); OK \n");
				 //avcodec_close(m_pVideoInfo->auddec.avctx);
				 avcodec_free_context(&m_pVideoInfo->auddec.avctx);
				 if (m_pVideoInfo->audio_buf1) {
					 av_freep(&m_pVideoInfo->audio_buf1);
				 }
				 m_pVideoInfo->audio_buf = 0;
			}
			
			if(m_pVideoInfo->subdec.avctx!=NULL)
			{
				 LOGE_KK("Debug", "avcodec_close(m_pVideoInfo->subdec.avctx); OK \n");
			   //avcodec_close(m_pVideoInfo->subdec.avctx);
			   avcodec_free_context(&m_pVideoInfo->subdec.avctx);
			}

			if(m_pVideoInfo->viddec.avctx!=NULL)
			{
			   LOGE_KK("Debug", "avcodec_close(m_pVideoInfo->viddec.avctx); \n");
			  // avcodec_close(m_pVideoInfo->viddec.avctx);
			   avcodec_free_context(&m_pVideoInfo->viddec.avctx);
			   LOGE_KK("Debug", "avcodec_close(avcodec_free_context(&m_pVideoInfo->viddec.avctx) OK \n");
			}

			if(m_pVideoInfo->pFormatCtx!=NULL&&m_pVideoInfo->ioflags==AVFMT_FLAG_CUSTOM_IO)
			{
				FreeKKIo(m_pVideoInfo);
				m_pVideoInfo->pFormatCtx->pb=NULL;
			}

			LOGE_KK("Debug", "m_pVideoInfo->pFormatCtx %d \n",m_pVideoInfo->pFormatCtx);
			if(m_pVideoInfo->pFormatCtx!=NULL){
				avformat_close_input(&m_pVideoInfo->pFormatCtx);
				m_pVideoInfo->pFormatCtx=NULL;
			}
			
		   
			if(m_pVideoInfo->pKKPluginInfo!=NULL){
			   LOGE_KK("Debug", "m_pVideoInfo->pKKPluginInfo\n");
			   KK_Free_(m_pVideoInfo->pKKPluginInfo);
			}

			av_packet_unref(m_pVideoInfo->pflush_pkt);
			KK_Free_(m_pVideoInfo->pflush_pkt);

			

			m_nStartTime  = AV_NOPTS_VALUE;
			m_fCurTime   = 0.00;
			m_TotalTime = 0;

			if(m_pAudioPicBuf!=NULL){
				m_AudioPicBufLen=0;
				KK_Free_(m_pAudioPicBuf);
				m_pAudioPicBuf=NULL;
			}


            KK_Free_(m_pVideoInfo);
			m_pVideoInfo=NULL;
			m_nhasVideoAudio=0;
			m_bOpen=false;
			{
				CKKGurd gd(m_StateLock);
				m_ePlayerState = EPS_Ini;
			}
			#ifdef Android_Plat
				m_pVideoRefreshJNIEnv=NULL;
				 m_bSurfaceDisplay=false;
				 m_pViewSurface=0;
			#endif
			LOGE_KK("Debug", "KKplay Over\n");
	}
}
void KKPlayer::OnResetQue()
{
	if (m_pVideoInfo) {
		m_pVideoInfo->videoq.m_pWaitCond->SetCond();
		m_pVideoInfo->audioq.m_pWaitCond->SetCond();
		m_pVideoInfo->subtitleq.m_pWaitCond->SetCond();

		m_pVideoInfo->pictq.m_pWaitCond->SetCond();
		m_pVideoInfo->sampq.m_pWaitCond->SetCond();
		m_pVideoInfo->subpq.m_pWaitCond->SetCond();
	}
}
//获取媒体播放的信息
bool KKPlayer::GetMediaInfo(MEDIA_INFO &info)
{
	bool ok=false;
	if(m_PlayerLock.TryLock()){
		if(m_bOpen&&m_pVideoInfo!=NULL&& m_ePlayerState==EPS_ReadingOk&&!m_pVideoInfo->abort_request){	
						
						    info.Open=m_bOpen;
							info.fCurTime=m_fCurTime;
							if(m_pVideoInfo->pFormatCtx!=NULL){
								            info.SegId=m_pVideoInfo->cursegid;
											info.AvFile=(const char*)m_pVideoInfo->filename;
											info.TotalTime=(m_pVideoInfo->pFormatCtx->duration/1000/1000);
											
											
											info.CacheInfo.AudioSize=m_AVCacheInfo.AudioSize;
											info.CacheInfo.VideoSize=m_AVCacheInfo.VideoSize;
											info.CacheInfo.MaxTime=m_AVCacheInfo.MaxTime;
										
											info.FileSize=m_pVideoInfo->fileSize;
										
											snprintf(info.AVRes,1024,"%dx%d",m_pVideoInfo->viddec_width,m_pVideoInfo->viddec_height);
											char infostr[1024]="";
											if(m_pVideoInfo->viddec.avctx!=NULL){

												memset(&info.videoinfo.codecname,0,32);
												strcpy(info.videoinfo.codecname, m_pVideoInfo->viddec.avctx->codec->name);
												info.videoinfo.bitrate= m_pVideoInfo->video_st->codecpar->bit_rate;
												info.videoinfo.framerate= m_pVideoInfo->viddec.avctx->framerate.num;
												
												info.serial=m_pVideoInfo->SeekVideoSerial;
												info.serial1=m_pVideoInfo->CurVideoSerial;
												
												if(m_pVideoInfo->auddec.avctx!=NULL)
													m_fCurTime=abs(m_pVideoInfo->vidclk.pts-m_pVideoInfo->audclk.pts)>60 ?  m_pVideoInfo->vidclk.pts:m_pVideoInfo->audclk.pts; //vidclk.pts;
												else{
													m_fCurTime=m_pVideoInfo->vidclk.pts;
												}
												info.fCurTime=m_fCurTime;
											}

											if(m_pVideoInfo->auddec.avctx!=NULL)
											{
												if(m_pVideoInfo->viddec.avctx==NULL){
													info.serial1=m_pVideoInfo->CurAudioSerial;
													info.serial=m_pVideoInfo->SeekAudioSerial;
												}/**/
												strcpy(info.audioinfo.codecname,m_pVideoInfo->auddec.avctx->codec->name);
												info.audioinfo.bitrate= m_pVideoInfo->auddec.avctx->bit_rate;
												info.audioinfo.sample_rate=m_pVideoInfo->auddec.avctx->sample_rate;
												info.audioinfo.channels=m_pVideoInfo->auddec.avctx->channels;
											}
											if(m_pVideoInfo->seek_req!=0||m_nSeekTime>-1)
											{
											    info.serial=-1;
											}

											
							}
							m_AVPlayInfo=info;
							ok=true;
				
		      }
		m_PlayerLock.Unlock();/**/
	}else{
	    info=m_AVPlayInfo;
	}
	
	///自定义Io
	if(m_pVideoInfo!=NULL&&m_pVideoInfo->pFormatCtx!=NULL&&m_pVideoInfo->ioflags == AVFMT_FLAG_CUSTOM_IO){
	     if(m_pVideoInfo->pKKPluginInfo!=NULL&&m_pVideoInfo->pKKPluginInfo->KKDownAVFileSpeedInfo!=NULL){
			 char infostr[1024]="";
			 m_pVideoInfo->pKKPluginInfo->KKDownAVFileSpeedInfo(m_pVideoInfo->filename,infostr,1024);
			 strcpy(info.SpeedInfo,infostr);
		 }
	}/**/
	return ok;
}


void KKPlayer::SetWindowHwnd(HWND hwnd)
{
	m_hwnd=hwnd;
	//m_pSound->SetWindowHAND((int)m_hwnd);
}

void      KKPlayer:: SetPlayAvModel(int model)
{
   m_nPlayAvModel = model;
}
///设置URL替换函数
void     KKPlayer::SetKKPlayerGetUrl(fpKKPlayerGetUrl pKKPlayerGetUrl)
{
    m_pKKPlayerGetUrl=pKKPlayerGetUrl;
}

//读取线程
unsigned __stdcall  KKPlayer::ReadAV_thread(LPVOID lpParameter)
{
	KKPlayer *pPlayer=(KKPlayer *  )lpParameter;
	pPlayer->m_ReadThreadInfo.ThOver=false;
	pPlayer->OnReadAV();
	
	pPlayer->m_ReadThreadInfo.ThOver=true;
    pPlayer->m_ReadThreadInfo.Addr=0;
#ifdef WIN32
	 _endthreadex(0);
#endif
	return 0;
}

#ifdef WIN32_KK
int index=0;
SYSTEMTIME Time_tToSystemTime(time_t t)
{
	tm temptm = *localtime(&t);
	SYSTEMTIME st = {1900 + temptm.tm_year, 
		1 + temptm.tm_mon, 
		temptm.tm_wday, 
		temptm.tm_mday, 
		temptm.tm_hour, 
		temptm.tm_min, 
		temptm.tm_sec, 
		0};
	return st;
}
#endif
//http://120.25.236.44:9999/1/test.m3u8
//rtmp://117.135.131.98/771/003 live=1
void KKPlayer::video_image_refresh(SKK_VideoState *is)
{
	
	
    double time=0,duration=0;
	if(!is->reneed_paused)
	{
		if(is->audio_st)
			m_fCurTime=is->audio_clock;
		else if(is->video_st)
			m_fCurTime=is->vidclk.pts;
		else
			m_fCurTime=get_master_clock(is);
	}
	if (is->video_st==NULL&&is->audio_st) {
		time = av_gettime_relative() / 1000000.0;
		if (is->force_refresh || is->last_vis_time + rdftspeed < time)
		{
			
			is->last_vis_time = time;
		}
		is->remaining_time = FFMIN(is->remaining_time, is->last_vis_time + rdftspeed - time);
		is->force_refresh=1;
	}

	
	if(is->video_st)
    {
retry:
			//没有数据
			if (frame_queue_nb_remaining(&is->pictq) <= 0)
			{
			   goto display;
			}
			

			SKK_Frame *vp, *lastvp ;
			
		
			//获取上一次的读取位置
			lastvp = frame_queue_peek_last(&is->pictq);
			/**********获取包位置**********/
			vp = frame_queue_peek(&is->pictq);


			is->video_clock=vp->pts;
			if(is->realtime&&is->audio_st==NULL&&!is->abort_request){
				if(m_pVideoInfo->nRealtimeDelay>0)
				{
				
					m_pVideoInfo->nRealtimeDelay-=(vp->pts-lastvp->pts);
					//is->nMinRealtimeDelay=3;
					if(m_pVideoInfo->nRealtimeDelay>is->nMaxRealtimeDelay)
					{
						frame_queue_next(&is->pictq,true);
						is->nRealtimeDelayCount++;
						goto retry;
						//goto retry:
					}else
						is->nRealtimeDelayCount=0;
				}
			}
			

			
				/*******时间**********/
				if (lastvp->serial != vp->serial)
				{
					is->frame_timer = av_gettime_relative() / 1000000.0;
				}

				if (is->paused)
					goto display;
				
				//is->frame_timer += delay;
				/******上一次更新和这一次时间的差值。图片之间差值******/
				is->last_duration = vp_duration(is, lastvp, vp);/******pts-pts********/
                is->delay = compute_target_delay(is->last_duration, is);


				time= av_gettime_relative()/1000000.0;
				if (time < is->frame_timer + is->delay) {
					double llxxxx=is->frame_timer + is->delay - time;
					is->remaining_time = FFMIN(llxxxx, is->remaining_time);
					goto display;
				}
	
				is->frame_timer += is->delay;
				if (is->delay > 0 && time - is->frame_timer > AV_SYNC_THRESHOLD_MAX)
					is->frame_timer = time;


				if(!isNAN(vp->pts))
				{
					update_video_pts(is, vp->pts, vp->pos, vp->serial);
				}
				if (frame_queue_nb_remaining(&is->pictq) > 1)
				{
					SKK_Frame *nextvp = frame_queue_peek_next(&is->pictq);
					duration = vp_duration(is, vp, nextvp);
					if((is->redisplay ||(get_master_sync_type(is) != AV_SYNC_VIDEO_MASTER)) && time > is->frame_timer + duration)
					{
						is->frame_drops_late++;
						frame_queue_next(&is->pictq,true);
						is->redisplay=0;
						goto retry;
					}
				}
				
				if (vp->buffer)
				{
					    int total=(m_pVideoInfo->pFormatCtx->duration/1000/1000);
						if(m_nStartTime==AV_NOPTS_VALUE)
						{
						  m_nStartTime=vp->pts;
						}
				}

				if (is->subtitle_st) {
					SKK_Frame *sp=0, *sp2=0;
                    while (frame_queue_nb_remaining(&is->subpq) > 0) {
                        sp = frame_queue_peek(&is->subpq);

                        if (frame_queue_nb_remaining(&is->subpq) > 1)
                            sp2 = frame_queue_peek_next(&is->subpq);
                        else
                            sp2 = NULL;

                        if (sp->serial != is->subtitleq.serial
                                || (is->vidclk.pts > (sp->pts + ((float) sp->sub.end_display_time / 1000)))
                                || (sp2 && is->vidclk.pts > (sp2->pts + ((float) sp2->sub.start_display_time / 1000))))
                        {
                            if (sp->uploaded)
							{
                               /* int i;
                                for (i = 0; i < sp->sub.num_rects; i++)
								{
                                    AVSubtitleRect *sub_rect = sp->sub.rects[i];
                                    uint8_t *pixels;
                                    int pitch, j;
                                    }
                                }*/
                            }
                            frame_queue_next(&is->subpq,true);
                        } else {
                            break;
                        }
                    }
                }

				frame_queue_next(&is->pictq,true);
				is->force_refresh=1;
	}
display:
	if(is->force_refresh&&m_pPlayUI!=NULL)
	{		
		int xx= av_gettime()/1000;
		m_pPlayUI->AVRender();
		
		xx =  av_gettime()/1000-xx;
		int lx = is->remaining_time*1000;
		if(xx>lx)
		{
		  is->remaining_time=0.0;
		}
		/*char temp[256]="";
		sprintf(temp,"d::%d %.3f\n",xx,is->remaining_time);
		::OutputDebugStringA(temp);*/
	}
	if(is->subtitle_st!=NULL)
	{

	}
	is->force_refresh=0;
}

static inline int compute_mod(int a, int b)
{
    return a < 0 ? a%b + b : a%b;
}
///渲染音频波形
void KKPlayer::video_audio_display(IkkRender *pRender,SKK_VideoState *s)
{
//	return ;
	int i, i_start, x, y1, y, ys, delay, n, nb_display_channels;
    int ch, channels, h, h2;
	unsigned int bgcolor, fgcolor;
    int64_t time_diff;
    int rdft_bits, nb_freq;
	int height=300;
	int width=700;
	if(m_pAudioPicBuf==NULL){
	    m_AudioPicBufLen=avpicture_get_size(AV_PIX_FMT_BGRA,width, height);
	    m_pAudioPicBuf=(uint8_t *)KK_Malloc_(m_AudioPicBufLen);
	}
    for (rdft_bits = 1; (1 << rdft_bits) < 2 * height; rdft_bits++)
        ;
    nb_freq = 1 << (rdft_bits - 1);

    channels = s->audio_tgt.channels;
    nb_display_channels = channels;
    if (!s->paused) {
		int data_used= s->show_mode ==  SKK_VideoState::SHOW_MODE_WAVES ? width : (2*nb_freq);
        n = 2 * channels;
        delay = s->audio_write_buf_size;
        delay /= n;

        /* to be more precise, we take into account the time spent since
           the last buffer computation */
        if (s->audio_callback_time) {
            time_diff = av_gettime_relative() - s->audio_callback_time;
            delay -= (time_diff * s->audio_tgt.freq) / 1000000;
        }

        delay += 2 * data_used;
        if (delay < data_used)
            delay = data_used;

        i_start= x = compute_mod(s->sample_array_index - delay * channels, SAMPLE_ARRAY_SIZE);
        if (1||s->show_mode == SKK_VideoState::SHOW_MODE_WAVES) {
            h = INT_MIN;
            for (i = 0; i < 1000; i += channels) {
                int idx = (SAMPLE_ARRAY_SIZE + x - i) % SAMPLE_ARRAY_SIZE;
                int a = s->sample_array[idx];
                int b = s->sample_array[(idx + 4 * channels) % SAMPLE_ARRAY_SIZE];
                int c = s->sample_array[(idx + 5 * channels) % SAMPLE_ARRAY_SIZE];
                int d = s->sample_array[(idx + 9 * channels) % SAMPLE_ARRAY_SIZE];
                int score = a - d;
                if (h < score && (b ^ c) < 0) {
                    h = score;
                    i_start = idx;
                }
            }
        }

        s->last_i_start = i_start;
    } else {
        i_start = s->last_i_start;
    }

	if(pRender!=NULL)
	{
				kkBitmap img;
				img.pixels=m_pAudioPicBuf;
				img.width=width;
				img.height=height;
				kkRect rt={0,0,img.width,img.height};
				bgcolor = kkRGB(0,0,0);
				//pRender->FillRect(img,rt,bgcolor);
		        fgcolor =kkRGB(0xff, 0xff, 0xff);
				
               //if(0)
				{
				   nb_display_channels= FFMIN(nb_display_channels, 2);
					if (rdft_bits != s->rdft_bits) {
						av_rdft_end(s->rdft);
						av_free(s->rdft_data);
						s->rdft = av_rdft_init(rdft_bits, DFT_R2C);
						s->rdft_bits = rdft_bits;
						s->rdft_data =(FFTSample *) av_malloc_array(nb_freq, 4 *sizeof(*s->rdft_data));
					}
					if (!s->rdft || !s->rdft_data){
#ifdef _DEBUG
						av_log(NULL, AV_LOG_ERROR, "Failed to allocate buffers for RDFT, switching to waves display\n");
#endif
						//s->show_mode = SHOW_MODE_WAVES;
					} else {
						FFTSample *data[2];
						for (ch = 0; ch < nb_display_channels; ch++) {
							data[ch] = s->rdft_data + 2 * nb_freq * ch;
							i = i_start + ch;
							for (x = 0; x < 2 * nb_freq; x++) {
								double w = (x-nb_freq) * (1.0 / nb_freq);
								data[ch][x] = s->sample_array[i] * (1.0 - w * w);
								i += channels;
								if (i >= SAMPLE_ARRAY_SIZE)
									i -= SAMPLE_ARRAY_SIZE;
							}
							av_rdft_calc(s->rdft, data[ch]);
						}
						/* Least efficient way to do this, we should of course
						 * directly access it but it is more than fast enough. */
						for (y = 0; y < height; y++) {
							double w = 1 / sqrt((double)nb_freq);
							int a = sqrt(w * hypot(data[0][2 * y + 0], data[0][2 * y + 1]));
							int b = (nb_display_channels == 2 ) ? sqrt(w * hypot(data[1][2 * y + 0], data[1][2 * y + 1]))
																: a;
							a = FFMIN(a, 255);
							b = FFMIN(b, 255);
							fgcolor =kkRGB( a, b, (a + b) / 2);
							kkRect rt2={s->xpos,height-y, s->xpos+1,height-y+ 1};
						    pRender->FillRect(img,rt2, fgcolor);
						}
					}
				}


                if(s->img_convert_ctx ==NULL)
				s->img_convert_ctx = sws_getCachedContext(s->img_convert_ctx,width, height ,AV_PIX_FMT_BGRA,
					            width, height,               s->DstAVff,                
			                    SWS_FAST_BILINEAR,
			                    NULL, NULL, NULL);
				 if (s->img_convert_ctx == NULL) 
				 {
					 /*if(s->bTraceAV)
					 fprintf(stderr, "Cannot initialize the conversion context\n");*/
					 assert(0);
					 
				 }
			     AVPicture  InBmp; 
				 avpicture_fill((AVPicture *)&InBmp,(uint8_t *) m_pAudioPicBuf,AV_PIX_FMT_BGRA, width,height);

				 AVPicture  OutBmp; 
				 int numBytes=avpicture_get_size(s->DstAVff,width,height); //pFrame->width,pFrame->height
		         static   uint8_t * OutBmpbuffer=(uint8_t *)KK_Malloc_(numBytes)+100;
                 avpicture_fill((AVPicture *)&OutBmp, OutBmpbuffer,s->DstAVff, width,height);

			      sws_scale(s->img_convert_ctx, InBmp.data, InBmp.linesize,0,height,
					 OutBmp.data,OutBmp.linesize);
				  ///pRender->render((char*) OutBmpbuffer,width,height,width,false);

                  KK_Free_(OutBmpbuffer);

				  if (!s->paused)
							s->xpos++;
				  if (s->xpos >= width)
							s->xpos= 0;
		         			
	}
	
}
int KKPlayer::GetPktSerial()
{
	return m_PktSerial;
}
short KKPlayer::GetSegId()
{
	m_PlayerLock.Lock();
	if(m_pVideoInfo!=NULL)
	{
		m_PlayerLock.Unlock();
		return m_pVideoInfo->segid;
	}
	m_PlayerLock.Unlock();
	return -1;

}
short KKPlayer::GetCurSegId()
{
	m_PlayerLock.Lock();
	if(m_pVideoInfo!=NULL)
	{
		m_PlayerLock.Unlock();
	    return m_pVideoInfo->cursegid;
	}
	m_PlayerLock.Unlock();
	return -1;
}
void   KKPlayer::EnbleUniformLoad()
{
	m_nUniformLoad =1;
}
double KKPlayer::OnVideoRefresh()
{
	{
		CKKGurd gd(m_StateLock);
		if(m_ePlayerState != EPS_ReadingOk)
			 return REFRESH_RATE;
	}
	if(!m_pVideoInfo&&(m_pVideoInfo->IsReady==0||m_pVideoInfo->paused))
	{
		return REFRESH_RATE;
	}

	if (m_pVideoInfo->remaining_time > 0.0){
				int64_t ll=(int64_t)(m_pVideoInfo->remaining_time* 1000000.0);
				av_usleep(ll);
	}
	m_pVideoInfo->remaining_time = REFRESH_RATE;
	video_image_refresh(m_pVideoInfo); 
	///延迟分析
	AvDelayParser();
	return m_pVideoInfo->remaining_time;
}
int   KKPlayer::GetIsReady()
{
	if(m_pVideoInfo!=NULL)
	    return m_pVideoInfo->IsReady;
	return -1;
}
double  KKPlayer::OnLoadVideo(IkkRender *pRender)
{
	m_dRemainingTime = 0.01;
	if (!m_nUnUsedFrame&&m_pLoadFrame)
	{
		int Ok = 0;
		kkAVPicInfo picinfo;
		m_PlayerLock.Lock();

		if (m_bOpen&&m_pPlayUI != NULL&&m_pVideoInfo != NULL&&m_ePlayerState != EPS_Ini)
		{  
			Ok = 1;
			memcpy(picinfo.data, m_pLoadFrame->data, sizeof(m_pLoadFrame->data));
			memcpy(picinfo.linesize, m_pLoadFrame->linesize, sizeof(m_pLoadFrame->linesize));
			picinfo.width = m_pLoadFrame->width;
			picinfo.height = m_pLoadFrame->height;
			picinfo.picformat = m_pLoadFrame->format;
		
		}
		m_PlayerLock.Unlock();

		if (Ok)
		{
			int xx = av_gettime()/1000;
			pRender->render(&picinfo, 1);
			xx = av_gettime()/1000 - xx;
			int lx = m_dRemainingTime * 1000;
			if (xx>lx)
			{
				m_dRemainingTime = 0.0;
			}
			
		}

		m_PlayerLock.Lock();
		av_frame_unref(m_pLoadFrame);
		m_nUnUsedFrame = 1;
		m_PlayerLock.Unlock();
	}
	return m_dRemainingTime;
}
void   KKPlayer::RenderImage(IkkRender *pRender,bool Force)
{
	 SKK_Frame *vp;
	 if(!m_bRender)
		 return;
	 if(m_PlayerLock.TryLock())
	 {
		 if(m_bOpen&&m_pPlayUI!=NULL&&m_pVideoInfo!=NULL&&m_ePlayerState!=EPS_Ini)
		 {			
			if(m_pVideoInfo->IsReady != 0&&m_pVideoInfo->video_st!=NULL)
			{
				m_pVideoInfo->pictq.mutex->Lock();
				vp =frame_queue_peek_last(&m_pVideoInfo->pictq);  
				m_lstPts=vp->pts;
				if(vp&&vp->Bmp.data[0]!=NULL&&(m_lstPts!=vp->pts||Force)||vp->picformat==AV_PIX_FMT_MEDIACODEC)
				{

					if(vp->picformat==(int)AV_PIX_FMT_MEDIACODEC){
														   
						kkAVPicInfo picinfo;
						picinfo.width=vp->width;
						picinfo.height=vp->height;
						picinfo.picformat=vp->picformat;
						pRender->render(&picinfo, Force);
					}else if(!vp->uploaded&&vp->picformat!=(int)AV_PIX_FMT_DXVA2_VLD)
					{
						//使用统一的线程加载数据
						if (m_nUniformLoad) {
							if (!m_pLoadFrame)
								m_pLoadFrame = av_frame_alloc();
							if (m_nUnUsedFrame) {
								av_frame_move_ref( m_pLoadFrame,vp->frame);
								m_dRemainingTime = m_pVideoInfo->remaining_time;
								m_nUnUsedFrame = 0;
							}
							    
						}else {
							kkAVPicInfo picinfo;
							memcpy(picinfo.data, vp->Bmp.data, sizeof(vp->Bmp.data));
							memcpy(picinfo.linesize, vp->Bmp.linesize, sizeof(vp->Bmp.linesize));
							picinfo.width = vp->width;
							picinfo.height = vp->height;
							picinfo.picformat = vp->picformat;
							pRender->render(&picinfo, Force);
						}
					}
				}
				if(vp->serial==m_pVideoInfo->SeekVideoSerial&&m_pVideoInfo->reneed_paused==1){
					m_pVideoInfo->reneed_paused=2;
				}
				m_pVideoInfo->pictq.mutex->Unlock();
			}else if(m_pVideoInfo->audio_st!=NULL&&m_pVideoInfo->iformat!=NULL){
				/*  #ifdef  _WINDOWS
						video_audio_display(pRender,m_pVideoInfo);
				    #endif*/
			}else if(m_pVideoInfo->IsReady==0){
					pRender->render(NULL,true);
			}
									
									  
		}
	    m_PlayerLock.Unlock();	
	 }
}


#ifdef WIN32_KK
void  KKPlayer::OnDrawImageByDc(HDC memdc)
{
	//return;
	SKK_Frame *vp;
	if(m_pVideoInfo->IsReady==0)
		return;
	m_pVideoInfo->pictq.mutex->Lock();
	/**********获取包位置**********/
	vp = frame_queue_peek(&m_pVideoInfo->pictq);
	//VideoDisplay(vp->buffer,m_pVideoInfo->width,vp->height,&memdc,m_pVideoInfo->last_duration,vp->pts,vp->duration,vp->pos,m_pVideoInfo->delay);
	m_pVideoInfo->pictq.mutex->Unlock();
}
void KKPlayer::VideoDisplay(void *buf,int w,int h,void *usadata,double last_duration,double pts,double duration,int64_t pos,double diff)
{
	HDC dc=*(HDC*)usadata;
	BITMAPINFOHEADER header;
	header.biSize = sizeof(BITMAPINFOHEADER);

	header.biWidth = w;
	header.biHeight = h*(-1);
	header.biBitCount = 32;
	header.biCompression = 0;
	header.biSizeImage = 0;
	header.biClrImportant = 0;
	header.biClrUsed = 0;
	header.biXPelsPerMeter = 0;
	header.biYPelsPerMeter = 0;
	header.biPlanes = 1;

	RECT rt;
	::GetClientRect(m_hwnd,&rt);
	int www=rt.right-rt.left;
	int hhh=rt.bottom-rt.top;

	//拷贝图像
	StretchDIBits(dc, 0,   0, 
		www,   hhh, 
		0,   0, 
		w,   h, 
		buf, (BITMAPINFO*)&header,
		DIB_RGB_COLORS, SRCCOPY);/**/

	{			
		//速率
		char t[256]="";
		sprintf(t, "VPdur:%f",duration);
		int w2=0;
		RECT rt2={w2,10,w2+150,30};
		::SetTextColor(dc,RGB(255,255,255));
		::DrawTextA(dc,t,strlen(t),&rt2,DT_CENTER);

		//PTS
		memset(t,0,256);
		sprintf(t, "VPpts:%f",pts);
		rt2.top=30;
		rt2.bottom=60;
		::SetTextColor(dc,RGB(255,255,255));
		::DrawTextA(dc,t,strlen(t),&rt2,DT_CENTER);

		//pos
		memset(t,0,256);
		sprintf(t, "pos:%ld",pos);
		rt2.top=60;
		rt2.bottom=90;
		::SetTextColor(dc,RGB(255,255,255));
		::DrawTextA(dc,t,strlen(t),&rt2,DT_CENTER);

		memset(t,0,256);
		int64_t timer=m_pVideoInfo->pFormatCtx->duration/1000;
		int h=(timer/(1000*60*60));
		int m=(timer%(1000*60*60))/(1000*60);
		int s=((timer%(1000*60*60))%(1000*60))/1000;
		sprintf(t, "timer:%d:%d:%d",h,m,s);
		rt2.top=90;
		rt2.bottom=120;
		::SetTextColor(dc,RGB(255,255,255));
		::DrawTextA(dc,t,strlen(t),&rt2,DT_CENTER);

		memset(t,0,256);
		sprintf(t, "Vp差值:%f",last_duration);
		rt2.top=120;
		rt2.bottom=150;
		::SetTextColor(dc,RGB(255,255,255));
		::DrawTextA(dc,t,strlen(t),&rt2,DT_CENTER);

		//PTS
		memset(t,0,256);
		int ll2=pts;
		h=(ll2/(60*60));
		m=(ll2%(60*60))/(60);
		s=((ll2%(60*60))%(60));
		sprintf(t, "timer:%d:%d:%d",h,m,s);
		rt2.top=150;
		rt2.bottom=180;
		::SetTextColor(dc,RGB(255,255,255));
		::DrawTextA(dc,t,strlen(t),&rt2,DT_CENTER);
		
		//音频
		memset(t,0,256);
		sprintf(t, "audio:%.3f",m_pVideoInfo->audio_clock);
		rt2.top=180;
		rt2.bottom=210;
		::SetTextColor(dc,RGB(255,255,255));
		::DrawTextA(dc,t,strlen(t),&rt2,DT_CENTER);

		//误差
		memset(t,0,256);
		sprintf(t, "V_Adif:%.3f",abs(pts-m_pVideoInfo->audio_clock));
		rt2.top=210;
		rt2.bottom=240;
		::SetTextColor(dc,RGB(255,255,255));
		::DrawTextA(dc,t,strlen(t),&rt2,DT_CENTER);

		double V_ADiff=pts-m_pVideoInfo->audio_clock+diff;
		memset(t,0,256);
		sprintf(t, "视频快:%.3f",V_ADiff);
		
		rt2.top=240;
		rt2.bottom=270;
		::SetTextColor(dc,RGB(255,255,255));
		::DrawTextA(dc,t,strlen(t),&rt2,DT_CENTER);

		memset(t,0,256);
		sprintf(t, "delay:%.3f",diff);
		rt2.top=270;
		rt2.bottom=300;
		::SetTextColor(dc,RGB(255,255,255));
		::DrawTextA(dc,t,strlen(t),&rt2,DT_CENTER);
		if(buf==NULL)
		{
		   memset(t,0,256);
		   sprintf(t, "丢:%f",pts);
		   rt2.top=300;
		   rt2.bottom=330;
		   ::SetTextColor(dc,RGB(255,255,255));
		  ::DrawTextA(dc,t,strlen(t),&rt2,DT_CENTER);
		}
	}
}

#endif
//unsigned __stdcall VideoRefreshthread(LPVOID lpParameter);  

char * c_left(char *dst,char *src, int n)
{
	char *p = src;
	char *q = dst;
	int len = strlen(src);
	if(n>len) n = len;
	/*p += (len-n);*/   /*从右边第n个字符开始*/
	while(n--) *(q++) = *(p++);
	*(q++)='\0'; /*有必要吗？很有必要*/
	return dst;
}
int  KKPlayer::KKProtocolAnalyze(char* StrfileName,KKPluginInfo &KKPl)
{
	char* pos=strstr(StrfileName,":") ;
	int ret=0;
	if(pos!=NULL)
	{ 
		ret =-1;
		int Len=strlen(StrfileName)+1024;
		char *ProName=(char*)::malloc(Len);
		memset(ProName,0,Len);
		int lll=pos-StrfileName;
		c_left(ProName,StrfileName,lll);

		std::list<KKPluginInfo>::iterator It=KKPlayer::KKPluginInfoList.begin();
		for(;It!=KKPluginInfoList.end();++It)
		{
			if(strcmp(ProName,It->ptl)==0)
			{
				int len=strlen(StrfileName)-lll;
				if(len>0)
				{
					strcpy(ProName,pos+1);
					//strcpy(ProName,"");
					strcpy(StrfileName,"");
					strcpy(StrfileName,ProName);
					KKPl=*It;/**/
					ret= 1;
				}
			}
		}
		::free(ProName);
      
		
	}
	return ret;
}
int  KKPlayer::GetRealtime()
{

	if(m_ePlayerState==EPS_ReadingOk && m_pVideoInfo!=NULL)
		return m_pVideoInfo->realtime;
	return -1;

}




void KKPlayer::ForceAbort()
{
    if(m_ePlayerState==EPS_ReadingOk && m_pVideoInfo!=NULL)
	{
	    m_pVideoInfo->abort_request=1;
	}
}

///设置是否呈现
void KKPlayer::SetRender(bool bRender)
{
    m_bRender=bRender;
}
//返回 1 流媒体
int  is_realtime2(const char *name)
{
	if(   !strcmp(name, "rtp")    || 
		!strcmp(name, "rtsp")   || 
		!strcmp(name, "sdp")
		)
		return 1;


	if(strncmp(name, "rtmp:",5)==0){
		return 1;
	}else if(!strncmp(name, "rtp:", 4)|| !strncmp(name, "udp:", 4)){
		  return 1;
	}else if(strncmp(name, "rtsp:",5)==0){
		return 1;
	}
	return 0;
}
int  KKPlayer::OpenMedia(const char* URL,const char* Other)
{
	
	if(strlen(URL)>2047)
	{
	    return -2;
	}
	if(m_pLoadFrame)
	  av_frame_unref(m_pLoadFrame);
	m_nUnUsedFrame = 1;
	m_PlayerLock.Lock();
	if(m_bOpen){
		m_PlayerLock.Unlock();
        return -1;
	}

	m_nEStreamMediaType=ESMT_No;
	{
		CKKGurd gd(m_StateLock);
		m_ePlayerState = EPS_Opening;
	}
	m_bOpen=true;	

	m_CacheAvCounter=0;
	
	int ll = sizeof(SKK_VideoState);
	m_pVideoInfo = (SKK_VideoState*)KK_Malloc_(sizeof(SKK_VideoState));
	memset(m_pVideoInfo,0,ll);
	m_pVideoInfo->pLock = &this->m_CtxLock;
	m_strcmd=Other;

	m_pVideoInfo->nMaxRealtimeDelay=32000;//单位s
	m_pVideoInfo->pKKPluginInfo=(KKPluginInfo *)KK_Malloc_(sizeof(KKPluginInfo));
	m_pVideoInfo->pflush_pkt =(AVPacket*)KK_Malloc_(sizeof(AVPacket));
    m_pVideoInfo->DstAVff=m_DstAVff;
	m_pVideoInfo->IRender=m_pPlayUI->GetRender();

	#ifndef _WINDOWS
			m_pVideoInfo->ViewSurface=m_pViewSurface;
			LOGE_KK("Debug", "m_pVideoInfo->ViewSurface= %d \n",(int)m_pViewSurface);
    #endif
	
    m_PktSerial=0;
	m_nSeekTime=-1;

	LOGE_KK("Debug", "Movie Path：\n");
	LOGE_KK("Debug", "%s",URL);
	LOGE_KK("Debug", "\n");
	m_pVideoInfo->viddec.decoder_tid.ThOver=true;
	m_pVideoInfo->auddec.decoder_tid.ThOver=true;
	m_pVideoInfo->subdec.decoder_tid.ThOver=true;
	
	m_ReadThreadInfo.ThOver=true;
	m_ReadThreadInfo.Addr=0;
	

	m_VideoRefreshthreadInfo.ThOver=true;
	m_VideoRefreshthreadInfo.Addr=0;

	m_AudioCallthreadInfo.ThOver=true;
	m_AudioCallthreadInfo.Addr=0;



#ifdef WIN32_KK
    m_ReadThreadInfo.ThreadHandel=0;
    m_VideoRefreshthreadInfo.ThreadHandel=0;
    m_AudioCallthreadInfo.ThreadHandel=0;
#endif

	
	
	av_init_packet(m_pVideoInfo->pflush_pkt);
	flush_pkt.data = (uint8_t *)m_pVideoInfo->pflush_pkt;

	memcpy(m_pVideoInfo->filename, URL, strlen(URL));

	m_pVideoInfo->realtime = is_realtime2(URL);
    if(m_pVideoInfo->realtime){
	   m_pVideoInfo->NeedWait=true;
	}

	if(strncmp(URL, "http:",5)==0||strncmp(URL, "https:",6)==0){
		m_pVideoInfo->NeedWait=true;
	}

	memset(&m_AVPlayInfo,0,sizeof(MEDIA_INFO));
	//初始化队列
	packet_queue_init(&m_pVideoInfo->videoq);
	m_pVideoInfo->videoq.m_pWaitCond=new CKKCond_t();
	//m_pVideoInfo->videoq.m_pWaitCond->SetCond();
    //音频包
	packet_queue_init(&m_pVideoInfo->audioq);
	m_pVideoInfo->audioq.m_pWaitCond=new CKKCond_t();
	//m_pVideoInfo->audioq.m_pWaitCond->SetCond();
	//字幕包
	packet_queue_init(&m_pVideoInfo->subtitleq);
	m_pVideoInfo->subtitleq.m_pWaitCond=new CKKCond_t();
	//m_pVideoInfo->subtitleq.m_pWaitCond->SetCond();


	init_clock(&m_pVideoInfo->vidclk, &m_pVideoInfo->videoq.serial);
	init_clock(&m_pVideoInfo->audclk, &m_pVideoInfo->audioq.serial);
	init_clock(&m_pVideoInfo->extclk, &m_pVideoInfo->extclk.serial);

	m_pVideoInfo->video_stream=-1;
	m_pVideoInfo->audio_stream=-1;
	m_pVideoInfo->subtitle_stream=-1;

	m_pVideoInfo->audio_clock_serial = -1;
	m_pVideoInfo->av_sync_type = av_sync_type;

	/* start video display */
	if (frame_queue_init(&m_pVideoInfo->pictq, &m_pVideoInfo->videoq, VIDEO_PICTURE_QUEUE_SIZE, 1) < 0)
	{

	}
	

	if (frame_queue_init(&m_pVideoInfo->subpq, &m_pVideoInfo->subtitleq, SUBPICTURE_QUEUE_SIZE, 1) < 0)
	{

	}
	

	if (frame_queue_init(&m_pVideoInfo->sampq, &m_pVideoInfo->audioq, SAMPLE_QUEUE_SIZE, 1) < 0)
	{

	}

	
    m_pVideoInfo->AVRate=100;
	
	float aa=(float)m_pVideoInfo->AVRate/100;
	snprintf(m_pVideoInfo->Atempo,sizeof(m_pVideoInfo->Atempo),"atempo=%f",aa);

	m_pVideoInfo->InAudioSrc=NULL;
	m_pVideoInfo->OutAudioSink=NULL;
	m_pVideoInfo->AudioGraph=NULL;
	m_nhasVideoAudio=0;
	LOGE_KK("Debug", "创建线程\n");

	m_ReadThreadInfo.ThOver        = false;
	m_VideoRefreshthreadInfo.ThOver= false;
	m_AudioCallthreadInfo.ThOver   = false;

	///命令行选项
	if (m_strcmd.length() > 1)
	{
		char   **argv = NULL;
		int argc = 0;
		argv = KKCommandLineToArgv(m_strcmd.c_str(), &argc);
		for (int i = 0; i < argc; i++)
		{
			char* cmd = argv[i];
			if (0 == strcmp(cmd, "-p")) {
				//暂停
				m_pVideoInfo->paused |= EKK_AVP1;
			}
		}
		free(argv);
	}

#ifdef WIN32_KK
	

	
	m_VideoRefreshthreadInfo.ThreadHandel  = (HANDLE)_beginthreadex(NULL, NULL, VideoRefreshthread, (LPVOID)this, 0,&m_VideoRefreshthreadInfo.Addr);
    m_AudioCallthreadInfo.ThreadHandel     = (HANDLE)_beginthreadex(NULL, NULL, Audio_Thread, (LPVOID)this, 0,&m_AudioCallthreadInfo.Addr);
	m_ReadThreadInfo.ThreadHandel          = (HANDLE)_beginthreadex(NULL, NULL, ReadAV_thread, (LPVOID)this, 0,&m_ReadThreadInfo.Addr);
	if(m_ReadThreadInfo.ThreadHandel==0)
		assert(0);
#else
	
	m_ReadThreadInfo.Addr = pthread_create(&m_ReadThreadInfo.Tid_task, NULL, (void* (*)(void*))ReadAV_thread, (LPVOID)this);
	LOGE_KK("Debug", "m_ReadThreadInfo.Addr =%d\n",m_ReadThreadInfo.Addr);
	LOGE_KK("Debug", "VideoRefreshthread XX");
	m_VideoRefreshthreadInfo.Addr = pthread_create(&m_VideoRefreshthreadInfo.Tid_task, NULL, (void* (*)(void*))VideoRefreshthread, (LPVOID)this);
	m_AudioCallthreadInfo.Addr =pthread_create(&m_AudioCallthreadInfo.Tid_task, NULL, (void* (*)(void*))Audio_Thread, (LPVOID)this);
#endif

	m_lstPts=-1;

	m_AVCacheInfo.AudioSize=0;
	m_AVCacheInfo.MaxTime=0;
	m_AVCacheInfo.VideoSize=0;


#ifndef WIN32
	m_PlayerLock.Unlock();
#endif
	/*if(strcmp(Other,"-pause")==0)
		m_pVideoInfo->paused=1;*/

	LOGE_KK("Debug", "create Over\n");
	return 0;
}


void KKPlayer::OnDecelerate()
{
	if(m_ePlayerState!=EPS_ReadingOk)
		return;
	int64_t seek_target =0;
	bool okk=false;
   // m_PlayerLock.Lock();
	if(m_pVideoInfo!=NULL&&m_pVideoInfo->AVRate>50)
	{
		m_pVideoInfo->AVRate-=10;
		float aa=(float)m_pVideoInfo->AVRate/100;
		snprintf(m_pVideoInfo->Atempo,sizeof(m_pVideoInfo->Atempo),"atempo=%f",aa); 
		seek_target= m_fCurTime;/**/
		//packet_queue_put(&m_pVideoInfo->audioq,m_pVideoInfo->pflush_pkt, m_pVideoInfo->pflush_pkt);
		//packet_queue_put(&m_pVideoInfo->videoq,m_pVideoInfo->pflush_pkt, m_pVideoInfo->pflush_pkt);
        okk=true;
	}
	//m_PlayerLock.Unlock();
	if(okk)
	{
		KKSeek(Right,1);
		m_pVideoInfo->seek_req=2;
	}
}
void KKPlayer::OnAccelerate()
{
	if(m_ePlayerState!=EPS_ReadingOk)
		return;

	int64_t seek_target=0;
	bool okk=false;
	//m_PlayerLock.Lock();
	if(m_pVideoInfo!=NULL&&m_pVideoInfo->AVRate<200)
	{
		m_pVideoInfo->AVRate+=10;
		float aa=(float)m_pVideoInfo->AVRate/100;
		  snprintf(m_pVideoInfo->Atempo,sizeof(m_pVideoInfo->Atempo),"atempo=%.2f",aa);
        //packet_queue_put(&m_pVideoInfo->audioq,m_pVideoInfo->pflush_pkt, m_pVideoInfo->pflush_pkt);
		//packet_queue_put(&m_pVideoInfo->videoq,m_pVideoInfo->pflush_pkt, m_pVideoInfo->pflush_pkt);
		seek_target = m_fCurTime;
		okk=true;
	}
	//m_PlayerLock.Unlock();

	if(okk)
	{
		KKSeek(Right,1);
		m_pVideoInfo->seek_req=2;
	}
}
int  KKPlayer::GetAVRate()
{
	int Rate=100;
	CKKGurd gd(m_StateLock);
    if(m_ePlayerState!=EPS_ReadingOk)
		return Rate;
	
	//m_PlayerLock.Lock();
	if(m_pVideoInfo!=NULL)
	{
		Rate=m_pVideoInfo->AVRate;
	}
	//m_PlayerLock.Unlock();
	return Rate;
}
/*********视频刷新线程********/
 unsigned __stdcall KKPlayer::VideoRefreshthread(LPVOID lpParameter)
 {
    //return 0;
	 LOGE_KK("Debug", "VideoRefreshthread strat \n");
     KKPlayer* pPlayer=(KKPlayer*)lpParameter;
	 pPlayer->m_VideoRefreshthreadInfo.ThOver=false;
	 int llxx=0;
     
#ifdef WIN32
	 ::SetThreadPriority(::GetCurrentThread(), THREAD_PRIORITY_HIGHEST);
#endif

#ifdef Android_Plat
	//pPlayer->m_pVideoRefreshJNIEnv=kk_jni_attach_env();
#endif
	 pPlayer->m_pVideoInfo->remaining_time = 0.0;
	
	 while(pPlayer->m_bOpen)
	 {
		
		if(pPlayer->m_pVideoInfo!=NULL)
		{
			if(pPlayer->m_pVideoInfo->abort_request==1)
				break;
             pPlayer->OnVideoRefresh();
		}else{
			av_usleep(5000);
		}
		
	 }
#ifdef Android_Plat
	//kk_jni_detach_env();
	pPlayer->m_pVideoRefreshJNIEnv=NULL;
#endif

	 pPlayer->m_VideoRefreshthreadInfo.Addr=0;
	 pPlayer->m_VideoRefreshthreadInfo.ThOver=true;
	 LOGE_KK("Debug", "VideoRefreshthread over \n");
#ifdef WIN32
	 _endthreadex(0);
#endif
	 return 0;
 }

 //音频数据回调线程
 unsigned __stdcall  KKPlayer::Audio_Thread(LPVOID lpParameter)
 {
	
	 KKPlayer* pPlayer=(KKPlayer*)lpParameter;
	 pPlayer->m_AudioCallthreadInfo.ThOver=false;
	 pPlayer->ReadAudioCall(); 
	 pPlayer->m_AudioCallthreadInfo.ThOver=true;
#ifdef WIN32
	 _endthreadex(0);
#endif
	 return 0;
 }
 void KKPlayer::ReadAudioCall()
 {
	
	 while(m_pVideoInfo!=NULL)
	 {
		 if (m_pVideoInfo->abort_request)
			 break;
		 if (m_pVideoInfo->pKKAudio != NULL&&m_pVideoInfo->IsReady&&m_pVideoInfo->audio_st != NULL)
		 {
			 if(!m_pVideoInfo->pKKAudio->ReadAudio())
			 {
				 break;
			 }
		 }
		 else
			 av_usleep(20000);
	 }
	 LOGE_KK("Debug", "KKPlayer Audio_Thread over \n");
	
 }


 static int decode_interrupt_cb(void *ctx)
{
	 SKK_VideoState *is =(SKK_VideoState *) ctx;
	 return is->abort_request;
}
int  KKPlayer::GetRealtimeDelay()
{
	CKKGurd gd(m_StateLock);
	if(m_ePlayerState!=EPS_ReadingOk)
		return 0;
	if(m_pVideoInfo!=NULL)
          return m_pVideoInfo->nRealtimeDelay;
	return 0;
}
//强制刷新
void KKPlayer::ForceFlushQue()
{
	AvflushRealTime(1);  
	AvflushRealTime(2);  
	AvflushRealTime(3);  
}
 AVIOContext * CreateKKIo(SKK_VideoState *);
 int KKPlayer::ShowTraceAV(bool Show)
 {
		if(m_pVideoInfo!=NULL){
			m_pVideoInfo->bTraceAV=Show;
			return 1;
		}
		return 0;
 }

float  KKPlayer::GetPlayTime()
{
    return m_fCurTime;
}
int  KKPlayer::GetTotalTime()
{
    return m_TotalTime;
}

 void  KKPlayer::OpenAudioDev()
 {
    ///再这里设置音频回调
	m_pVideoInfo->pKKAudio=m_pSound;
    m_pSound->SetAudioCallBack(audio_callback,m_pVideoInfo);
 }
//通知视频结束
void  KKPlayer::OnAvOverHandle(int AVQueSize,bool flush)
{	

   //循环播放，这样处理合理吗
   if(m_nPlayAvModel==1){
	   if(m_nOverNotify==0)
	   {
		   int64_t ss = ResetStartIo();
		   Avflush(ss);
		   /*int seektime = 0;
		   if(m_nStartTime!=AV_NOPTS_VALUE)
			  seektime = m_nStartTime/ AV_TIME_BASE;
		   AVSeek(seektime,-1);
		   m_CurTime = seektime;*/
		   m_nOverNotify=1;
		   m_fCurTime = 0;
	   }
   }else if(AVQueSize==-1){
	   double fCurtime =get_master_clock(m_pVideoInfo);
	   double fTotalTime = (double)(m_pVideoInfo->pFormatCtx->duration/1000)/1000;
	   if(fCurtime-fTotalTime>0.0000001|| abs(fCurtime - fTotalTime)<0.1){
		   m_pPlayUI->OpenMediaStateNotify(m_pVideoInfo->filename,KKAVOver);
	   }
   }
}
void  KKPlayer::InterSeek(AVFormatContext*  pFormatCtx)
{
	if(m_nSeekTime>-1&&m_pVideoInfo!=NULL&&m_pVideoInfo->IsReady){
		double incr, pos, frac;
		pos =m_nSeekTime;
		if(pos<=0)
			pos =0;
	

		if (pFormatCtx->start_time != AV_NOPTS_VALUE && pos < pFormatCtx->start_time / (double)AV_TIME_BASE)
			pos = pFormatCtx->start_time / (double)AV_TIME_BASE;
		stream_seek(m_pVideoInfo, (int64_t)(pos * AV_TIME_BASE), 0, 0);
		m_nSeekTime=-1;
		//m_pVideoInfo->seek_req=0;
	}
}

void  KKPlayer::AvDelayParser()
{

	int pkgsize=m_pVideoInfo->audioq.size+m_pVideoInfo->videoq.size;
	int NeedWaitCount=MAX_QUEUE_SIZE_HALF;
	if(m_nEStreamMediaType== ESMT_Rtmp){
	    NeedWaitCount=5120;//2560;//
	}else if(m_nEStreamMediaType== ESMT_Hls){
        NeedWaitCount=81920;
	}
	///有音频有视频
	if(m_pVideoInfo->NeedWait&&m_pVideoInfo->audio_st!=NULL){
		    ///
			if(m_pVideoInfo->audioq.size==0||m_pVideoInfo->videoq.size==0){
				  m_CacheAvCounter++; 
			}
			
			if(m_pVideoInfo->bTraceAV)
			   LOGE_KK("Debug", "de %.3fs,que audioq:%d,videoq:%d,subtitleq:%d \n",m_pVideoInfo->nRealtimeDelay,m_pVideoInfo->audioq.size,m_pVideoInfo->videoq.size,m_pVideoInfo->subtitleq.size);
			

			bool NeedWaitMoreData=false;///需要等待或者缓存已经完成
			//开始缓存&&avsize>100
			if(m_pVideoInfo->nRealtimeDelay<=2){
				
				
					///有音频，有视频
					if((m_nhasVideoAudio&EHVA_Audio)==EHVA_Audio&&(m_nhasVideoAudio&EHVA_Video)==EHVA_Video){
						if(m_pVideoInfo->audioq.size<=1000&&m_pVideoInfo->videoq.size<=1000)
							   NeedWaitMoreData  = true;
					}else {
						   if(m_nhasVideoAudio&EHVA_Video==EHVA_Video)///视频
								NeedWaitMoreData =  pkgsize<=500 ? true:false;
						   else
								NeedWaitMoreData =  pkgsize<=1000 ? true:false;
					}
					if(NeedWaitMoreData && m_pVideoInfo->IsReady && !m_pVideoInfo->eof){
						m_pVideoInfo->IsReady=0;
						m_pVideoInfo->paused|=EKK_AVP_NeedMore;
						m_CacheAvCounter=0;
						if(m_pPlayUI!=NULL){
							m_pPlayUI->OpenMediaStateNotify(m_pVideoInfo->filename,KKAVWait);
						}
					}
					
			}

			///既有音频又有视频
            if((m_nhasVideoAudio&EHVA_Video)==EHVA_Video&&(m_nhasVideoAudio&EHVA_Audio)==EHVA_Audio){
                    NeedWaitMoreData = pkgsize> MAX_QUEUE_SIZE_HALF ? true:false;
						//m_pVideoInfo->audioq.size>2000 && m_pVideoInfo->videoq.size>2000;
			}else{
				    NeedWaitMoreData = pkgsize> MAX_QUEUE_SIZE_HALF ? true:false;
			}
			
			///缓存已经完成了
			if((NeedWaitMoreData|| m_pVideoInfo->eof)&&!m_pVideoInfo->IsReady){
				m_pVideoInfo->IsReady=1;
				m_pVideoInfo->paused &=NOEKK_AVP_NeedMore;

				if(m_pPlayUI!=NULL){
					if(m_pVideoInfo->eof==0)
					   m_pPlayUI->OpenMediaStateNotify(m_pVideoInfo->filename,KKAVReady);
				}
			}
		}else if(m_pVideoInfo->NeedWait&&m_pVideoInfo->audio_st==NULL&&m_pVideoInfo->video_st!=NULL){
		        ///只有视频
				if(m_pVideoInfo->videoq.size==0){
					m_CacheAvCounter++; 
				}   

				if(m_pVideoInfo->videoq.size<=1000&&m_pVideoInfo->IsReady&&m_CacheAvCounter>100&&!m_pVideoInfo->eof){
					m_pVideoInfo->IsReady=0;
					m_pVideoInfo->paused|=EKK_AVP_NeedMore;
					m_CacheAvCounter=0;
				}
				
				if(m_pVideoInfo->bTraceAV)
					LOGE_KK("Debug", "de %.3fs,que audioq:%d,videoq:%d,subtitleq:%d \n",m_pVideoInfo->nRealtimeDelay,m_pVideoInfo->audioq.size,m_pVideoInfo->videoq.size,m_pVideoInfo->subtitleq.size);
				///缓存已经完成
				if((m_pVideoInfo->videoq.size>2000|| m_pVideoInfo->eof)&& m_pVideoInfo->IsReady==0){
					   m_pVideoInfo->IsReady=1;
					   m_pVideoInfo->paused &= NOEKK_AVP_NeedMore;
					   if(m_pPlayUI!=NULL){
						   if(m_pVideoInfo->eof==0)
						    m_pPlayUI->OpenMediaStateNotify(m_pVideoInfo->filename, KKAVReady);
					  }
				}
		}
}





 int64_t    KKPlayer::ResetStartIo(int ini)
{
	 if(m_pVideoInfo->realtime)
		 return 0;
     int64_t timestamp=0;
	 m_fCurTime = 0;
	 m_nInterCurTime = 0;
	 if (m_nStartTime != AV_NOPTS_VALUE) 
	 {
		
		timestamp = m_nStartTime*AV_TIME_BASE;
		
		/* add the stream start time */
		if (m_pVideoInfo->pFormatCtx->start_time != AV_NOPTS_VALUE)
		{
			if( m_nStartTime > m_pVideoInfo->pFormatCtx->start_time)
			{
			
			}
			else{
			   timestamp =   m_pVideoInfo->pFormatCtx->start_time;
			}
		}
	 }else{
		 if(ini)
			 return 0;
		/* add the stream start time */
		if (m_pVideoInfo->pFormatCtx->start_time != AV_NOPTS_VALUE)
		{
			   timestamp =   m_pVideoInfo->pFormatCtx->start_time;
		}
	 }

	 m_fCurTime= timestamp / AV_TIME_BASE;
	 //有些流媒体不支持
     int ret = avformat_seek_file(m_pVideoInfo->pFormatCtx, -1, INT64_MIN, timestamp, INT64_MAX, 0);
	 if (ret < 0) {
			 assert(0);
	 }
	 
	 m_pVideoInfo->audio_clock = m_fCurTime;
	 m_pVideoInfo->vidclk.pts  = m_fCurTime;
	 m_pVideoInfo->vidclk.pts  = m_fCurTime;
	 m_pVideoInfo->audclk.pts  = m_fCurTime;
	 m_pVideoInfo->extclk.pts  = m_fCurTime;

	 return timestamp;
}

void  KKPlayer:: OnRealSeek()
{

        InterSeek( m_pVideoInfo->pFormatCtx);
        /******真正的seek快进*******/
		if (m_pVideoInfo->seek_req&&!m_pVideoInfo->realtime)
		{
			int64_t seek_target = m_pVideoInfo->seek_pos;
			int64_t seek_min    =m_pVideoInfo->seek_rel > 0 ? seek_target - m_pVideoInfo->seek_rel + 2: INT64_MIN;//m_pVideoInfo->seek_pos-10 * AV_TIME_BASE; //
			int64_t seek_max    =m_pVideoInfo->seek_rel < 0 ? seek_target - m_pVideoInfo->seek_rel - 2: INT64_MAX;//=m_pVideoInfo->seek_pos+10 * AV_TIME_BASE; //

		
			if(m_pVideoInfo->seek_req==2){
				seek_min=seek_target;
				if(m_pVideoInfo->audio_st!=0)
                      seek_max =seek_target + 1000000;
			}
		
			EKKPlayerErr kkerr=KKSeekOk;
			int ret = avformat_seek_file( m_pVideoInfo->pFormatCtx, -1, seek_min, seek_target, seek_max, m_pVideoInfo->seek_flags);
			if (ret < 0) {
                 kkerr=KKSeekErr;
			     assert(0);
				//失败
			}else{
				Avflush(seek_target);
			}
			if(m_pPlayUI!=NULL){
		            m_pPlayUI->OpenMediaStateNotify(m_pVideoInfo->filename,kkerr);
		    }

			 m_fCurTime=seek_target/AV_TIME_BASE;
			 m_pVideoInfo->SeekVideoSerial=m_pVideoInfo->videoq.serial;
			 m_pVideoInfo-> SeekAudioSerial=m_pVideoInfo->videoq.serial;
		 	 m_pVideoInfo->seek_req=0;
			
		}else if(m_nNeedReset){
			
			if (!m_pVideoInfo->realtime) {
				m_fCurTime = 0;
				m_pVideoInfo->paused |= EKK_AVP1;
				int64_t ss = ResetStartIo();
				//强制清空队列
				packet_queue_flush(&m_pVideoInfo->videoq);
				packet_queue_flush(&m_pVideoInfo->audioq);
				packet_queue_flush(&m_pVideoInfo->subtitleq);

				
				/******字幕图片队列******/
				frame_queue_flush(&m_pVideoInfo->subpq);
				/******音频采样队列******/
				frame_queue_flush(&m_pVideoInfo->sampq);
				m_pVideoInfo->pictq.m_pWaitCond->SetCond();
				m_pVideoInfo->subpq.m_pWaitCond->SetCond();
				m_pVideoInfo->sampq.m_pWaitCond->SetCond();
				if (m_pLoadFrame)
					av_frame_unref(m_pLoadFrame);
				//avctx_flush(&m_pVideoInfo->viddec);
				//avctx_flush(&m_pVideoInfo->auddec);
				int poolCount=0;
				//av_get_def_pool(m_pVideoInfo->viddec.avctx, &poolCount);

				
				avformat_flush(m_pVideoInfo->pFormatCtx);

				m_CtxLock.Lock();
				m_pVideoInfo->nNeedResetViddec = 1;
				m_CtxLock.Unlock();
				//Avflush
				//Avflush(ss);

				/*m_pVideoInfo->audio_clock =0;
				m_pVideoInfo->vidclk.pts = 0;

				m_pVideoInfo->vidclk.pts = 0;
				m_pVideoInfo->audclk.pts = 0;
				m_pVideoInfo->extclk.pts = 0;*/
			}
		    m_nNeedReset= 0;
			  
		}

		m_PktSerial=m_pVideoInfo->viddec.pkt_serial;

}
void KKPlayer::RetArg(const char* cmd)
{
///命令行选项
	if(strlen(cmd)>1)
	{
		 char   **argv = NULL;
		 int argc=0;
		 argv =KKCommandLineToArgv(cmd, &argc);
		 for (int i = 0; i < argc; i++)
		 {
				char* cmd=argv[i];
				if(0 == strcmp(cmd,"-ss")){//开始时间
					 if (++i < argc){
					   cmd=argv[i];
					   int Start = parse_time_or_die(cmd, 1) / AV_TIME_BASE;
					   if(Start!=0&&m_nStartTime!= Start)
					   {
						   m_nStartTime = Start;
						   AVSeek(Start);
					   }
					} 
				}else if(0 == strcmp(cmd,"-es")){//结束时间
					 if (++i < argc){
					   cmd=argv[i];
					   m_nAvEndTime=parse_time_or_die(cmd, 1)/ AV_TIME_BASE;
					} 
				}
				else if (0 == strcmp(cmd, "-loop")) {
					if (++i < argc) {
						cmd = argv[i];
						m_nPlayAvModel = atoi(cmd);
					}
				}
		 }
		 free(argv);
	}
}

void KKPlayer::SetStartTime(int Startime)
{
	m_nStartTime = Startime;
}
void KKPlayer::SetEndTime(int EndTime)
{
	m_nAvEndTime = EndTime;
}
/*****读取视频信息******/
void  KKPlayer::OnReadAV()
{
	LOGE_KK("Debug", "ReadAV thread start \n");

#ifndef WIN32
	//只有windows才能跨进程解锁
	m_PlayerLock.Lock();
#endif
#ifdef WIN32
	 ::SetThreadPriority(::GetCurrentThread(), THREAD_PRIORITY_TIME_CRITICAL);
	 m_PlayerLock.Unlock();
#endif
	 {
		 CKKGurd gd(m_StateLock);
		 m_ePlayerState = EPS_ReadingThread;
	 }

	
	

   LOGE_KK("Debug", "ReadAV Unlock \n");

	AVFormatContext* pFormatCtx   = NULL;
	AVDictionary*    format_opts  = NULL;
	int err=-1,scan_all_pmts_set  = 0;
	AVIOContext *customio         = 0;
	{
		    LOGE_KK("Debug", "ReadAV lock \n");
			CKKGurd gd(m_PlayerLock);
			LOGE_KK("Debug", "ReadAV thread start \n");
			
			pFormatCtx= avformat_alloc_context();
			
			m_pVideoInfo->iformat=NULL;
			
			if(m_pKKPlayerGetUrl){
				char* outstr=0;
				err=m_pKKPlayerGetUrl(m_pVideoInfo->filename,&outstr);
				if(err==0&&outstr!=0){
					 strcpy(m_pVideoInfo->filename,outstr);
				}
			}

			pFormatCtx->interrupt_callback.callback =   decode_interrupt_cb;
			pFormatCtx->interrupt_callback.opaque   =   m_pVideoInfo;

			if (!av_dict_get(format_opts, "scan_all_pmts", NULL, AV_DICT_MATCH_CASE)) {
				av_dict_set(&format_opts, "scan_all_pmts", "1", AV_DICT_DONT_OVERWRITE);
				
				scan_all_pmts_set = 1;
			}
			if(!strncmp(m_pVideoInfo->filename, "http:",4)){
				av_dict_set(&format_opts,"user-agent", "Mozilla/5.0 (Windows; U; Windows NT 6.1; zh-CN; rv:1.9.2.15) Gecko/20110303 Firefox/3.6.15", AV_DICT_DONT_OVERWRITE);
			}
			m_pVideoInfo->pFormatCtx = pFormatCtx;
			
			/*if(m_pPlayUI->PreOpenUrlCallForSeg(m_pVideoInfo->filename,&m_AvIsSeg,(int*)&m_pVideoInfo->abort_request)){
			  
			   m_pVideoInfo->NeedWait=true;
			}*/

	
			if(KKProtocolAnalyze(m_pVideoInfo->filename,*m_pVideoInfo->pKKPluginInfo)==1){	
				customio=pFormatCtx->pb=CreateKKIo(m_pVideoInfo);
				pFormatCtx->flags = AVFMT_FLAG_CUSTOM_IO;
				m_pVideoInfo->ioflags= AVFMT_FLAG_CUSTOM_IO;
			}

			{
				CKKGurd gd(m_StateLock);
				m_ePlayerState = EPS_ReadingPre;
			}
			m_nEStreamMediaType=ESMT_No;
			if(!strncmp(m_pVideoInfo->filename, "rtmp:",5)){
				//rtmp 不支持 timeout
				av_dict_set(&format_opts, "rw_timeout", MaxTimeOutStr, AV_DICT_MATCH_CASE);
				av_dict_set(&format_opts, "fflags", "nobuffer", 0);
				av_dict_set(&format_opts, "analyzeduration","1000000",0);
				m_nEStreamMediaType=ESMT_Rtmp;
			}else if(!strncmp(m_pVideoInfo->filename, "rtsp:",5)){
				 av_dict_set(&format_opts, "rtsp_transport", "tcp", AV_DICT_MATCH_CASE);
				 av_dict_set(&format_opts, "fflags","nobuffer", 0);
				 av_dict_set(&format_opts, "max_delay","50",0);
				 av_dict_set(&format_opts, "analyzeduration","1000000",0);
				 m_nEStreamMediaType=ESMT_Rtsp;
			}else if(!strncmp(m_pVideoInfo->filename, "live:",5)){
				 av_dict_set(&format_opts, "fflags","nobuffer", 0);
				 av_dict_set(&format_opts, "max_delay","50",0);
				 av_dict_set(&format_opts, "analyzeduration","1000000",0);
			}else if(!0){
				 av_dict_set(&format_opts, "max_reload","10000",0);
			    
			}
			///命令行选项
			if(m_strcmd.length()>1)
			{
				 char   **argv = NULL;
				 int argc=0;
				 argv =KKCommandLineToArgv(m_strcmd.c_str(), &argc);
				 for (int i = 0; i < argc; i++)
				 {
						char* cmd=argv[i];
						if(0 == strcmp(cmd,"-fflags"))
						{
							if (++i < argc){
							   cmd=argv[i];
							   av_dict_set(&format_opts, "fflags", cmd, 0);
							}
						}else if(0 == strcmp(cmd,"-ss")){//开始时间
							 if (++i < argc){
							   cmd=argv[i];
							   m_nStartTime=parse_time_or_die(cmd, 1) / AV_TIME_BASE;
							} 
						}else if(0 == strcmp(cmd,"-es")){//结束时间
							 if (++i < argc){
							   cmd=argv[i];
							   m_nAvEndTime=parse_time_or_die(cmd, 1)/ AV_TIME_BASE;
							   int xxx = m_nAvEndTime;
							   xxx++;
							} 
						}else if(0 == strcmp(cmd,"-f")){//指定输入格式
							 if (++i < argc){
							   cmd=argv[i];
							   m_pVideoInfo->iformat = av_find_input_format(cmd);
							   if (0 == strcmp(cmd, "libndi_newtek"))
								   m_pVideoInfo->realtime = 1;
							} 
						}else if(0 == strcmp(cmd,"-vif")){
							 if (++i < argc){
							     cmd=argv[i];
								 if(!strcmp(cmd,"hap"))
									m_pVideoInfo->VIfCode=AV_CODEC_ID_HAP;
								 else if(!strcmp(cmd,"dxv"))
									 m_pVideoInfo->VIfCode=AV_CODEC_ID_DXV;
							} 
							if (++i < argc){
							     cmd=argv[i];
							     strcpy(m_pVideoInfo->VCodeName,cmd);
							} 
						}

						
				 }
				 free(argv);
			}

			//av_dict_set(&format_opts, "fflags", "-nobuffer", 0);
			m_pVideoInfo->OpenTime= av_gettime ()/1000/1000;
			double Opex=0;

			LOGE_KK("Debug", "call avformat_open_input \n");
	}

	//这一部分不需要锁
	{
			//此函数是阻塞的
			err =avformat_open_input(&pFormatCtx,m_pVideoInfo->filename,m_pVideoInfo->iformat,   &format_opts);
		    
			if(pFormatCtx!=0&&(strncmp(m_pVideoInfo->filename, "rtmp:",5)==0||strncmp(m_pVideoInfo->filename, "rtsp:",5)==0)){
				pFormatCtx->probesize = 100 *1024;
				pFormatCtx->max_analyze_duration=5 * AV_TIME_BASE;
				double  dx2=av_gettime ()/1000/1000-m_pVideoInfo->OpenTime;
				if(dx2>MaxTimeOut){
					err=-1;
				}
			}
			
			LOGE_KK("Debug", "avformat_open_input=%d,%s \n",err,m_pVideoInfo->filename);
			
			{
				CKKGurd gd(m_StateLock);
				m_ePlayerState = EPS_ReadingOk;
			}
			
			if(m_pVideoInfo->abort_request==1)
			{
				
				 if(pFormatCtx!=NULL)
				   avformat_free_context(pFormatCtx);
				 m_pVideoInfo->pFormatCtx = NULL; 
				 av_dict_free(&format_opts);
				 return;
			 }
 
			//文件打开失败
			if(err<0)
			{	
				if(customio!=0)
				{
						WillCloseKKIo(customio);
				}
			
				av_dict_free(&format_opts);
				if(pFormatCtx!=NULL){
				   avformat_free_context(pFormatCtx);
				   //avformat_close_input(&pFormatCtx);
				}
				m_pVideoInfo->pFormatCtx = NULL;
				char urlx[2048]="";
				strcpy(urlx,m_pVideoInfo->filename);
				m_pVideoInfo->abort_request=1;
				if(m_pPlayUI!=NULL){
					m_pPlayUI->OpenMediaStateNotify(urlx,KKOpenUrlOkFailure);
				}

				LOGE_KK("Debug", "avformat_open_input <0 \n");
				return;
			}
	}

	if (scan_all_pmts_set)
		av_dict_set(&format_opts, "scan_all_pmts", NULL, AV_DICT_MATCH_CASE);

	av_format_inject_global_side_data(pFormatCtx);
	
	AVDictionary **opts;
	AVDictionary *codec_opts=NULL;
	opts=setup_find_stream_info_opts(pFormatCtx, codec_opts);
	// Retrieve stream information
	if(avformat_find_stream_info(pFormatCtx, opts)<0){
		av_dict_free(&format_opts);
		char urlx[256]="";
		strcpy(urlx,m_pVideoInfo->filename);
		m_pVideoInfo->abort_request=1;
		//m_PlayerLock.Unlock();
		if(m_pPlayUI!=NULL){
			m_pPlayUI->OpenMediaStateNotify(urlx,KKAVNotStream);
		}
		LOGE_KK("Debug", "avformat_find_stream_info<0 \n");
        if(opts!=NULL){
			for (int i = 0; i < pFormatCtx->nb_streams; i++)
				av_dict_free(&opts[i]);
			av_freep(&opts);
			opts = 0;
	    }
		return;
    }
	
	if(opts!=NULL){
        for (int i = 0; i < pFormatCtx->nb_streams; i++)
            av_dict_free(&opts[i]);
        av_freep(&opts);
		opts = 0;
	}

	av_dict_free(&format_opts);
	format_opts = 0;
	//if(m_bTrace)
	LOGE_KK("Debug", "avformat_find_stream_info Ok \n");
	
	int  i, ret=-1;
	int st_index[AVMEDIA_TYPE_NB]={-1,-1,-1,-1,-1};
	AVPacket pkt1, *pkt = &pkt1;
	int64_t stream_start_time=0;
	int pkt_in_play_range = 0;

	int64_t pkt_ts;
    int64_t duration= AV_NOPTS_VALUE;
	av_init_packet(pkt);
	
    ResetStartIo(1);

    char* wanted_stream_spec[AVMEDIA_TYPE_NB] = {0};
	wanted_stream_spec[AVMEDIA_TYPE_VIDEO]="vst";
	wanted_stream_spec[AVMEDIA_TYPE_SUBTITLE]="sst";
	wanted_stream_spec[AVMEDIA_TYPE_AUDIO]="ast";
	for (i = 0; i < pFormatCtx->nb_streams; i++) {
		AVStream *st = pFormatCtx->streams[i];
		enum AVMediaType type = st->codecpar->codec_type;

        st->discard = AVDISCARD_ALL;
        if (type >= 0 &&  st_index[type] == -1)
           // if (avformat_match_stream_specifier(pFormatCtx, st,wanted_stream_spec[type] ) > 0)
                st_index[type] = i;
	}
	/* open the streams */
	if (st_index[AVMEDIA_TYPE_AUDIO] >= 0) 
	{
		OpenAudioDev();
		LOGE_KK("Debug", "AVMEDIA_TYPE_AUDIO \n");
		stream_component_open(m_pVideoInfo, st_index[AVMEDIA_TYPE_AUDIO]);
	}

	if (st_index[AVMEDIA_TYPE_VIDEO] >= 0) {
		LOGE_KK("Debug", "AVMEDIA_TYPE_VIDEO \n");
		stream_component_open(m_pVideoInfo, st_index[AVMEDIA_TYPE_VIDEO]);
	}

	if (st_index[AVMEDIA_TYPE_SUBTITLE] >= 0) {
		LOGE_KK("Debug", "AVMEDIA_TYPE_SUBTITLE \n");
		stream_component_open(m_pVideoInfo, st_index[AVMEDIA_TYPE_SUBTITLE]);
	}
#ifdef Android_Plat
	if(m_pVideoInfo&&m_pVideoInfo->Hard_Code==SKK_VideoState::HARD_CODE_MEDIACODEC&&m_pViewSurface)
	{
	    m_bSurfaceDisplay=true;
	}
#endif

	if(m_pVideoInfo->iformat==NULL)
	{
		m_pVideoInfo->iformat=m_pVideoInfo->pFormatCtx->iformat;
	}
	m_pVideoInfo->max_frame_duration = (m_pVideoInfo->iformat->flags & AVFMT_TS_DISCONT) ? 10.0 : 3600.0;


	m_TotalTime=m_pVideoInfo->pFormatCtx->duration/1000/1000;
	int AVReadyOk=1;

	if(m_pVideoInfo->realtime&&m_pVideoInfo->audio_st==NULL)
	{
         m_pVideoInfo->max_frame_duration = 2.0;
	}
	
	
	int avsize=0;
	

	if(m_pVideoInfo->video_st==NULL&&m_pVideoInfo->audio_st!=NULL){
		m_pVideoInfo->show_mode=  SKK_VideoState::SHOW_MODE_WAVES;
	}
	

	m_pVideoInfo->SeekVideoSerial=1;
    m_pVideoInfo->SeekAudioSerial=1;
	m_pVideoInfo->IsReady=1;	
	unsigned int bPktContinue=1;

		//4k:4096×2160       -> 34M(RGBA)*20    ---> 713031680
		//8K:7,680 × 4,320   -> 130M(RGBA)*10   --->1,363,148,800
	int K2MAX=2211840;
	int K4Max=15000000;
	int K8MAX=40000000;
	int TotalQueMaxBuf = MAX_QUEUE_SIZE;
	int vwh = 0;
 	if(m_pVideoInfo->viddec.avctx)
 	{
 		vwh = m_pVideoInfo->viddec.avctx->width*m_pVideoInfo->viddec.avctx->height;
 		m_nhasVideoAudio |= EHVA_Video;
 	}
		
	
	int nfirst=1;
	
#ifdef     PLAYER_64
		if(m_pVideoInfo->viddec.avctx)
		{
			
			if(vwh>K2MAX&&vwh<K4Max)
			{
			    TotalQueMaxBuf = 713031680;
				
			}else if(vwh>=K4Max)
			{
			      TotalQueMaxBuf =1363148800;
				
			}

		}
#endif

	int seekhandel = 1;
	while(m_bOpen) 
	{
		if(m_pVideoInfo->abort_request)
		{
			break;
		}
		

		
		int AVQueSize=0;

		/********************实时流媒体不支持暂停******************************/
		if (m_pVideoInfo->paused != m_pVideoInfo->last_paused&&!m_pVideoInfo->realtime) 
		{
			m_pVideoInfo->last_paused = m_pVideoInfo->paused;//
			if (m_pVideoInfo->paused)
			{
				m_pVideoInfo->read_pause_return=1;
				av_read_pause(pFormatCtx);
			}
			else{
				if(m_pVideoInfo->read_pause_return){
				   av_read_play(pFormatCtx);
				   m_pVideoInfo->read_pause_return=0;
				}
			}
		}
		

		if( m_pVideoInfo->paused)
		{
			//需要更多的数据
			if(  !(m_pVideoInfo->paused&EKK_AVP_NeedMore)|| seekhandel==0)
			{
				OnRealSeek();
			    av_usleep(5000);
				continue;
			}
		}

		/******缓存满了*******/
		int CacheCount=0;
		while(!m_pVideoInfo->abort_request)
		{
			OnRealSeek();
			AVQueSize=m_pVideoInfo->audioq.size + m_pVideoInfo->videoq.size + m_pVideoInfo->subtitleq.size;
			bool needwait= AVQueSize> TotalQueMaxBuf ? true:0;
			if( needwait){
				// LOGE_KK("catch full");
				 av_usleep(5000);//等待一会
				 CacheCount++;
				 if(m_pVideoInfo->ioflags == AVFMT_FLAG_CUSTOM_IO&&m_pVideoInfo->pKKPluginInfo!=NULL && CacheCount%200==0){
					AVIOContext *KKIO=pFormatCtx->pb;
					if(KKIO!=NULL){
                         KKPlugin* kk= (KKPlugin*)KKIO->opaque;
						 if(kk->GetCacheTime!=NULL){
                                m_pVideoInfo->nCacheTime=kk->GetCacheTime(kk);
							    if(m_pVideoInfo->nCacheTime>0&&m_pVideoInfo->nCacheTime>m_AVCacheInfo.MaxTime)
								      m_AVCacheInfo.MaxTime=m_pVideoInfo->nCacheTime;
						 }
                    }
					CacheCount =0;
				 }
				 continue;
			}else{
				break;
			}
		}


        if(m_pVideoInfo->nCacheTime>0&&m_pVideoInfo->nCacheTime>m_AVCacheInfo.MaxTime)
			m_AVCacheInfo.MaxTime=m_pVideoInfo->nCacheTime;

		if(m_pVideoInfo->realtime&&m_pVideoInfo->nRealtimeDelayCount>1000){	
			 m_pVideoInfo->OpenTime+=m_pVideoInfo->nRealtimeDelay;
			 m_pVideoInfo->nRealtimeDelayCount=0;
		}

		
		bool forceOver=false;

		

		if(m_nAvEndTime==0 || m_nAvEndTime == AV_NOPTS_VALUE || m_nAvEndTime>  m_nInterCurTime){
			
		     ret = av_read_frame(pFormatCtx, pkt);
			 m_nInterCurTime = m_fCurTime;
			 //视频帧
			 if(m_pVideoInfo->reneed_paused==1&& pkt->stream_index == m_pVideoInfo->video_stream)
			 {
			      if(pkt->flags&AV_PKT_FLAG_KEY)
				  {
					  seekhandel = 0;
				  }
			 }
			 else {
				 seekhandel = 1;
			 }

		}else{
			 forceOver = true;
		     ret = AVERROR_EOF;
			 AVQueSize = -1;
			// m_pVideoInfo->paused=1;
		}
		
       
		
		if (ret < 0) {
			 if(m_pVideoInfo->bTraceAV)
			 LOGE_KK("Debug", "readAV ret=%d \n",ret);

			 if(m_pVideoInfo->abort_request)
				 break;
			 if ((ret == AVERROR_EOF || avio_feof(pFormatCtx->pb)) && !m_pVideoInfo->eof&&!forceOver)
			 {
			        if(m_pVideoInfo->bTraceAV) 
				    LOGE_KK("Debug", "ret == AVERROR_EOF || avio_feof(pFormatCtx->pb)) && !m_pVideoInfo->eof \n");
                    m_pVideoInfo->eof=1;
					EKKPlayerErr err=KKEOF;
					if(m_pVideoInfo->realtime){ 
						m_pVideoInfo->nRealtimeDelay=0;
						m_pVideoInfo->IsReady=0;
						err=KKRealTimeOver;
					}else if(-pFormatCtx->pb->error== ERROR_BROKEN_PIPE){
					    err=KKEOF;
						m_pVideoInfo->abort_request=1;
					}else if (-pFormatCtx->pb->error == ERROR_INVALID_DATA) {
						//文件错误
						m_pVideoInfo->abort_request = 1;
						err = KKFileErr;
						this->OnResetQue();
					}
					m_pPlayUI->OpenMediaStateNotify(m_pVideoInfo->filename,err);

			 }else if (pFormatCtx->pb && pFormatCtx->pb->error&&ret != AVERROR_EOF){
					 m_pVideoInfo->eof=1;
					 m_pVideoInfo->abort_request=1;
					 m_pVideoInfo->nRealtimeDelay=0;
					 if(m_pVideoInfo->bTraceAV)
						 LOGE_KK("Debug", "pFormatCtx->pb && pFormatCtx->pb->error \n");
				     break;
			 }else if(ret == AVERROR_EOF && m_pVideoInfo->eof||forceOver){
    
					if(AVQueSize==0&&(m_pVideoInfo->pictq.size<=1&& m_pVideoInfo->sampq.size<=1)){
						AVQueSize=-1;///文件已经读取完了
					}
					if (this->m_nPlayAvModel == 1)
					{
						if(AVQueSize > -1)
							continue;
						m_nOverNotify = 0;
						forceOver = false;
						m_nInterCurTime = 0;
						
					}
						OnAvOverHandle(AVQueSize);

					if (forceOver) {
						m_pPlayUI->OpenMediaStateNotify(m_pVideoInfo->filename, KKAVOver);
					}
			 }
			av_usleep(10000);
			continue;
		} else {
			m_pVideoInfo->eof = 0;
			if(AVReadyOk==1&&m_pPlayUI!=NULL)
			{
				AVReadyOk=0;
				m_pPlayUI->OpenMediaStateNotify(m_pVideoInfo->filename,KKAVReady);
			}
		}
       
       
		
		/* check if packet is in play range specified by user, then queue, otherwise discard */
		stream_start_time = pFormatCtx->streams[pkt->stream_index]->start_time;
		
		INT64 xx=(INT64)pkt->dts ;
		xx=AV_NOPTS_VALUE;
		pkt_ts = pkt->pts == AV_NOPTS_VALUE ? pkt->dts : pkt->pts;
		
		int64_t  a1=(pkt_ts - (stream_start_time != AV_NOPTS_VALUE ? stream_start_time : 0));
		int64_t a2= av_q2d(pFormatCtx->streams[pkt->stream_index]->time_base);
		pkt_in_play_range = duration == AV_NOPTS_VALUE ||
			a1 * a2-(double)(m_nStartTime != AV_NOPTS_VALUE ? m_nStartTime : 0) / 1000000 <= ((double)duration / 1000000);
		pkt_in_play_range =1;

		
        m_nOverNotify =0;
		if(pkt->stream_index==1)
		{
			int ii = 0;
			ii++;
		}
		//音频
		if (pkt->stream_index == m_pVideoInfo->audio_stream && pkt_in_play_range&&pkt->data!=NULL) {
			packet_queue_put(&m_pVideoInfo->audioq, pkt,m_pVideoInfo->pflush_pkt,m_pVideoInfo->segid);
			
			m_AVCacheInfo.AudioSize=m_pVideoInfo->audioq.PktMemSize;
			if(m_pVideoInfo->audio_st!=NULL)
			{
				pkt_ts=pkt_ts*av_q2d(m_pVideoInfo->audio_st->time_base);
			}
			if(m_AVCacheInfo.MaxTime<pkt_ts)
			{
				m_AVCacheInfo.MaxTime=pkt_ts;
			}
			m_nhasVideoAudio|=EHVA_Audio;
		} else if (pkt->stream_index == m_pVideoInfo->video_stream && pkt_in_play_range&& !(m_pVideoInfo->video_st->disposition & AV_DISPOSITION_ATTACHED_PIC&&pkt->data!=NULL)) 
		{
			
			
			packet_queue_put(&m_pVideoInfo->videoq, pkt,m_pVideoInfo->pflush_pkt,m_pVideoInfo->segid);

			m_AVCacheInfo.VideoSize=m_pVideoInfo->videoq.PktMemSize;
			if(m_pVideoInfo->video_st!=NULL){
				pkt_ts=pkt_ts*av_q2d(m_pVideoInfo->video_st->time_base);
			}
			if(m_AVCacheInfo.MaxTime<pkt_ts)
			{
                 m_AVCacheInfo.MaxTime=pkt_ts;
			}
			m_nhasVideoAudio|=EHVA_Video;
		}else if (pkt->stream_index == m_pVideoInfo->subtitle_stream && pkt_in_play_range&&pkt->data!=NULL) //字幕
		{
			//printf("subtitleq\n");
			packet_queue_put(&m_pVideoInfo->subtitleq, pkt,m_pVideoInfo->pflush_pkt,m_pVideoInfo->segid);
			m_nhasVideoAudio|=EHVA_Subtitle;
		} else
		{
			av_packet_unref(pkt);
		}
		
	}

	av_dict_free(&format_opts);
	m_ReadThreadInfo.Addr=0;
	LOGE_KK("Debug", "readAV Over \n");
	
}
//释放队列数据
void KKPlayer::PacketQueuefree()
{
   if(m_pVideoInfo!=NULL)
   {
	   // LOGE_KK("m_pVideoInfo->videoq \n");
		if(m_pVideoInfo->videoq.pLock->TryLock())
		{
              m_pVideoInfo->videoq.pLock->Unlock();
		}else{
               m_pVideoInfo->videoq.pLock->Unlock();
		}
	    packet_queue_flush(&m_pVideoInfo->videoq);

		//LOGE_KK("m_pVideoInfo->audioq \n");
		if(m_pVideoInfo->audioq.pLock->TryLock())
		{
			m_pVideoInfo->audioq.pLock->Unlock();
		}else{
			m_pVideoInfo->audioq.pLock->Unlock();
		}
		packet_queue_flush(&m_pVideoInfo->audioq);
//
		//LOGE_KK("m_pVideoInfo->subtitleq \n");
		if(m_pVideoInfo->subtitleq.pLock->TryLock())
		{
			m_pVideoInfo->subtitleq.pLock->Unlock();
		}else{
			m_pVideoInfo->subtitleq.pLock->Unlock();
		}
		packet_queue_flush(&m_pVideoInfo->subtitleq);

		delete m_pVideoInfo->videoq.pLock;
		m_pVideoInfo->videoq.pLock=NULL;
		delete m_pVideoInfo->subtitleq.pLock;
		m_pVideoInfo->subtitleq.pLock=NULL;
		delete m_pVideoInfo->audioq.pLock;
		m_pVideoInfo->audioq.pLock=NULL;

		//视频包
		delete m_pVideoInfo->videoq.m_pWaitCond;
		m_pVideoInfo->videoq.m_pWaitCond=0;
		//音频包
		delete m_pVideoInfo->audioq.m_pWaitCond;
		m_pVideoInfo->audioq.m_pWaitCond=0;
		//字幕包
		delete m_pVideoInfo->subtitleq.m_pWaitCond;
		m_pVideoInfo->subtitleq.m_pWaitCond=0;

	//	LOGE_KK("m_pVideoInfo->pictq \n");
        frame_queue_destory(&m_pVideoInfo->pictq);

	//	LOGE_KK("m_pVideoInfo->subpq \n");
		frame_queue_destory(&m_pVideoInfo->subpq);

		//LOGE_KK("m_pVideoInfo->sampq \n");
		frame_queue_destory(&m_pVideoInfo->sampq);
   }
}

void KKPlayer::SetVolume(long value)
{ 
	if(m_pSound!=NULL)
	{
	   m_pSound->SetVolume(value);
	}
}
long KKPlayer::GetVolume()
{
	if(m_pSound!=NULL)
	     return m_pSound->GetVolume();
	return 0;
}
void KKPlayer::Pause(int pa)
{
	CKKGurd gd(this->m_PlayerLock);
	if(m_pVideoInfo!=NULL)
	{
		if (m_pVideoInfo->realtime)
			return;
		if(pa){
		    m_pVideoInfo->paused|=EKK_AVP1;
		}else{

		/*	SKK_Decoder* d = &m_pVideoInfo->viddec;
			IkkRender*  pRender = 0;
			if (m_pPlayUI) {
				pRender = m_pPlayUI->GetRender();
			}
			if (d != 0 && pRender) {
				kkAVPicInfo picinfo;
				memset(&picinfo, 0, sizeof(kkAVPicInfo));

				picinfo.width = d->avctx->width;
				picinfo.height = d->avctx->height;
				picinfo.picformat = d->avctx->pix_fmt;
				pRender->render(&picinfo, 0);
			}*/

		    m_pVideoInfo->reneed_paused=0;
			int ff = NOEKK_AVP1;
			m_pVideoInfo->paused &= ff;
			if(m_pVideoInfo->pKKAudio)
			m_pVideoInfo->pKKAudio->Start();
			

		}
	}
}
void          KKPlayer::ResetPlayIni()
{
    
	 CKKGurd gd(this->m_PlayerLock);
	 m_nNeedReset=1;
}
//快进 相对
int  KKPlayer::KKSeek( SeekEnum en,int value)
{
 if(m_ePlayerState==EPS_ReadingOk&&m_pVideoInfo!=NULL&&!m_pVideoInfo->realtime){
	   
	   m_fCurTime  = m_fCurTime + value;// get_master_clock(m_pVideoInfo) + value;


	   if (m_fCurTime  < 0.000001)
		   m_fCurTime = 0.00;
	   if( m_fCurTime>m_TotalTime)
		   m_fCurTime=m_TotalTime;
	   AVSeek(m_fCurTime,-1);
	
       return 0;
  }
  return -1; 
}

int  KKPlayer::AVSeek(int value,short segid)
{
	
	if(m_ePlayerState==EPS_ReadingOk&&m_pVideoInfo!=NULL&&!m_pVideoInfo->realtime)
    {
	   m_PktSerial=-1;
	   m_nSeekTime=value;
       m_fCurTime  =  value;
	   m_pVideoInfo->SeekAudioSerial++;
	   m_pVideoInfo->SeekVideoSerial++;
	  
	   int lxx = m_pVideoInfo->paused&EKK_AVP1;
	   int lxx2 = m_nhasVideoAudio&EHVA_Video;
	   if ((m_nhasVideoAudio&EHVA_Video) == EHVA_Video&&(m_pVideoInfo->paused&EKK_AVP1)) {
		   m_pVideoInfo->reneed_paused = 1;
		   m_pVideoInfo->paused &= NOEKK_AVP1;

		   if(m_pSound){
		    m_pVideoInfo->nVolume= m_pSound->GetVolume();
		    m_pSound->SetVolume(0);
		   }
	   }
	   return 0;
	}
	return -1; 
}

void KKPlayer::AvflushRealTime(int Avtype)
{
	if(m_pVideoInfo==NULL)
		return;
	if (m_pVideoInfo->video_stream >= 0&&Avtype==1) 
	{
		packet_queue_put(&m_pVideoInfo->videoq, m_pVideoInfo->pflush_pkt,m_pVideoInfo->pflush_pkt,m_pVideoInfo->segid);
	}else if(m_pVideoInfo->audio_st!=NULL&&m_pVideoInfo->audio_stream >= 0&&Avtype==2) {
		packet_queue_put(&m_pVideoInfo->audioq,m_pVideoInfo->pflush_pkt, m_pVideoInfo->pflush_pkt,m_pVideoInfo->segid);
	}else if(m_pVideoInfo->subtitle_stream >= 0&&Avtype==3) {
		packet_queue_put(&m_pVideoInfo->subtitleq, m_pVideoInfo->pflush_pkt,m_pVideoInfo->pflush_pkt,m_pVideoInfo->segid);
	}
}

/******设置实时流媒体最小延迟**********/
int  KKPlayer::SetMaxRealtimeDelay(double Delay)
{
     if(m_pVideoInfo!=NULL&&Delay>=1.0)
	 {
		 m_pVideoInfo->nMaxRealtimeDelay=Delay;
		 return 1;
	 }
	 return 0;
}
void KKPlayer::Avflush(int64_t seek_target)
{
	if (m_pVideoInfo->video_stream >= 0) 
	{
		packet_queue_put(&m_pVideoInfo->videoq, m_pVideoInfo->pflush_pkt,m_pVideoInfo->pflush_pkt,m_pVideoInfo->segid);
		m_pVideoInfo->pictq.mutex->Lock();
		m_pVideoInfo->pictq.size=0;
		m_pVideoInfo->pictq.rindex=0;
		m_pVideoInfo->pictq.windex=0;
		m_pVideoInfo->pictq.rindex_shown=1;

		m_pVideoInfo->pictq.m_pWaitCond->SetCond();
		m_pVideoInfo->pictq.mutex->Unlock();
	}

	if (m_pVideoInfo->audio_st!=NULL&&m_pVideoInfo->audio_stream >= 0) 
	{
		packet_queue_put(&m_pVideoInfo->audioq,m_pVideoInfo->pflush_pkt, m_pVideoInfo->pflush_pkt,m_pVideoInfo->segid);
		m_pVideoInfo->sampq.mutex->Lock();
		m_pVideoInfo->sampq.size=0;
		m_pVideoInfo->sampq.rindex=0;
		m_pVideoInfo->sampq.windex=0;
		m_pVideoInfo->sampq.rindex_shown=1;
		m_pVideoInfo->sampq.m_pWaitCond->SetCond();
		m_pVideoInfo->sampq.mutex->Unlock();/**/
	}
	if (m_pVideoInfo->subtitle_stream >= 0) 
	{
		packet_queue_put(&m_pVideoInfo->subtitleq, m_pVideoInfo->pflush_pkt,m_pVideoInfo->pflush_pkt,m_pVideoInfo->segid);
	}
		if (m_pVideoInfo->seek_flags & AVSEEK_FLAG_BYTE) 
		{
			set_clock(&m_pVideoInfo->extclk, NAN, 0);
		} else 
		{
			set_clock(&m_pVideoInfo->extclk, seek_target / (double)AV_TIME_BASE, 0);
		}
}