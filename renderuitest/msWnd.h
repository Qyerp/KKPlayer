#ifndef msui_msWnd_H_
#define msui_msWnd_H_
#include "UIWnd.h"
namespace KKUI
{
	class CMsWnd: public CUIWnd
	{
	public:
		   CMsWnd(KKUIHandle    pUIRoot);
		   ~CMsWnd();
             void OnSize(UINT nType, CSize size);
			 void IniCtl();
			 int OnCreate(LPCREATESTRUCT lpCreateStruct);
		     BEGIN_MSG_MAP_EX(CMsWnd)
				        MSG_WM_CREATE  ( OnCreate) 
                      MSG_WM_SIZE    (OnSize)
					  CHAIN_MSG_MAP(CUIWnd)
				  END_MSG_MAPX()
	};
}
#endif