#ifndef  KKUI_UISliceEditor_H_
#define  KKUI_UISliceEditor_H_
#include "UIWindow.h"
#include "UIYUV.h"
namespace KKUI
{
    //切片最多支持4个顶点
	class CUISliceEditor: public CUIWindow
	{
	    KKUI_CLASS_NAME(CUIWindow,CUISliceEditor,"uisliceeditor")
	    public:
				  CUISliceEditor();
				  ~CUISliceEditor();
				  
				  	CUIWindow*  WndFromPoint   (kkPoint  ptHitTest, int bOnlyText);
					int         IsContainPoint (const kkPoint &pt,int bClientOnly);
					int         OnNcHitTest    (kkPoint pt);
					void        SetUIYUV       (CUIYUV*  pUIYUV);
	    protected:
			        //点在那个顶点上
			       bool PtInVert(CPoint pt,int& Out);
			       void GetRectByPt(const CPoint& pt,CRect& rt);
				   void AdjustVertPt(int x,int y,int index,int init=0);
				   void OnPaint( IRenderTarget  *pRT);
				   void OnEraseBkgnd( IRenderTarget  *pRT);
				   int  OnRelayout(const CRect &rcWnd); 
				   
				   void OnLButtonDown(unsigned int nFlags,CPoint pt);
				   void OnLButtonUp(unsigned int nFlags,CPoint pt);
				   void OnRButtonUp(unsigned int nFlags,CPoint pt);
				   void OnMouseMove(unsigned int nFlags,CPoint pt);
				   void OnMouseLeave(unsigned int nFlags);

				   void OnNcLButtonDown(unsigned int nFlags,CPoint pt);
			       void OnNcLButtonUp(unsigned int nFlags,CPoint pt);

				   KKUI_ATTRS_BEGIN()
							//边框颜色
							ATTR_COLOR("cr", m_crBorderCapture, 1)
				   	        ATTR_INT("alloweditor", m_nAllowEditor,1)
				   KKUI_ATTRS_ENDEX(CUIWindow)

				   KKUI_MSG_MAP_BEGIN()   
						MSG_UI_SIZE         (OnSize)
						MSG_UI_LBUTTONDOWN  (OnLButtonDown)
	                    MSG_UI_LBUTTONUP    (OnLButtonUp)
						MSG_UI_RBUTTONUP    (OnRButtonUp) 
						MSG_UI_MOUSEMOVE    (OnMouseMove)
						MSG_UI_MOUSELEAVE   (OnMouseLeave)
	                    MSG_UI_NCLBUTTONDOWN(OnNcLButtonDown)
				        MSG_UI_NCLBUTTONUP  (OnNcLButtonUp)
                         MSG_UI_NCMOUSEMOVE(OnMouseMove)
						MSG_UI_PAINT        (OnPaint)
						MSG_UI_ERASEBKGND   (OnEraseBkgnd)
					   
				   KKUI_MSG_MAP_END(CUIWindow)
	    private:
			        CPoint                      m_PtS[4]; 
					CPoint                      m_LastPt;
					int                         m_nAllowEditor;
					int                         m_bDraging;
					int                         m_bVertIndex;
					CUIYUV*                     m_pUIYUV;
					COLORREF                    m_crBorderCapture;
	};
}
#endif