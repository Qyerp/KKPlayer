#ifndef UICurve_H_
#define UICurve_H_
#include "UIWindow.h"
#include <vector>
//曲线
namespace KKUI
{
	
    class  CUICurve: public CUIWindow
    {
        KKUI_CLASS_NAME( CUIWindow,CUICurve,"uicurve")
		public:
				 CUICurve(void);
				 ~CUICurve(void);

				 void  GetScale( SUVInfo& fUv1,SUVInfo& fUv2);
				 void  SetScale( SUVInfo fUv1,SUVInfo fUv2);
		protected:
				 int            OnCreate( int);
				 void           OnPaint( IRenderTarget  *pRT);
				 void           OnEraseBkgnd( IRenderTarget  *pRT);
				 void           OnSize(unsigned int nType, int x,int y);


				 void           OnLButtonDown  (unsigned int nFlags,CPoint pt);
				 void           OnLButtonUp    (UINT nFlags, CPoint point);
				 void           OnMouseMove    (unsigned int nFlags,CPoint pt);
				 KKUI_MSG_MAP_BEGIN()
				         MSG_UI_CREATE(OnCreate)
						 MSG_UI_PAINT(OnPaint)
						 MSG_UI_ERASEBKGND(OnEraseBkgnd)
						 MSG_UI_SIZE(OnSize)

						 MSG_UI_LBUTTONDOWN  (OnLButtonDown)
						 MSG_UI_LBUTTONUP    (OnLButtonUp)
						 MSG_UI_MOUSEMOVE    (OnMouseMove)
				 KKUI_MSG_MAP_END(CUIWindow)
	     private:
			   void             InitCtlInfo();
               void             CalScale();
			   //是否在拖动中
			   int              m_bDraging;
			   //第一个控制点
			   SUVInfo          m_FirstCtrlPt;
			   SUVInfo          m_SecondCtrlPt;
			   
			   //比例
			   SUVInfo          m_FirstCtrl1;
			   SUVInfo          m_SecondCtrl2;

			   SUVInfo          m_Uv1;
			   SUVInfo          m_Uv2;
			 
    };

}

#endif