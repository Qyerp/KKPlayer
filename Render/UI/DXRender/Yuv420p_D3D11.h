#ifndef Yuv420p_D3D11_H_
#define Yuv420p_D3D11_H_
#include <map>
#include <string>
#include <d3d11.h>
#include "ObjRef.h"
#include "Core/ObjectImp.h"
#include "Core/IRender.h"

namespace KKUI
{
	class SRenderFactory_D3D11;
	class CYuv420p_D3D11:public TObjRefImpl<IPicYuv420p>
	{
	        public:
				    CYuv420p_D3D11	(SRenderFactory_D3D11* pRenderFactory);
					~CYuv420p_D3D11();
                    void                   Release2();
					void                   ReLoadRes();
					kkSize                 Size() const;
					
					
					void                   ClearPicData(unsigned int Color);
					//设置引用
					void                   SetQuoteObj(IPicYuv420p*    pParentObj);
                    int	                   Load(const kkPicInfo* data);
                 
					
					ID3D11Texture2D*            GetTexture();
					ID3D11ShaderResourceView*   GetShaderView() { return m_pSRView; }
					ID3D11Texture2D*            LoadToClipTexture(SUVInfo xy1,SUVInfo xy2,SUVInfo xy3,SUVInfo xy4,int clipwidth,int clipheight);

					void                        SavePreFrame(bool save){}
					int                         HasPreFrame(){return 0;}
					void                        CreatePicCache(int width,int height,int format){};
					//渲染到纹理，由渲染线程调用，目前只支持但线程渲染，多线程有待研究
					void                   RenderToTex();
	        private:
				    int                    UpdateVertexBuffer(const void * vertexData, size_t dataSizeInBytes);
				    int                    LoadDxTexture(const kkPicInfo* data);
				    int                    LoadDxva2    (const kkPicInfo* data);
				    int                    LoadYuv      (const kkPicInfo* data);
					int                    LoadARGB     (const kkPicInfo* data);
					int                    LoadRGBA     (const kkPicInfo* data);

					//采样器
					ID3D11SamplerState*        m_pSamplerState;
					//定点这色器常量
					ID3D11Buffer*             m_pVsShaderConstants;
					ID3D11Buffer*             m_pVertexBuffer;
				    //引用对象
				    CYuv420p_D3D11*           m_pQuoteObj;	
				   
					 //呈现的纹理
				   ID3D11RenderTargetView*    m_pRTView;
				   ID3D11ShaderResourceView*  m_pSRView;
				   ID3D11DepthStencilView*	  m_pDSV;	// 输出纹理所用的深度/模板视图
				   ID3D11Texture2D*           m_pRenderTexture;

				   //用于渲染RGBA
				   ID3D11Texture2D*        m_pARGBTexture;
				   kkSize                  m_Size;

				   int	                   LoadData(const kkPicInfo* data);
				   SRenderFactory_D3D11*    m_pRenderFactory;
				
				   //用于渲染Yuv 
				   ID3D11Texture2D*           m_pYTexture;
				   ID3D11ShaderResourceView*  m_pYSRView;
				   ID3D11Texture2D*           m_pUTexture;
				   ID3D11ShaderResourceView*  m_pUSRView;
				   ID3D11Texture2D*           m_pVTexture;
				   ID3D11ShaderResourceView*  m_pVSRView;

				

				  //剪切的纹理
				   ID3D11Texture2D*           m_pClipTexture;
				   kkSize                     m_ClipSize;
				   int                        m_nPreOk;
	};
}
#endif