#include "UIMsView.h"
#include "UIYUVGroup.h"
namespace KKUI
{

	void GetScalRect(CRect& rt,const float& f)
	{
		rt.left =rt.left /f;
		rt.top =rt.top /f;
		rt.right =rt.right /f;
		rt.bottom =rt.bottom /f;
	}

	void GetScalRect(kkRect& rt,const float& f)
	{
		rt.left =rt.left /f;
		rt.top =rt.top /f;
		rt.right =rt.right /f;
		rt.bottom =rt.bottom /f;
	}

	void InitMagnetRegion(SMagnetRegion& Mgegion)
	{
	    Mgegion.LeftTop     = EMagnetState::EMS_NO;
	    Mgegion.RightTop    = EMagnetState::EMS_NO;

	    Mgegion.LeftBottom  = EMagnetState::EMS_NO;
	    Mgegion.RightBottom = EMagnetState::EMS_NO;
	}


	CUIMsView::CUIMsView():m_nStageCount(0),
		m_pCurActiveYUV(0),
		m_fDisplayScale(4),
		m_pFirstGroup(0),
	    m_pLastGroup(0),
		m_nGroupCount(0),
		m_nEditorModel(0),
		m_pActiveGroup(0),
		m_pCurActiveStage(0)
	{
		
	}
	CUIMsView::~CUIMsView()
	{
	  int i=0;
	  i++;
	}

	void  CUIMsView::InsertChild(CUIWindow *pNewChild,CUIWindow *pInsertAfter)
	{
		char* classnmae = pNewChild->GetObjectClass();
		if (strcmp(classnmae, "uimovestate") == 0) {
			CUIScrollView::InsertChild(pNewChild, pInsertAfter);
			m_nStageCount++;
			m_StageVec.push_back((CUIMoveState*)pNewChild);
		}
		else {
			CUIScrollView::InsertChild(pNewChild,  UICWND_FIRST);
		
		}
		
	}
	 bool     CUIMsView::DestroyChild(CUIWindow  *pChild)
	 {
            DeleteStage((CUIMoveState*)pChild);
		    std::vector<CUIMoveState*>::iterator It=  m_StageVec.begin();
			for(;It!= m_StageVec.end();++It){
					  CUIMoveState* Stage=*It;
					  Stage->DelIntersectCtl(pChild);
			}
		   return CUIScrollView::DestroyChild(pChild);
	}
	CUIYUVGroup*  CUIMsView::CreateYuvGroup()
	{
		    CUIYUVGroup* group=new CUIYUVGroup(this);
	       if( m_pFirstGroup==0)
		   {
		        m_pFirstGroup=m_pLastGroup = group;
		   }else{
		        
				m_pLastGroup->SetPrevNextGroup(group);
				group->SetPrevNextGroup(m_pLastGroup,true);
				m_pLastGroup = group;
		   }	 
           m_nGroupCount++;

           return group;
	}
	int      CUIMsView::DelYuvGroup(CUIYUVGroup* group)
	{
	        CUIYUVGroup *pGroup= m_pFirstGroup;
			while(pGroup)
			{
				if(pGroup==group)
				{
				   
					CUIYUVGroup *pPrevSib  = pGroup->GetPrevGroup();
					CUIYUVGroup *pNextSib  = pGroup->GetNextGroup();

					if(pPrevSib) 
						pPrevSib->SetPrevNextGroup(pNextSib);
				    else 
					    m_pFirstGroup=pNextSib;

					if(pNextSib) 
						pNextSib->SetPrevNextGroup(pPrevSib,true);
					else 
						m_pLastGroup=pPrevSib;

					if(m_pActiveGroup==pGroup)
						m_pActiveGroup = 0;
					delete pGroup;
					m_nGroupCount--;
				    return 1;
				}
				pGroup=pGroup->GetNextGroup();
			}
			 return 0;
	}
	void     CUIMsView::InsertYuvByGroup      (void* Group,CUIYUV* yuv,int indexId)
	{
	     InsertChild(yuv,0);
		 yuv->InitAfter();
         CUIYUVGroup *pGroup= ( CUIYUVGroup *)Group;
         pGroup->InsertYuv(yuv,indexId);
		  RrefreshStageCtls();
	}
	int      CUIMsView::SetGroupVisible (void* pGroup, int visible)
	{
	     CUIYUVGroup *YUVGroup= ( CUIYUVGroup *)pGroup;
		 if(visible){
			  m_pActiveGroup = YUVGroup;
              RrefreshStageCtls();
			}
		 ReleaseCapture();
		 return  YUVGroup->SetVisible(visible);
	}
	void       CUIMsView::DeleteStage(CUIMoveState *Child)
	{
	      std::vector<CUIMoveState*>::iterator It=  m_StageVec.begin();
			for(;It!= m_StageVec.end();++It)
			{
					  CUIMoveState* Stage=*It;
					  if(Stage==Child){
						  m_StageVec.erase(It);
						  break;
					  }

			}
	}
	void  CUIMsView::OnSize(unsigned int nType, int x,int y)
	 {
		    CSize size;
		    size.cx = x;
		    size.cy = y;
        
			
			CUIScrollView::OnSize(nType,x,y);
			UpdateScrollBar();
			UpdateChildrenPosition();
			
	}

	void   CUIMsView::OnEraseBkgnd( IRenderTarget  *pRT)
	{
		   //CUIWindow::OnEraseBkgnd(pRT);
		    CRect WindowRect;
			this->GetWindowRect(&WindowRect);
				pRT->FillRect(&WindowRect,0xFF000000);
	}
	void   CUIMsView::OnNcPaint(IRenderTarget *pRT)
	{
		    wchar_t TempStr[64]=L"";
		    CRect WindowRect;
			this->GetWindowRect(&WindowRect);
			CRect rcDest = WindowRect;

			//pRT->FillRect(&rcDest,0xFF000000);

			if(this->GetContainer()->GetIRenderFactory())
			{
				if(m_pFont==0){
					this->GetContainer()->GetIRenderFactory()->CreateFont(&m_pFont,12,L"宋体");
				}
				unsigned int RuleColor = 0xFF8B8B8B;

				int kedu=0;
				//X轴
				pRT-> DrawLine(WindowRect.left,WindowRect.top,WindowRect.right,WindowRect.top,RuleColor+1);
				//画大刻度
				for (int degree = WindowRect.left; degree <= WindowRect.right; degree += 100)
				{
					pRT-> DrawLine(degree,WindowRect.top+1,degree,WindowRect.top+15,RuleColor);

					if(HasScrollBar(1))
					{
						kedu = (degree+m_siHoz.nPos) *m_fDisplayScale;
					    swprintf_s(TempStr,64,L"%d",kedu);
					}else{
						kedu = degree *m_fDisplayScale;
					    swprintf_s(TempStr,64,L"%d",kedu);
					}

			
					rcDest.left = degree+2;
					rcDest.top = WindowRect.top+5;
					pRT->DrawText(m_pFont,TempStr,&rcDest,RuleColor);
				}

				//画中刻度
				for (int degree = WindowRect.left+50; degree <= WindowRect.right; degree += 50)
				{
					pRT-> DrawLine(degree,WindowRect.top+1,degree,WindowRect.top+10,RuleColor);
				}

				//画小刻度
				for (int degree = WindowRect.left+10; degree <= WindowRect.right; degree += 10)
				{
					pRT-> DrawLine(degree,WindowRect.top+1,degree,WindowRect.top+5,RuleColor);
				}

				//Y轴
				pRT-> DrawLine(WindowRect.left,WindowRect.top,WindowRect.left,WindowRect.bottom,RuleColor);
				//画大刻度
				for (int degree = WindowRect.top; degree <= WindowRect.bottom; degree += 100)
				{
					pRT-> DrawLine(WindowRect.left,degree,WindowRect.left+15,degree,RuleColor);

					if(HasScrollBar(1))
					{
						kedu = (degree+m_siVer.nPos) *m_fDisplayScale;
					    swprintf_s(TempStr,64,L"%d",kedu);
					}else{
						kedu = degree *m_fDisplayScale;
					    swprintf_s(TempStr,64,L"%d",kedu);
					}
					if( degree != WindowRect.top)
					{
					    rcDest.top = degree;
						rcDest.bottom = rcDest.top+100;
					    rcDest.left = WindowRect.left+10;
						rcDest.right = rcDest.left+10;
				    	pRT->DrawTextV(m_pFont,TempStr,&rcDest,RuleColor);
					}
				}

				//画中刻度
				for (int degree = WindowRect.top; degree <= WindowRect.bottom; degree += 50)
				{
					pRT-> DrawLine(WindowRect.left,degree,WindowRect.left+10,degree,RuleColor);
				}

				//画小刻度
				for (int degree = WindowRect.top; degree <= WindowRect.bottom; degree += 10)
				{
					pRT-> DrawLine(WindowRect.left,degree,WindowRect.left+5,degree,RuleColor);
				}
			}
	     CUIScrollView::OnNcPaint(pRT);
	}

	void   CUIMsView::_PaintRegion(IRenderTarget *pRT,unsigned int iZorderBegin,unsigned int iZorderEnd)
	{
	    if(!IsVisible(0))  //只在自己完全可见的情况下才绘制
			return;
		if(m_uZorder >= iZorderBegin
			&& m_uZorder < iZorderEnd )
		{
			_PaintClient(pRT);
		}
        CUIWindow *pActuvwChild = 0;
		if(m_nEditorModel ==1|| m_nEditorModel ==2)
		{
		  //舞台渲染
			PaintLayer(pRT,&pActuvwChild ,iZorderBegin,iZorderEnd);
			PaintStage(pRT,&pActuvwChild ,iZorderBegin,iZorderEnd);
		}else {
			//图层渲染
		    PaintStage(pRT,&pActuvwChild ,iZorderBegin,iZorderEnd);
			PaintLayer(pRT,&pActuvwChild ,iZorderBegin,iZorderEnd);
			
		}

		if(pActuvwChild){
			if(pActuvwChild->IsVisible(0))
		      pActuvwChild->SendMessage(UI_NCPAINT, (uint_ptr)pRT);
		}

		SendMessage(UI_NCPAINT, (uint_ptr)pRT);
	}

	void    CUIMsView::PaintStage(IRenderTarget *pRT,CUIWindow **pActuvwChild ,unsigned int iZorderBegin,unsigned int iZorderEnd)
	{
	
	    std::vector<CUIMoveState*>::iterator It = m_StageVec.begin();
		for(;It!= m_StageVec.end();++It)
		{
		        CUIWindow  *pChild=*It;
				if(m_pCurActiveYUV == pChild)
				{
				   *pActuvwChild =pChild;
				}

				if(pChild->GetZorder() >= iZorderEnd) 
					break;

				pChild->_PaintRegion(pRT,iZorderBegin,iZorderEnd);
		}
	}
	void   CUIMsView::PaintLayer(IRenderTarget *pRT,CUIWindow **pActuvwChild ,unsigned int iZorderBegin,unsigned int iZorderEnd)
	{
	    if(	m_pActiveGroup&&	m_pActiveGroup->GetVisible())
		{
		    
			std::vector<CUIYUV*> YuvVector =m_pActiveGroup-> GetYuvVector();
		    std::vector<CUIYUV*>::iterator It= YuvVector.begin();
	   		for(;It!= YuvVector.end(); It++)
			{
					CUIWindow* pChild=*It;
				  
					if(m_pCurActiveYUV == pChild)
					{
					   *pActuvwChild =pChild;
					}

					if(pChild->GetZorder() >= iZorderEnd) 
						break;
					if(pChild->IsVisible(0))
					pChild->_PaintRegion(pRT,iZorderBegin,iZorderEnd);
			}

		}
	}
	void   CUIMsView::OnMouseLeave(unsigned int nFlags)
	{
	       	m_dragSb=DSB_NULL;
	}
	int    CUIMsView::OnDisplayScale( SStringA strValue,BOOL bLoading )
	{
	
		m_fDisplayScale = atof(strValue.GetBuffer(64));
       
		OnAdjustDisplayScale();
      
		return bLoading?S_FALSE:S_OK;
	}
	void      CUIMsView::SetDisplayScale (float Scale)
	{
	    m_fDisplayScale = Scale;
		OnAdjustDisplayScale();
	}
	
	int      CUIMsView::CheckMagnetisRect(CUIWindow* desctl ,CPoint* pts,int ptCCont,SMagnetRegion& Mgegion)
	{

		InitMagnetRegion(Mgegion);

		CPoint& SrcLeftTop     =*pts;
		
		CPoint& SrcRightTop    = *(pts+1);
	

		CPoint& SrcLeftBottom  = *(pts+3);
		
		CPoint& SrcRightBottom = *(pts+2);
		

		int Ok = 0;

		/*******优先遍历舞台控件********/
	    std::vector<CUIMoveState*>::iterator It = m_StageVec.begin();
		for(;It!= m_StageVec.end();++It)
		{
		        CUIMoveState* pStage=*It;
				if(pStage!=desctl)
				{
					CRect WinRt;
					pStage->GetWindowRect(&WinRt);

					CPoint desPts[4];
					desPts[0].y = WinRt.top;
					desPts[0].x = WinRt.left;

					desPts[1].y = WinRt.top;
					desPts[1].x = WinRt.right;


					desPts[3].y = WinRt.bottom;
					desPts[3].x = WinRt.left;

					desPts[2].y = WinRt.bottom;
					desPts[2].x = WinRt.right;

					ClcMagnetisRect(pts,desPts, Mgegion);
				}
		}

		
		if( Mgegion.LeftTop     == EMagnetState::EMS_LEFT_TOP     || Mgegion.LeftBottom == EMagnetState::EMS_LEFT_BOTTOM ||
		    Mgegion.RightBottom == EMagnetState::EMS_RIGHT_BOTTOM || Mgegion.RightTop == EMagnetState::EMS_RIGHT_TOP
			)
		{
		     return 1;
		}

		/**********图层控件************/
		std::vector<CUIYUV*> yuvVector = m_pActiveGroup->GetYuvVector();
		std::vector<CUIYUV*>::iterator ItYuv = yuvVector.begin();
		for(;ItYuv != yuvVector.end();ItYuv++){
		    CUIYUV* yuv=*ItYuv;
			if(yuv!=desctl){

				CPoint desPts[4];
                yuv->GetRegion(desPts,4);
				ClcMagnetisRect(pts,desPts, Mgegion);
			}
		}
		if( Mgegion.LeftTop     == EMagnetState::EMS_LEFT_TOP     || Mgegion.LeftBottom == EMagnetState::EMS_LEFT_BOTTOM ||
		    Mgegion.RightBottom == EMagnetState::EMS_RIGHT_BOTTOM || Mgegion.RightTop == EMagnetState::EMS_RIGHT_TOP
			)
		{
		  return 1;
		}

		return  0;
	}
	//计算磁吸
	void     CUIMsView::ClcMagnetisRect(CPoint* pts,CPoint* DestPts,SMagnetRegion& Mgegion)
	{
	    CPoint& SrcLeftTop     = *pts;
		CPoint& SrcRightTop    = *(pts+1);
        CPoint& SrcRightBottom = *(pts+2);
		CPoint& SrcLeftBottom  = *(pts+3);
		

		CPoint& LeftTop = *DestPts;
		CPoint& RightTop=*(DestPts+1);
		CPoint& RightBottom=*(DestPts+2);	
		CPoint& LeftBottom=*(DestPts+3);

		int pxRange=10;
				/***************消炎lefttop*************/
				if (abs(SrcLeftTop.x - LeftTop.x)<pxRange && abs(SrcLeftTop.y - LeftTop.y) <pxRange) {
					
					int offx = LeftTop.x - SrcLeftTop.x;
					int offy = LeftTop.y - SrcLeftTop.y;
					SrcLeftTop.x = LeftTop.x;
					SrcLeftTop.y = LeftTop.y;

					SrcRightTop.x = SrcRightTop.x+offx;
					SrcRightTop.y = SrcRightTop.y+offy;


					SrcLeftBottom.y = SrcLeftBottom.y + offy;
					SrcLeftBottom.x = SrcLeftBottom.x + offx;

					SrcRightBottom.y = SrcRightBottom.y +offy;
					SrcRightBottom.x = SrcRightBottom.x +offx;
                    
					Mgegion.LeftTop = EMagnetState::EMS_LEFT_TOP;
				}else if (abs(SrcLeftTop.x - LeftBottom.x)< pxRange && abs(SrcLeftTop.y - LeftBottom.y) <pxRange) {
					
					int offx      = LeftBottom.x- SrcLeftTop.x;
					int offy      = LeftBottom.y - SrcLeftTop.y;
					SrcLeftTop.x  = LeftBottom.x;
					SrcLeftTop.y  = LeftBottom.y;

					SrcRightTop.x = SrcRightTop.x+offx;
					SrcRightTop.y = SrcRightTop.y+offy;


					SrcLeftBottom.y = SrcLeftBottom.y + offy;
					SrcLeftBottom.x = SrcLeftBottom.x + offx;

					SrcRightBottom.y = SrcRightBottom.y + offy;
					SrcRightBottom.x = SrcRightBottom.x + offx;


					Mgegion.LeftTop = EMagnetState::EMS_LEFT_TOP;
				}else if( abs(SrcLeftTop.x - RightTop.x)<pxRange &&abs(SrcLeftTop.y - RightTop.y) < pxRange ){
					
					int offx      =  RightTop.x - SrcLeftTop.x;
					int offy      =  RightTop.y - SrcLeftTop.y;
					SrcLeftTop.x  =  RightTop.x;
					SrcLeftTop.y  =  RightTop.y;

					SrcRightTop.x = SrcRightTop.x + offx;
					SrcRightTop.y = SrcRightTop.y + offy;


					SrcLeftBottom.y = SrcLeftBottom.y + offy;
					SrcLeftBottom.x = SrcLeftBottom.x + offx;

					SrcRightBottom.y = SrcRightBottom.y + offy;
					SrcRightBottom.x = SrcRightBottom.x + offx;


					Mgegion.LeftTop=EMagnetState::EMS_LEFT_TOP;
				}else if( abs(SrcLeftTop.x - RightBottom.x)< pxRange &&abs(SrcLeftTop.y - RightBottom.y) < pxRange ){
					
					int offx      = RightBottom.x - SrcLeftTop.x;
					int offy      = RightBottom.y - SrcLeftTop.y;
					
					SrcLeftTop.x  = RightBottom.x;
					SrcLeftTop.y  = RightBottom.y;

					SrcRightTop.x = SrcRightTop.x + offx;
					SrcRightTop.y = SrcRightTop.y + offy;


					SrcLeftBottom.y  = SrcLeftBottom.y + offy;
					SrcLeftBottom.x  = SrcLeftBottom.x + offx;

					SrcRightBottom.y = SrcRightBottom.y + offy;
					SrcRightBottom.x = SrcRightBottom.x + offx;


					Mgegion.LeftTop =EMagnetState::EMS_LEFT_TOP;
				}
				/***********效验left Bottom***********/
				else if (abs(SrcLeftBottom.x - LeftTop.x)<pxRange && abs(SrcLeftBottom.y - LeftTop.y) < pxRange ) {
					
					
					int offx      = LeftTop.x - SrcLeftBottom.x;
					int offy      = LeftTop.y - SrcLeftBottom.y;

					SrcLeftBottom.y = LeftTop.x;
					SrcLeftBottom.x = LeftTop.y;

					SrcLeftTop.x  = SrcLeftTop.x + offx;
					SrcLeftTop.y  = SrcLeftTop.y + offy;

					SrcRightTop.x    = SrcRightTop.x + offx;
					SrcRightTop.y    = SrcRightTop.y + offy;

					SrcRightBottom.y = SrcRightBottom.y + offy;
					SrcRightBottom.x = SrcRightBottom.x + offx;

					Mgegion.LeftBottom = EMagnetState::EMS_LEFT_BOTTOM;
				}else if (abs(SrcLeftBottom.x - LeftBottom.x)<pxRange && abs(SrcLeftBottom.y - LeftBottom.y) <pxRange) {
					
					int offx      = LeftBottom.x - SrcLeftBottom.x;
					int offy      = LeftBottom.y - SrcLeftBottom.y;

					SrcLeftBottom.y = LeftBottom.x;
					SrcLeftBottom.x = LeftBottom.x;

					SrcLeftTop.x  = SrcLeftTop.x + offx;
					SrcLeftTop.y  = SrcLeftTop.y + offy;

					SrcRightTop.x    = SrcRightTop.x + offx;
					SrcRightTop.y    = SrcRightTop.y + offy;

					SrcRightBottom.y = SrcRightBottom.y + offy;
					SrcRightBottom.x = SrcRightBottom.x + offx;

					Mgegion.LeftBottom = EMagnetState::EMS_LEFT_BOTTOM;
				}else if( abs(SrcLeftBottom.x - RightTop.x)<pxRange &&abs(SrcLeftBottom.y - RightTop.y) <pxRange ){
					
					int offx      = RightTop.x - SrcLeftBottom.x;
					int offy      = RightTop.y - SrcLeftBottom.y;

					SrcLeftBottom.y = RightTop.x;
					SrcLeftBottom.x = RightTop.x;

					SrcLeftTop.x  = SrcLeftTop.x + offx;
					SrcLeftTop.y  = SrcLeftTop.y + offy;

					SrcRightTop.x    = SrcRightTop.x + offx;
					SrcRightTop.y    = SrcRightTop.y + offy;

					SrcRightBottom.y = SrcRightBottom.y + offy;
					SrcRightBottom.x = SrcRightBottom.x + offx;


					Mgegion.LeftBottom = EMagnetState::EMS_LEFT_BOTTOM;
				}else if( abs(SrcLeftBottom.x - RightBottom.x)< pxRange &&abs(SrcLeftBottom.y-RightBottom.y) <pxRange ){

                    int offx      = RightBottom.x - SrcLeftBottom.x;
					int offy      = RightBottom.y- SrcLeftBottom.y;

					SrcLeftBottom.y = RightBottom.y;
					SrcLeftBottom.x = RightBottom.x;

					SrcLeftTop.x  = SrcLeftTop.x + offx;
					SrcLeftTop.y  = SrcLeftTop.y + offy;

					SrcRightTop.x    = SrcRightTop.x + offx;
					SrcRightTop.y    = SrcRightTop.y + offy;

					SrcRightBottom.y = SrcRightBottom.y + offy;
					SrcRightBottom.x = SrcRightBottom.x + offx;

					Mgegion.LeftBottom = EMagnetState::EMS_LEFT_BOTTOM;
				}
				/***********效验right Top*************/
				else if (abs(SrcRightTop.x - LeftTop.x)<pxRange && abs(SrcRightTop.y - LeftTop.y) < pxRange ) {
			
					int offx      = LeftTop.x - SrcRightTop.x;
					int offy      = LeftTop.y- SrcRightTop.y;

					SrcRightTop.x   = LeftTop.x;
					SrcRightTop.y   = LeftTop.y;

					SrcLeftBottom.y = SrcLeftBottom.y + offx;
					SrcLeftBottom.x = SrcLeftBottom.x + offy;

					SrcLeftTop.x    = SrcLeftTop.x + offx;
					SrcLeftTop.y    = SrcLeftTop.y + offy;

					SrcRightBottom.y = SrcRightBottom.y + offy;
					SrcRightBottom.x = SrcRightBottom.x + offx;

					Mgegion.RightTop  = EMagnetState::EMS_RIGHT_TOP;
				}else if (abs(SrcRightTop.x - LeftBottom.x)<pxRange && abs(SrcRightTop.y - LeftBottom.y) <pxRange) {
					
					int offx      = LeftBottom.x - SrcRightTop.x;
					int offy      = LeftBottom.y- SrcRightTop.y;

					SrcRightTop.x   = LeftBottom.x;
					SrcRightTop.y   = LeftBottom.y;

					SrcLeftBottom.y = SrcLeftBottom.y + offx;
					SrcLeftBottom.x = SrcLeftBottom.x + offy;

					SrcLeftTop.x    = SrcLeftTop.x + offx;
					SrcLeftTop.y    = SrcLeftTop.y + offy;

					SrcRightBottom.y = SrcRightBottom.y + offy;
					SrcRightBottom.x = SrcRightBottom.x + offx;

					Mgegion.RightTop = EMagnetState::EMS_RIGHT_TOP;
				}else if( abs(SrcRightTop.x - RightTop.x)<pxRange &&abs(SrcRightTop.y - RightTop.y) <pxRange ){

					int offx      = RightTop.x - SrcRightTop.x;
					int offy      = RightTop.y - SrcRightTop.y;

					SrcRightTop.x   = RightTop.x;
					SrcRightTop.y   = RightTop.y;

					SrcLeftBottom.y = SrcLeftBottom.y + offx;
					SrcLeftBottom.x = SrcLeftBottom.x + offy;

					SrcLeftTop.x    = SrcLeftTop.x + offx;
					SrcLeftTop.y    = SrcLeftTop.y + offy;

					SrcRightBottom.y = SrcRightBottom.y + offy;
					SrcRightBottom.x = SrcRightBottom.x + offx;

					Mgegion.RightTop = EMagnetState::EMS_RIGHT_TOP;
				}else if( abs(SrcRightTop.x - RightBottom.x)< pxRange &&abs(SrcRightTop.y - RightBottom.y) <pxRange ){

                    int offx         = RightBottom.x - SrcRightTop.x;
					int offy         = RightBottom.y - SrcRightTop.y;

					SrcRightTop.x    = RightBottom.x;
					SrcRightTop.y    = RightBottom.y;

					SrcLeftBottom.y  = SrcLeftBottom.y + offx;
					SrcLeftBottom.x  = SrcLeftBottom.x + offy;

					SrcLeftTop.x     = SrcLeftTop.x + offx;
					SrcLeftTop.y     = SrcLeftTop.y + offy;

					SrcRightBottom.y = SrcRightBottom.y + offy;
					SrcRightBottom.x = SrcRightBottom.x + offx;

					Mgegion.RightTop = EMagnetState::EMS_RIGHT_TOP;
				}
				/****************效验right Bottom**************/
				else if (abs(SrcRightBottom.x - LeftTop.x)<pxRange && abs(SrcRightBottom.y - LeftTop.y) < pxRange ) {
					
					int offx         = LeftTop.x - SrcRightBottom.x;
					int offy         = LeftTop.y - SrcRightBottom.y;

					SrcRightBottom.y = LeftTop.y;
					SrcRightBottom.x = LeftTop.x;

					SrcRightTop.x    = SrcRightTop.x +  offx;
					SrcRightTop.y    = RightBottom.y +  offy;

					SrcLeftBottom.y  = SrcLeftBottom.y + offx;
					SrcLeftBottom.x  = SrcLeftBottom.x + offy;

					SrcLeftTop.x     = SrcLeftTop.x + offx;
					SrcLeftTop.y     = SrcLeftTop.y + offy;

					Mgegion.RightBottom = EMagnetState::EMS_RIGHT_BOTTOM;
				}else if (abs(SrcRightBottom.x - LeftBottom.x)<pxRange && abs(SrcRightBottom.y - LeftBottom.y) <pxRange) {
					
					int offx         = LeftBottom.x - SrcRightBottom.x;
					int offy         = LeftBottom.y - SrcRightBottom.y;

					SrcRightBottom.y = LeftBottom.y;
					SrcRightBottom.x = LeftBottom.x;

					SrcRightTop.x    = SrcRightTop.x + offx;
					SrcRightTop.y    = RightBottom.y + offy;

					SrcLeftBottom.y  = SrcLeftBottom.y + offx;
					SrcLeftBottom.x  = SrcLeftBottom.x + offy;

					SrcLeftTop.x     = SrcLeftTop.x + offx;
					SrcLeftTop.y     = SrcLeftTop.y + offy;

					Mgegion.RightBottom = EMagnetState::EMS_RIGHT_BOTTOM;
				}else if( abs(SrcRightBottom.x-RightTop.x)<pxRange &&abs(SrcRightBottom.y - RightTop.y) <pxRange ){
					
					int offx         = RightTop.x - SrcRightBottom.x;
					int offy         = RightTop.y - SrcRightBottom.y;

					SrcRightBottom.y = RightTop.y;
					SrcRightBottom.x = RightTop.x;

					SrcRightTop.x    = SrcRightTop.x + offx;
					SrcRightTop.y    = RightBottom.y + offy;

					SrcLeftBottom.y  = SrcLeftBottom.y + offx;
					SrcLeftBottom.x  = SrcLeftBottom.x + offy;

					SrcLeftTop.x     = SrcLeftTop.x + offx;
					SrcLeftTop.y     = SrcLeftTop.y + offy;

					Mgegion.RightBottom = EMagnetState::EMS_RIGHT_BOTTOM;
				}else if( abs(SrcRightBottom.x - RightBottom.x)< pxRange &&abs(SrcRightBottom.y - RightBottom.y) <pxRange ){
					
                    int offx         = RightBottom.x - SrcRightBottom.x;
					int offy         = RightBottom.y - SrcRightBottom.y;

					SrcRightBottom.y = RightBottom.y;
					SrcRightBottom.x = RightBottom.x;

					SrcRightTop.x    = SrcRightTop.x + offx;
					SrcRightTop.y    = RightBottom.y + offy;

					SrcLeftBottom.y  = SrcLeftBottom.y + offx;
					SrcLeftBottom.x  = SrcLeftBottom.x + offy;

					SrcLeftTop.x     = SrcLeftTop.x + offx;
					SrcLeftTop.y     = SrcLeftTop.y + offy;


					Mgegion.RightBottom = EMagnetState::EMS_RIGHT_BOTTOM;
				}
				
	}
	CUIWindow*  CUIMsView::WndFromPointGroup( kkPoint  ptHitTest, int bOnlyText)
	{
	    return m_pActiveGroup->WndFromPoint(ptHitTest,  bOnlyText);
	}
    CUIWindow*  CUIMsView::WndFromPointStage( kkPoint  ptHitTest, int bOnlyText)
	{
		std::vector<CUIMoveState*>::iterator It = m_StageVec.begin();
		for(;It!= m_StageVec.end();++It)
		{
		        CUIWindow  *pChild=*It;
				if (pChild->IsVisible(0) && !pChild->IsMsgTransparent())
			    {
                       CUIWindow  *swndChild = pChild->WndFromPoint(ptHitTest, bOnlyText);
				       if (swndChild) 
					          return swndChild;
				}
		}
		return 0;
	}
	CUIWindow*    CUIMsView::WndFromPoint( kkPoint  ptHitTest, int bOnlyText)
	{
	  
       if(!IsContainPoint(ptHitTest,0))
			return NULL;

		if(!IsContainPoint(ptHitTest,1))
			return this;//只在鼠标位于客户区时，才继续搜索子窗口 


		 CUIWindow  *pChild = 0;
		 if(m_nEditorModel ==0 )
		 {
			    
				 if(m_pActiveGroup){
					pChild=WndFromPointGroup(ptHitTest,  bOnlyText);
					if(pChild)
						return pChild;
				 }
               
		
		 }else  if(m_nEditorModel ==1 ){
		         pChild=WndFromPointStage(ptHitTest, bOnlyText);
		         if(pChild)
						return pChild;
		 }
		
		/*CUIWindow  *pChild=GetWindow(GSW_LASTCHILD);
		while(pChild)
		{
			if (pChild->IsVisible(0) && !pChild->IsMsgTransparent())
			{
				CUIWindow  *swndChild = pChild->WndFromPoint(ptHitTest, bOnlyText);

				if (swndChild) 
					return swndChild;
			}

			pChild=pChild->GetWindow(GSW_PREVSIBLING);
		}*/

		return this;

	}
    CSize  CUIMsView::GetScrollPos()
	{
		CSize ss(m_siHoz.nPos,m_siVer.nPos);
		return ss;
	}
	void CUIMsView::RrefreshStageCtls()
	{
	         //开始计算舞台控件包含的控件
			std::vector<CUIMoveState*>::iterator It=  m_StageVec.begin();
			for(;It!= m_StageVec.end();++It){
					  CUIMoveState* Stage=*It;
					  Stage->ClearIntersectCtl();
					  CRect StageRt;
					  Stage->GetWindowRect(&StageRt);
                      GetScalRect(StageRt,m_fDisplayScale);

					  if(m_pActiveGroup){
						     std::vector<CUIYUV*>&   yuvVec=m_pActiveGroup->GetYuvVector();
							 std::vector<CUIYUV*>::iterator   yuvIt=yuvVec.begin();
							 for(; yuvIt!=yuvVec.end();yuvIt++)
							 {
							      CUIWindow *pChild=*yuvIt;
								  char* classnmae = pChild->GetObjectClass();
								  if (strcmp(classnmae, "uimovestate") != 0) 
								  {
										  CUIYUV* yuv =(CUIYUV*)pChild;
										  CRect Rt;
										  pChild->GetWindowRect(&Rt);
										  GetScalRect(Rt,m_fDisplayScale);
										  if(kkRectIntersects(StageRt,Rt)){
											  Stage->AddIntersectCtl(pChild);
										  }else
										  {
											//  CRect StageRt;
											
											 if( yuv->RectInterRegion(StageRt))
												 Stage->AddIntersectCtl(pChild);
										  }
								   }
							 }
							 
							 
					  }
					  //遍历所有的子窗口
				   //   CUIWindow *pChild=GetWindow(GSW_FIRSTCHILD);
					  //while(pChild){

						 // char* classnmae = pChild->GetObjectClass();
						 // if (strcmp(classnmae, "uimovestate") != 0) 
						 // {
       //                           CUIYUV* yuv =(CUIYUV*)pChild;
							//	  CRect Rt;
							//	  pChild->GetWindowRect(&Rt);
							//	  GetScalRect(Rt,m_fDisplayScale);
							//	  if(kkRectIntersects(StageRt,Rt)){
							//		  Stage->AddIntersectCtl(pChild);
							//	  }else
							//	  {
							//		//  CRect StageRt;
							//		
							//	     if( yuv->RectInterRegion(StageRt))
							//			 Stage->AddIntersectCtl(pChild);
							//	  }
						 //  }
						 //  pChild=pChild->GetWindow(GSW_NEXTSIBLING);
					  // }
			}
	}
	void   CUIMsView::UpdateChildrenPosition()
	{
		   
			CRect rt;
			this->GetClientRect(&rt);
			CUIWindow *pChild=GetWindow(GSW_FIRSTCHILD);
			CSize ss=GetScrollPos();
			while(pChild)
			{
				if(pChild->IsFloat())
				{
				/*	ss.cx*=m_fDisplayScale;
					ss.cy*=m_fDisplayScale;*/
					pChild->OnUpdateFloatPosition( ss,rt);
					pChild->UpdateChildrenPosition();
				}
				pChild=pChild->GetWindow(GSW_NEXTSIBLING);
			}

			CalViewSz();
			UpdateScrollBar();
            OnNcCalcSize(0,0);

            RrefreshStageCtls();
	}
	void   CUIMsView::OnScrollPosChanged()
	{
	        CRect rt;
			this->GetClientRect(&rt);
			CUIWindow *pChild=GetWindow(GSW_FIRSTCHILD);
			 CSize ss=GetScrollPos();
			while(pChild)
			{
				if(pChild->IsFloat())
				{
				/*	ss.cx*=m_fDisplayScale;
					ss.cy*=m_fDisplayScale;*/
					pChild->OnUpdateFloatPosition( ss,rt);
					pChild->UpdateChildrenPosition();
				}
				pChild=pChild->GetWindow(GSW_NEXTSIBLING);
			}
	}
	void   CUIMsView::OnAdjustDisplayScale()
	{     
	        CUIWindow *pChild=GetWindow(GSW_FIRSTCHILD);
			while(pChild)
			{
				IDisplayScale* pIDisplayScale = dynamic_cast<IDisplayScale*>(pChild);
				if(pIDisplayScale){
					pIDisplayScale->SetDisplayScale(m_fDisplayScale);
				}
				pChild=pChild->GetWindow(GSW_NEXTSIBLING);
			}
	}
	void   CUIMsView::SetActiveYUV( CUIYUV*  pCurActiveYUV)
	{
	     m_pCurActiveYUV=pCurActiveYUV;

		 //图层模式则设置焦点
		 if(!m_nEditorModel)
		      m_pCurActiveYUV->SetCapture();
	}
	CUIYUV*    CUIMsView::GetActiveYUV()
	{
		if(m_nEditorModel ==0)
	        return m_pCurActiveYUV;
		return 0;
	}


     void    CUIMsView::SetActiveStage(CUIMoveState* stage)
	 { 
		 m_pCurActiveStage= stage;
	 }
     CUIMoveState* CUIMsView::GetActiveStage()
	 {
		 if(m_nEditorModel ==1)
		 return m_pCurActiveStage;
		 return 0;
	 }
     CUIMoveState* CUIMsView::GetStageByRect(CRect rt)
	 {
		 std::vector<CUIMoveState*>::iterator It=  m_StageVec.begin();
		 for(;It!=m_StageVec.end();It++)
		 {
		     CUIMoveState* Stage = *It;
			 CRect StageRt;
			 Stage->GetWindowRect(&StageRt);
             GetScalRect(StageRt,m_fDisplayScale);
			 if( StageRt.left   <= rt.left&&
				 StageRt.top    <= rt.top&&
				 StageRt.right  >= rt.right&&
				 StageRt.bottom >= rt.bottom
				 ){
			    return Stage;
			 }
		 }
		 return 0;
	 }
	void      CUIMsView::AddYuvByGroup(void* Group,CUIYUV* yuv)
	{
	        CUIYUVGroup* YuvGroup = (CUIYUVGroup*)Group;
            YuvGroup->AddYuv(yuv);
	}
	void      CUIMsView::DelYuvByGroup(void* Group,CUIYUV* yuv)
	{
	        CUIYUVGroup* YuvGroup = (CUIYUVGroup*)Group;
            YuvGroup->DelYuv(yuv);
	}
	void     CUIMsView::SetEditorModel        (int nEditorModel)
	{
	       m_nEditorModel = nEditorModel;
		   ReleaseCapture();
		   Invalidate();
	}

	void   CUIMsView::GroupSwopYuv(CUIYUVGroup* Group,CUIYUV* srcYuv,CUIYUV* DestYuv,int pre)
	{
		 Group->SwopYuv(srcYuv,DestYuv,pre);
		 RrefreshStageCtls();
	}
}