#include "WarpImpl.h"
#include <math.h>
namespace KKUI
{
	CWarpImpl::CWarpImpl()
	{

		//topleft
		/*m_OriginalInfo[0].x=0.8;
		m_OriginalInfo[0].y=0.83;

		m_OriginalInfo[1].x=1.0;
		m_OriginalInfo[1].y=0.0;

		m_OriginalInfo[2].x=1.0;
		m_OriginalInfo[2].y=1.0;

		m_OriginalInfo[3].x=0.0;
		m_OriginalInfo[3].y=1.0;*/

	    SetXYDiv(2, 1);
	};

	void   CWarpImpl::GetXYDiv(int &x, int &y)
	{
	   x = m_nXDivision;
	   y = m_nYDivision;
	}
	void  CWarpImpl::SetXYDiv(int x, int y)
	{
	
		  m_nXDivision = x;
		  m_nYDivision = y;

		  int ptcount = (m_nXDivision+1)*(m_nYDivision+1);
		  m_OriginalVector.resize(ptcount);



		 int rowTotal = m_nYDivision +1;
		 int colTotal = m_nXDivision +1;
	     SVertInfo* infos=  &m_OriginalVector[0];
	
		 int off=0;
		 int Id=0;
		 for(int r =0 ;r< rowTotal; r ++)
		 {
		    off =r*colTotal;
			float f = 1.0/ m_nYDivision*r;
			for(int c = 0; c<colTotal;c++){
				SVertInfo* info=(infos+off+c);
				info->x = 1.0/ m_nXDivision * c;
				info->y =f;
				info->uv.x= info->x;
				info->uv.y= info->y;
			}
		 }
	}
	int   CWarpImpl::GetWarpInfos(SUVInfo* infos,int count)
	{
	   if(count<m_OriginalVector.size())
		   return 0;
	   for(int i=0;i<m_OriginalVector.size();i++)
	   {

		   SVertInfo& Item =m_OriginalVector.at(i);
		    (infos+i)->x=Item.x;
		    (infos+i)->y=Item.y;
	   }
	   return m_OriginalVector.size();
	}
	int   CWarpImpl::SetWarpInfos(SUVInfo* infos,int count)
	{
	   if(count>m_OriginalVector.size() || count==0)
		   return 0;

	      SVertInfo* infos2=  &m_OriginalVector[0];
	   for(int i=0;i<m_OriginalVector.size();i++)
	   {
		 (infos2+i)->x= (infos+i)->x;
		 (infos2+i)->y= (infos+i)->y;
	   }
	   return m_OriginalVector.size();
	}
	void GetRtByPt(kkRect& rt,kkPoint* pt,int width,int height)
	{
	    
		 int w = width/2;
		 int h = height/2;

		 rt.left = pt->x-w;
		 rt.right = pt->x+w;
		 rt.top = pt->y-h;
		 rt.bottom = pt->y+h;
	}

    bool  CWarpImpl::TestPtInMestWarping(CPoint pt,int &xOut,int& yOut,const CRect& rt)
	{
	     int rowTotal = m_nYDivision +1;
		 int colTotal = m_nXDivision +1;
	     SVertInfo* infos=  &m_OriginalVector[0];
		 int Width  = rt.right  - rt.left;
		 int Height = rt.bottom - rt.top;
         CPoint    ptx;
		 int off=0;
		 int Id=0;
		 for(int r =0 ;r< rowTotal; r ++)
		 {
		    off =r*colTotal;
			for(int c = 0; c<colTotal;c++)
			{
				SVertInfo* info=(infos+off+c);
				ptx.x = Width  * info->x+ rt.left;
				ptx.y = Height * info->y+ rt.top;

				CRect tempRt;
				GetRtByPt(tempRt,&ptx,8,8);
				if(tempRt.PtInRect(pt))
				{
						    xOut = r;
							yOut = c;
							return true;
				}
			}
		 }

		 xOut = -1;
		 yOut = -1;
		 return 0;
	}
	void  CWarpImpl::GetPtInfo(int RowId,int ColId, SUVInfo& Out)
	{
	     int rowTotal = m_nYDivision +1;
		 int colTotal = m_nXDivision +1;
	     SVertInfo* infos=  &m_OriginalVector[0];
		
		 int off=RowId*colTotal+ColId;
		
		 SVertInfo* info=(infos+off);
		 Out.x = info->x;
		 Out.y = info->y;
	}
    void  CWarpImpl::SetPtInfo(int RowId,int ColId,const SUVInfo& Out)
	{
	     int rowTotal = m_nYDivision +1;
		 int colTotal = m_nXDivision +1;
	     SVertInfo* infos=  &m_OriginalVector[0];
		
		 int off=RowId*colTotal+ColId;
		
		 SVertInfo* info=(infos+off);
		 info->x = Out.x;
		 info->y = Out.y;
	}

   
	void CWarpImpl::OnPaintMesh(IRenderTarget  *pRT,const CRect& rt,unsigned int crBorder)
	{
	    
		  int TotalRow =  m_nYDivision;
		  int TotalCol =  m_nXDivision;
		
		
          int off1=0 ,off2=0;
		  for(int r = 0;r<TotalRow;r++)
		  {
              off1=(TotalCol+1)*r;
			  off2=(TotalCol+1)*(r+1);
		      for(int c = 0;c<TotalCol;c++)
			  {
			      m_OriginalInfo[0] = m_OriginalVector[off1+c];
				  m_OriginalInfo[1] = m_OriginalVector[off1+c+1];
				 
				  m_OriginalInfo[2]= m_OriginalVector[off2+c];
				  m_OriginalInfo[3]= m_OriginalVector[off2+c+1];
				    //分段绘制
		          PaintMesh(pRT,rt,crBorder);
			  }
		  }

		 /* m_OriginalInfo[0] = m_OriginalVector[0];
		  m_OriginalInfo[1]=m_OriginalVector[1];
		 
		  m_OriginalInfo[2]= m_OriginalVector[2];
		  m_OriginalInfo[3]= m_OriginalVector[3];*/

		

	}
	void  CWarpImpl::PaintMesh(IRenderTarget  *pRT,const CRect& rt,unsigned int  crBorder)
	{
		  int nXDivision=20/ m_nXDivision;
		  int nYDivision=20/ m_nYDivision;

		  SUVInfo                      SideTopLeftRightPts[24];		
		  SUVInfo                      SideBottomLeftRightPts[24];

		  SUVInfo                      SideRightTopBottomPts[24];
		  SUVInfo                      SideLeftTopBottomPts[24];


	      SVertInfo topLeft  = m_OriginalInfo[0];
		  SVertInfo topRight = m_OriginalInfo[1];
		 
		  SVertInfo bottonLeft= m_OriginalInfo[2];
		  SVertInfo bottonRight= m_OriginalInfo[3];

		  //沿着X轴分割
		  if(nXDivision){
			       float xTopDiv=(topRight.x - topLeft.x) /nXDivision;
				   float xBottonDiv=(bottonRight.x - bottonLeft.x) /nXDivision;

				    float yTopf = 0.0000;
				    if(topRight.x - topLeft.x!=0.00000)
				       yTopf =(topRight.y - topLeft.y)/(topRight.x - topLeft.x);

					float yBottonf = 0.0000;
					if(bottonRight.x - bottonLeft.x!=0.00000)
						yBottonf =(bottonRight.y - bottonLeft.y) / (bottonRight.x - bottonLeft.x);
				   for(int i=0;i<=nXDivision;i++){
					   SideTopLeftRightPts[i].x= xTopDiv*i;
					   //这个需要修改
					   SideTopLeftRightPts[i].y =  SideTopLeftRightPts[i].x *yTopf  + topLeft.y;
					   SideTopLeftRightPts[i].x += topLeft.x;
					   
					   SideBottomLeftRightPts[i].x= xBottonDiv*i;
					   SideBottomLeftRightPts[i].y= SideBottomLeftRightPts[i].x * yBottonf + bottonLeft.y;
					   SideBottomLeftRightPts[i].x += bottonLeft.x ;
				   }
			}

		    //沿着y轴分割
			if(nYDivision>0)
			{
				   float yLeftDiv  = (bottonLeft.y - topLeft.y) /nYDivision;
				   float yRightDiv = (bottonRight.y - topRight.y) / nXDivision;

				   float xLeftf = 0.0000;
				   if(bottonLeft.x - topLeft.x!=0.00000 )
					   xLeftf =  (bottonLeft.x - topLeft.x) / (bottonLeft.y - topLeft.y);
				   float xRightf = 0.0000;
				   if(bottonRight.x - topRight.x!=0.00000 )
					   xRightf =(bottonRight.x - topRight.x) / (bottonRight.y - topRight.y);

				   for(int i=0;i<=nYDivision;i++){ 
					   SideLeftTopBottomPts[i].y= yLeftDiv*i;
					   SideLeftTopBottomPts[i].x= xLeftf * SideLeftTopBottomPts[i].y + topLeft.x;
					   SideLeftTopBottomPts[i].y += topLeft.y;
					  
					   SideRightTopBottomPts[i].y= yRightDiv*i;
					   SideRightTopBottomPts[i].x= xRightf*  SideRightTopBottomPts[i].y + topRight.x;
					   SideRightTopBottomPts[i].y += topRight.y;
				   }
				 
			}


		    SVertInfo   SidePtInfos[512];
		    SVertInfo*  pts=SidePtInfos;
			int Offset = 0;
			int rowId=0;
		    for(int y=0;y<=nYDivision;++y)
			{
				Offset = y*(nXDivision+1);
				
				for(int x=0;x<=nXDivision;++x){
					rowId = Offset+x;
					pts[rowId].x = (SideRightTopBottomPts[y].x - SideLeftTopBottomPts[y].x) / nXDivision*x+ SideLeftTopBottomPts[y].x;
					pts[rowId].y = (SideBottomLeftRightPts[x].y - SideTopLeftRightPts[x].y) / nYDivision*y+ SideTopLeftRightPts[x].y;
                    pts[rowId].z=1.0;
					pts[rowId].uv.x=pts[rowId].x;
					pts[rowId].uv.y=pts[rowId].y;
				}
			}
			
		

		    kkPoint    Pts1[512];
			 Offset = 0 ;
			rowId  =0;
			CPoint    ptx;
			int Width = rt.right -rt.left;
			int Height = rt.bottom -rt.top;
			for(int y=0;y<=nYDivision;++y)
			{
				
				  Offset = y*(nXDivision+1);
				  for(int x=0;x<=nXDivision;++x){
						rowId = Offset+x;

						ptx.x = rt.left + pts[rowId].x * Width;
						ptx.y = rt.top + pts[rowId].y  * Height;
						
						if(y>0)
						{
						    pRT->DrawLine(Pts1[x].x,Pts1[x].y,ptx.x,ptx.y,crBorder);
						}
					
						Pts1[x]=ptx;
					}
				 // pRT->DrawLine(Pts1[0].x, Pts1[0].y, Pts1[1].x, Pts1[1].y, crBorder);
				  pRT->DrawLines(Pts1,nXDivision+1,crBorder);

			}

			ptx.x = rt.left + topLeft.x * Width;
			ptx.y = rt.top + topLeft.y  * Height;
			
			kkRect rtx;
			GetRtByPt(rtx,&ptx,8,8);
			pRT->FillRect(&rtx,crBorder);

			ptx.x = rt.left + topRight.x  * Width;
			ptx.y = rt.top  + topRight.y  * Height;
			GetRtByPt(rtx, &ptx, 8, 8);
			pRT->FillRect(&rtx, crBorder);

			ptx.x = rt.left + bottonLeft.x  * Width;
			ptx.y = rt.top + bottonLeft.y  * Height;
			GetRtByPt(rtx, &ptx, 8, 8);
			pRT->FillRect(&rtx, crBorder);

			ptx.x = rt.left + bottonRight.x  * Width;
			ptx.y = rt.top + bottonRight.y  * Height;
			GetRtByPt(rtx, &ptx, 8, 8);
			pRT->FillRect(&rtx, crBorder);
	}

	 void  CWarpImpl::OnPaint(IRenderTarget  *pRT,const CRect& rt)
	{
	
		  int TotalRow =  m_nYDivision;
		  int TotalCol =  m_nXDivision;
		
		
          int off1=0 ,off2=0;
		  for(int r = 0;r<TotalRow;r++)
		  {
              off1=(TotalCol+1)*r;
			  off2=(TotalCol+1)*(r+1);
		      for(int c = 0;c<TotalCol;c++)
			  {
			      m_OriginalInfo[0] = m_OriginalVector[off1+c];
				  m_OriginalInfo[1] = m_OriginalVector[off1+c+1];
				 
				  m_OriginalInfo[2]= m_OriginalVector[off2+c];
				  m_OriginalInfo[3]= m_OriginalVector[off2+c+1];
				    //分段绘制
		         PaintCell(pRT, rt);
			  }
		  }

		  ////left
		  //CRect rt2=rt;
		  //rt2.right=rt2.left+2;
		  //pRT->FillRect(&rt2,0xFF000000);


		  ////top;
		  //rt2=rt;
		  //rt2.bottom =rt2.top+2;
		  //pRT->FillRect(&rt2,0xFF000000);

		  ////bottom;
		  //rt2=rt;
		  //rt2.top =rt2.bottom-2;
		  //pRT->FillRect(&rt2,0xFF000000);


		  ////right;
		  //rt2=rt;
		  //rt2.left =rt2.right-2;
		  //pRT->FillRect(&rt2,0xFF000000);

	}
	void  CWarpImpl::PaintCell(IRenderTarget  *pRT,const CRect& rt)
	{
	     int nXDivision=20/ m_nXDivision;
		 int nYDivision=20/ m_nXDivision;

		 SVertInfo                      SideTopLeftRightPts[24];		
		 SVertInfo                     SideBottomLeftRightPts[24];

		 SVertInfo                     SideRightTopBottomPts[24];
		 SVertInfo                  SideLeftTopBottomPts[24];


	      SVertInfo topLeft= m_OriginalInfo[0];
		  SVertInfo topRight= m_OriginalInfo[1];
		 
		 SVertInfo  bottonLeft= m_OriginalInfo[2];
		  SVertInfo bottonRight= m_OriginalInfo[3];

		 // 沿着X轴分割
		  if(nXDivision){
			       float  xTopDiv=(topRight.x - topLeft.x) /nXDivision;
				   float  xBottonDiv=(bottonRight.x - bottonLeft.x) /nXDivision;

				   float  uTopDiv =    (topRight.uv.x - topLeft.uv.x) /nXDivision;
				   float  uBottonDiv =  (bottonRight.uv.x - bottonLeft.uv.x) /nXDivision;

				   float yTopf = 0.0000;
				   if(topRight.x - topLeft.x!=0.00000)
				       yTopf =(topRight.y - topLeft.y)/(topRight.x - topLeft.x);

				   float vTopf = 0.0000;
				   if(topRight.uv.x - topLeft.uv.x!=0.00000)
				       vTopf =(topRight.uv.y - topLeft.uv.y)/(topRight.uv.x - topLeft.uv.x);

                  

				   float yBottonf = 0.0000;
				   if(bottonRight.x - bottonLeft.x!=0.00000)
						yBottonf =(bottonRight.y - bottonLeft.y) / (bottonRight.x - bottonLeft.x);


				   float vBottonf = 0.0000;
				   if(bottonRight.uv.x - bottonLeft.uv.x!=0.00000)
						vBottonf =(bottonRight.uv.y - bottonLeft.uv.y) / (bottonRight.uv.x - bottonLeft.uv.x);

				   for(int i=0;i<=nXDivision;i++){
					   SideTopLeftRightPts[i].x  =  xTopDiv*i;
					   SideTopLeftRightPts[i].y  =  SideTopLeftRightPts[i].x *yTopf  + topLeft.y;
					   SideTopLeftRightPts[i].x +=  topLeft.x;

                       SideTopLeftRightPts[i].uv.x  = uTopDiv*i;
					   SideTopLeftRightPts[i].uv.y  =  SideTopLeftRightPts[i].uv.x *vTopf  + topLeft.uv.y;
					   SideTopLeftRightPts[i].uv.x +=  topLeft.uv.x;


					   SideBottomLeftRightPts[i].x= xBottonDiv*i;
					   SideBottomLeftRightPts[i].y= SideBottomLeftRightPts[i].x * yBottonf + bottonLeft.y;
					   SideBottomLeftRightPts[i].x += bottonLeft.x ;

					   SideBottomLeftRightPts[i].uv.x= uBottonDiv*i;
					   SideBottomLeftRightPts[i].uv.y= SideBottomLeftRightPts[i].uv.x * vBottonf + bottonLeft.uv.y;
					   SideBottomLeftRightPts[i].uv.x += bottonLeft.uv.x ;

					  
				   }
			}

		    //沿着y轴分割
			if(nYDivision>0)
			{
				   float yLeftDiv  = (bottonLeft.y - topLeft.y) /nYDivision;
				   float yRightDiv = (bottonRight.y - topRight.y) / nXDivision;

				   float vLeftDiv  = (bottonLeft.uv.y - topLeft.uv.y) /nYDivision;
				   float vRightDiv = (bottonRight.uv.y - topRight.uv.y) / nXDivision;


				   float xLeftf = 0.0000;
				   if(bottonLeft.x - topLeft.x!=0.00000 )
					   xLeftf =  (bottonLeft.x - topLeft.x) / (bottonLeft.y - topLeft.y);
				  
				   float uLeftf = 0.0000;
				   if(bottonLeft.uv.x - topLeft.uv.x!=0.00000 )
					   uLeftf =  (bottonLeft.uv.x - topLeft.uv.x) / (bottonLeft.uv.y - topLeft.uv.y);

				   float xRightf = 0.0000;
				   if(bottonRight.x - topRight.x!=0.00000 )
					   xRightf =(bottonRight.x - topRight.x) / (bottonRight.y - topRight.y);

				   float uRightf = 0.0000;
				   if(bottonRight.uv.x - topRight.uv.x!=0.00000 )
					   uRightf =(bottonRight.uv.x - topRight.uv.x) / (bottonRight.uv.y - topRight.uv.y);


				   for(int i=0;i<=nYDivision;i++){ 
					   SideLeftTopBottomPts[i].y= yLeftDiv*i;
					   SideLeftTopBottomPts[i].x= xLeftf * SideLeftTopBottomPts[i].y + topLeft.x;
					   SideLeftTopBottomPts[i].y += topLeft.y;

					   SideLeftTopBottomPts[i].uv.y= vLeftDiv*i;
					   SideLeftTopBottomPts[i].uv.x= uLeftf * SideLeftTopBottomPts[i].uv.y + topLeft.uv.x;
					   SideLeftTopBottomPts[i].uv.y += topLeft.uv.y;


					  
					   SideRightTopBottomPts[i].y= yRightDiv*i;
					   SideRightTopBottomPts[i].x= xRightf*  SideRightTopBottomPts[i].y + topRight.x;
					   SideRightTopBottomPts[i].y += topRight.y;

					   SideRightTopBottomPts[i].uv.y= vRightDiv*i;
					   SideRightTopBottomPts[i].uv.x= uRightf*  SideRightTopBottomPts[i].uv.y + topRight.uv.x;
					   SideRightTopBottomPts[i].uv.y += topRight.uv.y;
				   }
				 
			}


            //计算每一个点
		    SVertInfo   SidePtInfos[512];
		    SVertInfo*  pts=SidePtInfos;
			int Offset = 0;
			int rowId=0;
				int Width = rt.right -rt.left;
			int Height = rt.bottom -rt.top;
		    for(int y=0;y<=nYDivision;++y)
			{
				Offset = y*(nXDivision+1);
				
				for(int x=0;x<=nXDivision;++x){
					rowId = Offset+x;
					pts[rowId].x = (SideRightTopBottomPts[y].x - SideLeftTopBottomPts[y].x) / nXDivision*x+ SideLeftTopBottomPts[y].x;
					pts[rowId].y = (SideBottomLeftRightPts[x].y - SideTopLeftRightPts[x].y) / nYDivision*y+ SideTopLeftRightPts[x].y;
                    pts[rowId].z=1.0;
					pts[rowId].uv.x= (SideRightTopBottomPts[y].uv.x - SideLeftTopBottomPts[y].uv.x) / nXDivision*x+ SideLeftTopBottomPts[y].uv.x;
					pts[rowId].uv.y= (SideBottomLeftRightPts[x].uv.y - SideTopLeftRightPts[x].uv.y) / nYDivision*y+ SideTopLeftRightPts[x].uv.y;

					//生成真实的坐标点
                    pts[rowId].x = rt.left + pts[rowId].x * Width;
				    pts[rowId].y = rt.top + pts[rowId].y  * Height;

				}
			}
			
		    //D3DPT_TRIANGLESTRIP 计算方式
			if(1){
				for(int y=0;y<nYDivision;++y)
				{
				
					SVertInfo VertInfo[68]={};
					int trcount=(nXDivision+1)*2;
			
					int id = 0;
					for(int i=0; i<= nXDivision; i++)
					{
						 int off1 = y*(nXDivision + 1) + i;
						 int off2=(y+1)*(nXDivision+1) + i;
					 
						 VertInfo[id++] = pts[off2];
						 VertInfo[id++] = pts[off1];
						// VertInfo[id++].z = 0;
					}
					pRT->DrawWarp(VertInfo, id);
				}
			}else {
				for (int y = 0; y<nYDivision; ++y)
				{

					SVertInfo VertInfo[68] = {};
					int trcount = nXDivision * 2*3;

					int id = 0;
					for (int i = 0; i < nXDivision; i++)
					{
						int off1 = y*(nXDivision + 1) + i;
						int off2 = y*(nXDivision + 1) + i+1;
						int off3 = (y + 1)*(nXDivision + 1) + i;
						int off4 = (y + 1)*(nXDivision + 1) + i+1;

						VertInfo[id++] = pts[off1];
						VertInfo[id++] = pts[off2];
						VertInfo[id++] = pts[off3];

						VertInfo[id++] = pts[off2];
						VertInfo[id++] = pts[off4];
						VertInfo[id++] = pts[off3];
					}
					pRT->DrawWarp(VertInfo, trcount);
				}
			
			}

		   
	}
}