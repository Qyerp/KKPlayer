// MangageTrade.cpp : main source file for MangageTrade.exe
//

#include "stdafx.h"
#include "MainPage/MainDlg.h"
#include "MainPage/SUIVideo.h"
#include "MainPage/KKWkeWebkit.h"
#include "control/kkmclv.h"
#include "control/kkseek.h"
#include "control/SHeaderCtrlEx.h"
#include "control/SMCListViewEx.h"

#include "Dir/Dir.hpp"
#include "Tool/CFileMgr.h"
#include "Tool/cchinesecode.h"
#include "Tool/FileRelation.h"
#include "Tool/WinDir.h"

#include "SqlOp/HistoryInfoMgr.h"
#include <ShellAPI.h>

#include "../KKPlayerCore/KKPlayerInfo.h"

#include "UpDate/UpDateCheck.h"
#include "SGifPlayer.h"
#include "SSkinAPNG.h"
#include "SSkinGif.h"
#include "uiInfo.h"
#include <set>
void DeclareDumpFile();

#ifdef _DEBUG
#define SYS_NAMED_RESOURCE _T("soui-sys-resourced.dll")
#else
#define SYS_NAMED_RESOURCE _T("soui-sys-resource.dll")
#endif

CreateRender pfnCreateRender = NULL;
FnDelRender    pfnDelRender=NULL;


fpKKPlayerGetUrl     pfpGetUrl=NULL;
//F:\ProgramTool\OpenPro\KKPlayer\KKPlayer>uiresbuilder.exe -iuires/uires.idx -puires -rres/KK_res.rc2
 #pragma data_seg("KKPLAYER_SHAREDATA")
HWND seg_hMainWindow = NULL;     ///< 程序主窗口
#pragma data_seg()
#pragma comment(linker,"/SECTION:KKPLAYER_SHAREDATA,RWS")

#define APPINSTANCE_STRING L"{3558C2CB-F38C-4F13-AAC6-711836BDA48A}"

HANDLE G_hAppMutex = NULL;    ///< 进程互斥对象，保证进程单例运行
/******************************************************************************/
/**
* @brief
* <pre>
*   访问类型 : protected
*   功能描述 : 单例方式运行进程的初始化判断接口；若已有进程启动实例，则切换显示至该实例的主窗口。
* </pre>
*
* @return BOOL
*         - TRUE，表示启动的进程为单例进程；
*         - FALSE，表示已启动了相关的进程，非单例方式。
*/
BOOL SingletonRunInit()
{
	BOOL bInit = TRUE;

	do
	{
		//======================================
		// 创建互斥对象，防止一台机器上同时运行多个进程

		G_hAppMutex = CreateMutexW(NULL, TRUE, APPINSTANCE_STRING);
		if (NULL == G_hAppMutex)
		{
			break;
		}

		if (ERROR_ALREADY_EXISTS != ::GetLastError())
		{
			break;
		}

		ReleaseMutex(G_hAppMutex);
		CloseHandle(G_hAppMutex);
		G_hAppMutex = NULL;

		//======================================

		bInit = FALSE;

		//======================================
		// 已启动进程的主窗口句柄判断

		if (!IsWindow(seg_hMainWindow))
		{
			seg_hMainWindow = NULL;
			break;
		}

		// ShowWindow(seg_hMainWindow, SW_SHOWNORMAL);
		SetForegroundWindow(seg_hMainWindow);

		//======================================

		// 获取 SwitchToThisWindow API 函数地址
		HMODULE hUser32 = GetModuleHandle(TEXT("user32"));
		if (NULL == hUser32)
		{
			break;
		}

		typedef void (WINAPI *PSWITCHTOTHISWINDOW)(HWND, BOOL);
		PSWITCHTOTHISWINDOW SwitchToThisWindow;
		SwitchToThisWindow = (PSWITCHTOTHISWINDOW)GetProcAddress(hUser32, "SwitchToThisWindow");

		// 调用 SwitchToThisWindow API 函数切换至已启动的程序主窗口
		
		SwitchToThisWindow(seg_hMainWindow, TRUE);
		
		//======================================
	} while (0);

	return bInit;
}

void GetRelationIcoInfo(std::set<std::string>& infoset)
{
	 CFileRelation FileRelation;
     bool bReIcon=FileRelation.CheckFileRelation(L".3gp",L"KKPlayer.3gp");
	 if(bReIcon){
		infoset.insert("3gp");
	 }
	
	 bReIcon=FileRelation.CheckFileRelation(L".avi",L"KKPlayer.avi");
	 if(bReIcon){
		infoset.insert("avi");
	 }
	
	 bReIcon=FileRelation.CheckFileRelation(L".bt",L"KKPlayer.bt");
	 if(bReIcon){
	    infoset.insert("bt");
	 }
	
	 bReIcon=FileRelation.CheckFileRelation(L".flv",L"KKPlayer.flv");
	 if(bReIcon){
	    infoset.insert("flv");
	 }
	
		bReIcon=FileRelation.CheckFileRelation(L".mkv",L"KKPlayer.mkv");
		if(bReIcon){
			infoset.insert("mkv");
		}
	
		bReIcon=FileRelation.CheckFileRelation(L".mov",L"KKPlayer.mov");
		if(bReIcon){
		   infoset.insert("mov");
		}

		bReIcon=FileRelation.CheckFileRelation(L".mp4",L"KKPlayer.mp4");
		if(bReIcon){
		    infoset.insert("mp4");
		}
	
		bReIcon=FileRelation.CheckFileRelation(L".mov",L"KKPlayer.mpg");
		if(bReIcon){
		infoset.insert("mpg");
		}
	
		bReIcon=FileRelation.CheckFileRelation(L".rm",L"KKPlayer.rm");
		if(bReIcon){
		infoset.insert("rm");
		}
	
		bReIcon=FileRelation.CheckFileRelation(L".rmvb",L"KKPlayer.rmvb");
		if(bReIcon){
		infoset.insert("rmvb");
		}
	
		bReIcon=FileRelation.CheckFileRelation(L".wma",L"KKPlayer.wma");
		if(bReIcon){
		infoset.insert("wma");
		}

		bReIcon=FileRelation.CheckFileRelation(L".ts",L"KKPlayer.ts");
		if(bReIcon){
		infoset.insert("ts");
		}

		bReIcon=FileRelation.CheckFileRelation(L".m4v",L"KKPlayer.m4v");
		if(bReIcon){
		infoset.insert("m4v");
		}
}

///关联图标
void RelationIco(const std::string &name)
{
    CFileRelation FileRelation;
	CWinDir windir;
	std::wstring Localpath=windir.GetModulePath();
	std::wstring pathIco=Localpath+L"\\KKIcons.dll,"; 
	std::wstring Propath=Localpath+_T("\\KKplayer.exe");
	bool bReIcon=0;
	if(name=="3gp"){
			pathIco=Localpath+L"\\KKIcons.dll,-101"; 
			FileRelation.RegisterFileRelation(_T(".3gp"),(LPTSTR)Propath.c_str(),_T("KKPlayer.3gp"),(LPTSTR)pathIco.c_str(),_T(".3gp"));
	}else if(name=="avi"){
		bReIcon=FileRelation.CheckFileRelation(L".avi",L"KKPlayer.avi");
		if(!bReIcon){
			pathIco=Localpath+L"\\KKIcons.dll,-102"; 
	       
			FileRelation.RegisterFileRelation(_T(".avi"),(LPTSTR)Propath.c_str(),_T("KKPlayer.avi"),(LPTSTR)pathIco.c_str(),_T(".avi"));
		}
	}else if(name=="bt"){
			pathIco=Localpath+L"\\KKIcons.dll,-103"; 
			FileRelation.RegisterFileRelation(_T(".bt"),(LPTSTR)Propath.c_str(),_T("KKPlayer.bt"),(LPTSTR)pathIco.c_str(),_T(".bt"));
	}else if(name=="flv"){
		bReIcon=FileRelation.CheckFileRelation(L".flv",L"KKPlayer.flv");
		if(!bReIcon){
			pathIco=Localpath+L"\\KKIcons.dll,-104"; 
	       
			FileRelation.RegisterFileRelation(_T(".flv"),(LPTSTR)Propath.c_str(),_T("KKPlayer.flv"),(LPTSTR)pathIco.c_str(),_T(".flv"));
		}
	}else if(name=="mkv"){
			pathIco=Localpath+L"\\KKIcons.dll,-105"; 
			FileRelation.RegisterFileRelation(_T(".mkv"),(LPTSTR)Propath.c_str(),_T("KKPlayer.mkv"),(LPTSTR)pathIco.c_str(),_T(".mkv"));
	}else if(name=="mov"){
			pathIco=Localpath+L"\\KKIcons.dll,-106"; 
			FileRelation.RegisterFileRelation(_T(".mov"),(LPTSTR)Propath.c_str(),_T("KKPlayer.mov"),(LPTSTR)pathIco.c_str(),_T(".mov"));
	}else if(name=="ts"){
			pathIco=Localpath+L"\\KKIcons.dll,-106"; 
			FileRelation.RegisterFileRelation(_T(".ts"),(LPTSTR)Propath.c_str(),_T("KKPlayer.ts"),(LPTSTR)pathIco.c_str(),_T(".ts"));
	}else if(name=="m4v"){
			pathIco=Localpath+L"\\KKIcons.dll,-106"; 
			FileRelation.RegisterFileRelation(_T(".m4v"),(LPTSTR)Propath.c_str(),_T("KKPlayer.m4v"),(LPTSTR)pathIco.c_str(),_T(".m4v"));
	}else if(name=="mp4"){
			pathIco=Localpath+L"\\KKIcons.dll,-107"; 
			FileRelation.RegisterFileRelation(_T(".mp4"),(LPTSTR)Propath.c_str(),_T("KKPlayer.mp4"),(LPTSTR)pathIco.c_str(),_T(".mp4"));
	}else if(name=="mpg"){
			pathIco=Localpath+L"\\KKIcons.dll,-108"; 
			FileRelation.RegisterFileRelation(_T(".mpg"),(LPTSTR)Propath.c_str(),_T("KKPlayer.mpg"),(LPTSTR)pathIco.c_str(),_T(".mpg"));
	}else if(name=="rm"){
			pathIco=Localpath+L"\\KKIcons.dll,-109"; 	       
			FileRelation.RegisterFileRelation(_T(".rm"),(LPTSTR)Propath.c_str(),_T("KKPlayer.rm"),(LPTSTR)pathIco.c_str(),_T(".rm"));
	}else if(name=="rmvb"){
			pathIco=Localpath+L"\\KKIcons.dll,-110"; 
			FileRelation.RegisterFileRelation(_T(".rmvb"),(LPTSTR)Propath.c_str(),_T("KKPlayer.rmvb"),(LPTSTR)pathIco.c_str(),_T(".rmvb"));
	}else if(name=="wma"){
			pathIco=Localpath+L"\\KKIcons.dll,-111"; 
			FileRelation.RegisterFileRelation(_T(".wma"),(LPTSTR)Propath.c_str(),_T("KKPlayer.wma"),(LPTSTR)pathIco.c_str(),_T(".wma"));
	}
	
}

///读写权限。
BOOL EnableDebugPriv()  
{  
    HANDLE hToken;  
    LUID sedebugnameValue;  
    TOKEN_PRIVILEGES tkp;  
  
    if ( ! OpenProcessToken( GetCurrentProcess(),  
        TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken ) )  
        return false;  
  
    if ( ! LookupPrivilegeValue( NULL, SE_DEBUG_NAME, &sedebugnameValue ) )  
    {  
        CloseHandle( hToken );  
        return false;  
    }  
  
    tkp.PrivilegeCount = 1;  
    tkp.Privileges[0].Luid = sedebugnameValue;  
    tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;  
  
    if ( ! AdjustTokenPrivileges( hToken, FALSE, &tkp, sizeof tkp, NULL, NULL ) )  
        CloseHandle(hToken);
    return true;  
}

BOOL EnableBackupPriv()
{
HANDLE hToken;
LUID sedebugnameValue;
TOKEN_PRIVILEGES tkp;

if ( ! OpenProcessToken( GetCurrentProcess(),
   TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken ) )
   return false;

if ( ! LookupPrivilegeValue( NULL, SE_BACKUP_NAME, &sedebugnameValue ) )
{
   CloseHandle( hToken );
   return false;
}

tkp.PrivilegeCount = 1;
tkp.Privileges[0].Luid = sedebugnameValue;
tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

if ( ! AdjustTokenPrivileges( hToken, FALSE, &tkp, sizeof tkp, NULL, NULL ) )
   CloseHandle( hToken );

return true;
}

void LoadPlugin();
int WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE /*hPrevInstance*/, LPTSTR /*lpstrCmdLine*/, int /*nCmdShow*/)
{
	
    HRESULT hRes = CoInitialize(0);
	int nArgs = 0;   
    DeclareDumpFile();
	EnableDebugPriv();
	//EnableBackupPriv();
    ///电源管理
	::SetThreadExecutionState(ES_SYSTEM_REQUIRED | ES_DISPLAY_REQUIRED |ES_CONTINUOUS);
	LPWSTR *szArglist = NULL;   
	std::wstring  urlpath;
	szArglist = CommandLineToArgvW(GetCommandLineW(), &nArgs);   
	if( NULL != szArglist)   
	{   
         for(int i=1;i<nArgs;i++)
		 {
		     urlpath+=*(szArglist+i);
			 if(i!=nArgs-1)
			   urlpath+=L" ";
		 }
	} 
	LocalFree(szArglist);

    
    
   
	FARPROC spdpia = GetProcAddress(GetModuleHandle(TEXT("user32")), "SetProcessDPIAware");  
	if(spdpia!=NULL)
	{
	   spdpia();
	}

	if (!SingletonRunInit())
	{
		return -1;
	}
	//SingletonRunInit();

	UpDateCheckTask updateTask;
	updateTask.start();
	CWinDir windir;
	std::wstring Propath=windir.GetUserAppDir();
	Propath+=L"\\kkplayer\\";
	windir.CreateDir(Propath.c_str());
	Propath+=L"DB";
	std::string pp;
	CChineseCode::UnicodeToUTF8((wchar_t*)Propath.c_str(),pp);/**/

	///关联图标
	//RelationAllIco();
	
	

	CHistoryInfoMgr *hismgr=CHistoryInfoMgr::GetInance();
	hismgr->SetPath(pp.c_str());
	hismgr->InitDb();
	hismgr->GetH264Codec();
	hismgr->GetH265Codec();

	std::string StrWorkDir=CUiInfo::GetStoreDir();
	if(StrWorkDir.length()<1)
	{
		wchar_t Appdir[512]=L"";
		windir.GetBestDrive(Appdir,512);
		_tcscat(Appdir,_T("kkMedia\\"));
		windir.CreateDir(Appdir);

		char Appdir2[1024]="";
	    CChineseCode::wcharTochar(Appdir,Appdir2,1024);
		CUiInfo::SetStoreDir(Appdir2);
	}/*else{
		CChineseCode::charTowchar(StrWorkDir.c_str(),Appdir,512);
	    windir.CreateDir(Appdir);
	}*/
	

	//加载插件
	LoadPlugin();

	
	HMODULE hRender = LoadLibraryA("Render.dll");
	if(hRender){
          pfnCreateRender = (CreateRender)GetProcAddress(hRender, "CreateRender");
		  pfnDelRender = (FnDelRender)GetProcAddress(hRender, "DelRender");
	}

	HMODULE hytburl=LoadLibrary(_T("ytburl.dll"));
	if(hytburl){
		    pfpGetUrl=(fpKKPlayerGetUrl )GetProcAddress(hytburl, "GetUrl");
	}

	SComMgr * pComMgr = new SComMgr;

	
    SOUI::CAutoRefPtr<SOUI::IRenderFactory> pRenderFactory=NULL;
	///GDI渲染器
	pComMgr->CreateRender_GDI((IObjRef**)&pRenderFactory);
	
	///图片解码器
	SOUI::CAutoRefPtr<SOUI::IImgDecoderFactory> pImgDecoderFactory;
	pComMgr->CreateImgDecoder((IObjRef**)&pImgDecoderFactory);
	pRenderFactory->SetImgDecoderFactory(pImgDecoderFactory);
	
	
	SOUI::SApplication *theApp=new SApplication(pRenderFactory,hInstance);

    theApp->RegisterWindowClass<CSuiVideo>();
    theApp->RegisterWindowClass<CKKmclv>();
    theApp->RegisterWindowClass<SAVSeekBar>();
	theApp->RegisterWindowClass<SMCListViewEx>();
	theApp->RegisterWindowClass<SHeaderCtrlEx>();

	//注册控件
	theApp->RegisterWindowClass<SGifPlayer>();
	theApp->RegisterSkinClass<SSkinGif>();//注册SkinGif
    theApp->RegisterSkinClass<SSkinAPNG>();//注册SSkinAPNG

	 KKWkeLoader wkeLoader;
	 if(wkeLoader.Init(_T("wke.dll")))
	 {
	   theApp->RegisterWindowClass<KKWkeWebkit>();
	 }/**/
    
	
	 ///不知道什么原因，在win10下，会导致百度网盘等闪烁,原来需要管理员权限就会闪烁啊
	 HMODULE hui=LoadLibrary(_T("kkui.dll"));
	if(hui){
		CAutoRefPtr<IResProvider>   pResProvider;
		CreateResProvider(RES_PE,(IObjRef**)&pResProvider);
		BOOL ret=pResProvider->Init((WPARAM)hui,0);
		theApp->AddResProvider(pResProvider);
	}else{
       assert(0);
	}
	//加载系统资源
	HMODULE hSysResource=LoadLibrary(SYS_NAMED_RESOURCE);
	if(hSysResource){
		CAutoRefPtr<IResProvider> sysSesProvider;
		CreateResProvider(RES_PE,(IObjRef**)&sysSesProvider);

		sysSesProvider->Init((WPARAM)hSysResource,0);
		theApp->LoadSystemNamedResource(sysSesProvider);
	}

	
	
	CAutoRefPtr<ITranslatorMgr> trans;
	pComMgr->CreateTranslator((IObjRef**)&trans);
	if(trans)
	{
	        theApp->SetTranslator(trans);
            pugi::xml_document xmlLang;
            if(theApp->LoadXmlDocment(xmlLang,_T("lang_cn"),_T("translator"))){
                CAutoRefPtr<ITranslator> langCN;
                trans->CreateTranslator(&langCN);
                langCN->Load(&xmlLang.child(L"language"),1);//1=LD_XML
                trans->InstallTranslator(langCN);
            }
	}


	

	int 	nRet=0;
	SOUI::SStringT str;
	SOUI::CMainDlg dlgMain;
	dlgMain.Create(NULL,WS_POPUP|WS_CLIPCHILDREN| WS_CLIPSIBLINGS,0,0,0,0,0);
	dlgMain.GetNative()->SendMessage(WM_INITDIALOG);
	dlgMain.CenterWindow(dlgMain.m_hWnd);
	dlgMain.ShowWindow(SW_SHOWNORMAL);
	if(urlpath.length()>3)
	{
	    char localurl[1024]="";
	    CChineseCode::wcharTochar(urlpath.c_str(), localurl,1024);
		dlgMain.WinTabShow(1);
	    dlgMain.OpenMedia(localurl);
	}
	::SwitchToThisWindow(dlgMain.m_hWnd,TRUE);
	seg_hMainWindow=dlgMain.m_hWnd;
	nRet = theApp->Run(dlgMain.m_hWnd);/**/

 
	
	
	::SetThreadExecutionState(ES_CONTINUOUS);

	//delete theApp;
    //delete pComMgr;
    OleUninitialize();

	return nRet;
}
