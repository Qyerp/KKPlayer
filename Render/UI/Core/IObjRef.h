///链接计数管理
#ifndef KKUI_IObjRef_H_
#define KKUI_IObjRef_H_
namespace KKUI
{
	class IObjRef
	{
	public:
		virtual long AddRef() = 0;

		virtual long Release()  = 0;
		
		virtual void OnFinalRelease()  = 0;
	};
}
#endif
