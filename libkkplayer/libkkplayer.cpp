// libkkplayer.cpp : 定义 DLL 应用程序的导出函数。
//
#include "../KKPlayer/stdafx.h"
#include "../KKPlayer/MainFrm.h"
#include "../KKPlayerCore/kkAvTool.h"
#include "../KKPlayerCore/VideoRefresMgr.h"
CreateRender pfnCreateRender=NULL;
FnDelRender pfnDelRender=NULL;
fpKKPlayerGetUrl     pfpGetUrl=NULL;
CVideoRefresMgr*     G_pVideoRefresMgr;
////
void Init(){

	if(!G_pVideoRefresMgr){
	   G_pVideoRefresMgr = new CVideoRefresMgr();
	   G_pVideoRefresMgr->Init();
	}
	if(pfnCreateRender==NULL || pfnDelRender==NULL){
		SetDllDirectory(_T("D:\\Work\\KKPlayer\\Debug"));
		HMODULE hRender = LoadLibraryA("Render.dll");
		//HMODULE hRender = LoadLibraryEx(_T("D:\\Work\\KKPlayer\\Debug\\Render.dll"), NULL, LOAD_WITH_ALTERED_SEARCH_PATH);
		if(hRender){
			  pfnCreateRender = (CreateRender)GetProcAddress(hRender, "CreateRender");
			  pfnDelRender = (FnDelRender)GetProcAddress(hRender, "DelRender");
		}
	}
}

void SetKKplayerH264HardCodec(int value);
void SetKKplayerH265HardCodec(int value);

void SetKKplayerHapHardCodec(int value);
void SetKKplayerDxvHardCodec(int value);
extern "C"{
	

	
	void   __declspec(dllexport) SetH264HardCodec(int value)
	{
		if(value){
		     SetKKplayerH264HardCodec(SKK_VideoState::HARD_CODE_DXVA);
		}else {
             SetKKplayerH264HardCodec(SKK_VideoState::HARD_CODE_NONE);
		}
	}
	void   __declspec(dllexport) SetH265HardCodec(int value)
	{
		if(value){
             SetKKplayerH265HardCodec(SKK_VideoState::HARD_CODE_DXVA);
		}else{
		     SetKKplayerH265HardCodec(SKK_VideoState::HARD_CODE_NONE);
		}
	}
	void   __declspec(dllexport) SetDxvHardCodec(int value)
	{
		/*if(value){
             SetKKplayerDxvHardCodec(SKK_VideoState::HARD_CODE_DXTEXTURE);
		}else{
		    SetKKplayerDxvHardCodec(SKK_VideoState::HARD_CODE_NONE);
		}*/
	}
	void   __declspec(dllexport) SetHapHardCodec(int value)
	{
		/*if(value){
             SetKKplayerHapHardCodec(SKK_VideoState::HARD_CODE_DXTEXTURE);
		}else{
		     SetKKplayerHapHardCodec(SKK_VideoState::HARD_CODE_NONE);
		}*/
	}

	void __declspec(dllexport) RegisterPlayerIoPlg(KKPluginInfo* info)
    {
		KKPlayer::AddKKPluginInfo(*info);
    }
	///创建一个带窗口的播放器
	void __declspec(dllexport) *CreateKKPlayer(HWND h,RECT Rt,DWORD style,HWND *OutHwnd,bool yuv420)
	{
			Init();
			RECT rt={0,100,200,300};
			//WS_CHILDWINDOW| WS_CLIPCHILDREN 
			CMainFrame *m_pVideoWnd = new CMainFrame(yuv420,true);
			
			if(m_pVideoWnd->CreateEx(h,Rt, style) == NULL){
					return 0;
			}
			
			*OutHwnd=m_pVideoWnd->m_hWnd;
			return m_pVideoWnd;
	}

	//创建一个无窗口的播放器
	void __declspec(dllexport) *CreateDuiKKPlayer(HWND hAudio,fpRenderImgCall fp,void *RenderUserData)
	{
			Init();
			RECT rt={0,100,200,300};
			CMainFrame *m_pVideoWnd = new CMainFrame(false,true);
			m_pVideoWnd->SetDuiDraw(hAudio,fp,RenderUserData);
			return m_pVideoWnd;
	}
	//typedef  void (*) (kkAVPicRAWInfo* data,void* UserData);

	//创建一个无窗口的播放器
	void __declspec(dllexport) *CreateDuiRawKKPlayer(HWND hAudio,fpRenderImgCall fp,void *RenderUserData,int imgType)
	{
			Init();
			RECT rt={0,100,200,300};
			CMainFrame *m_pVideoWnd = new CMainFrame(false,true);
			m_pVideoWnd->SetDuiDraw(hAudio,fp,RenderUserData,true);
			return m_pVideoWnd;
	}

	//创建一个无窗口的播放器,使用统一的线程刷新
	void __declspec(dllexport) *CreateDuiRawKKPlayer2(HWND hAudio,fpRenderImgCall fp,void *RenderUserData)
	{
			Init();
			RECT rt={0,100,200,300};
			CMainFrame *m_pVideoWnd = new CMainFrame(false,true);
			m_pVideoWnd->SetDuiDraw(hAudio,fp,RenderUserData,true);
            m_pVideoWnd->EnbleUniformRefresh();
			if(G_pVideoRefresMgr){
               G_pVideoRefresMgr->AddPlayer(m_pVideoWnd);
			}
			return m_pVideoWnd;
	}

	/******添加输入格式插件********/
	void __declspec(dllexport) RegisterInputFormat(void* format)
	{
	
	    AVInputFormat *InFormat=(AVInputFormat*)format;
		KKPlayer::RegisterInputFormat(InFormat);
	}

	//注册一个解码器
	void __declspec(dllexport) RegisterAvDecoder(void* decoder)
    {
       KKPlayer::RegisterAvDecoder((AVCodec*)decoder);
    }

	//设置呈现
	void __declspec(dllexport) SetKKPlayerRenderInfo(void* player ,fpRenderImgCall fp,void *RenderUserData)
	{
			CMainFrame *m_pVideoWnd =static_cast<CMainFrame  *>(player);
			if(m_pVideoWnd)
			m_pVideoWnd->SetRenderInfo(fp,RenderUserData);
	}


	void __declspec(dllexport) RegisterAvPrepareDecompress(FnPrepareDecompress fn)
	{
	    KKPlayer::G_fpPrepareDecompress=(fpPrepareDecompress)fn;
	}
	

    //设置呈现
	void __declspec(dllexport) SetKKPlayerCloseAudio(void* player )
	{
			CMainFrame *m_pVideoWnd =static_cast<CMainFrame  *>(player);
			if(m_pVideoWnd)
			  m_pVideoWnd->CloseAudio();
	}

	void  __declspec(dllexport) RelativeAvSeek(void* player,int value)
	{
	{
	     CMainFrame *Player = static_cast<CMainFrame *>(player);
		 if(Player!=NULL){
			 Player->RelativeAvSeek(value);
		 }
	}
	}

	void  __declspec(dllexport) KKAvSeek(void* player,int value)
	{
	     CMainFrame *Player = static_cast<CMainFrame *>(player);
		 if(Player!=NULL){
			 Player->AvSeek(value);
		 }
	}
	void  __declspec(dllexport) KKSetPlayAvModel(void* player,int model)	{
	     CMainFrame *Player = static_cast<CMainFrame *>(player);
		 if(Player!=NULL){
			 Player->SetPlayAvModel(model);
		 }
	}
	///建议已100ms调用
	void __declspec(dllexport) RefreshDuiKKPlayer(void* player)
	{
		IKKPlayUI  *Player = static_cast<CMainFrame  *>(player);
		 if(Player!=NULL){
			 Player->AVRender();
		 }
	}

   void __declspec(dllexport) KKSetErrNotify(void* player,fpKKPlayerErrNotify noti,void* UserData)
   {
         CMainFrame *Player = static_cast<CMainFrame *>(player);
		 if(Player!=NULL){
		    Player-> SetErrNotify(UserData,noti);
		 }
   }

   void __declspec(dllexport) KKPlayPause(void* player,int p)
   {
         CMainFrame *Player = static_cast<CMainFrame *>(player);
		 if(Player!=NULL){
			 Player->Pause(p);
		 }
   }
   void  __declspec(dllexport) KKPlayRetArg(void* player,const char* cmd)
   {
         CMainFrame *Player = static_cast<CMainFrame *>(player);
		 if(Player!=NULL){
			 Player->RetArg(cmd);
		 }
   }
   void __declspec(dllexport) KKPlayResetPlayIni(void* player)
   {
         CMainFrame *Player = static_cast<CMainFrame *>(player);
		 if(Player!=NULL){
			 Player->ResetPlayIni();
		 }
   }

   void __declspec(dllexport) KKDuiOnSize(void* player,int w,int h)
   {
         CMainFrame *Player = static_cast<CMainFrame *>(player);
		 if(Player!=NULL){
			 BOOL hhh=false;
			 Player->OnSize(0,w,h,hhh);
		 }
   }	

   void  __declspec(dllexport) SetMaxRealtimeDelay(void* player,double Delay)
   {
        CMainFrame *Player = static_cast<CMainFrame *>(player);
		if(Player!=NULL){
			Player->SetMaxRealtimeDelay(Delay);
		}
   }
   int __declspec(dllexport) KKOpenMedia(void* player,const char* url,const char* cmd)
   {
	   
         CMainFrame *Player = static_cast<CMainFrame *>(player);
		 if(Player!=NULL)
		 {
	             return	 Player->OpenMedia(url,cmd);
		 }
		 return -2;
   }
   
   void __declspec(dllexport) KKCloseMedia(void* player)
   {
          CMainFrame *Player = static_cast<CMainFrame *>(player);
		  if(Player!=NULL){
		      Player->CloseMedia();
		  }
   }
   void __declspec(dllexport) KKSetVolume(void* player,int volume,bool tip)
   {
        CMainFrame *Player = static_cast<CMainFrame *>(player);
		if(Player!=NULL){
		 Player->SetVolume(volume,tip);
		}
   }
   void __declspec(dllexport) KKDelPlayer(void* player,bool dui)
   {
        CMainFrame *Player = static_cast<CMainFrame *>(player);
		if(Player!=NULL){
			if(dui){
				if(G_pVideoRefresMgr){
                    G_pVideoRefresMgr->RemovePlayer(Player);
			    }
				Player->m_hWnd=0;
				delete Player;
			}else{
			     if(::IsWindow(Player->m_hWnd))
			        ::SendMessage(Player->m_hWnd,WM_CLOSE,0,0);
			}
			// 
		}
   }

   void __declspec(dllexport) KKGetPlayerTime(void* player,int* playTime,int* TotalTime)
   {
        CMainFrame *Player = static_cast<CMainFrame *>(player);
		if(Player!=NULL){
			*playTime   = Player->GetPlayTime();
	        *TotalTime  = Player->GetTotalTime();
		}
   }

  
   int __declspec(dllexport) GetMediaInfo(const char* path,char** OutJson,int NeePic,int sec,int *NeedWidth,int *NeedHeight,char** BGRA,int* BGRAlen)
   {
	   if(!path)
		   return -1;
       CKkAvTool tool;
	   return tool.GetMediaInfo(path,OutJson,NeePic,sec, *NeedWidth,*NeedHeight,BGRA,BGRAlen);
   }
   void __declspec(dllexport) KKFree(void* ptr){
        KKPlayerFree(ptr);
   }

  
}
