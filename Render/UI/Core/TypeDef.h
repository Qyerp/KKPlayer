#ifndef  TypeDef_H_
#define TypeDef_H_
#ifdef WIN64
  typedef unsigned long long   uint_ptr;
  typedef long long   int_ptr;
#else
  typedef unsigned int   uint_ptr;
  typedef unsigned long  int_ptr;
#endif
#endif