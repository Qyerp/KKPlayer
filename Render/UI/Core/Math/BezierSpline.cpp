#include "BezierSpline.h"
#include <assert.h>
#include <math.h>
#include <math.h>
 
 
//static double round(double r)
//{
//	return (r > 0.0) ? floor(r + 0.5) : ceil(r - 0.5);
//}

namespace KKUI
{

	CBezierSpline:: CBezierSpline()
	{
	
	}
	CBezierSpline::~CBezierSpline()
	{
	
	}
	void CBezierSpline::GetFirstControlPoints( const std::vector<float>& rhs, std::vector<float>& x )
	{
		 assert(rhs.size()==x.size());
		 int n = rhs.size();
		 std::vector<float> tmp(n);    // Temp workspace.

		 float b = 2.0f;
		 x[0] = rhs[0] / b;
		 for (int i = 1; i < n; i++) // Decomposition and forward substitution.
		 {
			 tmp[i] = 1 / b;
			 b = (i < n-1 ? 4.0f : 3.5f) - tmp[i];
			 x[i] = (rhs[i] - x[i-1]) / b;
		 }
		 for (int i = 1; i < n; i++)
		 {
			 x[n-i-1] -= tmp[n-i] * x[n-i]; // Back substitution.
		 }
	}

	void   CBezierSpline::GetCurveControlPoints(const std::vector<SUVInfo>& knots,std::vector<SUVInfo>& firstCtrlPt,std::vector<SUVInfo>& secondCtrlPt)
	{
			 assert( (firstCtrlPt.size()==secondCtrlPt.size())
			 && (knots.size()==firstCtrlPt.size()+1) );

			  int n = knots.size()-1;
			  assert(n>=1);

			 if (n == 1)
			 { 
				 // Special case: Bezier curve should be a straight line.
				 // 3P1 = 2P0 + P3
				 firstCtrlPt[0].x = (2 * knots[0].x + knots[1].x) / 3.0f;
				 firstCtrlPt[0].y = (2 * knots[0].y + knots[1].y) / 3.0f;

				 // P2 = 2P1 �C P0
				 secondCtrlPt[0].x = 2 * firstCtrlPt[0].x - knots[0].x;
				 secondCtrlPt[0].y = 2 * firstCtrlPt[0].y - knots[0].y;
				 return;
			 }

			 // Calculate first Bezier control points
			 // Right hand side vector
			 std::vector<float> rhs(n);

			 // Set right hand side X values
			 for (int i = 1; i < (n-1); ++i)
			 {
				 rhs[i] = 4 * knots[i].x + 2 * knots[i+1].x;
			 }
			 rhs[0] = knots[0].x + 2 * knots[1].x;
			 rhs[n-1] = (8 * knots[n-1].x + knots[n].x) / 2.0f;
			 // Get first control points X-values
			 std::vector<float> x(n);
			 GetFirstControlPoints(rhs,x);

			 // Set right hand side Y values
			 for (int i = 1; i < (n-1); ++i)
			 {
				 rhs[i] = 4 * knots[i].y + 2 * knots[i+1].y;
			 }
			 rhs[0] = knots[0].y + 2 * knots[1].y;
			 rhs[n-1] = (8 * knots[n-1].y + knots[n].y) / 2.0f;
			 // Get first control points Y-values
			 std::vector<float> y(n);
			 GetFirstControlPoints(rhs,y);

			 // Fill output arrays.
			 for (int i = 0; i < n; ++i)
			 {
				 // First control point
				 firstCtrlPt[i].x = x[i];
				 firstCtrlPt[i].y = y[i];
				 // Second control point
				 if (i < (n-1))
				 {
					 secondCtrlPt[i].x = 2 * knots[i+1].x - x[i+1];
					 secondCtrlPt[i].y = 2*knots[i+1].y-y[i+1];
				 }
				 else
				 {
					 secondCtrlPt[i].x = (knots[n].x + x[n-1])/2;
					  secondCtrlPt[i].y =(knots[n].y+y[n-1])/2;
				 }
			 }
	}


	void CBezierSpline::GetBezier(SUVInfo xy1, SUVInfo xy2, SUVInfo cp1, SUVInfo cp2, std::vector<SUVInfo>& XyVec)
	{
		/*float prev_x = (int)round(xy1.x);
		float prev_y = (int)round(xy1.y);
		float last_x = (int)round(xy2.x);
		float last_y = (int)round(xy2.y);*/
	 
		float DELTA_S	=		0.01;
		for (double s = 0.0; s < (1.0 + 0.00001); s += DELTA_S)
		{
			double J0 = pow(1 - s, 3);
			double J1 = pow(1 - s, 2) * s * 3;
			double J2 = pow(s, 2) * (1 - s) * 3;
			double J3 = pow(s, 3);
	 
			double ptx = xy1.x * J0 + cp1.x * J1 + cp2.x * J2 + xy2.x * J3;
			double pty = xy1.y * J0 + cp1.y * J1 + cp2.y * J2 + xy2.y * J3;
	 
			//int iptx = (int)round(ptx);
			//int ipty = (int)round(pty);
	 
			SUVInfo info;
			info.x = ptx;
			info.y = pty;
			XyVec.push_back(info);
		}
		return;
	}

}