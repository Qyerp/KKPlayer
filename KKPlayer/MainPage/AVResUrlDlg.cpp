#include "AVResUrlDlg.h"
#include "MainDlg.h"
#include "../Tool/cchinesecode.h"
#include "UiInfo.h"
#include "Model/IoPluginAdapter.h"
#include "control/SMCListViewEx.h"
#include <string>
#include <vector>
#include <set>
#include <ShlObj.h>
#include "KKPlayer.h"
///关联文件图标
void RelationIco(const std::string& name);
///获取关联信息
void GetRelationIcoInfo(std::set<std::string>& infoset);
namespace SOUI
{
	CAVResUrlDlg::CAVResUrlDlg(CMainDlg *pDlgMain): SHostDialog(_T("LAYOUT:XML_AvResUrl")),m_pDlgMain(pDlgMain)
	{
	}
	CAVResUrlDlg::~CAVResUrlDlg()
	{
        
	}
	
	void  CAVResUrlDlg::OnOk()
	{
		SStringT Wurl;
	    Wurl=FindChildByName("EditAVResUrl")->GetWindowText();

		char urlx[2048]="";
		CChineseCode::wcharTochar(Wurl.GetBuffer(1024),urlx,2048);
		Wurl.ReleaseBuffer();
		CUiInfo::SetResUrl(urlx);
		m_pDlgMain->ReLoadResUrl();
	}
	void GetChekAllTip(SWindow* win_format_1,std::vector<std::string>& fomvec)
	{
	    int count=win_format_1->GetChildrenCount();
		for(int i=0;i<count;i++)
		{
			char name[64]="";
			sprintf(name,"chk_%d",i+1);
			SWindow* chk=  win_format_1->FindChildByName(name);
		   if(chk->IsChecked())
		   {
			   SStringT tip=chk->GetToolTipText();
			   
               char xxx[256]="";
			   CChineseCode::wcharTochar(tip.GetBuffer(128),xxx,256);
			   fomvec.push_back(xxx);
		   }
		}
	}
	void CAVResUrlDlg::OnBtnAvFile()
	{
	
		std::vector<std::string> fomvec;
		SWindow* win_format_1=FindChildByName("win_format_1");
	    GetChekAllTip(win_format_1,fomvec);
        win_format_1=FindChildByName("win_format_2");
	    GetChekAllTip(win_format_1,fomvec);
	
		for(int i=0;i<fomvec.size();i++){
			
			std::string &name=fomvec.at(i);
		    RelationIco(name);
		}
	}
	void CAVResUrlDlg::OnClose()
	{
		OnOK();
	}

	void RelationIcoInfo(SWindow* win_format_1,std::set<std::string>& infoset)
	{
	   int count=win_format_1->GetChildrenCount();
		for(int i=0;i<count;i++)
		{
			char name[64]="";
			char xxx[256]="";
			sprintf(name,"chk_%d",i+1);
			SWindow* chk=  win_format_1->FindChildByName(name);
			//assert(chk);
			SStringT tip=chk->GetToolTipText();
			CChineseCode::wcharTochar(tip.GetBuffer(128),xxx,256);
			std::set<std::string>::iterator It=infoset.find(xxx);
			if(chk&&It!=infoset.end()){
				chk->SetCheck(1);
			}  
		}
	}
	BOOL CAVResUrlDlg::OnInitDialog( HWND hWnd, LPARAM lParam )
	{

		std::string url=CUiInfo::GetResUrl();
		wchar_t Wurl[2048]=L"";
		CChineseCode::charTowchar(url.c_str(),Wurl,2048);
		FindChildByName("EditAVResUrl")->SetWindowText(Wurl);

		
        std::list<KKPluginInfo>  IoPlugin=KKPlayer::GetKKPluginInfoList();
		std::list<KKPluginInfo>::iterator ItIo=IoPlugin.begin();
	
		std::vector<IoDataItem> DataItemVector;
		for(;ItIo!=IoPlugin.end();ItIo++){
		        IoDataItem item;
				CChineseCode::charTowchar(ItIo->ptl,item.name,32);
				CChineseCode::charTowchar(ItIo->autor,item.author,32);
				wsprintf(item.ver,L"%d",ItIo->ver );
				DataItemVector.push_back(item);
		}

		SMCListViewEx* view=(SMCListViewEx*)FindChildByName("mclv_IoPlugin");
		CIoPluginAdapterFix* ad = new CIoPluginAdapterFix(view);
		ad->SetData(DataItemVector);
		view->SetAdapter(ad);
		ad->Release();

		


		std::set<std::string> infoset;
		GetRelationIcoInfo(infoset);
		SWindow* win_format_1=FindChildByName("win_format_1");
	    RelationIcoInfo(win_format_1,infoset);
        win_format_1=FindChildByName("win_format_2");
	    RelationIcoInfo(win_format_1,infoset);

		std::string dir=CUiInfo::GetStoreDir();
		memset(Wurl,0,2048);
		CChineseCode::charTowchar(dir.c_str(),Wurl,2048);
		FindChildByName("EdiStoreDir")->SetWindowText(Wurl);
	    return 1;
	}

	static int CALLBACK _SHBrowseForFolderCallbackProc( HWND hwnd, UINT uMsg, LPARAM lParam, LPARAM lpData )  
{  
    switch(uMsg)  
    {  
    case BFFM_INITIALIZED:      
        ::SendMessage(hwnd,BFFM_SETSELECTION,TRUE,lpData);   //传递默认打开路径 break;  
    case BFFM_SELCHANGED:    //选择路径变化，BFFM_SELCHANGED  
        {  
            char curr[MAX_PATH];     
            SHGetPathFromIDList((LPCITEMIDLIST)lParam,(LPWSTR)(LPCTSTR)curr);     
            ::SendMessage(hwnd,BFFM_SETSTATUSTEXT,0,(LPARAM)&curr[0]);    
        }  
        break;  
    default:  
        break;  
    }   
    return 0;  
}  
	void  CAVResUrlDlg::SelectStoreDir()
	{
		SStringT dirStr=FindChildByName("EdiStoreDir")->GetWindowText();
		wchar_t* path=dirStr.GetBuffer(128);
	        TCHAR pszPath[MAX_PATH];  
			BROWSEINFO bi;   
			bi.hwndOwner      = m_hWnd;
			bi.pidlRoot       = NULL;  
			bi.pszDisplayName = NULL;   
			bi.lpszTitle      = TEXT("请选择文件夹");   
			bi.ulFlags        =BIF_DONTGOBELOWDOMAIN | BIF_RETURNONLYFSDIRS | BIF_NEWDIALOGSTYLE ;  // BIF_RETURNONLYFSDIRS | BIF_STATUSTEXT;  
			bi.lpfn           = _SHBrowseForFolderCallbackProc  ;
			bi.lParam         = (LPARAM)path;  
			bi.iImage         = 0;   
		 
			LPITEMIDLIST pidl = SHBrowseForFolder(&bi);  
			if (pidl == NULL)  
			{  
				return;  
			}  
		  
			if (SHGetPathFromIDList(pidl, pszPath))  
			{  
				 FindChildByName("EdiStoreDir")->SetWindowText(pszPath);
			}  
	}

	void  CAVResUrlDlg::OnbtnSaveDir()
	{
	     	SStringT dirStr=FindChildByName("EdiStoreDir")->GetWindowText();
			char xxx[256]="";
			CChineseCode::wcharTochar(dirStr.GetBuffer(128),xxx,256);
			CUiInfo::SetStoreDir(xxx);

			KKPlayer::SetStoreInfo(xxx,0,0);
	}
}