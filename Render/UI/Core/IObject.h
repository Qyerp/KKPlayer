#ifndef IObject_H_
#define IObject_H_
#include "TString.h"
#include "pugixml/pugixml.hpp"
#define KKUI_CLASS_NAME_EX(parentclass,theclass, classname,clsType) \
public:                                                 \
    static char* GetClassName()                       \
    {                                                   \
        return classname;                               \
    }                                                   \
    \
	static int GetClassType()                           \
    {                                                   \
        int ret = clsType;                              \
		if(ret == KKUI::Undef)                                \
			ret = parentclass::GetClassType();              \
		return ret;                                     \
    }                                                   \
    \
    static char* BaseClassName()                      \
	{                                                   \
		return parentclass::GetClassName();                 \
	}                                                   \
	\
	virtual char* GetObjectClass()  const             \
	{                                                   \
		return classname;                               \
	}                                                   \
	\
	virtual int GetObjectType()  const                  \
	{                                                   \
        int ret = clsType;                              \
		if(ret == KKUI::Undef)                          \
			ret = parentclass::GetObjectType();             \
		return ret;                                     \
	}  \
	virtual int IsClass(char* lpszName) const        \
	{                                                   \
		if(strcmp(GetClassName(), lpszName)  == 0)      \
			return 1;                                \
		return parentclass::IsClass(lpszName);              \
	}   
	
#define KKUI_CLASS_NAME(parentclass,theclass, classname) \
	KKUI_CLASS_NAME_EX(parentclass,theclass,classname,0)
namespace KKUI
{
	enum EObjectType
	{
		None = -1,
		Undef = 0,
		UICtl,
		Skin,
		Layout,
		Style
	};
	class IObject
	{
	  public:
		      virtual ~IObject(){}
		      virtual int             GetObjectType()                    const = 0;
			  virtual char*           GetObjectClass()                   const = 0;
			  virtual int             IsClass(char* lpszName)            const = 0;
		      virtual int             InitFromXml( pugi::xml_node xmlNode ) = 0;
			  virtual void            OnInitFinished(pugi::xml_node xmlNode) = 0;
			  virtual int             SetAttribute(const SStringA &  strAttribName, const SStringA &  strValue, int bLoading) = 0;
			  virtual int             AfterAttribute(const SStringA & strAttribName,const SStringA & strValue, int bLoading,int hr) =0;
			  virtual int             DefAttributeProc(const SStringA & strAttribName,const SStringA & strValue, int bLoading) = 0;
			  static  char* GetClassName(){
				  return "object";
			  }
			  static  int GetClassType(){
				  return None;
			  }

			  static void MarkAttributeHandled(pugi::xml_attribute xmlAttr, bool bHandled)
				{
					xmlAttr.set_userdata(bHandled?1:0);
				}


				/**
				 * IsAttributeHandled
				 * @brief    检测一个属性是否已经被处理
				 * @param    pugi::xml_node xmlNode --  属性节点
				 * @return   bool true-已经处理过
				 * Describe  
				 */
				static bool IsAttributeHandled(pugi::xml_attribute xmlAttr)
				{
					return xmlAttr.get_userdata()!=0;
				}

	};

	template<class T>
    T * UIObj_cast(IObject *pObj)
    {
        if(!pObj)
            return 0;

        if(pObj->IsClass(T::GetClassName()))
            return (T*)pObj;
        else
            return 0;
    }
}
#endif