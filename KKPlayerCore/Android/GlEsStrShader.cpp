const char* g_Yuv_Vertex_shader =  
        "precision mediump  float; \n"
        "varying    vec2 v_Texcoord; \n"
        "attribute vec4 av4_Position; \n"
        "attribute  vec2 av2_Texcoord; \n"
        "uniform         mat4 um4_ModelViewProjection; \n"
		
        "void main() \n"
        "{ \n"
        "    gl_Position  = um4_ModelViewProjection * av4_Position; \n"
        "    v_Texcoord = av2_Texcoord; \n"
        "} \n";
//bt601   YUV420
//Shader.frag文件内容
const char* g_Yuv_Frag_shader =
        "precision mediump  float;  \n"
                "varying   vec2 v_Texcoord;\n"
                "uniform   sampler2D tex_y;\n"
                "uniform   sampler2D tex_u;\n"
                "uniform   sampler2D tex_v;\n"
                "void main()\n"
                "{\n"
                "   mediump vec3 yuv;\n"
                "   lowp    vec3 rgb;\n"
				"   const  vec3 offset = vec3(-0.0627451017, -0.501960814, -0.501960814);\n" 
				"   const  mat3  bt601=  mat3( 1.1644,  1.1644,   1.1644,\n"        \
"                          0,      -0.2132,   2.1124,\n"        \
"                          1.7927, -0.5329,   0);\n"            \       
                "   yuv.x = texture2D(tex_y, v_Texcoord).r;\n"
                "   yuv.y = texture2D(tex_u, v_Texcoord).r;\n"
                "   yuv.z = texture2D(tex_v, v_Texcoord).r;\n"
				"   yuv += offset;\n"         
                "   rgb = bt601* yuv;\n"
                "   gl_FragColor = vec4(rgb, 1);\n"
				"   gl_FragColor.r*=1.0;"
				"   gl_FragColor.g*=1.0;"
				"   gl_FragColor.b*=1.0;"
				"   gl_FragColor.a*=1.0;"
                "}\n";

