#ifndef UIWindow_H_
#define UIWindow_H_
#include <set>
#include <vector>
#include "UIBase.h"
#include "UIMisc.h"
#include "IUIContainer.h"
#include "Layout/ILayout.h"
#include "UIWndStyle.h"
#include "IUIMessage.h"
#include "UIMsgCracker.h"
#include  "UISkin.h"
#include "Layout/UiLayout.h"
namespace KKUI{


    class   CUIWindow;	
	#define UICWND_FIRST    ((CUIWindow*)-1)   /*子窗口插入在开头*/
	#define UICWND_LAST      NULL              /*子窗口插入在末尾*/

	typedef CUIWindow* (*FINDCONTROLPROC)(CUIWindow*, void*);
    enum{
		HRET_FLAG_STYLE = (1<<16),
		HRET_FLAG_LAYOUT = (1<<17),
		HRET_FLAG_LAYOUT_PARAM = (1<<18),
	};

	 typedef struct UIWNDMSG
    {
        unsigned int uMsg;
         unsigned int wParam;
       long lParam;
    } *LPUIWNDMSG;

     enum EDrag_Type
	{
	   EDrag_None     = 0,
	   EDrag_Client   = 1,
	   EDrag_NcClient = 2,
	   EDrag_Vect     = 3,
	   EDrag_WarpVect = 4,
	   EDrag_Cp1      = 5,
	   EDrag_Cp2      = 6,
	};
	 class UIMsgHandleState
    {
    public:
        UIMsgHandleState():m_bMsgHandled(0)
        {

        }

        int IsMsgHandled() const
        {
            return m_bMsgHandled;
        }

        void SetMsgHandled(int bHandled)
        {
            m_bMsgHandled = bHandled;
        }

        int m_bMsgHandled;
    };

	class CUIWindow:public CUIBase,public UIMsgHandleState
	{
		KKUI_CLASS_NAME_EX(CUIBase,CUIWindow,"uiwindow",UICtl)
			friend class CUIMsView;
	    public:
                 CUIWindow();
			     virtual ~CUIWindow();
		
				
				        void       OnPrint(IRenderTarget *pRT);
				virtual bool       IsVisible(int bCheckParent = 1) const;
                virtual void       SetVisible(bool bVisible = true);
                virtual int        GetCtlAttr(const SStringA& attrName,SStringA& OutAttr);
				
				///事件操作
				virtual void       OnEvent(UITEvent& Event);

                ///找到控件
				virtual CUIWindow* FindCtl(FINDCONTROLPROC Proc, void* pData,unsigned uFlags);


				CUIWindow *        GetParent() const;
				//插入子窗口
				virtual void       InsertChild(CUIWindow *pNewChild,CUIWindow *pInsertAfter=UICWND_LAST);
				virtual void       InitAfter(){};
				 /**
				 * RemoveChild
				 * @brief    从窗口树中移除一个子窗口对象
				 * @param    CUIWindow * pChild --  子窗口对象
				 * @return   BOOL 
				 *
				 * Describe  子窗口不会自动释放
				 */
                bool               RemoveChild(CUIWindow  *pChild); 
				//移动控件在pMoveAfter之后
				int                MoveChild(CUIWindow  *pChild,CUIWindow *pMoveAfter);              
                //销毁一个子窗口
                virtual bool       DestroyChild(CUIWindow  *pChild);


				IUIContainer*      GetContainer();
                void               SetContainer(IUIContainer *pContainer);


				
 
				//更新窗口位置
				virtual int        OnRelayout(const CRect &rcWnd);
			     //更新子窗口位置
                virtual void       UpdateChildrenPosition();

				virtual void       OnUpdateFloatPosition(const  CSize&  szView,const CRect & rcParent);
				//请求重新布局
                void               RequestRelayout();

				virtual void       RequestRelayout( CUIWindow *pSource,int bSourceResizable);
				void               UpdateLayout();
				virtual int        IsPanel(){return 0;}
				int          GetScale(){return 100;}
				int          GetID(){return m_nID;}
				void         SetID(int nID){m_nID=nID;}


				virtual      CUIWindow*   SetCapture();
	            int          ReleaseCapture();
                virtual int  OnSetCursor(const CPoint &pt);
				int          OnAttrName(const SStringA& strValue, int bLoading);

				void         Invalidate();
				void         InvalidateRect(CRect& rect);
				void         SetUserData(void* UserData);
				void*        GetUserData();


				unsigned int GetZorder() {return m_uZorder;}
	   public:

		         //KKUI_MSG_MAP_BEGIN()     
			    // WND_MSG_MAP_END_BASE()

				 KKUI_ATTRS_BEGIN()
					ATTR_CUSTOM("name",OnAttrName)
					ATTR_INT("visible",m_bVisible,1)
					ATTR_CHAIN_PTR((&m_style),HRET_FLAG_STYLE)				//交给SwndStyle处理
					ATTR_CHAIN_PTR(m_pLayout,HRET_FLAG_LAYOUT)				//交给Layout处理
					ATTR_CHAIN_PTR(m_pLayoutParam,HRET_FLAG_LAYOUT_PARAM)	//交给LayoutParam处理
				 KKUI_ATTRS_END()
	    protected:
			  
			   
			    
				
				virtual void OnActivate(unsigned int  nState);
               

				void   _PaintClient(IRenderTarget *pRT);
				virtual void   _PaintRegion(IRenderTarget *pRT,unsigned int iZorderBegin,unsigned int iZorderEnd);
				void   GetScaleSkin(ISkinObj * &pSkin,int nScale);
				void   ReleaseFocus();
	    public:
			    bool IsFloat() const{
			      return !!m_bFloat;
				}

				bool IsDisplay() const{
					return m_bDisplay!=0;
		        }
			    CUIWindow *GetWindow(int uCode) const;    
				ILayout * GetLayout(){return m_pLayout;}

				template<class T>
				T * GetLayoutParamT() const
				{
					return UIObj_cast<T>(m_pLayoutParam);
				}

				ILayoutParam * GetLayoutParam()
				{
					return m_pLayoutParam;
				}
				CUIWndStyle&   GetStyle();
				bool           SetLayoutParam(ILayoutParam * pLayoutParam);

				void           GetWindowRect(kkRect* pRect) const;
                virtual void   GetClientRect(kkRect* pRect) const;
				CUIWindow*     GetNextLayoutChild(CUIWindow *pCurChild);
				CRect          GetChildrenLayoutRect();

				int            InitFromXml(pugi::xml_node xmlNode);
				CUIWindow*     CreateChildren(char* strxml);
                virtual CUIWindow*     CreateChildren(pugi::xml_node xmlNode);
				void           SetZorder(unsigned int Zorder){ m_uZorder=Zorder;}
				int            GetChildrenCount(){return m_nChildrenCount;}


				int            IsMsgTransparent(){return m_bMsgTransparent;}
				int            IsDisabled(int bCheckParent = 0);
				int            GetState(){return m_dwState;}

				//
				virtual CUIWindow*     WndFromPoint( kkPoint  ptHitTest, int bOnlyText);
				virtual int    IsContainPoint(const kkPoint &pt,int bClientOnly);
				virtual int    OnNcHitTest(kkPoint pt);

				CSize         GetDesiredSize(kkRect* pRcContainer);
				CSize         GetDesiredSize(int nParentWid , int nParentHei );
			    // 将窗口移动到指定位置
                void          Move(kkRect* prect);

				//找到控件
				CUIWindow*    FindChildByName(const char* pszName , int nDeep=-1);
				virtual       SStringA GetName() const {return m_strName;}
                void          SetName(SStringA pszName){m_strName=pszName;}
		 public:
			    int           SendMessage(unsigned int Msg,uint_ptr wParam = 0, int_ptr lParam = 0,int *pbMsgHandled=NULL);
			   /**
				* SwndProc
				* @brief    默认的消息处理函数
				* @param    UINT uMsg --  消息类型
				* @param    WPARAM wParam --  参数1
				* @param    LPARAM lParam --  参数2
				* @param    LRESULT & lResult --  消息返回值
				* @return   BOOL 是否被处理
				*
				* Describe  在消息映射表中没有处理的消息进入该函数处理
				*/
				virtual int UIWndProc(unsigned int uMsg,uint_ptr wParam,int_ptr lParam,long& lResult){return 0;}
         protected:

                int  OnCreate( int);
			    ///绘制操作
				void OnPaint( IRenderTarget  *pRT);
				void OnEraseBkgnd( IRenderTarget  *pRT);

			    void OnSize(unsigned int nType, int x,int y);
                void OnDestroy();
				KKUI_MSG_MAP_BEGIN()   
				    MSG_UI_CREATE(OnCreate)
					MSG_UI_SIZE(OnSize)
					MSG_UI_PAINT(OnPaint)
					MSG_UI_ERASEBKGND(OnEraseBkgnd)
					MSG_UI_DESTROY(OnDestroy)
				KKUI_MSG_MAP_END_BASE()
		 protected:
				   CRect               m_rcWindow;         /**< 窗口在容器中的位置,由于它的值包含POS_INIT等，调整为private，不允许派生类中直接访问该变量的值 */
				   int                 m_nID;
				  
	    protected:
			    //布局脏标志类型
				enum LayoutDirtyType
				{
					dirty_clean = 0,//干净
					dirty_self = 1, //自己变脏
					dirty_child = 2, //有孩子变脏
				};
			    CAutoRefPtr<ILayout>          m_pLayout;
		        CAutoRefPtr<ILayoutParam>     m_pLayoutParam;
			    //窗口容器接口
			    IUIContainer*                 m_pUIContainer;
				CUIWindow *                   m_pParent;          /**< 父窗口 */
				
				CUIWindow *                   m_pFirstChild;      /**< 第一子窗口 */
				CUIWindow *                   m_pLastChild;       /**< 最后窗口 */
				CUIWindow *                   m_pNextSibling;     /**< 前一兄弟窗口 */
				CUIWindow *                   m_pPrevSibling;     /**< 后一兄弟窗口 */

				int                           m_nChildrenCount;   /**< 子窗口数量 */
                int                           m_bFloat;           /**< 窗口位置固定不动的标志 */

                int                           m_dwState;         /**< 窗口在渲染过程中的状态 */
				int                           m_bDisplay;        /**< 窗口隐藏时是否占位，不占位时启动重新布局 */
			    int                           m_bVisible;        /**< 窗口可见状态 */
				int                           m_bDisable;        /**< 窗口禁用状状态 */
			
				CUIWndStyle                   m_style;
				LayoutDirtyType               m_layoutDirty;
				unsigned int                  m_uZorder;          /**< 窗口Zorder */
				int                           m_bMsgTransparent;
				UIWNDMSG*                     m_pCurMsg;          /**< 当前正在处理的窗口消息 */
				CLayoutSize                   m_nMaxWidth;        /**< 自动计算大小时，窗口的最大宽度 */
				SStringA                      m_strName;          /**< 窗口名称 */
				void*                         m_pUserData;

				
	};
}
#endif