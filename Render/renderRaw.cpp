#include "stdafx.h"
#include "renderRaw.h"
extern FpRenderLock              G_pRenderLock;
extern FpRenderUnLock            G_pRenderUnLock;

extern FpCreateDxva2Ctx          G_pCreateDxva2Ctx;
extern FpFreeDxva2Ctx            G_pFreeDxva2Ctx;

extern FpCreateDXTexture         G_pCreateDXTexture;
extern FpFreeDXTexture           G_pFreeDXTexture;

CRenderRaw::CRenderRaw(): m_Reanderfp(0),m_pRenderUserData(0)
{

}
CRenderRaw::~CRenderRaw()
{

}
bool CRenderRaw::init(HWND hView)
{
   return true;
}
void CRenderRaw::destroy()
{

}
void  CRenderRaw::resize(unsigned int w, unsigned int h)
{

}
void  CRenderRaw::LoadCenterLogo(unsigned char* buf,int len)
{

}
void  CRenderRaw::render(kkAVPicInfo *Picinfo,bool wait)
{
	
	if(Picinfo!=NULL)
	{
		m_lock.Lock();
			if(m_Reanderfp!=NULL)
			m_Reanderfp(Picinfo,m_pRenderUserData);
		m_lock.Unlock();
	}
}
void  CRenderRaw::renderBk(unsigned char* buf,int len)
{

}

void CRenderRaw::SetWaitPic(unsigned char* buf,int len)
{

}
void CRenderRaw::SetBkImagePic(unsigned char* buf,int len)
{

}
void CRenderRaw::WinSize(unsigned int w, unsigned int h)
{

}

void CRenderRaw::SetErrPic(unsigned char* buf,int len)
{

}
void CRenderRaw::ShowErrPic(bool show)
{

}
void CRenderRaw::FillRect(kkBitmap img,kkRect rt,unsigned int color)
{

}
void CRenderRaw::SetLeftPicStr(const char *str)
{

}
void CRenderRaw::SetRenderImgCall(fpRenderImgCall fp,void* UserData)
{
	m_lock.Lock();
   
    m_Reanderfp=fp;
	m_pRenderUserData=UserData;
	m_lock.Unlock();
}
bool CRenderRaw::GetHardInfo(void** pd3d,void** pd3ddev,int *ver)
{
   return false;
}
void CRenderRaw::SetResetHardInfoCall(fpResetDevCall call,void* UserData)
{

}
void CRenderRaw::renderLock()
{
    if(G_pRenderLock)
		G_pRenderLock();
}
void CRenderRaw::renderUnLock()
{
   if(G_pRenderUnLock)
	   G_pRenderUnLock();
}

IDXVA2Ctx* CRenderRaw::CreateDxva2Ctx(int CodecId,int Width,int Height)
{
   if(G_pCreateDxva2Ctx)
	   return G_pCreateDxva2Ctx(CodecId,Width,Height);
   return 0;
}
void      CRenderRaw:: FreeDxva2Ctx (IDXVA2Ctx* ctx)
{
        if(G_pFreeDxva2Ctx)
			G_pFreeDxva2Ctx(ctx);
}

IDXTexture* CRenderRaw::CReateDXTexture()
{
   if(G_pCreateDXTexture)
       return G_pCreateDXTexture();
         return 0;
} 
void      CRenderRaw:: FreeDXTexture(IDXTexture* ctx)
{
       if(G_pFreeDXTexture)
		   G_pFreeDXTexture(ctx);
}