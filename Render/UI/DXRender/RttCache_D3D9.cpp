#include "RttCache_D3D9.h"
#include "RenderTarget_D3D9.h"
#include "DXVA2_D3D9.h"
#include "DX9Text.h"
#include "FastMemcpy.h"
namespace KKUI
{

   
	bool SYUVKey::operator < (const SYUVKey& right) const
	{
		  unsigned long long Key1 =  nWidth;
          Key1 =Key1 << 32;
		  Key1 +=nHeight;

		  unsigned long long Key2 =  right.nWidth;
          Key2 =Key2 << 32;
		  Key2 +=right.nHeight;
		  if(Key1<Key2)
			  return 1;
		  return 0;

	}

	CRttCache_D3D9::CRttCache_D3D9(SRenderFactory_D3D9* pRenderFactory):m_pRenderFactory(pRenderFactory)
	{
	
	}
	 
	CRttCache_D3D9::~CRttCache_D3D9()
	{
	       ReleaseX();
		   std::map<unsigned long long,std::vector<SRender3DTexture*> >::iterator It=m_BufferRenderMap.begin();
		   for(; It!=m_BufferRenderMap.end();It++)
		   {
			   std::vector<SRender3DTexture*>::iterator Sit = It->second.begin();
			   for(; Sit!=It->second.end();Sit++)
			   {
				   SRender3DTexture* ItTx=*Sit;
				  delete ItTx;
			   }
		   }
		   m_BufferRenderMap.clear();
	}
	void       CRttCache_D3D9::ReleaseX()
	{
		   std::map<unsigned long long,std::vector<SRender3DTexture*> >::iterator It=m_BufferRenderMap.begin();
		   for(; It!=m_BufferRenderMap.end();It++)
		   {
			   std::vector<SRender3DTexture*>::iterator Sit = It->second.begin();
			   for(; Sit!=It->second.end();Sit++)
			   {
				   SRender3DTexture* ItTx=*Sit;
				   if(ItTx->pTexture)
				   ItTx->pTexture->Release();
				   ItTx->pTexture=0;
			   }
		   }


		   std::map<SYUVKey, SYUVInfoTex >::iterator Ityuv=  m_YUVInfoTexMap.begin();
		   for(; Ityuv!=m_YUVInfoTexMap.end(); Ityuv++)
		   {
			    SYUVInfoTex& Item= Ityuv->second;
                Item.pYTexture->Release();
		        Item.pUTexture->Release();
		        Item.pVTexture->Release();
		   }
		   m_YUVInfoTexMap.clear();
	}
	void       CRttCache_D3D9::ReLoadResX()
	{
		             IDirect3DDevice9* m_pDevice = m_pRenderFactory->GetDevice();
	                 std::map<unsigned long long,std::vector<SRender3DTexture*> >::iterator It=m_BufferRenderMap.begin();
					 for(; It!=m_BufferRenderMap.end();It++)
					 {
					   std::vector<SRender3DTexture*>::iterator Sit = It->second.begin();
					   for(; Sit!=It->second.end();Sit++)
					   {
						   SRender3DTexture* tx=*Sit;
					   
						   HRESULT hr =m_pDevice->CreateTexture( tx->Width,tx->Height,
							1,
							D3DUSAGE_RENDERTARGET,
							D3DFMT_A8R8G8B8,//D3DFMT_R5G6B5,
							D3DPOOL_DEFAULT,
							&tx->pTexture,
							NULL) ;

					   }
					 }
	}
	//渲染缓冲对象
    IDirect3DSurface9*          CRttCache_D3D9::GetBufferRenderTexture(int width,int height, IDirect3DTexture9**      ppTexture)
	{
	      IDirect3DSurface9*         pSurface = 0;
	      unsigned long long Key =  width;
          Key =Key << 32;
		  Key +=height;
		  std::map< unsigned long long,  std::vector<SRender3DTexture*> >::iterator It=    m_BufferRenderMap.find(Key);
		  if(  It==    m_BufferRenderMap.end())
		  {
			  std::vector<SRender3DTexture*> vec;
			  m_BufferRenderMap.insert(std::pair< unsigned long long,  std::vector<SRender3DTexture*> >( Key,vec));
              It=m_BufferRenderMap.find(Key);
			 
		  }


		  std::vector<SRender3DTexture*>::iterator  ItVec = It->second.begin();

		  SRender3DTexture* tx=0;
		  for(; ItVec!=It->second.end();ItVec++)
		  {
			  if(!(*ItVec)->Used)
			  {
			     tx=(*ItVec);
				 break;
			  }
		  }

		  if(tx==0){
		       tx = new SRender3DTexture();
			   //m_pRenderFactory->GetDevice()->CreateRenderTarget(
			    HRESULT hr =m_pRenderFactory->GetDevice()->CreateTexture( width, height,
				1,
				D3DUSAGE_RENDERTARGET,
				D3DFMT_A8R8G8B8,//D3DFMT_R5G6B5,
				D3DPOOL_DEFAULT,
				&tx->pTexture,
				NULL) ;
				tx->Height = height;
				tx->Width  = width;

				It->second.push_back(tx);
		  }
          
		  tx->Used =1;
				
		  *ppTexture= tx->pTexture;
		  tx->pTexture->GetSurfaceLevel(0, &pSurface);
		  return pSurface;
	}
	void        CRttCache_D3D9::CreateRttCache(int width,int height)
	{
		
	      IDirect3DSurface9*         pSurface = 0;
	      unsigned long long Key =  width;
          Key =Key << 32;
		  Key +=height;
		  std::map< unsigned long long,  std::vector<SRender3DTexture*> >::iterator It=    m_BufferRenderMap.find(Key);
		  if(  It== m_BufferRenderMap.end()){
			  std::vector<SRender3DTexture*> vec;
			  m_BufferRenderMap.insert(std::pair< unsigned long long,  std::vector<SRender3DTexture*> >( Key,vec));
              It=m_BufferRenderMap.find(Key);
			 
		  }else{
		      return;
		  }


		  std::vector<SRender3DTexture*>::iterator  ItVec = It->second.begin();

		  SRender3DTexture* tx=0;
		  for(; ItVec!=It->second.end();ItVec++)
		  {
			  if(!(*ItVec)->Used)
			  {
			     tx=(*ItVec);
				 break;
			  }
		  }

		  if(tx==0){
		       tx = new SRender3DTexture();
			   //m_pRenderFactory->GetDevice()->CreateRenderTarget(
			    HRESULT hr =m_pRenderFactory->GetDevice()->CreateTexture( width, height,
				1,
				D3DUSAGE_RENDERTARGET,
				D3DFMT_A8R8G8B8,//D3DFMT_R5G6B5,
				D3DPOOL_DEFAULT,
				&tx->pTexture,
				NULL) ;
				tx->Height = height;
				tx->Width  = width;

				
				IDirect3DSurface9*         pSurface = 0;
		        tx->pTexture->GetSurfaceLevel(0, &pSurface);
                pSurface->Release();
				m_pRenderFactory->GetDevice()->ColorFill( pSurface , 0, D3DCOLOR_ARGB(0, 0, 0, 0));
				
				It->second.push_back(tx);
		  }
          
	  
	}
	void                        CRttCache_D3D9::ReleaseBufferRenderTexture(int width,int height, IDirect3DTexture9*  pTexture)
	{
	      unsigned long long Key =  width;
          Key =Key << 32;
		  Key +=height;
		  std::map< unsigned long long,  std::vector<SRender3DTexture*> >::iterator It=    m_BufferRenderMap.find(Key);
		  if(  It==    m_BufferRenderMap.end())
		  {
			 
			 return;
		  }

	
		  std::vector<SRender3DTexture*>::iterator vecIt = It->second.begin();
		  for(;vecIt  != It->second.end(); vecIt++)
		  {
		     SRender3DTexture* tx=*(vecIt);
			 if(tx->pTexture == pTexture)
			 {
				 tx->Used =0;
				 break;
			 }
		  }
	}

	 int                   CRttCache_D3D9::GetCacheYuvInfo(int width,int height,int yuvFormat,IDirect3DTexture9** ppYTexture,IDirect3DTexture9**   ppUTexture,IDirect3DTexture9**      ppVTexture)
	 {
		   SYUVKey Key={ width,height,yuvFormat};
		   std::map<SYUVKey, SYUVInfoTex >::iterator It=   m_YUVInfoTexMap.find(Key);
		   if(It!=   m_YUVInfoTexMap.end())
		   {
		       *ppYTexture = It-> second.pYTexture;
			   *ppUTexture = It-> second.pUTexture;
			   *ppVTexture = It-> second.pVTexture;
			   return 1;
		   }


		   int pixel_h = height;
		   int pixel_u =  pixel_h;
		   int pixel_v =  pixel_h;   
		   IDirect3DDevice9* pDevice= m_pRenderFactory-> GetDevice();
		   if(yuvFormat==0){
							pDevice->CreateTexture ( width, height, 1, D3DUSAGE_DYNAMIC, D3DFMT_L8, D3DPOOL_DEFAULT, ppYTexture, NULL ) ;
							pDevice->CreateTexture ( (width+1)/2, (height+1) / 2, 1, D3DUSAGE_DYNAMIC, D3DFMT_L8, D3DPOOL_DEFAULT, ppUTexture, NULL ) ;
							pDevice->CreateTexture ( (width+1)/2, (height+1) / 2, 1, D3DUSAGE_DYNAMIC, D3DFMT_L8, D3DPOOL_DEFAULT, ppVTexture, NULL ) ;

							 pixel_u =  pixel_h/2;
		                     pixel_v =  pixel_h/2; 
		   }else if(yuvFormat==1){
				 pDevice->CreateTexture( width, height, 1, D3DUSAGE_DYNAMIC, D3DFMT_L8, D3DPOOL_DEFAULT, ppYTexture, NULL ) ;
				 pDevice->CreateTexture ((width + 1) / 2, height, 1, D3DUSAGE_DYNAMIC, D3DFMT_L8, D3DPOOL_DEFAULT, ppUTexture, NULL ) ;
				 pDevice->CreateTexture ((width + 1) / 2, height, 1, D3DUSAGE_DYNAMIC, D3DFMT_L8, D3DPOOL_DEFAULT, ppVTexture, NULL ) ;
		   }else if(yuvFormat==2) {
				 pDevice->CreateTexture( width, height, 1, D3DUSAGE_DYNAMIC, D3DFMT_L16, D3DPOOL_DEFAULT, ppYTexture, NULL ) ;
				 pDevice->CreateTexture ((width + 1) / 2, height, 1, D3DUSAGE_DYNAMIC, D3DFMT_L16, D3DPOOL_DEFAULT, ppUTexture, NULL ) ;
				 pDevice->CreateTexture ((width + 1) / 2, height, 1, D3DUSAGE_DYNAMIC, D3DFMT_L16, D3DPOOL_DEFAULT, ppVTexture, NULL ) ;
		   }else {
		     assert(0);
		   }	 


		    
		   SYUVInfoTex Text={*ppYTexture,*ppUTexture,*ppVTexture};


		   //解决首次慢的问题
		   {

		            D3DLOCKED_RECT d3d_rect;
					LRESULT lRet = Text.pYTexture->LockRect(0, &d3d_rect, 0, 0);
					
					//Y
					byte *pDest = (byte *)d3d_rect.pBits;
					int stride = d3d_rect.Pitch; 
					void* tempbuf = ::malloc(stride );
					for( int i=0;i < pixel_h;i ++){  
					    memcpy(pDest+ i * stride,tempbuf,stride);
					}
					
					Text.pYTexture->UnlockRect(0);

					//U
					D3DLOCKED_RECT d3d_rectu;
					lRet =  Text.pUTexture->LockRect(0, &d3d_rectu, 0, 0);
					pDest = (byte *)d3d_rectu.pBits;
                    stride = d3d_rectu.Pitch; 
					for( int i=0;i < pixel_u;i ++){  
                      	memcpy(pDest+ i * stride,tempbuf,stride);
					}
					Text.pUTexture->UnlockRect(0);


					//V
					D3DLOCKED_RECT d3d_rectv;
					lRet =  Text.pVTexture->LockRect(0, &d3d_rectv, 0, 0);
					pDest = (byte *)d3d_rectv.pBits;
                    stride = d3d_rectv.Pitch; 
					for( int i=0;i < pixel_u;i ++){  
                   	memcpy(pDest+ i * stride,tempbuf,stride);
					}
					Text.pVTexture->UnlockRect(0);

					free(tempbuf );
	       }
		   m_YUVInfoTexMap.insert(std::pair< SYUVKey,SYUVInfoTex>(Key, Text));
		   return 1;
	 }
}