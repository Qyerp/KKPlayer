#include "UIScrollView.h"
namespace KKUI
{
	CUIScrollView::CUIScrollView():m_bAutoViewSize(0)
	{
		//this->SetContainer(this);
		//CUIContainerImpl::SetWindow(this);
		m_szView.cx =8000;
		m_szView.cy =5000;
	}
	CUIScrollView::~CUIScrollView()
	{
	
	}

	void  CUIScrollView::GetOffXY(int& x,int& y,int& WinWidth,int& WinHeight)
	{
		    x=0;
			y =0;
			if(HasScrollBar(1))
				y = m_siVer.nPos;
			if(HasScrollBar(0))
				x =  m_siHoz.nPos;

		    CRect rcClient;
			CUIWindow::GetClientRect(&rcClient);
			WinWidth = rcClient.Width();
			WinHeight= rcClient.Height();

	}
	void  CUIScrollView::OnSize(unsigned int nType, int x,int y)
	 {
		    CSize size;
		    size.cx = x;
		    size.cy = y;
            CUIPanel::OnSize(nType,x,y);
			if(m_bAutoViewSize)
			{
				//计算viewSize
				CSize szOld = m_szView;
				//m_szView = GetLayout()->MeasureChildren(this,size.cx,size.cy);
				 CalViewSz();
				if(szOld != m_szView)
				{
					OnViewSizeChanged(szOld,m_szView);
				}
			}
			UpdateScrollBar();
	}
	

	void CUIScrollView::UpdateScrollBar()
	{
			CRect rcClient;
			CUIWindow::GetClientRect(&rcClient);
			int sbx = GetSbWidth();;
			CSize size=rcClient.Size();
			//减去滚动条宽度
			if(HasScrollBar(1))
			   size.cx -= sbx ;
			if(HasScrollBar(0))
			    size.cy -= sbx ;

			m_wBarVisible=SSB_NULL;    //关闭滚动条
			CPoint ptOrigin = m_ptOrigin;//backup

			if(size.cy<m_szView.cy || (size.cy<m_szView.cy&& size.cx<m_szView.cx))
			{
				//需要纵向滚动条
				m_wBarVisible|=SSB_VERT;
				m_siVer.nMin=0;
				m_siVer.nMax=m_szView.cy-1;
				m_siVer.nPage=size.cy;
				if(m_siVer.nPos==0&& m_siVer.nTrackPos!=0)
                      m_siVer.nPos=m_siVer.nTrackPos;

				if(m_siVer.nPos + (int)m_siVer.nPage > m_siVer.nMax)
				{
					m_siVer.nPos = m_siVer.nMax - m_siVer.nPage;
					m_ptOrigin.y = m_siVer.nPos;
				}
				if(size.cx<m_szView.cx+GetSbWidth())
				{
					//需要横向滚动条
					m_wBarVisible |= SSB_HORZ;
					m_siVer.nPage=size.cy-GetSbWidth() > 0 ? size.cy-GetSbWidth() : 0;

					m_siHoz.nMin=0;
					m_siHoz.nMax=m_szView.cx-1;
					m_siHoz.nPage=size.cx-GetSbWidth() > 0 ? size.cx-GetSbWidth() : 0;
					if(m_siHoz.nPos==0&& m_siHoz.nTrackPos!=0)
                      m_siHoz.nPos=m_siHoz.nTrackPos;

					if(m_siHoz.nPos + (int)m_siHoz.nPage > m_siHoz.nMax)
					{
						m_siHoz.nPos = m_siHoz.nMax - m_siHoz.nPage;
						m_ptOrigin.x = m_siHoz.nPos;
					}
				}
				else
				{
					//不需要横向滚动条
					m_siHoz.nPage=size.cx;
					m_siHoz.nMin=0;
					m_siHoz.nMax=m_siHoz.nPage-1;
					m_siHoz.nPos=0;
					m_ptOrigin.x=0;
				}
			}
			else
			{
				//不需要纵向滚动条
				m_siVer.nPage=size.cy;
				m_siVer.nMin=0;
				m_siVer.nMax=size.cy-1;
				m_siVer.nPos=0;
				m_ptOrigin.y=0;
                   
				if(size.cx<m_szView.cx)
				{
					//需要横向滚动条
					m_wBarVisible|=SSB_HORZ;
					m_siHoz.nMin=0;
					m_siHoz.nMax=m_szView.cx-1;
					m_siHoz.nPage=size.cx;
					if(m_siHoz.nPos==0&& m_siHoz.nTrackPos!=0)
                      m_siHoz.nPos=m_siHoz.nTrackPos;

					if(m_siHoz.nPos + (int)m_siHoz.nPage > m_siHoz.nMax)
					{
						m_siHoz.nPos = m_siHoz.nMax - m_siHoz.nPage;
						m_ptOrigin.x = m_siHoz.nPos;
					}
				}
				//不需要横向滚动条
				else
				{
					m_siHoz.nPage=size.cx;
					m_siHoz.nMin=0;
					m_siHoz.nMax=m_siHoz.nPage-1;
					m_siHoz.nPos=0;
					m_ptOrigin.x=0;
				}
			}

			SetScrollPos(TRUE,m_siVer.nPos,TRUE);
			SetScrollPos(FALSE,m_siHoz.nPos,TRUE);

			SendMessage(UI_NCCALCSIZE);

			Invalidate();
	 }

	void CUIScrollView::OnViewSizeChanged( CSize szOld,CSize szNew )
	{
		
	}
	
    void CUIScrollView::SetViewSize(CSize szView)
	{
	    if(szView == m_szView) 
			return;
    
		CSize oldViewSize=m_szView;
		m_szView=szView;
		UpdateScrollBar();
		OnViewSizeChanged(oldViewSize,szView);
	}
   void  CUIScrollView::CalViewSz()
   {
	        int x = 0;
			int y = 0;
	        CUIWindow *pChild=GetWindow(GSW_FIRSTCHILD);
			kkRect Rect;
			while(pChild)
			{
				
				pChild->GetWindowRect(&Rect);
				if(Rect.right >  x)
					x = Rect.right;

				if(Rect.bottom  >  y)
					y = Rect.bottom;
				pChild=pChild->GetWindow(GSW_NEXTSIBLING);
			}

		
			x+=m_siHoz.nTrackPos;
			y+=m_siVer.nTrackPos;
			CRect rcWnd = GetClientRect();
			CRect rcMargin = GetStyle().GetMargin();
			rcWnd.DeflateRect(rcMargin);
			rcWnd.DeflateRect(GetStyle().GetPadding());
			int sbw=this->GetSbWidth();
			if(HasScrollBar(1))
			   rcWnd.right  -=sbw;
			if(HasScrollBar(0))
			   rcWnd.bottom -=sbw;
			if(x<rcWnd.right )
				x= rcWnd.right;
			if(y<rcWnd.bottom )
				y= rcWnd.bottom;

		 /*   m_szView.cx = x - rcWnd.left;
			m_szView.cy = y - rcWnd.top;*/

		/*	int cx = x - rcWnd.left;
			int cy = y - rcWnd.top;
			if(m_szView.cx <cx)
                 m_szView.cx=cx;
			if(m_szView.cy<cy )
                 m_szView.cy=cy;*/

   }
	void CUIScrollView::UpdateChildrenPosition()
	{

		if(!m_bAutoViewSize)
		{
			CUIPanel::UpdateChildrenPosition();
		}
		else{//计算viewSize
			CSize szOld = m_szView;
            CalViewSz();
			CUIPanel::UpdateChildrenPosition();
			UpdateScrollBar();
            OnNcCalcSize(0,0);
			if(szOld != m_szView)
			{
				OnViewSizeChanged(szOld,m_szView);
			}
		}
	}

}