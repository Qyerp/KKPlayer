#ifndef IFusionzone_H_
#define IFusionzone_H_
#include <vector>
#include "Core/IRender.h"
class IFusionzone
{
  public:
	  virtual void DrawFusionzone(KKUI::IRenderTarget* pRT,kkRect& rt,int syn) = 0;
          SUVInfo     m_XY1;
		  SUVInfo     m_XY2;
		  SUVInfo     m_XY3;
		  SUVInfo     m_XY4;
};
class IFusionzoneGather
{
public:
	virtual std::vector<IFusionzone*> GetIFusionzoneVect() = 0;
};
#endif